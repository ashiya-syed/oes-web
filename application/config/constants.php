<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define("APP_FOLDER","meeExam");
define("ROOT",$_SERVER['DOCUMENT_ROOT']."/".APP_FOLDER);
//define("SITEURL2","https://".$_SERVER['SERVER_NAME']."/".APP_FOLDER."index.php");
define("SITEURL","http://".$_SERVER['SERVER_NAME']."/".APP_FOLDER.'/');
define("SITEURL2","http://".$_SERVER['SERVER_NAME']."/".APP_FOLDER.'/');


define("LOGOUT_URL",SITEURL.'account/logout');
define("DASHBOARD_URL",SITEURL.'dashboard');
define("ACCOUNT_URL",SITEURL.'account');
define("REGISTRATION_URL",SITEURL.'account/registration');
define("VERIFICATION_URL",SITEURL.'account/verifyAccount');
define("PROFILE_URL",SITEURL.'account/profile');
define("FORGOTPASSWORD",SITEURL.'account/forgotpassword');
define("RECOVERPASSWORD",SITEURL.'account/resetpassword');
define("CHANGEPASSWORD",SITEURL.'account/changepassword');
define("START_EXAM_URL",SITEURL.'dashboard/startExam');
define("EXAM_URL",SITEURL.'dashboard/questionPaper');
define("SUBMIT_EXAM_URL",SITEURL.'dashboard/submitTest');
define("SUBMIT_ON_BROWSER_CLOSE_URL",SITEURL.'dashboard/browserCloseSubmit');
define("VIEW_RECORD_URL",SITEURL.'dashboard/viewRecord');
define("OTP_URL",SITEURL.'dashboard/verifyOtp');
define("VIEW_SYLLABUS_URL",SITEURL.'dashboard/viewSyllabus');
define("BUY_PACKAGES_URL",SITEURL.'dashboard/buypackage');
define("SINGLE_PAGE_URL",SITEURL.'dashboard/newsDetails');
define("FAILURE_URL",SITEURL.'dashboard/failure');
define("NOT_FOUND_URL",SITEURL.'dashboard/notFound');
define("CART_URL",SITEURL.'dashboard/cart');
define("BUY_PACKAGE_OR_TEST_URL",SITEURL.'dashboard/purchasePackageOrTest');

define("REDIRECT_URL",SITEURL.'account/redirect');
//define("USER_DASHBOARD_URL",SITEURL2.'/Account/d');
define("UPLOADS_OPTIONS_PATH",SITEURL.'admin/uploads/options/');
define('QUESTION_SUMMARY_URL',SITEURL.'dashboard/summeryReport');//new
define('VIEWMORE',SITEURL.'dashboard/viewMoreRecords');//new
define('CONTACT_US',SITEURL.'Content/contactUs');    

//on 14 the
define('TBL_TEST_QUESTIONS','oes_test_questions');//qb


define("ADMINURL","http://".$_SERVER['SERVER_NAME']."/".'admin');

define("LIBRARYP_PATH",ADMINURL.'/uploads/libraries/');//on 14 th


define("PROFILE_IMAGE_PATH",SITEURL."uploads/");
define("UPLOADS_PATH",ROOT."/uploads/");
define("SAVE_OPTIONS_URL",SITEURL.'dashboard/saveOptions');
define("IMAGE_PATH",SITEURL.'uploads/');


define('ASSETS_URL', SITEURL."assets/");
define('CSS_URL', ASSETS_URL."css/");
define('JS_URL', ASSETS_URL."js/");
define('IMAGES_URL', ASSETS_URL."images/");
define('FONT_URL', ASSETS_URL."fonts/");

//tables
define('TBL_USERS','users');
define('TBL_TESTS','tests');
define('TBL_QUETIONS','quetions');
define('TBL_QUE_OPTIONS','que_options');
define('TBL_PACKAGES','packages');
define('TBL_CITIES','oes_cities');
define('TBL_STATES','oes_states');
define('TBL_EXAM_RESULTS','oes_exam_results');
define('TBL_TIMES','oes_time');
define('TBL_USER_PACKAGE','oes_user_packages');
define('TBL_OTPS','oes_otp');
define('TBL_COURSES','oes_courses');
//define('TBL_PAYU','payukey');
define('TBL_NEWS','oes_news');
define('RECOVERY_EMAILS','oes_recoveryemails');
define('TBL_ENROLLMENTDETAILS','oes_enrollmentDetails');
define('TBL_CART','oes_cart');
define('TBL_QUESTIONS_ORDER','oes_questions_reference');
define('TBL_USER_COUPONS_DETAILS','oes_user_coupon_details ');//new
define('TBL_COUPONS','oes_coupons ');
define('TBL_OPTIONS_REFERENCE','oes_options_reference');//new
define('BUY_PACK_PAYU',SITEURL.'dashboard/buyPackThroughPayU');//on 14 th
define("LIBRARIES_URL",SITEURL.'dashboard/libraries');//on 14 th
define("GET_LIB_DETAILS",SITEURL.'dashboard/getLibDetails');//on 14 th
define('TBL_LIBRARIES','oes_libraries');//new
define('TBL_USER_TRACK','oes_usertrack');//19th
define('TBL_CC_CALLBACKS','ccavenue_callbacks');
define('TBL_PROMOTER_PACKAGES','oes_promoter_packages');//17-18
define('TBL_PROMOTER_TESTS','oes_promoter_tests');//17-18
define('TBL_USER_COUPONS','oes_user_coupons');//17-18
define('TBL_QUE_IMAGES','oes_questions_images');//17-18


//on november
define("PROMOTOR_LOGOUT_URL",SITEURL.'Promotion/logout');
define("PROMOTION_MEMBER_LOGIN_URL",SITEURL.'promotion/');
define("PROMOTOR_EXAM_URL",SITEURL.'Promotion/promotion_questionPaper');
define("PROMOTER_DASHBOARD_URL",SITEURL.'Promotion/dashborad');
define("PROMOTION_OTP_URL",SITEURL.'dashboard/promotionVerify');
//end
define("CCAVENUE_URL",SITEURL.'ccavenue/orderPaymentRequestHandler.php');
define("CCAVENUE_REQUEST_URL",SITEURL.'dashboard/ccavenueRequestHandler');
define("CCAVENUE_RESPONSE_URL",SITEURL.'dashboard/callbackThroughCCAvenue');
define("CCAVENUE_SUCCESS_URL",SITEURL.'dashboard/saveDataAfterCCavenueSuccess');
define("CCAVENUE_MERCHANTID","155713");
define("CCAVENUE_WORKING_KEY","F0E3BDEAB0823BA212521CBE8E203784");
define("CCAVENUE_ACCESS_CODE","AVLN75EL18CC38NLCC");
define("LOGOUT_FROM_ALL_DEVICES",SITEURL.'account/logoutFromAllDevices');


//user roles
define('USER_ROLE_ID','2');
define('SUPERADMIN_ROLE_ID','1');
//test types
define('MAIN_TEST','1');
define('PRACTICAL_TEST','2');

define('DEFAULT_LIMIT','10');


//test status
define('TEST_COMPLETED','3');
define('TEST_PENDING','2');
define('TEST_INCOMPLETE','1');

//items
define('PACKAGES','1');
define('TESTS','2');


define("USER_REGISTRATION_SUCCESS",'Registration success');
define("USER_REGISTRATION_FAILED",'Registration failed');
define("PHONE_NUMBER_ALREADY_EXISTS","This phone number already exists.");
define("PHONE_NUMBER_OR_EMAIL_ALREADY_EXISTS","This phone number or email already exists.");
define("RECORDS_EXISTS","Records exists");
define("NO_RECORDS_EXISTS","No records exists");
define("LOGIN_SUCCESS","loggin success");
define("REGISTRATION_SUCCESS","Registration success");
define("EMAIL_OR_PHONE_AND_PASSWORD_NOT_MATCH","Failed to authenticate email or phone number,passwords are invalid.");
define("PHONE_NUMBER_OR_EMAIL_OR_NAME_ALREADY_EXISTS","The phone number or email already exists.");
define("SUCCESS","Success.");
define("ERROR","Error Occured.");
define("SUCCESSFULLY_UPDATED","Successfully updated.");
define("NOT_UPDATED","Not Updated.");
define("SUCCESSFULLY_DELETED","Successfully Deleted.");
define('USER_CREATION_FAILED','Registration failed.');
define('USER_CREATION_SUCCESS','Registration success.');
define("UPDATE_SUCCESS","Your Profile updated successfully.");
define("NO_PACKAGES_TESTS","No examination test available in your account.");
define("NO_PRACTISE_TESTS_EXISTS","Practice tests are not available.");
define("NO_PACKAGES_AVAILABLE","Packages are not available.");
define("NO_MAIN_TESTS_EXISTS","Tests are not available.");
define("ADMIN_EXAM_URL",SITEURL.'dashboard/admin_questionPaper');
define("ADMIN_OTP_URL",SITEURL.'dashboard/adminVerify');
define('QUETION_MANAGEMENT_URL',ADMINURL.'/authority/d/maintest/quetions');    
define('ADMIN_QUESTIONPAPER_URL',SITEURL.'dashboard/admin_questionPaper');    
define('ADMIN_FIFTY_FIFTY_QUE',SITEURL.'dashboard/getadminFiftyFiftyOptions');    




define('PASSWORD_UPDATE_SUCCESS','Password successfully updated.');                                                                                              
define('UPDATE_FAILED','Updating failed.');
define('INVALID_OTP','Invalid otp.');
define('UNABLE_TO_ATTEMPT_EXAM','Your unable to attempt test because already you completed this test 3 times.');
define('CART_ITEMS_NOT_FOUND','Your cart is empty.');
define('TESTS_NOT_FOUND','This package does not contain any tests.');
define('NEWS_UPDATES_NOT_FOUND','News and updates are not found.');

define("SELECT_COURSE_URL",SITEURL.'account/selectCourses');
define("PAYMENT_HISTORY",SITEURL.'dashboard/paymentHistory');

             

/* End of file constants.php */
/* Location: ./application/config/constants.php */