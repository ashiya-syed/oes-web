<?php

Class Packagemodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_PACKAGES;
    protected $_order_by = 'package_id desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
	function promoterDetails($resellerId){
		$checkIsPromoter="";
		 $sql="SELECT * FROM oes_resellers WHERE reseller_id= ".$resellerId."  AND isDeleted=0 AND isActive=1 AND isReseller = 2";
		 $query=$this->db->query($sql);
		 if($query){
		 $checkIsPromoter=$query->row();
		 }
		if($checkIsPromoter){
			$data['tests']="";
			$data['packages']="";
			$sql1="SELECT  GROUP_CONCAT(p_test_id SEPARATOR ', ') AS tests FROM oes_promoter_tests where p_t_promoter_id =".$resellerId."  AND p_t_is_active =1 AND p_t_is_deleted=0";
			$query=$this->db->query($sql1);
			if($query){
				$tests=$query->row();
				$data['tests']=$tests->tests;
			}
		    $sql2="SELECT  GROUP_CONCAT(p_package_id SEPARATOR ', ') AS packages FROM oes_promoter_packages where p_promoter_id =".$resellerId." AND p_is_active =1 AND p_is_deleted=0";
			$query=$this->db->query($sql2);
			if($query){
				$packages=$query->row();
			    $data['packages']=$packages->packages;
			}
			return $data;
		}else{
			return false;
		}
		
	}
    
      function getPackages()
      {
		  $resellerId=($this->session->userdata('reseller_id'))?$this->session->userdata('reseller_id'):0;
		  $course=$this->session->userdata('course');//16-18
		  $isPromoterUser=$this->promoterDetails($resellerId);//16-18
		  $packages=$isPromoterUser['packages'];
		if(empty($resellerId)){ $resellerId = 0;}
			 $sql="SELECT `package_id`, `package_name`, `package_amount`, `package_test`, `isPaidPackage`, `courseId`, `isActive`, `isDeleted` FROM `oes_packages` WHERE `courseId` IN (".$course.")  AND isPaidPackage = 1 AND isActive =1 AND isDeleted = 0  ";
			if(empty($packages)){
				$sql.=" AND `resellerId`= ".$resellerId."";
			}else{
				$sql.=" AND package_id IN (".$packages.")";
			}
			 $query=$this->db->query($sql); 
			 if($query->num_rows() >0){
				 if($packages){
					 $newpacks=$query->result();
				      foreach($newpacks as $pack){
					      $amountsql="select p_sell_amount from oes_promoter_packages where p_promoter_id = ".$resellerId." and p_package_id = ".$pack->package_id."";
					      $amount=$this->db->query($amountsql)->row();
						  $pack->package_amount=$amount->p_sell_amount;
						  $p[]=$pack;
					} 
					return $p;
				 }else{
					 return $query->result();
				 }
			 }else{
				 return array();
			 }
		// return ($query->num_rows() >0)?$query->result():array();
    }  
   function getalltestsByPackage($ids)
      {
		 $this->db->select('*'); 
		 $this->db->from(TBL_TESTS);
		 $this->db->where('isActive',1);
		 $this->db->where_in('testId',$ids);
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->result():'';
    }  
	  	
	
}








