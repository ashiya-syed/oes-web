<?php

Class Testsmodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_TESTS;
    protected $_order_by = 'testId desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
	function promoterDetails($resellerId){
		 $checkIsPromoter="";
		 $sql="SELECT * FROM oes_resellers WHERE reseller_id= ".$resellerId."  AND isDeleted=0 AND isActive=1 AND isReseller = 2";
		 $query=$this->db->query($sql);
		 if($query){
			 $checkIsPromoter=$query->row();
        }
		if($checkIsPromoter){
			$data['tests']="";
			$data['packages']="";
			$sql1="SELECT  GROUP_CONCAT(p_test_id SEPARATOR ', ') AS tests FROM oes_promoter_tests where p_t_promoter_id =".$resellerId."  AND p_t_is_active =1 AND p_t_is_deleted=0";
			$query=$this->db->query($sql1);
			if($query){
				$tests=$query->row();
				$data['tests']=$tests->tests;
			}
		    $sql2="SELECT  GROUP_CONCAT(p_package_id SEPARATOR ', ') AS packages FROM oes_promoter_packages where p_promoter_id =".$resellerId." AND p_is_active =1 AND p_is_deleted=0";
			$query=$this->db->query($sql2);
			if($query){
				$packages=$query->row();
			    $data['packages']=$packages->packages;
			}
			return $data;
		}else{
			return false;
		}
		
	}
     function getTests($testType,$limit,$offet=0)
	{ 
	 	
		 if($testType ==1)
		 {
			 $mainarray=array();
       	     $testArray=array();
			 $myPurchasedTests=array();
             $myPackageTests=array();
			 $userId=$this->session->userdata('user_id');
			 $this->db->select('*'); 
			 $this->db->from(TBL_USER_PACKAGE);
			 $this->db->where('isDeleted',0);
			 $this->db->where('userId',$userId);
			 $this->db->where('isActive',1);
			 $this->db->order_by('id','desc');
			 $query = $this->db->get(); 
			 $results=$query->result();  
			 if($results){
                  
				  foreach($results as $result){
				          $testId=$result->packageTests;
				          $packageId=$result->packageId;
				          $uniqueId=$result->id;
						  if($result->amount == 0 && $result->txnid ==0){
							  $puchasedOne=0;
						  }else{
							  $puchasedOne=1;
						  }
						   $validSql="SELECT DATEDIFF(package_valid_to,package_valid_from) as diff FROM oes_packages WHERE package_id = $packageId";
				           $validityCheck=$this->db->query($validSql)->row();
						  
					if(@$validityCheck->diff){
				           $validityCheck=$validityCheck->diff;
					       $createdTime=$result->createdTime;
						   $expiryDate = date('Y-m-d h:i:s', strtotime("+$validityCheck days", strtotime($createdTime)));
						   $A=strtotime($expiryDate);$B=strtotime($createdTime);
						   if($A >= $B){  
						  $sql1="SELECT * FROM oes_tests WHERE testId IN ($testId)  and  isActive=1 and isDeleted=0 and isFree = 0";
						  $query = $this->db->query($sql1); 
					      $mypckTests=$query->result();
						  foreach($mypckTests as $mypckTest){
							$mypckTest->main='1';
							$mypckTest->puchasedOne=$puchasedOne;
							$mypckTest->expiryDate=$expiryDate;
							$mypckTest->packageId=$packageId;
							$sql2="SELECT `package_name`,`package_amount`,`isPaidPackage` FROM oes_packages WHERE package_id IN ($packageId)";
							$query = $this->db->query($sql2); 
							$pckName=$query->row();
							if($pckName){
								if($pckName->isPaidPackage == 0){
									$mypckTest->puchasedOne=1;
								}
							$mypckTest->package_name=$pckName->package_name;
							$mypckTest->package_amount=$pckName->package_amount;
                            }else{$mypckTest->package_name=0;}
							$mypckTest->uniqueId=$uniqueId;
							$myPackageTests[]=$mypckTest;
						  }
					    }
				}else{  
				         $expiryDate="";
				         $sql1="SELECT * FROM oes_tests WHERE testId IN ($testId)  and  isActive=1 and isDeleted=0 and isFree = 0";
						
						  $query = $this->db->query($sql1);  
					      $mypckTests=$query->result();
						  foreach($mypckTests as $mypckTest){
							  $mypckTest->main='1';
							  $mypckTest->puchasedOne=$puchasedOne;
							$mypckTest->expiryDate=$expiryDate;
							$mypckTest->packageId=$packageId;
							$sql2="SELECT `package_name`,`package_amount` FROM oes_packages WHERE package_id IN ($packageId)";
							
							$query = $this->db->query($sql2); 
							$pckName=$query->row();
							if($pckName){
							$mypckTest->package_name=$pckName->package_name;
							$mypckTest->package_amount=$pckName->package_amount;
						    }else{$mypckTest->package_name=0;}
							$mypckTest->uniqueId=$uniqueId;
							$myPackageTests[]=$mypckTest;}
							
				  }
				}
			 }
			return (count($myPackageTests) > 0)?$myPackageTests:'';
			//for practise tests
		}else{
			 $resellerId=($this->session->userdata('reseller_id'))?$this->session->userdata('reseller_id'):0;
			 $course=$this->session->userdata('course');//16-18
			 $isPromoterUser=$this->promoterDetails($resellerId);//16-18
			 $result =array();
			 /* $this->db->select('*'); 
			 $this->db->from(TBL_TESTS);
			 $this->db->where('testType',$testType);
             $this->db->where('isDeleted',0);
             $this->db->where('isFree',1);
			 $this->db->where('isActive',1);
			 $this->db->where_in('course_id',$course);
			 if($resellerId && $isPromoterUser){
			     $this->db->where_in('testId',$isPromoterUser['tests']);
		     }else if($resellerId && empty($isPromoterUser)){
				 $this->db->where('resellerId',$resellerId);
			 }else{
				 $this->db->where('resellerId',0);
			 }
			 $query = $this->db->get(); print_r($this->db->last_query());die(); */
			 $sql="SELECT * FROM `oes_tests` WHERE `testType` = ".$testType." AND `isDeleted` =0 AND `isFree` = 1 AND `isActive` = 1 AND `course_id` IN(".$course.")";
             if($isPromoterUser['tests']){
				 $sql.="AND `testId` IN(".$isPromoterUser['tests'].")";
			 }
			 else{
				 $sql.=" AND `resellerId` = ".$resellerId."";
			 }
			 $sql.=" order by testId DESC";
            $query=$this->db->query($sql);	
			  if($query){			
				$res=$query->result();
			  }else{
				  $res=array();
			  }
			 if($res){
			 foreach($res as $r){ 
			   $r->packageId=0;
			   $r->package_name=0;
			   $r->main='2';$r->uniqueId=0;
			   $r->expiryDate="";
			   $r->puchasedOne="";
			   $r->package_amount=0;
			   $result[]=$r;}
			 }
			 $newResults=$this->freeTests();
			 $completedresult=$result;$newarray=array();
			 $newResults=$this->freeTests();
			 if($newResults){
			 $k=count($result)-1; 
			 foreach($newResults as $new){
				 if($new->isFree == 1){
					 $newarray[]=$new;
				 }
				
			 $k++;}
			 $completedresult=array_merge($result,$newarray);
			 } 
			// echo"<pre>";print_r($completedresult);die();
			 return ($completedresult >0)?$completedresult:'';
		}
		 
		 
	}
	function freeTests(){
		 $mainarray=array();
       	     $testArray=array();
			 $myPurchasedTests=array();
             $myPackageTests=array();
			 $userId=$this->session->userdata('user_id');
			 $this->db->select('*'); 
			 $this->db->from(TBL_USER_PACKAGE);
			 $this->db->where('isDeleted',0);
			 $this->db->where('userId',$userId);
			 $this->db->where('isActive',1);
			 $this->db->order_by('id','desc');
			 $query = $this->db->get(); 
			 $results=$query->result();  
			 if($results){
                  
				  foreach($results as $result){
				          $testId=$result->packageTests;
				          $packageId=$result->packageId;
				          $uniqueId=$result->id;
						  if($result->amount == 0 && $result->txnid ==0){
							  $puchasedOne=0;
						  }else{
							  $puchasedOne=1;
						  }
						   $validSql="SELECT DATEDIFF(package_valid_to,package_valid_from) as diff FROM oes_packages WHERE package_id = $packageId";
				           $validityCheck=$this->db->query($validSql)->row();
						  
					if(@$validityCheck->diff){
				           $validityCheck=$validityCheck->diff;
					       $createdTime=$result->createdTime;
						   $expiryDate = date('Y-m-d h:i:s', strtotime("+$validityCheck days", strtotime($createdTime)));
						   $A=strtotime($expiryDate);$B=strtotime($createdTime);
						   if($A >= $B){  
						  $sql1="SELECT * FROM oes_tests WHERE testId IN (".$testId.")  and  isActive=1 and isDeleted=0";
						  $query = $this->db->query($sql1); 
					      $mypckTests=$query->result();
						  foreach($mypckTests as $mypckTest){
							$mypckTest->main='1';
							$mypckTest->puchasedOne=$puchasedOne;
							$mypckTest->expiryDate=$expiryDate;
							$mypckTest->packageId=$packageId;
							$sql2="SELECT `package_name`,`package_amount`,`isPaidPackage` FROM oes_packages WHERE package_id IN ($packageId)";
							$query = $this->db->query($sql2); 
							$pckName=$query->row();
							if($pckName){
								if($pckName->isPaidPackage == 0){
									$mypckTest->puchasedOne=1;
								}
							$mypckTest->package_name=$pckName->package_name;
							$mypckTest->package_amount=$pckName->package_amount;
                            }else{$mypckTest->package_name=0;}
							$mypckTest->uniqueId=$uniqueId;
							$myPackageTests[]=$mypckTest;
						  }
					    }
				}else{  
				         $expiryDate="";
				         $sql1="SELECT * FROM oes_tests WHERE testId IN (".$testId.")  and  isActive=1 and isDeleted=0";
						
						  $query = $this->db->query($sql1);  
					      $mypckTests=$query->result();
						  foreach($mypckTests as $mypckTest){
							  $mypckTest->main='1';
							  $mypckTest->puchasedOne=$puchasedOne;
							$mypckTest->expiryDate=$expiryDate;
							$mypckTest->packageId=$packageId;
							$sql2="SELECT `package_name`,`package_amount` FROM oes_packages WHERE package_id IN ($packageId)";
							
							$query = $this->db->query($sql2); 
							$pckName=$query->row();
							if($pckName){
							$mypckTest->package_name=$pckName->package_name;
							$mypckTest->package_amount=$pckName->package_amount;
						    }else{$mypckTest->package_name=0;}
							$mypckTest->uniqueId=$uniqueId;
							$myPackageTests[]=$mypckTest;}
							
				  }
				}
			 }
			 
			return (count($myPackageTests) > 0)?$myPackageTests:'';
	}

	function getOptedTests($testType,$limit,$offet=0)
	{
		     $userId=$this->session->userdata('user_id');
			 $this->db->select('*'); 
			 $this->db->from(TBL_TIMES);
			 $this->db->where('userId',$userId);
			 $this->db->where('isActive',1);
			  if($limit){
			 $this->db->limit($limit);
			 }
			if($testType==2){
				  $this->db->group_by('testId');
				  $this->db->group_by('userPackageID');
				  // $this->db->where('userPackageID',0);
			 }
			  if($testType==1){
				// $this->db->group_by('userPackageID');
				 $this->db->group_by('testId'); 
				 $this->db->where('userPackageID!=',0);
			 }
			
			 $this->db->where('testType',$testType);
			 $this->db->where('status',TEST_COMPLETED);
			 $this->db->order_by('id','desc');
			 $query = $this->db->get();
			 $optedTests=$query->result();
				 for($i=0;$i<count($optedTests);$i++){ 
					$id=$optedTests[$i]->testId;
					$testResults=$this->db->query("select * from ".TBL_TIMES."  where status= 3 AND testId=$id and userId=$userId and testType=$testType and isActive=1 ORDER BY id DESC limit 1")->row();
					$optedTests[$i]->testResults=$testResults;
					$this->db->select('*'); 
			        $this->db->from(TBL_TESTS);
				    $this->db->where('testId', $id);
				    $query = $this->db->get();
				    $tests=$query->row();
				    $optedTests[$i]->testDetails=$tests;
					$packageId= $optedTests[$i]->packageId;
					$packageDetails=$this->db->query("SELECT `package_name` FROM oes_packages WHERE package_id IN ($packageId)")->row();
					if($packageDetails){
					$optedTests[$i]->packageName=$packageDetails->package_name;
					}else{$optedTests[$i]->packageName='';}
					$statusCount=$this->db->query("select * from ".TBL_TIMES."  where status= 3 AND testId=$id and userId=$userId and testType=$testType and isActive=1")->result();
					if($statusCount){$optedTests[$i]->completedTestCount=count($statusCount);}else{$optedTests[$i]->completedTestCount=0;}
				    $optedTests[$i]->type=$testType;
				   
				 }
			
			return (count($optedTests) >0)?$optedTests:'';
			 
		
	}
	
   function getMainTests($testType)
	{ 
	    $resellerId=$this->session->userdata('reseller_id');
		 $course=$this->session->userdata('course');//16-18
		 $isPromoterUser=$this->promoterDetails($resellerId);//16-18
		
		 /* $resellerId=$this->session->userdata('reseller_id');
		 $course=$this->session->userdata('course');//16-18
		 $isPromoterUser=$this->promoterDetails($resellerId);//16-18
	 	 $this->db->select('*'); 
		 $this->db->from(TBL_TESTS);
		// $this->db->where('testType',$testType);
			if($course){
			  $this->db->where_in('course_id',$course);
			}
		 $this->db->where('isDeleted',0);
		 $this->db->where('isActive',1);
		 $this->db->where('isFree',0); 
		 if($resellerId && $isPromoterUser){ //16-18
			 $this->db->where_in('testId',$isPromoterUser['tests']);
		 }else if(empty($isPromoterUser) && $resellerId){
			 $this->db->where('resellerId',$resellerId);
		 }else{
			$this->db->where('resellerId',0);
		 }
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->result():''; */
		 if(empty($resellerId)){ $resellerId = 0;}
			 $sql="SELECT `testId`,`testName`,`testType`,`attempts` ,`course_id`,`testAmount` FROM oes_tests WHERE `course_id` IN (".$course.")  AND isDeleted = 0 AND isFree = 0 AND isActive = 1 ";
			 /* if($start){
				$sql.= ",".$start."";
			 } */
			 if($resellerId && $isPromoterUser){ //16-18
			    $tests=$isPromoterUser['tests'];
			    $sql.=" AND `testId` IN (".$tests.") ";
			 }else if(empty($isPromoterUser) && $resellerId){
				 $sql.="AND  `resellerId` = ".$resellerId."";
			 }else{
				 $sql.="AND  `resellerId` = 0";
			 } //print_r($sql);die();
			 $query=$this->db->query($sql); 
			 if($query){
				 $result=$query->result();
				 if($result){
					 foreach($result as $res){
						 if($isPromoterUser){
						  $amountsql="select p_t_sell_amount from oes_promoter_tests where p_t_promoter_id = ".$resellerId." and p_test_id = ".$res->testId."";
					      $amount=$this->db->query($amountsql)->row();
						  $res->testAmount=$amount->p_t_sell_amount;
						 }
						  $t[]=$res;
						 
					 }
					 return $t;
				 }
			 }else{
				 return array();
			 }
		 //return ($query->num_rows() >0)?$query->result():array();
	}
	
	function getTotalQueByTest($test_id)
	{
		 $this->db->select('*'); 
		 $this->db->from(TBL_QUETIONS);
		 $this->db->where('test_id',$test_id);
		 $this->db->where('isActive',1);
		 $this->db->where('isDeleted',0);
         $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->num_rows():'';
	}
	
   function getAttemptedQueByTest($test_id,$userId,$type,$attempts,$package,$unique)
	{
		 $this->db->select('*'); 
		 $this->db->from(TBL_EXAM_RESULTS);
		 $this->db->where('testId',$test_id);
		 $this->db->where('userId',$userId);
		 $this->db->where('testType',$type);
		 $this->db->where('attempts',$attempts);
		 $this->db->where('packageId',$package);
		  $this->db->where('userPackageID',$unique);
		 $this->db->where('isActive',1);
		 $where = '(option_id !=0 OR textOption != "")';
		 $this->db->where($where);
		 $query = $this->db->get(); 
         $result=$query->result();
		 if($result){return $result;}else{return '';}
	} 
  function getAttemptedQue($test_id, $userId, $type, $attempts, $package, $uniqueId)
	{
		 $this->db->select('*'); 
		 $this->db->from(TBL_EXAM_RESULTS);
		 $this->db->where('testId',$test_id);
		 $this->db->where('userId',$userId);
		 $this->db->where('testType',$type);
		 $this->db->where('attempts',$attempts);
		 $this->db->where('packageId',$package);
		 $this->db->where('userPackageID',$uniqueId);
		 //$this->db->where('option_id!=',0);
		 //$this->db->where('textOption!=',"");
		 $this->db->where('isActive',1);
		 $where = '(option_id !=0 OR textOption != "")';
		 $this->db->where($where);
		 
		 $query = $this->db->get(); 
		
         $result=$query->result();
		 if($result){return $result;}else{return '';}
	}
	
   function getCorrectAnsByTest($test_id,$queId)
	{$i=0;
         $this->db->select('option_id,option');  
		 $this->db->from(TBL_QUE_OPTIONS);
		 $this->db->where('test_id',$test_id);
		 $this->db->where('que_id',$queId);
		 $this->db->where('is_answer',1);
		 $query = $this->db->get(); 
		 $array=array();
		 $res=$query->result();
		 if($res){
		 foreach($res as $re){
		 $array[$i]=$re->option_id;
		 $i++;}
		 }
		 return ($query->num_rows() >0)?$array:array();

	}
   function getQuestionNumbers($examid,$isShuffle="")
   {
         $this->db->select('que_id'); 
		 $this->db->from(TBL_QUETIONS);
		 $this->db->where('isActive',1);
		 $this->db->where('isDeleted',0);
		 $this->db->where('test_id',$examid);
		 if($isShuffle == '1'){
         $this->db->order_by('rand()');   
		 }		 
         $query = $this->db->get(); 
		 $result=$query->result();
         if($result){return $result;}else{return '';}
    }

function getRandomElements($table,$where="",$select="",$opt="")
   {
         $this->db->select($select); 
		 $this->db->from($table);
		 $this->db->where($where);
		 if($opt){
			  $this->db->where('option_id!=',$opt);
		 }
         //$this->db->order_by('rand()');   
         $this->db->limit(1);   
		 $query = $this->db->get(); 
		 $result=$query->result();
         if($result){return $result;}else{return '';}
    }


	function getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,$uniqueId,$order="",$que='')
	{ 
		 $this->db->select('que_id,que,test_id,hint,queType'); 
		 $this->db->from(TBL_QUETIONS);
		 $this->db->where('test_id',$examid);
		 $this->db->where('isActive',1);
		 $this->db->where('isDeleted',0);
		 $this->db->where('que_id',$offset);
         $query = $this->db->get();
		 $questions=$query->row();
		 if($questions){
			     $questions->isShuffle="";
				 $que_id=$questions->que_id;
				 
				 $this->db->select('option_id,option,is_answer,textType'); 
		         $this->db->from(TBL_QUE_OPTIONS);
		         $this->db->where('test_id',$examid);
		         $this->db->where('que_id',$que_id);
		         $this->db->where('isActive',1);
		         $this->db->where('isDeleted',0);
				 $query = $this->db->get();
				 $options=$query->result();
				 $questions->images=$this->getQueImages($que_id);//for image urls
				 $questions->options=$options;//for normal options
				 $questions->optedOption=$this->getOptedOption($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts); //for opted options
				 $ReferenceOptions=$this->getReferenceOptions($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts); //for reference options
				//30
 				 $fiftyFityChancesOfTest=$this->getfiftyFityChancesOfTest($examid,$package,$testType,$uniqueId,$userId,$attempts); 
                 $questions->FiftyFity=$fiftyFityChancesOfTest;            
			   if($ReferenceOptions){
					$questions->options=$ReferenceOptions;
					$questions->isShuffle=1;
				}
				// $questions->optedOption=$optedOption;
				 $questions->questionNumber=1;
				 if($order){$questions->questionNumber=$order;}
                 
           }
		  // debug( $questions);
		if($questions){return $questions;}else{return '';}
		
	}
	function getQueImages($queId){
		$this->db->select('q_image,q_position');
		$this->db->from(TBL_QUE_IMAGES);
		$this->db->where('q_que_id',$queId);
		$this->db->where('q_is_active',1);
		$this->db->where('q_is_deleted',0);
		$query = $this->db->get();
		if($query){
			$images=$query->result();
        }else{
			$images=array();
		}
		return $images;
	}
	function getOptedOption($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts){
		         $this->db->select('option_id,queType,textOption');  //31
		         $this->db->from(TBL_EXAM_RESULTS);
		         $this->db->where('testId',$examid);
		         $this->db->where('que_id',$que_id);
		         $this->db->where('packageId',$package);
		         $this->db->where('testType',$testType);
		         $this->db->where('userPackageID',$uniqueId);
		         $this->db->where('userId',$userId);
		         $this->db->where('attempts',$attempts);
				 $this->db->where('isActive',1);
				 $query = $this->db->get();
				 $optedOption=$query->row();
				 return $optedOption;
	}
	function getReferenceOptions($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts){
		         $this->db->select('opt_id'); 
		         $this->db->from(TBL_OPTIONS_REFERENCE);
		         $this->db->where('test_id',$examid);
		         $this->db->where('que_id',$que_id);
		         $this->db->where('userPackage',$uniqueId);
		         $this->db->where('user_id',$userId);
		         $this->db->where('attempts',$attempts);
				 $this->db->where('isActive',1);
                 $query = $this->db->get();
				 $optedOption=$query->row();
				 @$opt_id=$optedOption->opt_id;
				 if($opt_id){
				 $sql="select * from oes_que_options where  option_id in ($opt_id) and test_id=$examid and que_id =$que_id and isActive=1 and isDeleted=0";
				 //debug($sql);
				 $res=$this->db->query($sql)->result();
				 return $res;
				 }else{
					 return "";
				 }
	}
	function getfiftyFityChancesOfTest($examid,$package,$testType,$uniqueId,$userId,$attempts){
		         $this->db->select('opt_id'); 
		         $this->db->from(TBL_OPTIONS_REFERENCE);
		         $this->db->where('test_id',$examid);
		       //  $this->db->where('que_id',$que_id);
		         $this->db->where('userPackage',$uniqueId);
		         $this->db->where('user_id',$userId);
		         $this->db->where('attempts',$attempts);
				 $this->db->where('isActive',1);
                 $query = $this->db->get();
				 $optedOption=$query->result();
				 if($optedOption){
					 return count($optedOption);
				 }
				 else{
					 return 0;
				 }
	}

	function getNumberOfAttempts($table,$where)
	{
		         $this->db->select('*'); 
		         $this->db->from($table);
		         $this->db->where($where);
				 $this->db->order_by('id','desc');
                 $query = $this->db->get();
				 $attempts=$query->row();
				 if($attempts){return $attempts;}else{return '';}
				 
	}
	function getReferenceQuestions($test_id,$user_id,$uniqueId,$attempts)
	{            
	             $testIdsArray=$this->db->query("select que_id from oes_quetions where test_id=$test_id And isActive=1 And isDeleted=0")->result();
				 if($testIdsArray){
					 foreach($testIdsArray as $questions){$mainarray[]=$questions->que_id;}
					  	  
					  $testIdsArray = array_values($mainarray);
                      }
					
				 $queArray='';$Qarray=array();
		         $this->db->select('que_id'); 
		         $this->db->from(TBL_QUESTIONS_ORDER);
		         $this->db->where('user_id',$user_id);
		         $this->db->where('test_id',$test_id);
		         $this->db->where('userPackage',$uniqueId);
		         $this->db->where('attempts',$attempts);
		         $this->db->where('isActive',1);
				 $this->db->order_by('id','desc');
				 $this->db->limit(1);
                 $query = $this->db->get();
				 $querystring=$query->row();
				 if($querystring){$querystring=$querystring->que_id;$queArray=explode(',',$querystring);
				  if($queArray){
					   foreach($queArray as $q){
						   if(in_array($q,$testIdsArray)){
							   $Qarray[]=$q;
						   }
						   
					   }
					 }
					 if(count($Qarray) < count($testIdsArray)){
					
						 foreach($testIdsArray as $qu){
							 
							  if(!in_array($qu,$Qarray)){
								  
							     array_push($Qarray,$qu);
						       }
					     }
					 }
	}
				 if($Qarray){ return $Qarray;}else{ return '';}
				 
	}
	function DeactiveTestReports($ids,$courseID,$uiD)
      {
		 $this->db->where('testType',1);
		 $this->db->where('userId',$uiD);
		 $this->db->where('isActive',1);
		 $this->db->where_in('testId',$ids);
		 $this->db->where_in('courseId',$courseID);
		 $data['isActive']=0;
		 $query = $this->db->update(TBL_EXAM_RESULTS,$data); 
		 if($query){
			 $query1=$this->DeactiveRecords($ids,$courseID,$uiD);
			 }
		return $query;
    } 
	function DeactiveRecords($ids,$courseID,$uiD){
		 $this->db->where('testType',1);
		 $this->db->where('userId',$uiD);
		 $this->db->where('isActive',1);
		 $this->db->where_in('testId',$ids);
		 $this->db->where_in('courseId',$courseID);
		 $data['isActive']=0;
		 $query=$this->db->update(TBL_TIMES,$data);
		 return $query;
	}
	public function saveRecoveryEmailData($userid,$token,$purpose)
   {
	    //we create a new date that is 3 days in the future
	    $expireDateFormat = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")+3, date("Y"));
	    $expirationDate = date("Y-m-d H:i:s",$expireDateFormat);
	    $data = array(
			'userID'=>$userid,
			'purpose'=>$purpose,
			'token'=>$token,
			'isactive'=>1,
			'expirationDate'=>$expirationDate
	     );
	    $query = $this->db->insert(TBL_OTPS,$data);
	      if (!$query)
	      {
		    $this->throwException($query);
	      }
	     else
	     { 
		    return $this->db->insert_id();
	     }
    }
	//21 march
function getTotalCartItemsCount($userId){
	             $this->db->select_sum('quantity'); 
		         $this->db->from(TBL_CART);
		         $this->db->where('isActive',1);
		         $this->db->where('userId',$userId);
				 $query = $this->db->get();
				 $result=$query->row();
				 if($result){
					return $result->quantity;
				 }else{return '';}
}
function getTotalCartItemsAmount($userId){
	             $this->db->select_sum('amount'); 
		         $this->db->from(TBL_CART);
		         $this->db->where('isActive',1);
		         $this->db->where('userId',$userId);
				 $query = $this->db->get();
				 $result=$query->row();
				 if($result){
					return $result->amount;
				 }else{return '';}
}
function getPackageItens(){
                 $this->db->select('*'); 
		         $this->db->from(TBL_CART);
		         $this->db->where('isActive',1);
		         $this->db->where('userId',$userId);
		         $this->db->where('itemType',1);
				 $query = $this->db->get();
				 $result=$query->row();
				 if($result){
					return $result;
				 }else{return '';}
}

function getSelectedTestsAmount($ids)
{
            //$sql = "SELECT SUM(testAmount) as total FROM oes_tests WHERE testId IN ($ids) and testType= 1 and isActive=1 and isDeleted=0 ";
            $sql = "SELECT SUM(testAmount) as total FROM oes_tests WHERE testId IN ($ids)  and isActive=1 and isDeleted=0 ";
			$query = $this->db->query($sql);
			$testArray = $query->row();
			return $testArray;
}
function getSelectedPackagesAmount($ids)
{
            $sql = "SELECT SUM(package_amount) as total FROM oes_packages WHERE package_id IN ($ids)  and isActive=1 and isDeleted=0";
			$query = $this->db->query($sql);
			$testArray = $query->row();
			return $testArray;
}
function getSelectedPackagesDetails($ids)
{
            $sql = "SELECT * FROM oes_packages WHERE package_id IN ($ids)  and isActive=1 and isDeleted=0";
			$query = $this->db->query($sql);
			$testArray = $query->result();
			return $testArray;
}
function getSelectedTestDetails($ids)
{
            
            $sql = "SELECT * FROM oes_tests WHERE testId IN ($ids) and testType= 1 and isActive=1 and isDeleted=0 ";
			$query = $this->db->query($sql);
			$testArray = $query->result();
			return $testArray;
}
function getTestsNameByPackage($packageId)
	{    
		 $this->db->select(TBL_PACKAGES.'.package_test');
         $this->db->from(TBL_PACKAGES);
         $this->db->where(TBL_PACKAGES.'.package_id',$packageId);
		 $query = $this->db->get(); 
		 $res = $query->row();
		 if($res){
			 $testId=$res->package_test;
			 $sql = "SELECT * FROM oes_tests WHERE testId IN ($testId)  and isActive=1 and isDeleted=0";
			 $query = $this->db->query($sql);
			 $testArray = $query->result();
			 return $testArray;
		}
		
		
	}
	/*function getPackgesByPackageName($txt)
	{
		$this->db->select('*');
		$this->db->from(TBL_PACKAGES);
		$this->db->like('package_name', $txt);
		$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$query = $this->db->get();
		$results = $query->result();
	     return $results;
		
	}
	function getTestsBytestName($txt)
	{
		$this->db->select('*');
		$this->db->from(TBL_TESTS);
		$this->db->like('testName', $txt);
		$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$query = $this->db->get();
		$results = $query->result();
	     return $results;
	}*/
	function getPackgesByPackageName($txt,$resellerid)
	{
		$this->db->select('*');
		$this->db->from(TBL_PACKAGES);
		if($txt){
		$this->db->like('package_name', $txt);
		}
		$this->db->where('resellerId', $resellerid);
		$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$query = $this->db->get();
		$results = $query->result();
	     return $results;
		
	}
	function getTestsBytestName($txt,$resellerid)
	{
		$this->db->select('*');
		$this->db->from(TBL_TESTS);
		if($txt){
		$this->db->like('testName', $txt);
		}
		$this->db->where('resellerId', $resellerid);
		$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$this->db->where('testType', 1);
		$query = $this->db->get();
		$results = $query->result();
	     return $results;
	}
	function getLatestStatus($id, $uId, $uniqueId)
	{
		$this->db->select('*');
		$this->db->from(TBL_TIMES);
		$this->db->where('isActive', 1);
		//$this->db->where('status', 2);
		$this->db->where('testId', $id);
		$this->db->where('userId', $uId);
		$this->db->where('userPackageID', $uniqueId);
		$this->db->order_by('attempts', 'desc');
		//$this->db->group_by('testId');
		$query = $this->db->get();
		$results = $query->row();
		return $results;
	}
	function getLastAtemptedQue($id, $uId, $uniqueId, $attempts)
	{
		$this->db->select('que_id');
		$this->db->from(TBL_EXAM_RESULTS);
		$this->db->where('isActive', 1);
		$this->db->where('testId', $id);
		$this->db->where('userId', $uId);
		$this->db->where('userPackageID', $uniqueId);
		$this->db->where('attempts', $attempts);
		$this->db->order_by('attempts', 'desc');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		$results = $query->row();
		if($results){
	     return $results->que_id;
		}else{return 0;}
	}
	function getDifference($uniqueId,$attempts,$testID){
		 $sql = "SELECT TIMEDIFF(endTime,startTime) as time FROM oes_time WHERE testId=$testID  and userPackageID=$uniqueId and attempts=$attempts";
		 $query = $this->db->query($sql);
		 $result=$query->row();
		 $tt=(string)$result->time; 
		 $sql1 = "SELECT TIME_TO_SEC('$tt') as sec";  
		 $query = $this->db->query($sql1);
		 $rr=$query->row(); 
		
		 return $rr;
	}
	function getUserAnswers($testId,$queId,$option_id,$queType=""){
		
		//31
		if($queType == 2 || $queType == 3){
			$sql = "SELECT `option`,`textType`,`option_id` FROM oes_que_options WHERE  isActive=1 and isDeleted=0 and que_id=$queId and test_id=$testId";
		}else{
		$sql = "SELECT `option`,`textType`,`option_id` FROM oes_que_options WHERE option_id IN ($option_id)  and isActive=1 and isDeleted=0 and que_id=$queId and test_id=$testId";
		}
		$query = $this->db->query($sql);
		
		$result=$query->result();
		return $result;
	}
	function getUserCouponsDetails($uId,$couponId){
		$sql = "SELECT * FROM `oes_user_coupon_details` WHERE userId=$uId  and couponId=$couponId order by id DESC limit 1 ";
		$query = $this->db->query($sql);
		$result=$query->row();
		return $result;
	}
	function getRecord($table,$where){ 
		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		$results = $query->row();
		
		if($results){
			return $results->textOption;
			
		}else{
			return "";
		}
	}
	function getMyAnswers($userId,$testId,$userPackageId,$type,$queId,$attempts){
		 $this->db->select('*');
	     $this->db->from(TBL_EXAM_RESULTS);
	     $this->db->where('userId',$userId);
	     $this->db->where('testId',$testId);
	     $this->db->where('userPackageID',$userPackageId);
	     $this->db->where('testType',$type);
	     $this->db->where('que_id',$queId);
	     $this->db->where('attempts',$attempts);
	     $query = $this->db->get();
		 $result=$query->result(); 
		foreach($result as $res){
			if($res->queType ==0 || $res->queType ==1){
				$res->option="";$res->textType="";
				$option_id=$res->option_id;
				$query="select `textType`,`option`,`option_id` from oes_que_options where test_id=$testId and que_id=$queId and option_id in ($option_id)";
			    $re= $this->db->query($query); 
				if($re){
				$res->normalOptions=$re->result();
				}
			   
			}else if($res->queType ==2 || $res->queType ==3){
				$option_id=$res->option_id;
				$res->textType=1;
				if(empty($option_id)){
					$res->option=$res->textOption;
				}
				
			}
		
		}
		 return $result;
	}
	function getLibraries($id){
		$isPromoterUser=$this->session->userdata('isPromoterUser');
		
         if($this->session->userdata('reseller_id')){
			$resellerId=$this->session->userdata('reseller_id');
		    }else if($isPromoterUser == 1){
			$resellerId=0;
		    }
			 if($isPromoterUser==1){
				 $resellerId=0;
			 }
		$query='SELECT * FROM `oes_libraries` WHERE 
`packageId` like "%,'.$id.',%" OR `packageId` like "'.$id.',%" OR  `packageId`  like "%,'.$id.'" OR `packageId`  like "%'.$id.'%" and isActive=1 and isDeleted=0 ';
	$results=$this->db->query($query)->result();
 
	    if($results){
			return $results;
			
		}else{
			return "";
		}
	} 
	function getUserPacks($table,$where){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$this->db->group_by('packageId');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		$results = $query->result();
		
		if($results){
			return $results;
			
		}else{
			return "";
		}
	}
/* for promotion tests */
function getPromotionTests(){
		     $resellerId=$this->session->userdata('reseller_id');
			 $result ='';
			 $this->db->select('*'); 
			 $this->db->from(TBL_TESTS);
			// $this->db->where('testType',$testType);
			 $this->db->where('isPromotionalTest',1);
             $this->db->where('isDeleted',0);
			 $this->db->where('isActive',1);
			 if($resellerId){
			 $this->db->where('resellerId',$resellerId);
		     }else{ $this->db->where('resellerId',0);}
			 $query = $this->db->get();
			  
			 $res=$query->result();
			 if($res){
			 foreach($res as $r){ 
			   $r->packageId=0;
			   $r->package_name=0;
			   $r->main='2';$r->uniqueId=0;
			   $r->expiryDate="";
			   $r->puchasedOne="";
			   $r->package_amount=0;
			   $result[]=$r;}
			 } 
			 $completedresult=$result;$newarray=array();
			 $newResults=$this->freeTests(); 
			 if($newResults){ 
			 $k=count($result)-1; 
			 foreach($newResults as $new){
				 if($new->isFree == 1){
					 $newarray[]=$new;
				 }
				
			 $k++;} 
			
			 $completedresult=array_merge($result,$newarray);
			 } 
			 return ($completedresult >0)?$completedresult:'';
	}
function checkCouponValidity($couponCode){
		$result=array();
		$sql="SELECT * FROM `oes_coupons` where BINARY `coupon_code` = '".$couponCode."' AND isActive = 1 AND isDeleted = 0"; //print_r($sql);die();
		$query=$this->db->query($sql);
		if($query){
			$result=$query->row();
		}else{
			return array();
		}
		return $result;
	}
	function getUserPayments($userId){
		$this->db->select('txnid');$transactions=$Result=array();
        $this->db->distinct();		
		$this->db->From('oes_user_packages');
		$this->db->where('isActive',1);
		$this->db->where('userId',$userId);
		$this->db->where('txnid!=',"");
		$this->db->where('isDeleted',0);
		$this->db->order_by('id','desc');
		//$this->db->limit($offset,$start); 
        $query=$this->db->get();
		$transactions=$query->result();
		if($transactions){
			foreach($transactions as $transaction){
				$txnid=$transaction->txnid;
				$Result[]=$this->transactionDetails($txnid);
			}
		}
		return $Result;
	}
	function transactionDetails($txnid){
		$RESULT=array();
		$packIds=$testIds="";
		$packages=$tests=array();
		$this->db->select('*');
        $this->db->distinct();		
		$this->db->From('oes_user_packages');
		$this->db->where('isActive',1);
		$this->db->where('isDeleted',0);
		$this->db->where('txnid',$txnid);
		$query=$this->db->get();
		$transactions=$query->result();
		if($transactions){
		$i=0;
			foreach($transactions as $trans){ 
				if($trans->packageId){
					$packIds.=$trans->packageId.',';
				}
				if(empty($trans->packageId) && $trans->packageTests){
					$testIds.=$trans->packageTests.',';
				}
				$RESULT['paidAmount']=$trans->paidAmount;
				$RESULT['ccavenueOrderId']=($trans->ccavenueOrderId)?$trans->ccavenueOrderId:0;
				$RESULT['couponCode']=$trans->couponCode;
				$RESULT['couponAmount']=$trans->couponAmount;
				$RESULT['txnid']=$trans->txnid;
				$RESULT['paymentDate']=$trans->createdTime;
				if($trans->ccavenueOrderId){
					$paymentType="CCavenue";
				}else{
					$paymentType="Used Coupon";
				}
				$RESULT['paymentType']=$paymentType;
				$i++;
			}
			$packId=rtrim($packIds,',');
			$testId=rtrim($testIds,',');
			if($testId){
				$sql="SELECT  testId,testName FROM oes_tests WHERE testId IN (".$testId.")  and  isActive=1 and isDeleted=0";
				$query = $this->db->query($sql);
				 if($query){
					 $tests=$query->result();
				 }
				//$tests=$this->getTestsByTestIds($testId);
			}
			if($packId){
				$sql = "SELECT package_id,package_name from oes_packages WHERE package_id IN (".$packId.")";
			     $query = $this->db->query($sql);
				 if($query){
					 $packages=$query->result();
				 }
			 }
			 $RESULT['tests']=$tests;
			 $RESULT['packages']=$packages;
			 
		}
		return $RESULT;
		
	}
	

}








