<?php

Class Usermodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_USERS;
    protected $_order_by = 'userId desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
		$this->CI->load->helper('common_helper');
    }
    
	  function login($Email_or_Phone,$password)
    {
      
        $this->db->select('*');
		//$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$this->db->where('roleID', USER_ROLE_ID);
		$this->db->where("(emailAddress = '$Email_or_Phone' OR phoneNumber = '$Email_or_Phone')");
	    $this->db->where('isPromotionMember', 0);

		//$this->db->where('password', md5($password)); /on 4 th july
		$this->db->where('resellerId', 0); 
		$this->db->order_by('userId', 'Desc'); 
		
		$this->db->limit(1);
        $query = $this->db->get(TBL_USERS);
		if (!$query)
		{
		     $this->throwException($query);
		}else{
		    $pwd="";
			$user = $query->row();
			if($user){
			$pwd=decrypt($user->password); //debug($pwd);
			}
			if ($user && $pwd == $password){
				$user->isPromoterUser=0;
			    if($user->resellerId){
				$isPromoter=$this->checkResellerIsPromoter($user->resellerId);
				if($isPromoter){
					$user->isPromoterUser=1;
				}else{
					$user->isPromoterUser=0;
				}
			} 
				return $user;
			}else{
				return false;
			}
	   }
    }
	function checkResellerIsPromoter($id){
		     $sql="SELECT * FROM oes_resellers WHERE reseller_id= ".$id."  AND isDeleted=0 AND isActive=1 AND isReseller = 2";
		     $query = $this->db->query($sql);
			if(!$query)
			{
				$this->throwException($query);
			}else{
				$row = $query->row();
				if($row){
						 return $row;
						}else{
							 return FALSE;
						}
					}
	}
	public function user_exists($phone_number,$email_address)
	{
       // $query = $this->db->query("SELECT userId,phoneNumber,emailAddress FROM oes_users WHERE (phoneNumber='".$phoneNumber."' OR emailAddress='".$email_address."') AND isDeleted=0 AND is_user_verified=1");
        $query = $this->db->query("SELECT userId,phoneNumber,emailAddress,isPromotionMember FROM oes_users WHERE (phoneNumber='".$phone_number."' OR emailAddress='".$email_address."') AND isDeleted=0 AND isActive=1 and resellerId=0 and roleID = 2");
		if (!$query)
		{
          $this->throwException($query);
		}else{
			$row = $query->row();
			if($row){
             return $row;
			}else{
				 return FALSE;
			}
		}
        
	}
	public function email_exists($email_address)
	{
      $query = $this->db->query("SELECT userId,phoneNumber,emailAddress FROM oes_users WHERE  emailAddress='".$email_address."' and resellerId=0 and isPromotionMember=0");
		if (!$query)
		{
          $this->throwException($query);
		}else{
			$row = $query->row();
			if($row){
             return $row;
			}else{
				 return FALSE;
			}
		}
        
	}
	function register($data)
	{
		$result=$this->db->insert(TBL_USERS, $data);
		return $result;
	}
      
   function getallUsers()
    {
		 $this->db->select('userId,userName,emailAddress,phoneNumber,isActive,roleID,profilePicture,updatedTime'); 
		 $this->db->from(TBL_USERS);
		 $this->db->where('isDeleted',0);
		 $this->db->where('roleID',USER_ROLE_ID);
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->result():'';
    }   
	function profile($userId){
		$this->db->select('*');
		$this->db->from(TBL_USERS);
		$this->db->where('userId',$userId);
		$this->db->where('isDeleted',0);
		$this->db->where('roleID',USER_ROLE_ID);
		$this->db->limit(1);
        $query = $this->db->get();
		$result=$query->row();
		if($result){return $result;}else{return 0;}
	}
	function getCities()
	{
		$this->db->select('*');
		$this->db->from(TBL_CITIES);
		$this->db->where('isDeleted',0);
		$this->db->where('isActive',1);
		$query = $this->db->get();
		$result=$query->result();
		if($result){return $result;}else{return 0;}

	}
	function getStates()
	{
		$this->db->select('*');
		$this->db->from(TBL_STATES);
		$this->db->where('isDeleted',0);
		$this->db->where('isActive',1);
		$query = $this->db->get();
		$result=$query->result();
		if($result){return $result;}else{return 0;}

	}
	function getCourses()
	{
		$this->db->select('*');
		$this->db->from(TBL_COURSES);
		$this->db->where('isDeleted',0);
		$this->db->where('isActive',1);
		$query = $this->db->get();
		$result=$query->result();
		if($result){return $result;}else{return 0;}

	}
	function updateUser($data,$id)
	{
		$this->db->where('userId',$id);
		$query=$this->db->update(TBL_USERS,$data);
		if (!$query)
			{    
		       return false;
			}else{
				return true;
			}
	}
	
	
	public function isValidToken($token)
    {
    	
    	
    	$sql="SELECT * FROM oes_users WHERE `token` =  '".$token."' AND is_user_verified = 0";
    	$query =$this->db->query($sql);
    	return $query->row();
    	
    }
    public function UpdateUserVerified($token)
    {
    	 
    	 
    	$sql="update oes_users set is_user_verified=1 where token='".$token."'";
    	$query =$this->db->query($sql);
    	if (!$query)
    	{
    		$this->throwException($query);
    	}
    	else
    	{
    		if($query)
    		{    
		        $this->db->select('userId,userName,phoneNumber,emailAddress,testIds,packageIds');
		        $this->db->where('token',$token);
		        $this->db->where('is_user_verified',1);
				$query=$this->db->get(TBL_USERS);
				return $query->row();
				
    		}
    		else {
    			return false;
    		}
    	}
    }
	public function recoverUserPassword($userEmail,$uid="")
	{	
	    if($uid){
		     $query = $this->db->get_where(TBL_USERS, array('userId' => $uid,'emailAddress'=>$userEmail,'isActive'=>1,'isDeleted'=>0));
	     }else{
		     $query = "select * from oes_users where emailAddress='".$userEmail."' and resellerId=0 and isActive=1 and isDeleted=0 ";
			  $query=$this->db->query($query);
			 // $query->result();
			 
		}
       
		//$result=$this->db->query($query);
		//print_r();die();
		if (!$query)
		{
			 $this->throwException($query);
		}
		else
		{
			
			if($query)
			{
					$encrypt = rand(10000,99999);
					$message ='Hi, <br/> <br/>Click here to reset your password '.RECOVERPASSWORD.'/'.$encrypt.'   <br/> <br/>--<br>OnyxEducationals.com<br>';
					$smtpEmailSettings = $this->config->item('smtpEmailSettings');
					
					$isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$query->row()->emailAddress,'Forgot Password',$message);
		
					if($isEmailSent == true)
					{
						$this->saveRecoveryEmailData($query->row()->userId,$encrypt);
					}
					return 1;			
			}
			return 0;
		} 
        
	}
	/*public function recoverUserPassword($userEmail)
	{		
        $query = $this->db->get_where(TBL_USERS, array('emailAddress' => $userEmail));
		//$result=$this->db->query($query);
		//print_r();die();
		if (!$query)
		{
			 $this->throwException($query);
		}
		else
		{
			
			if($query)
			{
					$encrypt = rand(10000,99999);
					$message ='Hi, <br/> <br/>Click here to reset your password '.RECOVERPASSWORD.'/'.$encrypt.'   <br/> <br/>--<br>OnyxEducationals.com<br>';
					$smtpEmailSettings = $this->config->item('smtpEmailSettings');
					
					$isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$query->row()->emailAddress,'Forgot Password',$message);
					
					if($isEmailSent == true)
					{
						$this->saveRecoveryEmailData($query->row()->userId,$encrypt);
					}
					return 1;			
			}
			return 0;
		} 
        
	}*/
	public function saveRecoveryEmailData($userID,$token)
	{	
		//we create a new date that is 3 days in the future
		$expireDateFormat = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")+3, date("Y"));
        $expirationDate = date("Y-m-d H:i:s",$expireDateFormat);
		$data = array(	         
			  'userID'=>$userID,
			  'token'=>$token,
			  'isactive'=>1,
			  'expirationDate'=>$expirationDate
	        );
		$query = $this->db->insert(RECOVERY_EMAILS,$data); 
		if (!$query)
		{
			 $this->throwException($query);
		}
		else
		{			
			return $this->db->insert_id();		
		}
	}
	function getAmount($pack,$tests=""){
		      $pamount =""; $tamount="";
				 if($pack){
					$res=$this->db->query("SELECT sum(package_amount) as packageAmount FROM oes_packages WHERE package_id IN ($pack)")->row();
				    $pamount=$res->packageAmount;
				 }
				 if($tests)
				 {
					$res=$this->db->query("SELECT sum(testAmount) as testAmount FROM oes_tests WHERE testId IN ($tests)")->row();
				    $tamount=$res->testAmount; 
					
				 }
			   $amount['pamount']=$pamount;
			   $amount['tamount']=$tamount;
		       return $amount;
	}
	function checkResellerUsersExists($name){
		$this->db->select('*');
		$this->db->from(TBL_USERS);
		$this->db->where('resellerId!=',0);
		$this->db->where('userName',$name);
		$this->db->order_by('userId', 'desc'); 
		$this->db->limit(1);
		$query=$this->db->get();
		$res=$query->row();
		return $res;
	}
	function resellerlogin($Email_or_Phone,$password,$resellerId){
		$this->db->select('*');
		//$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$this->db->where('roleID', USER_ROLE_ID);
		$this->db->where("(emailAddress = '$Email_or_Phone' OR phoneNumber = '$Email_or_Phone')");
		$this->db->where('resellerId', $resellerId); 
		$this->db->order_by('userId', 'Desc'); 
		//$this->db->where('password', md5($password)); 
		//$this->db->where('is_user_verified', 1); 
		//$this->db->order_by('userId', 'desc'); 
		
		$this->db->limit(1);
        $query = $this->db->get(TBL_USERS);
		if (!$query)
		{
		     $this->throwException($query);
		}else{
		
			$pwd="";
			$user = $query->row();
			if($user){
			$pwd=decrypt($user->password);
			
			}
			if ($user && $pwd == $password){
				$user->isPromoterUser=0;
			    if($user->resellerId){
				$isPromoter=$this->checkResellerIsPromoter($user->resellerId);
				if($isPromoter){
					$user->isPromoterUser=1;
				}else{
					$user->isPromoterUser=0;
				}
			} 
				return $user;
			}else{
				return false;
			}
	   }
	}
	/*FOR PROMOTION LOGIN*/
	function promotionMemberlogin($Email_or_Phone,$mobile,$name)
    {
        $this->db->select('*');
		$this->db->where('isDeleted', 0);
		//$this->db->where('isPromotionMember', 1);
		$this->db->where("(emailAddress = '$Email_or_Phone' AND phoneNumber = '$mobile' )");
		$this->db->order_by('userId', 'Desc'); 
		$this->db->limit(1);
        $query = $this->db->get(TBL_USERS);
		if (!$query)
		{
		     $this->throwException($query);
		}else{
			$user = $query->row();
		    return $user;
		}
			
    }
	function checkUserLogedInOtherDevice($userId){
		
		$this->db->select('*');
		$this->db->where('isActive', 1);
		$this->db->where('userID', $userId);
		$this->db->where('isDeleted', 0);
		$this->db->order_by('id', 'Desc'); 
		$this->db->limit(1);
        $query = $this->db->get(TBL_USER_TRACK);
		if($query){
			return $query->row();
		}else{
			return "";
		}
	}
	function courseDetails($course){
		   if($course){
				$sql="select courseId,courseName from oes_courses where courseId in (".$course.")";
				$query=$this->db->query($sql);
				if($query){
					$courseDetails=$query->result();
				}else{
					$courseDetails=array();
				}
				return $courseDetails;
			}else{
				return array();
			}
	}
	
	
}








