<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Promotion extends Oescontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Usermodel'); 
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		$this->load->helper('common_helper');
	}
   function index(){
		  $data=$input=array();
         try{
			if(isset($_POST['submit'])){ 
				
				if (!empty($_POST)) 
				{
					$this->load->library('form_validation');
					$this->form_validation->set_rules('email','email','trim|required|valid_email');
					$this->form_validation->set_rules('name','name','trim|required|min_length[3]|max_length[15]');
					$this->form_validation->set_rules('mobile','Phone Number','trim|required|min_length[10]|max_length[10]');
                    $data['input']=$_POST;
						if($this->form_validation->run()!=false)
						{
							$email = trim($this->input->post('email'));
							$name = trim($this->input->post('name'));
							$phoneNumber = trim($this->input->post('mobile'));
							$input=array('userName'=>$name,'emailAddress'=>$email,'PhoneNumber'=>$phoneNumber);
							$checkMemberExists=$this->Usermodel->promotionMemberlogin($email,$phoneNumber,$name);
							if($checkMemberExists){
								if($checkMemberExists->isActive == 0){
									 $data['errorMessage'] ='Please contact admin to activate your account.';
								}else if($checkMemberExists->isPromotionMember == 1){
									 $trackData['userId']=$checkMemberExists->userId;
									 $trackData['ipAddresss']=$this->input->ip_address();
									 $trackData['createdTime']=date("Y-m-d H:i:s");
									 $this->insertOrUpdate(TBL_USER_TRACK,$where=array(),$trackData);
									 $where=array('userId'=>$checkMemberExists->userId);
			                        $data['profileDetails']= $this->getSingleRecord(TBL_USERS,$where,$select="*");
									$this->session->set_userdata('name',$data['profileDetails']->userName);
									$this->session->set_userdata('email',$data['profileDetails']->emailAddress);
									$this->session->set_userdata('user_id',$data['profileDetails']->userId);
									$this->session->set_userdata('profilePicture',$data['profileDetails']->profilePicture);
									$this->session->set_userdata('phoneNumber',$data['profileDetails']->phoneNumber);
									 $this->session->set_userdata('isPromotionMember',1);//30
									 $this->session->set_userdata('reseller_id',$data['profileDetails']->resellerId);//30
									 $data['successMessage'] = LOGIN_SUCCESS; 
							         redirect(PROMOTER_DASHBOARD_URL.'?name='.$checkMemberExists->userName);
								}else if($checkMemberExists->isPromotionMember == 0){
									$data['errorMessage']="You are unable to login as promotion member.Please login as a enrolled user.";
								}
								
								
							}else{
								$input['isPromotionMember']=1;
								$where=array();
								$userId=$this->insertOrUpdate(TBL_USERS,$where,$input);
								$where=array('userId'=>$userId);
								    $data['profileDetails']= $this->getSingleRecord(TBL_USERS,$where,$select="*");
									$this->session->set_userdata('name',$data['profileDetails']->userName);
									$this->session->set_userdata('email',$data['profileDetails']->emailAddress);
									$this->session->set_userdata('user_id',$data['profileDetails']->userId);
									$this->session->set_userdata('profilePicture',$data['profileDetails']->profilePicture);
									 $this->session->set_userdata('phoneNumber',$data['profileDetails']->phoneNumber);
									 $this->session->set_userdata('isPromotionMember',1);//30
									 $this->session->set_userdata('reseller_id',$data['profileDetails']->resellerId);//30
									 $data['successMessage'] = LOGIN_SUCCESS; 
							         redirect(PROMOTER_DASHBOARD_URL.'?name='.$data['profileDetails']->userName);
							}
							
						}
						else {
							
							$data['errorMessage'] = validation_errors();
						}
				}
			}
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		    
		    $this->load->view('promotions/promotionMember',$data);
      }
	  
	  function dashborad(){
		  try{
			$data = array();
			$limit = DEFAULT_LIMIT;
		    $userId=$this->session->userdata('user_id'); 
			if(!$userId){
				redirect(PROMOTION_MEMBER_LOGIN_URL,'refresh');
			}
			$urlNmae=@$_GET['name'];
			if(@$urlNmae){
				if($urlNmae!=$this->session->userdata('name')){
					redirect(NOT_FOUND_URL);
				 }
			}
			$data['tests']= $this->Testsmodel->getPromotionTests(PRACTICAL_TEST,$limit); 
			$this->load->view('promotions/promotionHeader',$data);
			$this->load->view('promotions/dashboard',$data);
			$extrafooter = $this->load->view('dashboard_script');
			$this->load->view('footer',$extrafooter);
			
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	  }
	  
	 function logout(){
			$this->session->unset_userdata('email');
			$this->session->unset_userdata('phoneNumber');
			$this->session->unset_userdata('profilePicture');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('testType');
			$this->session->unset_userdata('examId');
			$this->session->unset_userdata('packageiD');
			$this->session->unset_userdata('uniqueId');
			redirect(PROMOTION_MEMBER_LOGIN_URL);	
            
        }
		function getusers(){
			$sql="select * from oes_users";
			$result=$this->db->query($sql)->result();
			echo"<pre>";print_r($result);die();
		}
		
}
?>