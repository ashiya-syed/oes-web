<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Dashboard extends Oescontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		date_default_timezone_set('Asia/Kolkata');
            $loginToken=$this->session->userdata('loginToken'); 
			$notvalidToken=$this->checkTokenValid($loginToken);
			if($notvalidToken){
				redirect(LOGOUT_URL,'refresh');
			}
	}
	/*******************
	********************
	This method is useful to 
	call dashborad with all details
	********************
	********************/
	
	function index()
	{
		
		try{
			$data = array();
			$limit = DEFAULT_LIMIT;
		    $userId=$this->session->userdata('user_id'); 
		    $loginToken=$this->session->userdata('loginToken'); 
			$notvalidToken=$this->checkTokenValid($loginToken);
			if(!$userId || $notvalidToken){
				redirect(LOGOUT_URL,'refresh');
			}
			$urlNmae=@$_GET['name'];
			if(@$urlNmae){
			if($urlNmae!=$this->session->userdata('name')){
				redirect(NOT_FOUND_URL);
			}
			}
			
			$where=array('userId'=>$userId);			
			$records=$this->getAllRecords(TBL_USER_PACKAGE,$where,$select="*");
			$data['practicalTests']= $this->Testsmodel->getTests(PRACTICAL_TEST,$limit);
			if($data['practicalTests']){
			for($i=0;$i<count($data['practicalTests']);$i++)
			{
				$data['practicalTests'][$i]->redo=0;
				$testId=$data['practicalTests'][$i]->testId;
				$packageId=$data['practicalTests'][$i]->packageId;
				$uniqueId=$data['practicalTests'][$i]->uniqueId;
				//$where=array('testId'=>$testId,'testType'=>PRACTICAL_TEST,'userId'=>$userId,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'status'=>TEST_COMPLETED,'isActive'=>1);
				$where=array('testId'=>$testId,'userId'=>$userId,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'status'=>TEST_COMPLETED,'isActive'=>1);
				$count=$this->getAllRecords(TBL_TIMES,$where,$select="status");//completed tests
				if($count){
				$data['practicalTests'][$i]->count=count($count);
				}else{$data['practicalTests'][$i]->count=0;}
				//$where=array('testId'=>$testId,'testType'=>PRACTICAL_TEST,'userId'=>$userId,'userPackageID'=>$uniqueId,'packageId'=>$packageId,'isActive'=>1);
				$where=array('testId'=>$testId,'userId'=>$userId,'userPackageID'=>$uniqueId,'packageId'=>$packageId,'isActive'=>1);
				$attempts=$this->Testsmodel->getNumberOfAttempts(TBL_TIMES,$where);
				if($attempts){
				$data['practicalTests'][$i]->attempted=$attempts->attempts;
				}else{$data['practicalTests'][$i]->attempted=0;}
				$presentSatus=$this->Testsmodel->getLatestStatus($testId, $userId, $uniqueId);
				if($presentSatus){
					if($presentSatus->status == 2){$data['practicalTests'][$i]->redo=1;}
				}
				
			}
			
			}
			$data['optedTests']= $this->Testsmodel->getOptedTests(PRACTICAL_TEST,2);
            $data['packages']= $this->Packagemodel->getPackages();
			
			
			if($this->session->userdata('reseller_id')){
			$resellerId=$this->session->userdata('reseller_id');
		    }else{
			$resellerId=0;
		    }
			$where=array('resellerId'=>$resellerId,'isActive'=>1,'isDeleted'=>0);
			$data['newsUpdates']=$this->getAllRecords(TBL_NEWS,$where,$select="*");
			$userId=$this->session->userdata('user_id');
			
			$where=array('userId'=>$userId);
    	    $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
			$cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
				$data['cartCount']=$cartCount;
			}else{
				$data['cartCount']=0;
			} 
			$this->load->view('header',$data);
			$this->load->view('dashboard',$data);
			$extrafooter = $this->load->view('dashboard_script');
			$this->load->view('footer',$extrafooter);
			
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	}
	
	
/*******************
	********************
	This method is useful to 
	get number of attempts popup
	********************
	********************/
	function attemptsByTest(){
		 $userId=$this->session->userdata('user_id'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
		$examid=$this->input->post('testId');
		$uniqueId=$this->input->post('uniqueId');
		$where=array('testId'=>$examid,'userId'=>$userId,'userPackageID'=>$uniqueId,'isActive'=>1,'status'=>3);
        $data['atempts']=$this->getAllRecords(TBL_TIMES,$where,$select="*");
		$this->load->view('attempts_ajax',$data);
	}
	
	
	
	
	/*******************
	********************
	This method is useful to 
	start exam with competed details
	********************
	********************/
	
	function saveUserIp($userId){
		if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
			$data['ipAddresss']=$this->input->ip_address();
			$data['userID']=$userId;
			$data['createdTime']=date('y-m-d h:i:s');
			$where=array();
		    $this->insertOrUpdate(TBL_USER_TRACK,$where,$data);
	}
	function generateOtp(){
		 $token=md5(uniqid());
	     $this->session->set_userdata('checktoken',$token);
	     $session_token=$this->session->userdata('token');
	 }
	function startExam()
	{
		     
	        $userId=$this->session->userdata('user_id'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
		try{
			$this->generateOtp();
			$checktoken=$this->session->userdata('checktoken');
			if($checktoken){
			$data = array();
			$type = $this->uri->segment(3);
			if($type=="practicalexam"){ $ty=2; }else{ $ty=1; }
				$examId =$this->uri->segment(5);
				$package = $this->uri->segment(7);
				$uniqueId = $this->uri->segment(9);
				$this->session->set_userdata('testType',$ty);
				$this->session->set_userdata('examId',$examId);
				$this->session->set_userdata('packageiD',$package);
				$this->session->set_userdata('uniqueId',$uniqueId);
			if(!$examId)
			{
				redirect(DASHBOARD_URL,'refresh');
			}
			
			if($package){
			  $where=array('package_id'=>$package);
              $packageName=$this->getSingleRecord(TBL_PACKAGES,$where);
              $data['package_name'] = !empty($packageName)?$packageName->package_name:0;
              $data['package_id'] = $package;
            }
			$where=array('testId'=>$examId,'packageId'=>$package,'userPackageID'=>$uniqueId,'userId'=>$userId,'status'=>TEST_COMPLETED,'isActive'=>1);
			$CompletedTestTimes=$this->getAllRecords(TBL_TIMES,$where);
			$testAttempts=$this->getSingleRecord(TBL_TESTS,$where=array('testId'=>$examId),'attempts');
			$testAttempts=$testAttempts->attempts; 
			if($this->session->userdata('isPromotionMember')!=1){//on nov 21st
			if(($CompletedTestTimes )&& (count($CompletedTestTimes)>=$testAttempts)) {
				$this->session->set_flashdata('examError', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your attempts for this test  was completed.</strong> </div>");
				  echo  "<script type='text/javascript'>";
                  echo "window.close();";
				  echo  "window.opener.location ='http://onyx-exams.com/dashboard'";
                  echo "</script>";die();
				  redirect(DASHBOARD_URL,'refresh');
			}
			}
            $user_id=$this->session->userdata('user_id');
			
			//test details
			$where=array('testId'=>$examId);
            $data['totalQue'] = $this->Testsmodel->getTotalQueByTest($examId);
			if($data['totalQue'] == 0){
				$this->session->set_flashdata('examError', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your unable to attempt test because this test contains no questions.</strong> </div>");
				  echo  "<script type='text/javascript'>";
                  echo "window.close();";
				  echo  "window.opener.location ='http://onyx-exams.com/dashboard'";
				// echo  "window.opener.location =".DASHBOARD_URL;
                  echo "</script>";die();
				redirect(DASHBOARD_URL,'refresh');
				
				
			}
			$RESULT=$this->sentOtp();
            $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks");
            $data['examId'] = $examId;	
			$data['type'] = $ty;
			$data['uniqueId'] = $uniqueId;
			if($CompletedTestTimes){
				$atempts=count($CompletedTestTimes);
			}else{$atempts=0;}
			$data['attempts'] =$atempts+1 ;
            $where=array('userId'=>$userId);
    	    $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");		
			$queToken=md5(uniqid());
			$this->session->set_userdata('queToken', $queToken);
			$this->load->view('exam_header',$data);
			$this->load->view('start_exam',$data);
		 }
		 else{
		  redirect(NOT_FOUND_URL);
			 }	
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 

	}

	/*******************
	********************
	This method is useful to 
	send otp
	********************
	********************/
	function sentOtp(){
		        $user_id=$this->session->userdata('user_id');
				$where=array('userId'=>$user_id);
				$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
				$phoneNumber=$details->phoneNumber;
			    $token=generateRandNumber(6);
				//$to[] = array('to'=>$phoneNumber);
				//$jsondata = array('sender'=>'ONYXED','message'=>'Please use this otp to start exam '.$token,'sms'=>$to);
				//$jsondata = json_encode($jsondata);
                //$response = sendOTP($jsondata);
			    $msgText = "Please use this otp to start your exam ".$token;
                $response = sendOTP($msgText, $phoneNumber);
				$userId=$details->userId;
				$where=array('userID'=>$userId,'isactive'=>1);
				$checkUserMultipleotp=$this->getAllRecords(TBL_OTPS,$where);
				if($checkUserMultipleotp){
					$where=array('userID'=>$userId,'isactive'=>1);
					$udata['isactive']=0;
					$this->insertOrUpdate(TBL_OTPS,$where,$udata);
				}
				$purpose="startExam";
			    $this->Testsmodel->saveRecoveryEmailData($userId,$token,$purpose);
	}
	
	     function verifyOtp(){
	   
	                $this->load->library('form_validation');
					$this->form_validation->set_rules('otp','otp','trim|required');
					$this->form_validation->set_rules('isAgree','isAgree','trim|required');
					$url = trim($this->input->post('url'));
					$packageId = trim($this->input->post('packageId'));
				    $examid = trim($this->input->post('examid'));
					$uniqueId = trim($this->input->post('uniqueId'));
					if($this->form_validation->run()!=false)
						{
							$packageId = trim($this->input->post('packageId'));
							$examid = trim($this->input->post('examid'));
							$uniqueId = trim($this->input->post('uniqueId'));
							$otp = trim($this->input->post('otp'));
							$type = trim($this->input->post('type'));
							$user_id=$this->session->userdata('user_id');
				            $where=array('userID'=>$user_id,'isactive'=>1,'token'=>$otp,'purpose'=>'startExam');
				            $checkvalidOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*");
							//if($otp == '123456'){
						    if(count($checkvalidOtp)>0){
                                $this->session->unset_userdata('token');
								$save=$this->saveExamDetails($examid,$type,$packageId, $uniqueId);
								$this->session->set_userdata('otp',$otp);
								
                                $link= EXAM_URL.'/exam/'.$examid.'/package/'.$packageId.'/uniqueId/'.$uniqueId;
								//$link= EXAM_URL.'?exam='.$examid.'&package='.$packageId;
								redirect($link);
							}else{
							 $this->session->set_flashdata('otp', "  <div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Invalid otp!</strong> </div>");
							 $url=SITEURL.'dashboard/startExam/mainexam/test/'.$examid.'/package/'.$packageId.'/uniqueId/'.$uniqueId;
							  redirect($url);
				             }
						}
						else 
						{
							$this->session->set_flashdata('otp', " <div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please enter otp and check terms and conditions to start exam</strong> </div>");
							$url=SITEURL.'dashboard/startExam/mainexam/test/'.$examid.'/package/'.$packageId.'/uniqueId/'.$uniqueId;
							redirect($url);
							

						}
	}
	function saveExamDetails($examId, $type, $packageId, $uniqueId)
	{     
           $userId=$this->session->userdata('user_id'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
	        $where=array('testId'=>$examId);
            $data['totalQue'] = $this->Testsmodel->getTotalQueByTest($examId);
            $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks");
			    $startedExamId=$examId; 
				$updata['testId']=$startedExamId;
				$updata['testTime']=$data['testname']->testTime;
				$updata['startTime']=date("Y-m-d H:i:s");
				$updata['userId']=$userId;
				$updata['packageId']=$packageId;
				$updata['userPackageID']=$uniqueId;
				$updata['status']=0;
				$updata['testType']=$type;
				$updata['ipAddresss']=$this->input->ip_address(); //today
				$updata['createdTime']=date("Y-m-d H:i:s");
				$where=array('testId'=>$startedExamId,'userId'=>$userId,'testType'=>$type,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'isActive'=>1);
				$isDoneBefore=$this->Testsmodel->getNumberOfAttempts(TBL_TIMES,$where);
				$where=array();
				if($isDoneBefore){
					$attempts=$isDoneBefore->attempts;
					$updata['attempts']=$attempts+1;
					if($isDoneBefore->status==2){
						$updata['attempts']=$attempts;
						$updata['timeDifference']=$isDoneBefore->timeDifference;				
						$updata['status']=$isDoneBefore->status;
                        $where=array('id'=>$isDoneBefore->id);
					}
					
				}else{
					$updata['attempts']=1;
				}
				$queToken=md5(uniqid());
				$this->session->set_userdata('queToken',$queToken);
				$this->session->set_userdata('attempts',$updata['attempts']);
				$results=$this->insertOrUpdate(TBL_TIMES,$where,$updata);
			
	}
	/*******************
	********************
	This method is useful to 
	load questions and to save options
	********************
	********************/
	
	function questionPaper()
	{  
	    $time=0; 
		$queToken=$this->session->userdata('queToken'); 
        $userId=$this->session->userdata('user_id'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
		$examid=$this->uri->segment(4);
        $package=$this->uri->segment(6);
        $uniqueId=$this->uri->segment(8);
		$attempts=$this->session->userdata('attempts');
		$testType=$this->session->userdata('testType');
		$userId=$this->session->userdata('user_id');
		
		$where=array('userId'=>$userId);
    	$data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
        $this->load->view('exam_header',$data);
        $examid=$this->uri->segment(4);
        $package=$this->uri->segment(6);
		$where=array('testId'=>$examid);
        $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks,isShuffleQueOrder");
		$isShuffle= $data['testname']->isShuffleQueOrder;
		$limit = 1;
		$offset=0;
        $type=1;
        $data['questionNumbers']=$this->Testsmodel->getQuestionNumbers($examid,$isShuffle);
		$attempted = $this->Testsmodel->getAttemptedQue($examid, $userId, $testType, $attempts, $package, $uniqueId);
		$data['attempted']=array();
        if($attempted){
        $data['attempted']=$attempted;
        }
		$attemptedQues=$data['attempted'];
		if($attemptedQues){
		for($i=0;$i<count($attemptedQues);$i++){
		 $attemptArray[$i]=$attemptedQues[$i]->que_id;
		}$data['attemptArray']=$attemptArray;}
		else{$data['attemptArray']=array();}
		$ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts);
		$data['ReferenceTestIdArray']=$ReferenceTestId;
		
		//start ajax call
		
		if($this->input->is_ajax_request())
		{      
			$selectedOpt=$this->input->post('val');
            $offset=$this->input->post('index');
			$type=$this->input->post('type');
			$review=$this->input->post('review');
			$order=$this->input->post('order');
			if($type==2){
					  $offset=$ReferenceTestId[$order-1];
					  $data['ques']=$order;
				      $data['details']=$this->Testsmodel->getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,$uniqueId,$order);
                      $filter_view =$this->load->view('questions_ajax',$data,true);
                      echo $filter_view;die();
			}else{
					 if($selectedOpt){
				      $options = implode(",", $selectedOpt);
				      $option = str_replace(" ","", $options);
			          }else{
				      $option =0;
			         }
						   
					 $where=array('que_id'=>$offset,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1);
					 $check=$this->getSingleRecord(TBL_EXAM_RESULTS,$where,$select="*");
					 if($check){
						  $where=array('que_id'=>$offset,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1); $udata['updatedTime']=date("Y-m-d H:i:s");
						}else{
						   $where=array();
						   $udata['createdTime']=date("Y-m-d H:i:s");
						}
						   $udata['option_id']=$option;
						   $udata['que_id']=$offset;
						   $udata['userPackageID']=$uniqueId;
						   $udata['testId']=$examid;
						   $udata['packageId']=$package;
						   $udata['userId']=$userId;
						   $udata['attempts']=$attempts;
						   $udata['testType']=$testType;
						   $udata['is_reviewed'] = $review;
						
						   $results=$this->insertOrUpdate(TBL_EXAM_RESULTS,$where,$udata);
						   if($review && empty($selectedOpt)){
								   $where=array('que_id'=>$offset,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1);
								   $check=$this->getSingleRecord(TBL_EXAM_RESULTS,$where,$select="*");
								   if($check){
									   $where=array('que_id'=>$offset,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1); $udata['updatedTime']=date("Y-m-d H:i:s");
									   $udata['option_id']=$check->option_id;
									   }else{$where=array();
								   $udata['createdTime']=date("Y-m-d H:i:s"); $udata['option_id']=0;}
								   $udata['que_id']=$offset;
								   $udata['testId']=$examid;
								   $udata['userId']=$userId;
								   $udata['packageId']=$package;
								   $udata['attempts']=$attempts;
								   $udata['testType']=$testType;
								   $udata['is_reviewed'] = $review;
								   $udata['userPackageID']=$uniqueId;
                                   $results=$this->insertOrUpdate(TBL_EXAM_RESULTS,$where,$udata);
								   }
						    $attempted = $this->Testsmodel->getAttemptedQue($examid,$userId,$testType,$attempts, $package ,$uniqueId);
                            $data['attempted']=array();
                             if($attempted){
                                  $data['attempted']=$attempted;
                              }
								$attemptedQues=$data['attempted'];
								if($attemptedQues){
								 for($i=0;$i<count($attemptedQues);$i++){
								 $attemptArray[$i]=$attemptedQues[$i]->que_id;
								}$data['attemptArray']=$attemptArray;}
								else{$data['attemptArray']=array();}
								$ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts);
								$data['ReferenceTestIdArray']=$ReferenceTestId;
								@$offset=$ReferenceTestId[$order-1];
								$data['ques']=$order;
				     $data['details']=$this->Testsmodel->getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,$uniqueId);
                     $filter_view =$this->load->view('questions_ajax',$data,true);
                     echo $filter_view;die();
				 }
		    }
			
			//end ajax call
		
		      $type=2;
		      $ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts);
			   if($ReferenceTestId){
				   $data['ReferenceTestIdArray']=$ReferenceTestId;$offset=$ReferenceTestId[0];
				   /****last question for redo**/
				   $details=$this->Testsmodel->getLatestStatus($examid,$userId,$uniqueId);
				    $data['ques']=1;
		           if($details){
			          $time=($details->testTime)-($details->timeDifference);
			          $data['time']=$time;
			          $lastAnsweredQuestion=$this->Testsmodel->getLastAtemptedQue($examid,$userId,$uniqueId,$attempts);
					  if($lastAnsweredQuestion){
						  $ques = array_search($lastAnsweredQuestion, $ReferenceTestId);$offset=$ReferenceTestId[$ques];
						  $data['ques']=$ques+1;
						  }else{ $ques =0; $data['ques']=$ques+1;}
				   }
				   /***end***/
				}else{
				     //saving order of questions
					  $mainarray=array();
					  if($data['questionNumbers']){
						 foreach($data['questionNumbers'] as $questions){$mainarray[]=$questions->que_id;}
					  }		  
					  $array = array_values($mainarray);
					  $where=array();$table=TBL_QUESTIONS_ORDER;
					  $referenceData['que_id']=implode(',',$array);
					  $referenceData['test_id']=$examid;
					  $referenceData['isActive']=1;
					  $referenceData['user_id']=$userId;
					  $referenceData['userPackage']=$uniqueId;
					  $referenceData['attempts']=$attempts;
					  $res=$this->insertOrUpdate($table,$where,$referenceData);
					  $offset= $array[0]; $data['ques']=1;
		       }
		  $data['details']=$this->Testsmodel->getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,$uniqueId);
        	 
		 $ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts);
		  $data['ReferenceTestIdArray']=$ReferenceTestId;
		  if($time){
			 $clock=$this->getTimer($time); 
			 $data['clock']=$clock;
		  }else{
			  $clock=$this->getTimer($data['testname']->testTime);
			  $data['clock']=$clock;
		  }
		 if($queToken){		 
		  $this->load->view('exam_questions',$data);
		  $this->load->view('footer');
	      $this->session->unset_userdata('queToken');
		}else{
			redirect(NOT_FOUND_URL);
		}
		
	}
	function getTimer($duration){
		$duration=$duration;
    //if(@$time){$duration=$time;}
	$currentDate = date("Y-m-d H:i:s");
	$this->session->set_userdata('currentTime',$currentDate);
	$cTime=$this->session->userdata('currentTime');
  
	$currentDate =strtotime($cTime);
	$futureDate = $currentDate+(60*$duration);
	$formatDate = date("Y-m-d H:i:s", $futureDate);
	$time = new DateTime($formatDate);
	$d= date('M d ,Y', strtotime($formatDate));
	$t= $time->format('H:i:s');
	 $x=$d .' '.  $t;
	 return  $x; 
	}
	function notFound(){
		     $data['heading']="404 Page Not Found";
			 $data['message']="The page you requested was not found";
			 $this->load->view('errors/html/error_404',$data);
	}
	/*******************
	********************
	This method is useful to 
	get review questions
	********************
	********************/
	
	
	function reviewQuestions(){
		
		 $userId=$this->session->userdata('user_id'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
		$examid=$this->input->post('examId');
		$package=$this->input->post('package');
		if($package == ''){$package=$this->session->userdata('packageiD');}
		if($examid == ''){$examid=$this->session->userdata('examId');}
		$attempts=$this->session->userdata('attempts');
		$uniqueId=$this->session->userdata('uniqueId');
		$testType=$this->session->userdata('testType');
		$userId=$this->session->userdata('user_id');
		$where=array('testId'=>$examid,'userId'=>$userId,'attempts'=>$attempts,'is_reviewed'=>1,'testType'=>$testType,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1);
        $data['reviewQusetions']=$this->getAllRecords(TBL_EXAM_RESULTS,$where,$select="que_id");
		$ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts);
		$data['ReferenceTestIdArray']=$ReferenceTestId;
		$res=$this->load->view('review_questions_ajax',$data);
	}
	
   
		
	/*******************
	********************
	This method is useful to 
	save options for test questions
	********************
	********************/
 function saveOptions()
    {  
	    $que=$this->input->post('que'); 
        $selectedOpt=$this->input->post('val');
        $userId=$this->session->userdata('user_id'); 
		$examid=$this->input->post('examId');
		$order=$this->input->post('order');
		$uniqueId=$this->session->userdata('uniqueId');
		$package=$this->input->post('package');
		$attempts=$this->session->userdata('attempts');
		$testType=$this->session->userdata('testType');
	    if($selectedOpt){
				$options = implode(",", $selectedOpt);
				$option = str_replace(" ","", $options);
			}else{
				 $option =0;
			}
	    $where=array('que_id'=>$que,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1);
		$check=$this->getSingleRecord(TBL_EXAM_RESULTS,$where,$select="*"); 
		if($check){
			$where=array('que_id'=>$que,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1); $udata['updatedTime']=date("Y-m-d H:i:s");
		}else{
			$where=array();
			$udata['createdTime']=date("Y-m-d H:i:s");
		}
			$udata['option_id']=$option;
			$udata['que_id']=$que;
			$udata['testId']=$examid;
			$udata['packageId']=$package;
			$udata['userId']=$userId;
			$udata['userPackageID']=$uniqueId;
			$udata['attempts']=$attempts;
			$udata['testType']=$testType;
			$results=$this->insertOrUpdate(TBL_EXAM_RESULTS,$where,$udata);
		  if($results){
				 $limit = 1;
		         $offset=$order;
				 $type=4;
			  $data['questionNumbers']=$this->Testsmodel->getQuestionNumbers($examid);
			  $where=array('testId'=>$examid);
			  $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks");
			
			 $ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts);
		     $data['ReferenceTestIdArray']=$ReferenceTestId;
			 @$offset=$ReferenceTestId[$order-1];
			  
			  $data['details']=$this->Testsmodel->getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,$uniqueId);
		 
			  $data['ques']=$order; 
			  $attempted = $this->Testsmodel->getAttemptedQue($examid,$userId,$testType,$attempts,$package, $uniqueId);
              $data['attempted']=array();
			 
              if($attempted){
                  $data['attempted']=$attempted;
               }
		      $attemptedQues=$data['attempted'];
			  if($attemptedQues){
					foreach($attemptedQues as $atmpt){
						$attemptArray[]=$atmpt->que_id;
						}
						$data['attemptArray']=$attemptArray;
						}else{
							$data['attemptArray']=array();}
                            $filter_view =$this->load->view('questions_ajax',$data,true);
							echo $filter_view;die();
            }
		}
		
		function saveBlankOptions(){
			
        $que=$this->input->post('que'); 
        $val=$this->input->post('val');
        $userId=$this->session->userdata('user_id'); 
		$examid=$this->input->post('examId');
		
		$uniqueId=$this->input->post('unique');
		$package=$this->input->post('package');
		
		$attempts=$this->session->userdata('attempts');
		$testType=$this->session->userdata('testType');
		$queType=2;
	    
	    $where=array('que_id'=>$que,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1,'queType'=>2);
		$check=$this->getSingleRecord(TBL_EXAM_RESULTS,$where,$select="*"); 
		if($check){
			$where=array('que_id'=>$que,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1,'queType'=>2); $udata['updatedTime']=date("Y-m-d H:i:s");
		}else{
			$where=array();
			$udata['createdTime']=date("Y-m-d H:i:s");
		}
			$udata['textOption']=$val;
			$udata['que_id']=$que;
			$udata['testId']=$examid;
			$udata['packageId']=$package;
			$udata['userId']=$userId;
			$udata['userPackageID']=$uniqueId;
			$udata['attempts']=$attempts;
			$udata['testType']=$testType;
			$udata['queType']=$queType;
			if($val){
			$results=$this->insertOrUpdate(TBL_EXAM_RESULTS,$where,$udata);  
			}
           		
		}
	/*******************
	********************
	This method is useful to 
	get record after submission
	********************
	********************/
	function submitTest()
	{ 
    	
		 $userId=$this->session->userdata('user_id');
		 $packageiD=$this->session->userdata('packageiD'); 
		 $uniqueId=$this->session->userdata('uniqueId'); 
		 $data['packageiD']=$packageiD;
		 $data['uniqueId']=$uniqueId;
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
			$testType=$this->session->userdata('testType');
	        $attempts=$this->session->userdata('attempts');
	        $examId=$this->session->userdata('examId');
		    //deactivate otp
			   $userId=$this->session->userdata('user_id');
			   if($this->session->userdata('otp')){
					  $otp=$this->session->userdata('otp');
					  $where=array('userID'=>$userId,'isactive'=>1,'token'=>$otp,'purpose'=>'startExam');
					  $checkvalidOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*");
					//if($otp == '123456'){
					 if(count($checkvalidOtp)>0 || $this->session->userdata('isPromotionMember')==1){
							$where=array('userID'=>$userId,'isactive'=>1,'token'=>$otp,'purpose'=>'startExam');
							$udata['isactive']=0;
							$this->insertOrUpdate(TBL_OTPS,$where,$udata);
							
							$updata['status']=TEST_COMPLETED;
							$updata['endTime']= date("Y-m-d H:i:s");
							$updata['updatedTime']= date("Y-m-d H:i:s");
							$where=array('testId'=>$examId,'userId'=>$userId,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$packageiD,'userPackageID'=>$uniqueId,'isActive'=>1);
							
							$date_e=date("Y-m-d H:i:s");
							$details=$this->getSingleRecord(TBL_TIMES,$where);
							if($details){
							$date_s=$details->startTime;
							$to_time = strtotime($date_s);
	                        $from_time = strtotime($date_e);
	                        $diff= round(abs($to_time - $from_time) / 60,2);
							$updata['timeDifference']=$diff;
							if($details->timeDifference){
								$updata['timeDifference']=$diff+$details->timeDifference;
								$where=array('id'=>$details->id);
							}
                            $results=$this->insertOrUpdate(TBL_TIMES,$where,$updata);
							}
						}
			   }
	          $where=array('testId'=>$examId);
	          $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,negativeMarks,rightMarks");
	          $data['totalQue'] = $this->Testsmodel->getTotalQueByTest($examId);	
	          $data['attemptedQuestions'] = $this->Testsmodel->getAttemptedQueByTest($examId,$userId,$testType,$attempts,$packageiD,$uniqueId);	//new
			  if($data['attemptedQuestions']){
	                $data['leftQuestions'] = $data['totalQue']-(count($data['attemptedQuestions']));	 
                }else{
	              $data['leftQuestions'] = $data['totalQue'];	
                }
	         $where=array('testId'=>$examId,'userId'=>$userId,'attempts'=>$attempts,'testType'=>$testType,'packageId'=>$packageiD,'userPackageID'=>$uniqueId,'isActive'=>1);
	         $questions=$this->getAllRecords(TBL_EXAM_RESULTS,$where,$select="que_id,option_id,queType,textOption");//31
		     $count=0;
	           if($questions){
					  foreach($questions as $que){
						   $queId=$que->que_id;
						   $option=$que->option_id;
						   $queType=$que->queType;
						   if($queType == 2){
								   $textOption=$que->textOption;
								   $where=array('option'=>$textOption,'test_id'=>$examId,'que_id'=>$queId);
								   $r=$this->getSingleRecord(TBL_QUE_OPTIONS,$where,$select="*");
								  if($r){
								   $count=$count+1;
								  }
						   }else{				   
							   $option=explode(',',$option);
							   $res=$this->Testsmodel->getCorrectAnsByTest($examId,$queId);
							
							   /*$checkSelectedOptions=array_diff($option, $res);
							   
							   if(count($checkSelectedOptions)<=0){
								  $count=$count+1;
							   }*/
							   if( is_array($option) && is_array($res) && 
                        count($option) == count($res) &&
                        array_diff($option, $res) === array_diff($res, $option)){$count=$count+1;}
						   }
					  
					 }
	           }
	   
	       $data['correctAns']=$count;
		   if($data['attemptedQuestions']){$attemptedQuestions=count($data['attemptedQuestions']);}else{$attemptedQuestions=0;}
	       $data['incorrectAns'] = $attemptedQuestions-$data['correctAns'];
	       $where=array('testId'=>$examId,'userId'=>$userId,'attempts'=>$attempts,'packageId'=>$packageiD,'userPackageID'=>$uniqueId,'isActive'=>1);
	       $data['timings'] =  $this->getSingleRecord(TBL_TIMES,$where,$select="*");
	       if($data['timings']){
			  $tt= ($data['timings']->timeDifference)*60;
			}else{
		    $tt=0; 
	       }
	   $timeTakn=secondsToTime($tt); 
	   $data['timeTaken']= $timeTakn;
	   $data['testType']=$testType;
	   $data['attempts']=$attempts;
	   $data['status']=TEST_COMPLETED;
	   
	    $ref['isActive']=0;
		$where=array('user_id'=>$userId,'userPackage'=>$uniqueId,'attempts'=>$attempts,'test_id'=>$examId);
		$ref=$this->insertOrUpdate(TBL_OPTIONS_REFERENCE,$where,$ref);
		$ref=$this->insertOrUpdate(TBL_QUESTIONS_ORDER,$where,$ref);
		/* for sending promotion test results to mobile */
		if($this->session->userdata('isPromotionMember') == 1){
         if($data['attemptedQuestions']){$aQNS=count($data['attemptedQuestions']);}else{$aQNS=0;}
		   $totalMarks=($data['totalQue']*($data['testname']->rightMarks));
		   $answs=$data['correctAns'];
		   $scoredMarks=($answs*($data['testname']->rightMarks))-($data['testname']->negativeMarks*($data['incorrectAns'])) ;
			$message = "Hi , you have attempted ".$data['testname']->testName. " test having ".$data['totalQue']." questions for ".$totalMarks." marks.Out of these you attempted ".$aQNS." questions and scored ".$scoredMarks." marks ";
			@$response = sendOTP($msgText, $phoneNumber);
		}
		
		
	    $this->load->view('submit_exam',$data);

}

/*******************
	********************
	This method is useful to 
	submit test on browser close 
	********************
	********************/

function browserCloseSubmit(){
	    
	   $userId=$this->session->userdata('user_id'); 
   	   $packageiD=$this->session->userdata('packageiD'); 
   	   $uniqueId=$this->session->userdata('uniqueId'); 
	   $data['packageiD']=$packageiD;
	   $data['uniqueId']=$uniqueId;
       if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
	   $testType=$this->session->userdata('testType');
	   $attempts=$this->session->userdata('attempts');
	   $examId=$this->session->userdata('examId');
		//deactivate otp
			   $userId=$this->session->userdata('user_id');
			   if($this->session->userdata('otp')){
               $otp=$this->session->userdata('otp');
               $where=array('userID'=>$userId,'isactive'=>1,'token'=>$otp,'purpose'=>'startExam');
			   $checkvalidOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*");
			if(count($checkvalidOtp)>0){
           //  if($otp == '123456'){
					$where=array('userID'=>$userId,'isactive'=>1,'token'=>$otp,'purpose'=>'startExam');
					$udata['isactive']=0;
					$this->insertOrUpdate(TBL_OTPS,$where,$udata);
					$updata['status']=TEST_PENDING;
				    $updata['endTime']= date("Y-m-d H:i:s");
					$updata['updatedTime']= date("Y-m-d H:i:s");
					$where=array('testId'=>$examId,'userId'=>$userId,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$packageiD,'userPackageID'=>$uniqueId,'isActive'=>1);//new
					        $date_e=date("Y-m-d H:i:s");
							$details=$this->getSingleRecord(TBL_TIMES,$where);
							if($details){
								$date_s=$details->startTime;
								$to_time = strtotime($date_s);
								$from_time = strtotime($date_e);
								$diff= round(abs($to_time - $from_time) / 60,2);
								$updata['timeDifference']=$diff;
									if($details->timeDifference){
										$updata['timeDifference']=$diff+$details->timeDifference;
										$where=array('id'=>$details->id);
									}
							}
						
					$results=$this->insertOrUpdate(TBL_TIMES,$where,$updata);	
					 
				}
			   }
	
					 $where=array('testId'=>$examId);
	                 $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,negativeMarks,rightMarks");
	                 $data['totalQue'] = $this->Testsmodel->getTotalQueByTest($examId);	
                     $this->load->view('test_incomplete',$data);		
      // redirect(DASHBOARD_URL,'refresh');
	 
}

/*******************
	********************
	This method is useful to 
	get view record details
	********************
	********************/
function viewRecord()
{
	$userId=$this->session->userdata('user_id'); 
	$TY=$_GET['TY'];
      if(!$userId){
	      redirect(LOGOUT_URL,'refresh');
		}
		$testType=$_GET['type'];
		$attempts=$_GET['attempts'];
		$examId=$_GET['testId'];
		$packageiD=$_GET['package'];
		$userPackageId=$_GET['userPackageId'];
  
   $where=array('testId'=>$examId);
   $userId=$this->session->userdata('user_id');
   $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,negativeMarks,rightMarks,testTime");
   $data['totalQue'] = $this->Testsmodel->getTotalQueByTest($examId);	
   $data['attemptedQuestions'] = $this->Testsmodel->getAttemptedQueByTest($examId,$userId,$testType,$attempts,$packageiD,$userPackageId);//new
   if($data['attemptedQuestions']){
	   $data['leftQuestions'] = $data['totalQue']-(count($data['attemptedQuestions']));	 
   }else{
	    $data['leftQuestions'] = $data['totalQue'];	
   }
  
   $where=array('testId'=>$examId,'userId'=>$userId,'attempts'=>$attempts,'testType'=>$testType,'packageId'=>$packageiD,'userPackageID'=>$userPackageId,'isActive'=>1);		//new
   $questions=$this->getAllRecords(TBL_EXAM_RESULTS,$where,$select="que_id,option_id,queType,textOption");
    $count=0;
    if($questions){
	    foreach($questions as $que){
		   $queId=$que->que_id;
		   $option=$que->option_id;
		   $res=$this->Testsmodel->getCorrectAnsByTest($examId,$queId);
		   $queType=$que->queType;
                   if($queType == 2 || $queType == 3){
					   $textOption=$que->textOption;
					   $where=array('option'=>$textOption,'test_id'=>$examId,'que_id'=>$queId);
					   $r=$this->getSingleRecord(TBL_QUE_OPTIONS,$where,$select="*");
						  if($r){
						   $count=$count+1;
						  }
				   }else{				   
					   $option=explode(',',$option);
					   $res=$this->Testsmodel->getCorrectAnsByTest($examId,$queId);
					   if( is_array($option) && is_array($res) && 
                        count($option) == count($res) &&
                        array_diff($option, $res) === array_diff($res, $option)){$count=$count+1;}
					   /*$checkSelectedOptions=array_diff($option, $res);
						   if(count($checkSelectedOptions)<=0){
							  $count=$count+1;
						   }*/
				   }
		}
   }
    $where=array('testId'=>$examId,'userId'=>$userId,'attempts'=>$attempts,'testType'=>$testType,'packageId'=>$packageiD,'userPackageID'=>$userPackageId,'isActive'=>1);		//new

   $data['correctAns']=$count;	
   $data['incorrectAns'] =  $data['totalQue']-$data['correctAns'];	
   $data['timings'] =  $this->getSingleRecord(TBL_TIMES,$where,$select="*");
   $data['timeTaken']=0;
   $data['idleTime']= 0;
   $duration=$data['testname']->testTime;
   if($data['timings']){ 
	   $diff=$data['timings']->timeDifference;
	   $date_s=$data['timings']->startTime;
	   $date_e=$data['timings']->endTime;
	   $tt= $this->Testsmodel->getDifference($userPackageId,$attempts,$examId);
	   $timeTakn=secondsToTime($diff*60); 
	   $data['timeTaken']= $timeTakn;
	   
	   $idleTime=abs(($duration*60)-($diff*60));
	  
	   $idlTime=secondsToTime($idleTime);
	   $data['idleTime']= $idlTime;
	  
	   }
	
   $where=array('userId'=>$userId);
   $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
   $cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
				$data['cartCount']=$cartCount;
			}else{
				$data['cartCount']=0;
			}
   if($TY=="multiple"){
	   $this->load->view('header',$data);
	    $this->load->view('report',$data);
	   $this->load->view('footer');
   }else{
   $this->load->view('exam_header',$data);
    $this->load->view('report',$data);
   }
 
}




/*******************
	********************
	This method is useful to 
	get syllabus details of packages
	********************
	********************/
	function failure(){
		$this->session->set_flashdata('buypackage', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Your purchase is cancelled.</strong> </div>");
        redirect(DASHBOARD_URL,'refresh');
	}

/*******************
	********************
	This method is useful to 
	get syllabus details of packages
	********************
	********************/
	
function viewSyllabus()
{
	$userId=$this->session->userdata('user_id'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
	$userId=$this->session->userdata('user_id');
	$packageId=$this->uri->segment(4);
	if(!$userId){
		redirect(ACCOUNT_URL,'refresh');
	}
	    $where=array('package_id'=>$packageId);
		$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
		$data['packageName']=$packageDetails->package_name;
		$data['package_amount']=$packageDetails->package_amount;
		$testId=$packageDetails->package_test;
		$ids= explode(',', $testId);
		$packageTests= $this->Packagemodel->getalltestsByPackage($ids);
		$data['testDetails']=$packageTests;
		if($packageTests && count($packageTests)>0)
			{
				$testNames = '';
				foreach($packageTests as $packageTest)
				{
					$testNames .= $packageTest->testName.', ';
				}
				$data['testNames'] = rtrim($testNames, ", ");
				
			}else { $data['testNames']  = '';  }
		 $this->load->view('viewSyllabus',$data);
	}
	
	
	function newsDetails(){
		
		$userId=$this->session->userdata('user_id');
	    if(!$userId){
		 redirect(ACCOUNT_URL,'refresh');
	     }
		 $where=array('userId'=>$userId);
		 $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
        $id=$_GET['id'];
	    $where=array('id'=>$id);$table=TBL_NEWS;
        $result = $this->getSingleRecord($table,$where,$select="*");
		$data['details']=$result;
		$cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
				$data['cartCount']=$cartCount;
			}else{
				$data['cartCount']=0;
			}
		$this->load->view('header',$data);		
        $this->load->view('newsDetails',$data);		
        $this->load->view('footer',$data);
      }
	  
	
	   function cart()
     { 
	     $tests=array();
	     $packages=array();
		 $data['packageids']=array();
		 $data['testIds']=array();
         $userId=$this->session->userdata('user_id');
	     if(!$userId){
		   redirect(ACCOUNT_URL,'refresh');
	     }
		  $where=array('userId'=>$userId);
		  $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
        
		  $where=array('isActive'=>1,'userId'=>$userId,'itemType'=>1);
		  $packages = $this->getAllRecords(TBL_CART,$where,$select="*");
	      if($packages && count($packages)>0)
		  {
		      for($i=0;$i<count($packages);$i++)
                   {
					$itemId=$packages[$i]->itemId;
                       $where=array('package_id'=>$itemId);   
                       $details=$this->getSingleRecord(TBL_PACKAGES,$where);
                     if($details){
					   $packages[$i]->packageName=$details->package_name;
					   }
					  $packageids[$i]=$itemId;
					 } $data['packageids']=$packageids;
				}
		   $where=array('isActive'=>1,'userId'=>$userId,'itemType'=>2);
		   $tests = $this->getAllRecords(TBL_CART,$where,$select="*");
		   if($tests && count($tests)>0)
		    {
		      for($i=0;$i<count($tests);$i++)
                   {
					  $testitemId=$tests[$i]->itemId;
					  $where=array('testId'=>$testitemId);   
                      $details=$this->getSingleRecord(TBL_TESTS,$where);
                      if($details){
					   $tests[$i]->testName=$details->testName;
					   }
					    $testids[$i]=$testitemId;
					 }
					 $data['testIds']=$testids;
				 }
		   
           $cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
				$data['cartCount']=$cartCount;
			}else{
				$data['cartCount']=0;
			}
         $data['tests']=$tests;
         $data['packages']=$packages;
         $where=array('userId'=>$userId);
		 $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
          $totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
          $data['amount']=(!empty($totalAmount))?$totalAmount:0;//debug($data);
          $this->load->view('header',$data);		
          $this->load->view('cart',$data);	
           $this->load->view('dashboard_script');		  
          $this->load->view('footer',$data); 
				
     }
	 
	function summeryReport(){
	    $TY=$_GET['TY'];
		$testId=$_GET['testId'];
		$type=$_GET['type'];
		$package=$_GET['package'];
		$attempts=$_GET['attempts'];
		$userPackageId=$_GET['userPackageId'];
		$userId=$this->session->userdata('user_id'); 
		$where=array('testId'=>$testId);
		$data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,negativeMarks,rightMarks,testTime");
		$where=array('test_id'=>$testId,'isActive'=>1,'isDeleted'=>0);
		$questions=$this->getAllRecords(TBL_QUETIONS,$where,$select="que_id,que,queType");
		$i=0; $details="";
		if($questions){ 
		foreach($questions as $que){
			$userOtpArry=array();$crctOptionArray=array();
			@$details[$i]->TotalAnswers="";
			@$details[$i]->Myoption="";
			@$details[$i]->question=$que;
			$queId=$que->que_id;
			//on 14th
			if($que->queType == 2 || $que->queType == 3){
				$where=array('que_id'=>$queId,'test_id'=>$testId,'isActive'=>1,'isDeleted'=>0);
				$TotalAnswers=$this->getAllRecords(TBL_QUE_OPTIONS,$where,$select="option_id,option,textType");
			
			}else{
				$where=array('que_id'=>$queId,'test_id'=>$testId,'isActive'=>1,'isDeleted'=>0);
				$TotalAnswers=$this->getAllRecords(TBL_QUE_OPTIONS,$where,$select="option_id,option,textType"); 
				$where=array('que_id'=>$queId,'is_answer'=>1,'test_id'=>$testId,'isActive'=>1,'isDeleted'=>0);
			
			}
			$questionAnswers=$this->getAllRecords(TBL_QUE_OPTIONS,$where,$select="option_id,option,textType"); 
			$details[$i]->option=$questionAnswers;
			$details[$i]->TotalAnswers=$TotalAnswers;
			$where=array('userId'=>$userId,'testId'=>$testId,'userPackageID'=>$userPackageId,'testType'=>$type,'que_id'=>$queId,'attempts'=>$attempts);
			$userAnswers=$this->Testsmodel->getMyAnswers($userId,$testId,$userPackageId,$type,$queId,$attempts);
		    
		if($que->queType == 0 || $que->queType == 1){
			
				if($questionAnswers){
					foreach($questionAnswers as $qa){
						$crctOptionArray[]=$qa->option_id;
					}
				}
				if($userAnswers){

					foreach($userAnswers as $ua){
					    $notps=$ua->normalOptions;
						for($j=0;$j<count($notps);$j++){
							$userOtpArry[$j]=$notps[$j]->option_id;
							
						}
						$userOtpArry=array_filter($userOtpArry);
					}
				}
				if(is_array($crctOptionArray) && is_array($userOtpArry) && count($crctOptionArray)>0 && 
					   count($userOtpArry) == count($crctOptionArray) &&
					   array_diff($userOtpArry, $crctOptionArray) === array_diff($crctOptionArray, $userOtpArry))
					   {$isCorrect='yes';}else{$isCorrect='No';}
					   $details[$i]->isCorrect=$isCorrect;
					  
	      }else{
			  
				  @$crctOpt=$questionAnswers[0]->option;
				  @$myopt=$userAnswers[0]->textOption;
				  
				   if($crctOpt && $crctOpt==$myopt)
				   {$isCorrect='yes';}
				   else{$isCorrect='No';}
				   $details[$i]->isCorrect=$isCorrect;
			}
			
			
				if($userAnswers){
				$details[$i]->Myoption=$userAnswers;
				}else{$details[$i]->Myoption="";} $i++;
			}
			
		}
	   $data['details']=$details;
	   $cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
				$data['cartCount']=$cartCount;
			}else{
				$data['cartCount']=0;
			}
//debug($data);	   
		if($TY=="multiple"){
	      $this->load->view('header',$data);
		}else{
			$this->load->view('exam_header');
		}
		
		$this->load->view('summaryReport',$data);
		$this->load->view('footer');
		
	}
	 function getFiftyFiftyOptions()
	 {
		
		 $test=$this->input->post('test');
		 $que=$this->input->post('que');
		 $package=$this->input->post('package');
		 $unique=$this->input->post('unique');
		 $IncorrectAnswers=array();
		 $where=array('que_id'=>$que,'is_answer'=>1,'test_id'=>$test,'isActive'=>1,'isDeleted'=>0);
		 $correctAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*");
		 $correctAnswers=($correctAnswers)?$correctAnswers:array();
		
		 $where=array('que_id'=>$que,'is_answer'=>0,'test_id'=>$test,'isActive'=>1,'isDeleted'=>0);
		 $IncorrectAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*");
		 $IncorrectAnswers=($IncorrectAnswers)?$IncorrectAnswers:array();
		 //new
		 if(empty($correctAnswers) && $IncorrectAnswers){
		  $incorctOption=$IncorrectAnswers[0]->option_id;
		  if(count($correctAnswers)<=0){
			  $correctAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*",$incorctOption);
		  }
		 }
		 if(empty($IncorrectAnswers) && $correctAnswers){
			 $where=array('que_id'=>$que,'is_answer'=>1,'test_id'=>$test,'isActive'=>1,'isDeleted'=>0);
		  $corctOption=$correctAnswers[0]->option_id;
		  if(count($IncorrectAnswers)<=0){
			  $IncorrectAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*",$corctOption);
		  }
		 }
		  $details=array_merge($correctAnswers,$IncorrectAnswers);
		
		 foreach($details as $detail){
			 $optArr[]=$detail->option_id;
		 }
		 $opt_id=implode(',',$optArr);
		 $data['options']=$details;
		 $insertData['user_id']=$this->session->userdata('user_id');
		 $insertData['test_id']=$test;
		 
		 $insertData['que_id']=$que;
		 $insertData['opt_id']=$opt_id;
		 $insertData['userPackage']=$unique;
		 $insertData['attempts']=$this->session->userdata('attempts');
		 $where=array();
		 $this->insertOrUpdate(TBL_OPTIONS_REFERENCE,$where,$insertData);
		 $data['que']=$que;
		 $data['package']=$package;
		if(empty($package)){
			$data['package']=0;
		}
		
		$data['test']=$test;
		$res=$this->load->view('options_ajax',$data,true);
		echo  $res ;die();
		
		 
	 }
	
	
	function checkCouponValidity()
	{  
	
	    if($this->input->post('userId')){
			$userId=$this->input->post('userId');
		}else{
			$userId=$this->session->userdata('user_id');
		}
		
	    if(!$userId){
		 redirect(ACCOUNT_URL,'refresh');
	     }
		 $couponCode=$this->input->post('text');
		 $amount=$this->input->post('amount');
		 $where=array('coupon_code'=>$couponCode,'isActive'=>1,'isDeleted'=>0);
		 $result=$this->getSingleRecord(TBL_COUPONS,$where,$select="*");
		
		 if(!$result){
			 $messege="Please enter valid coupon number.";
			 echo $messege;die();
		 }else{
			 $couponValidFrom = date('Y-m-d', strtotime($result->coupon_createdTime));
             $couponValidTo = date('Y-m-d', strtotime($result->coupon_validity));
			 $todatDate=date('Y-m-d');
			 
           if (($todatDate >= $couponValidFrom) && ($todatDate <= $couponValidTo))
             {
               if($amount != $result->coupon_amount){
				   $messege="Your Coupon amount is not equal to purchase amount.";
				    echo $messege;die();
			   } else{
				    echo 1;die();
				  
				 }
             }
			else
           {
               $messege="Your coupon was expired";
			    echo $messege;die();
           }
		}
		 
		
	}
	/*******************
	********************
	This method is useful to 
	buy package through pay u money and 
	uer to send mail with details
	********************
	********************/
	function purchasePackageOrTest(){
		
		$resellerId=0;
		$PackageAmount=0;
		$testAmount=0;
		$packageids=$this->session->userdata('packageId');
		$testIds=$this->session->userdata('testId');
		//$data['userName']=$this->session->userdata('userName');
		
		$userId=$this->input->post('userId');
	    $CouponCode=$this->input->post('CouponCode');
		$where=array('coupon_code'=>$CouponCode);
        $couponDetails=$this->getSingleRecord(TBL_COUPONS,$where,$select="*");
	  if($CouponCode && count($couponDetails)>0){
	    $amount=$this->input->post('amount');
		$where=array('coupon_code'=>$CouponCode);
		$couponData['isActive']=0;
		$this->insertOrUpdate(TBL_COUPONS,$where,$couponData);
		
		if(empty($packageids) && empty($testIds)){
			$where=array('userId'=>$userId);
			$res=$this->getSingleRecord(TBL_USERS,$where);
			$testIds=$res->testIds;
			$packageids=$res->packageIds;
			$userName=$res->userName;
			$resellerId=$res->resellerId;
			$data['userName']=$userName;
		}
		if($packageids){
		  $packageArray=explode(',',$packageids);
		  foreach($packageArray as $packary){
			$pId=$packary;
			$where=array('package_id'=>$pId);
			$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
			$testId=$packageDetails->package_test;
			$where=array();
			$updata['createdTime']= date("Y-m-d H:i:s");
            $updata['userId']=$userId;
			$updata['packageId']=$pId;
			$updata['isActive']= 1;
			$updata['packageTests']=$testId;
			$updata['amount']= $packageDetails->package_amount;
			$updata['couponCode']= $CouponCode;
			$updata['isDeleted']= 0;
		    $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
	     }
	  }
	
	   if($testIds){
		$where=array();
		$updata['createdTime']= date("Y-m-d H:i:s");
        $updata['userId']=$userId;
        $updata['packageId']=0;
        $updata['isActive']= 1;
        $updata['isDeleted']= 0;
        $updata['packageTests']= $testIds;
		$updata['couponCode']= $CouponCode;
		$testAmount=$this->Testsmodel->getSelectedTestsAmount($testIds);
		$testAmount=$testAmount->total;
		$updata['amount']= $testAmount;
        $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
		}
		
		$data['packageDetails']=0;$data['testDetails']=0;
		if($packageids){
		$data['packageDetails']=$this->Testsmodel->getSelectedPackagesDetails($packageids);
		$PackageAmount=$this->Testsmodel->getSelectedPackagesAmount($packageids);
		$PackageAmount=$PackageAmount->total;
		}
		if($testIds){
		$data['testDetails']=$this->Testsmodel->getSelectedTestDetails($testIds);
		$testAmount=$this->Testsmodel->getSelectedTestsAmount($testIds);
		$testAmount=$testAmount->total;
		}
		$amount=$PackageAmount+$testAmount;
		
		//$data['amount']=($this->session->userdata('amount'))?$this->session->userdata('amount'):$amount;
		$where=array('userId'=>$userId);
		$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
		$data['userName']=$details->userName;
		$data['amount']=$amount;
		$subject="Purchase Details";
		$message=$this->load->view('emailDetails', $data,true);
		$smtpEmailSettings = $this->config->item('smtpEmailSettings');
		        $where=array('userId'=>$userId);
				$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
				$email_id=$details->emailAddress;
		$isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
	  
		$where=array('userId'=>$userId);
		$vdata['is_user_verified']=1;
		$vdata['updatedTime']=date("y-m-d H:i:s");
		$res=$this->insertOrUpdate(TBL_USERS,$where,$vdata);
		$this->session->set_flashdata('message', "<div class='alert alert-info alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Your Purchase is done successfully.Check your email for details.Login to your account.</strong> </div>");
        if($resellerId){
		   redirect(SITEURL.$userName);
	   } else{
		   redirect(SITEURL);
	   }
	   }else{
		$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Please enter valid coupon number.</strong> </div>"); 
	     $url=$this->input->post('url'); redirect( $url);
		 
	}	
	}
	
	/*******************
	********************
	This method is useful to 
	buy package from cart through pay u money and 
	uer to send mail with details
	********************
	********************/
	
	function buypackage()
	{
	 $packageids=$this->session->userdata('packageids');
	
	 $testIds=$this->session->userdata('testIds');
	 $userId=$this->input->post('userId');
	 $CouponCode=$this->input->post('CouponCode');
	  $where=array('coupon_code'=>$CouponCode);
      $couponDetails=$this->getSingleRecord(TBL_COUPONS,$where,$select="*");
	  if($CouponCode && count($couponDetails)>0){
	 $amount=$this->input->post('amount');
	 $where=array('coupon_code'=>$CouponCode);
		$couponData['isActive']=0;
		$this->insertOrUpdate(TBL_COUPONS,$where,$couponData);
	
	 if(!$userId){
		redirect(LOGOUT_URL,'refresh');
	  }
		
	   if($packageids){
		  $packageArray=explode(',',$packageids);
		  foreach($packageArray as $packary){
			$pId=$packary;
			$where=array('package_id'=>$pId);
			$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
			$testId=$packageDetails->package_test;
			$where=array();
			$updata['createdTime']= date("Y-m-d H:i:s");
            $updata['userId']=$userId;
			$updata['packageId']=$pId;
			$updata['isActive']= 1;
			$updata['packageTests']=$testId;
			$updata['couponCode']= $CouponCode;
			$updata['amount']= $packageDetails->package_amount;
			$updata['isDeleted']= 0;
			//on 17 th
			$where=array('packageId'=>$pId,'amount'=>0,'txnid'=>0,'userId'=>$userId);
			$check=$this->getSingleRecord(TBL_USER_PACKAGE,$where,$select="*"); 
			if($check){ 
				$where=array('id'=>$check->id);
			}else{
				$where=array();
			}
			//end
			
			$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
				if($results){
				   $where=array('userId'=>$userId,'itemId'=>$pId,'itemType'=>1);
				   $cartData['isActive']=0;
				   $cartData['isDeleted']=1;
				   $results=$this->insertOrUpdate(TBL_CART,$where,$cartData);
				}
        }
	   }
	
	   if($testIds){
		$where=array();
		$updata['createdTime']= date("Y-m-d H:i:s");
        $updata['userId']=$userId;
        $updata['packageId']=0;
        $updata['isActive']= 1;
        $updata['isDeleted']= 0;
        $updata['packageTests']= $testIds;
		$updata['couponCode']= $CouponCode;
		$testAmount=$this->Testsmodel->getSelectedTestsAmount($testIds);
		$testAmount=$testAmount->total;
		$updata['amount']= $testAmount;
		//on 17 th
			$where=array('packageTests'=>$testIds,'amount'=>0,'txnid'=>0,'userId'=>$userId);
			$check=$this->getSingleRecord(TBL_USER_PACKAGE,$where,$select="*"); 
			if($check){
				$where=array('id'=>$check->id);
			}else{
				$where=array();
			}
			//end
        $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
			if($results){
				$tests=explode(',',$testIds);
				foreach($tests as $test){
				   $where=array('userId'=>$userId,'itemId'=>$test,'itemType'=>2);
				   $cartData['isActive']=0;
				   $cartData['isDeleted']=1;
				   $results=$this->insertOrUpdate(TBL_CART,$where,$cartData);
				}
	          }
		 }
		 
		 $data['packageDetails']=0;$data['testDetails']=0;
		if($packageids){
		$data['packageDetails']=$this->Testsmodel->getSelectedPackagesDetails($packageids);
		}
		if($testIds){
			
		$data['testDetails']=$this->Testsmodel->getSelectedTestDetails($testIds);
		}
		$data['txnid']=$this->session->userdata('txnid');
		$where=array('userId'=>$userId);
		$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
		$data['userName']=$details->userName;
		$data['amount']=$this->input->post('amount');
		$subject="Purchase Details";
		$message=$this->load->view('emailDetails', $data,true);
		$smtpEmailSettings = $this->config->item('smtpEmailSettings');
		//$where=array('userId'=>$updata['userId']);
		//$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
				$email_id=$details->emailAddress;
		$isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
        $this->session->set_flashdata('buypackage', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Your Purchase is done successfully.Check your email for details.</strong> </div>");
        redirect(DASHBOARD_URL);
		}else{
		$this->session->set_flashdata('buypackage', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Please enter valid coupon number.</strong> </div>"); 
	    redirect(CART_URL);
		 
	}
}

   function users(){
		$where=array('userId'=>$this->session->userdata('user_id'));
		$result=$this->getAllRecords(TBL_TIMES,$where,$select="*");
		debug($result);
	}
	function change(){
		
		$query="TRUNCATE TABLE `oes_libraries`";
		//$res=$this->db->query($query);
		debug($res);
	}
function viewMoreRecords(){
		    $Ttype = $_GET['type'];
			$offset = $this->input->post('offset');
			$limit = 0;
			$optedTests = $this->Testsmodel->getOptedTests($Ttype,$limit,$offset);
			$data['optedTests']=$optedTests;
		   $this->load->view('header');
		$this->load->view('dashboard_script');
		$this->load->view('viewMoreReports',$data);
		$this->load->view('footer');

		
	}
	//neww on 14 th
	function buyPackThroughPayU(){
	$packageids=$this->session->userdata('packageids'); 
	 $testIds=$this->session->userdata('testIds');
	 $txnid=$this->session->userdata('txnid');
	 $amountt=$this->session->userdata('amountt');
	 $userId=$_GET['userId'];
	 
	
	 if(!$userId){
		redirect(LOGOUT_URL,'refresh');
	  }
		
	   if($packageids){
		  $packageArray=explode(',',$packageids);
		  foreach($packageArray as $packary){
			$pId=$packary;
			$where=array('package_id'=>$pId);
			$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
			$testId=$packageDetails->package_test;
			$where=array();
			$updata['createdTime']= date("Y-m-d H:i:s");
            $updata['userId']=$userId;
			$updata['packageId']=$pId;
			$updata['isActive']= 1;
			$updata['packageTests']=$testId;
			$updata['txnid']= $txnid;
			$updata['amount']= $packageDetails->package_amount;
			$updata['isDeleted']= 0;
			$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
				if($results){
				   $where=array('userId'=>$userId,'itemId'=>$pId,'itemType'=>1);
				   $cartData['isActive']=0;
				   $cartData['isDeleted']=1;
				   $results=$this->insertOrUpdate(TBL_CART,$where,$cartData);
				}
        }
	   }
	
	   if($testIds){
		$where=array();
		$updata['createdTime']= date("Y-m-d H:i:s");
        $updata['userId']=$userId;
        $updata['packageId']=0;
        $updata['isActive']= 1;
        $updata['isDeleted']= 0;
        $updata['packageTests']= $testIds;
		$updata['couponCode']= $CouponCode;
		$testAmount=$this->Testsmodel->getSelectedTestsAmount($testIds);
		$testAmount=$testAmount->total;
		$updata['amount']= $testAmount;
        $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
			if($results){
				$tests=explode(',',$testIds);
				foreach($tests as $test){
				   $where=array('userId'=>$userId,'itemId'=>$test,'itemType'=>2);
				   $cartData['isActive']=0;
				   $cartData['isDeleted']=1;
				   $results=$this->insertOrUpdate(TBL_CART,$where,$cartData);
				}
	          }
		 }
		 
		 $data['packageDetails']=0;$data['testDetails']=0;
		if($packageids){
		$data['packageDetails']=$this->Testsmodel->getSelectedPackagesDetails($packageids);
		}
		if($testIds){
			
		$data['testDetails']=$this->Testsmodel->getSelectedTestDetails($testIds);
		}
		$data['txnid']=$this->session->userdata('txnid');
		$where=array('userId'=>$updata['userId']);
		$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
		$data['userName']=$details->userName;
		$data['amount']=$this->session->userdata('amount');;
		$subject="Purchase Details";
		$message=$this->load->view('emailDetails', $data,true);
		$smtpEmailSettings = $this->config->item('smtpEmailSettings');
		$email_id=$details->emailAddress;
		$isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
        $this->session->set_flashdata('buypackage', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Your Purchase is done successfully.Check your email for details.</strong> </div>");
        redirect(DASHBOARD_URL);
		/* }else{
		$this->session->set_flashdata('buypackage', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Please enter valid coupon number.</strong> </div>"); 
	    redirect(CART_URL);
		 
	} */
}
	function libraries(){ 
            $userId=$this->session->userdata('user_id'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
			$libraries=$this->getLibraries($userId);
			$data['libraries']=$libraries; 
			$where=array('userId'=>$userId);
    	    $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
			$cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
				$data['cartCount']=$cartCount;
			}else{
				$data['cartCount']=0;
			}
		    $this->load->view('header',$data);
			$this->load->view('libraries',$data); 
			$extrafooter = $this->load->view('dashboard_script');
			$this->load->view('footer',$extrafooter);  
	}
	function getLibraries($userId){
		$where=array('userId'=>$userId);	$libraries=array();			
		$records=$this->Testsmodel->getUserPacks(TBL_USER_PACKAGE,$where,$select="*");
		if($records){
			foreach($records as $rec){
				$packageid=$rec->packageId;
				$validSql="SELECT DATEDIFF(package_valid_to,package_valid_from) as diff FROM oes_packages WHERE package_id = $packageid";
				$validityCheck=$this->db->query($validSql)->row();
				//print_r($validityCheck);die();
				if(@$validityCheck->diff){
				           $validityCheck=$validityCheck->diff;
					       $createdTime=$rec->createdTime;
						   $expiryDate = date('Y-m-d h:i:s', strtotime("+$validityCheck days", strtotime($createdTime)));
						   $A=strtotime($expiryDate);$B=strtotime($createdTime);
				if($A >= $B){ 
                 $libraries[]=$this->Testsmodel->getLibraries($packageid);
                   

				}
				}
			}
			//echo "<pre>";print_r($libraries);die();
			
		}
		return $libraries;
	}
	function getLibDetails(){
		$id=$_GET['id'];
		$where=array('id'=>$id);
		$details=$this->getSingleRecord(TBL_LIBRARIES,$where,"*");
		//debug($details);
		$data['details']=$details;
		$this->load->view('viewlibDetails',$data);
		
		
	}
	/* function admin_questionPaper()
	{  
	
	   if($_POST){
		 $userId=$this->session->userdata('userId'); 
		 $userId=1; 
		$attempts=0;$offset=0;
		$package=0;
		$uniqueId=0;$limit=1;
		$examid=$this->input->post('test');
		$type=2;
		$order=$this->input->post('order'); 
		$testType=$this->input->post('testType');
		$where=array('testId'=>$examid);
        $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks,isShuffleQueOrder");
		$isShuffle= $data['testname']->isShuffleQueOrder;
		$ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts); 
		if($ReferenceTestId){
		$data['ReferenceTestIdArray']=$ReferenceTestId;
		$offset=$ReferenceTestId[$order-1];
       
		}
		else{
		  $offset=0;
		}
		 $data['questionNumbers']=$this->Testsmodel->getQuestionNumbers($examid,$isShuffle);
					  $data['ques']=$order;
				      $data['details']=$this->Testsmodel->getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,
					  $uniqueId,$order);
                      $filter_view =$this->load->view('admin_questions_ajax',$data,true); 
                      echo $filter_view;die();
					  
					  //end
		}
	  
	    $time=0; 
		//$queToken=$this->session->userdata('queToken'); 
        $userId=$this->session->userdata('userId'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
		$examid=$this->uri->segment(4);
        $package=$this->uri->segment(6);
        $uniqueId=$this->uri->segment(8);
		$attempts=0;
		$testType=$this->session->userdata('testType');
		
		$where=array('userId'=>$userId);
    	$data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
        $this->load->view('exam_header',$data);
        $examid=$this->uri->segment(4);
        $package=$this->uri->segment(6);
		$where=array('testId'=>$examid);
        $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks,isShuffleQueOrder");
		$isShuffle= $data['testname']->isShuffleQueOrder;
		$limit = 1;
		$offset=0;
        $type=1;
        $data['questionNumbers']=$this->Testsmodel->getQuestionNumbers($examid,$isShuffle);
		if(empty($data['questionNumbers'])){
			if($data['questionNumbers'] == 0){
				$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your unable to attempt test because this test contains no questions.</strong> </div>");
				  echo  "<script type='text/javascript'>";
                  echo "window.close();";
				  echo  "window.opener.location ='http://localhost/oes_live/admin/authority/d/maintest/quetions'";
                  echo "</script>";die();
			}
		}
		$type=2;
		$data['ques']=1;
		 $mainarray=array();
					  if($data['questionNumbers']){
						 foreach($data['questionNumbers'] as $questions){$mainarray[]=$questions->que_id;}
					  }		  
					  $array = array_values($mainarray);
					  $where=array();$table=TBL_QUESTIONS_ORDER;
					  $referenceData['que_id']=implode(',',$array);
					  $referenceData['test_id']=$examid;
					  $referenceData['isActive']=1;
					  $referenceData['user_id']=$userId;
					  $referenceData['userPackage']=$uniqueId;
					  $referenceData['attempts']=$attempts;
					  $res=$this->insertOrUpdate($table,$where,$referenceData); 
					  $offset= $array[0]; $data['ques']=1; 
		    
		  $data['details']=$this->Testsmodel->getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,$uniqueId);
          $ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts);
		  $data['ReferenceTestIdArray']=$ReferenceTestId;
		  if($time){
			 $clock=$this->getTimer($time); 
			 $data['clock']=$clock;
		  }else{
			  $clock=$this->getTimer($data['testname']->testTime);
			  $data['clock']=$clock;
		  }
		  
		  //strat
		 //debug($data);
		  $this->load->view('admin_exam_questions',$data);
		  $this->load->view('footer');
		
		
	} */
	function startAdminExam()
	{
		     
	        $userId=$this->session->userdata('userId'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
		try{
			
			$data = array();
			$type = $this->uri->segment(3);
			if($type=="practicalexam"){ $ty=2; }else{ $ty=1; }
				$examId =$this->uri->segment(5);
				$package = $this->uri->segment(7);
				$uniqueId = $this->uri->segment(9);
				$this->session->set_userdata('testType',$ty);
				$this->session->set_userdata('examId',$examId);
				$this->session->set_userdata('packageiD',$package);
				$this->session->set_userdata('uniqueId',$uniqueId);
			if(!$examId)
			{
				redirect(DASHBOARD_URL,'refresh');
			}
			
			if($ty==1){
			  $where=array('package_id'=>$package);
              $packageName=$this->getSingleRecord(TBL_PACKAGES,$where);
              $data['package_name'] = !empty($packageName)?$packageName->package_name:0;
              $data['package_id'] = $package;
            }
			$where=array('testId'=>$examId,'packageId'=>$package,'userPackageID'=>$uniqueId,'userId'=>$userId,'status'=>TEST_COMPLETED,'isActive'=>1);
			$CompletedTestTimes=$this->getAllRecords(TBL_TIMES,$where);
           //$where=array('testId'=>$examId);
			$testAttempts=$this->getSingleRecord(TBL_TESTS,$where=array('testId'=>$examId),'attempts');
			$testAttempts=$testAttempts->attempts; 
			
			$where=array('testId'=>$examId);
            $data['totalQue'] = $this->Testsmodel->getTotalQueByTest($examId);
			$data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks");
            $data['examId'] = $examId;	
			$data['type'] = $ty;
			$data['uniqueId'] = $uniqueId;
            $where=array('userId'=>$userId);
    	    $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");		
			$this->load->view('exam_header',$data);
			$this->load->view('admin_start_exam',$data);
		 	
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 

	}
	function adminVerify(){
	   
	                $this->load->library('form_validation');
					$this->form_validation->set_rules('isAgree','isAgree','trim|required');
					$url = trim($this->input->post('url'));
					$packageId = trim($this->input->post('packageId'));
				    $examid = trim($this->input->post('examid'));
					$uniqueId = trim($this->input->post('uniqueId'));
					$type = trim($this->input->post('type'));
					$this->session->set_userdata('testType',$type);
					if($this->form_validation->run()!=false)
						{
							 $link= ADMIN_EXAM_URL.'/exam/'.$examid.'/package/'.$packageId.'/uniqueId/'.$uniqueId;
							  redirect($link);
						}
						else 
						{
							$this->session->set_flashdata('otp', " <div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please agree terms and conditions to start exam</strong> </div>");
							$url=SITEURL.'dashboard/startAdminExam/mainexam/test/'.$examid.'/package/'.$packageId.'/uniqueId/'.$uniqueId.'/mode/Admin';
							redirect($url);
							

						}
	}
	function admin_questionPaper()
	{  
	
	   if($_POST){
		 $userId=$this->session->userdata('userId'); 
		
		$attempts=0;$offset=0;
		$package=0;
		$uniqueId=0;$limit=1;
		$examid=$this->input->post('test');$data['examid']=$examid;
		$type=2;
		$order=$this->input->post('order'); 
		$testType=$this->input->post('testType');
		$where=array('testId'=>$examid);
        $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks,isShuffleQueOrder");
		$isShuffle= $data['testname']->isShuffleQueOrder;
		$ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts); 
		if($ReferenceTestId){
		$data['ReferenceTestIdArray']=$ReferenceTestId;
		$offset=$ReferenceTestId[$order-1];
       
		}
		else{
		  $offset=0;
		}
		 $data['questionNumbers']=$this->Testsmodel->getQuestionNumbers($examid,$isShuffle);
					  $data['ques']=$order;
				      $data['details']=$this->Testsmodel->getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,
					  $uniqueId,$order);
                      $filter_view =$this->load->view('admin_questions_ajax',$data,true); 
                      echo $filter_view;die();
					  
					  //end
		}
	  
	    $time=0; 
		//$queToken=$this->session->userdata('queToken'); 
        $userId=$this->session->userdata('userId'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
		$examid=$this->uri->segment(4);
        $package=$this->uri->segment(6);
        $uniqueId=$this->uri->segment(8);
		$attempts=0;
		$testType=$this->session->userdata('testType');
		
		$where=array('userId'=>$userId);
    	$data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
        $this->load->view('exam_header',$data);
        $examid=$this->uri->segment(4);
        $package=$this->uri->segment(6);
		$where=array('testId'=>$examid);
        $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks,isShuffleQueOrder");
		$isShuffle= $data['testname']->isShuffleQueOrder;
		$limit = 1;
		$offset=0;
        $type=1;
        $data['questionNumbers']=$this->Testsmodel->getQuestionNumbers($examid,$isShuffle);
		if(empty($data['questionNumbers'])){
			if($data['questionNumbers'] == 0){
				$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your unable to attempt test because this test contains no questions.</strong> </div>");
				  echo  "<script type='text/javascript'>";
                  echo "window.close();";
				  echo  "window.opener.location ='https://onyx-exams.com/admin/authority/d/maintest/quetions'";
                  echo "</script>";die();
			}
		}
		$type=2;
		$data['ques']=1;
		 $mainarray=array();
					  if($data['questionNumbers']){
						 foreach($data['questionNumbers'] as $questions){$mainarray[]=$questions->que_id;}
					  }		  
					  $array = array_values($mainarray);
					  $where=array();$table=TBL_QUESTIONS_ORDER;
					  $referenceData['que_id']=implode(',',$array);
					  $referenceData['test_id']=$examid;
					  $referenceData['isActive']=1;
					  $referenceData['user_id']=$userId;
					  $referenceData['userPackage']=$uniqueId;
					  $referenceData['attempts']=$attempts;
					  $res=$this->insertOrUpdate($table,$where,$referenceData); 
					  $offset= $array[0]; $data['ques']=1; 
		    
		  $data['details']=$this->Testsmodel->getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,$uniqueId);
          $ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts);
		  $data['ReferenceTestIdArray']=$ReferenceTestId;
		  $clock=$this->getTimer($data['testname']->testTime);
		  $data['clock']=$clock;
		 
		  $this->load->view('admin_exam_questions',$data);
		  $this->load->view('footer');
		
		
	}
	function admin(){
		//debug($_POST);
		 $userId=$this->session->userdata('userId'); 
		 $userId=1; 
		$attempts=0;$offset=0;
		$package=0;
		$uniqueId=0;$limit=1;
		$examid=$this->input->post('test');
		$type=2;
		$order=$this->input->post('order'); 
		$testType=$this->input->post('testType');
		$where=array('testId'=>$examid);
        $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks,isShuffleQueOrder");
		$isShuffle= $data['testname']->isShuffleQueOrder;
		$ReferenceTestId=$this->Testsmodel->getReferenceQuestions($examid,$userId,$uniqueId,$attempts); 
		if($ReferenceTestId){
		$data['ReferenceTestIdArray']=$ReferenceTestId;
		$offset=$ReferenceTestId[$order-1];
       
		}
		else{
		  $offset=0;
		}
					  $data['ques']=$order;
				      $data['details']=$this->Testsmodel->getQuestions($examid,$limit,$offset,$type,$userId,$attempts,$testType,$package,
					  $uniqueId,$order);
                      $filter_view =$this->load->view('admin_questions_ajax',$data,true); 
                      echo $filter_view;die();
		
		
	}
	  function getadminFiftyFiftyOptions()
	 {
		
		 $test=$this->input->post('test');
		 $que=$this->input->post('que');
		 $package=$this->input->post('package');
		 $unique=$this->input->post('unique');
		 $IncorrectAnswers=array();
		 $where=array('que_id'=>$que,'is_answer'=>1,'test_id'=>$test,'isActive'=>1,'isDeleted'=>0);
		 $correctAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*");
		 $correctAnswers=($correctAnswers)?$correctAnswers:array();
		
		 $where=array('que_id'=>$que,'is_answer'=>0,'test_id'=>$test,'isActive'=>1,'isDeleted'=>0);
		 $IncorrectAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*");
		 $IncorrectAnswers=($IncorrectAnswers)?$IncorrectAnswers:array();
		 //new
		 if(empty($correctAnswers) && $IncorrectAnswers){
		  $incorctOption=$IncorrectAnswers[0]->option_id;
		  if(count($correctAnswers)<=0){
			  $correctAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*",$incorctOption);
		  }
		 }
		 if(empty($IncorrectAnswers) && $correctAnswers){
			 $where=array('que_id'=>$que,'is_answer'=>1,'test_id'=>$test,'isActive'=>1,'isDeleted'=>0);
		  $corctOption=$correctAnswers[0]->option_id;
		  if(count($IncorrectAnswers)<=0){
			  $IncorrectAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*",$corctOption);
		  }
		 }
		  $details=array_merge($correctAnswers,$IncorrectAnswers);
		
		 foreach($details as $detail){
			 $optArr[]=$detail->option_id;
		 }
		 $opt_id=implode(',',$optArr);
		 $data['options']=$details;
		 $data['que']=$que;
		 $data['package']=$package;
		if(empty($package)){
			$data['package']=0;
		}
		
		$data['test']=$test;
		$res=$this->load->view('admin_options_ajax',$data,true);
		echo  $res ;die();
		
		 
	 }
	 /* for starting protion exam */
	 
	  function startPromotionExam()
	{
		     
	        $userId=$this->session->userdata('user_id'); 
			if(!$userId){
				redirect(LOGOUT_URL,'refresh');
			}
		try{
			$this->generateOtp();
			$checktoken=$this->session->userdata('checktoken');
			if($checktoken){
			$data = array();
			$type = $this->uri->segment(3);
			  $ty=2; 
				$examId =$this->uri->segment(5);
				$package = $this->uri->segment(7);
				$uniqueId = $this->uri->segment(9);
				$this->session->set_userdata('testType',$ty);
				$this->session->set_userdata('examId',$examId);
				$this->session->set_userdata('packageiD',$package);
				$this->session->set_userdata('uniqueId',$uniqueId);
			if(!$examId)
			{
				redirect(PROMOTER_DASHBOARD_URL,'refresh');
			}
			
			if($package){
			  $where=array('package_id'=>$package);
              $packageName=$this->getSingleRecord(TBL_PACKAGES,$where);
              $data['package_name'] = !empty($packageName)?$packageName->package_name:0;
              $data['package_id'] = $package;
            }
			$where=array('testId'=>$examId,'packageId'=>$package,'userPackageID'=>$uniqueId,'userId'=>$userId,'status'=>TEST_COMPLETED,'isActive'=>1);
			$CompletedTestTimes=$this->getAllRecords(TBL_TIMES,$where);
			$testAttempts=$this->getSingleRecord(TBL_TESTS,$where=array('testId'=>$examId),'attempts');
			$testAttempts=$testAttempts->attempts; 
			if(($CompletedTestTimes )&& (count($CompletedTestTimes)>=$testAttempts)) { print_r('sasa');die();
				$this->session->set_flashdata('examError', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your attempts for this test  was completed.</strong> </div>");
				  echo  "<script type='text/javascript'>";
                  echo "window.close();";
				  echo  "window.opener.location ='http://localhost/ONYX-EXAMS/promotions/dashboard'";
                  echo "</script>";die();
				  redirect(PROMOTER_DASHBOARD_URL,'refresh');
			}
            $user_id=$this->session->userdata('user_id');
			//$RESULT=$this->sentOtp();
			//test details
			$where=array('testId'=>$examId);
            $data['totalQue'] = $this->Testsmodel->getTotalQueByTest($examId);
			if($data['totalQue'] == 0){ 
				$this->session->set_flashdata('examError', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your unable to attempt test because this test contains no questions.</strong> </div>");
				  echo  "<script type='text/javascript'>";
                  echo "window.close();";
				  echo  "window.opener.location ='http://localhost/ONYX-EXAMS/promotions/dashboard'";
                  echo "</script>";die();
				 redirect(PROMOTER_DASHBOARD_URL,'refresh');
			}
            $data['testname']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks");
            $data['examId'] = $examId;	
			$data['type'] = $ty;
			$data['uniqueId'] = $uniqueId;
			if($CompletedTestTimes){
				$atempts=count($CompletedTestTimes);
			}else{$atempts=0;}
			$data['attempts'] =$atempts+1 ;
            $where=array('userId'=>$userId);
    	    $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");		
			$queToken=md5(uniqid());
			$this->session->set_userdata('queToken', $queToken);
			$this->load->view('promotions/exam_header',$data);
			$this->load->view('promotions/start_exam',$data);
		 }
		 else{
		  redirect(NOT_FOUND_URL);
			 }	
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 

	}
	 function promotionVerify(){
	   
	                $this->load->library('form_validation');
					$this->form_validation->set_rules('isAgree','isAgree','trim|required');
					$url = trim($this->input->post('url'));
					$packageId = trim($this->input->post('packageId'));
				    $examid = trim($this->input->post('examid'));
					$uniqueId = trim($this->input->post('uniqueId'));
					if($this->form_validation->run()!=false)
						{
							$packageId = trim($this->input->post('packageId'));
							$examid = trim($this->input->post('examid'));
							$uniqueId = trim($this->input->post('uniqueId'));
							$type = trim($this->input->post('type'));
							$user_id=$this->session->userdata('user_id');
				            $where=array('userID'=>$user_id,'isactive'=>1,'token'=>$otp,'purpose'=>'startExam');
				            $checkvalidOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*");
							$this->session->unset_userdata('token');
								$save=$this->saveExamDetails($examid,$type,$packageId, $uniqueId);
								$this->session->set_userdata('otp','123456');
								
                                $link= EXAM_URL.'/exam/'.$examid.'/package/'.$packageId.'/uniqueId/'.$uniqueId;
								redirect($link);
						}
						else 
						{
							$this->session->set_flashdata('otp', " <div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please enter otp and check terms and conditions to start exam</strong> </div>");
							//$url=SITEURL.'dashboard/startExam/mainexam/test/'.$examid.'/package/'.$packageId.'/uniqueId/'.$uniqueId;
							redirect($_SERVER['HTTP_REFERER']);
							

						}
	}
	

 
/* send request to ccavenue */
	
	function ccavenueRequestHandler()
	{
		if(!$this->session->userdata('user_id')){
			redirect(LOGOUT_URL,'refresh');
		}
		$data=array();
		$merchant_data = CCAVENUE_MERCHANTID;
		$working_key   = CCAVENUE_WORKING_KEY;
		$access_code = CCAVENUE_ACCESS_CODE;
		$packages=@$_POST['packages'];
		$tests=@$_POST['tests'];
		$orderId=rand().time();
		 unset($_POST['Buy_Now']); 
		 if($tests){ 
			$this->saveOrderIdWitests($_POST['userId'],$_POST['tests'],$orderId);
		} 
		if(@$packages){
			$this->saveOrderId($_POST['userId'],$_POST['packages'],$orderId);
		}
		$array['mobile'] =  @$_POST['phone'];
		$array['amount']= @$this->Testsmodel->getTotalCartItemsAmount($_POST['userId']);
		$array['merchant_id'] = CCAVENUE_MERCHANTID;
		$array['order_id'] = $orderId;
		$array['cid'] =  @$_POST['userId'];
		$array['currency'] = "INR";
		$_SESSION["userId"] = @$_POST['userId'];
		$array['redirect_url'] = CCAVENUE_RESPONSE_URL;
		$array['cancel_url'] = CART_URL;
		$array['language'] = "EN";
		$array["billing_country"] = "india";
		$array["billing_tel"] = @$_POST['phone'];
		$array["billing_email"] =  @$_POST['email']; 
		$cnt = count($array);
		$i=0;
		foreach ($array as $key => $value){
			++$i;
			$merchant_data.=$key.'='.$value;
			$merchant_data.=$cnt>$i?'&':'';
				
		}
		$encrypted_data="";
		$encrypted_data=$this->encrypt($merchant_data,$working_key);
		$saveEncData['userID']=$_POST['userId'];
		$saveEncData['request']=$encrypted_data;
		$encId=$this->insertOrUpdate(TBL_CC_CALLBACKS,$where=array(),$saveEncData);
		$this->session->set_userdata('encId', $encId);
		$data['encrypted_data']=$encrypted_data;
		$data['access_code']=$access_code;
		$this->load->view('processCCAvenueRequest',$data);
	}
	
	function saveOrderId($userId,$packageids,$orderId){ 
	
		  if($packageids){ 	
          $packageArray=explode(',',$packageids);
		  foreach($packageArray as $packary){
			$pId=$packary;
			$where=array('package_id'=>$pId);
			$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
			$testId=$packageDetails->package_test;
			$where=array();
			$updata['createdTime']= date("Y-m-d H:i:s");
            $updata['userId']=$userId;
			$updata['packageId']=$pId;
			$updata['isActive']= 0;
			$updata['packageTests']=$testId;
			$updata['ccavenueOrderId']= $orderId;
			$updata['amount']= $packageDetails->package_amount;
			$updata['isDeleted']= 0;
			$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
		}
	  }
	
	  
	}
	function saveOrderIdWitests($userId,$testIds,$orderId){
		 if($testIds){
		$where=array();
		$updata['createdTime']= date("Y-m-d H:i:s");
        $updata['userId']=$userId;
        $updata['packageId']=0;
        $updata['isActive']= 0;
        $updata['isDeleted']= 0;
		$updata['ccavenueOrderId']= $orderId;
        $updata['packageTests']= $testIds;
		$testAmount=$this->Testsmodel->getSelectedTestsAmount($testIds);
		$testAmount=$testAmount->total;
		$updata['amount']= $testAmount;
        $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
		 } 
	}
	/* Handling response from ccavenue */
	function callbackThroughCCAvenue(){
    $workingKey = CCAVENUE_WORKING_KEY;
    $encResponse=$_POST["encResp"];	
	$rcvdString=$this->decrypt($encResponse, $workingKey);
    $saveEncData['response']=$rcvdString;
	$where=array('encId'=>$this->session->userdata('encId'));
	$this->insertOrUpdate(TBL_CC_CALLBACKS,$where,$saveEncData);	
	$order_status="";
    $orderID=0;
	$decryptValues=explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);
	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
		if($i==3)	$order_status=$information[1];
		if($i==0)	$orderID=$information[1];
	}//print_r($orderID);die();
	
	if($order_status=="Success"){
		redirect(CCAVENUE_SUCCESS_URL.'?orderId='.$orderID);
		//redirect(CCAVENUE_SUCCESS_URL);

	}elseif($order_status == "Aborted"){
		 redirect(CCAVENUE_SUCCESS_URL);
		 $this->session->set_flashdata('buypackage', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Your purchase is done successfully.Soonly you will get confirmation mail.</strong> </div>");
	   
	}else{
		redirect(FAILURE);
	}
	 
}
/* update order id status and remoove items from cart */
function saveDataAfterCCavenueSuccess(){ 
	 $userId=$this->session->userdata('user_id');
	  if(!$userId){
		redirect(LOGOUT_URL,'refresh');
	  }
	   $orderId=trim($this->input->get('orderId'));
	   $details=$this->getAllRecords(TBL_USER_PACKAGE,$where=array('ccavenueOrderId'=>$orderId,'isActive'=>0,'isDeleted'=>0));
		if($orderId && $details){ 
        	   
		        $updata['createdTime']= date("Y-m-d H:i:s");
				$updata['isActive']= 1;
				$updata['isDeleted']= 0;
				$packageids="";
				$testIds="";
				$results=array();
		    foreach($details as $detail){
			  if($detail->packageId){ 
			    $userId=$detail->userId;
				$packageids.=$detail->packageId.',';
				$where=array('packageId'=>$detail->packageId,'ccavenueOrderId'=>$orderId);
				$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
			   }else if($detail->packageId == 0){
				  $userId=$detail->userId;
				  $testIds.=$detail->packageTests.',';
			      $where=array('testId'=>$detail->packageTests,'ccavenueOrderId'=>$orderId);
				  $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
			   }
		     }
		         if($results){
					   $where=array('userId'=>$userId);
					   $cartData['isActive']=0;
					   $cartData['isDeleted']=1;
					   $results=$this->insertOrUpdate(TBL_CART,$where,$cartData);
				}
	  
	    $data['packageDetails']=0;$data['testDetails']=0;
		$packageids=rtrim($packageids,','); 
		$testIds=rtrim($testIds,',');
		if($packageids){
		$data['packageDetails']=$this->Testsmodel->getSelectedPackagesDetails($packageids);
		}
		if($testIds){
		$data['testDetails']=$this->Testsmodel->getSelectedTestDetails($testIds);
		}
		$where=array('userId'=>$userId);
		$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
		$data['userName']=$details->userName;
		$data['amount']=$this->session->userdata('amount');;
		$subject="Purchase Details";
		$message=$this->load->view('emailDetails', $data,true);
		$smtpEmailSettings = $this->config->item('smtpEmailSettings');
		$email_id=$details->emailAddress;
		$isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
        $this->session->set_flashdata('buypackage', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Your Purchase is done successfully.Check your email for details.</strong> </div>");
        }else{ 
		$this->session->set_flashdata('buypackage', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Invalid order.</strong> </div>");
         }
		redirect(DASHBOARD_URL);
		
} 
	/* crypto functions */
	
	function encrypt($plainText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
		$plainPad = $this->pkcs5_pad($plainText, $blockSize);
		if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1)
		{
			$encryptedText = mcrypt_generic($openMode, $plainPad);
			mcrypt_generic_deinit($openMode);
			 
		}
		return bin2hex($encryptedText);
	}
	/* to decrypt ccavenue response */
	function decrypt($encryptedText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=$this->hextobin($encryptedText);
		$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
		mcrypt_generic_deinit($openMode);
		return $decryptedText;
	
	}
	//*********** Padding Function *********************
	
	function pkcs5_pad ($plainText, $blockSize)
	{
		$pad = $blockSize - (strlen($plainText) % $blockSize);
		return $plainText . str_repeat(chr($pad), $pad);
	}
	
	//********** Hexadecimal to Binary function for php 4.0 version ********
	
	function hextobin($hexString)
	{
		$length = strlen($hexString);
		$binString="";
		$count=0;
		while($count<$length)
		{
			$subString =substr($hexString,$count,2);
			$packedString = pack("H*",$subString);
			if ($count==0)
			{
				$binString=$packedString;
			}
			 
			else
			{
				$binString.=$packedString;
			}
			 
			$count+=2;
		}
		return $binString;
	}
	/* function getUserPacks(){
		$sql=$this->db->query("SELECT * FROM `oes_user_packages` ORDER BY `oes_user_packages`.`userId` DESC")->result();
		echo"<pre>";print_r($sql);die();
	} */
	
}





?>