<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This class contains the information about User data
 *
 * 
 *
 * @copyright  
 * @license   
 * @version    
 * @link      
 * @since      
 */
class User extends wbcoinscontroller {
	public $swfCharts ;
	public $swfCharts1 ;

	function __construct()
    {     
    	// Call the Model constructor
        parent::__construct();    	   
        $this->load->model('Usermodel');  
		$this->load->model('Visitmodel');	
		$this->load->model('Sportmodel');
		$this->load->model('Sportsubtypemodel');
		$this->load->model('Eventmodel');
		$this->load->model('Transactionmodel');	
		$this->load->model('Historymodel');
		$this->load->model('Notificationmodel');
		$this->load->model('Bettypemodel');
		$this->load->model('Discussionmodel');
		$this->load->model('Commentsmodel');
		$this->load->model('Likemodel');
		$this->load->model('Teammodel');
		$this->load->model('Oddsmodel');
		$this->load->model('Ordermodel');
		$this->load->model('Oddtyperefmodel');
		$this->load->library('fusioncharts') ;
		$this->swfCharts  = 'C:\xampp\htdocs\wbcorders\public\images\Pie3D.swf';
		$this->load->library('mathcaptcha');
		//$this->swfCharts1  = 'http://splink.cria.org.br/fusioncharts/charts/Pie3D.swf';
		
		
    }
public function validateUser()
{
	
    if (preg_match('/^[a-z0-9 .\-_]+$/i', trim($this->input->post('userName'))) )
    {
        return TRUE;
    }
    else
    {
		 $this->form_validation->set_message('validateUser', 'The %s field is not valid!');
        return FALSE;
       
    }
}
function check_math_captcha($str)
	{
		if ($this->mathcaptcha->check_answer($str))
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_math_captcha', 'Enter a valid math captcha response.');
			return FALSE;
		}
	}
	/**
	 * Method to register the new users 
	 *
	 * @return (json)  json data containing the status and message
	 */
	public function register($userName='')
	{
		
		try 
		{
			  $config = array('question_format' => 'numeric','answer_format' => 'numeric');
			  $this->mathcaptcha->init($config);
				
				//Registration
				if(isset($_POST['btnRegisterUser']))	
				{
					if(parent::isValidRequest())
					{
						$this->form_validation->set_rules('userName', 'Username', 'required|min_length[6]|callback_validateUser');
						$this->form_validation->set_rules('userPass', 'Password', 'trim|required|xss_clean|min_length[6]');
						$this->form_validation->set_rules('userEmail', 'Email', 'required|valid_email');
						$this->form_validation->set_rules('userConfirmPass', 'ConfirmPassword', 'required|min_length[6]');
						//$this->form_validation->set_rules('captcha', 'Captcha', 'callback_validateCaptcha');
						$this->form_validation->set_rules('math_captcha', 'Math CAPTCHA', 'required|callback_check_math_captcha');
						
						if ($this->form_validation->run() == TRUE)
						{
							//neatPrintAndDie('dd');
							$isUserExistsAlready = $this->Usermodel->isUserExistsAlready(trim($this->input->post('userEmail')),trim($this->input->post('userName')));
					       //varDumpDie($isUserExistsAlready);
							if($isUserExistsAlready)
							{
							
								$data['errormessage'] = USER_ALREADY_EXISTS;
							}
							else
							{							
								$isUserNameExistsAlready = $this->Usermodel->isUserNameExistsAlready(trim($this->input->post('userName')));

								if($isUserNameExistsAlready)
								{										
									$data['errormessage'] = USER_ALREADY_EXISTS;
								}
								else 
								{
									
									$refferalName = urldecode(trim($this->input->post('referralscore')));
									$username = urldecode(trim($this->input->post('userName')));
									$refferalScore = $refferalName;
									$userEmail = trim($this->input->post('userEmail'));
									$password = trim($this->input->post('userPass'));
									
									if(strlen($refferalName) > 0)
									{
										//neatPrintAndDie($refferalName);
										$isReferralUserNameExistsAlready = $this->Usermodel->isUserNameExistsAlready($refferalName);
										if($isReferralUserNameExistsAlready)
										{											
											$token=md5(uniqid());
											$lastInsertedId = $this->Usermodel->register($username,$refferalScore,$password,$userEmail,$token);
											if($lastInsertedId > 0)
											{
												/*if($refferalName == REGISTRATION_REFERRALNAME)
												{
													$this->Transactionmodel->saveUserTransaction($lastInsertedId,REGISTRATION_REFERRAL_AMOUNT,1,1,0,1);
												}*/
												if(ENABLE_REGISTER_BONOUS == 1)
												{
													$this->Transactionmodel->saveUserTransaction($lastInsertedId,REGISTRATION_REFERRAL_AMOUNT,1,1,0,1);
												}
												$subject="Blockshooter Signup";
												$url=SITEURL;
												
												$html_data['user_name']=$username;
												$verify_url=SITEURL.'/user/VerifyUser/'.$token;
												$html_data['url']=$verify_url;
												$message=$this->load->view('email_templates/signup_user',$html_data,true);
												$smtpEmailSettings = $this->config->item('smtpEmailSettings');
												$isEmailSent = sendNewSmtpEmail($smtpEmailSettings['smtp_user'],$userEmail,$subject,$message,$smtpEmailSettings);
												
												$this->session->unset_userdata('captcha');
												//redirect(SITEURL2.'/dashboard/cashier', 'refresh');
												redirect(SITEURL2, 'refresh');
											}
											else
											{
												$data['errormessage'] = REGISTRATION_FAILED;
											}
										}
										else 
										{
											$data['errormessage'] = NO_REFEERAL_NAME_FOUND;
										}
									}
									else 
									{
										//neatPrintAndDie('dd');
										$token=md5(uniqid());
										$lastInsertedId = $this->Usermodel->register($username,$refferalScore,$password,$userEmail,$token);
										if($lastInsertedId > 0)
										{
											if(ENABLE_REGISTER_BONOUS == 1)
											{
												$this->Transactionmodel->saveUserTransaction($lastInsertedId,REGISTRATION_REFERRAL_AMOUNT,1,1,0,1);
											}
											$subject="Blockshooter Signup";
											$url=SITEURL;
											
											$html_data['user_name']=$username;
											$verify_url=SITEURL.'/user/VerifyUser/'.$token;
											$html_data['url']=$verify_url;
											$message=$this->load->view('email_templates/signup_user',$html_data,true);
											$smtpEmailSettings = $this->config->item('smtpEmailSettings');
											$isEmailSent = sendNewSmtpEmail($smtpEmailSettings['smtp_user'],$userEmail,$subject,$message,$smtpEmailSettings);
											$this->session->unset_userdata('captcha');
											//redirect(SITEURL2.'/dashboard/cashier', 'refresh');
											redirect(SITEURL2, 'refresh');
										}
										else
										{
											$data['errormessage'] = REGISTRATION_FAILED;
										}
										
									}
									
								}
							}	
								
						}
						else
						{
							$data['validation_errors'] = strip_tags(validation_errors());
						}
					}
					else
						{
							$data['errormessage'] = AUTHENTICATION_FAILED;
						}
				}
				else
				{
					//Login
					if(isset($_POST['btnSignIn']))	
					{
						
						if(parent::isValidRequest())
						{
							
							$this->form_validation->set_rules('txtUserName', 'Username', 'required|min_length[6]');
							$this->form_validation->set_rules('txtPassword', 'Password', 'trim|required|xss_clean|min_length[6]');
							if ($this->form_validation->run() == FALSE) {
								$data['validation_errors'] = (validation_errors());
							   
							}else {
								$userName =trim($this->input->post('txtUserName'));
								$password =trim($this->input->post('txtPassword'));
								$userID = $this->Usermodel->authenticateUser($userName,$password);
								
								if($userID > 0)
								{   
							        //Generate and update clientSeed if user does not has 
							        $uData = $this->Usermodel->getUserDetails($userID);
									$clientSeed = $uData->clientSeed;
									if(empty($clientSeed) || $clientSeed == '')
									{
										$rand = generateRandNumber(60);
										$seedData['clientSeed'] =$rand;
										$this->Usermodel->updateUserData($seedData,array('userID'=>$userID));
									}
									//redirect(SITEURL2.'/dashboard', 'refresh');
									$controllerName = getUserProfileBetType();
									
									if(ENABLE_ORDERS)
									{
										//redirect(SITEURL2.'/order', 'refresh');
										redirect(SITEURL2, 'refresh');
									}else
									{
										//redirect(SITEURL2.'/dashboard', 'refresh');
										redirect(SITEURL2, 'refresh');
									}
								}
								else
								{
									$data['errormessage'] = INCORRECT_USERNAME_OR_PASSWORD;
								}
							}
						}
						else
						{
							$data['errormessage'] = AUTHENTICATION_FAILED;
						}
					}	
				}
				

				if(parent::isUserLoggedIn())
				{
					redirect(SITEURL2, 'refresh');
				}
				else
				{
				   
					$data['userName']=$userName;
					$data['catchaImage'] =$this->getCaptchaImage();
					$userID = getCurrentUserID();	
					if($userID && $userID > 0)
					{
						$userBalanceData = getUserAvailableBalanceAndCurrency($userID);
						$data['userSavedCurrencyAvailableBalance']=$userBalanceData['userSavedCurrencyAvailableBalance'];
						$data['userPreferenceCurrency']=$userBalanceData['userPreferenceCurrency'];
					}
					else
					{
						$data['userSavedCurrencyAvailableBalance']=0;
						$data['userPreferenceCurrency']=BTC;
					}
					
					$data['coinTypes']= parent::getCoinTypes();
					//neatPrintAndDie($data['coinTypes']);die();
					$this->Visitmodel->saveVisit(USER_PAGE,$this->router->method );
					$data['token'] =parent::getToken();
					$data['Dashboard'] = $this;
					$data['math_captcha_question'] = $this->mathcaptcha->get_question();
					$data['sportTypes'] = ($this->Sportmodel->getSportTypes());						
					$data['events'] = ($this->Eventmodel->getMostRecentEvents());
					$this->load->view('dashboardheader',$data);
					$this->load->view( 'registersignin',$data);
					$this->load->view('footer',$data);
				}
			
		}
		catch (Exception $exception)
		{
			$this->logExceptionMessage($exception);					
		}
	    
	   
	}
	/**
	 * Method to validate the captcha, returns true if user enters the correct captcha else false
	 *
	 * @param (string) ($loginUsername) username
	 * @param (string) ($loginPassword) user password	 
	 * @return (json)  json data containing the status and message
	 */
	public function validateCaptcha() 
	{	
        if(trim($this->input->post('captcha')) != $this->session->userdata['captcha'])
		{
			$this->form_validation->set_message('validateCaptcha', WRONG_CAPTCHA);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	/**
	 * Method to return the captcha image 
	 *
	 * @return (image) captcha image
	 */
	public function getNewCaptchaImage()
	{
	   if ($this->input->is_ajax_request())
	   {
		 echo parent::getCaptchaImage();
	   }
		else
		{
			return parent::getCaptchaImage();
		}	   
	  
	}
	
	/**
	 * Method to get the user data,if success store the user data in session else return 0 if user doesn't exist in the database
	 *
	 * @param (string) ($loginUsername) username
	 * @param (string) ($loginPassword) user password	 
	 * @return (json)  json data containing the status and message
	 */
	public function login()
	{	
		try 
		{
			$this->form_validation->set_rules('loginUsername', 'Username', 'required|min_length[6]');
			$this->form_validation->set_rules('loginPassword', 'Password', 'trim|required|xss_clean|min_length[6]');
			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array('status'=>'false','msg'=>'ERROR:<br/>'.validation_errors()));
			   
			}else {
				$lastInsertedId = $this->Usermodel->login();
				$status = $lastInsertedId > 0?array("status"=>"true"):array("status"=>"false");
				echo json_encode($status) ; 
			}
		}
		catch (Exception $exception)
		{
			$this->logExceptionMessage($exception);					
		}
	    
	}
	/**
	 * Method to get the  User ID for the loggedIn User
	 *	 
	 * @return (int)  number of effected rows
	 */
	function getCurrentUserID()
	{
		try 
		{
			return getCurrentUserID();
		}
		catch (Exception $exception)
		{
			$this->logExceptionMessage($exception);					
		}
		
	}
	
	/**
	 * Method to check whether user already logged in or not,returns true if success else false
	 *	 
	 * @return (int)  number of effected rows
	 */
	function isUserLoggedIn()
	{
		try 
		{
			if($this->session->userdata('userName') != '')
			{
				return true;
			}
			else
			{
				return false;	
			}
		}
		catch (Exception $exception)
		{
			$this->logExceptionMessage($exception);					
		}
	}
	
	/**
	 * Method to save/update the user preferences 
	 *
	 * @return (int)  last inserted / updated id
	 */
    public function saveOrUpdateUserPreferences()
    {	
		try 
		{	
			$currency = getNumericValue($this->input->post('currency'));
			$userID = getCurrentUserID();
			$result = $this->Usermodel->saveOrUpdateUserPreferences($currency,$userID);
			if($result > 0)
			{
				echo json_encode(array('status'=>'true','message'=>'User preferences saved'));
			}
			else
			{
				echo json_encode(array('status'=>'false','message'=>'Failed in saving user preferences'));
			}
		}
		catch (Exception $exception)
		{
			$this->logExceptionMessage($exception);								
		}
    }

	/**
	 * Method to save the user preferences 
	 *
	 * @return (int)  last inserted id
	 */
    public function saveUserPreferences()
    {	
		try 
		{	
			$currency = getNumericValue($this->input->post('currency'));
			$userID = getCurrentUserID();
			$userBalanceData = getUserAvailableBalanceAndCurrency ($userID);
			
			$data['userPreferenceOddType']=$userBalanceData['userPreferenceOddType'];
			$result = $this->Usermodel->saveUserPreferences($currency,$userID,$data['userPreferenceOddType']);
			if($result > 0)
			{
				echo json_encode(array('status'=>'true','message'=>'User preferences saved'));
			}
			else
			{
				echo json_encode(array('status'=>'false','message'=>'Failed in saving user preferences'));
			}
		}
		catch (Exception $exception)
		{
			$this->logExceptionMessage($exception);								
		}
    }
	/**
	 * Method to reset the user password
	 *
	 * 
	 */
	public function resetPassword($token='')
	{
		try 
		{
			$data='';
			if(!empty($token))
			{
				$token =trim($token);
			}
			$this->form_validation->set_rules('txtNewpassword', 'Password', 'trim|required|xss_clean|min_length[6]|matches[txtConfirmNewPassword]');			
			$this->form_validation->set_rules('txtConfirmNewPassword', 'ConfirmPassword', 'required|min_length[6]');
			 
				if(isset($_POST['btnResetPassword']))	
				{
					
						if ($this->form_validation->run() == TRUE)
						{
							
							$tokenDetails = $this->Usermodel->getTokenDetails(trim($token));
							
							if(count($tokenDetails) > 0)
							{
								
								$userData =array('userPass'=>md5(trim($this->input->post('txtNewpassword'))));
								$this->Usermodel->updateEmailOrPassword($userData,$tokenDetails->userID);
								$this->Usermodel->deactivateResetPassword($tokenDetails->userID);
								$data['successMessage'] = PASSWORD_CHANGED;
							}
							else
							{
								$data['errorMessage'] = INVALID_KEY;
							}	
								
						}
						else
						{
							
							$data['validation_errors'] = validation_errors();
						}
				}
				
				$data['Dashboard'] = $this;
			$data['sportTypes'] = ($this->Sportmodel->getSportTypes());
			//neatPrintAndDie($data['sportTypes']);
			//$data['sportsubtypes'] = ($this->Sportsubtypemodel->getHomePageSportSubTypes());	
			
			if($this->isUserLoggedIn())
			{
					$userBalanceData = $this->getUserAvailableBalanceAndCurrency();
					$data['userSavedCurrencyAvailableBalance']=$userBalanceData['userSavedCurrencyAvailableBalance'];
					$data['userPreferenceCurrency']=$userBalanceData['userPreferenceCurrency'];
					$data['coinTypes']= $this->getCoinTypes();
					$this->Visitmodel->saveVisit(HOME_PAGE,$this->router->method);
					$userID =parent::getCurrentUserID ();
					$data['alerts'] = $this->Notificationmodel->getUserOrderAlerts($userID,$data['userPreferenceCurrency']);
			}
				$this->load->view('dashboardheader',$data);
				//$this->load->view('home/homeheader',$data);
				$this->load->view( 'resetpassword',$data);
				$this->load->view('emptyfooter',$data);
			
		}
		catch (Exception $exception)
		{
			$this->logExceptionMessage($exception);					
		}
	    
	   
	}
	/**
	 * Method to get the user order profile data
	 * @return (object) user order profile data
	 */
	public function profile($userName=NULL) {
		try {
			//varDumpDie($userName);
			if($userName != NULL)
			{
				
				$userID =0;
				$userName = getUrlString($userName);
				$data['userData'] = $userDetails = $this->Usermodel->getUserDetailsByUserName($userName);
				
				if(count($userDetails) > 0 ){
					$userID = $userDetails->userID;
				}
				
	
			}
			else
			{
				//
				$userID = $this->session->userdata('userID');
				$data['userData'] =  $this->Usermodel->getUserDetails($userID);
			}

			
			
			$data ['coinTypes'] = $this->getCoinTypes ();
			//$userID = $this->Usermodel->getCurrentUserID ();
			$data ['token'] = parent::getToken ();
				
			$data ['Dashboard'] = $this;
				
			$data ['sportTypes'] = ($this->Sportmodel->getSportTypes ());
				
			$data ['events'] = ($this->Eventmodel->getMostRecentEvents ());
				
			$data ['bettypes'] = ($this->Bettypemodel->getBetTypes ());
			$data ['orderStatuses'] = ($this->Ordermodel->getOrderStatuses ());
				
			
			//echo 'dd';
			
			$data ['completedOrders'] = ($this->Ordermodel->getUserCompletedOrders($userID));
				
			$data['countries'] = $this->Usermodel->getCountries();			
			$data['states'] = $this->Usermodel->getStates();
			
			$userBalanceData = getUserAvailableBalanceAndCurrency ($this->session->userdata('userID'));
			
			$data ['userSavedCurrencyAvailableBalance'] = $userBalanceData ['userSavedCurrencyAvailableBalance'];
			$data ['userPreferenceCurrency'] = $userBalanceData ['userPreferenceCurrency'];
			$data['userPreferenceOddType']=$userBalanceData['userPreferenceOddType'];
			
			if($userID > 0)
			{
				$data['userPartiallyOrFilledOrdersCount'] = $this->Ordermodel->getUserPartiallyOrFilledOrders($userID,$data ['userPreferenceCurrency']);
			}
			else 
			{
				$data['userPartiallyOrFilledOrdersCount'] =0;
			}
			
			$data ['alerts'] = $this->Notificationmodel->getUserOrderAlerts ( $userID, $data ['userPreferenceCurrency'] );
	
			$where = array (
					'orderUserID' => $userID
			);
			$updateData = array (
					'status' => 1
			);
			$where = array (
					'fromUserID' => $userID
			);
			$this->Notificationmodel->updateNotifications ( $updateData, $where );
	
			
			//varDumpDie($data ['completedOrders']);
			$data ['ordersInProgress'] = ($this->Ordermodel->getUserInCompletedOrders($userID));
			$data ['allorders'] = ($this->Ordermodel->getUserOrders($userID));
			
			//$data ['oneYearOrders'] = ($this->Ordermodel->getUserOrdersWithInOneYear($userID));
			//$data ['ninetydaysOrders'] = ($this->Ordermodel->getUserOrdersWithIn90Days($userID));
			//$data ['thirtydaysOrders'] = ($this->Ordermodel->getUserOrdersWithInOneMonth($userID));
			//$data ['sevendaysOrders'] = ($this->Ordermodel->getUserOrdersWithIn7Days($userID));
			//$data ['twentyfourHourOrders'] = ($this->Ordermodel->getUserOrdersWithIn24Hours($userID));
			$data['followers'] = count($this->Usermodel->getUserFollowers($userID));//getUserFollowers($userID)
			
			
			$data['biggestPick'] =$this->Usermodel->getLargestOrder($userID,$data ['userPreferenceCurrency']);
			
			$data['Ranks'] = $this->Usermodel->getUserRank($userID);
			
			$data['favSport'] = $this->Usermodel->getFavoriteSport($userID);
			
			$data['discussionsCount'] = $this->Discussionmodel->getUserDiscussionsCount($userID);			
			$data['referrals'] = $this->Usermodel->getUserRefferals($userID);
			$data['oddTypes'] = $this->Oddtyperefmodel->getOddTypes();
			
			$loggedInUserID = getLoggedInUserID();
			if($loggedInUserID > 0)			
			$data['isAlreadyFan'] = $this->Usermodel->isFan($userID,getLoggedInUserID());
			else
				$data['isAlreadyFan'] = false;
			
			$data['userID']=$userID;
						
		
			$data['victims'] = $this->Ordermodel->getVictims($userID,$data ['userPreferenceCurrency']);
			
			//$sportsOrders = $this->Ordermodel->getSportByOrdersCount($userID,$data ['userPreferenceCurrency']);
			$j=0;
			
			$sports = $data ['sportTypes'];
			
			foreach ($sports as $sport)
			{
				$sportOrdersData = $this->Ordermodel->getSportOrdersCount($userID,$data ['userPreferenceCurrency'],$sport->sportID);
				$arrData[$j][1] = $sportOrdersData->sportName;
				$arrData[$j][2] = $sportOrdersData->totalOrders;
				$j++;
			}
			
			$homeTeamOrders = $this->Ordermodel->getHomeTeamOrdersCount($userID,$data ['userPreferenceCurrency']);
			$awayTeamOrders = $this->Ordermodel->getAwayTeamOrdersCount($userID,$data ['userPreferenceCurrency']);
			
			$homeAwayOrders[0][1] = "Home";
			$homeAwayOrders[1][1] = "Away";
			
			$homeAwayOrders[0][2] = $homeTeamOrders;
			$homeAwayOrders[1][2] = $awayTeamOrders;
		
			//neatPrintAndDie($this->swfCharts);
			
			//$strXML    	= $this->fusioncharts->setDataXML($arrData,'','') ;
			/*$data['graph'] = $this->fusioncharts->renderChart($this->swfCharts,'',$strXML,"SportwithOrders", 600, 300, false, true) ;
			
			$homeAwayXML    	= $this->fusioncharts->setDataXML($homeAwayOrders,'','') ;
				
			$data['homeAwayGraph'] = $this->fusioncharts->renderChart($this->swfCharts,'',$homeAwayXML,"SportwithOrders1", 600, 300, false, true) ;*/
				//neatPrintAndDie($userID);
				$data['graph']=array();
				$data['homeAwayGraph']=array();
		
				
		} catch ( Exception $exception ) {
			$this->logExceptionMessage ( $exception );
			print_r($exception);die();
		}
		$this->load->view ( 'dashboardheader', $data );
		$this->load->view ( 'profile', $data );
		$this->load->view ( 'footer', $data );
	}
	/**
	 * Method to winning orders count for a given user and sport
	 *
	 * @param (int) ($userID) user ID
	 * @param (int) ($sportID) Sport ID
	 * @return (int) winning orders count
	 */
	public function sportWinOrdersPercentage($userID,$sportID)
	{
		try {
			return $this->Ordermodel->sportWinOrdersPercentage($userID,$sportID);
	
		} catch ( Exception $exception ) {
			$this->logExceptionMessage ( $exception );
		}
	}
    
    /**
     * Method to winning orders count for a given user and sport
     *
     * @param (int) ($userID) user ID
     * @param (int) ($sportID) Sport ID
     * @return (int) winning orders count
     */
    public function sportWinOrdersCount($userID,$sportID)
    {
        try {
            return $this->Ordermodel->sportWinOrdersCount($userID,$sportID);
            
        } catch ( Exception $exception ) {
            $this->logExceptionMessage ( $exception );
        }
    }
    
    public function userRank($userID)
    {
    	try {
    		return $this->Ordermodel->sportWinOrdersCount($userID,$sportID);
    
    	} catch ( Exception $exception ) {
    		$this->logExceptionMessage ( $exception );
    	}
    }
    
    public function sportOrderBetsCount($userID,$sportID)
    {
    	try {
    		return $this->Ordermodel->sportOrderBetsCount($userID,$sportID);
    
    	} catch ( Exception $exception ) {
    		$this->logExceptionMessage ( $exception );
    	}
    }
	/**
	 * Method to lost orders count for a given user and sport
	 *
	 * @param (int) ($userID) user ID
	 * @param (int) ($sportID) Sport ID
	 * @return (int) lost orders count
	 */
	public function sportLostOrders($userID,$sportID)
	{
		try {
			return $this->Ordermodel->sportLostOrders($userID,$sportID);
	
		} catch ( Exception $exception ) {
			$this->logExceptionMessage ( $exception );
		}
	}
	
	/**
	 * Method to get the lost orders for a given userID
	 *
	 * @param (int) ($userID) userID
	 * @param (int) ($coinType) coin type
	 * @param (int) ($sportID) sportID
	 * @return (object)
	 */
	public function getUserSportLostOrders($userID,$coinType,$sportID)
	{
		try {
			return $this->Ordermodel->getUserSportLostOrders($userID,$coinType,$sportID);
	
		} catch ( Exception $exception ) {
			$this->logExceptionMessage ( $exception );
		}
	}
	/**
	 * Method to get the lost orders for a  opposite users
	 *
	 * @param (int) ($userID) userID
	 * @param (int) ($coinType) coin type
	 * @param (int) ($sportID) sportID
	 * @return (object)
	 */
	public function getOppositeUserSportLostOrders($userID,$coinType,$betOrderID)
	{
		try {
			return $this->Ordermodel->getOppositeUserSportLostOrders($userID,$coinType,$betOrderID);
	
		} catch ( Exception $exception ) {
			$this->logExceptionMessage ( $exception );
		}
	}
	public function VerifyUser($token='')
	{
		
		if(!empty($token))
		{
			$token =trim($token);
		}
		else {
			$data['errorMessage']='Please pass valid authenication code to veriy the user';
		}
		
		$tokenDetails = $this->Usermodel->isValidToken(trim($token));
			
		if(count($tokenDetails) > 0)
		{
			$this->session->set_userdata(array(
					'userName'       => $tokenDetails->userName,
					'Administrator'   => '',
					'userID' => $tokenDetails->userID
			));
			
			$this->Usermodel->UpdateUserVerified(trim($token));
			$data['successMessage']=' Thanks , Your email has been verified';
		}
		else {
			$data['errorMessage']='Not a valid user';
		}
		$data['Dashboard'] = $this;
		$data ['coinTypes'] = $this->getCoinTypes ();
		$data ['Dashboard'] = $this;
		
		$data ['sportTypes'] = ($this->Sportmodel->getSportTypes ());
		
		$data ['events'] = ($this->Eventmodel->getMostRecentEvents ());
		
		$data ['bettypes'] = ($this->Bettypemodel->getBetTypes ());
		$userBalanceData = $this->getUserAvailableBalanceAndCurrency();
		$data['userSavedCurrencyAvailableBalance']=$userBalanceData['userSavedCurrencyAvailableBalance'];
		$data['userPreferenceCurrency']=$userBalanceData['userPreferenceCurrency'];
		$this->load->view('dashboardheader',$data);
		$this->load->view('verify',$data);
		$this->load->view('footer');
	}
	public function settings()
	{
		try
		{
			if(parent::isUserLoggedIn())
			{
				$userID = $this->session->userdata('userID');
				//Save User Name
				if(isset($_POST['btnSaveUserName']))
				{
					$this->form_validation->set_rules('newUserName', 'Username', 'required|min_length[6]');
					if ($this->form_validation->run() == FALSE) {
						//$data['errorMessage'] = validation_errors();
						$data['errorMessage'] = array('errorMessage'=>validation_errors(),'flag'=>1);
					}else {
						$userName = $this->input->post('newUserName');
	
	
						if(!empty($userName))
						{
							//$isUserNameAlreadyExists = $this->Usermodel->isUserNameAlreadyExists($userName);
							$isUserNameAlreadyExists = $this->Usermodel->isChatUserNameAlreadyExists($userName);
							if(!$isUserNameAlreadyExists)
							{
								$where = array('userID'=>$userID);
								$data = array('chatUserName'=>$userName); 
								$result = $this->Usermodel->updateUserData($data,$where);
								if($result)
								{
									$data['successMessage'] =SUCCESSFULLY_SAVED;
								}
								else {
									$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>1);
								}
							}
							else
							{
								$data['errorMessage'] = array('errorMessage'=>USERNAME_ALREADY_EXISTS,'flag'=>1);
							}
								
						}
					}
						
						
				}
				//Save User Email
				if(isset($_POST['btnSaveEmail']))
				{
					$this->form_validation->set_rules('newUserEmail', 'UserEmail', 'required|valid_email');
					if ($this->form_validation->run() == FALSE) {
						$data['errorMessage'] = array('errorMessage'=>validation_errors(),'flag'=>2);
					}else {
						$userEmail = $this->input->post('newUserEmail');
	
	
						if(!empty($userEmail))
						{
							$isUserEmailAlreadyExists = $this->Usermodel->isUserExistsAlready($userEmail);
							if(!$isUserEmailAlreadyExists)
							{
								$where = array('userID'=>$userID);
								$data = array('userEmail'=>$userEmail);
								$result = $this->Usermodel->updateUserData($data,$where);
								if($result)
								{
									$data['successMessage'] =SUCCESSFULLY_SAVED;
								}
								else {
									$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>2);
								}
							}
							else
							{
								$data['errorMessage'] = array('errorMessage'=>EMAIL_ALREADY_EXISTS,'flag'=>2);
							}
	
						}
					}
	
	
				}
				
				if (isset ( $_POST ['btnSaveCoinPreference'] )) {
					if (parent::isValidRequest ()) {
						$currency = getNumericValue ( $this->input->post ( 'currencyPref' ) );
						//neatPrintAndDie($currency);
						//$currency = $currency == 0 ? 1 : $currency;
						$userID = $this->getCurrentUserID ();
						$result = $this->Usermodel->saveUserPreferences ( $currency, $userID );
						if ($result > 0) {
							$data ['statusMessage'] = 'Success';
						} else {
							$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>5);
						}
					} else {
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED);
						//$data ['errorMessage'] = AUTHENTICATION_FAILED;
					}
				}

				//When user clicks on Update Password
				if(isset($_POST['btSavePassword']))
				{
					if(parent::isValidRequest())
					{
						$this->form_validation->set_rules('userPass', 'Password', 'required|min_length[6]')->set_rules('newPassword', 'New Password', 'required|min_length[6]|matches[confirmNewPassword]')->set_rules('confirmNewPassword', 'Confirm Password', 'required|min_length[6]');
							
						if ($this->form_validation->run() == TRUE)
						{
							$currentPassword = trim($_POST['userPass']);
							$newPassword = trim($_POST['newPassword']);
							$encryptedPassword =md5($currentPassword);
	
							$confirmNewPassword = trim($_POST['confirmNewPassword']);
							$encryptedNewPassword =md5($newPassword);
							$userDetails = $this->Usermodel->getUserDetails($userID);
							if($encryptedPassword == $userDetails->userPass)
							{
								$pass_counter = 0;
								$data = array('userPass'=>$encryptedNewPassword);
								$pass_update = $this->Usermodel->updateEmailOrPassword($data,$userID);
								if($pass_update)
									$pass_counter++;
								if($pass_counter > 0)
								{
									$data['successMessage'] = SUCCESSFULLY_SAVED;
								}
							}
							else
							{
								$data['errorMessage'] = array('errorMessage'=>INCORRECT_PASSWORD,'flag'=>3);
								
							}
						}
						else
						{
							
							$data['errorMessage'] = array('errorMessage'=>validation_errors(),'flag'=>3);
						}
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>3);
					}
				}
				//Update Country
				if(isset($_POST['btnSaveCountry']))
				{
					if(parent::isValidRequest())
					{
						$country = $this->input->post('ddlCountry');
						//varDumpDie($country);
						if($country > 0)
						{
							$state = $this->input->post('ddlState');
							$city = $this->input->post('city');
							$where = array('userID'=>$userID);
							$data = array('city'=>$city,'stateID'=>$state,'countryID'=>$country);
							$result = $this->Usermodel->updateUserData($data,$where);
							if($result)
							{
								$data['successMessage'] =SUCCESSFULLY_SAVED;
							}
							else {
								
								$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>4);
							}
						}
						else
						{
							$data['errorMessage'] = array('errorMessage'=>SELECT_COUNTRY,'flag'=>4);
						}
	
	
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>4);
					}
				}
				if(isset($_POST['btnSaveCoin']))
				{
					if(parent::isValidRequest())
					{
						$currency = getNumericValue($this->input->post('rdCoinType'));
							
						//$userID = $this->session->userdata('userID');
							
						$userBalanceData = getUserAvailableBalanceAndCurrency($userID);
							
						//$data['userPreferenceCurrency']=$userBalanceData['userPreferenceCurrency'];
						$userPreferenceOddType=$userBalanceData['userPreferenceOddType'];
						//neatPrintAndDie($userPreferenceOddType);
						$result = $this->Usermodel->saveUserPreferences($currency,$userID,$userPreferenceOddType);
						if($result)
						{
							$data['successMessage'] =SUCCESSFULLY_SAVED;
						}
						else {
							
							$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>5);
						}
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>5);
					}
				}
				//Save Privacy Settings
				if(isset($_POST['btnSavePrivacySettings']))
				{
					if(parent::isValidRequest())
					{
						$privacySetting = getNumericValue($this->input->post('rdPrivacySetting'));
						$where = array('userID'=>$userID);
						$data = array('privacySettingID'=>$privacySetting);
	
						$result = $this->Usermodel->updateUserData($data,$where);
						if($result)
						{
							$data['successMessage'] =SUCCESSFULLY_SAVED;
						}
						else {
							
							$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>6);
						}
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>6);
					}
				}
	
				//Save OddType settings
				if(isset($_POST['btnSaveOddTypePref']))
				{
	
					if(parent::isValidRequest())
					{
						$userBalanceData = getUserAvailableBalanceAndCurrency($userID);
						$data['userSavedCurrencyAvailableBalance']=$userBalanceData['userSavedCurrencyAvailableBalance'];
						$userPreferenceCurrency=$userBalanceData['userPreferenceCurrency'];
						//$data['userPreferenceOddType']=$userBalanceData['userPreferenceOddType'];
						$userPreferenceOddType=$this->input->post('rdOddType');
						//neatPrintAndDie($userPreferenceOddType);
						$result = $this->Usermodel->saveUserPreferences($userPreferenceCurrency,$userID,$userPreferenceOddType);
						if($result)
						{
							$data['successMessage'] = SUCCESSFULLY_SAVED;
						}
						else
						{
							
							$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>7);
						}
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>7);
					}
	
	
				}
				//Upload Image
				//Save OddType settings
				if(isset($_POST['btnSaveProfileImage']))
				{
	
					if(parent::isValidRequest())
					{
						$folder = "./uploads/";
						if (is_uploaded_file($_FILES['file']['tmp_name']))  {
							$temp = explode(".",$_FILES["file"]["name"]);
							$newFileName =$userID.'.'.end($temp);
							if (move_uploaded_file($_FILES['file']['tmp_name'], $folder.$newFileName)) {
								$where = array('userID'=>$userID);
								$data = array('imageUrl'=>SITEURL.'/uploads/'.$newFileName);
									
								$result = $this->Usermodel->updateUserData($data,$where);
								if($result)
								{									
									$data['successMessage'] =SUCCESSFULLY_SAVED;
								}
								else {
									
									$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>8);
								}
							} else {
								
								$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>8);
							};
						} else {
							
							$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>8);
						};
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>8);
					}
				}
	
				//Save Fav Sport
				if(isset($_POST['btnSaveSport']))
				{
					if(parent::isValidRequest())
					{
						$favSportID = getNumericValue($this->input->post('ddlFavSport'));
						if(isNumericAndGreaterThanZero($favSportID))
						{
							$where = array('userID'=>$userID);
							$data = array('favoriteSportID'=>$favSportID);
								
							$result = $this->Usermodel->updateUserData($data,$where);
							if($result)
							{
								$data['successMessage'] =SUCCESSFULLY_SAVED;
							}
							else {
								
								$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>9);
							}
								
						}
						else {
							
							$data['errorMessage'] = array('errorMessage'=>SELECT_SPORT,'flag'=>9);
						}
	
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>9);
					}
				}
				//Save Fav Sport
				if(isset($_POST['btnSaveBetType']))
				{
					if(parent::isValidRequest())
					{
						$betType = getNumericValue($this->input->post('rdnBetType'));
						if(isNumericAndGreaterThanZero($betType))
						{
							$where = array('userID'=>$userID);
							$data = array('betTypeID'=>$betType);
				
							$result = $this->Usermodel->updateUserData($data,$where);
							if($result)
							{
								$data['successMessage'] =SUCCESSFULLY_SAVED;
								$this->session->set_userdata('betTypeID', $betType);
							}
							else {
				
								$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>5);
							}
				
						}
						else {
								
							$data['errorMessage'] = array('errorMessage'=>SELECT_BET_TYPE,'flag'=>5);
						}
				
					}
					else
					{
				
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>5);
					}
				}
	
				
				$data['oddTypes'] = $this->Usermodel->getOddTypes();
				
				$data['countries'] = $this->Usermodel->getCountries();
				
				$data['privacySettings'] =$this->Usermodel->getPrivacySettings();
				$data['userData'] = $this->Usermodel->getUserDetails($userID);
				
				
				
				$userBalanceData =getUserAvailableBalanceAndCurrency($userID);
				$data['userSavedCurrencyAvailableBalance']=$userBalanceData['userSavedCurrencyAvailableBalance'];
				$data['userPreferenceCurrency']=$userBalanceData['userPreferenceCurrency'];
				$data['userPreferenceOddType']=$userBalanceData['userPreferenceOddType'];
				//neatPrintAndDie($userID);
				if($data['userData']->favoriteSportID > 0)
				{
				$data['lostOrders'] = $this->Ordermodel->getUserSportLostOrders($userID,$data['userPreferenceCurrency'],$data['userData']->favoriteSportID);
				}
				else
				{
					$data['lostOrders'] = NULL;
				}
				//neatPrintAndDie($lostOrders);
				
				$data['coinTypes']= $this->getCoinTypes();
				
				if(count($data['userData']) > 0)
				$this->session->set_userdata(array('userName'=> $data['userData']->userName));
				
				$data['alerts'] = $this->Notificationmodel->getUserOrderAlerts($userID,$data['userPreferenceCurrency']);
					
	
				$data['captchImage'] = parent::getCaptchaImage();
				if(isset($_POST['SubmitTicket']))
				{
					$data['token'] =$this->session->userdata('csrf_test_name');
	
				} else{
					$data['token'] = parent::getToken();
				}
				$data['Dashboard'] = $this;
	
				$data['sportTypes'] = ($this->Sportmodel->getSportTypes());
				//neatprintAndDie($data['sportTypes']);
				$data['events'] = ($this->Eventmodel->getMostRecentEvents());
				$data['bettypes'] = ($this->Bettypemodel->getBetTypes());
				
			}
			else
			{
				redirect(SITEURL2, 'refresh');
			}
		}
		catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);
		}
	
	
		$this->load->view('dashboardheader',$data);
		$this->load->view( 'settings',$data);
		$this->load->view('footer',$data);
		$this->Visitmodel->saveVisit(SETTINGS_PAGE,$this->router->method);
	}
	public function wbcsettings()
	{
		try
		{
			if(parent::isUserLoggedIn())
			{
				$userID = $this->session->userdata('userID');
				
	
				//Save OddType settings
				if(isset($_POST['btnSaveOddTypePref']))
				{
	
					if(parent::isValidRequest())
					{
						$userBalanceData = getUserAvailableBalanceAndCurrency($userID);
						$data['userSavedCurrencyAvailableBalance']=$userBalanceData['userSavedCurrencyAvailableBalance'];
						$userPreferenceCurrency=$userBalanceData['userPreferenceCurrency'];
						//$data['userPreferenceOddType']=$userBalanceData['userPreferenceOddType'];
						$userPreferenceOddType=$this->input->post('rdOddType');
						//neatPrintAndDie($userPreferenceOddType);
						$result = $this->Usermodel->saveUserPreferences($userPreferenceCurrency,$userID,$userPreferenceOddType);
						if($result)
						{
							$data['successMessage'] = SUCCESSFULLY_SAVED;
						}
						else
						{
							
							$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>7);
						}
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>7);
					}
	
	
				}
				
	
				//Save Fav Sport
				if(isset($_POST['btnSaveSport']))
				{
					if(parent::isValidRequest())
					{
						$favSportID = getNumericValue($this->input->post('ddlFavSport'));
						if(isNumericAndGreaterThanZero($favSportID))
						{
							$where = array('userID'=>$userID);
							$data = array('favoriteSportID'=>$favSportID);
								
							$result = $this->Usermodel->updateUserData($data,$where);
							if($result)
							{
								$data['successMessage'] =SUCCESSFULLY_SAVED;
							}
							else {
								
								$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>9);
							}
								
						}
						else {
							
							$data['errorMessage'] = array('errorMessage'=>SELECT_SPORT,'flag'=>9);
						}
	
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>9);
					}
				}
				//Save Fav Sport
				if(isset($_POST['btnSaveBetType']))
				{
					if(parent::isValidRequest())
					{
						$betType = getNumericValue($this->input->post('rdnBetType'));
						if(isNumericAndGreaterThanZero($betType))
						{
							$where = array('userID'=>$userID);
							$data = array('betTypeID'=>$betType);
				
							$result = $this->Usermodel->updateUserData($data,$where);
							if($result)
							{
								$data['successMessage'] =SUCCESSFULLY_SAVED;
								$this->session->set_userdata('betTypeID', $betType);
							}
							else {
				
								$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>5);
							}
				
						}
						else {
								
							$data['errorMessage'] = array('errorMessage'=>SELECT_BET_TYPE,'flag'=>5);
						}
				
					}
					else
					{
				
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>5);
					}
				}
	
				
				$data['oddTypes'] = $this->Usermodel->getOddTypes();
				
				$data['countries'] = $this->Usermodel->getCountries();
				
				$data['privacySettings'] =$this->Usermodel->getPrivacySettings();
				$data['userData'] = $this->Usermodel->getUserDetails($userID);
				
				
				
				$userBalanceData =getUserAvailableBalanceAndCurrency($userID);
				$data['userSavedCurrencyAvailableBalance']=$userBalanceData['userSavedCurrencyAvailableBalance'];
				$data['userPreferenceCurrency']=$userBalanceData['userPreferenceCurrency'];
				$data['userPreferenceOddType']=$userBalanceData['userPreferenceOddType'];
				//neatPrintAndDie($userID);
				if($data['userData']->favoriteSportID > 0)
				{
				$data['lostOrders'] = $this->Ordermodel->getUserSportLostOrders($userID,$data['userPreferenceCurrency'],$data['userData']->favoriteSportID);
				}
				else
				{
					$data['lostOrders'] = NULL;
				}
				//neatPrintAndDie($lostOrders);
				$data['favoriteSportID']=$data['userData']->favoriteSportID;
				$data['coinTypes']= $this->getCoinTypes();
				
				if(count($data['userData']) > 0)
				$this->session->set_userdata(array('userName'=> $data['userData']->userName));
				
				$data['alerts'] = $this->Notificationmodel->getUserOrderAlerts($userID,$data['userPreferenceCurrency']);
					
	
				$data['captchImage'] = parent::getCaptchaImage();
				if(isset($_POST['SubmitTicket']))
				{
					$data['token'] =$this->session->userdata('csrf_test_name');
	
				} else{
					$data['token'] = parent::getToken();
				}
				$data['Dashboard'] = $this;
	
				$data['sportTypes'] = ($this->Sportmodel->getSportTypes());
				//neatprintAndDie($data['sportTypes']);
				$data['events'] = ($this->Eventmodel->getMostRecentEvents());
				$data['bettypes'] = ($this->Bettypemodel->getBetTypes());
				
			}
			else
			{
				redirect(SITEURL2, 'refresh');
			}
		}
		catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);
		}
	
	
		$this->load->view('dashboardheader',$data);
		$this->load->view( 'wbcsettings',$data);
		$this->load->view('footer',$data);
		$this->Visitmodel->saveVisit(SETTINGS_PAGE,$this->router->method);
	}
public function coinsettings()
	{
		try
		{
			if(parent::isUserLoggedIn())
			{
				$userID = $this->session->userdata('userID');
				
	//Save User Name
				if(isset($_POST['btnSavePin']))
				{
					$userData = $this->Usermodel->getUserDetails($userID);
					if ($userData && md5($_POST['password']) != $userData->userPass) {
						//$data['errorMessage'] = validation_errors();
						$data['errorMessage'] = array('errorMessage'=>'Please enter correct password to change the pin','flag'=>1);
					}else if ($userData && !empty($userData->pin) && (($_POST['pin']) != $userData->pin)) {

						//$data['errorMessage'] = validation_errors();
						$data['errorMessage'] = array('errorMessage'=>'Please enter correct pin','flag'=>1);
					}else {
						$pin = $this->input->post('newpin');
	
	
						if(!empty($pin))
						{
							
								$where = array('userID'=>$userID);
								$data = array('pin'=>$pin);
								$result = $this->Usermodel->updateUserData($data,$where);
								if($result)
								{
									$data['successMessage'] =PIN_SUCCESSFULLY_UPDATED;
								}
								else {
									$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>1);
								}
							
								
						}
					}
						
						
				}

				if(isset($_POST['btnSaveCoin']))
				{
					if(parent::isValidRequest())
					{
						$currency = getNumericValue($this->input->post('rdCoinType'));
							
						//$userID = $this->session->userdata('userID');
							
						$userBalanceData = getUserAvailableBalanceAndCurrency($userID);
							
						//$data['userPreferenceCurrency']=$userBalanceData['userPreferenceCurrency'];
						$userPreferenceOddType=$userBalanceData['userPreferenceOddType'];
						//neatPrintAndDie($userPreferenceOddType);
						$result = $this->Usermodel->saveUserPreferences($currency,$userID,$userPreferenceOddType);
						if($result)
						{
							$data['successMessage'] =SUCCESSFULLY_SAVED;
						}
						else {
							
							$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>5);
						}
					}
					else
					{
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>5);
					}
				}
				if(isset($_POST['btnSaveClientseed']))
				{
                     if(parent::isValidRequest())
					 { 
                          $userID = $this->session->userdata('userID');
                          $clientSeed = $_POST['clientseed'];
                          $data['clientSeed'] =  $clientSeed;
                          $where = array('userID'=>$userID);
						  $result = $this->Usermodel->updateUserData($data,$where);
								if($result)
								{
									$data['successMessage'] = "Successfully updated.";
								}
								else {
									$data['errorMessage'] = array('errorMessage'=>FAILED_SAVED,'flag'=>1);
								}
                     }
                     else
					 {
						
						$data['errorMessage'] = array('errorMessage'=>AUTHENTICATION_FAILED,'flag'=>5);
					 }
                }
				
				$data['oddTypes'] = $this->Usermodel->getOddTypes();
				
				$data['countries'] = $this->Usermodel->getCountries();
				
				$data['privacySettings'] =$this->Usermodel->getPrivacySettings();
				$data['userData'] = $this->Usermodel->getUserDetails($userID);
				
				
				
				$userBalanceData =getUserAvailableBalanceAndCurrency($userID);
				$data['userSavedCurrencyAvailableBalance']=$userBalanceData['userSavedCurrencyAvailableBalance'];
				$data['userPreferenceCurrency']=$userBalanceData['userPreferenceCurrency'];
				$data['userPreferenceOddType']=$userBalanceData['userPreferenceOddType'];
				//neatPrintAndDie($userID);
				if($data['userData']->favoriteSportID > 0)
				{
				$data['lostOrders'] = $this->Ordermodel->getUserSportLostOrders($userID,$data['userPreferenceCurrency'],$data['userData']->favoriteSportID);
				}
				else
				{
					$data['lostOrders'] = NULL;
				}
				//neatPrintAndDie($lostOrders);
				$data['favoriteSportID']=$data['userData']->favoriteSportID;
				$data['coinTypes']= $this->getCoinTypes();
				
				if(count($data['userData']) > 0)
				$this->session->set_userdata(array('userName'=> $data['userData']->userName));
				
				$data['alerts'] = $this->Notificationmodel->getUserOrderAlerts($userID,$data['userPreferenceCurrency']);
					
	
				$data['captchImage'] = parent::getCaptchaImage();
				if(isset($_POST['SubmitTicket']))
				{
					$data['token'] =$this->session->userdata('csrf_test_name');
	
				} else{
					$data['token'] = parent::getToken();
				}
				$data['Dashboard'] = $this;
	
				$data['sportTypes'] = ($this->Sportmodel->getSportTypes());
				//neatprintAndDie($data['sportTypes']);
				$data['events'] = ($this->Eventmodel->getMostRecentEvents());
				$data['bettypes'] = ($this->Bettypemodel->getBetTypes());
				
			}
			else
			{
				redirect(SITEURL2, 'refresh');
			}
		}
		catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);
		}
	
	
		$this->load->view('dashboardheader',$data);
		$this->load->view( 'coinsettings',$data);
		$this->load->view('footer',$data);
		$this->Visitmodel->saveVisit(SETTINGS_PAGE,$this->router->method);
	}
	
	/**
	 * Method to get the user fans data
	 * @return (object) user fans data
	 */
	public function fans($userName=NULL) {
		try {
			//varDumpDie($userName);
			if($userName != NULL)
			{
			
				$userID =0;
				$userName = getUrlString($userName);
				$data['userData'] = $userDetails = $this->Usermodel->getUserDetailsByUserName($userName);
			
				if(count($userDetails) > 0 ){
					$userID = $userDetails->userID;
				}
			
			
			}
			else
			{
				//
				$userID = $this->session->userdata('userID');
				$data['userData'] =  $this->Usermodel->getUserDetails($userID);
			}
			if($userID==0)
			{
				//$userID = $this->session->userdata('userID');
			}
				
				
			$data ['coinTypes'] = $this->getCoinTypes ();
			//$userID = $this->Usermodel->getCurrentUserID ();
			$data ['token'] = parent::getToken ();
	
			$data ['Dashboard'] = $this;
	
			$data ['sportTypes'] = ($this->Sportmodel->getSportTypes ());
	
			$data ['events'] = ($this->Eventmodel->getMostRecentEvents ());
	
			$data ['bettypes'] = ($this->Bettypemodel->getBetTypes ());
			
			if($userID > 0)
			{
				$data['fans'] = $this->Usermodel->getUserFollowers($userID);
				$userBalanceData = getUserAvailableBalanceAndCurrency ($userID);
				//neatPrintAndDie($userBalanceData);
				$data ['userSavedCurrencyAvailableBalance'] = $userBalanceData ['userSavedCurrencyAvailableBalance'];
				$data ['userPreferenceCurrency'] = $userBalanceData ['userPreferenceCurrency'];
				$data['userPreferenceOddType']=$userBalanceData['userPreferenceOddType'];
				$data ['alerts'] = $this->Notificationmodel->getUserOrderAlerts ( $userID, $data ['userPreferenceCurrency'] );
				$where = array (
						'orderUserID' => $userID
				);
				$updateData = array (
						'status' => 1
				);
				$where = array (
						'fromUserID' => $userID
				);
				$this->Notificationmodel->updateNotifications ( $updateData, $where );
			}
			else
			{
				$data['fans'] = NULL;
				$data ['userSavedCurrencyAvailableBalance'] = 0;
				$data ['userPreferenceCurrency'] =0;
				$data['userPreferenceOddType']=0;
				$data ['alerts'] = NULL;
			}
			
			
	
			
	
	
			
			$data['oddTypes'] = $this->Oddtyperefmodel->getOddTypes();
				
			
	
	
		} catch ( Exception $exception ) {
			$this->logExceptionMessage ( $exception );
		}
		
		$this->load->view ( 'dashboardheader', $data );
		$this->load->view ( 'fans', $data );
		$this->load->view ( 'footer', $data );
	}
	/******Generate Client Seed *******/ 
	function generateRand()
	{
		$rand = generateRandNumber(60);
		echo  $rand; die();
	}
}

