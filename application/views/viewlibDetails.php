<html>
<style>
.hidebtn {
    width: 68px;
    height: 57px;
    background: #BDBDBD!important;
    position: absolute;
    right: 18px;
    top: 20px;
}
</style>
 <script src="<?php echo JS_URL; ?>jquery.min.js"></script>
<body>
 <?php  if($details){   
 if($details->fileType == 'pdf'){ 
$doc_url=LIBRARYP_PATH.$details->library_file;
$doc_url=urlencode($doc_url);
$url="https://docs.google.com/viewer?url=$doc_url&embedded=true";
?>

<div class="hidebtn">&nbsp;</div>
<!---<iframe src='<?php echo $url; ?>' frameborder='0' scrolling='no' height="100%" width="100%" style="pointer-events:none;" >Module  Brochure</iframe>-->
<iframe src='<?php echo $url; ?>' frameborder='0' scrolling='no' height="100%" width="100%">Module  Brochure</iframe>

<?php }
else if($details->fileType == 'text'){  
$doc_url=LIBRARYP_PATH.$details->library_file;
$doc_url=urlencode($doc_url);
$url="https://docs.google.com/viewer?url=$doc_url&embedded=true";?>
<div class="hidebtn">&nbsp;</div>
<iframe src="<?php echo $url; ?>" height="100%" width="100%" >
</iframe>

<?php } else if($details->fileType == 'doc' ){
$doc_url=LIBRARYP_PATH.$details->library_file;
$doc_url=urlencode($doc_url);
$url="https://docs.google.com/viewer?url=$doc_url&embedded=true";
?>
<div class="hidebtn">&nbsp;</div>
<iframe src='<?php echo $url; ?>' frameborder='0'  scrolling='no' height="100%" width="100%" >Module  Brochure</iframe>

 <?php } else if($details->fileType == 'image'){ ?>
 
 <img src="<?php echo LIBRARYP_PATH.$details->library_file; ?>" alt="Smiley face" height="100%" width="100%">

	
<?php } else if($details->fileType == 'video'){ ?>
<video width='100%' height='100%' controls controlsList="nodownload"> <source src=<?php echo  LIBRARYP_PATH.$details->library_file?> ></video>

<?php } else if($details->fileType == 'audio'){  ?>
<audio height="100%" width="100%" controls> <source src=<?php echo  LIBRARYP_PATH.$details->library_file?> ></audio>

 <?php }else{ 
	  $doc_url=LIBRARYP_PATH.$details->library_file;
$doc_url=urlencode($doc_url);
$url="https://docs.google.com/viewer?url=$doc_url&embedded=true";  ?>
<div class="hidebtn">&nbsp;</div>
<iframe src='<?php echo $url; ?>' frameborder='0' height="100%" width="100%" >Module  Brochure</iframe>
<?php 
 }

 } ?>
 </body>
 </html>
 
 
 <script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){ 
        return false;
    });
});
 $(document).ready(function(){ 
  $(document).oncontextmenu = function() {return false;};
  $(document).on('keydown',function(e)
  { 
    var key = e.charCode || e.keyCode;
	if(key == 122 || key == 123 || key == 27 || key == 112 || key == 114 || key == 115 || key == 116 || key == 144 || key == 82 || key == 18 || key == 17 || key == 20 || key == 9 )
        {return false; }
   
  });
});
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });//for cntrl+p
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 67){  e.preventDefault(); } });//cntrl+c
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });//cntrl+u
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 86){  e.preventDefault(); } });//cntrl+v
jQuery(document).bind("keyup keydown", function(e){ if(e.altKey && e.keyCode == 9){  e.preventDefault(); } });//alt+tab
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 83){  e.preventDefault(); } });//cntrl+s
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 65){  e.preventDefault(); } }); //cntrl+a
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 90){  e.preventDefault(); } }); //cntrl+z
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 89){  e.preventDefault(); } }); //cntrl+y
 </script>
<script>
 function copyToClipboard() {
	var aux = document.createElement("input"); 
  aux.setAttribute("value", "Dont take screenshot.");
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
 document.body.innerHTML ="<br><br><br><div style='color: #FF0000; font-size: 36px;text-align: center;'>Screenshorts are restricted in this site</div>";
 // alert("Screenshots are restricted.");
}

$(window).keyup(function(e){
  if(e.keyCode == 44){
    copyToClipboard();
  }
}); 

$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
}); 
$('.ndfHFb-c4YZDc-Wrql6b').remove();
</script>
