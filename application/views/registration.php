<!DOCTYPE html>
<html lang="en">
<title>Online Examnation Services</title>
 <link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />
 <link href="<?php echo CSS_URL; ?>bootstrap.min.css" rel="stylesheet">
 <link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet">
 <link href="<?php echo CSS_URL; ?>font-awesome.css" rel="stylesheet">
  <link href="<?php echo SITEURL ?>/assets/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo SITEURL ?>/assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo SITEURL ?>/assets/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo JS_URL; ?>jquery.min.js"></script>
<script src="<?php echo JS_URL; ?>common.js"></script>
<script>var baseUrl = "<?php echo SITEURL2; ?>"; </script>
<body>
<style>
.select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field {
   min-width: 21em;
}
</style>
<div id="wrapper">
    <!--Seat Selection Section!-->
    <div class="clearfix"></div>
    <section class="login_bg login_bg_height">
	<form action="<?php echo REGISTRATION_URL?>" method="post">
        <div class="container">
		
        <div class="row">
                <center><a href="<?php echo SITEURL?>"><img class="mt25" src="<?php echo SITEURL?>/assets/images/logo.png"></a></center>
                <p style="font-size:15px;color:#FFF; text-align:center;margin:10px 0; text-transform:uppercase;">Onyx Educationals</p>
				<?php if (isset($successMessage)) { ?>
				    <div class="alert alert-success alert-dismissable" style="font-size:15px;text-align:center;margin:10px 0; text-transform:uppercase;">
					 <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"></a>
					 <strong> <?php echo $successMessage; ?></strong>
					</div>
					<?php } ?>
					<?php if (isset($errorMessage)) { ?>
				    <div  style="font-size:15px;color:#ff0000; text-align:center;margin:10px 0; ">
					 <?php echo $errorMessage; ?>
					</div>
					<?php } ?>
					
              <?php echo $this->session->flashdata('error'); ?>
					
            <div class="login_panel register_panel">
            	<div class="login_area" style="padding:15px 0px;">
				<div id="numberExists" style="font-size:15px;color:#ff0000; text-align:center;margin:10px 0; "></div>
					<div id="NameError" style="font-size:15px;color:#ff0000; text-align:center;margin:10px 0; "></div>
					<div id="PackageError" style="font-size:15px;color:#ff0000; text-align:center;margin:10px 0; "></div>
					<div id="PwdError" style="font-size:15px;color:#ff0000; text-align:center;margin:10px 0; "></div>
					<div id="cPwdError" style="font-size:15px;color:#ff0000; text-align:center;margin:10px 0; "></div>
					<div id="otpSend" style="font-size:15px;color:#31708f; text-align:center;margin:10px 0; "></div>
                	<h1 class="m_l_r15"><img src="<?php echo SITEURL?>/assets/images/register_icon.png"> Registration</h1>
					<div class="form-group col-sm-6">
                    <input type="text"  maxlength="15"  placeholder="Name *" id="name" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/ +?/g, '');"  value="<?php if (isset($_POST['name'])) echo $_POST['name'];  ?>" name="name" class="form-control login_field" required >
                </div>
                <div class="form-group col-sm-6">
                   <input type="email" placeholder="Email *" id="email"  value="<?php if (isset($_POST['email'])) echo $_POST['email'];  ?>" onkeyup="checkEmailExists();" name="email" class="form-control login_field" required > 
                </div>
                <div class="form-group col-sm-6">
                    <input type="password" placeholder="Password *" id="password"  name="password" class="form-control login_field" required> 
                </div>
                <div class="form-group col-sm-6">
                    <input type="password" placeholder="Confirm password *"  id="cpassword"  name="cpassword"class="form-control login_field" required>
                </div>
				<!-- on 14 th -->
                <div class="form-group col-sm-12" id="">
				 <label class="control-label">Select Courses: </label>&nbsp;
					<select id="course" onchange="getResultsByCourse(this.value)" required class="form-control login_field select2" name="course[]" multiple>
                                <option value="">Select Courses</option>
								<?php 
									$selected=array();
									if($courses && count($courses) > 0){ 
									foreach( $courses as $key=>$val ){
										$selected[$key] = $val->testId;
										 }
										 }
									if($courses && count($courses)>0){
										foreach($courses as $key=>$val){
										$select="";
									 ?>
                                   <option value="<?php echo $val->courseId ?>" <?php echo $select; ?>><?php echo $val->courseName ?></option>
									<?php } }  ?>
					</select>
                </div>
				<div id="showTests"></div>
               <div class="form-group col-sm-12" id="tAmnt">
					 <label class="control-label">Total Amount</label>&nbsp;
					 <input type="text" class="form-control login_field" placeholder="Total amount" readonly value="" id="amount">
					</div>
				 <input type="hidden" placeholder="" id="roleID"  name="roleID" value="2" class="form-control login_field">
                <div class="form-group col-sm-6">
                    <input type="mobile" maxlength="10"  value="<?php if (isset($_POST['mobile'])) echo $_POST['mobile'];  ?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  placeholder="Mobile *" id="mobile" name="mobile" class="form-control login_field" required>
                </div>
				<div class="form-group col-sm-6">
                   <input type="otp"  maxlength="6" value="<?php if (isset($_POST['otp'])) echo $_POST['otp'];  ?>" placeholder="Otp *" id="otp"  name="otp" class="form-control login_field" required>
                 </div>
                  
                <div class="clearfix"></div>
                <center>
				<button type="submit" name="submit" value="submit" class="btn login_btn mt15" >SUBMIT</button>
				
				<a href="<?php echo SITEURL;?>"><input type="button" name="back" value="Back" class="btn register_btn mt15" ></a>
				</center>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
		</div>
		</div>
    <div class="clearfix"></div>
  </section>
</div>
</body>
<script type="text/javascript">
jQuery('#otp').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;		
    this.value = this.value.replace(/[^a-zA-Z0-9  \s]/g,'');
	this.setSelectionRange(start, end); 
    });	
$('#mobile').keyup(function() {
		var mob=$('#mobile').val();
        var l=mob.length;
	    if(l==10){
			$.ajax({
                    type: "post",
                    url:baseUrl+'/Account/sendOtp',  
				    data: {mob:mob},
                            success: function(response){
								
								   if(response == 1){
									     $('#otpSend').html("Check your mobile for otp");
										  $('#numberExists').html("");
								   }else{
									    $('#numberExists').html(response);
									    $('#otpSend').html("");
								   }
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	   }
	   
    });
	$('#otp').keyup(function() {
		var otp=$('#otp').val();
		var mob=$('#mobile').val();
        var l=otp.length;
	    if(l==6){
			$.ajax({
                    type: "post",
                    url:baseUrl+'/Account/checkRegistrationOtp',  
				    data: {mob:mob,otp:otp},
                            success: function(response){
								
								   if(response == 1){
									    $('#numberExists').html("Invalid Otp");
								   }else{
									   
								   }
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	   }
	   
    });
	$('#name').keyup(function() {
	
		var name=$('#name').val();
		if(name.length < 3 ) {  $('#NameError').html("The name field must be at least 3 characters in length.");}else{ 
			$('#NameError').html("");
		};
		/*if(name=="") { $('#NameError').html("Please fill all fields."); }else{ 
		$('#NameError').html(""); }*/
		
	});
	$('#password').keyup(function() {
	      var pwd=$('#password').val();
		if(pwd.length < 6 ){ $('#PwdError').html("Password field must be at least 6 characters in length."); }
		else { $('#PwdError').html(""); }
	});
	
	$('#cpassword').keyup(function() {
	    var cpwd=$('#cpassword').val();
		var pwd=$('#password').val();
	     if(pwd == cpwd ){ $('#cPwdError').html("");}else{$('#cPwdError').html("Confirm password must be match with password field.");}
	});
	$('#mobile').keyup(function() {
	   var uptest = [];
        $.each($("#test option:selected"), function(){            
            uptest.push($(this).val());
        });
	var count= uptest.length;
	
	var pack = [];
        $.each($("#package option:selected"), function(){            
            pack.push($(this).val());
        });
    var uppacks= pack.join(", ");
	
	var pcount= pack.length;
	if(pcount < 1 && count<1 ){   //on 14 th
		$('#PackageError').html("Please select at least one package or test.");
	}else{
		$('#PackageError').html("");
	}
});
	
	
	function getTotalAmount(){
	var uptest = [];
        $.each($("#test option:selected"), function(){            
            uptest.push($(this).val());
        });
    var uptests= uptest.join(", ");
	var count= uptests.split(',').length;
	var x = new Array();
    x = uptests.split(",");
    t = JSON.stringify(x);
    var pack = [];
        $.each($("#package option:selected"), function(){            
            pack.push($(this).val());
        });
    var uppacks= pack.join(", ");
	var count= uppacks.split(',').length;
	var y = new Array();
    y = uppacks.split(",");
    p = JSON.stringify(y);
	 $.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Account/getTotalAmount',  
				    data: {tests:t,packages:p},
					dataType: "JSON",
                    success: function(response){
						
						if(response)
						{
							$("#pamount").html(response.pamount);
							$("#tamount").html(response.tamount);
							$("#amount").val(response.amount);
						}else{
						}
						   
                    },
					error: function(xhr, statusText, err){
                         console.log("Error:" + xhr.status);  
					}
						
                });
} 
         //on 14 th
		 $(document).ready(function() {
				$('#tesddsdst').hide();
				$('#pack').hide();
			});

			  function showDiv(){ 
				var select=$("#select").val();
				if(select == 'test'){ 
				$('#tesddsdst').show();
				$('#pack').hide();
				}
                if(select == 'pack'){
                  $('#pack').show();
                  $('#tesddsdst').hide();}
              if(select == ''){
                  $('#pack').hide();
                  $('#tesddsdst').hide();}
				}
				function getResultsByCourse($id){
					$('#showTests').hide();
					var course = [];
					$.each($("#course option:selected"), function(){            
						course.push($(this).val());
					});
                   var courses= course.join(", ");
				   if(courses){
	                $('#courseIds').val(courses);
					$.ajax({ 
						           url:baseUrl+'/Account/testOrPackByCourseIds',  
								    type: "POST",
									data: {courses:courses},
						           success: function(output) { 
								  
								   $('#showTests').html(output);
								    $('#showTests').show();
						  }
				    });
				   }else{
					    //$('#NameError').html('Please select atleast one course.');
					   
				   }
				}
	
</script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="<?php echo JS_URL; ?>bootstrap.min.js"></script>
        <script src="<?php echo SITEURL ?>/assets/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/js/app.min.js" type="text/javascript"></script>
       <script src="<?php echo SITEURL ?>/assets/js/components-select2.min.js" type="text/javascript"></script>
        <script src="<?php echo JS_URL; ?>common.js"></script>
</html>