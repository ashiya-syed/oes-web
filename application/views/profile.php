<!DOCTYPE html>
<html lang="en">
<style>
input[type="file"]{
    color: transparent;
}
#footer{
	position:relative;
}

</style>
<body>
<div id="wrapper">
    <!--Body!-->
    <section class="main_body">
        <div class="container">
        <div class="row profile">
            <h1>Account overview</h1>
			<?php if (isset($messege)) {?>
			 <div class="alert alert-success alert-dismissable">
							       <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"></a>
							        <strong>Success!</strong> <?php echo $messege; ?>
							   </div>
			<?php }?>
			<?php if (isset($success)) { ?>
			<div class="alert alert-success alert-dismissable">
						<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"></a>
			<strong><?php echo $success; ?></strong></div><?php }?>
			
			<?php if (isset($failure)) { ?>
			<div class="alert alert-danger alert-dismissable">
						<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"></a>
			<strong><?php echo $failure; ?></strong></div><?php }?>
			<?php if (isset($profile_messege)) {?>
                 <div class="alert alert-danger alert-dismissable">
				<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"></a>
			     <strong><?php echo $profile_messege; ?></strong></div>
			<?php } ?>
            <div class="profile_tab">
            	Personal Information
                <!--span><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span-->
            </div>
			<form   method="POST" enctype="multipart/form-data" action="<?php echo PROFILE_URL ?>"  >
            <div class="profile_details">
			
            	<div class="form-group">
                  <label class="control-label col-sm-3" for="name" >Name</label>
                  <div class="col-sm-6">
                    <input type="name"  maxlength="15" class="form-control login_field" id="name" placeholder="Name"  name="name" value="<?php if($results){echo $results->userName;}else{echo '';}?>" readonly>
                  </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-3" for="mobile">Mobile</label>
                  <div class="col-sm-6">
                    <input type="mobile" maxlength="10" readonly  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" class="form-control login_field" id="mobile" placeholder="Mobile"  name="mobile" value="<?php if($results){echo $results->phoneNumber;}else{echo '';}?>" >
                  </div>
                </div>
				<div class="form-group clearfix">
                  <label class="control-label col-sm-3" for="mobile">Email Address</label>
                  <div class="col-sm-6">
                    <input type="email"  readonly  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" class="form-control login_field" id="mobile" placeholder="emailAddress"  name="emailAddress" value="<?php if($results){echo $results->emailAddress;}else{echo '';}?>" >
                  </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-3" for="gender">Gender</label>
                  <div class="col-sm-6">
                    <div class="mb15">
                    <label class="radio-inline"><input type="radio" name="gender" value="1"  <?php if($results){if(@$results->gender == '1') echo 'checked';}?>>Male</label>
                    <label class="radio-inline"><input type="radio" name="gender" value="2"  <?php if($results){if(@$results->gender == '2') echo 'checked';}?>>Female</label>
                    </div>
                  </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-3" for="address">Address</label>
                  <div class="col-sm-6">
                    <textarea class="form-control login_field" rows="2" id="address" placeholder="Address" name="address" value=""><?php if($results){echo $results->address;}?></textarea>
                  </div>
                </div>
            	<div class="form-group clearfix" style="display:none">
                  <label class="control-label col-sm-3" for="state">Select State</label>
                  <div class="col-sm-6" >
				     <select class="form-control login_field select-down-new" id="stateID" name="state" onchange="getCitys(this.value);" required>
                        <option>Select State</option>
					    <?php if($states && count($states) > 0){
						 foreach($states as $state){
							 if($state->stateID == $results->state ){?>		 
					    <option value="<?php echo $state->stateID;?>" selected><?php echo $state->stateName;?></option><?php }else{ ?>
                        <option value="<?php echo $state->stateID  ?>"><?php echo $state->stateName ?> </option>
						<?php } }  } ?>
                    </select>
                  </div>
                </div>
            	<div class="form-group clearfix" style="display:none">
                  <label class="control-label col-sm-3" for="city">Select City</label>
                  <div class="col-sm-6">
                    <select class="form-control login_field select-down-new" id="cityID" name="city">
                        <option>Select City</option>
                        <?php if($cities && count($cities) > 0){
						 foreach($cities as $city){
							 if($city->cityID == $results->city ){?>		 
					    <option value="<?php echo $city->cityID;?>" selected><?php echo $city->cityName;?></option><?php }else{ ?>
                        <option value="<?php echo $city->cityID  ?>"><?php echo $city->cityName ?> </option>
						<?php } }  } ?>
                    </select>
                  </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-3" for="zip">Zip Code</label>
                  <div class="col-sm-6">
                    <input type="zip" maxlength="6"   onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  class="form-control login_field" id="zip" placeholder="Zip Code" name="zipCode"  value="<?php if($results){echo $results->zipCode;}?>">
                  </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-3" for="upload">Upload Profile image</label>
                  <div class="col-sm-6">
                  <input type="file"  placeholder="Profile Pic" name="profile_pic" id="profile_pic" >
                    <button type="submit" name="save" value="" class="btn register_btn blue_btn mt50">Save</button>
                    <a type="button" href="javascript:window.history.back();" class="btn register_btn red_btn mt50">Cancel</a>
                  </div>
                </div>
                <div class="clearfix"></div>    
            </div>
			</form>
            
            <div class="profile_tab">
            	Security
                <!--span><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span-->
            </div>
			<form method="post" action="">
			  <div class="profile_details">
            	
				<div class="form-group">
                  <label class="control-label col-sm-3" for="password"> Password</label>
                  <div class="col-sm-6">
                    <input type="password" class="form-control login_field" id="password" placeholder="Password" name="password">
                  </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-3" for="password" name="cpassword">Confirm Password</label>
                  <div class="col-sm-6">
                    <input type="password" class="form-control login_field" id="cpassword" name="cpassword" placeholder="Confirm Password">
                    <button type="submit" name="update_password" value="" href="<?php echo PROFILE_URL?>" class="btn register_btn blue_btn mt50">Save</button>
                    <a type="button" href="javascript:window.history.back();" class="btn register_btn red_btn mt50">Cancel</a>
                  </div>
                </div>
                <div class="clearfix"></div>    
            </div>
			</form>
			
			<div class="profile_tab">
            	Selected courses
                <span><a href="<?php echo SELECT_COURSE_URL.'/'.$results->userId ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
            </div>
			
			<form method="post" action="">
			  <div class="profile_details">
			  <?php  $courses=""; if($courseDetails){ ?>
            	<?php $cours=""; foreach($courseDetails as $course){
					
                $cours.=$course->courseName.',';
				$courses=rtrim($cours,',');

				?>
				
            	<?php } ?><?php } ?>
				<div class="form-group">
                  <label class="control-label col-sm-3" for="password"> Course Name</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control login_field" id="course"  placeholder="Course" readonly name="course" value="<?php echo $courses; ?>">
                  </div>
                </div>
                <div class="clearfix"></div>    
            </div>
			</form>
			
            <?php 
			$enrollmentNumber="";
			$institute="";
			$batch="";
			if($enrollmentDetails){
				$enrollmentNumber=$enrollmentDetails->enrollment_id;
				$institute=$enrollmentDetails->institute;
				$batch=$enrollmentDetails->batch;
			}?>
           <!-- <div class="profile_tab">
            	Enrollment Information
            </div>
            <div class="profile_details">
            	<div class="form-group">
                  <label class="control-label col-sm-3">Enrollment Number</label>
                  <div class="col-sm-6 profile_view_txt">
				  <?php if($enrollmentNumber){echo $enrollmentNumber;}else{echo "You are not allotted any enrollment number.";}?>
                   </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-3">Institute Name</label>
                  <div class="col-sm-6 profile_view_txt">
				  <?php if($institute){echo $institute;}else{echo "You are not Enrolled.";}?>
                    
                  </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-3">Course</label>
                  <div class="col-sm-6 profile_view_txt">
                    <?php if($course){echo $course;}else{echo "No Course is selected by you";}?>
                  </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-3">Batch</label>
                  <div class="col-sm-6 profile_view_txt">
				   <?php if($batch){echo $batch;}else{echo " You are not allotted any Batch.";}?>
                   
                  </div>
                </div>
                <div class="clearfix"></div>    
            </div>-->
        </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo JS_URL; ?>jquery.scrollto.js"></script>
</body>
</html>
<script>
jQuery('#name').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;		
    this.value = this.value.replace(/[^a-zA-Z0-9  \s]/g,'');
	this.setSelectionRange(start, end); 
    });
	jQuery('#address').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;		
    this.value = this.value.replace(/[^a-zA-Z0-9.@#&_\-(,)/ \s]/g,'');
	this.setSelectionRange(start, end); 
    });
jQuery('#mobile').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;
    this.value = this.value.replace(/[^[789]\d{9}]/g,'');
    this.setSelectionRange(start, end);
  });
$(function () {
		$('input[type="file"]').change(function () {
			  if ($(this).val() != "") {
					 $(this).css('color', '#333');
			  }else{
					 $(this).css('color', 'transparent');
			  }
		 });
	})

</script>
