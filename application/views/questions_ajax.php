<style type="text/css">
.zoomin img {
  height: 100px;
  width: 100px;
  -webkit-transition: all 2s ease;
     -moz-transition: all 2s ease;
      -ms-transition: all 2s ease;
          transition: all 2s ease;
}
.zoomin img:hover {
  width: 200px;
  height: 200px;
}
</style>
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML">
</script>
<?php 
$examid=$this->uri->segment(4);
$package=$this->uri->segment(6);
$uniqueid=$this->uri->segment(8);
if(empty($examid)){
	$examid=$this->session->userdata('examId');
}
if(empty($package)){
	$package=$this->session->userdata('packageiD');
}
if(empty($uniqueid)){
	$uniqueid=$this->session->userdata('uniqueId');
}


if($details){$OPT=$details->optedOption;$userId=$this->session->userdata('user_id');$attempts=$this->session->userdata('attempts');
		$testType=$this->session->userdata('testType');
		 } ?> 
      <div class="col-md-9">
            	<div class="examquestion_module ">
                    <ul class="exam_title">
                    <li><?php echo $testname->testId;?>.<?php echo $testname->testName ?> </li>
                   <li class="pull-right"><span style="color:#feee66;">Maximum Mark - <?php echo $testname->rightMarks  ?></span><span style="color:#dd3e3e;">Negative Mark - <?php echo $testname->negativeMarks ?></span></li>
                    </ul>
                    <div class="clearfix"></div>
					
                    <div class="question_module" >
					<?php 
					@$Opted=$details->optedOption->option_id;
								 $selected=array();
								 $selectedOptions = explode(',', $Opted);
								 if($selectedOptions && count($selectedOptions) > 0){ 
										foreach( $selectedOptions as $key=>$val ){
													$selected[$key] = $val;
										}
								}
						?>
						<?php if($details->images){
							$imageUrls=$details->images;
							foreach($imageUrls as $imgU){
								$position=$imgU->q_position;
							}
						} ?>
						
						<?php if($position == 1 && $details->images){
							$imageUrls=$details->images;
							foreach($imageUrls as $imgU){
								$position=$imgU->q_position;
								echo '<span class="zoomin"><img  src="'.$imgU->q_image.'" height="70" width="70" alt="finding image"></span>';
							}
						} ?>
						<p><span>Q. <?php echo @$ques;?>-</span> <b><?php echo @$details->que; ?></b></p>
						<?php if($position == 0 && $details->images){
							$imageUrls=$details->images;
							foreach($imageUrls as $imgU){
								$position=$imgU->q_position;
								echo '<span class="zoomin"><img  src="'.$imgU->q_image.'" height="70" width="70" alt="finding image"></span>';
							}
						} ?>
						<div id="showHint"><b>Hint : </b> <?php if(@$details->hint){echo @$details->hint;}else{echo '<p class="text-danger">Found no hints for this Question</p>';}?> </div>
						<div id="opt">
						<?php 
						if(@$details->queType == 2){
							$queType=2;
							$this->load->model('Testsmodel');
							 $where=array('que_id'=>$details->que_id,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueid,'isActive'=>1,'queType'=>2);
		                      $ans=$this->Testsmodel->getRecord(TBL_EXAM_RESULTS,$where);
							?>
						<div class="checkbox mt25">
						Enter Answers :  <input type="text" style="border: 0;outline: 0;background: transparent;border-bottom: 1px solid black;"  class="chb" id="textOption" name="option[]" value="<?php echo $ans; ?>" >
						
						</div>
						<?php }elseif(@$details->queType == 3){
							$queType=3;
							$this->load->model('Testsmodel');
							 $where=array('que_id'=>$details->que_id,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueid,'isActive'=>1,'queType'=>2);
		                      $ans=$this->Testsmodel->getRecord(TBL_EXAM_RESULTS,$where);
							 
							  ?>
							Select Answers :  
							
						<div id="roll">
					   <h4 class="modal-title" id="myModalLabel" ><input type="radio" id="r1" name="role" <?php if($ans == "True") echo "checked"; ?> value="True" onclick="saveRadioOptions(<?php echo @$details->que_id;?>,<?php echo $examid ?>,<?php echo $package ?>,<?php echo $uniqueid ?>)" >True &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <input type="radio" id="r2" name="role"  value="False" <?php if($ans == "False") echo "checked"; ?> onclick="saveRadioOptions(<?php echo @$details->que_id;?>,<?php echo $examid ?>,<?php echo $package ?>,<?php echo $uniqueid ?>)" >False</h4><br>
                       </div>
					<?php
						}else{
						 $options=@$details->options;
						 if(@$options){
							 $cc="";
							 $count=0;
							 foreach($options as $opt){
								 $cc=$opt->is_answer;
								 if($cc == 1){$count=$count+1;}
							 }
									 foreach($options as $key=>$val){
										 $textType=$val->textType;
										if (in_array($val->option_id ,$selected))
										{
												$select="checked";
										}	else {
												$select="";
								        }
										
							 ?>
                    	<div class="checkbox mt25">
						 <label><input type="checkbox" onclick="save(<?php echo @$details->que_id;?>,<?php echo $examid ?>,<?php echo $package ?>,<?php echo @$ques;?>);" class="chb" id="check" name="option[]"  value="<?php echo $val->option_id; ?>" <?php echo $select ?> >
						 <?php if($textType == 0 || $textType == 1 ){
							 echo $val->option;
						 }else if($textType == 2){
							 $src=UPLOADS_OPTIONS_PATH.$val->option;
							 echo "<div class='zoomin'><img src=$src width='90' height='45'></div>";
						 }else if($textType == 3){
							  $src= UPLOADS_OPTIONS_PATH.$val->option;
							 echo "<video width='110' height='95' controls> <source src=$src ></video>";
						 }else{
							 $src= UPLOADS_OPTIONS_PATH.$val->option;
							 echo "<audio width='110' height='95' controls> <source src=$src ></audio>";
						 }
						 
						 ?></label>
                        </div>
						
						<?php }}}?>
                        </div>
						<button id="hint" >Hint</button>
						<?php if(@$details->queType == 1 || @$details->queType == 0){
							if(@$count == 1){
							?>
						<button  <?php if(@$details->isShuffle == 1 || @$details->FiftyFity >= 3) echo "disabled"; ?> onclick="getFiftyFiftyOptions(<?php echo $examid ?>,<?php echo @$details->que_id; ?>,<?php echo $uniqueid ?>,<?php echo $package ?>);" >Take 50-50 option</button>
						<?php } } ?>
						<?php if(@$details->queType == 2){?>
						<button onclick="saveBlankOptions(<?php echo @$details->que_id;?>,<?php echo $examid ?>,<?php echo $package ?>,<?php echo $uniqueid ?>)"> save </button><?php } ?>
                    </div>
					<div class="col-sm-6 btn_grp">
					       <button id="reviewnext" onclick="next(<?php echo @$details->que_id;?>,1,1,<?php echo @$ques+1 ?>)" class="btn register_btn blue_btn">Review & Next</button>
                            <button id="reviewprev" onclick="privious(<?php echo @$details->que_id;?>,3,1,<?php echo @$ques-1?>)" class="btn register_btn blue_btn">Review & Previous</button>
                           <button id="review" type="button" href="#agentDetailsPopup" data-toggle="modal" onclick="getReviewQuestions('<?php echo $examid;?>','<?php echo $package ?>')"   class="btn register_btn blue_btn" >Review Questions</button>
                        </div>
                        <div class="col-sm-6 text-right btn_grp">
						
                            <button id="previous"   class="btn register_btn blue_btn btn2" onclick="privious(<?php if($details){echo $details->que_id;}?>,3,0,<?php echo $ques-1?>);"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Previous</button>
                            <button type="button"  id="next" class="btn register_btn blue_btn btn2" onclick="next(<?php echo @$details->que_id;?>,1,0,<?php echo @$ques+1 ?>);"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Next</button>
							<!--<button id="save" class="btn register_btn blue_btn btn2"  onclick="save(<?php //echo $details->que_id;?>,<?php //echo $examid ?>);"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> save</button>-->
                        </div>
                        <div class="clearfix"></div>
                        
                </div>
            </div>
			
            <div class="col-md-3">
            	<div class="examquestion_module">
                    <ul class="exam_title text-center">
                    <li>Review Questions</li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="question_module scrollbar">
					 
                    <ul class="qstn_number">
					   <?php 
					   if($ReferenceTestIdArray){for($i=0;$i<count($ReferenceTestIdArray);$i++){$j=$i+1;
						if($ques == $j ){?><a href="javascript:void(0)" onclick="next(<?php echo $ReferenceTestIdArray[$i]?>,2,0,<?php echo $j;?>)"><li class="active" id="num<?php echo $j;?>"><?php echo $j;?></li></a>
							<?php
							}elseif(in_array($ReferenceTestIdArray[$i],$attemptArray)){
							?>
							<a href="javascript:void(0)"  onclick="next(<?php echo $ReferenceTestIdArray[$i]?>,2,0,<?php echo $j;?>)"><li class="selected" id="num<?php echo $j;?>"><?php echo $j?></li></a>
							<?php }else{?>
								<a href="javascript:void(0)"  onclick="next(<?php echo $ReferenceTestIdArray[$i]?>,2,0,<?php echo $j;?>)"><li class="" id="num<?php echo $j;?>"><?php echo $j?></li></a>
							<?php }}}?>
                     </ul>
					 
                    </div>
                    <p style="background:#eee; color:#0C0;padding:5px 10px;margin-bottom: 1px;">Attempted -<?php echo count($attempted)?></p>
                    <p style="background:#eee; color:#F00;padding:5px 10px;margin-bottom: 0px;">Unattempted - <?php echo (count($questionNumbers))-(count($attempted));?></p>
                        <div class="clearfix"></div>
                </div>
            </div>
           
        </div>
        </div>
    </section>
 

<div class="modal fade" id="agentDetailsPopup" tabindex="-1" role="basic" aria-hidden="true">
<div class="modal-dialog"  id="_agent_details_div" style="margin-top:80px;">
</div>
</div>	

<script>
function saveTime() { 
$.ajax({ 
        url: baseUrl+'/Dashboard/saveTime',  
        type: 'post',
         success: function(output) {
          }
});
}
var i = setInterval(function() { saveTime(); }, 120000);

var endExam='<?php echo  $this->session->userdata('endExam'); ?>';
var url='<?php  echo SUBMIT_ON_BROWSER_CLOSE_URL ?>';

$(document).ready(function(){
	if(endExam == 2){ 
	window.open(url,'win2','top=0,left=0,width='+screen.width+',height='+screen.height+',status=yes,resizable=no,scrollbars=yes',"_self");
 //  window.opener.location.reload();
 self.close();
  window.opener.clearDiv();
}
	$("#showHint").hide();
	$("#hint").click(function(){
        $("#showHint").toggle();
    });
	
  $("#reviewprev").hide();
   var que=<?php echo $ques;?>;
   var count=<?php echo  count($questionNumbers);?>;
    
  
	 if(que == count){
		$("#next").attr('disabled', 'disabled');
		$("#reviewprev").show();
		$("#reviewnext").hide();
   }
   if(que == 1 ){
		$("#previous").attr('disabled', 'disabled');
		$("#reviewprev").hide();
	}
	if(que == 1 && count == 1){
		$("#previous").attr('disabled', 'disabled');
		$("#next").attr('disabled', 'disabled');
		$("#reviewnext").hide();$("#reviewprev").hide();$("#review").hide();
	}
   if(count == 1 ){
	  $("#review").hide();
	}
   if (que > 1 && que < count) {$("#previous").removeAttr('disabled');}
  
});



MathJax.Hub.Queue(['Typeset',MathJax.Hub,'body']);

</script>

<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>-->


 <script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
$(document).ready(function(){ 
  $(document).oncontextmenu = function() {return false;};
  $(document).on('keydown',function(e)
  { 
    var key = e.charCode || e.keyCode;
	if(key == 122 || key == 123 || key == 27 || key == 112 || key == 114 || key == 115 || key == 116 || key == 144  || key == 18 || key == 17 || key == 20 || key == 9 )
        {return false; }
   
  });
});
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });//for cntrl+p
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 67){  e.preventDefault(); } });//cntrl+c
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });//cntrl+u
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 86){  e.preventDefault(); } });//cntrl+v
jQuery(document).bind("keyup keydown", function(e){ if(e.altKey && e.keyCode == 9){  e.preventDefault(); } });//alt+tab
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 83){  e.preventDefault(); } });//cntrl+s
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 65){  e.preventDefault(); } }); //cntrl+a
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 90){  e.preventDefault(); } }); //cntrl+z
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 89){  e.preventDefault(); } }); //cntrl+y

function saveBlankOptions(que,examId,package,unique){


	 var val=$('#textOption').val();
	 $.ajax({
				   type:'POST',
				   url:baseUrl+'/Dashboard/saveBlankOptions', 
				   data:{que:que,val:val,examId:examId,package:package,unique:unique},
				   success:function(t){
					  //alert(t);
					}
				   });
 return false; 
}
function saveRadioOptions(que,examId,package,unique){
	if (document.getElementById('r1').checked) {
            var roll = document.getElementById('r1').value;
			}
			else if (document.getElementById('r2').checked) {
				 var roll = document.getElementById('r2').value;
			}else{
				var roll='';
			}
			//alert(roll);
$.ajax({
				   type:'POST',
				   url:baseUrl+'/Dashboard/saveBlankOptions', 
				   data:{que:que,val:roll,examId:examId,package:package,unique:unique},
				   success:function(t){
					 // alert(t);
					}
				   });
 return false; 
}
function privious(index,type,review,order){
	$("#showHint").hide();
	    var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });

  //  if(index>1){
    $.ajax({
				type:'POST',
				url:pageUrl,
				data:{index:index,type:type,val:val,review:review,order:order},
				success:function(t){
				  $('#question_module').html(t);
				}
		  });
   return false; 
  // }
}
 
function next(index,type,review,order){
	$("#showHint").hide();
	 var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });
	 $.ajax({
				   type:'POST',
				   url:pageUrl,
				   data:{index:index,type:type,val:val,review:review,order:order},
				   success:function(t){
                      $('body').removeClass('modal-open');
					  //$('#num'+52).focus();
                      $('.modal-backdrop').remove();
					  $('#question_module').html(t);
					}
				   });
 return false; 
}

function save(que,examId,package,order){
	 var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });
     $.ajax({
				   type:'POST',
				   url:saveUrl,
				   data:{que:que,val:val,examId:examId,package:package,order:order},
				   success:function(t){
					  // $('#question_module').html(t);
					}
				   });
 return false; 
}
function getReviewQuestions(examid, package){
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Dashboard/reviewQuestions',  
				    data: {examId:examid,package:package},
                            success: function(response){
							$("#agentDetailsPopup").modal('show');
						     $('#_agent_details_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
function getFiftyFiftyOptions(test,que,unique,package){
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Dashboard/getFiftyFiftyOptions',  
				    data: {test:test,que:que,unique:unique,package:package},
                            success: function(response){
								//alert(response);
								 $('#opt').html(response);
								   },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
} 

</script>

  