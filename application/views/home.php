<!DOCTYPE html>
<body>
<div id="wrapper">
    <!--Header !-->
    <header id="header">
    	<nav class="navbar navbar-inverse navbar-static-top">
                <div class="container">
                        <div class="row">
                        <div class="col-sm-6 logo-brand">
                        <a href="index.html"><img src="<?php echo SITEURL?>/assets/images/logo.png" alt="Logo" title="OES"/>
                        ONYX Educational</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <div class="user-profile dropdown">
                                <a href="#" title="" class="clearfix" data-toggle="dropdown" aria-expanded="true">
                                    <img width="41" class="img-circle" src="images/default_profile_img.png" alt="">
                                    <span>Deepak Kumar Muduli</span>
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="profile.html">Profile</a></li>
                                    <li><a href="#">Help</a></li>
                                    <li><a href="#">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                    <div class="clearfix"></div>
                </div>
            </nav>
    </header>
    <!--Body!-->
    <section class="main_body">
        <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="home_thumbnail border_color1">
                  <h1><img src="images/home_tab_icon1.png" style="margin-right:10px;"> Opted Tests
                    <div class="form-group exam_dropdown">
                    <select class="form-control login_field select-down-new border_none" id="">
                        <option>Prac</option>
                        <option>Exam</option>
                    </select>
                    </div>
                  </h1>
                  <div class="scrollbar" id="scroll_style">
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> 18 - jan - 2017</span>
                            </p>
                            <a type="button" href="report.html" class="btn register_btn blue_btn mt5">View Report</a>
                            </div>
                            <div class="test_tag">1/5</div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> 18 - jan - 2017</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">View Report</a>
                            </div>
                            <div class="test_tag">1/5</div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> 18 - jan - 2017</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">View Report</a>
                            </div>
                            <div class="test_tag">1/5</div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> 18 - jan - 2017</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">View Report</a>
                            </div>
                            <div class="test_tag">1/5</div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> 18 - jan - 2017</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">View Report</a>
                            </div>
                            <div class="test_tag">1/5</div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> 18 - jan - 2017</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">View Report</a>
                            </div>
                            <div class="test_tag">1/5</div>
                            <div class="clearfix"></div>
                        </div>
                  </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="home_thumbnail border_color2">
                  <h1><img src="images/home_tab_icon2.png" style="margin-right:10px;"> Take a Test
                    <div class="form-group exam_dropdown">
                    <select class="form-control login_field select-down-new border_none" id="">
                        <option>Prac</option>
                        <option>Exam</option>
                    </select>
                    </div>
                  </h1>
                  <div class="scrollbar" id="scroll_style">
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span>Practice section exam only 3 times</span>
                            </p>
                            <a type="button" href="start_exam.html" class="btn register_btn blue_btn mt5">Start Test</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span>Practice section exam only 3 times</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">Start Test</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span>Practice section exam only 3 times</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">Start Test</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span>Practice section exam only 3 times</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">Start Test</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span>Practice section exam only 3 times</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">Start Test</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p>General Geology Sample<br>
                            <span>Practice section exam only 3 times</span>
                            </p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">Start Test</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="home_thumbnail home_thumbnail2 border_color3">
                  <h1><img src="images/home_tab_icon3.png" style="margin-right:10px;"> Buy Packages
                  </h1>
                  <div class="scrollbar scrollbar2" id="scroll_style">
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/package.png"/></div>
                            <div class="thumb_txt thumb_txt2">
                            <p>APPSC-2017</p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">View Syllabus</a>
                            <a type="button" href="#" class="btn login_btn green_btn mt5"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Buy Now</a>
                            <div class="price_tag">Price<br>
                            ₹5000</div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="images/package.png"/></div>
                            <div class="thumb_txt thumb_txt2">
                            <p>APPSC-2017</p>
                            <a type="button" href="#" class="btn register_btn blue_btn mt5">View Syllabus</a>
                            <a type="button" href="#" class="btn login_btn green_btn mt5"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Buy Now</a>
                            <div class="price_tag">Price<br>
                            ₹5000</div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="home_thumbnail home_thumbnail2 border_color4">
                  <h1><img src="images/home_tab_icon4.png" style="margin-right:10px;"> News & Updates
                  </h1>
                  <div class="scrollbar scrollbar2" id="scroll_style">
                  		<div class="thumb_panel">
                        	<div class="thumb_img news_calender pull-left">
                            	2017<br/>
                                <span>6th<br>
                                December
                                </span>
                            </div>
                            <div class="thumb_txt thumb_txt2 blue_txt">
                            <p>*Technical Support<br>
                            <span>Dear Student - If you need support for our online examination system, please contact our technical executive.</span><br/>
                            <a class="news_readmore" href="#">Read More..</a>
                            </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img news_calender pull-left">
                            	2017<br/>
                                <span>6th<br>
                                December
                                </span>
                            </div>
                            <div class="thumb_txt thumb_txt2 blue_txt">
                            <p>*Technical Support<br>
                            <span>Dear Student - If you need support for our online examination system, please contact our technical executive.</span><br/>
                            <a class="news_readmore" href="#">Read More..</a>
                            </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img news_calender pull-left">
                            	2017<br/>
                                <span>6th<br>
                                December
                                </span>
                            </div>
                            <div class="thumb_txt thumb_txt2 blue_txt">
                            <p>*Technical Support<br>
                            <span>Dear Student - If you need support for our online examination system, please contact our technical executive.</span><br/>
                            <a class="news_readmore" href="#">Read More..</a>
                            </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<div class="thumb_panel">
                        	<div class="thumb_img news_calender pull-left">
                            	2017<br/>
                                <span>6th<br>
                                December
                                </span>
                            </div>
                            <div class="thumb_txt thumb_txt2 blue_txt">
                            <p>*Technical Support<br>
                            <span>Dear Student - If you need support for our online examination system, please contact our technical executive.</span><br/>
                            <a class="news_readmore" href="#">Read More..</a>
                            </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    
    <!-- Footer -->
	<footer id="footer">
    <div class="container">
    <div class="row">
        	<p>Copyright © 2017, All Rights Reserved.</p>
    </div>            
    </div>
    <div class="clearfix"></div>
    </footer>
    
</div>
</body>
</html>