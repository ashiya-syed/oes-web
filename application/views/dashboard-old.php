 <!--Body!-->
 <section class="main_body">
        <div class="container">
		<?php if (isset($successMessage)) { ?>
				    <div class="alert alert-success alert-dismissable">
					 <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"></a>
					 <strong> <?php echo $successMessage; ?></strong>
					</div>
					<?php } ?>
		<?php echo $this->session->flashdata('message'); ?>
		<?php echo $this->session->flashdata('examError'); ?>
		<?php echo $this->session->flashdata('buypackage'); ?>
		<?php echo $this->session->flashdata('verify'); ?>
		<div id="cartCount"></div>
        <div class="row">
            <div class="col-md-6">
                <div class="home_thumbnail border_color1">
                  <h1><img src="<?php echo IMAGES_URL; ?>home_tab_icon1.png" style="margin-right:10px;"> Opted Tests
                    <div   style="width: 25%;margin: 0px -5px;" class="form-group exam_dropdown" id="examSelect">
                    <select  class="form-control login_field select-down-new border_none"  onchange="optedTests(this.value)">
                       <option value="practical">Practice Test</option>
                        <option value="main">Main Test</option>
                    </select>
                    </div>
                  </h1>
                  <div class="scrollbar" id="scroll_style">
				      
					  <div id="optedTests">
					  <?php if($optedTests  && count($optedTests)>0){
						     $o=1;
                             foreach($optedTests as $optedTest){ 
							// $testResults=$optedTest->testResults;
							 $attempts=$optedTest->attempts;
							 $uniqueId=$optedTest->userPackageID;
							 $time=$optedTest->createdTime;
							 $d= date('d - M -Y', strtotime($time));
							 $testDetails=$optedTest->testDetails;
							 $testName=$testDetails->testName;
							 $testId=$testDetails->testId;
							 $testType=$optedTest->type;
							 $count=$optedTest->completedTestCount;
							 $packageId=$optedTest->packageId;
							 
					 ?>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="<?php echo IMAGES_URL; ?>thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                          <p><?php echo $o;?>.<?php echo $testName; ?><!--  , Attempts:  <?php// echo $optedTest->attempts; ?>-->
                           <!-- <span><i class="fa fa-clock-o" aria-hidden="true"></i> <?php //echo $d; ?></span>-->
                            </p>
							  <a type="button" id="review" type="button" href="#reportsPopup" data-toggle="modal" onclick="getAttemptsOfTest('<?php echo $testId;?>','<?php echo $uniqueId ?>')"  class="btn register_btn blue_btn mt25" >view reports</a>
                            <!--<a type="button" href="<?php echo VIEW_RECORD_URL.'?testId='.$testId.'&attempts='.$optedTest->attempts.'&type='.$testType.'&package='.$packageId.'&userPackageId='.$uniqueId?>" class="btn register_btn blue_btn mt25">view report</a>
                          --> </div>
                            <!--<div class="test_tag"><?php //echo $count; ?>/<?php //echo '3'?></div>-->
                            <div class="clearfix"></div>
                        </div>
					 <?php $o++;} }else { 
					    echo "<span style='color:red'>Your not took any test yet. Try to take a test.</span>";
					} ?>
						</div>
                  		
                  </div>
				  <!--new-->
				    <div class="modal fade" id="reportsPopup" tabindex="-1" role="basic" aria-hidden="true">
					<div class="modal-dialog"  id="_reports_Popup_div" style="margin-top:80px;">
					</div>
					</div>	
				  <!--end-->
                </div>
            </div>
			
            <div class="col-md-6">
                <div class="home_thumbnail border_color2">
                  <h1><img src="<?php echo IMAGES_URL; ?>home_tab_icon2.png" style="margin-right:10px;"> Take a Test
                   <div   style="width: 25%;margin:4px -6px;" class="form-group exam_dropdown" id="examSelect">
                    <select class="form-control login_field select-down-new border_none" id="examSelect" onchange="getTestTypesRecords(this.value)">
                        <option value="practical">Practice Test</option>
                        <option value="main">Main Test</option>
                    </select>
                    </div>
                  </h1>
                  <div class="scrollbar" id="scroll_style">
				    <div id="takeAtest">
				     <?php if($practicalTests  && count($practicalTests)>0){$i=1;
                             foreach($practicalTests as $practicalTest){
					 ?>
				   
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="<?php echo IMAGES_URL; ?>thumb_img.jpg"/></div>
                            <div class="thumb_txt">
                            <p><?php echo $i;?>.<?php echo $practicalTest->testName; ?><br>
							<?php 
							 $style="";
							$attempts=$practicalTest->attempts ;
							$count=$practicalTest->count;
							$uniqueId=$practicalTest->uniqueId;
							$class="btn register_btn blue_btn mt5";
							if($count){
								if($count==1){
									 $button="2nd attempt";
								}elseif($count==2){
									$button="Last Attempt";
								}else{
									$class="btn login_btn green_btn mt5";
									$button="completed";
								}
							}else{
								$button="start Test";
							}
							if($attempts){
								$messege="Attempts:".$attempts;
							}else{
								$messege="Practice section exam only 3 times";
                             }
							 $status=$practicalTest->redo;
							 if($status){
								 $button="redo test";
								 $class="btn login_btn green_btn mt5";
								 $style='style=" background-color: orange"';
							 }
							 ?>
                            <span><?php echo $messege; ?></span></p>
							<a type="button" <?php echo $style; ?> <?php if($count==3){?> onclick="this.removeAttribute('href');" <?php }?> onclick="start(<?php echo $practicalTest->testId;?>,<?php echo PRACTICAL_TEST;?>,<?php echo $practicalTest->packageId;?>,<?php echo $uniqueId ?>)" class="<?php echo $class; ?>"><?php echo $button;?></a>
							</div>
                            <div class="clearfix"></div>
                        </div>
					 <?php $i++;} }else { 
					    echo "<span class='text-center' style='color:red'>".NO_PRACTISE_TESTS_EXISTS."</span>";
					} ?>
                </div>	
					
                  </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="home_thumbnail home_thumbnail2 border_color3">
                <h1><img src="<?php echo IMAGES_URL; ?>home_tab_icon3.png" style="margin-right:10px;"> Buy Packages/Tests 
				  <div   style="width: 25%;margin: 0px -5px;" class="form-group exam_dropdown" id="examSelect">
                    <select  class="form-control login_field select-down-new border_none"  onchange="getProducts(this.value)">
                       <option value="package">Packages</option>
                        <option value="test">Tests</option>
                    </select>
                    </div>
                  </h1>
                 
				<!-- <a href="<?php //echo CART_URL ;?>"><img src="<?php //echo IMAGES_URL; ?>Shopping_cart_icon.svg.png" style="margin-right:10px;height:20px"><div id="cartCount"><?php echo $cartCount;?></div>
                  --><div class="scrollbar scrollbar2" id="scroll_style" >
				  <div id="product">
				  <?php if($packages && count($packages) >0){
                          foreach($packages as $package){
				  ?>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left"><img class="img-responsive" src="<?php echo IMAGES_URL; ?>package.png"/></div>
                            <div class="thumb_txt thumb_txt2">
							
                            <p><?php echo $package->package_name; ?></p>
                            <a type="button" onclick="popWindow(<?php echo $package->package_id;?>)" class="btn register_btn blue_btn mt5">View Details</a> 
							</br>
							<input type="submit" name="Buy Now" value="Add to cart" id="submit" placeholder="Buy Now" class="btn login_btn green_btn mt5" onclick="addtoCart(<?php echo $package->package_id;?>,<?php echo PACKAGES ;?>,<?php echo $package->package_amount; ?>)"> </input>
							<input type="hidden" name="quantity" id="quantity" value="1" size="2" style="width:36px" />
                             <div class="price_tag">Price<br>
                            ₹<?php echo $package->package_amount; ?></div>
							
                            </div>
                            <div class="clearfix"></div>
                        </div>
						<?php } } else { 
					        echo "<span class='text-center' style='color:red'>".NO_PACKAGES_AVAILABLE."</span>";
					    } ?>
                  		
                  </div>
				  </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="home_thumbnail home_thumbnail2 border_color4">
                  <h1><img src="<?php echo IMAGES_URL; ?>home_tab_icon4.png" style="margin-right:10px;"> News & Updates
                  </h1>
                  <div class="scrollbar scrollbar2" id="scroll_style">
				  <?php if($newsUpdates){foreach($newsUpdates as $news){?>
					  <div class="thumb_panel">
                        	<div class="thumb_img news_calender pull-left">
                            	<?php echo date('Y ', strtotime($news->createdTime));?><br/>
                                <span><?php echo date(' d ', strtotime($news->createdTime));?>th<br>
                                <?php echo date('  F  ', strtotime($news->createdTime));?>
                                </span>
                            </div>
                            <div class="thumb_txt thumb_txt2 blue_txt">
                            <p>*<?php echo $news->newsTitle;?><br>
                            <span><?php echo substr($news->description,0,380).'...'; ?></span><br/>
                            <a class="news_readmore" href="<?php echo SINGLE_PAGE_URL.'?id='.$news->id;?>">Read More..</a>
                            </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
					  
					  
				  <?php }}else{ echo "<span class='text-center' style='color:red'>".NEWS_UPDATES_NOT_FOUND."</span>";}?>
                  		
                  	
                  </div>
                </div>
            </div>
        </div>
        </div>
    </section>
	
<script type="text/javascript" language="javascript"> 
 var syllabusUrl = "<?php echo VIEW_SYLLABUS_URL; ?>";
 
//for closing the child window and redircting to parent
function HandlePopupResult(result) {
  
}
//for restricting browser background
/*history.pushState({ page: 1 }, "Title 1", "#no-back");
window.onhashchange = function (event) {
window.location.hash = "no-back";
};*/
function clearDiv(){
	  window.location="<?php echo DASHBOARD_URL;?>";
}

</script>
	
