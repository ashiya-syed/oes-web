﻿<!DOCTYPE html>
<html lang="en">
<?php if(@$packageids){$packageids=implode(',',$packageids);} $this->session->set_userdata('packageids',$packageids );
      if(@$testIds){$testIds=implode(',',$testIds);} $this->session->set_userdata('testIds',$testIds );?> 

<body>
<div id="wrapper">
    <!--Header !-->
    <!--Body!-->
    <section class="main_body">
        <div class="container">
        <div class="row cart">
		<div class="row nm">
            <h1 class="">Items Checkout</h1>
			<?php if(empty($packages) && empty($tests)) { ?>
			<div class="col-sm-10 col-xs-8 cart_blue_txt">
			<div class="col-sm-2 col-xs-4"><img class="img-responsive" src="<?php echo IMAGES_URL; ?>empty_shopcart.png"></div>
			<p>Click here to go home and add items to your cart. &nbsp;<a class="news_readmore" href="<?php echo ACCOUNT_URL;?>">Back to Home..</a></p>
			</div>
		<?php	}?>
		</div>
        	<?php  if($packages || $tests){ ?>
			<!--<div class="col-md-12">
            <h1>Items Checkout</h1>
			<div class="news_readmore text_decoration_no red_txt" id="Error"></div><br>
            </div>-->
			<?php }?>
			<div class="col-md-9">
			<?php  if($packages){ ?>
            	<div class="table-responsive clearfix">    
              
                          <table class="table table-bordered">
                            <thead>
                              <tr class="cart_table">
                                <th width="12%">Package</th>
                                <th width="64%">Package Details</th>
                                <th width="12%">Price</th>
                                <th width="12%">Quantity</th>
                              </tr>
                            </thead>
                            <tbody>
							<?php 
                            
               			      foreach($packages as $package){ ?>
                              <tr class="bg_white">
                                <td><img class="img-responsive" src="<?php echo IMAGES_URL; ?>package.png"/></td>
                                <td>
                                <p class="tble_blue_txt"><?php echo $package->packageName;?></p>
                                <p class="tble_blue_txt">Onyx educationals</p>
                                <a class="news_readmore text_decoration_no red_txt" href="javascript:void(0)" onclick="deleteCartItems(<?php echo $package->Id;?>)"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>
                                </p>
                                </td>
                                <td><p class="tble_blue_txt">₹ <?php echo $package->amount;?></p></td>
                                <td>
                                <div class="form-group"><p class="tble_blue_txt"><?php echo $package->quantity;?></p>
                              
                                </div>
                              </td>
							  
                              </tr><?php }?>
							  </tbody>
			   </table>
			   </div>
                              <?php }?>
                            
                  
			<?php if($tests){?>	
            	<div class="table-responsive clearfix">    
               		
                          <table class="table table-bordered">
                            <thead>
                              <tr class="cart_table">
                                <th width="12%">Test</th>
                                <th width="64%">Test Details</th>
                                <th width="12%">Price</th>
                                <th width="12%">Quantity</th>
                              </tr>
                            </thead>
                            <tbody>
							<?php 
                             
               			      foreach($tests as $test){ ?>
                              <tr class="bg_white">
                                <td><img class="img-responsive" src="<?php echo IMAGES_URL; ?>package.png"/></td>
                                <td>
                                <p class="tble_blue_txt"><?php echo $test->testName;?></p>
                                <p class="tble_blue_txt">Onyx educationals</p>
                                <a class="news_readmore text_decoration_no red_txt" href="javascript:void(0)" onclick="deleteCartItems(<?php echo $test->Id;?>)"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>
                                </p>
                                </td>
                                <td class="tble_blue_txt"><p class="tble_blue_txt">₹ <?php echo $test->amount;?></p></td>
                                <td>
                                <div class="form-group"><p class="tble_blue_txt"><?php echo $test->quantity;?></p>
                               <!-- <select class="form-control login_field select-down-new" id="" style="border: 1px solid #b2b2b2 !important;">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>-->
                                </div>
                              </td>
							  
                              </tr><?php }?>
                              
                            </tbody>
			   </table>
                  </div><?php }?>
            </div>
			
			<?php if($tests || $packages){	
			$userName="";$phoneNumber="";$emailAdress="";$userId="";
							if($userDetails){
								$userName=$userDetails->userName;
								$phoneNumber=$userDetails->phoneNumber;
								$emailAdress=$userDetails->emailAddress;
								$userId=$userDetails->userId;
							}
							?>
					  <div class="col-md-3">
				      <select id="select" class="form-control login_field select-down-new"   onchange="showPaymentTypes()" >
                      <option value="">Select Payment method </option>
                      <option value="coupon">Coupon code </option>
					  <option value="paytm" >Online Payment</option>
					  <!--option value="payu" >Pay u money</option>
					  <option value="paytm" >CC Avenue</option-->
					  </select>
                      </div>
			<form name="form1" method="post" id="form1" action="<?php echo BUY_PACKAGES_URL; ?>">
			<input type="hidden" name="tests" value="<?php print_r($testIds); ?>">
			<input type="hidden" name="packages" value="<?php print_r($packageids);?>">
			<input type="hidden" name="amount" value="<?php echo $amount ?>">
			<input type="hidden" name="userId" value="<?php echo $userId ?>">
             <div class="col-md-3">
			 <div class="news_readmore text_decoration_no red_txt" id="Error" > </div>
			  <?php echo $this->session->flashdata('buypackage'); ?>
			  <div class="table-responsive clearfix">          
                          <table class="table table-bordered">
                            <thead>
                            <tr class="coupon_box_title">
                                <th><img class="" src="<?php echo IMAGES_URL; ?>coupon_title_icon.png"> ENTER Coupon</th>
                            </thead>
                            <tbody>
                              <tr class="bg_white coupon_box_bg">
                                <td>
								<div class="coupon_box_price"><i class="fa fa-inr" aria-hidden="true"></i> <?php if($amount){
				 echo $amount;}else{echo 0;} ?></div>
						 <input type="name" name="CouponCode" id="CouponCode" maxlength="8" id="CouponCode" onkeyup="checkCouponValidity(<?php echo $amount; ?>)"  class="form-control login_field coupon_input" id="name" placeholder="ENTER COUPON CODE">
                                <center><input type="submit" name="Buy Now" id="submit" value="APPLY CODE" class="btn login_btn mt15 mb15 mr0"></a></center>
                                </td>
                                
								
                                
                              </tr>
				  <!--<input type="submit" name="Buy Now"  <?php if ($userVerified == '1'){ ?> disabled <?php   } ?>  onclick="add_user_details(<?php //echo $userId; ?>)" value="check out" id="submit" placeholder="Buy packages/tests" class="btn login_btn green_btn mt5"> </input>
                            -->
				 
							
				<!--end-->
                         </tbody>
                          </table>
                  </div>
				  </div>
				  </form>
			 <!--payment start-->
							<?php 
			                 $userName="";$phoneNumber="";$emailAdress="";$userId="";
							if($userDetails){
								$userName=$userDetails->userName;
								$phoneNumber=$userDetails->phoneNumber;
								$emailAdress=$userDetails->emailAddress;
								$userId=$userDetails->userId;
							}
                       //for test card
					   /* $MERCHANT_KEY ="RSGByDGB";
						$SALT = "BsdgDFKnKW"; */
						$MERCHANT_KEY ="uDB0BEqI";
						$SALT = "FoiBem8XCt";
					    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
						$this->session->set_userdata('txnid',$txnid);
						$posted['txnid']=$txnid;
						$posted['surl']= BUY_PACK_PAYU.'?userId='.$userId;
						$posted['furl']= FAILURE_URL;
						$posted['id']= 1;
						$posted['salt']=$SALT;
						$posted['key']=$MERCHANT_KEY;
						if($amount){
							$posted['amount']= $amount;
						}else{
							$posted['amount']=0;
						}
						$this->session->set_userdata('amount',$posted['amount']);
						$posted['firstname']= $userName;
						//$posted['email']= "ashiyasyed419@gmail.com";
						$posted['email']= $emailAdress;
						//$posted['phone']= '8985716639';
						$posted['phone']= $phoneNumber;
						$posted['productinfo']= '111';
						$posted['service_provider']= 'payu_paisa';
						$posted['txnid']= $txnid;
						
						$hash = '';
					    $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
					    $hashVarsSeq = explode('|', $hashSequence);
						$hash_string = '';	
						foreach($hashVarsSeq as $hash_var) {
							
						  $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
						  $hash_string .= '|';
						}
						$hash_string .= $SALT;
						$hash = strtolower(hash('sha512', $hash_string));
						?>				
				 <!-- <form action="https://test.payu.in/_payment" method="post" name="payuForm" id="payuForm">-->
				  <form action=" https://secure.payu.in/_payment" method="post" name="payuForm" id="payuForm">
			      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
				  <input type="hidden" name="hash" value="<?php echo $hash ?>" id="hash"/>
				  <input type="hidden" name="txnid" value="<?php echo $txnid ?>" id="txnid"/> 
				  <input type="hidden" name="amount"  id="totalCost"  value="<?php echo $posted['amount'];?>" />
				  <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' :$posted['firstname']; ?>" />
				  <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" />
				  <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" />
				  <input  type="hidden" name="productinfo"  value="<?php echo (empty($posted['productinfo'])) ? '' :$posted['productinfo'] ?>" />
				  <input type="hidden" name="surl" value="<?php echo (empty($posted['surl'])) ? '' : $posted['surl'] ?>" size="64" />
				  <input type="hidden" name="furl" value="<?php echo (empty($posted['furl'])) ? '' : $posted['furl'] ?>" size="64" />
				  <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                <div class="col-md-3">
			   <div class="news_readmore text_decoration_no red_txt"  id="Error" > </div>
            	  <div class="table-responsive clearfix">           
                          <table class="table table-bordered">
                            <thead>
                            <tr class="coupon_box_title">
                                <th><img class="" src="<?php echo IMAGES_URL; ?>coupon_title_icon.png"> Checkout Details</th>
                            </thead>
                            <tbody>
                              <tr class="bg_white coupon_box_bg">
                                <td>
								<div class="coupon_box_price"><i class="fa fa-inr" aria-hidden="true"></i> <?php if($amount){
				 echo $amount;}else{echo 0;} ?></div>
                                <!--<p class="tble_blue_txt">Subtotal (<?php echo $cartCount;?> items):   <span class="tble_idle_txt ml15">₹  <?php echo $amount; ?></span></p>
                                <center><input type="submit" name="Buy Now" value="Check Out"  class="btn login_btn mt50 mb15"></input></center>
                                -->
								<center><input type="submit" name="Buy Now" id="submit" value="Check Out" class="btn login_btn mt15 mb15 mr0"></a></center>
                               
								
								</td>
                              </tr>
                          
				 
							
				<!--end-->
                      </tbody>
                          </table>
                  </div>
            </div>
			</form>
			<!--for paytm form --->
			<form action="<?php echo CCAVENUE_REQUEST_URL; ?>" method="post" name="paytm" id="paytm">
			<input type="hidden" name="amount"  id="totalCost"  value="<?php echo $posted['amount'];?>" />
			<input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" />
			<input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" />
			<input type="hidden" name="userId" value="<?php echo (empty($userId)) ? '' : $userId; ?>" />
			<input type="hidden" name="tests" value="<?php if($testIds){ print_r($testIds); } ?>">
			<input type="hidden" name="packages" value="<?php  if($testIds){ print_r($packageids); } ?>">
			<input type="hidden" name="amount" value="<?php echo $amount ?>">
			<div class="col-md-3">
			   <div class="news_readmore text_decoration_no red_txt"  id="Error" > </div>
            	  <div class="table-responsive clearfix">           
                          <table class="table table-bordered">
                            <thead>
                            <tr class="coupon_box_title">
                                <th><img class="" src="<?php echo IMAGES_URL; ?>coupon_title_icon.png"> Checkout Details</th>
                            </thead>
                            <tbody>
                              <tr class="bg_white coupon_box_bg">
                                <td>
								<div class="coupon_box_price"><i class="fa fa-inr" aria-hidden="true"></i> <?php if($amount){
				 echo $amount;}else{echo 0;} ?></div>
				 
                             <center><input type="submit" name="Buy Now" id="submit" value="Check Out" class="btn login_btn mt15 mb15 mr0"></a></center>
                             </td>
                           </tr>
                       </tbody>
                     </table>
                  </div>
            </div>
			
			</form>
			<!--end paytm ---->
			<?php }?>
			
        </div>
        </div>
    </section>
        
</div>
 </body>
<script>
$("#paytm").hide();
	 function showPaymentTypes(){
			 var select= $("#select").val();
			if(select == "coupon" ){
				
				$("#form1").show();
				$("#payuForm").hide();
				$("#paytm").hide();
            }
			if(select == "payu") {
				 $("#form1").hide();
				$("#payuForm").show();
				$("#paytm").hide();
			}
			if(select == "paytm") {
				 $("#form1").hide();
				$("#payuForm").hide();
				$("#paytm").show();
			}
	 }
</script>
 <script>
jQuery('#CouponCode').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;		
    this.value = this.value.replace(/[^a-zA-Z0-9  \s]/g,'');
	this.setSelectionRange(start, end); 
    });	
 $(document).ready(function() {
	$("#payuForm").hide();
$("#submit").attr('disabled', 'disabled');

});
</script>
</html>
