<html>
<?php if($details){?>
<section class="main_body">
<div class="container">
	<div class="thumb_panel">
                        	<div class="thumb_img pull-left" style="background:url(<?php echo IMAGES_URL ?>thumb_img.jpg) no-repeat top center;"></div>
                            <div class="thumb_txt blue_txt">
                            <p class="nm">*<?php echo $details->newsTitle;?><br>
                             <span><?php echo $details->description; ?></span></p>
                            <p class="nm"><span class="date_txt"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo date(' d ', strtotime($details->createdTime));?> - <?php echo date('  F  ', strtotime($details->createdTime));?> - <?php echo date('Y ', strtotime($details->createdTime));?></span> 
							<a class="news_readmore" href="<?php echo ACCOUNT_URL ?>"> <br>Back to Home..</a></p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
</div>
</section>
<?php }?>
</html>

