<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>
    <link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />

    <!-- Bootstrap -->
    <link href="<?php echo CSS_URL; ?>bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>font-awesome.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>record.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>image.css" rel="stylesheet">
     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="<?php echo JS_URL; ?>jquery.min.js"></script>
	</head>
<body>
<div id="wrapper">
    <!--Header !-->
<header id="header">
    	<nav class="navbar navbar-inverse2 navbar-fixed-top">
                <div class="container">
                        <div class="row">
                        <div class="col-sm-6 col-md-6 logo-brand">
                        <a href="<?php echo SITEURL; ?>"><img src="<?php echo IMAGES_URL; ?>logo.png" alt="Logo" title="OES"/>
                        ONYX Educational</a>
                        </div>
						</div>
                    <div class="clearfix"></div>
                </div>
            </nav>
    </header>
	