<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>
	<link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />
     <!-- Bootstrap -->
    <link href="<?php echo SITEURL?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/font-awesome.css" rel="stylesheet">
    <script src="<?php echo SITEURL?>/assets/js/jquery.min.js"></script>
</head>
<body><br><br><br><br>
<div id="wrapper">
 <div class="container cart" style="margin: 20px auto 60px;">
<h1>Copy right & Privacy policy</h1>
<div class="row">
<div class="col-xs-12 col-sm-12">
<h3 class="content">Copy Right Policy:</h3>
<p>All the data and graphics presented on this website are the property of info@onyx-ed.com. The pages may not be redistributed or reproduced in any way, shape,
 or form without the written permission of meeexam.com. We respects the copyrights, trademarks and intellectual property of others and also we expect this from other users. 
In this site, if you found any information that is owned by you or any content that violates your intellectual property rights, please contact to us with all necessary documents or information that authenticate your authority on your property. 
</p>
<h3 class="content">Privacy policy:</h3>
<p>The Personal information, email that submitted while registering to the site, will NOT be
distributed, shared with any other third-parties. We only use this data for our information, for
research, to improve our services and for contacting you purposes.</p>
</div>
</div>
</div>

    <!-- Footer 
	<footer id="footer2">
    <div class="container">
    <div class="row">
   <?php $this->load->view('links'); ?>
    <p>Copyright &copy; <?php echo date("Y"); ?>, All Rights Reserved.</p>
    </div>            
    </div>
    <div class="clearfix"></div>
    </footer>-->
      <?php $this->load->view('footer'); ?>
</div>
    
</body>
</html>