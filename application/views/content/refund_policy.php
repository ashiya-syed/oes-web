<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>
	<link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />
     <!-- Bootstrap -->
    <link href="<?php echo SITEURL?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/font-awesome.css" rel="stylesheet">
    <script src="<?php echo SITEURL?>/assets/js/jquery.min.js"></script>
</head>
<body><br><br><br><br>
<div id="wrapper">
 <div class="container cart" style="margin: 20px auto 60px;">
<h1>Refund and Cancellation Policy</h1>
<div class="row">
<div class="col-xs-12 col-sm-12">
<p>Amount once paid through the payment gateway shall not be refunded other than in the following circumstances: </p>
<ul><li>Multiple times debiting of Customer’s Card or Bank Account due to technical error OR Customer's account being debited with excess amount in a single transaction due to technical error.
 In such cases, excess amount excluding payment gateway charges would be refunded to the Customer.</li>
<li>Due to technical error, payment being charged on the Customer’s Card or Bank Account but the enrolment for the examination is unsuccessful. Customer would be provided with the enrolment by meeexam.com at no extra cost. However, if in such cases, customer wishes to seek refund of the amount, he or she would be refunded net the amount, after deduction of payment gateway charges or any other charges. </li>
</ul>
<p>The customer will have to make an application for refund along with the transaction number and original payment receipt if any generated at the time of making payments.</p>
</div>
</div>
</div>
</div>

    <!-- Footer 
	<footer id="footer2">
    <div class="container">
    <div class="row">
   <?php $this->load->view('links'); ?>
    <p>Copyright &copy; <?php echo date("Y"); ?>, All Rights Reserved.</p>
    </div>            
    </div>
    <div class="clearfix"></div>
    </footer>
    -->
	      <?php $this->load->view('footer'); ?>

</div>
    
</body>
</html>