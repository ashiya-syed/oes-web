<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>
	<link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />
     <!-- Bootstrap -->
    <link href="<?php echo SITEURL?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/font-awesome.css" rel="stylesheet">
    <script src="<?php echo SITEURL?>/assets/js/jquery.min.js"></script>
</head>
<body>
<div id="wrapper">
 <html>
<body>
	<div class="pad-B30 col-md-12 main_body">
        <div class="container">
        <div class="row">
		<div class="inner_form_panel">
			<div id="errors"><span style="color:red"><?php echo $this->session->flashdata('message');?></span></div>
			<div id="errors"><span style="color:red">  <?php echo $this->session->flashdata('forgotpassword');?></span></div>
             <?php if (isset($successMessage)) { ?>
				    <div class="alert alert-success alert-dismissable" style="font-size:15px;text-align:center;margin:10px 0; text-transform:uppercase;">
					 <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"></a>
					 <strong> <?php echo $successMessage; ?></strong>
					</div>
					<?php } ?>
					<?php if (isset($errorMessage)) { ?>
				    <div  style="font-size:15px;color:#ff0000; text-align:center;margin:10px 0; ">
					 <?php echo $errorMessage; ?>
					</div>
					<?php } ?>
			<h1 class="home_thumb_title m_l_r15"><img src="<?php echo IMAGES_URL; ?>forgot_password.png" style="margin-right:10px;">Contact us</h1>
			<?php $attributes = array('name' => 'loginform', 'id' => 'loginform','method' => 'post', 'class' => 'appt-form2', 'accept-charset' => 'UTF-8', 'role' => 'form');
						if(@$uId){echo form_open(CONTACT_US.'/'.@$uId,$attributes);}else{echo  form_open(CONTACT_US,$attributes);}
					?>
					 <div class="form-group clearfix">
					  <div class="col-sm-10">
						<center><input type="email" class="form-control login_field" id="email_id" name="email_id" value="<?php if(@$input['email_id']){ echo @$input['email_id'];} ?>" placeholder="Enter email" required>
						<input type="text" class="form-control login_field" id="mobile" name="mobile" maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="<?php if(@$input['mobile']){ echo @$input['mobile'];} ?>"placeholder="Mobile">
                         <textarea type="text" class="form-control login_field" id="message" name="message" value="<?php if(@$input['message']){ echo @$input['message'];} ?>" placeholder="Message" required><?php if(@$input['message']){ echo @$input['message'];} ?></textarea></center>
						<button type="submit" name="submit" value="submit" class="btn register_btn blue_btn mt25">Submit</button>
						 
					  </div>
					</div>
			</form>
			<div class="clearfix"></div>
		</div>
		</div>
		</div>
	</div>
</body>
</html>
    <!-- Footer -->
	<footer id="footer2">
    <div class="container">
    <div class="row">
   <?php $this->load->view('links'); ?>
    <p>Copyright &copy; <?php echo date("Y"); ?>, All Rights Reserved.</p>
    </div>            
    </div>
    <div class="clearfix"></div>
    </footer>
</div>
 </body>
</html>