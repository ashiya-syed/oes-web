<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>
	<link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />
     <!-- Bootstrap -->
    <link href="<?php echo SITEURL?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/font-awesome.css" rel="stylesheet">
    <script src="<?php echo SITEURL?>/assets/js/jquery.min.js"></script>
</head>
<body><br><br><br><br>
<div id="wrapper">
 <div class="container cart" style="margin: 20px auto 60px;">
<h1>Terms And Conditions</h1>
<div class="row">
<div class="col-xs-12 col-sm-12">
<h3 class="content">INTRODUCTION</h3>
<p>This website page states the Terms of Use under which You may use meeexam.com (the Site, Website, Web site). Please read these terms of Use, carefully, and if you do not accept them, do not use this Website or Service. By using the page, you are indicating your acceptance to be bound by these Terms of Use.
.</p>
<h3 class="content">REGISTRATION AND PASSWORD</h3>
<p>You are responsible for maintaining the confidentiality of your information and password. You shall be responsible for all uses of your registration, whether or not authorized by You. You agree to immediately notify the Company of any unauthorized use of your registration or password.
</p>
<h3 class="content">MODIFICATIONS</h3>
<p>meeexam.com. reserves the right to alter or amend this Agreement at any time, for any reason or may be for no reason at all,
at meeexam.com discretion. If any alteration or amendment occurs on the Agreement, it will be posted on the meeexam.com website. If any modifications or amendments takes place on the site, meeexam.com will provide notice of that material changes on the site but as a member or user of meeexam.com, it is user's sole responsibility to update yourself apprised of any modifications or amendments. If member objects to any terms and conditions of the Agreement or after any Modifications or Amendments or dissatisfied with meeexam.com in any way, member's only recourse to immediately:
<ul>
<li> Withdraw from the use of meeexam.com website.</li>
<li>Terminate registration from the meeexam.com website.</li>
</p>


</div>
</div>
</div>

    <!-- Footer -->
	<footer id="footer2">
    <div class="container">
    <div class="row">
   <?php $this->load->view('links'); ?>
    <p>Copyright &copy; <?php echo date("Y"); ?>, All Rights Reserved.</p>
    </div>            
    </div>
    <div class="clearfix"></div>
    </footer>
    
</div>
    
</body>
</html>