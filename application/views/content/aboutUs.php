<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>
	<link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />
     <!-- Bootstrap -->
    <link href="<?php echo SITEURL?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/font-awesome.css" rel="stylesheet">
    <script src="<?php echo SITEURL?>/assets/js/jquery.min.js"></script>
</head>
<body><br><br><br><br>
<div id="wrapper">
 <div class="container cart" style="margin: 20px auto 60px;">
<h1>About Us</h1>
<div class="row">
<div class="col-xs-12 col-sm-12">
Onyx Educational aims to be a powerful examination tool for e-Learning and
online education. Useful for, Mining Engineering students to practice their exams. Go
wherever you want to and practice whenever you want, using the Onyx Educational
online exam platform. Experience a lag-free synchronized performance Extensive range
of high quality practice tests with thousands of questions and their answers as per latest
exam pattern. Complete set of practice and Main tests needed for all exams.
</div>
</div>
</div>
<!-- Footer 
<footer id="footer2">
    <div class="container">
    <div class="row">
   <?php $this->load->view('links'); ?>
    <p>Copyright &copy; <?php echo date("Y"); ?>, All Rights Reserved.</p>
    </div>            
    </div>
    <div class="clearfix"></div>
</footer>-->
 <?php $this->load->view('footer'); ?>

</div>
    
</body>
</html>