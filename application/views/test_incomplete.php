<!DOCTYPE html>
<html lang="en">
 <link href="<?php echo CSS_URL; ?>bootstrap.min.css" rel="stylesheet">
 <link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet">
 <link href="<?php echo CSS_URL; ?>font-awesome.css" rel="stylesheet">
<head>
</head>
<body>
<div id="wrapper">
    <!--Body!-->
    <section class="main_body">
        <div class="profile">
            <h1>Test summary</h1>
				<ul class="report_text">
                    <li><img src="<?php echo IMAGES_URL ?>cross.jpg" class="img-responsive"></li>
                    <li>
					
					<p class="red_txt">Your Test is Incomplete</p>
					<!--<p> The summary for the test is given below:</p>-->
					
                    <p><span>Test Name:</span> <?php echo $testname->testId ?>. <?php echo $testname->testName ?></p>
                    <!--<p><span>Subject Name:</span> APPSC Sample Exam</p>-->
                    </li>
                </ul>
				<a type="button" href="#" result="allow" onclick="return CloseMySelf(this);" class="btn register_btn red_btn mt25">close</a>

        </div>
    </section>
</div>

</body>
</html>

<script type="text/javascript">
history.pushState({ page: 1 }, "Title 1", "#no-back");
window.onhashchange = function (event) {
  window.location.hash = "no-back";
};
function CloseMySelf(sender) {
    try {
        window.opener.HandlePopupResult(sender.getAttribute("result"));
    }
    catch (err) {}
    window.close();
    return false;
}

</script>
