<!DOCTYPE html>
<html lang="en">
 <link href="<?php echo CSS_URL; ?>bootstrap.min.css" rel="stylesheet">
 <link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet">
 <link href="<?php echo CSS_URL; ?>font-awesome.css" rel="stylesheet">
  <script src="<?php echo JS_URL; ?>jquery.min.js"></script>
<head>
</head>
<body>
<div id="wrapper">
    <!--Body!-->
    <section class="main_body">
        <div class="profile">
            <h1>Test summary</h1>
				<ul class="report_text">
                    <li><img src="<?php echo IMAGES_URL ?>tick.png" class="img-responsive"></li>
                    <li>
					<?php if($status == TEST_PENDING){?> 
					<p class="green_txt">Your Test is Incomplete</p>
					<p> The summary for the test is given below:</p>
					<?php }else{?>
                    <p class="green_txt">Your Test Submitted Successfully</p>
                    <p>Thank you for submitting your test. The summary for the test is given below:</p>
					<?php }?>
                    <p><span>Test Name:</span> <?php echo $testname->testName ?></p>
                    <!--<p><span>Subject Name:</span> APPSC Sample Exam</p>-->
                    </li>
                </ul>
                <div class="table-responsive clearfix">          
                          <table class="table table-bordered">
                            <thead>
                              <tr class="tble_clr2">
                                <th width="9%">Total Questions</th>
                                <th width="9%">Maximum Marks</th>
                                <th width="9%">Total Attempted</th>
                                <th width="9%">Left Questions</th>
                                <th width="9%">Correct Ans.</th>
                                <th width="9%">InCorrect Ans.</th>
                                <th width="9%">Total Time(in min.)</th>
                                <th width="9%">Time Taken</th>
                                <th width="9%">Right Marks</th>
                                <th width="9%">Negative Marks</th>
                                <th width="9%">Total Marks</th>
                              </tr>
                            </thead>
                            <tbody>
							<?php if($attemptedQuestions){$attemptedQuestions=count($attemptedQuestions);}else{$attemptedQuestions=0;}
							    /* $minutes= gmdate('H ', $timeTaken * 3600).' min';
                                 $seconds= gmdate('i ', $timeTaken * 3600).' sec';
								 $time=$minutes.' '.$seconds;*/
								  $time="-";
								  if($timeTaken){
									$h=$timeTaken['h'];
									$m=$timeTaken['m'];
									$s=$timeTaken['s'];
									$time= $h.'h '. $m .'m '. $s.'s ';
								}
							?>
                              <tr class="text-center">
                                <td><?php echo $totalQue;?></td>
                                <td><?php echo ($totalQue*($testname->rightMarks));?></td>
                                <td><?php echo $attemptedQuestions;?></td>
                                <td><?php echo $leftQuestions;?></td>
                                <td><?php echo $correctAns;?></td>
                                <td><?php echo $incorrectAns;?></td>
                                <td><?php if(@ $timings->testTime){echo @$timings->testTime;}else{echo '0';}?></td>
                                <td><?php echo @$time;?></td>
								<td class="tble_grn_txt"><?php echo $testname->rightMarks?></td>
                                <td class="tble_red_txt"><?php echo $testname->negativeMarks?></td>
                               
								<?php  $marks=($correctAns*($testname->rightMarks))-($testname->negativeMarks*($incorrectAns)) ;?>
                                <td><?php echo $marks;?></td>
                              </tr>
                            </tbody>
                          </table>
                  </div>
				  <?php 
				  $testId=$testname->testId;
				  $attempts=$attempts;
				  $type=$testType;
				  $packageiD= ($packageiD)?$packageiD:0;
				  $uniqueId= ($uniqueId)?$uniqueId:0;
				 ?>
				
                <a type="button" href="<?php echo VIEW_RECORD_URL.'?testId='.$testId.'&attempts='.$attempts.'&type='.$testType.'&package='.$packageiD.'&userPackageId='.$uniqueId.'&TY=single';?>" class="btn register_btn blue_btn mt25">view report</a>
                <a type="button" href="#" result="allow" onclick="return CloseMySelf(this);" class="btn register_btn red_btn mt25">close</a>

        </div>
    </section>
</div>

</body>
</html>
<script type="text/javascript">
$(document).ready(function(){ 
  $(document).oncontextmenu = function() {return false;};
  $(document).on('keydown',function(e)
  { 
    var key = e.charCode || e.keyCode;
	if(key == 122 || key == 123 || key == 27 || key == 112 || key == 114 || key == 115 || key == 116 || key == 144 || key == 82 || key == 18 || key == 17 || key == 20 || key == 9 )
        {return false; }
   
  });
});
   
$(document).ready(function () {
   //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });//for cntrl+p
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });//cntrl+u
</script>
<script type="text/javascript">
history.pushState({ page: 1 }, "Title 1", "#no-back");
window.onhashchange = function (event) {
  window.location.hash = "no-back";
};
function CloseMySelf(sender) {
    try {
        window.opener.HandlePopupResult(sender.getAttribute("result"));
    }
    catch (err) {}
    window.close();
    return false;
}
function copyToClipboard() {
	var aux = document.createElement("input"); 
  aux.setAttribute("value", "Dont take screenshot.");
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
  document.body.innerHTML ="<br><br><br><div style='color: #FF0000; font-size: 36px;text-align: center;'>Screenshorts are restricted for this site.</div>";
 // alert("Screenshots are restricted.");
}

$(window).keyup(function(e){
  if(e.keyCode == 44){
    copyToClipboard();
  }
});
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>

