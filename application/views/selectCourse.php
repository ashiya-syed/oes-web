<!DOCTYPE html>
<html lang="en">
  <link href="<?php echo SITEURL ?>/assets/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo SITEURL ?>/assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo SITEURL ?>/assets/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<style>
.select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field {
   min-width: 21em;
}
</style>
<body>
<div id="wrapper">
    <!--Body!-->
    <section class="main_body">
        <div class="container">
        <div class="row profile">
            <h1>Select Courses</h1>
			<?php echo $this->session->flashdata('message'); ?>
            <div class="profile_tab">
            	Select courses
            </div>
			<form method="post" action="">
			  <div class="profile_details">
			  
				<div class="col-sm-6">
 <label class="control-label col-sm-3" for="password"> Course Name</label>					
 <select id="test"  onchange="getTotalAmount()" class="form-control login_field select2 col-sm-12" name="course[]" multiple>
                                <option value="">Select courses</option>
								<?php 
									$selected=array();
									if($courses && count($courses) > 0){ 
									foreach( $courses as $key=>$val ){
										$selected[$key] = $val->courseId;
										 }
										 }
									if($courses && count($courses)>0){
										foreach($courses as $key=>$val){
											
										$select="";
										if(in_array($val->courseId,$selectedCourses)){
											$select="selected";
										}
									 ?>
                                   <option value="<?php echo $val->courseId ?>" <?php echo $select; ?>><?php echo $val->courseName ?></option>
									<?php } }  ?>
					</select>
					<button type="submit" name="save" value="save" class="btn register_btn blue_btn mt50">Save</button>
                </div>
				
                <!--/div-->
                <div class="clearfix"></div>    
            </div>
			</form>
		
           
        </div>
        </div>
    </section>
</div>
        <script src="<?php echo SITEURL ?>/assets/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/js/app.min.js" type="text/javascript"></script>
       <script src="<?php echo SITEURL ?>/assets/js/components-select2.min.js" type="text/javascript"></script>
</body>
</html>

