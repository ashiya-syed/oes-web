<!DOCTYPE html>
<html lang="en">
<head>
<style>
#footer{
	position:relative;
}
</style>
<body>
<div id="wrapper">
    <!--Header !-->
    <<!--Body!-->
    <section class="main_body">
        <div class="container">
		<?php echo $this->session->flashdata('message'); ?>
		<div class="news_readmore text_decoration_no red_txt" id="RECORDS_MESSAGE"></div>
        <div class="row cart">
        	<div class="col-md-12">
            <h1>User Details</h1>
             <?php   $userName=$emailAdress=$phoneNumber=$testIds=$packageIds='';  
	         $userId = 0; 
             if($userdetails){ //debug($userdetails);
				  $userId=$userdetails->userId;
				  $userName=$userdetails->userName;
				  $userVerified=$userdetails->is_user_verified;
				  $this->session->set_userdata('userName',$userName);
                  $emailAdress=$userdetails->emailAddress;
				  $phoneNumber=$userdetails->phoneNumber;
				  $testIds=$userdetails->testIds;
				  $resellerId=$userdetails->resellerId;
				  $tids=explode(',',$testIds);
				  $packageIds=$userdetails->packageIds;
				  $pIds=explode(',',$packageIds);
			}
 ?>
			</div>
			<div class="col-sm-4">
				<div class="user_details_thumb clearfix" style="border-left:1px solid #367fa8;">
					<div class="u_d_t_img"><img class="img-responsive" src="<?php echo IMAGES_URL; ?>account_user_icon.png"></div>
					<div class="u_d_t_txt"><?php echo $userName; ?></div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="user_details_thumb clearfix" style="border-left:1px solid #f39c11;">
					<div class="u_d_t_img"><img class="img-responsive" src="<?php echo IMAGES_URL; ?>account_email_icon.png"></div>
					<div class="u_d_t_txt"><?php echo $emailAdress; ?></div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="user_details_thumb clearfix" style="border-left:1px solid #6cb242;">
					<div class="u_d_t_img"><img class="img-responsive" src="<?php echo IMAGES_URL; ?>account_call_icon.png"></div>
					<div class="u_d_t_txt"><?php echo $phoneNumber; ?></div>
				</div>
			</div>
			
			<div class="col-md-9">
				<div class="package_details clearfix">
					<div class="col-sm-6"><h2><img class="" src="<?php echo IMAGES_URL; ?>package_title_icon.png"> Packages/Test Details</h2></div>
					<div class="col-sm-3">
					<select class="form-control login_field select-down-new mb0 mt8" id="Filter" onchange ="showPakcgesortests();" style="border: 1px solid #b2b2b2 !important; background-color:#FFF;">
						<option value= "Package"> Packages </option>
			            <option value= "Test"> Tests </option>
					</select>
					</div>
					<div class="col-sm-3">
					<input type="name" class="form-control login_field mb0 mt8" id="name" placeholder="Search" onkeyup="getTestOrPackgesByKeyword($(this).val(),<?php echo $resellerId;?>,<?php echo $userId ?>)">
					</div>
				</div>
				
				<div class="package_details_box" id="packages">
				<div  id="packageDIV">
				<?php 
				if($packages && count($packages)>0){
					foreach($packages as $package){
					 $class = "label label-info";
					 if(in_array($package->package_id,$pIds)){
						 $check="checked";
					  }else{
						$check="";
					}
				
				?>
		  <div class="checkbox col-sm-2 col-xs-4" id="">
		  <label><input type="checkbox" <?php echo $check;?> name="packages[]" value="<?php  echo $package->package_id ?>" onclick="getPackageAmount('<?php  echo $package->package_id ?>','<?php  echo $package->package_name; ?>','<?php  echo $package->package_amount; ?>',  this.checked ? 1 : 0);"> 
		  <a href="#TestsByPackagePopUp" data-toggle="modal" onclick="getTestsByPackageId('<?php  echo $package->package_id; ?>')" id="package"><?php echo $package->package_name ?></a>
				 </label></div>
				<?php    } }  ?>
			
		  </div>
		</div>
		<?php //debug($tids); ?>
			<div class="package_details_box" id="tests" style="display:none">
				<div  id="testDIV">
		 <?php		
					if($tests && count($tests)>0){
					foreach($tests as $test){ 
					  $class = "label label-info";
						 if(in_array($test->testId,$tids)){
					       $check="checked";
				          }else{
					       $check="";
				          }
						
		?>
		<div class="checkbox col-sm-2 col-xs-4" id=""><label><input type="checkbox" <?php echo $check;?> name="tests[]" value="<?php  echo $test->testId ?>" onclick="getTestAmount('<?php  echo $test->testId ?>','<?php  echo $test->testName; ?>','<?php  echo $test->testAmount; ?>',  this.checked ? 1 : 0);"><font color="white"><?php echo $test->testName ?></font> </label>
				</div><?php    } } ?>
			
		</div>
				</div>
            </div>
            <div class="col-md-3">
            	<div class="table-responsive clearfix">          
                          <table class="table table-bordered">
                            <thead>
                              <tr class="coupon_box_title">
                                <th><img class="" src="<?php echo IMAGES_URL; ?>coupon_title_icon.png"> ENTER Coupon</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr class="bg_white coupon_box_bg">
                                <td>
								<?php
								  if($totalAmount){
											$posted['amount']=$totalAmount;
										}else{
											$posted['amount']=0;
										}
								   $url=current_url();
								   $token=$_GET['token'];
								   $url.='?token='.$token;

								  ?>
								<form name="form1" method="post" action="<?php echo BUY_PACKAGE_OR_TEST_URL; ?>">
								<input type="hidden" name="url" value="<?php echo $url;?>">
			                    <input type="hidden" name="amount" value="<?php echo $posted['amount']; ?>">
			                    <input type="hidden" name="userId" value="<?php echo $userId ?>">
								<div class="coupon_box_price"><i class="fa fa-inr" aria-hidden="true"></i> <?php if($totalAmount){
				 echo $totalAmount;}else{echo 0;} ?></div>
                                <input type="name" name="CouponCode" id="CouponCode" maxlength="8" onkeyup="checkCoupondalidity(<?php echo $posted['amount']; ?>,<?php echo $userId; ?>)" class="form-control login_field coupon_input" id="name" placeholder="ENTER COUPON CODE">
                                <center><input type="submit" name="submit" id="submit" value="APPLY CODE" class="btn login_btn mt15 mb15 mr0"></a></center>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                  </div>
            </div>
			<div class="clearfix"></div>
					   <center> <button type="button" class="btn login_btn mt25 mb15"  id="ghgh" onclick="add_user_details(<?php echo $userId; ?>)" >SAVE</button></center>

        </div>
		<div class="modal fade" id="TestsByPackagePopUp" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog"  id="_edit_TestsPackages_div" style="margin-top:80px;">
                      	</div>
            </div>
        </div>
    </section>
        
</div>
<script>
jQuery('#CouponCode').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;		
    this.value = this.value.replace(/[^a-zA-Z0-9  \s]/g,'');
	this.setSelectionRange(start, end); 
    });	
 $(document).ready(function() {
	
$("#submit").attr('disabled', 'disabled');

});

</script>
   
</body>
</html>