<html>
<head>
<script src="<?php echo JS_URL; ?>jquery-1.9.1.min.js"></script>
<link href="<?php echo CSS_URL; ?>bootstrap.min.css" rel="stylesheet">
<link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet">
<link href="<?php echo CSS_URL; ?>font-awesome.css" rel="stylesheet">
</head>
<body>
<div class="table-responsive clearfix"  width="50%">          
                          <table class="table table-bordered">
                            <thead>
                              <tr class="tble_clr1">
                                <th colspan="3">
                                   <?php echo $packageName; ?> Details 
                                </th>
                              </tr>
							  
                              <tr class="tble_clr2">
                                <th width="12%">Test Name</th>
                                <th width="25%"> Amount</th>
                                <th width="15%"> Attempts</th>
                               </tr>
                            </thead>
                            <tbody>
                              <?php if($testDetails){ 
							    foreach($testDetails as $test){?>
								 <tr class="text-center">
                                <td><?php echo $test->testName;?></td>
                                <td><?php echo $test->testAmount.' Rs';?></td>
                                <td><?php echo $test->attempts; ?></td>
                               </tr>
							<?php	} } else{ ?>
								<div class="news_readmore text_decoration_no red_txt"> <?php echo TESTS_NOT_FOUND ;?> </div>
						<?php	} ?>
                            </tbody>
                          </table>
                  </div>
				  </body>
<script>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  
   }
});
</script>
</html>