<!DOCTYPE html>
<?php
$TY=$_GET['TY'];
 $URL=QUESTION_SUMMARY_URL.'?testId='.$_GET['testId'].'&type='.$_GET['type'].'&package='.$_GET['package'].'&userPackageId='.$_GET['userPackageId'].'&attempts='.$_GET['attempts'].'&TY='.$TY; 
 ?>
<html lang="en">
<body>
<div id="wrapper">
   <section class="main_body">
        <div class="container">
        <div class="row profile">
            <h1>Test Analysis Report</h1>
				<ul class="report_text">
                    <li><img src="<?php echo IMAGES_URL ?>report_icon.png" class="img-responsive"></li>
                    <li>
                     Score Card For Test :
                    <span> <?php echo $testname->testName?></span>
                    </li>
                </ul>
                 	<div class="table-responsive mt25 clearfix">          
                              <table class="table table-bordered">
                                <thead>
                                  <tr class="tble_clr1">
                                    <th colspan="4">
                                        Your test details
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
								<?php @$percentile=round((($correctAns/$totalQue)*100),2);$time='';$idltime='';
								/*if($timeArray){$h=$timeArray[0];
									$m=$timeArray[1];
									$s=$timeArray[2];
								    $time= $h. 'h '. $m .'m '. $s.'s ';
								}*/
                               $TY=$_GET['TY'];
								if($timeTaken){
									$h=$timeTaken['h'];
									$m=$timeTaken['m'];
									$s=$timeTaken['s'];
									$time= $h.'h '. $m .'m '. $s.'s ';
								}
								if($idleTime){
									$h=$idleTime['h'];
									$m=$idleTime['m'];
									$s=$idleTime['s'];
									$idltime= $h. 'h '. $m .'m '. $s.'s ';
								}
								      $incorrect=$marks=0;
								      if($attemptedQuestions){
                                      $incorrect=count($attemptedQuestions)-$correctAns;
									  }
									 if($correctAns || $incorrect){
										  $marks=($correctAns*($testname->rightMarks))-($testname->negativeMarks*($incorrect));
									  }
										?>
                                  <tr>
                                    <td width="25%">Total No. of Questions <span class="pull-right tble_blue_txt"><?php if(@$totalQue){ echo @$totalQue;}else{echo "0";} ?></span></td>
                                    <td width="25%">My Marks <span class="pull-right tble_blue_txt"><?php if($marks){echo $marks;}else{echo 0;}?></span></td>
                                    <td width="25%">Correct Answers <span class="pull-right tble_blue_txt"><?php echo $correctAns;?></span></td>
                                    <td>Incorrect Answers<span class="pull-right tble_blue_txt"><?php echo $incorrect;?></span></td>
                                  </tr>
                                  <tr>
                                    <td width="25%">Total Marks for this Test <span class="pull-right tble_blue_txt"><?php echo $totalQue*($testname->rightMarks);?></span></td>
                                    <td width="25%">My Percentile<span class="pull-right tble_blue_txt"><?php echo $percentile ?>%</span></td>
                                    <td width="25%">Right Marks<span class="pull-right tble_blue_txt"><?php echo $testname->rightMarks?></span></td>
                                    <td>Negative Marks<span class="pull-right tble_blue_txt"><?php echo $testname->negativeMarks?></span></td>
                                  </tr>
                                  <tr>
                                    <td width="25%">My Questions in Test<span class="pull-right tble_blue_txt"><?php if($attemptedQuestions) echo count($attemptedQuestions); else echo 0; ?></span></td>
                                    <td width="25%">My Rank <span class="pull-right tble_blue_txt">0</span></td>
                                    <td width="25%">Left Questions <span class="pull-right tble_red_txt"><?php echo $leftQuestions;?></span></td>
                                    <td>Left Question Marks<span class="pull-right tble_red_txt"><?php echo ($leftQuestions*($testname->rightMarks));?></span></td>
                                  </tr>
                                  <tr>
                                    <td width="25%">Total Time of Test <span class="pull-right tble_blue_txt"><?php if($timings->testTime){ echo @$timings->testTime;}else{echo 0;}?> minutes</span></td>
                                    <td width="26%">My Time<span class="pull-right tble_blue_txt"> <?php if($time){ echo @$time;}else{echo 0;}?></span></td>
                                    <td width="26%">Unproductive Time<span class="pull-right tble_idle_txt"><?php echo $idltime; ?></span></td>
                                    <td>Idle Time<span class="pull-right tble_idle_txt"><?php echo $idltime;?></span></td>
                                  </tr>
                                </tbody>
                              </table>
                          </div>

                           <?php if($TY=="multiple"){?>
						         <div class="col-sm-7 np login_area">
                               <center>
                                <a type="button" href="<?php echo ACCOUNT_URL; ?>" class="btn register_btn blue_btn mt25"><i class="fa fa-home" aria-hidden="true"></i> Back to Home</a> <?php }else{ ?>
								<a type="button" href="#" result="allow" onclick="closeWindow();" class="btn register_btn red_btn mt25">close</a><?php } ?>
                               <a type="button" href="<?php echo $URL; ?>" class="btn register_btn blue_btn mt25">Summary Report</a></center>
                              <!--  <center><a type="button" href="<?php //echo DASHBOARD_URL; ?>" class="btn register_btn blue_btn mt25"><i class="fa fa-home" aria-hidden="true"></i> Back to Home</a></center>-->
                </div>
           </div>
    </section>
 </div>
</body>
</html>
<script type="text/javascript">
function closeWindow(){
	window.close();
	window.opener.clearDiv();
window.opener.location.reload();
	
}
function clearDiv(){
	 window.location="<?php echo ACCOUNT_URL ?>";
}
$(document).ready(function(){ 
  $(document).oncontextmenu = function() {return false;};
  $(document).on('keydown',function(e)
  { 
    var key = e.charCode || e.keyCode;
	if(key == 122 || key == 123 || key == 27 || key == 112 || key == 114 || key == 115 || key == 116 || key == 144 || key == 82 || key == 18 || key == 17 || key == 20 || key == 9 )
        {return false; }
   
  });
});
</script>
<script type="text/javascript">
   $(document).ready(function () {
   //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });
</script>