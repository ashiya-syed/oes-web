<!DOCTYPE html>
<br><br>
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML">
</script>
<html lang="en">
<?php  

        $page_url=current_url();$examid=$this->uri->segment(4);$uniqueid=$this->uri->segment(8);
        $package=$this->uri->segment(6);if($details){$OPT=$details->optedOption;}
		$userId=$this->session->userdata('user_id');$attempts=$this->session->userdata('attempts');
		$testType=$this->session->userdata('testType');
?> 
 
<body>

  <form name="paperFrm" id="paperFrm" method="post" action="">
     <div class="exam_timer" > <input type="hidden" name="demo" id="demo" value=""></div>
  </form>
<!--Body!
<marquee bgcolor="#bfcee5" direction="left" style="color:red;font-size: 19px;font-style: italic;"><i>Don't close the window while the exam is running if you close the window your test will submitted automatically.</i></marquee>
--><section class="main_body">

 <div class="container">
 
<?php if($details){?>
<!--<div class="exam_timer" id="demo">Time Remaining: <span id="hours"></span><span id="minutes"></span><span id="seconds"></span></div>
		-->
<div class="timer">
 <div class="exam_timer"  id="demo">Time Remaining:
 <span class="time" id="countdown"></span> <span class="sub-text" id="countdown1">60 s</span>
 </div>
</div>
		<div class="row exam_question" id="question_module">
		  <div class="col-md-9">
            	<div class="examquestion_module ">
                    <ul class="exam_title">
                    <li><?php echo $testname->testId;?>.<?php echo $testname->testName ?> </li>
                   <li class="pull-right"><span style="color:#feee66;">Maximum Mark - <?php echo $testname->rightMarks  ?></span><span style="color:#dd3e3e;">Negative Mark - <?php echo $testname->negativeMarks ?></span></li>
                    </ul>
                    <div class="clearfix"></div>
					
                    <div class="question_module" >
					<?php 
					@$Opted=$details->optedOption->option_id;
								 $selected=array();
								 $selectedOptions = explode(',', $Opted);
								 if($selectedOptions && count($selectedOptions) > 0){ 
										foreach( $selectedOptions as $key=>$val ){
													$selected[$key] = $val;
										}
								}
						?>
						<p><span>Q. <?php echo @$ques;?>-</span><b> <?php echo @$details->que; ?></b></p>
						
						<div id="showHint"><b>Hint : </b> <?php if(@$details->hint){echo @$details->hint;}else{echo '<p class="text-danger">Found no hints for this Question.</b>';}?> </div>
						<div id="opt">
						<?php 
						if($details->queType == 2){
							$queType=2;
							/* $this->load->model('Testsmodel');
						    $where=array('que_id'=>$details->que_id,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueid,'isActive'=>1,'queType'=>2);
		                    $ans=$this->Testsmodel->getRecord(TBL_EXAM_RESULTS,$where); */
							$ans="";
							?>
							<div class="checkbox mt25">
							Enter Answers :  <input type="text" style="border: 0;outline: 0;background: transparent;border-bottom: 1px solid black;"  class="chb" id="textOption" name="option[]" value="<?php echo $ans; ?>" >
							</div>
						
						<?php }elseif($details->queType == 3){
							 $queType=3;
							/*  $this->load->model('Testsmodel');
							 $where=array('que_id'=>$details->que_id,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueid,'isActive'=>1,'queType'=>2);
		                     $ans=$this->Testsmodel->getRecord(TBL_EXAM_RESULTS,$where); */
							 $ans="";
							 ?>
							Select Answers :  
							<div id="roll">
					        <h4 class="modal-title" id="myModalLabel" ><input type="radio" id="r1" name="role" <?php if($ans == "True") echo "checked"; ?> value="True">True &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           <input type="radio" id="r2" name="role"  value="False" <?php if($ans == "False") echo "checked"; ?> >False</h4><br>
                            </div>
						<?php
						}else{
						 $options=@$details->options;
						 if(@$options){
							 $cc="";
							 $count=0;
							 foreach($options as $opt){
								 $cc=$opt->is_answer;
								 if($cc == 1){$count=$count+1;}
							 }
									 foreach($options as $key=>$val){
										 $textType=$val->textType;
										/* if (in_array($val->option_id ,$selected))
										{
												$select="checked";
										}	else {
												$select="";
								        } */
										$select="";
										
							 ?>
                    	 <div class="checkbox mt25">
						 <label><input type="checkbox"  class="chb" id="check" name="option[]"  value="<?php echo $val->option_id; ?>" <?php echo $select ?> >
						 <?php if($textType == 0 || $textType == 1 ){
							 echo $val->option;
						 }else if($textType == 2){
							 $src=UPLOADS_OPTIONS_PATH.$val->option;
							 echo "<div class='zoomin'><img src=$src width='90' height='45'></div>";
						 }else if($textType == 3){
							  $src= UPLOADS_OPTIONS_PATH.$val->option;
							 echo "<video width='110' height='95' controls> <source src=$src ></video>";
						 }else{
							 $src= UPLOADS_OPTIONS_PATH.$val->option;
							 echo "<audio width='110' height='95' controls> <source src=$src ></audio>";
						 }
						 
						 ?></label>
                        </div>
						
						<?php }}}?>
                        </div>
						<button id="hint" >Hint</button>
						<?php if($details->queType == 1 || $details->queType == 0){
							if(@$count == 1){
							?>
						<button   onclick="getFiftyFiftyOptions(<?php echo $examid ?>,<?php echo $details->que_id; ?>,0,0)"  >Take 50-50 option</button>
						<?php } } ?>
						<?php if($details->queType == 2){?>
						<button > save </button><?php } ?>
                        </div>
					<!--<div class="col-sm-6 btn_grp">
					       <button id="reviewnext" onclick="next(<?php echo @$details->que_id;?>,1,1,<?php echo @$ques+1 ?>)" class="btn register_btn blue_btn">Review & Next</button>
                            <button id="reviewprev" onclick="privious(<?php echo @$details->que_id;?>,3,1,<?php echo @$ques-1?>)" class="btn register_btn blue_btn">Review & Previous</button>
                           <button id="review" type="button" href="#agentDetailsPopup" data-toggle="modal" onclick="getReviewQuestions('<?php echo $examid;?>','<?php echo $package ?>')"   class="btn register_btn blue_btn" >Review Questions</button>
                        </div>-->
                        <div class="col-sm-6 text-right btn_grp">
						
                            <button id="previous"   class="btn register_btn blue_btn btn2" onclick="privious(<?php if($details){echo $details->que_id;}?>,3,0,<?php echo $ques-1?>);"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Previous</button>
                            <button type="button"  id="next" class="btn register_btn blue_btn btn2" onclick="next(<?php echo @$details->que_id;?>,2,0,<?php echo @$ques+1 ?>);"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Next</button>
							<!--<button id="save" class="btn register_btn blue_btn btn2"  onclick="save(<?php //echo $details->que_id;?>,<?php //echo $examid ?>);"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> save</button>-->
                        </div>
                        <div class="clearfix"></div>
                        
                </div>
            </div>
			 <div class="col-md-3">
            	<div class="examquestion_module">
                    <ul class="exam_title text-center">
                    <li>Review Questions</li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="question_module scrollbar">
                    <ul class="qstn_number">
					   <?php 
					   if($ReferenceTestIdArray){for($i=0;$i<count($ReferenceTestIdArray);$i++){$j=$i+1;
						?><a href="javascript:void(0)" onclick="next(<?php echo $ReferenceTestIdArray[$i]?>,2,0,<?php echo $j;?>)"><li class="active" id="num<?php echo $j;?>"><?php echo $j;?></li></a>
							<?php
					   } }?>
                     </ul>
                    </div>
                   <!-- <p style="background:#eee; color:#0C0;padding:5px 10px;margin-bottom: 1px;">Attempted -<?php echo count($attempted)?></p>
                    <p style="background:#eee; color:#F00;padding:5px 10px;margin-bottom: 0px;">Unattempted - <?php echo (count($questionNumbers))-(count($attempted));?></p>-->
                        <div class="clearfix"></div>
                </div>
            </div>
        </div>
 <center></center>
		<?php }else{ echo "<span class='text-center' style='color:red'>".NO_RECORDS_EXISTS."</span>";
		}?>
        </div>
</section>
<div class="modal fade" id="agentDetailsPopup" tabindex="-1" role="basic" aria-hidden="true">
<div class="modal-dialog"  id="_agent_details_div" style="margin-top:80px;">
</div>
</div>	
<?php $duration=$testname->testTime; 
  if(@$time){$duration=$time; } ?>
  <script type="text/javascript">
var userInput = '<?php echo $duration*60 ?>';
var numericExpression = /^[0-9]+$/;

function display( notifier, str ) {
document.getElementById(notifier).innerHTML = str;
}
function second( x ) {
document.getElementById('demo').value = x;
return Math.round( x%60)+' s ';
}
function minute( x ) {
document.getElementById('demo').value = x;
return Math.floor(x/60) +' m ';
}

function setTimer( remain, actions ) {
( 
function countdown() {
display("countdown", minute(remain));         
display("countdown1", second(remain));         
actions[remain] && actions[remain]();
(remain -= 1) >= 0 && setTimeout(countdown, 1000);
})();
}

setTimer(userInput, {
0: function () {
testfn();

return false;
}
});
</script>
<script type="text/javascript">

$(document).ready(function(){  
	$("#showHint").hide();
	$("#hint").click(function(){
        $("#showHint").toggle();
    });
	
  $("#reviewprev").hide();
   var que=<?php echo $ques;?>;
   var count=<?php echo  count($questionNumbers);?>;
    
  
	 if(que == count){
		$("#next").attr('disabled', 'disabled');
		
   }
   if(que == 1 ){
		$("#previous").attr('disabled', 'disabled');
		$("#reviewprev").hide();
	}
	if(que == 1 && count == 1){
		$("#previous").attr('disabled', 'disabled');
		$("#next").attr('disabled', 'disabled');
		
	}
   if(count == 1 ){
	  
	}
   if (que > 1 && que < count) {$("#previous").removeAttr('disabled');}
  
});
</script>

<script type="text/javascript">
/* $(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
 $(document).ready(function(){ 
  $(document).oncontextmenu = function() {return false;};
  $(document).on('keydown',function(e)
  { 
    var key = e.charCode || e.keyCode;
	if(key == 122 || key == 123 || key == 27 || key == 112 || key == 114 || key == 115 || key == 116 || key == 144 || key == 82 || key == 18 || key == 17 || key == 20 || key == 9 )
        {return false; }
   
  });
});
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });//for cntrl+p
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 67){  e.preventDefault(); } });//cntrl+c
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });//cntrl+u
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 86){  e.preventDefault(); } });//cntrl+v
jQuery(document).bind("keyup keydown", function(e){ if(e.altKey && e.keyCode == 9){  e.preventDefault(); } });//alt+tab
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 83){  e.preventDefault(); } });//cntrl+s
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 65){  e.preventDefault(); } }); //cntrl+a
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 90){  e.preventDefault(); } }); //cntrl+z
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 89){  e.preventDefault(); } }); //cntrl+y
  */

function next(index,type,review,order){ 
var currentUrl='<?php echo current_url(); ?>';  
var test='<?php  echo $examid ?>';
var testType = '<?php  echo $testType ?>';
	$("#showHint").hide();
	 var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });
	 $.ajax({
				   type:'POST',
				   url:"<?php echo ADMIN_QUESTIONPAPER_URL ?>",
				   data:{index:index,type:type,val:val,review:review,order:order,test:test,testType:testType},
				   success:function(t){
                      $('body').removeClass('modal-open');
					  //$('#num'+52).focus();
                      $('.modal-backdrop').remove();
					  $('#question_module').html(t);
					}
				   });
 return false; 
}
function privious(index,type,review,order){
	var test='<?php  echo $examid ?>';
    var testType =' <?php  echo $testType ?>';
	 $("#showHint").hide();
	    var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });

    $.ajax({
				type:'POST',
				url:"<?php echo ADMIN_QUESTIONPAPER_URL ?>",
				data:{index:index,type:type,val:val,review:review,order:order,test:test,testType:testType},
				success:function(t){ 
				  $('#question_module').html(t);
				}
		  });
   return false;  
  
} 
function getFiftyFiftyOptions(test,que,unique,package){
	$.ajax({
                    type: "post",
                    url:"<?php echo ADMIN_FIFTY_FIFTY_QUE ?>",  
				    data: {test:test,que:que,unique:unique,package:package},
                            success: function(response){
								//alert(response);
								 $('#opt').html(response);
								   },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}


</script>


</body>
</html>
