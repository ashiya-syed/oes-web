<!DOCTYPE html>
 <head>
  <meta charset="utf-8" />
  <title>Onyx Educationals</title>
 <link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />
 <link href="<?php echo CSS_URL; ?>bootstrap.min.css" rel="stylesheet">
 <link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet">
 <link href="<?php echo CSS_URL; ?>font-awesome.css" rel="stylesheet">
 </head>
<body>
<div id="wrapper">
    <div class="clearfix"></div>
    <section class="login_bg">
        <div class="container">
        <div class="row">
		<?php echo $this->session->flashdata('buypackagesdsd'); ?>
		<?php echo $this->session->flashdata('buypackage'); ?>
		<?php echo $this->session->flashdata('message'); ?>
		<?php echo $this->session->flashdata('successMessage'); ?>
            <div class="login_panel">
			
			<?php if (isset($successMessage)) { ?>

				    <div class="alert alert-success alert-dismissable">
					 <a href="javascript:void(0)" class='close' data-dismiss='alert' aria-label='close'></a>
					 <strong> <?php echo $successMessage; ?></strong>
					</div>
					<?php } ?>
                <?php if (isset($errorMessage)) { ?>
				 <div class="alert alert-danger alert-dismissable">
					 <a href="javascript:void(0)" class='close' data-dismiss='alert' aria-label='close'></a>
					 <strong> <?php echo $errorMessage; ?></strong>
					</div>
					  <?php  } ?>
            	<div class="col-sm-5 np">
				
                <div class="logo_content">
                	<center><img src="<?php echo SITEURL?>/assets/images/logo.png"></center>
                    <p>ONYX Educational</p>
                    <!--<div class="login_short_desc">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor.</div>
                    -->
					<div class="login_short_desc">Onyx Education in collaboration with Hughes Global Education provides Executive Programs and certifications for working professionals from IITs, IIMs, and similar leading Indian and global business and technology schools.</div>
                </div>   
                </div>
				<form action="" method="post">
			   <div class="col-sm-7 login_area">
                	<h1><img src="<?php echo SITEURL?>/assets/images/login_icon.png"> Login Here</h1>
					
                <div class="form-group">
				<div   class="form-group" id="error"></div>
                    <input type="text" name="username" placeholder="USER EMAIL/PHONE NUMBER" required id="name" class="form-control login_field" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="PASSWORD" id="password" required class="form-control login_field" autocomplete="off">
                </div>
                <p><a href="<?php echo FORGOTPASSWORD ?>">Forgot your Password?</a></p>
				<button type="submit"  class="btn login_btn mt15" name="User_LoginBtn">Login</button>
				</form>
				<a type="button"  href="<?php echo REGISTRATION_URL?>" class="btn register_btn mt15" name="User_RegisterBtn">Register</a>
               
                </div>
				
                <div class="clearfix"></div>
            </div>
        </div>
   </div>
  <div class="clearfix"></div>
 </section>
</div>
<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
</body>

</html>

<script type="text/javascript">
   /* var isIE = /*@cc_on!@*/false || !!document.documentMode;
    /*var newWin = window.open("http://www.google.com","myWindow",'top=0,left=0,width='+screen.width+',height='+screen.height+',status=yes,resizable=no,scrollbars=yes');
	if(isIE){
		if (newWin == null ){  alert("Please Enable Popup For this Site...");}
	}else{
     if (!newWin || newWin.closed || typeof newWin.closed == 'undefined' || newWin.outerHeight == 0){
     alert("Please Enable Popup For this Site...");

    }else{newWin.close();}
}*/
$('input').attr('autocomplete', 'off');
   $("input").prop('required',true);
</script>


