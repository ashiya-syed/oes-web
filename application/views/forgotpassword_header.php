<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>
    <link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />

    <!-- Bootstrap -->
    <link href="<?php echo CSS_URL; ?>bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>font-awesome.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>record.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>image.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="<?php echo JS_URL; ?>jquery.min.js"></script>
	<script>var baseUrl = "<?php echo SITEURL; ?>"; </script>
	<script>var examUrl = "<?php echo EXAM_URL; ?>"; </script>
	<script>var pageUrl = "<?php echo current_url(); ?>"; </script>
	<script>var otpUrl = "<?php echo OTP_URL; ?>"; </script>
	<script>var saveUrl = "<?php echo SAVE_OPTIONS_URL; ?>"; </script>
	
                           
</head>
<body>
<div id="wrapper">
    <!--Header !-->
<header id="header">
    	<nav class="navbar navbar-inverse2 navbar-fixed-top">
                <div class="container">
                        <div class="row">
                        <div class="col-sm-6 col-md-6 logo-brand">
                        <a href="<?php echo SITEURL; ?>"><img src="<?php echo IMAGES_URL; ?>logo.png" alt="Logo" title="OES"/>
                        ONYX Educational</a>
                        </div>
						
                        <div class="col-sm-4 col-md-5 text-right">
                            <div class="user-profile dropdown">
                                <a href="#" title="" class="clearfix" data-toggle="dropdown" aria-expanded="true">
								<img width="50" height="50" class="img-circle" src="<?php echo IMAGES_URL; ?>default_profile_img.png" alt="">
									 <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                   <?php if(@$url){ ?>
                                    <li><a href="<?php echo @$url ?>">Login</a></li>
								<?php }else{ ?>
									<li><a href="<?php echo ACCOUNT_URL?>">Login</a></li>
								<?php } ?>
                                </ul>
                            </div>
                        </div>
                       
						
                        </div>
                    <div class="clearfix"></div>
                </div>
            </nav>
    </header>
	