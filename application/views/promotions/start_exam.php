<!--Body!-->
<style type="text/css">
html, body, {
    position:fixed;
    top:0;
    bottom:0;
    left:0;
    right:0;
}
</style>
    <section class="main_body">
        <div class="container">
        <div class="row profile">
                <div class="table-responsive clearfix">          
                          <table class="table table-bordered">
                            <thead>
                              <tr class="tble_clr1">
                                <th colspan="7">
                                   Start Exam 
                                </th>
                              </tr>
                              <tr class="tble_clr2">
                               <!-- <th width="10%">Package Id</th>-->
                                <th width="12%">Package Name</th>
                                <!--<th width="25%">Test Id</th>-->
                                <th width="15%">Test Name</th>
                                <th width="15%">Attempts</th>
                                <th width="14%">Total Questions</th>
                                <th width="10%">Total Time</th>
                                <th width="14%">Total Marks</th>
                              </tr>
                            </thead>
                            <tbody>
							<?php 
							$package_name=(!empty($package_name))?$package_name:'-';
							$testname->testName=(!empty($testname->testName))?$testname->testName:'-';
							$testname->testTime=(!empty($testname->testTime))?$testname->testTime:0;
							$testname->rightMarks=(!empty($testname->rightMarks))?$testname->rightMarks:1;
							$testname->negativeMarks=(!empty($testname->negativeMarks))?$testname->negativeMarks:0;
							$totalQue=(!empty($totalQue))?$totalQue:0;
							$package_Id=(!empty($package_id))?$package_id:'0';
							?>
							
							
                              <tr class="text-center">
                                <!--<td><?php if($package_Id==0){echo '_';}else{echo $package_Id;}?></td>-->
                                <td><?php echo @$package_name; ?></td>
								<!--<td><?php echo  $testname->testId;?></td>-->
                                <td> <?php echo $testname->testName?></td>
                                <td> <?php echo @$attempts;  ?> </td>
                                <td><?php echo $totalQue; ?></td>
                                <td><?php echo $testname->testTime?> min</td>
                                <td><?php echo ($totalQue * ($testname->rightMarks))?></td>
                              </tr>
                            </tbody>
                          </table>
                  </div>
				<p class="mb25" style="font-size:15px; font-weight:700;">
                <span style="color:#25557a; font-size:19px;">General Instructions:</span><br>
                Please read the following instructions very carefully:
                </p>
				<span id="blinker" style="font-size:15px; font-weight:700;"><p class="text-info">Please allow  popups in your browser settings before starting exam.</p></span>
                <ol style="padding-left:20px;">
				<li><p class="text-danger">Your attempting this test for <?php echo $attempts; ?> time. </p></li>
				    <li>You have <strong><?php echo $testname->testTime?> minutes</strong> to complete the test.</li>
                    <li>This test contains  total  <strong><?php echo $totalQue; ?> questions</strong>.</li>
                    <li>You will be awarded <strong><?php echo $testname->rightMarks?> mark</strong> for each correct answer.</li>
                    <li>There is  <strong><?php echo $testname->negativeMarks?> penalty marks </strong>for each wrong answer..</li>
                    <li>You can change your answer by clicking on some other option.</li>
                    <li>You can unmark your answer by clicking on the “Deselect” button.</li>
					<li><b>Fifity - fifty</b> option is available for any of <b>three</b> questions only</li>
                   <!-- <li>You can flag a question for reviewing it later by clicking on the “Flag” button. Once clicked, the button changes to “Unflag”. Click “Unflag” to unflag the question.</li>
                    --><li>You can move back and forth between the questions by clicking the buttons “Previous” and “Next” respectively.</li>
                    <li>A Number list of all questions appears at the right hand side of the screen. You can access the questions in any order within a section or across sections by clicking on the question number given on the number list.</li>
                    <li>You can use rough sheets while taking the test. Do not use calculators, log tables, dictionaries, or any other printed/online reference material during the test.</li>
                    <li>Do not click the button “Submit test” before completing the test. A test once submitted cannot be resumed.</li>
                </ol>
				
                  <div id="error" style="color:red"><?php if($this->session->flashdata('otp')){echo $this->session->flashdata('otp');}?></div>
                  <form method="post" action="<?php echo PROMOTION_OTP_URL?>" >
                  <!--div> <label>Enter otp&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;<input type="text" name="otp" id="otp" maxlength="6"  placeholder="Enter otp.." ></label></div-->
				  <div class="checkbox mt25" style="color:#004886;">
                  <label><input type="checkbox" name="isAgree" id="isAgree" onclick="$(this).attr('value', this.checked ? 1 : 0);check();">I have read and understood the instructions. I agree that in case of not adhering to the exam instructions, I will be disqualified from giving the exam.</label>
                </div>
                <input type="hidden" id="url" name="url" value="<?php  echo current_url();?>">
                <input type="hidden" name="uniqueId" id="uniqueId" value="<?php echo $uniqueId;?>">
                <input type="hidden" name="examid" id="examid" value="<?php echo $examId;?>">
                <input type="hidden" name="type" id="type" value="<?php echo $type;?>">
                <input type="hidden" name="packageId" id="packageId" value="<?php echo $package_Id;?>">
				
				<center><button type="submit" value="submit" id="submit" class="btn register_btn mt25" id="startTest">Start Test</button></center></form>
        </div>
        </div>
    </section>

<script>//history.pushState(null, null, "/");
$(document).ready(function() {
$("#submit").attr('disabled', 'disabled');
$("form").on('keyup change', function (){
var otp = $("#otp").val();	
var isAgree = $("#isAgree").val();

		if (otp == "" || isAgree == 'on' ) {
          $("#submit").attr('disabled', 'disabled');
        }else{
          $("#submit").removeAttr('disabled');
		
           }

});
});
function check(){
	var otp = $("#otp").val();	
      if (otp == "" || isAgree == 'on' ) {
          $("#submit").attr('disabled', 'disabled');
        }else{
          $("#submit").removeAttr('disabled');
		}
}
</script>
<script>
var blink_speed = 500; var t = setInterval(function () { var ele = document.getElementById('blinker'); 
ele.style.visibility = (ele.style.visibility == 'hidden' ? '' : 'hidden'); }, blink_speed);

</script>
<script src="<?php echo JS_URL; ?>bootstrap.min.js"></script>
    <script src="<?php echo JS_URL; ?>jquery-1.9.1.js"></script>
	<?php if(isset($extrafooter)){ print_r($extrafooter); } ?>
    <script type="text/javascript" src="<?php echo JS_URL; ?>common.js"></script>	
	