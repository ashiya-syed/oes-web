<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Onyx Educationals</title>
    <link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />
    <!-- Bootstrap -->
    <link href="<?php echo CSS_URL; ?>bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>font-awesome.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>record.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>image.css" rel="stylesheet">
    <script src="<?php echo JS_URL; ?>jquery.min.js"></script>
	<script>var baseUrl = "<?php echo SITEURL2; ?>"; </script>
	<script>var examUrl = "<?php echo EXAM_URL; ?>"; </script>
	<script>var pageUrl = "<?php echo current_url(); ?>"; </script>
	<script>var otpUrl = "<?php echo OTP_URL; ?>"; </script>
	<script>var saveUrl = "<?php echo SAVE_OPTIONS_URL; ?>"; </script>
	<!--MATHJAX CDN
	<script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'></script>
    <script>
	  MathJax.Hub.Config({
	  tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
	});
	 MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	  </script>
  <!--END-->
	<?php $examid=$this->session->userdata('examId');
	 $package=$this->session->userdata('packageiD');
	 $uniqueid=$this->session->userdata('uniqueId');
     $url=SITEURL2.'/dashboard/questionPaper/exam/'.$examid.'/package/'.$package.'/uniqueId/'.$uniqueid;?>
	<script>var pageUrl = "<?php echo $url; ?>"; </script>
                           
</head>
<?php
$name="";
 if(isset($userDetails)){
		$name=$userDetails->userName;
		$profilePicture=$userDetails->profilePicture;
	 }
	 $cntlr=$this->uri->segment(1);
	  $method=$this->uri->segment(2);
?>
<header id="header">
    	<nav class="navbar navbar-inverse2 navbar-fixed-top">
                <div class="container">
                        <div class="row">
                        <div class="col-sm-6 col-md-6 logo-brand">
						
                       <!-- <a href="<?php  //if($name){ echo DASHBOARD_URL.'?user='.$name; }else{echo SITEURL;} ?>"><img src="<?php echo IMAGES_URL; ?>logo.png" alt="Logo" title="OES"/>
                        ONYX Educational</a>-->
						<?php if(@$cntlr == "Account" && $method!= "forgotpassword"  && $method!= "resetpassword"){?>
                      <a href="<?php echo SITEURL ?>"><img src="<?php echo IMAGES_URL; ?>logo.png" alt="Logo" title="OES"/>
                        ONYX Educational</a>
						<?php }else{?>
						 <a href="#"><img src="<?php echo IMAGES_URL; ?>logo.png" alt="Logo" title="OES"/>
                        ONYX Educational</a>
						<?php }?>
                        </div>
						
                    <div class="clearfix"></div>
                </div>
            </nav>
    </header>
	<script type="text/javascript">
   $(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });
function copyToClipboard() {
	var aux = document.createElement("input"); 
  aux.setAttribute("value", "Dont take screenshot.");
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
  document.body.innerHTML ="<br><br><br><div style='color: #FF0000; font-size: 36px;text-align: center;'>Screenshorts are restricted for this site.</div>";
 // alert("Screenshots are restricted.");
}

$(window).keyup(function(e){
  if(e.keyCode == 44){
    copyToClipboard();
  }
});
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });//for cntrl+p
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 67){  e.preventDefault(); } });//cntrl+c
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });//cntrl+u
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 86){  e.preventDefault(); } });//cntrl+v
jQuery(document).bind("keyup keydown", function(e){ if(e.altKey && e.keyCode == 9){  e.preventDefault(); } });//alt+tab
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 83){  e.preventDefault(); } });//cntrl+s
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 65){  e.preventDefault(); } }); //cntrl+a
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 90){  e.preventDefault(); } }); //cntrl+z
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 89){  e.preventDefault(); } }); //cntrl+y
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>