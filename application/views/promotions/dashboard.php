<!DOCTYPE html>
<html lang="en">
<body>
<div id="wrapper">
   <section class="main_body">
        <div class="container">
        <div class="row">
            <div class="clearfix"></div>
        <div class="col-md-12">
                  <h1 class="home_thumb_title"><img src="<?php echo IMAGES_URL ?>home_tab_icon2.png" style="margin-right:10px;"> Tests
                   
                  </h1>
				<div class="row">
				<?php if($tests  && count($tests)>0){
						     $o=1;
                             foreach($tests as $test){ 
							  $testId=$test->testId;
							  $testName=$test->testName;
					 ?>
				<div class="col-sm-4">
					<div class="opted_test_module mb0">
						<div class="opted_test_img" style="background:url(<?php echo IMAGES_URL ?>opted_test_img1.jpg) no-repeat top center;"></div>
						<div class="opted_test_txt">
						<center><p><?php echo $testName; ?><br></center>
						<!--<span><i class="fa fa-clock-o" aria-hidden="true"></i> 18 - jan - 2017</span>-->
						</p>
						 <center><a type="button" class="btn register_btn blue_btn mt5"  onclick="start(<?php echo $testId;?>,0,0,0)" >Start Exam</a></center>
                       </div>
						<!--<div class="test_tag">1/5</div>-->
						<div class="clearfix"></div>
					</div>
				</div>
				<?php $o++;} }else { 
					    echo "<span style='color:red'>No promotion tests exists.</span>";
					} ?>
			        
				
				</div>
				<div class="modal fade" id="reportsPopup" tabindex="-1" role="basic" aria-hidden="true">
					<div class="modal-dialog"  id="_reports_Popup_div" style="margin-top:80px;">
					</div>
					</div>
				
            </div>
			
            
                  </div>
				<div class="clearfix"></div>
                </div>
            </div>
       
       
    </section>
    
    <!-- Footer -->
	<footer id="footer">
    <div class="container">
    <div class="row">
        	<p>Copyright © 2017, All Rights Reserved.</p>
    </div>            
    </div>
    <div class="clearfix"></div>
    </footer>
    
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
    <script src="js/jquery-1.9.1.min.js"></script>
</body>
</html>