<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>
	<link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />

    <!-- Bootstrap -->
    <link href="<?php echo SITEURL?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/font-awesome.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="<?php echo SITEURL?>/assets/js/jquery.min.js"></script>
</head>
<body>
<div id="wrapper">
<!-- header -->	
		<header id="header">
			<nav class="navbar navbar-inverse navbar-fixed-top">
			  <div class="col-lg-12">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-navbar" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <div class="logo-brand">
				  <a href="<?php echo SITEURL?>"><img src="<?php echo SITEURL?>/assets/images/logo.png" alt="Logo" title="OES"/>ONYX Educational</a>
				  </div>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="collapse-navbar">
				 
				  <ul class="nav navbar-nav navbar-right mr0">
                    <!--<li><a href="#">Practice</a></li>
                    <li><a href="#">Plans</a></li>
                    <li><a href="#">Offers</a></li>-->
                    <li><a href="<?php echo ACCOUNT_URL; ?>">Login</a></li>
                    <li><a href="<?php echo REGISTRATION_URL; ?>" class="top_signup">Sign Up</a></li>
				  </ul>
				</div><!-- /.navbar-collapse -->
			  </div><!-- /.container -->
			</nav>	
		</header>
    <!--Header End !-->
	<!-- Top banner -->	
		<section id="banner">
			<div class="container">	
			<?php echo $this->session->flashdata('buypackagesdsd'); ?>
		<?php echo $this->session->flashdata('buypackage'); ?>
		<?php echo $this->session->flashdata('message'); ?>
		<?php echo $this->session->flashdata('successMessage'); ?>
                <h2>Attempt Test Any Time Any Where</h2>	
                <h3>A new innovation in online examination. </h3>
				
				<center><a type="button" href="<?php echo ACCOUNT_URL; ?>" class="btn start_exam_btn">Get Start Your Test</a></center>				 
			</div>		
		</section>
    <!--Body!-->
	<section class="service_bg">
        <div class="container">
        <div class="row">
			<h1>Onyx Educational<br/>
			<span>Join the most comprehensive online preparation portal for Practice and Main tests of Mining Academics. </span></h1>
			<div class="separator"></div>
			<div class="col-sm-6 col-md-3 mt20">
                <div class="service_category s_c_border1">
                  <img src="<?php echo SITEURL?>/assets/images/landing_color_icon1.png" alt="" title="">  
                </div>
                <div class="s_category_txtbg1">
               
                <p class="nm">An enormous number of candidates are catered smoothly providing concurrent exams, a delight to both exam takers and the exam managers.
                Smooth registration, swift creation of tests and synchronized user- interface for you and your candidates.</p>
                </div>
            </div>
			<div class="col-sm-6 col-md-3 mt20">
                <div class="service_category s_c_border2">
                  <img style="bottom:-8px;" src="<?php echo SITEURL?>/assets/images/landing_color_icon2.png" alt="" title="">  
                </div>
                <div class="s_category_txtbg2">
                <p class="nm">Advance question types with a strong performance The question can be presented to candidates in multiple ways such as scenario based, Video and Audio questions.</p>   
                <p class="nm">All exams are automatically graded with detailed and accurate performance report.</p>
                </div>
            </div>
			<div class="col-sm-6 col-md-3 mt20">
                <div class="service_category s_c_border3">
                  <img style="bottom:-10px;" src="<?php echo SITEURL?>/assets/images/landing_color_icon3.png" alt="" title="">  
                </div>
                <div class="s_category_txtbg3">
                
                <p class="nm">Advanced Reporting System Instant scorecard generation,computational analysis. An ultimate combination of detailed and drilled methodologies that will eventually complement your skills and grades. Separate test report for each attempt shows by dates.</p>
                </div>
            </div>
			<div class="col-sm-6 col-md-3 mt20">
                <div class="service_category s_c_border4">
                  <img style="bottom:-25px;" src="<?php echo SITEURL?>/assets/images/landing_color_icon4.png" alt="" title="">  
                </div>
                <div class="s_category_txtbg4">
                <p class="nm">Exam news and updates We recommend that you visit this page regularly, to stay informed of any important news or updates that may affect your exams.Available all the details including important dates, news regarding any changes, tips and related information.</p>
                </div>
            </div>
		</div>
		</div>
	</section>
	
	<section class="exam_screen">
    <div class="container">
    <div class="row">
			<div class="col-md-5">
				<p>Onyx Educational aims to be a powerful examination tool for e-Learning and online education. Useful for, Mining Engineering students to practice their exams. Go wherever you want to and practice whenever you want, using the Onyx Educational online exam platform. 
				Experience a lag-free synchronized performance Extensive range of high quality practice tests with thousands of questions and their answers as per latest exam pattern. Complete set of practice and Main tests needed for all exams. </p>
				<a type="button" href="<?php echo ACCOUNT_URL; ?>" class="btn start_exam_btn">Get Started</a>
			</div>
			<div class="col-md-7">
				<img class="img-responsive" src="<?php echo SITEURL?>/assets/images/exam_screenshot.png" alt="" title="">
			</div>
	</div>
	</div>
	</section>
	
	<!--<section class="counter_content">
    <div class="container">
    <div class="row">
			<div class="col-sm-3">
				<center><img src="<?php echo SITEURL?>/assets/images/counter_number_icon1.png" alt="" title=""></center>
				<h1><span class="counter"><?php echo $userCount; ?></span>+</h1>
				<div class="counter_separator"></div>
				<p>Users Registered</p>
			</div>
			<div class="col-sm-3">
				<center><img src="<?php echo SITEURL?>/assets/images/counter_number_icon2.png" alt="" title=""></center>
				<h1><span class="counter"><?php echo $completedTests; ?></span>+</h1>
				<div class="counter_separator"></div>
				<p>Users Test Completed</p>
			</div>
			<div class="col-sm-3">
				<center><img src="<?php echo SITEURL?>/assets/images/counter_number_icon3.png" alt="" title=""></center>
				<h1><span class="counter"><?php echo $attemptedQue; ?></span>+</h1>
				<div class="counter_separator"></div>
				<p>QUESTIONS ATTEMPTED</p>
			</div>
			<div class="col-sm-3">
				<center><img src="<?php echo SITEURL?>/assets/images/counter_number_icon3.png" alt="" title=""></center>
				<h1><span class="counter"><?php echo $attemptedTests; ?></span>+</h1>
				<div class="counter_separator"></div>
				<p>TESTS ATTEMPTED</p>
			</div>
	</div>
	</div>
	</section>-->
	
	<section class="testimonial">
    <div class="container">
			<h1>Testimonial</h1>
			<div class="separator"></div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators testimonial_indicator">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1" class=""></li>
            </ol>
        
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
        
              <div class="item active">
					<div class="testi_img" style="background: url(<?php echo SITEURL?>/assets/images/default_profile_img.png) no-repeat top center;"></div>
                  <p>We have been using onyx-exams services for managing pre-admission process for our reputed courses of mining since last 3 years. We have found the system very user-friendly. The manual activities involved in our pre-admission process were greatly reduced.  SMS facility provided the applicants with timely remainders of our admission notices. We look forward to a continued support from onyx-exams and good work.. </p>
                  <div class="testi_name mt25">- Sharath Reddy, <span>JNTUH</span></div>
              </div>
        
              <div class="item">
					<div class="testi_img" style="background: url(<?php echo SITEURL?>/assets/images/sumana.jpg) no-repeat top center;"></div>
                  <p>Let me appreciate all efforts put in by entire team to successfully complete the examination. We are very thankful to all of you for entire support extended during the examination.We are able to execute our Online Exam Process with secure browsers seamlessly. Thanks once again. </p>
                  <div class="testi_name mt25">- Sumana Ajra, <span>VRSEC</span></div>
              </div>
            
            </div>
        
          </div>
    </div>
    <div class="clearfix"></div>
    </section>

    <!-- Footer -->
	<footer id="footer2">
    <div class="container">
    <div class="row">
        	<p>Copyright � 2017, All Rights Reserved.</p>
    </div>            
    </div>
    <div class="clearfix"></div>
    </footer>
    
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo SITEURL?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo SITEURL?>/assets/js/smooth-scroll.js"></script>
	<script src="<?php echo SITEURL?>/assets/js/main.js"></script>
	<script src="<?php echo SITEURL?>/assets/js/waypoints.min.js"></script> 
	<script src="<?php echo SITEURL?>/assets/js/jquery.counterup.min.js"></script> 
	<script>
		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
				delay: 10,
				time: 3000
			});
		});
	</script>
</body>
</html>