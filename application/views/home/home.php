<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>
	<link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/images/logo.png" />

    <!-- Bootstrap -->
    <link href="<?php echo SITEURL?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo SITEURL?>/assets/css/font-awesome.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="<?php echo SITEURL?>/assets/js/jquery.min.js"></script>
</head>
<body>
<div id="wrapper">
<!-- header -->	
		<header id="header">
			<nav class="navbar navbar-inverse navbar-fixed-top">
			  <div class="col-lg-12">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-navbar" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <div class="logo-brand">
				  <a href="<?php echo SITEURL?>"><img src="<?php echo SITEURL?>/assets/images/logo.png" alt="Logo" title="OES"/>ONYX-EXAMS</a>
				  </div>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="collapse-navbar">
				 
				  <ul class="nav navbar-nav navbar-right mr0">
                    <!--<li><a href="#">Practice</a></li>
                    <li><a href="#">Plans</a></li>
                    <li><a href="#">Offers</a></li>-->
                    <li><a href="<?php echo ACCOUNT_URL; ?>">Login</a></li>
                    <li><a href="<?php echo REGISTRATION_URL; ?>" class="top_signup">Sign Up</a></li>
				  </ul>
				</div><!-- /.navbar-collapse -->
			  </div><!-- /.container -->
			</nav>	
		</header>
    <!--Header End !-->
	<!-- Top banner -->	
		<section id="banner">
			<div class="container">	
			<?php echo $this->session->flashdata('buypackagesdsd'); ?>
		<?php echo $this->session->flashdata('buypackage'); ?>
		<?php echo $this->session->flashdata('message'); ?>
		<?php echo $this->session->flashdata('successMessage'); ?>
                <h2>A KNOWLEDGE MOMENTUM</h2>	
                <h2 style="font-size: 26px;">An innovation in Mining and Geology exams- First of its kind </h2>
                <h3 style=" text-transform: capitalize;">Attempt your exams in any place &amp; at any time. </h3>
				
				<center><a type="button" href="<?php echo PROMOTION_MEMBER_LOGIN_URL; ?>" class="btn start_exam_btn">START YOUR FREE TEST</a><center><h2 style="font-size: 26px;margin:0px" >A wonderful platform of mining education</h2></center>				 
			</div>		
		</section>
    <!--Body!-->
	<section class="service_bg">
        <div class="container">
        <div class="row">
			<h1>ONYX EXAMS<br/>
			<span>The only providers of online &amp; off line coaching for all DGMS statutory exams in India. </span></h1>
			<div class="separator"></div>
			<h1><span>100% success in orals of those who passed with ONYX coaching.</span></h1> 
			<div class="col-sm-6 col-md-3 mt20">
                <div class="service_category s_c_border1">
                  <img src="<?php echo SITEURL?>/assets/images/landing_color_icon1.png" alt="" title="">  
                </div>
                 <div class="s_category_txtbg1">
              <div>THE DGMS EXAMS : </div><br>
                <p class="nm"> The DGMS statutory exam coaching is available for 1st class, 2nd class and Foreman exams. The notes is given in hard copy as per syllabus. </p><p class="nm"> The exams are covered in multiple papers of 50 questions each, and all the questions are mixed together and presented as 'GRAND TESTS' with 80 questions each.  Each paper can be attempted 3 times. </p>
                </div>
            </div>
			<div class="col-sm-6 col-md-3 mt20">
                <div class="service_category s_c_border2">
                  <img style="bottom:-8px;" src="<?php echo SITEURL?>/assets/images/landing_color_icon2.png" alt="" title="">  
                </div>
                <div class="s_category_txtbg2">
				<div >UNIQUE FEATURES : </div>
                <p class="nm">The DGMS exams incorporate NEGATIVE MARKING. For that reason ONYX exams also providing practice with NEGATIVE MARKING for wrong answers.A candidate will practice with caution and do the same in exams in future.Another feature of interest is the '50-50' choice.In this candidate can opt to ask computer to remove two wrong answers.Please remember that this feature is only with our exams and not with DGMS exams.</p> </div>
            </div>
			<div class="col-sm-6 col-md-3 mt20">
                <div class="service_category s_c_border3">
                  <img style="bottom:-10px;" src="<?php echo SITEURL?>/assets/images/landing_color_icon3.png" alt="" title="">  
                </div>
                <div class="s_category_txtbg3">
               <div> NOTES & ITS FEATURES : </div><br>
                <p class="nm">The notes is in simple English for the candidate to understand.</p><br> <p class="nm">Especially in legislation the candidates
feel the difficulty in interpreting the meaning. This is completely eliminated with the way that we
presented the notes.</p><p class="nm">We are providing libraries to the candidate based on selected packages. Materials,document available in the libraries.</p>
                </div>
            </div>
			<div class="col-sm-6 col-md-3 mt20">
                <div class="service_category s_c_border4">
                  <img style="bottom:-25px;" src="<?php echo SITEURL?>/assets/images/landing_color_icon4.png" alt="" title="">  
                </div>
                 <div class="s_category_txtbg4">
				<br>
				<div>CLASS ROOM COACHING : </div>
                <p class="nm"> <br>ONYX assures that there will be no need of any class room coaching. But for doubling your confidence we will conduct class room coaching at Hyderabad just before one month of the exam dates on separate payment.</p><p class="nm"> During right time the notification of class room coaching will be sent to all the candidates. </p>
                </div>
            </div>
		</div>
		</div>
	</section>
	
	<section class="exam_screen">
    <div class="container">
    <div class="row">
			<div class="col-md-5">
				<p>Onyx Educational aims to be a powerful examination tool for e-Learning and online education. Useful for, Mining Engineering students to practice their exams. Go wherever you want to and practice whenever you want, using the Onyx Educational online exam platform. 
				Experience a lag-free synchronized performance Extensive range of high quality practice tests with thousands of questions and their answers as per latest exam pattern. Complete set of practice and Main tests needed for all exams. </p>
				<a type="button" href="<?php echo ACCOUNT_URL; ?>" class="btn start_exam_btn">Get Started</a>
			</div>
			<div class="col-md-7">
				<img class="img-responsive" src="<?php echo SITEURL?>/assets/images/exam_screenshot.png" alt="" title="">
			</div>
	</div>
	</div>
	</section>
	
	<!--<section class="counter_content">
    <div class="container">
    <div class="row">
			<div class="col-sm-3">
				<center><img src="<?php echo SITEURL?>/assets/images/counter_number_icon1.png" alt="" title=""></center>
				<h1><span class="counter"><?php echo $userCount; ?></span>+</h1>
				<div class="counter_separator"></div>
				<p>Users Registered</p>
			</div>
			<div class="col-sm-3">
				<center><img src="<?php echo SITEURL?>/assets/images/counter_number_icon2.png" alt="" title=""></center>
				<h1><span class="counter"><?php echo $completedTests; ?></span>+</h1>
				<div class="counter_separator"></div>
				<p>Users Test Completed</p>
			</div>
			<div class="col-sm-3">
				<center><img src="<?php echo SITEURL?>/assets/images/counter_number_icon3.png" alt="" title=""></center>
				<h1><span class="counter"><?php echo $attemptedQue; ?></span>+</h1>
				<div class="counter_separator"></div>
				<p>QUESTIONS ATTEMPTED</p>
			</div>
			<div class="col-sm-3">
				<center><img src="<?php echo SITEURL?>/assets/images/counter_number_icon3.png" alt="" title=""></center>
				<h1><span class="counter"><?php echo $attemptedTests; ?></span>+</h1>
				<div class="counter_separator"></div>
				<p>TESTS ATTEMPTED</p>
			</div>
	</div>
	</div>
	</section>-->
	
	<section class="testimonial">
    <div class="container">
			<h1>Testimonial</h1>
			<div class="separator"></div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators testimonial_indicator">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1" class=""></li>
            </ol>
        
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
        
              <div class="item active">
					<div class="testi_img" style="background: url(<?php echo SITEURL?>/assets/images/default_profile_img.png) no-repeat top center;"></div>
                  <p>We have been using onyx-exams services for managing pre-admission process for our reputed courses of mining since last 3 years. We have found the system very user-friendly. The manual activities involved in our pre-admission process were greatly reduced.  SMS facility provided the applicants with timely remainders of our admission notices. We look forward to a continued support from onyx-exams and good work.. </p>
                  <div class="testi_name mt25">- Sharath Reddy, <span>JNTUH</span></div>
              </div>
        
              <div class="item">
					<div class="testi_img" style="background: url(<?php echo SITEURL?>/assets/images/default_profile_img.png) no-repeat top center;"></div>
                  <p>Let me appreciate all efforts put in by entire team to successfully complete the examination. We are very thankful to all of you for entire support extended during the examination.We are able to execute our Online Exam Process with secure browsers seamlessly. Thanks once again. </p>
                  <div class="testi_name mt25">- Sumana Ajra, <span>VRSEC</span></div>
              </div>
            
            </div>
        
          </div>
    </div>
    <div class="clearfix"></div>
    </section>

    <!-- Footer -->
	<footer id="footer2">
   <?php $this->load->view('links'); ?>
   <div class="container">
    <div class="row">
        	<p>Copyright &copy; <?php echo date("Y"); ?>, All Rights Reserved.</p>
    </div>            
    </div>
    <div class="clearfix"></div>
    </footer>
    
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo SITEURL?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo SITEURL?>/assets/js/smooth-scroll.js"></script>
	<script src="<?php echo SITEURL?>/assets/js/main.js"></script>
	<script src="<?php echo SITEURL?>/assets/js/waypoints.min.js"></script> 
	<script src="<?php echo SITEURL?>/assets/js/jquery.counterup.min.js"></script> 
	<script>
		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
				delay: 10,
				time: 3000
			});
		});
	</script>
</body>
</html>