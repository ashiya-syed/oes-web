<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ONYX Educational</title>

    <!-- Bootstrap -->
    <link href="<?php echo CSS_URL; ?>bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>font-awesome.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>record.css" rel="stylesheet">
    <link href="<?php echo CSS_URL; ?>image.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="<?php echo JS_URL; ?>jquery.min.js"></script>
	<script>var baseUrl = "<?php echo SITEURL; ?>"; </script>
	<script>var examUrl = "<?php echo EXAM_URL; ?>"; </script>
	<script>var pageUrl = "<?php echo current_url(); ?>"; </script>
	<script>var otpUrl = "<?php echo OTP_URL; ?>"; </script>
	<script>var saveUrl = "<?php echo SAVE_OPTIONS_URL; ?>"; </script>
	
                           
</head>
<body>
<div id="wrapper">
    <!--Header !-->
	<?php
	if(@$cartCount){$cartCount=$cartCount;}else{
	$cartCount=0;	
	}
	$profilePicture="";
	$controller=$this->uri->segment(1);
	$controller_method=$this->uri->segment(2);
	$name=$this->session->userdata('name');
	 if(isset($userDetails)){
		$name=$userDetails->userName;
		$profilePicture=$userDetails->profilePicture;
	 }
	
//	if($controller!=="Account" || $controller !=="" ){ ?>
   <!-- <header id="header">
    	<nav class="navbar navbar-inverse navbar-static-top">
                <div class="container">
                        <div class="row">
                        <div class="col-sm-6 logo-brand">
                        <a href="<?php// echo DASHBOARD_URL.'?user='.$name; ?>"><img src="<?php echo IMAGES_URL; ?>logo.png" alt="Logo" title="OES"/>
                        ONYX Educational</a>
                        </div>
						<?php// $sses_value = $this->session->userdata('name'); 
						//if($sses_value){
						?>
                        <div class="col-sm-6 text-right">
                            <div class="user-profile dropdown">
                                <a href="#" title="" class="clearfix" data-toggle="dropdown" aria-expanded="true">
								   <?php //if($profilePicture){?>
									   <img width="41" class="img-circle" src="<?php echo PROFILE_IMAGE_PATH.$profilePicture?>" alt="">
								  <?php //}else{?>
                                    <img width="41" class="img-circle" src="<?php echo IMAGES_URL; ?>default_profile_img.png" alt="">
								  <?php //}?>
                                    <span><?php// echo $name;?></span>
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php //echo PROFILE_URL; ?>">Profile</a></li>
                                   <!-- <li><a href="#">Help</a></li>-->
                                    <!--<li><a href="<?php //echo LOGOUT_URL; ?>">Logout</a></li>
                                </ul>
                            </div>
                        </div>
						<?php //if(@$cartCount){?>
						<div class="col-sm-2 col-md-1 text-right pull-right" style="padding-left:0px;">
                            <div class="user-profile">
                              <a href="<?php //echo CART_URL ;?>" title="Cart">
                                <p class="mt10"><i class="fa fa-shopping-cart" aria-hidden="true" style="font-size:18px;"></i> Cart <span class="tble_idle_txt">(<?php echo $cartCount; ?>)</span></p>
                                </a>
                            </div>
                        </div>
						<?php// }?>
	                    <?php// } ?>
                            
                        </div>
                    <div class="clearfix"></div>
                </div>
            </nav>
    </header>-->
	
	<?php //}?>
	  <header id="header">
    	<nav class="navbar navbar-inverse2 navbar-fixed-top">
                <div class="container">
                        <div class="row">
                        <div class="col-sm-6 col-md-6 logo-brand">
                        <a href="<?php echo DASHBOARD_URL.'?name='.$name; ?>"><img src="<?php echo IMAGES_URL; ?>logo.png" alt="Logo" title="OES"/>
                        ONYX Educational</a>
                        </div>
						<?php if($controller_method != "verifyAccount"){?>
                        <div class="col-sm-4 col-md-5 text-right">
                            <div class="user-profile dropdown">
                                <a href="#" title="" class="clearfix" data-toggle="dropdown" aria-expanded="true">
								<?php if($profilePicture){?>
                                    <img width="50" height="50" class="img-circle" src="<?php echo PROFILE_IMAGE_PATH.$profilePicture?>" alt="">
								<?php }else{?>
                                    <img width="50" height="50" class="img-circle" src="<?php echo IMAGES_URL; ?>default_profile_img.png" alt="">
									<?php }?>
                                    <span><?php echo $name;?></span>
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php echo PROFILE_URL; ?>">Profile</a></li>
                                   <li><a href="<?php echo LOGOUT_URL; ?>">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-1 text-right pull-right" style="padding-left:0px;">
                            <div class="user-profile">
                                <a href="<?php echo CART_URL ;?>" title="Cart" >
                                    <p class="mt10" id="cart"><i class="fa fa-shopping-cart" aria-hidden="true" style="font-size:18px;"></i> Cart <span class="tble_idle_txt">(<?php echo @$cartCount; ?>)</span></p>
                                </a>
                            </div>
                        </div>
						
                        </div><?php }?>
                    <div class="clearfix"></div>
                </div>
            </nav>
    </header>
	<script type="text/javascript">
  /* $(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });*/
</script>
	