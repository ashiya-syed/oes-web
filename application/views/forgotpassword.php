<html>
<body>
	<div class="pad-B30 col-md-12 main_body">
        <div class="container">
        <div class="row">
		<div class="inner_form_panel">
			<div id="errors"><span style="color:red"><?php echo $this->session->flashdata('message');?></span></div>
			<div id="errors"><span style="color:red">  <?php echo $this->session->flashdata('forgotpassword');?></span></div>
			<h1 class="home_thumb_title m_l_r15"><img src="<?php echo IMAGES_URL; ?>forgot_password.png" style="margin-right:10px;">Forgot Password</h1>
			<?php $attributes = array('name' => 'loginform', 'id' => 'loginform','method' => 'post', 'class' => 'appt-form2', 'accept-charset' => 'UTF-8', 'role' => 'form');
						if(@$uId){echo form_open(FORGOTPASSWORD.'/'.@$uId,$attributes);}else{echo  form_open(FORGOTPASSWORD,$attributes);}
					?>
					 <div class="form-group clearfix">
					  <label class="control-label col-sm-2" for="email_id" name="email_id">Email<span style="font-size:x-large;color:red">*</span></label>
					  <div class="col-sm-10">
						<input type="email" class="form-control login_field" id="email_id" name="email_id" placeholder="Enter email" required>
						<button type="submit" name="submit" value="submit" class="btn register_btn blue_btn mt25">Submit</button>
						<a type="button" href="javascript:window.history.back();" class="btn register_btn red_btn mt25">Cancel</a>
						 
					  </div>
					</div>
			</form>
			<div class="clearfix"></div>
		</div>
		</div>
		</div>
	</div>
</body>
</html>