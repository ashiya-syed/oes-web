<!DOCTYPE html>
<html lang="en">
<body>
<div id="wrapper">
    <!--Header !-->
    <!--Body!-->
    <section class="main_body">
        <div class="container">
		<?php echo $this->session->flashdata('message');?>
        <div class="row cart">
		<div class="row nm">
            <h1 class="">Payment History</h1>
			<?php if(empty($paymentDetails)) { ?>
			<div class="col-sm-10 col-xs-8 cart_blue_txt">
			<div class="col-sm-2 col-xs-4"><img class="img-responsive" src="<?php echo IMAGES_URL; ?>empty_shopcart.png"></div>
			<p>You don't have any transactions. &nbsp;</p>
			</div>
		<?php	}?>
		</div>
        	
			<div >
			<?php  if($paymentDetails){ ?>
            	<div class="table-responsive clearfix">    
              
                          <table class="table table-bordered">
                            <thead>
                              <tr class="cart_table">
                                <th>Transaction Id</th>
                                <th> Date</th>
                                <th>Amount</th>
                                <th>Payment type</th>
                                <th>Packages</th>
                                <th >Tests</th>
                              </tr>
                            </thead>
                            <tbody>
							<?php 
                            $pkgs=$tsts="-";
               			      foreach($paymentDetails as $pay){  
							   
							     if(count($pay['packages'])>0){
								 $p="";
								 foreach($pay['packages'] as $pack){
								   $p.=$pack->package_name.',';
								  }
								  $pkgs=rtrim($p,',');
								 } 
								 if(count($pay['tests'])>0){
								 $t="";
								 foreach($pay['tests'] as $test){
								   $t.=$test->testName.',';
								  }
								  $tsts=rtrim($t,',');
								 }
							    $time=strtotime($pay['paymentDate']);
								$year = date('Y',$time);
								$monthName = date('F',$time);
								$monthNo = date('m',$time);
								$day=date('d',$time);
								$tdate=$day ." , ".$monthName ." , ".$year;
							  ?>
                              <tr class="bg_white">
                                <!--td><img class="img-responsive" src="<?php echo IMAGES_URL; ?>package.png"/></td-->
                                <td><p class="tble_blue_txt"><?php echo $pay['txnid'];?></p></td>
                                <td><p class="tble_blue_txt"><?php echo $tdate;?></p></td>
                                <td><p class="tble_blue_txt">₹ <?php echo $pay['paidAmount'];?></p></td>
                                <td><p class="tble_blue_txt"><?php echo $pay['paymentType']; ?></p></td>
							  <td>
                                <p class="tble_blue_txt"><?php  echo $pkgs;?></p>
                              </td>
							 <td>
                                <p class="tble_blue_txt"><?php  echo $tsts;?></p>
                              </td>
							  
                              </tr><?php }?>
							  </tbody>
			            </table>
			         </div>
                    <?php }?>
              </div>
		</div>
    </div>
 </section>
        
</div>
 </body>

</html>
