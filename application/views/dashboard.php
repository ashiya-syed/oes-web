<!DOCTYPE html>
<html lang="en">
<style>
#footer{
	position:relative;
}
</style>

<body>
<div id="wrapper">
   <!--Body!-->
    <section class="main_body">
        <div class="container">
		<?php if (isset($successMessage)) { ?>
				    <div class="alert alert-success alert-dismissable">
					 <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"></a>
					 <strong> <?php echo $successMessage; ?></strong>
					</div>
					<?php } ?>
		<?php echo $this->session->flashdata('message'); ?>
		<?php echo $this->session->flashdata('examError'); ?>
		<?php echo $this->session->flashdata('buypackage'); ?>
		<?php echo $this->session->flashdata('verify'); ?>
		<div id="cartCount"></div>
		<div class="row">
		
            <div class="col-md-7 mb25" >
                  <h1 class="home_thumb_title"><img src="<?php echo IMAGES_URL ?>home_tab_icon1.png" style="margin-right:10px;"> Take a Tests
                    <div class="form-group exam_dropdown">
                    <select class="form-control login_field select-down-new border_none" id="examSelect" onchange="getTestTypesRecords(this.value)">
                        <option value="practical">Free</option>
                        <option value="main">Main</option>
                    </select>
                    </div>
                  </h1>
					<div id="takeatest">		
				  <div  class="owl-carousel owl-theme" >
				  
				<?php if($practicalTests  && count($practicalTests)>0){$i=1;
                             foreach($practicalTests as $practicalTest){
					 ?>
                  		<div class="take_test_module item">
                        	<div class="take_test_img" style="background:url(<?php echo IMAGES_URL ?>take_test_img1.jpg) no-repeat top center;">
								<h1><?php echo $practicalTest->testName; ?></h1>
								<?php 
							 $style="";
							$attempted=$practicalTest->attempted ;
							$count=$practicalTest->count;
							$uniqueId=$practicalTest->uniqueId;
							$class="btn register_btn blue_btn mt5";
                            $attempts=$practicalTest->attempts;
                             $c=($attempts-$count); //debug($c);
							if($count && $count<$attempts){$no=$count+1;
                                   $button=" attempt - " .$no;
								
							}else if($c == 0 && $attempts!=0){
                                  $class="btn login_btn green_btn mt5";
								  $button="completed";
								
							}else{$button="start Test";}
							if($attempted){
								$messege="Attempts:".$attempted;
							}else{
								$messege="Practice section exam only ".$attempts." times";
                             }
							 $status=$practicalTest->redo;
							 if($status){
								 $button="redo test";
								 $class="btn login_btn green_btn mt5";
								 $style='style=" background-color: orange"';
							 }
							 ?>
							</div>
                            <p><?php echo $messege ?></p>
                            <center><a type="button" <?php echo $style; ?><?php if($c==0 && $attempts!=0){?> onclick="this.removeAttribute('href');" <?php }?> onclick="start(<?php echo $practicalTest->testId;?>,<?php echo $practicalTest->testType;?>,<?php echo $practicalTest->packageId;?>,<?php echo $uniqueId ?>)" class="<?php echo $class; ?>"><?php echo $button;?></a></center>
                        </div>
                           
						 <?php $i++;} }else { 
					    echo "<center><div class='text-center' style='color:red'>".NO_MAIN_TESTS_EXISTS."</div></center>";
					} ?>
                  		
                      </div>
            </div>
            </div>
			
            <div class="col-md-5">
                <h1 class="home_thumb_title"><img src="<?php echo IMAGES_URL ?>home_tab_icon3.png" style="margin-right:10px;"> Buy Packages/Tests 
				<div class="form-group exam_dropdown" id="examSelect">
                    <select class="form-control login_field select-down-new border_none" onchange="getProducts(this.value)" id="">
                        <option value="package">Pack</option>
                        <option value="test">Tests</option>
                    </select>
                    </div>
				</h1>
					<div class="scrollbar scrollbar2" id="scroll_style">
					<div id="product">
				  <?php if($packages && count($packages) >0){
                          foreach($packages as $package){
				  ?>
						<div class="package_panel">
							<h1><img src="<?php echo IMAGES_URL; ?>package_tittle_icon.png" style="margin-right:10px;"><?php echo $package->package_name; ?></h1>
							<div class="price_tag"><?php echo $package->package_amount; ?></div>
							<div class="package_panel_img pull-left"><img class="img-responsive" src="<?php echo IMAGES_URL; ?>package.png"/></div>
							<div class="package_panel_btn">
							<a type="button" href="JavaScript:Void(0);" class="btn register_btn blue_btn mt5" onclick="popWindow(<?php echo $package->package_id;?>)">View Syllabus</a><br/>
							<input type="hidden" name="quantity" id="quantity" value="1" size="2" style="width:36px" />

							<a type="button" href="JavaScript:Void(0);" onclick="addtoCart(<?php echo $package->package_id;?>,<?php echo PACKAGES ;?>,<?php echo $package->package_amount; ?>)" class="btn login_btn green_btn mt5"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Add to cart</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<?php } } else { 
					        echo "<center><div class='text-center' style='color:red'>".NO_PACKAGES_AVAILABLE."</div></center>";
					    } ?>
                  		</div>
						
            </div>
            </div>
			<div class="clearfix"></div>
            <div class="col-md-7 mt40" >
                  <h1 class="home_thumb_title"><img src="<?php echo IMAGES_URL ?>home_tab_icon2.png" style="margin-right:10px;"> Opted Tests
                    <div class="form-group exam_dropdown">
                    <select class="form-control login_field select-down-new border_none" id="" onchange="optedTests(this.value)">
                       
					   <option value="practical">Free</option>
					   <option value="main">Main</option>
                    </select>
                    </div>
                  </h1>
				  
				<div class="row" >
				<div id="optedTests">
				<?php if($optedTests  && count($optedTests)>0){
						     $o=1;
                             foreach($optedTests as $optedTest){ 
							 $attempts=$optedTest->attempts;
							 $uniqueId=$optedTest->userPackageID;
							 $time=$optedTest->createdTime;
							 $d= date('d - M -Y', strtotime($time));
							 $testDetails=$optedTest->testDetails;
							 $testName=$testDetails->testName;
							 $testId=$testDetails->testId;
							 $testType=$optedTest->type;
							 $count=$optedTest->completedTestCount;
							 $packageId=$optedTest->packageId;
							 
					 ?>
				<div class="col-sm-6">
					<div class="opted_test_module">
						<div class="opted_test_img" style="background:url(<?php echo IMAGES_URL ?>opted_test_img1.jpg) no-repeat top center;"></div>
						<div class="opted_test_txt">
						<center><p><?php //echo $o;?><?php echo $testName; ?><br></center>
						<!--<span><i class="fa fa-clock-o" aria-hidden="true"></i> 18 - jan - 2017</span>-->
						</p>
						<center><a type="button" href="#reportsPopup" data-toggle="modal" onclick="getAttemptsOfTest('<?php echo $testId;?>','<?php echo $uniqueId ?>')"  class="btn register_btn blue_btn mr0 mt5">View Report</a></center>
						</div>
						<!--<div class="test_tag">1/5</div>-->
						<div class="clearfix"></div>
					</div>
				</div>
				<?php $o++;} }else { 
					    echo "<center><div  style='color:red'>Please take a test to view records.</div></center>";
					} ?>
					<?php if($optedTests  && count($optedTests)>0){ ?>
					<p class="load_more clearfix"><a href="<?php echo VIEWMORE.'?type='.PRACTICAL_TEST; ?>" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> Load more..</a></p>
					<?php  } ?>                  
				  </div>
				<div class="modal fade" id="reportsPopup" tabindex="-1" role="basic" aria-hidden="true">
					<div class="modal-dialog"  id="_reports_Popup_div" style="margin-top:80px;">
					</div>
					</div>
				</div>
				
            </div>
			
            <div class="col-md-5 mt40">
                  <h1 class="home_thumb_title"><img src="<?php echo IMAGES_URL ?>home_tab_icon4.png" style="margin-right:10px;"> News & Updates</h1>
                <?php if(count($newsUpdates)<=0){echo "<center><div  style='color:red'>".NEWS_UPDATES_NOT_FOUND."</div></center>"; }else{?>
                <div class="news_module">
              <?php } ?>
                  <div class="scrollbar scrollbar3" id="scroll_style">
				   <?php if($newsUpdates){foreach($newsUpdates as $news){?>
                  		<div class="thumb_panel">
                        	<div class="thumb_img pull-left" style="background:url(<?php echo IMAGES_URL ?>thumb_img.jpg) no-repeat top center;"></div>
                            <div class="thumb_txt blue_txt">
                            <p class="nm">*<?php echo $news->newsTitle;?><br>
                            <span><?php echo substr($news->description,0,380).'...'; ?></span></p>
                            <p class="nm"><span class="date_txt"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo date(' d ', strtotime($news->createdTime));?> - <?php echo date('  F  ', strtotime($news->createdTime));?> - <?php echo date('Y ', strtotime($news->createdTime));?></span> <a class="news_readmore pull-right" href="<?php echo SINGLE_PAGE_URL.'?id='.$news->id;?>">Read More..</a></p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                  		<?php } ?>
                  </div> <?php }else{ //echo "<div class='text-center alert alert-danger' style='color:red'>".NEWS_UPDATES_NOT_FOUND."</div>";
                  }?>
				<!--<div class="clearfix"></div>-->
                </div>
            </div>
        </div>
        </div>
    </section>
    <script type="text/javascript" language="javascript"> 
 var syllabusUrl = "<?php echo VIEW_SYLLABUS_URL; ?>";
 
//for closing the child window and redircting to parent
function HandlePopupResult(result) {
  
}
//for restricting browser background
/*history.pushState({ page: 1 }, "Title 1", "#no-back");
window.onhashchange = function (event) {
window.location.hash = "no-back";
};*/
function clearDiv(){
	  window.location="<?php echo DASHBOARD_URL;?>";
}
</script>
</div>
</body>
</html>