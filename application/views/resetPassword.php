  <html>
  <body>
	<div class="pad-B30 col-md-12 main_body">
        <div class="container">
        <div class="row">
  <?php if (isset($error)) {?>
			 <div class="alert alert-danger alert-dismissable">
							       <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"></a>
							        <strong> <?php echo $error; ?></strong>
				</div>
			<?php }?>
  
 <?php echo $this->session->flashdata('message');?>
			<?php
			$token=$this->uri->segment(3);
			$url= RECOVERPASSWORD.'/'.$token ; 
			if(@$ptoken) $url= RECOVERPASSWORD.'/'.@$ptoken ; 
			$attributes = array('name' => 'loginform', 'id' => 'loginform','method' => 'post', 'class' => 'appt-form2', 'accept-charset' => 'UTF-8', 'role' => 'form');
			echo form_open( CHANGEPASSWORD,$attributes);?>
		<div class="inner_form_panel">
			<h1 class="home_thumb_title m_l_r15"><img src="<?php echo IMAGES_URL; ?>forgot_password.png" style="margin-right:10px;">Reset Password</h1>
				<div class="form-group">
                  <label class="control-label col-sm-4" for="password"> Password <span style="font-size:x-large;color:red">*</span></label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control login_field" id="password" placeholder="Password" name="password" required>
                  </div>
                </div>
            	<div class="form-group clearfix">
                  <label class="control-label col-sm-4" for="password" name="cpassword">Confirm Password <span style="font-size:x-large;color:red">*</span></label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control login_field" id="cpassword" name="cpassword" placeholder="Confirm Password" required>
                    <input type="hidden" class="form-control login_field" id="userId" name="userId"  value="<?php if($userId){echo $userId;}else{echo "";}?>">
                    <input type="hidden" class="form-control login_field" id="token" name="token"  value="<?php if(@$token){echo @$token;}else{echo "";}?>">
                    <button type="submit" name="update_password" value="" href="<?php echo PROFILE_URL?>" class="btn register_btn blue_btn mt25">Save</button>
                    <a type="button" href="<?php echo SITEURL; ?>" class="btn register_btn red_btn mt25">Cancel</a>
                  </div>
                </div>
                <div class="clearfix"></div>    
			</form>
		</div>
		</div>
		</div>
	</div>
</body>
</html>