<!DOCTYPE html>
<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
	
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML"></script>
<html lang="en">
<body>
<div id="wrapper">
   <section class="main_body">
        <div class="container">
        <div class="row profile">
            <h1>Summary Report</h1>
				<ul class="report_text">
                    <li><img src="<?php echo IMAGES_URL ?>report_icon.png" class="img-responsive"></li>
                    <li>
                    Summary report for test : 
                    <span><?php echo $testname->testName?></span>
                    </li>
                </ul>
                 	<div class="table-responsive mt25 clearfix">          
                              <table class="table table-bordered">
                                <thead>
                                  <tr class="tble_clr1">
                                    <th colspan="6">
                                         Test Summary Report
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
								 <tr>
                                    <th width="25%">Question Id <span class="pull-right tble_blue_txt"></span></th>
                                    <th width="25%">Question <span class="pull-right tble_blue_txt"></span></th>
                                    <th width="25%">Total Answers <span class="pull-right tble_blue_txt"></span></th>
                                    <th width="25%">Correct Answer <span class="pull-right tble_blue_txt"></span></th>
                                    <th width="25%">My Answer<span class="pull-right tble_blue_txt"></span></th>
                                    <th width="25%">Is Correct<span class="pull-right tble_blue_txt"></span></th>
                                  </tr>
								  <?php 
								    $TY=$_GET['TY'];
								  if($details){ 
									  $Myopt="" ; $Myoption="" ;$option="" ; $crctOption="";$i=1; 
									  foreach($details as $detail){ 
										  
										  $Myopt="" ; $crctOption="";$USerOptArray=array();
										  $USerOptArray=array();
										  $optionArray=array();
										  $question=$detail->question;
										  $option=$detail->option;
										  if($option){
											    $crctOption="";
											    foreach($option as $opt){ 
											  
											   $textType=$opt->textType;
											  if($textType == 0 || $textType == 1 ){
													$opton= strip_tags($opt->option);
												 }else if($textType == 2){
													 $src=UPLOADS_OPTIONS_PATH.$opt->option;
													$opton= "<img src=$src width='90' height='45'>";
												 }else if($textType == 3){
													  $src= UPLOADS_OPTIONS_PATH.$opt->option;
													$opton= "<video width='110' height='95' controls> <source src=$src ></video>";
												}else if($textType == 3){
													$opton= "<audio width='110' height='95' controls> <source src=$src ></audio>";
												}
											  
											  $crctOption.= $opton.',';  $optionArray[]=$opt->option_id;
											  }
										      $crctOption = rtrim($crctOption, ", ");
										     
										  }
									//for my option	 
									$Myoption=$detail->Myoption; 
									if($Myoption){ $Myopt="";
										foreach($Myoption as $mopt){
											if($mopt->queType==1 || $mopt->queType==0)
											{  $normalOptions=$mopt->normalOptions;
										        foreach($normalOptions as $opt){
													   $op=strip_tags($opt->option);
														$textType=$opt->textType;
												   if($textType == 0 || $textType == 1 ){
														$Mopton= strip_tags($op);
													 }else if($textType == 2){
														 $src=UPLOADS_OPTIONS_PATH.$op;
														$Mopton= "<img src=$src width='50' height='45'>&nbsp;";
													 }else if($textType == 3){
														  $src= UPLOADS_OPTIONS_PATH.$op;
														$Mopton= "<video width='90' height='90' controls> <source src=$src ></video><br>";
													}else if($textType == 3){
														$Mopton= "<audio width='50' height='50' controls> <source src=$src ></audio>";
													}
													 
													  $USerOptArray[]=$opt->option_id;
													  $Myopt.=$Mopton.',';
												}$Myopt = rtrim($Myopt, ", ");
												
											  
											  }else{
												   $Myopt=$mopt->textOption;
											  }
										}
										  
								  }
								  $TotalAnswers=$detail->TotalAnswers; 
									if($TotalAnswers){ $Tsdopt=""; 
										foreach($TotalAnswers as $totalopt){
											 $op=$totalopt->option;
													$textType=$totalopt->textType;
												   if($textType == 0 || $textType == 1 ){
														$Topt= strip_tags($op);
													 }else if($textType == 2){
														 $src=UPLOADS_OPTIONS_PATH.$op;
														$Topt= "<img src=$src width='50' height='45'>&nbsp;";
													 }else if($textType == 3){
														  $src= UPLOADS_OPTIONS_PATH.$op;
														$Topt= "<video width='90' height='90' controls> <source src=$src ></video><br>";
													}else if($textType == 3){
														$Topt= "<audio width='50' height='50' controls> <source src=$src ></audio>";
													}
													 
													  $Tsdopt.=$Topt.',';
												
											}$Topt = rtrim($Tsdopt, ", ");
										  
								  }
									?>
                                  <tr>
                                    <td width="25%"><?php echo $i;?></td>
                                    <td width="25%"><?php echo strip_tags($question->que);?></td>
									 <td width="25%"><?php if( @$Topt) echo @$Topt;else echo '-';?></td>
                                    <td width="25%"><?php if(@$crctOption) echo @$crctOption;else echo '-';?></td>
                                    <td width="25%"><?php if(@$Myopt) echo @$Myopt;else echo '-';?></td>
                                    <td width="25%"><?php if(@$detail->isCorrect) echo @$detail->isCorrect;else echo '-';?></td>
                                  </tr>
								  <?php $i++; } } ?>
                                  
                                </tbody>
                              </table>
                          </div>
						   <?php if($TY=="multiple"){?>
                                <center><a type="button" href="<?php echo ACCOUNT_URL; ?>" class="btn register_btn blue_btn mt25"><i class="fa fa-home" aria-hidden="true"></i> Back to Home</a></center> <?php }else{ ?>
								<center><a type="button" href="#" result="allow" onclick="closeWindow();" class="btn register_btn red_btn mt25">close</a></center><?php } ?>
			</div>
    </section>
 </div>
</body>
</html>
<script type="text/javascript">
/*history.pushState({ page: 1 }, "Title 1", "#no-back");
window.onhashchange = function (event) {
window.location.hash = "no-back";
};*/
function closeWindow(){
	window.close();
	window.opener.clearDiv();
window.opener.location.reload();
}
function clearDiv(){
	 window.location="<?php echo ACCOUNT_URL ?>";
}
</script>
<script type="text/javascript">
   $(document).ready(function () {
    //Disable cut copy paste
   /* $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });*/
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });
</script>