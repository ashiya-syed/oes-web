<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Content extends Oescontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->helper('common_helper');
	}
  function termsAndConditions(){
    $this->load->view('content/header');
    $this->load->view('content/termsAndConditions');
  }

function privacyPolicy(){
    $this->load->view('content/header');
    $this->load->view('content/privacy_policy');
  }
function refundPolicy(){
    $this->load->view('content/header');
    $this->load->view('content/refund_policy');
  }
function aboutUs(){
    $this->load->view('content/header');
    $this->load->view('content/aboutUs');
  }
function registrationAndServiceDetails(){
    $this->load->view('content/header');
    $this->load->view('content/registrationAndServiceDeatails');
  }
function contactUs(){
  $data=array();
 if(isset($_POST['submit'])){
                if (!empty($_POST)) 
				{ $data['input']=$_POST;
					$this->load->library('form_validation');
					$this->form_validation->set_rules('email_id','email','trim|required|valid_email');
					$this->form_validation->set_rules('message','message','trim|required');
                    if($_POST['mobile']){
					$this->form_validation->set_rules('mobile','Mobile','trim|required|min_length[10]|max_length[10]');
                       }
                    if($this->form_validation->run()!=false)
						{ 
                         $message="";
						 $message.=$this->input->post('message');
                         if($this->input->post('mobile')){
						  $message.="<br>Contact number : ".$this->input->post('mobile');
                          }
						 $subject="Contact us";
						 $email_id=$this->input->post('email_id');
						 $smtpEmailSettings = $this->config->item('smtpEmailSettings');
						 $isEmailSent = @sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
						 $this->session->set_flashdata('message', "<div class='alert alert-info alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Thanks for contacting us.</strong> </div>");
                         $data['successMessage'] = "Thanks for contacting us.";						 
                          redirect(CONTACT_US);
                        }
						else {
						$data['errorMessage'] = validation_errors();
				}
          } 
       }
    $this->load->view('content/header');
    $this->load->view('content/contactUs',$data);
  }

}
?>