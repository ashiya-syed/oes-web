<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Home extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('Homemodel');
	}
	
	function index()
	{
		$this->load->view('header.php');
		$this->load->view('index.php');
		$this->load->view('footer.php');
	}
	function login()
	{  
	    $this->load->library('form_validation');
		$this->form_validation->set_rules('name',' Name','trim|required');
		$this->form_validation->set_rules('password','password','trim|required');
		
		if($this->form_validation->run()!=false)
		{
			$name=$this->input->post('name');
			$password=$this->input->post('password');
			$result = $this->Homemodel->login($name,$password);
			if($result){
				redirect('Home/register');
			}else{
				echo "No user Found";die();
			}
			
		}
		
		else
		{
			echo validation_errors();die();
		}
	}
	function register()
	{
	  $this->load->view('footer.php');

	}
	
	
}