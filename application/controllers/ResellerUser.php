<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class ResellerUser extends Oescontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Usermodel'); 
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
	}
	
	/*******************
	********************
	This method is useful to 
	user login into the dashboard.
	********************
	********************/
	function index()
	{
		
		$data=array();
		$name=$this->uri->segment(1);
		$resellerDetails=$this->Usermodel->checkResellerUsersExists($name);
		$data['resellerDetails']=$resellerDetails;
		try{
			 $this->load->helper('url');
			 // 
				if (!empty($_POST)) 
				{
					$this->load->library('form_validation');
					$this->form_validation->set_rules('username','Email or Phone Number','trim|required');
					$this->form_validation->set_rules('password','Password','trim|required');
					if($this->form_validation->run()!=false)
					{
						$Email_OR_phone = trim($this->input->post('username'));
						$password       = trim($this->input->post('password'));
						$resellerId=$resellerDetails->resellerId;
						$result = $this->Usermodel->resellerlogin($Email_OR_phone, $password,$resellerId);
						//debug($this->db->last_query());
						if($result) 
						{
                                 $userId=$result->userId;
								 $checkUserLogedInOtherDevice=$this->Usermodel->checkUserLogedInOtherDevice($userId);//print_r($checkUserLogedInOtherDevice);die();
								if($checkUserLogedInOtherDevice && $checkUserLogedInOtherDevice->isActive==1){
									$mes="You already logged in device ".$checkUserLogedInOtherDevice->browser_or_device_details.".Please logout from that device to login.";
									$link='<a href="'.LOGOUT_FROM_ALL_DEVICES.'/'.$userId.'">Logout</a>';
									$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".$mes."</strong>".$link."</div>");
                                    redirect(SITEURL.$name);
								}							
					       $token=generateRandNumber(6);
						   $exp=strtotime($result->accountExpiryDate);
				           if($result->is_user_verified == 0){
						       $data['errorMessage'] = "Verify your account to login";
							   $this->load->view('index',$data);
					         }else{
						      if($exp){
								if($result->accountExpiryDate>$result->createdTime)
								{ 
							         $where=array('userId'=>$result->userId);
							         $loginUpdate['updatedTime']=date("Y-m-d H:i:s");
									 $res= $this->insertOrUpdate(TBL_USERS,$where,$loginUpdate);
									
									$trackData['userId']=$result->userId;
									 $trackData['ipAddresss']=$this->input->ip_address();
									 $trackData['createdTime']=date("Y-m-d H:i:s");
									  /* new */
									 
									 $trackData['token'] = $token;
                                     $trackData['browser_or_device_details'] = $_SERVER['HTTP_USER_AGENT'];
									 $trackData['isActive'] = 1;
									 $trackData['isDeleted'] = 0;
									 $this->session->set_userdata('loginToken',$token);
									 $this->insertOrUpdate(TBL_USER_TRACK,$where=array(),$trackData);

								     $where=array('userId'=>$result->userId);
			                         $data['profileDetails']= $this->getSingleRecord(TBL_USERS,$where,$select="*");
									 $this->session->set_userdata('course',$data['profileDetails']->course);//16-18
                                     $this->session->set_userdata('name',$data['profileDetails']->userName);
									 $this->session->set_userdata('email',$data['profileDetails']->emailAddress);
									 $this->session->set_userdata('user_id',$data['profileDetails']->userId);
									 $this->session->set_userdata('profilePicture',$data['profileDetails']->profilePicture);
									 $this->session->set_userdata('phoneNumber',$data['profileDetails']->phoneNumber);
									 $this->session->set_userdata('reseller_id',$data['profileDetails']->resellerId);//30
									 $this->session->set_userdata('isPromoterUser',$result->isPromoterUser);//30
									 $data['successMessage'] = LOGIN_SUCCESS;
									 redirect(DASHBOARD_URL.'?name='.$result->userName); 
							     }else{
									 $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your account is expired</strong> </div>");
                                     redirect(SITEURL,'refresh');
								}
						    
					         }
							 $where=array('userId'=>$result->userId);
							         $loginUpdate['updatedTime']=date("Y-m-d H:i:s");
									 $res= $this->insertOrUpdate(TBL_USERS,$where,$loginUpdate);
									 $trackData['userId']=$result->userId;
									 $trackData['ipAddresss']=$this->input->ip_address();
									 $trackData['createdTime']=date("Y-m-d H:i:s");
									 $trackData['token'] = $token;
                                     $trackData['browser_or_device_details'] = $_SERVER['HTTP_USER_AGENT'];
									 $trackData['isActive'] = 1;
									 $trackData['isDeleted'] = 0;
									 $this->session->set_userdata('loginToken',$token);
									 $this->insertOrUpdate(TBL_USER_TRACK,$where=array(),$trackData);
									 $this->session->set_userdata('loginToken',$token);
									 $cookie= array('name'   => 'loginToken', 'value'  => ($token+1), 'expire' => '630720000','path'=>'/'  );
									 $this->input->set_cookie($cookie);
							         $where=array('userId'=>$result->userId);
							
			                $data['profileDetails']= $this->getSingleRecord(TBL_USERS,$where,$select="*");
						    $this->session->set_userdata('name',$data['profileDetails']->userName);
							$this->session->set_userdata('course',$data['profileDetails']->course);//16-18
                            $this->session->set_userdata('email',$data['profileDetails']->emailAddress);
					        $this->session->set_userdata('user_id',$data['profileDetails']->userId);
					        $this->session->set_userdata('profilePicture',$data['profileDetails']->profilePicture);
					        $this->session->set_userdata('phoneNumber',$data['profileDetails']->phoneNumber);
							 $this->session->set_userdata('reseller_id',$data['profileDetails']->resellerId);//30
							 $this->session->set_userdata('isPromoterUser',$result->isPromoterUser);//30

							$data['successMessage'] = LOGIN_SUCCESS;
							redirect(DASHBOARD_URL.'?name='.$result->userName); 
							 }
						} 
						else 
						{
								//$data['errorMessage'] = EMAIL_OR_PHONE_AND_PASSWORD_NOT_MATCH;
								$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".EMAIL_OR_PHONE_AND_PASSWORD_NOT_MATCH."</strong> </div>");
                                redirect(SITEURL.$name);
						}
					}
					else {
						//$data['errorMessage'] = validation_errors();
						$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".validation_errors()."</strong> </div>");
                             redirect(SITEURL.$name);
					}
					
				}
				
				
		 }catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		 } 	
		
		
		if($resellerDetails){
			
			$this->load->view('resellerLogin',$data);
		}else{
			$this->load->view('pageNotFound');
		}
		
		
	}
	
						  
}
?>