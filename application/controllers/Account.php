<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Account extends Oescontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Usermodel'); 
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		$this->load->helper('common_helper');
	}
	
	/*******************
	********************
	This method is useful to 
	user login into the dashboard.
	********************
	********************/
	function index()
	{
	  
		try{
				$this->load->helper('url');
				$data=array();
				if($this->session->userdata('user_id')){
					redirect(DASHBOARD_URL.'?name='.$this->session->userdata('name')); 
				}
				if (!empty($_POST)) 
				{
					$this->load->library('form_validation');
					$this->form_validation->set_rules('username','Email or Phone Number','trim|required');
					$this->form_validation->set_rules('password','Password','trim|required');
					if($this->form_validation->run()!=false)
					{
						$Email_OR_phone = trim($this->input->post('username'));
						$password       = trim($this->input->post('password'));
						$result = $this->Usermodel->login($Email_OR_phone, $password);
						if($result) 
						{  
					        $exp=strtotime($result->accountExpiryDate);
				           if($result->is_user_verified == 0){
						      // $data['errorMessage'] = "Verify your account to login";
							   $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Verify your account to login.</strong> </div>");
                               //redirect(SITEURL,'refresh');
							   redirect(SITEURL.'account');
					         }else{
								 if($result->isActive == 0){
									 $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your account is deactivated.Please contact admin for deatils.</strong> </div>");
									 redirect(SITEURL.'account');
								 }
								 $userId=$result->userId;
								 $checkUserLogedInOtherDevice=$this->Usermodel->checkUserLogedInOtherDevice($userId);//print_r($checkUserLogedInOtherDevice);die();
								if($checkUserLogedInOtherDevice && $checkUserLogedInOtherDevice->isActive==1){
									$mes="You already logged in device ".$checkUserLogedInOtherDevice->browser_or_device_details.".Please logout from that device to login.";
									$link='<a href="'.LOGOUT_FROM_ALL_DEVICES.'/'.$userId.'">Logout</a>';
									$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".$mes."</strong>".$link."</div>");
                                     redirect(SITEURL.'account');
								}
								$token=generateRandNumber(6);
						      if($exp){
								if($result->accountExpiryDate>$result->createdTime)
								{    
							         $where=array('userId'=>$result->userId);
							         $loginUpdate['lastLogin']=date("Y-m-d H:i:s");
									 $res= $this->insertOrUpdate(TBL_USERS,$where,$loginUpdate);
									
									 $trackData['userId']=$result->userId;
									 $trackData['ipAddresss']=$this->input->ip_address();
									 $trackData['createdTime']=date("Y-m-d H:i:s");
									  /* new */
									 
									 $trackData['token'] = $token;
                                     $trackData['browser_or_device_details'] = $_SERVER['HTTP_USER_AGENT'];
									 $trackData['isActive'] = 1;
									 $trackData['isDeleted'] = 0;
									 $this->session->set_userdata('loginToken',$token);
									 /* end */
									 $this->insertOrUpdate(TBL_USER_TRACK,$where=array(),$trackData);
									
								     $where=array('userId'=>$result->userId);
			                         $data['profileDetails']= $this->getSingleRecord(TBL_USERS,$where,$select="*");
									 $this->session->set_userdata('course',$data['profileDetails']->course);//16-18
                                     $this->session->set_userdata('name',$data['profileDetails']->userName);
									 $this->session->set_userdata('email',$data['profileDetails']->emailAddress);
									 $this->session->set_userdata('user_id',$data['profileDetails']->userId);
									 $this->session->set_userdata('profilePicture',$data['profileDetails']->profilePicture);
									 $this->session->set_userdata('phoneNumber',$data['profileDetails']->phoneNumber);
									 $this->session->set_userdata('reseller_id',$data['profileDetails']->resellerId);//30
									 $this->session->set_userdata('isPromoterUser',$result->isPromoterUser);//30

									 $data['successMessage'] = LOGIN_SUCCESS;
									 redirect(DASHBOARD_URL.'?name='.$result->userName); 
							     }else{
									 $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your account is expired.</strong> </div>");
                                     //redirect(SITEURL,'refresh');
									 redirect(SITEURL.'account');
								}
						    
					         }
							 
							 $where=array('userId'=>$result->userId);
							 $loginUpdate['lastLogin']=date("Y-m-d H:i:s");
							 $res= $this->insertOrUpdate(TBL_USERS,$where,$loginUpdate);
							 
							 $trackData['userId']=$result->userId;
							 $trackData['ipAddresss']=$this->input->ip_address();
							 $trackData['createdTime']=date("Y-m-d H:i:s");
							       /* new */
                                    $trackData['token'] = $token;
                                    $trackData['browser_or_device_details'] = $_SERVER['HTTP_USER_AGENT'];
									$trackData['isActive'] = 1;
									$trackData['isDeleted'] = 0;
									$this->session->set_userdata('loginToken',$token);
									 $cookie= array('name'   => 'loginToken', 'value'  => ($token+1), 'expire' => '630720000','path'=>'/'  );
									 $this->input->set_cookie($cookie);
									 /* end */
							 $this->insertOrUpdate(TBL_USER_TRACK,$where=array(),$trackData);
							 
							$where=array('userId'=>$result->userId);
			                $data['profileDetails']= $this->getSingleRecord(TBL_USERS,$where,$select="*");
							$this->session->set_userdata('course',$data['profileDetails']->course);//16-18
                            $this->session->set_userdata('name',$data['profileDetails']->userName);
					        $this->session->set_userdata('email',$data['profileDetails']->emailAddress);
					        $this->session->set_userdata('user_id',$data['profileDetails']->userId);
					        $this->session->set_userdata('profilePicture',$data['profileDetails']->profilePicture);
					        $this->session->set_userdata('phoneNumber',$data['profileDetails']->phoneNumber);
							 $this->session->set_userdata('reseller_id',$data['profileDetails']->resellerId);//30
							 $this->session->set_userdata('isPromoterUser',$result->isPromoterUser);//30

							$data['successMessage'] = LOGIN_SUCCESS;
							redirect(DASHBOARD_URL.'?name='.$result->userName); 
							 }
						} 
						else 
						{
								$data['errorMessage'] = EMAIL_OR_PHONE_AND_PASSWORD_NOT_MATCH;
								$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".EMAIL_OR_PHONE_AND_PASSWORD_NOT_MATCH."</strong> </div>");
                                redirect(SITEURL.'account');
						}
					}
					else {
						$data['errorMessage'] = validation_errors();
						$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".validation_errors()."</strong> </div>");
                       // redirect(SITEURL);
						redirect(SITEURL.'account');
						
					}
					
				}
				
				
		 }catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		 } 	
		
		
		$this->load->view('index',$data);
		
	}
	
	/*******************
	********************
	This method is useful to 
	logout the user.
	********************
	********************/
	function logout(){
		  
			$trackData['isActive']=0;
			if($this->session->userdata('loginToken')){
			    $token=$this->session->userdata('loginToken');	
			}
			else{
				$token=($this->input->cookie('loginToken')-1);
			}
			$res= $this->insertOrUpdate(TBL_USER_TRACK,$where=array('token'=>$token),$trackData);
            //$this->session->sess_destroy();
			//$this->session->unset_userdata('name');
			$this->session->unset_userdata('email');
			$this->session->unset_userdata('phoneNumber');
			$this->session->unset_userdata('profilePicture');
			$this->session->unset_userdata('user_id');
			//$this->session->unset_userdata('reseller_id');
			$this->session->unset_userdata('testType');
			$this->session->unset_userdata('examId');
			$this->session->unset_userdata('packageiD');
			$this->session->unset_userdata('uniqueId');
			$this->session->unset_userdata('endExam');
			$this->session->unset_userdata('loginToken');
			if($this->session->userdata('reseller_id'))
            redirect(SITEURL.$this->session->userdata('name'));
		    else
			redirect(SITEURL);	
            
        }
		
		function logoutFromAllDevices(){
			$userId=$this->uri->segment(3);
			$trackData['userID']=$userId;
			$trackData['isActive']=0;
			
		   $res= $this->insertOrUpdate(TBL_USER_TRACK,$where=array('userID'=>$userId),$trackData);
		   if($res){
			   $this->session->unset_userdata('endExam');
			$this->session->unset_userdata('loginToken');
			   $this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>logout from all the devices successfully.</strong> </div>");
		   }else{
			   $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Error occured.</strong> </div>");
		   }
		   $userDetails=$this->getSingleRecord(TBL_USERS,$where=array('userId'=>$userId),'*');
		   if($userDetails && $userDetails->resellerId){
			    redirect(SITEURL.$userDetails->userName);
		   }
			redirect(SITEURL.'account');

		}
		
	/*******************
	********************
	This method is useful to 
	get city names based on stateId.
	********************
	********************/	
	function citiesByState()
	 {
		 $stateID = trim($this->input->post('stateID'));
		 $from = trim($this->input->post('from'));
		 $where = array('isActive' => 1,'isDeleted' => 0,'stateID'=>$stateID);
		 $result=$this->getAllRecords(TBL_CITIES,$where);
	     echo "<option value=''>Select City</option>";
			  if($result && count($result)>0){
				foreach($result as $city){
			            echo "<option value=$city->cityID>$city->cityName</option>";
	            } 
	         }
			 die();
	}
		
		
	/*******************
	********************
	This method is useful to 
	register  the user.
	********************
	********************/
	//new
	  function registration(){
		  $data=array();
         try{
			if(isset($_POST['submit'])){
				
				if (!empty($_POST)) 
				{
					$this->load->library('form_validation');
					$this->form_validation->set_rules('email','email','trim|required|valid_email');
					$this->form_validation->set_rules('name','name','trim|required|min_length[3]|max_length[15]');
					$this->form_validation->set_rules('password','Password','trim|required|min_length[6]|max_length[20]');
					$this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]');
					$this->form_validation->set_rules('mobile','Phone Number','trim|required|min_length[10]|max_length[10]');
					$this->form_validation->set_rules('otp','Otp','trim|required');

						if($this->form_validation->run()!=false)
						{
							$email = trim($this->input->post('email'));
							$name = trim($this->input->post('name'));
							$password = trim($this->input->post('password'));
							$cpassword = trim($this->input->post('cpassword'));
							$phoneNumber = trim($this->input->post('mobile'));
							$otp = trim($this->input->post('otp'));
							$where=array('token'=>$otp,'phoneNumber'=>$phoneNumber,'isactive'=>1,'purpose'=>'registration');
							$checkOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*");
							//print_r( $this->input->post('course'));die();
							if(count($checkOtp)>0){
							$course=$this->input->post('course');
							if($course){
								$course=implode(',',$course);
							}
							$test = $this->input->post('test');
                            if($test){
							$test=implode(',',$test);
                             } 
                            $package = $this->input->post('package');
                            if($package){
							$package=implode(',',$package);
                            } 
							if(empty($course)){
								 $this->session->set_flashdata('error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please select at least one course.</strong> </div>");
                                 redirect(REGISTRATION_URL);
							}
							if(empty($package) && empty($test)){
								 $this->session->set_flashdata('error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please select at least one package or test.</strong> </div>");
                                 redirect(REGISTRATION_URL);
							}
							$roleID = trim($this->input->post('roleID'));
							$token=md5(uniqid());
							$pwd=encrypt($password);
							if($test){
							$data = array('userName' => $name , 'emailAddress' => $email, 'phoneNumber' => $phoneNumber, 'password' =>$pwd,'roleID'=>$roleID,'token'=>$token,'testIds'=>$test,'course'=>$course);
							}
							if($package){
								$data = array('userName' => $name , 'emailAddress' => $email, 'phoneNumber' => $phoneNumber, 'password' => $pwd,'roleID'=>$roleID,'token'=>$token,'packageIds'=>$package,'course'=>$course);
							}
							if($test && $package){
							$data = array('userName' => $name , 'emailAddress' => $email, 'phoneNumber' => $phoneNumber, 'password' => $pwd,'roleID'=>$roleID,'token'=>$token,'testIds'=>$test,'packageIds'=>$package,'course'=>$course);
							}
							//$data = array('userName' => $name , 'emailAddress' => $email, 'phoneNumber' => $phoneNumber, 'password' => md5($password),'roleID'=>$roleID,'token'=>$token,'testIds'=>$test,'packageIds'=>$package);
							$details=$this->Usermodel->user_exists($phoneNumber,$email);
							
							if($details && $details->isPromotionMember == 0){
								
								$this->session->set_flashdata('error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>The phone number or email already exists.</strong> </div>");
								
								redirect(REGISTRATION_URL);
							}else{
								if($details->isPromotionMember == 1){
									$where1=array('userId'=>$details->userId);
									$data['isPromotionMember']= 0;
									//$data['is_user_verified']= 1;
									$inputdata=$data;
									$result=$this->insertOrUpdate(TBL_USERS,$where1,$inputdata);
								}else{
									$result=$this->Usermodel->register($data);	
								}
							   if($result) 
								 {
								  $savePcakOrTest=$this->savePcakOrTest($test,$package,$result);	
								 
								  $subject="Verify your Account";
								  //new
								  $subject="Verify your Account";
								  $url=SITEURL.'Account/verifyAccount?token='.$token;
								  $emailData['url']=$url;
								  $emailData['name']=$name;
								  $emailData['email']=$email;
								  $emailData['password']=$password;
								  $emailData['phoneNumber']=$phoneNumber;
								  $message=$this->load->view('verifyEmail', $emailData,true);
								  //end
								  /*$url=SITEURL.'Account/verifyAccount?token='.$token;
								  $message= "Hi ".$name.',';
								  $message.= "Thanks for registering with OnyxEducationals.Your Details are<br>
					              <table><tr>Name:".$name."</tr><tr>Mobile:".$phoneNumber."</tr>
					              <tr>Email:".$email."</tr>
					              <tr>Password:".$password."</tr></table><br>";
								  $message.="<p>Please <a type='button' href='$url'  class='btn register_btn blue_btn mt5'>Click here</a> to check your packages or test details..</p>";
								 */
				                  $email_id=$email;
		                          $smtpEmailSettings = $this->config->item('smtpEmailSettings');
		                          $isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
		                          
								  $this->session->set_flashdata('message', "<div class='alert alert-info alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please verify your email id to login into your account.</strong> </div>");
                                  redirect(SITEURL);
								  
								 } 
							}
						}else{
							   $data['errorMessage'] ='Invalid Otp';
						   }
						}
						else {
							$data['errorMessage'] = validation_errors();
						}
				}
			}
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		    //$where = array('isActive'=>1,'isDeleted'=>0,'testType'=>1,'resellerId'=>0);
		    $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
			$data['tests'] = $this->getAllRecords(TBL_TESTS,$where,'*');
			$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
			$data['packages'] = $this->getAllRecords(TBL_PACKAGES,$where,'*');
			$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
			$data['courses'] = $this->getAllRecords(TBL_COURSES,$where,'*');
		    $this->load->view('registration',$data);
      }
	   function savePcakOrTest($test,$package,$userId){
		              if($test){   
									$where=array();
									$updata['createdTime']= date("Y-m-d H:i:s");
									$updata['userId']=$userId;
									$updata['packageId']=0;
									$updata['isActive']= 1;
									$updata['isDeleted']= 0;
									$updata['packageTests']= $test;
									$updata['couponCode']= 0;
									$updata['txnid']= 0;
									$updata['amount']= 0;
									$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
									
								}
								 if($package){
									 $packageArray=explode(',',$package);
									 foreach($packageArray as $packary){
									$pId=$packary;
									$where=array('package_id'=>$pId);
									$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
									$testId=$packageDetails->package_test;
									$where=array();
									$updata['createdTime']= date("Y-m-d H:i:s");
									$updata['userId']=$userId;
									$updata['packageId']=$pId;
									$updata['isActive']= 1;
									$updata['packageTests']=$testId;
									$updata['amount']= 0;
									$updata['couponCode']= 0;
									$updata['txnid']= 0;
									$updata['isDeleted']= 0;
									$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
									   }
									}
	 }
	  function checkRegistrationOtp(){
		              $otp = trim($this->input->post('otp'));
		              $mob = trim($this->input->post('mob'));
							$where=array('token'=>$otp,'phoneNumber'=>$mob,'isactive'=>1,'purpose'=>'registration');
							$checkOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*");
							
							if(count($checkOtp)>0){echo 0;die();}
								
								else{ echo 1;die();}
								
		                    
	 }
	     function sendOtp(){
		if($this->input->is_ajax_request())
		{      
			$phoneNumber=$this->input->post('mob');
		        $token=generateRandNumber(6);
				$to[] = array('to'=>$phoneNumber);
				
				$where=array('phoneNumber'=>$phoneNumber,'isActive'=>1,'isDeleted'=>0,'resellerId'=>0,'isPromotionMember'=>0);
				$numberExists=$this->getAllRecords(TBL_USERS,$where);
				
				if($numberExists){ echo "User already existed with this number.";die(); }
				//$jsondata = array('sender'=>'ONYXED','message'=>'Please use this otp for registering with onyx educationals '.$token,'sms'=>$to);
                //$jsondata = json_encode($jsondata);
                //$response = sendOTP($jsondata);
				$smsText = "Please use this otp for validate your account with onyx educationals ".$token; 
                $response = sendOTP($smsText, $phoneNumber);
				
			    $where=array('phoneNumber'=>$phoneNumber,'isActive'=>1);
				$checkUserMultipleotp=$this->getAllRecords(TBL_OTPS,$where);
				if($checkUserMultipleotp){
					$where=array('phoneNumber'=>$phoneNumber,'isactive'=>1);
					$udata['isactive']=0;
					$this->insertOrUpdate(TBL_OTPS,$where,$udata);
				}
				$purpose="registration";
				$Rdata['phoneNumber']=$phoneNumber;
				$Rdata['token']=$token;
				$Rdata['purpose']=$purpose;
				$Rdata['isactive']=1;
				$where=array();
			   $res=$this->insertOrUpdate(TBL_OTPS,$where,$Rdata);
			   echo 1;die();
		}
	  } 
	  function checkEmailExists(){
		if($this->input->is_ajax_request())
		{      
			    $email=$this->input->post('email');
		        $where=array('emailAddress'=>$email,'isActive'=>1,'isDeleted'=>0,'resellerId'=>0,'isPromotionMember'=>0);
				$emailExists=$this->getAllRecords(TBL_USERS,$where);
				if($emailExists){ echo "User already existed with this email.";die(); }
				else{ echo 1;die();}
			   
		}
	  }
	  
	  function verifyAccount(){
		 
		  $token = $_GET['token'];
		  $this->session->set_userdata('verifyToken',$token);
		  if(empty($token))
		  {
			redirect(SITEURL);
		  }
		  
		  $tokenDetails = $this->Usermodel->isValidToken(trim($token));
		   
		  if(empty($tokenDetails)){
			  $this->session->set_flashdata('successMessage', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Invalid token</strong> </div>");
			  redirect(SITEURL);
		  }
		    $where=array('token'=>trim($token));
			$res=$this->getSingleRecord(TBL_USERS,$where,'userId,userName,phoneNumber,emailAddress,testIds,packageIds,is_user_verified,createdTime,accountExpiryDate,resellerId,accountType');
			$data['userdetails']=$res;
			$accountType=$res->accountType;
			
			if($accountType == 2){
				$up=$this->Usermodel->UpdateUserVerified(trim($token));
				$this->session->set_flashdata('successMessage', "<div class='alert alert-info alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Thanks , Your email has been verified.Login to your account.</strong> </div>");
			    if($res->resellerId){
					  redirect(SITEURL.$res->userName);
				}else{
					  redirect(SITEURL);
				}
			}else{
				$testIDs=$res->testIds;
				$packageIds=$res->packageIds;
				//on 14 th
				$testIDs=$res->testIds;
				$packageIds=$res->packageIds;
				if($testIDs){ 
				    $where=array();
					$updata['createdTime']= date("Y-m-d H:i:s");
					$updata['userId']=$res->userId;
					$updata['packageId']=0;
					$updata['isActive']= 1;
					$updata['isDeleted']= 0;
					$updata['packageTests']= $testIDs;
					$updata['couponCode']= 0;
					$updata['txnid']= 0;
								$updata['amount']= 0;

					$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
					
				}
				if($packageIds){
		             $packageArray=explode(',',$packageIds);
					 foreach($packageArray as $packary){
					$pId=$packary;
					$where=array('package_id'=>$pId);
					$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
					$testId=$packageDetails->package_test;
					$where=array();
					$updata['createdTime']= date("Y-m-d H:i:s");
					$updata['userId']=$res->userId;
					$updata['packageId']=$pId;
					$updata['isActive']= 1;
					$updata['packageTests']=$testId;
					$updata['amount']= 0;
					$updata['couponCode']= 0;
					$updata['txnid']= 0;
					$updata['isDeleted']= 0;
					$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
					   }
					}
				$up=$this->Usermodel->UpdateUserVerified(trim($token));
				$this->session->set_flashdata('successMessage', "<div class='alert alert-info alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Thanks , Your email has been verified.Login to your account.</strong> </div>");
			    if($res->resellerId){
					  redirect(SITEURL.$res->userName);
				}else{
					  redirect(SITEURL);
				}
				
            }
			$up=$this->Usermodel->UpdateUserVerified(trim($token));
			$this->session->set_flashdata('successMessage', "<div class='alert alert-info alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Thanks , Your email has been verified.Login to your account.</strong> </div>");
			 if($res->resellerId){
					  redirect(SITEURL.$res->userName);
				}else{
					  redirect(SITEURL);
				}
			$this->session->set_flashdata('successMessage', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Thanks , Your email has been verified.Login to your account.</strong> </div>");
			$this->load->view('header');
			$this->load->view('viewDetails',$data);
			$extrafooter = $this->load->view('dashboard_script');
			$this->load->view('footer',$extrafooter);
			
	  }
	/*******************
	********************
	This method is useful to 
	update the user profile.
	********************
	********************/
	 function profile()
	 { 
	
	     $userId=$this->session->userdata('user_id');
	     $loginToken =$this->session->userdata('loginToken');
		 $notvalidToken=$this->checkTokenValid($loginToken);
         if(!$userId || $notvalidToken){
                redirect(LOGOUT_URL,'refresh');
            }
		
		 try{	 
			 $updateData=array();
			if(isset($_POST['save'])){  
		        if($this->input->post('name')){$name= trim($this->input->post('name'));$updateData['userName']=$name;}
				if($this->input->post('mobile')){$mobile= trim($this->input->post('mobile'));$updateData['phoneNumber']=$mobile;}
				if($this->input->post('address')){$address= trim($this->input->post('address')); $updateData['address']=$address;}
				if($this->input->post('gender')){$gender= trim($this->input->post('gender'));  $updateData['gender']=$gender	;}
				if($this->input->post('state')){$state= trim($this->input->post('state')); $updateData['state']=$state;		}
				if($this->input->post('city')){$city= trim($this->input->post('city')); $updateData['city']=$city	;	}
				if($this->input->post('zipCode')){$zipCode= trim($this->input->post('zipCode'));  $updateData['zipCode']=$zipCode;}
					if(isset($_FILES['profile_pic']))
				        {		
							$fileTypes = array('jpeg', 'png', 'jpg');
							if(!empty($_FILES['profile_pic']['name'])){
								 $icon_target_path = UPLOADS_PATH;
				                // $icon_target_path = $icon_target_path . basename($_FILES['profile_pic']['name']);
					             $response['file_name'] = basename($_FILES['profile_pic']['name']);
					         $filename=basename($_FILES['profile_pic']['name']);
					         $file_extension=pathinfo($_FILES['profile_pic']['name']);
					             $rand=rand();
								 $picname=$rand.time().'.'.strtolower($file_extension['extension']); 
								 $icon_target_path = $icon_target_path .  $picname;
					           if (in_array(strtolower($file_extension['extension']), $fileTypes)) {
						  
								  $movefile=move_uploaded_file($_FILES['profile_pic']['tmp_name'], $icon_target_path);
								 if($movefile){
									  $updateData['profilePicture']= $picname;
									  $this->session->set_userdata('profilePicture', $updateData['profilePicture']);
								  }else{
									 $profile_messege ="file module was not uploaded";
								   }
				                 }		
				            }
			            }
                     					
				  $updateData['updatedTime'] = date("Y-m-d H:i:s");
				 
				  $userId=$this->session->userdata('user_id');
				  $result=$this->Usermodel->updateUser($updateData,$userId);	
				  if($result == 1){$data['messege']=UPDATE_SUCCESS;}else{
					 $data['profile_messege']=UPDATE_FAILED; 
				  }
			 }
			 if(isset($_POST['update_password'])){
				
				 $this->load->library('form_validation');
		         $this->form_validation->set_rules('password',' Password','trim|required|min_length[6]|max_length[20]');
		         $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]');
				   if($this->form_validation->run()!=false)
				   {				
					 
					  $password = trim($this->input->post('password'));
					  $cpassword = trim($this->input->post('cpassword'));
					  $pwd=encrypt($password);
					  $updateData['password']=$pwd;						
					  $updateData['updatedTime'] = date("Y-m-d H:i:s");
					  $where=array('userId'=>$userId);
					  $result=$this->insertOrUpdate(TBL_USERS,$where,$updateData);
						if(!$result)
						{ 
							 $data['failure'] = UPDATE_FAILED;
						}else{
							 $data['success'] = PASSWORD_UPDATE_SUCCESS;
						}
					}else{
					   $data['failure'] = validation_errors();
				
					 }				   
				}
			 
			 $userId=$this->session->userdata('user_id');
			 $data['results']=$this->Usermodel->profile($userId);
			 $data['course']=0;
			 if($data['results']->course){
				 $where=array('course_id'=>$data['results']->course);
				 $res=$this->getSingleRecord(TBL_COURSES,$where);
				 $data['course']=$res->course_name;
			 }
			 
			 $data['cities']=$this->Usermodel->getCities();
		     $data['states']=$this->Usermodel->getStates();
			 $where=array('userId'=>$userId,'isActive'=>1);
			 $data['enrollmentDetails']=$this->getSingleRecord(TBL_ENROLLMENTDETAILS,$where,$select="*");
			 
			 $where=array('userId'=>$userId);
		     $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,course,emailAddress,phoneNumber,gender,profilePicture");
			 $data['courseDetails']=$this->Usermodel->courseDetails($data['userDetails']->course);
			$cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
				$data['cartCount']=$cartCount;
			}else{
				$data['cartCount']=0;
			}
			
			 $this->load->view('header',$data);
			 $this->load->view('profile',$data);
			 $this->load->view('footer');
		 }
		 catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	 }

   /*  function forgotpassword(){
		 $data=array();
		 if( isset($_POST['submit']) ) 
		{			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email_id','Email','trim|required|valid_email');		
			if($this->form_validation->run()!=false)
			{
				$email=$this->input->post('email_id');	
				$result = $this->Usermodel->email_exists($email);	
              	if($result){
						$resut=$this->Usermodel->recoverUserPassword($email);
						if($result){
							
							$this->session->set_flashdata('message', "<div class='alert alert-info alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please verify your Email id to change your password.</strong> </div>");
							 redirect(FORGOTPASSWORD);
						}else{
							  $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Mail sending failed.</strong> </div>");
							   redirect('FORGOTPASSWORD');
						}
					
					}else{
						$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>No user exists With this email.</strong> </div>");
							redirect(FORGOTPASSWORD);
				      }			 
				
				
			}
			else 
			{
				$this->session->set_flashdata('message', validation_errors());
			}
		}
		$this->load->view('exam_header');
		$this->load->view('forgotpassword', $data);
		$this->load->view('footer');
	 }
	
	function resetpassword(){
		
		$token=$this->uri->segment(3);
		$where=array('token'=>$token,'isActive'=>1);
		$details=  $this->getSingleRecord(RECOVERY_EMAILS,$where,$select="userID");
		if($details){
				$userId=$details->userID;
				/*$updateToken['isActive']=0;
				$where=array('userID'=>$userId,'token'=>$token);
				$this->insertOrUpdate(RECOVERY_EMAILS,$where,$updateToken);*/
			/*	$data['userId']=$userId;
				$data['token']=$token;
				$this->load->view('exam_header');
				$this->load->view('resetPassword',$data);
				$this->load->view('footer');
				 }
			   else{
				   
				$this->session->set_flashdata('messege', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your Token has been expired</strong> </div>");
				redirect(FORGOTPASSWORD);
				} 
		       		 
	}
	function changepassword(){
		 $userId = trim($this->input->post('userId'));
		 $where=array('userId'=>$userId);
		 $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
		 $data['userId']=$userId ;
		 if(isset($_POST['update_password'])){
			     $this->load->library('form_validation');
		         $this->form_validation->set_rules('password',' Password','trim|required|min_length[6]|max_length[20]');
		         $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]');
				   if($this->form_validation->run()!=false)
				   {				
					 
					  $password = trim($this->input->post('password'));
					  $cpassword = trim($this->input->post('cpassword'));
					  $userId = trim($this->input->post('userId'));
					  $token = trim($this->input->post('token'));
					  $updateData['password']=md5($password);					
					  $updateData['updatedTime'] = date("Y-m-d h:i:s");
					  //$userId=(int)$userId;
					  $where=array('userId'=>$userId);
					  
					   $result=$this->insertOrUpdate(TBL_USERS,$where,$updateData);
					 if(!$result) 
						{ 
					       $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Reset password is failed</strong> </div>");
							redirect(FORGOTPASSWORD);
						}
						else{
							//new
						  if($token){
							   $updateToken['isActive']=0;
							   $where=array('userID'=>$userId,'token'=>$token);
							   $this->insertOrUpdate(RECOVERY_EMAILS,$where,$updateToken);
							}
						  //end
							$this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a></strong> Login into your account with new password</div>");
							 redirect(SITEURL2);
						}
					}else{
						
						$data['error']=validation_errors();
						$data['userId']=$userId ;
						$data['ptoken']=$this->input->post('token');
						$this->load->view('exam_header');
				        $this->load->view('resetPassword',$data);
				        $this->load->view('footer');
					  }
		             }else{
                        $this->load->view('exam_header',$data);
				        $this->load->view('resetPassword',$data);
				        $this->load->view('footer');	
		 }						
	}*/
	
	/*function selectNumbers(){
		$where=array('phoneNumber'=>8985716639);
		$results=$this->getAllRecords(TBL_USERS,$where,'*');
		foreach($results as $res){
			$users[]=$res->userId;
			$udata['isActive']=0;
			$where=array('userId'=>$res->userId);
			$res=$this->insertOrUpdate(TBL_USERS,$where,$udata);
			
		}
		
	}*/
	 function forgotpassword(){
		 
		 $data=array();
		 $uId="";
		 @$uId=$this->uri->segment(3); $data['url']='';
		 if($uId){$data['uId']=$uId;
		   $where=array('userId'=>$uId);
		   $result =$this->getSingleRecord(TBL_USERS,$where,"*");
		   $data['url']=SITEURL.$result->userName;
		  }
		
		 if( isset($_POST['submit']) ) 
		{		
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email_id','Email','trim|required|valid_email');		
			if($this->form_validation->run()!=false)
			{
				$email=$this->input->post('email_id');
                if($uId){
					
					$where=array('userId'=>$uId,'emailAddress'=>$email);
					$result =$this->getSingleRecord(TBL_USERS,$where,"*");
				}else{
					$result = $this->Usermodel->email_exists($email);	
				}			
				if($result){
						$resut=$this->Usermodel->recoverUserPassword($email,$uId);
						if($result){
							//debug($result);
							$this->session->set_flashdata('message', "<div class='alert alert-info alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please verify your email id to change your password.</strong> </div>");
							
							if($result->resellerId)
							redirect(FORGOTPASSWORD.'/'.$result->userId);
						else
							redirect(FORGOTPASSWORD);
						}else{
							  $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Mail sending failed.</strong> </div>");
							  if($result->resellerId)
							redirect(FORGOTPASSWORD.'/'.$result->userId);
						else
							redirect(FORGOTPASSWORD);
						}
					
					}else{
						$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>No user exists with given email.</strong> </div>");
							//debug($uId);
							if($uId)
							redirect(FORGOTPASSWORD.'/'.$uId);
						else
							redirect(FORGOTPASSWORD);

							
				      }			 
				
				
			}
			else 
			{
				$this->session->set_flashdata('message', validation_errors());
			}
		}
		$this->load->view('forgotpassword_header',$data);
		$this->load->view('forgotpassword', $data);
		$this->load->view('footer');
	 }
	
	function resetpassword(){
		
		$token=$this->uri->segment(3);
		$where=array('token'=>$token,'isActive'=>1);
		$details=  $this->getSingleRecord(RECOVERY_EMAILS,$where,$select="userID");
		if($details){
			$userId=$details->userID;
			   $where=array('userId'=>$userId);
		        $result =$this->getSingleRecord(TBL_USERS,$where,"*");
				$userId=$details->userID;
				$data['url']="";
				if($result->resellerId){
					$data['url']=SITEURL.$result->userName ;
				}
				/*$updateToken['isActive']=0;
				$where=array('userID'=>$userId,'token'=>$token);
				$this->insertOrUpdate(RECOVERY_EMAILS,$where,$updateToken);*/
				$data['userId']=$userId;
				$data['token']=$token;
				$this->load->view('forgotpassword_header',$data);
				$this->load->view('resetPassword',$data);
				$this->load->view('footer');
				 }
			   else{
				   
				$this->session->set_flashdata('messege', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Your token has been expired</strong> </div>");
				redirect(FORGOTPASSWORD);
				} 
		       		 
	}
	function changepassword(){
		 $userId = trim($this->input->post('userId'));
		 $where=array('userId'=>$userId);
		 $data['userDetails']=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
		 
		 
		 $data['userId']=$userId ;
		 if(isset($_POST['update_password'])){
			     $this->load->library('form_validation');
		         $this->form_validation->set_rules('password',' Password','trim|required|min_length[6]|max_length[20]');
		         $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]');
				   if($this->form_validation->run()!=false)
				   {				
					 
					  $password = trim($this->input->post('password'));
					  $cpassword = trim($this->input->post('cpassword'));
					  $userId = trim($this->input->post('userId'));
					  $token = trim($this->input->post('token'));
					   $pwd=encrypt($password);
					  $updateData['password']=$pwd;
					 // $updateData['password']=md5($password);					
					  $updateData['updatedTime'] = date("Y-m-d H:i:s");
					//  $userId=(int)$userId;
					  $where=array('userId'=>$userId);
					  $result=$this->insertOrUpdate(TBL_USERS,$where,$updateData);
					  $res=$this->getSingleRecord(TBL_USERS,$where=array('userId'=>$userId),"*"); 
					 if(!$result)
						{ 
					       $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Reset password is failed</strong> </div>");
							if($res->resellerId)
							redirect(FORGOTPASSWORD.'/'.$userId);
						    else 
							redirect(FORGOTPASSWORD);	
						}
						if($result==1){
							
						  if($token){
							   $updateToken['isActive']=0;
							   $where=array('userID'=>$userId,'token'=>$token);
							   $this->insertOrUpdate(RECOVERY_EMAILS,$where,$updateToken);
							}
						  
						 
							$this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a></strong> Login into your account with new password.</div>");
							 if($res->resellerId){
							redirect(SITEURL.$res->userName);
							 }
						    else{ 
							redirect(SITEURL);
							}
						}
					}else{
						
						$data['error']=validation_errors();
						$data['userId']=$userId ;
						$data['ptoken']=$this->input->post('token') ;
						$this->load->view('exam_header');
				        $this->load->view('resetPassword',$data);
				        $this->load->view('footer');
					  }
		             }else{
                        $this->load->view('exam_header',$data);
				        $this->load->view('resetPassword',$data);
				        $this->load->view('footer');	
		 }						
	}
	 function getTotalAmount(){
		
		 $tests = trim($this->input->post('tests'));
		 $packages = trim($this->input->post('packages')); 
		 if(!empty($tests)) {
			$test=json_decode($tests,TRUE);
		  }
		 $test = implode(',', $test);
		if(!empty($packages)) {
		   $package=json_decode($packages,TRUE);
		 }
		 $package = implode(',', $package);
		
        $res=$this->Usermodel->getAmount($package,$test);
		if($res){
        $message = array('pamount' => $res['pamount'],'tamount' => $res['tamount'],'amount'=>$res['pamount']+$res['tamount']);	
		echo json_encode($message); die();
		}
	 }
	 function selectCourses(){
		 $resellerId=$this->session->userdata('reseller_id');
		 $isPromoterUser=$this->session->userdata('isPromoterUser');
		 $userId=$this->uri->segment(3);
		 $userDetails=$this->getSingleRecord(TBL_USERS,$where=array('userId'=>$userId),"*");
		 if($userDetails->course){
			$data['selectedCourses']=explode(',',$userDetails->course);
         }
		 if(@$_POST['save']){
			 $courses=@$_POST['course'];
			if(count($courses)>0){
				$str="";
				foreach($courses as $course){
					$str.=$course.',';
				}
				$courseIds=rtrim($str,',');
				$updata['course']=$courseIds;
				$where=array('userId'=>$userId);
				$res=$this->insertOrUpdate(TBL_USERS,$where,$updata);//print_r($this->db->last_query());die();
				if($res){
					$this->session->set_userdata('course',$courseIds);//16-18

					$this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a></strong> Course updated successfully.</div>");
			    redirect(SELECT_COURSE_URL.'/'.$userId);
				}else{
					$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a></strong> Course updating failed.</div>");
			    redirect(SELECT_COURSE_URL.'/'.$userId);
				}
			}else{
				$this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a></strong> Please select atleast one course.</div>");
			    redirect(SELECT_COURSE_URL.'/'.$userId);
			}
		 }
		 if($isPromoterUser == 1 || $resellerId == 0){
			 $data['courses']=$this->getAllRecords(TBL_COURSES,$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0),'*');
		 }else{
			$data['courses']=$this->getAllRecords(TBL_COURSES,$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId),'*'); 
		 }
		 
		 $this->load->view('header');
		 $this->load->view('selectCourse',$data);
		 $this->load->view('footer');
	 }
		
		function testOrPackByCourseIds(){
	    $tests=$packages=array();
		$courses=$this->input->post('courses'); 
		//$courses=7; 
		$resellerId=0;
        $tsql="SELECT  * FROM oes_tests WHERE course_id IN(".$courses.") AND `isActive` = 1 AND `isDeleted` =0 AND `resellerId` =  ".$resellerId."";
		$psql="SELECT  * FROM oes_packages WHERE courseId IN(".$courses.") AND `isActive` = 1 AND `isDeleted` =0 AND `resellerId` = ".$resellerId."";
		
		$tquery = $this->db->query($tsql);
		$pquery = $this->db->query($psql);
		if($tquery){
			$tests=$tquery->result();
		}
		if($pquery){
			$packages=$pquery->result();
		}
		@$data['tests'] = $tests;
		@$data['packages'] = $packages; 
	    $filter_view =$this->load->view('showTestsInRegistration',$data,true);
		echo $filter_view;die();
		}
	
				  
	
                                 
							
								  
}
?>