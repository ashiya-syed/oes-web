<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Home extends Oescontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('Homemodel');
		/* if($ip != "202.53.12.82"){
		   print_r('This site wont be available for u.');die();
	   } */
	}
	
	function index()
	{
		$userId=$this->session->userdata('user_id');
		if($userId > 0)
		{
			$where=array('userId'=>$userId);
    	    $userDetails=$this->getSingleRecord(TBL_USERS,$where,$select="userId,userName,emailAddress,phoneNumber,gender,profilePicture");
			$name = $userDetails->userName;
			redirect(DASHBOARD_URL.'?user='.$name, 'refresh');
		}
		$data['userCount'] = $this->Homemodel->getUserCount();
		$data['completedTests'] = $this->Homemodel->getTestsCompletedCount();
		$data['attemptedTests'] = $this->Homemodel->getTestsAttemptedCount();
		$data['attemptedQue'] = $this->Homemodel->getQueAttemptedCount();
		
		$this->load->view('home/home.php',$data);
		
	}
	function login()
	{  
	    $this->load->library('form_validation');
		$this->form_validation->set_rules('name',' Name','trim|required');
		$this->form_validation->set_rules('password','password','trim|required');
		
		if($this->form_validation->run()!=false)
		{
			$name=$this->input->post('name');
			$password=$this->input->post('password');
			$result = $this->Homemodel->login($name,$password);
			if($result){
				redirect('Home/register');
			}else{
				echo "No user Found";die();
			}
			
		}
		
		else
		{
			echo validation_errors();die();
		}
	}
	function register()
	{
	  $this->load->view('footer.php');

	}
	
	
}