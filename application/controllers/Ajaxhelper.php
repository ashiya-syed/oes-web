<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Ajaxhelper extends Oescontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
	}
	
	
	function getTestRecordsByType()
	{
		if($this->input->is_ajax_request())
		{
			$userId=$this->session->userdata('user_id');
	        $loginToken =$this->session->userdata('loginToken');
		    $notvalidToken=$this->checkTokenValid($loginToken);
            if(!$userId || $notvalidToken){
                redirect(LOGOUT_URL,'refresh');
            }
			$practicalTests="";
			$Ttype = $this->input->post('Ttype');
			$limit = DEFAULT_LIMIT;
			$practicalTests = $this->Testsmodel->getTests($Ttype,$limit);
			if($practicalTests){

			for($i=0;$i<count($practicalTests);$i++)
			{
				$testId=$practicalTests[$i]->testId;
				$packageId=$practicalTests[$i]->packageId;
				$uniqueId=$practicalTests[$i]->uniqueId;
				//$where=array('testId'=>$testId,'testType'=>$Ttype,'userId'=>$userId,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'status'=>TEST_COMPLETED,'isActive'=>1);
				$where=array('testId'=>$testId,'userId'=>$userId,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'status'=>TEST_COMPLETED,'isActive'=>1);
				$count=$this->getAllRecords(TBL_TIMES,$where,$select="status");//completed tests
				if($count){
				$practicalTests[$i]->count=count($count);
				}else{$practicalTests[$i]->count=0;}
				//$where=array('testId'=>$testId,'testType'=>$Ttype,'userId'=>$userId,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'isActive'=>1); //on 1st
				$where=array('testId'=>$testId,'userId'=>$userId,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'isActive'=>1);
				$attempts=$this->Testsmodel->getNumberOfAttempts(TBL_TIMES,$where); //number of attempts wether complete or not no need
				if($attempts){
				$practicalTests[$i]->attempted=$attempts->attempts;
				}else{$practicalTests[$i]->attempted=0;}
				$presentSatus=$this->Testsmodel->getLatestStatus($testId, $userId, $uniqueId);
				$practicalTests[$i]->redo=0;
				if($presentSatus){
				  if($presentSatus->status == 2){$practicalTests[$i]->redo=1;}
				} 
			}
            
			
			}
            $data['practicalTests']=$practicalTests;
            $data['Ttype']=$Ttype;
			$res=$this->load->view('ajax',$data);
			/*if($practicalTests){
				$i=1;
			    foreach($practicalTests as $practicalTest){
					 $expireMessege= "";
			               $imageUrl =  IMAGES_URL.'thumb_img.jpg';
			               $testUrl =   SITEURL2.'/dashboard/startExam/mainexam?test='.$practicalTest->testId.'&package='.$practicalTest->packageId;
						   $type=MAIN_TEST;
						    $attempts=$practicalTest->attempts ;
							$uniqueId=$practicalTest->uniqueId;
							$package_name=$practicalTest->package_name;
							$count=$practicalTest->count;
							$class="btn register_btn blue_btn mt5"; $style="";
							if($count){
								if($count==1){
									 $button="2nd attempt";
								}elseif($count==2){
									$button="Last Attempt";
								}else{
									$class="btn login_btn green_btn mt5";
									$button="completed";
								}
							}else{
								$button="start Test";
							}
							if($attempts){
								$messege="Attempts:".$attempts;
							}else{
								$messege="Practice section exam only 3 times";
                             }
							 if($practicalTest->expiryDate){
								 $expiryDate=date('d-m-Y',strtotime($practicalTest->expiryDate));
								  $expireMessege= "Expires on : $expiryDate";
							 }
							 $status=$practicalTest->redo;
							 if($status){
								 $button="redo test";
								 $class="btn login_btn green_btn mt5"; $style='style=" background-color: orange"';
							 }
							  if($count==3){
								 $displayButton="<a type='button' $style if($count==3){ echo 'disabled';}   class='$class;'>$button</a>";
							 } else{
									 $displayButton="<a type='button' $style if($count==3){ echo 'disabled';} onclick='start($practicalTest->testId,$type,$practicalTest->packageId,$uniqueId)'  class='$class;'>$button</a>";
							    }
								if($package_name){
									$packName=' , Package : '.$package_name;
								}else{
									$packName="";}
                           echo "<div class='thumb_panel'>
										<div class='thumb_img pull-left'><img class='img-responsive' src=$imageUrl ></div>
										<div class='thumb_txt'>
										<p>$i.$practicalTest->testName  $packName<br><span>$messege <br> $expireMessege</span></p>
										$displayButton
										</div>
										<div class='clearfix'></div>
									</div>
						  ";$i++;
			}}else {
				if($Ttype==MAIN_TEST){
					echo "<span class='text-center' style='color:red'>".NO_PACKAGES_TESTS."</span>";
				}else{
				echo "<span class='text-center' style='color:red'>".NO_PRACTISE_TESTS_EXISTS."</span>";}
			}
			die();*/
		}
	}
	
	/*function getOptedTestsByType()
	{
		if($this->input->is_ajax_request())
		{
			$Ttype = $this->input->post('Ttype');
			$limit = DEFAULT_LIMIT;
			$optedTests = $this->Testsmodel->getOptedTests($Ttype,$limit);
			if($optedTests  && count($optedTests)>0){
				$o=1;
                   foreach($optedTests as $optedTest){
					   
							// $testResults=$optedTest->testResults;
							 $uniqueId=$optedTest->userPackageID;
							 $attempts=$optedTest->attempts;
							 $time=$optedTest->createdTime;
							 $d= date('d - M -Y', strtotime($time));
							 $testDetails=$optedTest->testDetails;
							 $testName=$testDetails->testName;
							 $testId=$testDetails->testId;
							 $count=$optedTest->completedTestCount;
							 $testType=$optedTest->type;
					         $packageId=$optedTest->packageId;
							  $packageName=$optedTest->packageName;
							 if($packageName){
							 $pack=', Package : ' .$packageName;
							 }else{$pack="";}
							$btn="<a type='button' id='review' type='button' href='#reportsPopup' data-toggle='modal' onclick='getAttemptsOfTest($testId,$uniqueId)'  class='btn register_btn blue_btn mt25' >view reports</a>";
			                $imageUrl =  IMAGES_URL.'thumb_img.jpg';
			                 $testUrl =   VIEW_RECORD_URL.'?testId='.$testId.'&attempts='.$optedTest->attempts.'&type='.$testType.'&package='.$packageId.'&userPackageId='.$uniqueId;
						   echo "<div class='thumb_panel'>
                        	<div class='thumb_img pull-left'><img class='img-responsive' src= $imageUrl></div>
                            <div class='thumb_txt'>
                            <p>$o.$testName $pack<br>
                            </p>
							 $btn
                            </div>
                           <div class='clearfix'></div>
                          </div>
						  ";
						/*echo "<div class='thumb_panel'>
                        	<div class='thumb_img pull-left'><img class='img-responsive' src= $imageUrl></div>
                            <div class='thumb_txt'>
                            <p>$testName ,Attempts:$optedTest->attempts<br>
                            <span><i class='fa fa-clock-o' aria-hidden='true'></i> $d</span>
                            </p>
							<a type='button' href='$testUrl' class='btn register_btn blue_btn mt25'>view report</a>
                            </div>
                            <div class='test_tag'> $optedTest->attempts/$count</div>
                            <div class='clearfix'></div>
                        </div>
						  ";*/
				/*	$o++;	  
			} }else {
				
				echo "<span class='text-center' style='color:red'>Your not took any test yet. Try to take a test.</span>";
			}
			die();
		}
	}*/
	function getOptedTestsByType()
	{ 
		if($this->input->is_ajax_request())
		{
			$Ttype = $this->input->post('Ttype');
			$offset = $this->input->post('offset');
			$limit = 2;
			$optedTests = $this->Testsmodel->getOptedTests($Ttype,$limit,$offset);
			if($optedTests  && count($optedTests)>0){
				$o=1;
                   foreach($optedTests as $optedTest){
					         $uniqueId=$optedTest->userPackageID;
							 $attempts=$optedTest->attempts;
							 $time=$optedTest->createdTime;
							 $d= date('d - M -Y', strtotime($time));
							 $testDetails=$optedTest->testDetails;
							 $testName=$testDetails->testName;
							 $testId=$testDetails->testId;
							 $count=$optedTest->completedTestCount;
							 $testType=$optedTest->type;
					         $packageId=$optedTest->packageId;
							  $packageName=$optedTest->packageName;
							 if($packageName){
							 $pack=', Package : ' .$packageName;
							 }else{$pack="";}
							$btn="<center><a type='button' href='#reportsPopup' data-toggle='modal' onclick='getAttemptsOfTest($testId,$uniqueId)'  class='btn register_btn blue_btn mr0 mt5'>View Report</a></center>";
						    $imageUrl =  IMAGES_URL.'opted_test_img1.jpg';
			                 $testUrl =   VIEW_RECORD_URL.'?testId='.$testId.'&attempts='.$optedTest->attempts.'&type='.$testType.'&package='.$packageId.'&userPackageId='.$uniqueId;
						   echo "
						   <div class='col-md-6'>
						   <div class='opted_test_module'>
						   <div class='opted_test_img' style='background:url($imageUrl) no-repeat top center;'></div>
						<div class='opted_test_txt'>
						<center><p>$testName $pack<br></center>
                        	</p>$btn
                            </div></div></div>
                           ";
						
					$o++;	  
			} 
			?>
			 <p class='load_more clearfix'><a href='<?php echo VIEWMORE.'?type='.$Ttype; ?>' ><i class='fa fa-plus-square-o' aria-hidden='true'></i> Load more..</a></p>
						 
						 
			
			<?php
			
			}else {
				
				echo "<center><div  class='text-center' style='color:red'>Please take a test to view records.</div></center>";
			}
			die();
		}
	}
//on 21 st march
	function getPurchaseItems()
	{    
	     $userId=$this->session->userdata('user_id');
	     $loginToken =$this->session->userdata('loginToken');
		 $notvalidToken=$this->checkTokenValid($loginToken);
         if(!$userId || $notvalidToken){
                redirect(LOGOUT_URL,'refresh');
        }
			
		if($this->input->is_ajax_request())
		{
			$item = $this->input->post('item');
			if($item == "package"){
				$packages=$this->Packagemodel->getPackages();;
				
				 if($packages && count($packages) >0){
                          foreach($packages as $package){
							   $imageUrl =  IMAGES_URL.'package_tittle_icon.png';
							   $imageUrl2 =  IMAGES_URL.'package.png';
							   $packageName=$package->package_name;
							   $id=$package->package_id;
							   $amount=$package->package_amount;
				         // debug($package->package_name);
                  		echo "<div class='package_panel'>
							<h1><img src='$imageUrl' style='margin-right:10px;'> $packageName</h1>
							<div class='price_tag'>$amount</div>
							<div class='package_panel_img pull-left'><img class='img-responsive' src='$imageUrl2'/></div>
							<div class='package_panel_btn'>
							<a type='button' href='#' class='btn register_btn blue_btn mt5' onclick='popWindow($id)''>View Syllabus</a><br/>
							<input type='hidden' name='quantity' id='quantity' value='1' size='2' style='width:36px' />

							<a href='JavaScript:Void(0);' type='button'   onclick='addtoCart($id,1,$amount)' value='Buy Now' class='btn login_btn green_btn mt5'><i class='fa fa-shopping-cart' aria-hidden='true'></i>  Add to cart </a>
							</div>
							<div class='clearfix'></div>
						</div>
						 ";
						 
                  		
				     }die();
			    } else { 
					      echo "<center><div  class='text-center' style='color:red'>".NO_PACKAGES_AVAILABLE."</div></center>";die();
					   } 
				
			}else{
				$PurchaseTests= $this->Testsmodel->getMainTests(MAIN_TEST);
				   if($PurchaseTests && count($PurchaseTests) >0){ 
				   $imageUrl =  IMAGES_URL.'package_tittle_icon.png';
				   $imageUrl2 =  IMAGES_URL.'package.png';
                       foreach($PurchaseTests as $test){
						   $amount=$test->testAmount;
						   $id=$test->testId;
			      	       echo "<div class='package_panel'>
						   <h1><img src='$imageUrl' style='margin-right:10px;' > $test->testName</h1>
						   <div class='price_tag'>$amount</div>
										<div class='package_panel_img pull-left'><img class='img-responsive' src='$imageUrl2'/></div>
										<div class='package_panel_btn'>
										 <input type='hidden' name='quantity' id='quantity' value='1' size='2' />
										<a type='button' href='JavaScript:Void(0);' onclick='addtoCart($id,2,$amount)' class='btn login_btn green_btn mt5'><i class='fa fa-shopping-cart' aria-hidden='true'></i>  Add to cart</a>
							</div>
							<div class='clearfix'></div>
						</div> ";
						}die();
						 
					}else { 
					     echo "<center><div  class='text-center' style='color:red'>".NO_MAIN_TESTS_EXISTS."</div></center>";die();
					   } 
				//debug($PurchaseTests);
			}
			
		}
	}
	
	function addToCart(){
		 $resellerId=$this->session->userdata('reseller_id');
		 $userId=$this->session->userdata('user_id');
		 $userdetails=$this->getSingleRecord(TBL_USERS,array('userId'=>$userId),$select="*");
		 $resellerId=$userdetails->resellerId;
	     $loginToken =$this->session->userdata('loginToken');
		 $notvalidToken=$this->checkTokenValid($loginToken);
         if(!$userId || $notvalidToken){
                redirect(LOGOUT_URL,'refresh');
            }
			
		if($this->input->is_ajax_request())
		{
			$amount=0;
			$item = $this->input->post('item');
			$id = $this->input->post('id');
			// $amount = $this->input->post('amount');
			 $isPromoterUser=$this->session->userdata('isPromoterUser'); 
			 if($item == 1){
			    if($isPromoterUser == 1){
				  $packageamount=$this->getSingleRecord(TBL_PROMOTER_PACKAGES,$where=array('p_package_id'=>$id,'p_promoter_id'=>$resellerId),'p_sell_amount as package_amount'); 
                }else{ 
				  $packageamount=$this->getSingleRecord(TBL_PACKAGES,$where=array('package_id'=>$id),'package_amount'); 
                }
				$amount=$packageamount->package_amount;	
			 }else{
				       if($isPromoterUser == 1){
					       $testAmount=$this->getSingleRecord(TBL_PROMOTER_TESTS,$where=array('p_test_id'=>$id,'p_t_promoter_id'=>$resellerId),'p_t_sell_amount as testAmount'); 
						}else{ 
					      $testAmount=$this->getSingleRecord(TBL_TESTS,$where=array('testId'=>$id),'testAmount'); 
                        }
					    $amount=$testAmount->testAmount;
			 }
			
		    if($this->input->post('quantity')){
				$quantity = $this->input->post('quantity');
			}else{
				$quantity = 1;
			}
			if($_POST){
				$where=array('itemType'=>$item,'itemId'=>$id,'userId'=>$userId,'isActive'=>1,'isDeleted'=>0);
				$checkisItemSelected=$this->getSingleRecord(TBL_CART,$where,'*');
				    $updata['itemType']=$item;
					$updata['userId']=$userId;
					$updata['itemId']=$id;
				if($checkisItemSelected && count($checkisItemSelected)>0){
					echo 'no';die();
					/*$qaunt=$checkisItemSelected->quantity;
					$quantity=$qaunt+$quantity;
					$where=array('Id'=>$checkisItemSelected->Id);
					$updata['updatedTime']=date("Y-m-d H:i:s");
					$updata['quantity']=$quantity;
					$updata['amount']=$amount*($quantity);*/
				}else{
					$where=array();
					$updata['createdTime']=date("Y-m-d H:i:s");
					$updata['quantity']=$quantity;
					$updata['amount']=$amount*($quantity);
				}
				$results=$this->insertOrUpdate(TBL_CART,$where,$updata);
				if($results){
					$cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
					echo $cartCount;die();
				}
		    }
			
			
			
		}
	}
	
	 function deleteCartItems(){
	        $id = $this->input->post('id');
			$where = array('Id'=>$id);
			$data['isDeleted'] = 1;
			$data['isActive'] = 0;
			$success = $this->insertOrUpdate(TBL_CART,$where,$data);
			if($success)
			{
				echo 1;
			}
			else
			{
				echo 0;
			}
			die();
		}
	//new
	
	 function TestsListByPackage()
     {
	  $packageId = trim($this->input->post('packageId'));
	  $tests = $this->Testsmodel->getTestsNameByPackage($packageId);
	   $data['tests']= $tests;
	    $this->load->view('edit_TestsPackages_ajax',$data);
	   
  } 
 /* function getPackagesOrTestsBySearch()
	{
		
		$txt = trim($this->input->post('txt'));
		$filter = trim($this->input->post('filter'));
		$packageIdlist = $this->input->post('listOfTests');
		 $checked ="";
		if($filter == 'Package')
		{
			$packages = $this->Testsmodel->getPackgesByPackageName($txt);
			if($packages && count($packages)>0){
				foreach($packages as $package){
					$class = "label label-info";
					if (!empty($packageIdlist)) {
						$checked = in_array($package->package_id,$packageIdlist) ? ' checked="checked"' : '';
					  }
				      echo "<span class='".$class."'><input type='checkbox' name='packages[]' value=".$package->package_id." ".$checked."  onclick='getPackageAmount(\"$package->package_id\",\"$package->package_name\",$package->package_amount, this.checked ? 1 : 0)'>
						   <a href='#TestsByPackagePopUp' data-toggle='modal' onclick='getTestsByPackageId($package->package_id)' id='package'><font  color='white'>".$package->package_name." </font> </a>
								 </span>
							 ";
			}
		  }else{
			  echo "No packages related to your search";
		  }
			
		}
		if($filter == 'Test'){
			$testsList = $this->input->post('listOfTests');
			$tests = $this->Testsmodel->getTestsBytestName($txt);
			if($tests && count($tests)>0){
			  foreach($tests as $test){
				  if (!empty($testsList)) {
                      $checked = in_array($test->testId,$testsList) ? ' checked="checked"' : '';	
                    }
				    
					echo"<span class='label label-warning'><input type='checkbox' name='tests[]' value=".$test->testId." ".$checked." onclick='getTestAmount(\"$test->testId\",\"$test->testName\", $test->testAmount, this.checked ? 1 : 0);'> <a href='javascript:void(0)'>".$test->testName." </a></span>
					";
			  }
			}else{
			  echo "No tests related to your search";
		  }
		}
		die();
		
		
	}*/
	function getPackagesOrTestsBySearch()
	{
		
		$txt = trim($this->input->post('txt'));
		$resellerid = trim($this->input->post('id'));
		$filter = trim($this->input->post('filter'));
		$userId = trim($this->input->post('userId'));
		$where=array('userId'=>$userId);
		$udetails=$this->getSingleRecord(TBL_USERS,$where,"*");
		$testIds=$udetails->testIds;
		$packageIds=$udetails->packageIds;
		$tids=explode(',',$testIds);
		$pIds=explode(',',$packageIds); 
		$packageIdlist = $this->input->post('listOfTests');
		 $checked ="";
		if($filter == 'Package')
		{
			$packages = $this->Testsmodel->getPackgesByPackageName($txt,$resellerid);
			if($packages && count($packages)>0){
				foreach($packages as $package){
					$class = "checkbox col-sm-2 col-xs-4";
					if (!empty($pIds)) {
						$checked = in_array($package->package_id,$pIds) ? ' checked="checked"' : '';
					  }
				      echo "<div class='".$class."'><label><input type='checkbox' ".$checked." name='packages[]' value=".$package->package_id." ".$checked."  onclick='getPackageAmount(\"$package->package_id\",\"$package->package_name\",$package->package_amount, this.checked ? 1 : 0)'>
						   <a href='#TestsByPackagePopUp' data-toggle='modal' onclick='getTestsByPackageId($package->package_id)' id='package'><font  color='white'>".$package->package_name." </font> </a>
								 </label></div>
							 ";
			}
		  }else{
			  echo "<p class='alert alert-danger'>No packages related to your search.</p>";
		  }
			
		}
		if($filter == 'Test'){
			$testsList = $this->input->post('listOfTests');
			$tests = $this->Testsmodel->getTestsBytestName($txt,$resellerid);
			if($tests && count($tests)>0){
			  foreach($tests as $test){
				  if (!empty($tids)) {
                      $checked = in_array($test->testId,$tids) ? ' checked="checked"' : '';	
                    }
				    
					echo"<div class='checkbox col-sm-2 col-xs-4'><label><input type='checkbox' ".$checked." name='tests[]' value=".$test->testId." ".$checked." onclick='getTestAmount(\"$test->testId\",\"$test->testName\", $test->testAmount, this.checked ? 1 : 0);'> <a href='javascript:void(0)'>".$test->testName." </a></label></div>
					";
			  }
			}else{
			  echo "<p class='alert alert-danger'>No tests related to your search.</p>";
		  }
		}
		die();
		
		
	}
function addUserPackagesorTests(){
	
	$userId=$this->input->post('userId');
	$testId=$this->input->post('testId');
	$packageId=$this->input->post('packageId');
	$totalCost=$this->input->post('totalCost');
	$this->session->set_userdata('cost',$totalCost);
	$this->session->set_userdata('testId',$testId);
	$this->session->set_userdata('packageId',$packageId);
	$updata['testIds']=$testId;
	$updata['packageIds']=$packageId;
	$updata['updatedTime']=date("Y-m-d H:i:s");
	$where=array('userId'=>$userId);
	$res=$this->insertOrUpdate(TBL_USERS,$where,$updata);
	if($res){
		
		$updata['testId']=$testId;
	    $updata['packageId']=$packageId;
	    $updata['createdTime']=date("Y-m-d H:i:s");
	    $updata['amount']=$totalCost;
	    $updata['userId']=$userId;
	    $updata['isActive']=0;
		$where=array();
		$res=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
	}
	echo $res;die();
	
	
	
}	
	
}
?>