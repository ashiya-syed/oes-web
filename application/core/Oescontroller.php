<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oescontroller extends CI_Controller {

	function __construct()
    {

	
        // Call the Model constructor
        parent::__construct();
	
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('email');
	
        $this->load->library('session');
		$this->load->helper('url');
		//$this->load->helper('captcha');
    }
	
	
	/**
	 * Method to log the exception
	 *
	 */
	protected function logExceptionMessage($exception)
	{
		$errorMessage = 'Exception of type \'' . get_class($exception) . 
					'\' occurred with message: ' . $exception->getMessage() . 
					' in file ' . $exception->getFile() . 
					' at line ' . $exception->getLine();
				 
					// Add backtrace:					
					$errorMessage .= "\r\n Backtrace: \r\n";
					$errorMessage .= $exception->getTraceAsString();
					log_message('error',$errorMessage,TRUE);
	}
		
		
	function makeServiceCall($service_url, $data = array(), $method = 'POST')
	{		
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => $method,
				'content' => http_build_query($data),
			),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($service_url, false, $context);	
		//var_dump($result);die();
		$result = utf8_encode($result);
		
		$result = str_replace('ï»¿','',$result);
		$result = json_decode($result);
		return $result;
	}	
	

   function isValidSession($sessionId,$Method)
   {
	   return true;
	   
   }   
   
   //custom 
	function getAllRecords($table,$where,$select="*")
	{
		$this->db->select($select);
		if($where){
		$this->db->where($where);
		}
        $query = $this->db->get($table); 
		$results = $query->result();
	    return $results;
	}
	
	function insertOrUpdate($table,$where=array(),$data)
	{
		if($where){
			$this->db->where($where);
			$query=$this->db->update($table,$data);
			//echo $this->db->last_query();die();
			if (!$query)
			{    
		         return false;
				
			}else{
				return true;
			}
			
		}else{
		
			if(!empty($data))
			{
				$this->db->insert($table,$data);
				return $this->db->insert_id();
			}
		}	
			
			return false;
	}
	
	function getSingleRecord($table,$where,$select="*")
	{
		$this->db->select($select);
		$this->db->where($where);
        $query = $this->db->get($table);
		if (!$query)
			{    
		        $results ="";
			}else{
				$results = $query->row();
	            return $results;
			}
		
	}
	function checkTokenValid($token)
	{
		$this->db->select('*');
		$this->db->where('token',$token);
		$this->db->where('isActive',1);
		$this->db->where('isDeleted',0);
        $query = $this->db->get(TBL_USER_TRACK); 
		$results = $query->row_array();
		if($results){
			return false;
		}else{
			return true;
		}
	   
	}
}

