<?php 
/**
 * This class contains the information about serivices data
 *
 * 
 *
 * @copyright  
 * @license   
 * @version    
 * @link      
 * @since      
 */
class Healthmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
		
        parent::__construct();
		
    }
    
	/**
	 * Method to throw the exception
	 *
	 * @param ($query)  database query resource object  
	 */ 
    protected function throwException($query='')
    {	
			
			  // if query returns null
			  $errorMessage = $this->db->_error_message();
			  $errorNumber = $this->db->_error_number();
			  
			  if ($errorMessage) 
			  {
					throw new Exception($errorMessage);
			  }
			
    }
	
	
}

?>