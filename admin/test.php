<?php
 if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
     echo ' [OK] PHP version is newer than 5.4: '.phpversion();
 } else {
     echo ' [ERROR] Your PHP version is too old for CKFinder 3.x.';
 }
 
 if (!function_exists('gd_info')) {
     echo ' [ERROR] GD extension is NOT enabled.';
 } else {
     echo ' [OK] GD extension is enabled.';
 }
 
 if (!function_exists('finfo_file')) {
     echo ' [ERROR] Fileinfo extension is NOT enabled.';
 } else {
     echo ' [OK] Fileinfo extension is enabled.';
 }
 if (!extension_loaded('php_gd2')) {
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        dl('php_gd2.dll');
    } else {
        dl('php_gd2.so');
    }
}
 ?>