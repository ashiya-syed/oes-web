function add_country(){
	allButtonsDisabled();
	var country_name=$('#country_name').val();
	var country_code=$('#country_code').val();
	var dataString = 'country_name=' + country_name + '&country_code=' + country_code;
	if(country_name=="" || country_code=="")
	{
		allButtonsEnabled();
		$('#COUNTRY_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/addCountry',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#COUNTRY_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									allButtonsEnabled();
									$('#COUNTRY_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}


function country_inactivate(countryID,id){ 
   
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/countryActiveNess', 
				    data: {countryID:countryID,id:id},
                            success: function(response){
						    $('#activecountry_'+countryID).html(response);
                    },
						error: function(xhr, statusText, err){
                              console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}
function country_active(countryID,id){

            $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/countryActiveNess',  
				    data: {countryID:countryID,id:id},
                            success: function(response){
						    $('#activecountry_'+countryID).html(response);
                    },
						error: function(xhr, statusText, err){
                              console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}


	function update_country(countryID)
	{
	   var country_name=$('#updatecountry_name').val();
	   var country_code=$('#updatecountry_code').val();
	   if(country_name=="" || country_code=="")
	   {
		 $('#UPDATE_COUNTRY_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		 return false;
	  }else{
	      $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/updateCountry',  
				    data: {countryID:countryID,country_name:country_name,country_code:country_code},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_COUNTRY_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_COUNTRY_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                              console.log("Error:" + xhr.status);  
						}
						
                });
	  }		
         return false;
	
}

function edit_country(countryID)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/editCountry',  
				    data: {countryID:countryID},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editcountry").modal('show');
						         $('#_edit_country_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}

function deleteCountry(countryID)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Master/deleteCountry', 
							data: {countryID:countryID},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}


/***********
Add State
************/
function add_state(){
	
	var state_name=$('#state_name').val();
	var state_code=$('#state_code').val();
	var countryID = $("#countryID option:selected").val();
	var dataString = 'state_name=' + state_name + '&state_code=' + state_code + '&countryID=' + countryID;
	if(state_name=="" || state_code=="" || countryID == "")
	{
		$('#STATE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/addState',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#STATE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#STATE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}

function state_inactivate(stateID,id){ 
   
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/stateActiveNess', 
				    data: {stateID:stateID,id:id},
                            success: function(response){
						    $('#activestate_'+stateID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}
function state_active(stateID,id){

            $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/stateActiveNess',  
				    data: {stateID:stateID,id:id},
                            success: function(response){
						    $('#activestate_'+stateID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}

function deleteState(stateID)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Master/deleteState', 
							data: {stateID:stateID},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}

function edit_state(stateID)
{  
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/editState',  
				    data: {stateID:stateID},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editstate").modal('show');
						         $('#_edit_state_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function update_state(stateID)
{
	   var state_name=$('#updatestate_name').val();
	   var state_code=$('#updatestate_code').val();
	   var countryID = $("#countryId option:selected").val();
	   if(state_name=="" || state_code=="" || countryID == "")
		{
			$('#UPDATE_STATE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
			return false;
		}else {
	          $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/updateState',  
				    data: {stateID:stateID,state_name:state_name,state_code:state_code,countryID:countryID},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_STATE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_STATE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
		}	
         return false;
	
}

function getSates(countryID)
{ 
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/StatesByCountry',  
				    data: {countryID:countryID},
                            success: function(response){
								
							   $('#stateID').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function getCitys(stateID)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/citiesByState',  
				    data: {stateID:stateID},
                            success: function(response){
								
							   $('#cityID').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

/***********
Add City
************/
function add_city(){
	
	var city_name=$('#city_name').val();
	var city_code=$('#city_code').val();
	var countryID = $("#countryID option:selected").val();
	var stateID = $("#stateID option:selected").val();
	var cityID = $("#cityID option:selected").val();
	var dataString = 'city_name=' + city_name + '&city_code=' + city_code + '&countryID=' + countryID + '&stateID=' + stateID + '&cityID=' + cityID;
	if(city_name=="" || city_code=="" || countryID == "" || stateID == "" || cityID == "")
	{
		$('#CITY_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/addCity',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#CITY_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#CITY_MESSAGE').html('<span class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}
function add_area(){
	
	var area_name=$('#area_name').val();
	var area_code=$('#area_code').val();
	var countryID = $("#countryID option:selected").val();
	var stateID = $("#stateID option:selected").val();
	var cityID = $("#cityID option:selected").val();
	var dataString = 'area_name=' + area_name + '&area_code=' + area_code + '&countryID=' + countryID + '&stateID=' + stateID + '&cityID=' + cityID;
	if(area_name=="" || area_code=="" || countryID == "" || stateID == "" || cityID == "")
	{
		$('#AREA_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/addArea',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{  
							       $('#AREA_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									alert('noo');
									$('#AREA_MESSAGE').html('<span class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}
function area_inactivate(ID,id){ 
              $.ajax({
				  
				    type: "POST",
                    url:baseUrl+'/Webservices/Master/areaActiveNess', 
				    data: {ID:ID,id:id},
                            success: function(response){
						    $('#activeArea_'+ID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}
function area_active(ID,id){
	
	         $.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/areaActiveNess',  
				    data: {ID:ID,id:id},
                            success: function(response){
						    $('#activeArea_'+ID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}
function edit_area(ID)
{  
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/editArea',  
				    data: {ID:ID},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editarea").modal('show');
						         $('#_edit_area_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function update_area(ID)
{
	   var area_name=$('#updatearea_name').val();
	   var area_code=$('#updatearea_code').val();
	   var stateID = $("#stateId option:selected").val();
	   var cityID = $("#cityId option:selected").val();
	   var countryID = $("#countryId option:selected").val();
	   if(area_name=="" || area_code=="" || stateID == ""|| cityID == ""|| countryID == "")
		{
			$('#UPDATE_AREA_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
			return false;
		}else {
	          $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/updateArea',  
				    data: {ID:ID,area_name:area_name,area_code:area_code,cityID:cityID,stateID:stateID,countryID:countryID},
					
				    cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_AREA_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_AREA_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
		}	
         return false;
	
}
function deleteArea(ID)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Master/deleteArea', 
							data: {ID:ID},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}

/***********
Add City
************/
/*function add_city(){
    var city_name=$('#city_name').val();
	var city_code=$('#city_code').val();
	var stateID = $("#stateID option:selected").val();
	var countryID = $("#countryID option:selected").val();
    var dataString = 'city_name=' + city_name + '&city_code=' + city_code + '&countryID=' + countryID + '&stateID=' + stateID;
	if(city_name=="" || city_code=="" || countryID == "" || stateID == "" )
	{
		$('#CITY_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/addCity',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#CITY_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#CITY_MESSAGE').html('<span class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}*/
function edit_city(cityID)
{  
$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/editCity',  
				    data: {cityID:cityID},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editcity").modal('show');
						         $('#_edit_city_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function update_city(cityID)
{
	  var city_name=$('#updatecity_name').val();
	   var city_code=$('#updatecity_code').val();
	   var stateID = $("#stateId option:selected").val();
	   var countryID = $("#countryId option:selected").val();
	   if(city_name=="" || city_code=="" || stateID == ""  || countryID == "" )
		{
			$('#UPDATE_CITY_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
			return false;
		}else {
	          $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/updateCity',  
				    data: {cityID:cityID,city_name:city_name,city_code:city_code,stateID:stateID,countryID:countryID},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_CITY_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_CITY_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
		}	
         return false;
	
}
function city_inactivate(cityID,id){ 
   
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/cityActiveNess', 
				    data: {cityID:cityID,id:id},
                            success: function(response){
						    $('#activecity_'+cityID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}
function city_active(cityID,id){

            $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/cityActiveNess',  
				    data: {cityID:cityID,id:id},
                            success: function(response){
						    $('#activecity_'+cityID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}

function deleteCity(cityID)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Master/deleteCity', 
							data: {cityID:cityID},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}

/***********
Add Agents
************/
function add_agent(){
	var agent_name=$('#agent_name').val();
	var agent_location=$('#agent_location').val();
	var serviceID = $("#serviceID option:selected").val();
	var emailAdress=$('#emailAdress').val();
	var phoneNumber=$('#phoneNumber').val();
	var password=$('#password').val();
	var cpassword=$('#cpassword').val();
	var verticalsID = $("#verticals option:selected").val();
	
	var dataString = 'agent_name=' + agent_name + '&agent_location=' + agent_location + '&serviceID=' + serviceID + '&emailAdress=' + emailAdress + '&phoneNumber=' + phoneNumber + '&password=' + password + '&cpassword=' + cpassword + '&verticalsID=' + verticalsID;
	if(agent_name=="" || agent_location=="" || serviceID=="" || emailAdress=="" || phoneNumber=="" || password=="" || cpassword=="" || verticalsID == "")
	{
		$('#AGENTS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Agents/addAgents',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#AGENTS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#AGENTS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}
function edit_agent(agentID)
{ 
$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Agents/editAgent',  
				    data: {agentID:agentID},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editagent").modal('show');
						         $('#_edit_agent_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function update_agent(agentID)
{      
	   var agent_name=$('#updateagent_name').val();
	   var agent_location=$('#updateagent_location').val();
	   var serviceId = $("#serviceId option:selected").val();
	   var emailAddress=$('#update_emailAddress').val();
	   var phoneNumber=$('#update_phoneNumber').val();
	   var verticalsId = $("#verticalsId option:selected").val();
	    if(agent_name=="" || agent_location=="" || serviceId == "" || emailAddress=="" || phoneNumber=="" || verticalsId =="")
		{
			$('#UPDATE_AGENTS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
			return false;
		}else {
	          $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Agents/updateAgent',  
				    data: {agentID:agentID,agent_name:agent_name,agent_location:agent_location,serviceId:serviceId,phoneNumber:phoneNumber,emailAddress:emailAddress},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								if(response.status==1)
								{
									$('#UPDATE_AGENTS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_AGENTS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
		}	
         return false;
	
}
function deleteAgent(agentID)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Agents/deleteAgent', 
							data: {agentID:agentID},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}

function agent_inactivate(agentID,id){ 
   
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Agents/agentActiveNess', 
				    data: {agentID:agentID,id:id},
                            success: function(response){
								
						    $('#activeAgents_'+agentID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}

function agent_active(agentID,id){
         
            $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Agents/agentActiveNess',  
				    data: {agentID:agentID,id:id},
                            success: function(response){
								
						    $('#activeAgents_'+agentID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}

//new 

function edit_agent_password(agentID)
{
	
	$.ajax({
                    type: "post",
					 url:baseUrl+'/Webservices/Agents/editAgentPassword',  
                    data: {agentID:agentID},
                            success: function(response){
								 
								 $("#Update_Password").modal('show');
						         $('#_update_password_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
function Update_agent_Password(agentID){
	var update_password=$('#update_password').val();
	var new_password=$('#update_npassword').val();
	var confirm_password=$('#update_cpassword').val();
	var dataString = 'update_password=' + update_password + '&new_password=' + new_password + '&confirm_password=' + confirm_password +  '&agentID=' + agentID;
	
	if(update_password=="" || new_password=="" || confirm_password=="")
	{
		$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Agents/updateAgentPassword',  					
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}
function update_agent_profile(userId)
	{
		
	   var name=$('#name').val();
	   var emailAdress=$('#emailAdress').val();
	  var number=$('#number').val();
	  
	   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Agents/updateProfile',  
				    data: {name:name,emailAdress:emailAdress,number:number,userId:userId},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#PROFILE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#PROFILE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
/***********
Add Admins
************/
function add_admin()
{
	var serviceID = $("#serviceID option:selected").val();
	var name=$('#name').val();
	var emailAdress=$('#emailAdress').val();
	var phoneNumber=$('#phoneNumber').val();
	var password=$('#password').val();
	var cpassword=$('#cpassword').val();
	var adminLocation=$('#admin_location').val();
	var dataString = 'name=' + name + '&emailAdress=' + emailAdress + '&phoneNumber=' + phoneNumber + '&password=' + password + '&cpassword=' + cpassword + '&serviceID=' + serviceID + '&adminLocation=' + adminLocation;
	if(name=="" || emailAdress=="" || phoneNumber=="" || password=="" || cpassword=="" || serviceID=="" || adminLocation=="")
	{
		$('#ADMINS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Admins/addAdmin',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#ADMINS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#ADMINS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	
}

function deleteAdmin(adminId)
{
	var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Admins/deleteAdmin', 
							data: {adminId:adminId},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}

function admin_inactivate(adminId,id){ 
   
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Admins/adminActiveNess', 
				    data: {adminId:adminId,id:id},
                            success: function(response){
								
						    $('#activeAdmin_'+adminId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}

function admin_active(adminId,id){
         
            $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Admins/adminActiveNess',  
				    data: {adminId:adminId,id:id},
                            success: function(response){
								
						    $('#activeAdmin_'+adminId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}

function edit_admin(adminId)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Admins/editAdmin',  
				    data: {adminId:adminId},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editadmin").modal('show');
						         $('#_edit_admin_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function update_admin(adminId)
{
	
	var serviceID = $("#serviceId option:selected").val();
	var name=$('#update_name').val();
	var emailAddress=$('#update_emailAddress').val();
	var phoneNumber=$('#update_phoneNumber').val();
	var location=$('#update_location').val();
	var dataString = 'name=' + name + '&emailAddress=' + emailAddress + '&phoneNumber=' + phoneNumber + '&serviceID=' + serviceID + '&adminId=' + adminId + '&location=' + location;
	
	if(name=="" || emailAddress=="" || phoneNumber=="" || serviceID=="" || location == "" )
	{
		$('#UPDATE_ADMINS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Admins/updateAdmin',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#UPDATE_ADMINS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_ADMINS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}
function edit_admin_password(agentId)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Admins/passwordUpdatePopup',  
				    data: {agentId:agentId},
                            success: function(response){
								 
								 $("#Update_Password").modal('show');
						         $('#_update_password_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function Update_admin_Password(adminId){
	var update_password=$('#update_password').val();
	var new_password=$('#update_npassword').val();
	var confirm_password=$('#update_cpassword').val();
	var dataString = 'update_password=' + update_password + '&new_password=' + new_password + '&confirm_password=' + confirm_password +  '&adminId=' + adminId;
	
	if(update_password=="" || new_password=="" || confirm_password=="")
	{
		$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Admins/updatePassword',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}

function update_admin_profile(userId)
	{
		
	   var name=$('#name').val();
	   var emailAdress=$('#emailAdress').val();
	  var number=$('#number').val();
	  
	   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Admins/updateProfile',  
				    data: {name:name,emailAdress:emailAdress,number:number,userId:userId},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#PROFILE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#PROFILE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}

/***********
Add Tests
************/

function add_test(){
	
	var partnerId = $("#partnerId option:selected").val();
	if(logginRoleId == partnerRoleID){
		partnerId=logginuserId;
	}
	
    var test_name=$('#test_name').val();
	var test_cost=$('#test_cost').val();
	var discount_cost=$('#discount_cost').val();
	var add_parameter = $("input[id='parameter']") .map(function(){return $(this).val();}).get();
	var count=add_parameter.length;
	var add_value = $("input[id='value']") .map(function(){return $(this).val();}).get();
	var add_measurment = $("input[id='measurment']") .map(function(){return $(this).val();}).get();
	var add_remarks = $("input[id='remarks']") .map(function(){return $(this).val();}).get();
	var dataString = 'test_name=' + test_name + '&test_cost=' + test_cost+ '&add_parameter=' + add_parameter+ '&add_value=' + add_value+ '&add_measurment=' + add_measurment+ '&add_remarks=' + add_remarks+ '&count=' + count + '&discount_cost=' + discount_cost+ '&partnerId=' + partnerId;
	if(test_name=="" || test_cost=="") 
	{
		$('#TEST_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/addTest',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									
									$('#TEST_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#TEST_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}

function test_inactivate(testId,id){ 
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/testActiveNess', 
				    data: {testId:testId,id:id},
                            success: function(response){
						    $('#activetest_'+testId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}
function test_active(testId,id){
  $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/testActiveNess',  
				    data: {testId:testId,id:id},
                            success: function(response){
						    $('#activetest_'+testId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}
function edit_test(testId)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/editTest',  
				    data: {testId:testId},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#edittest").modal('show');
						         $('#_edit_test_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function update_test(testId)
	{
		
	   var test_name=$('#updatetest_name').val();
	   var test_cost=$('#updatetest_cost').val();
	   var updatediscount_cost=$('#updatediscount_cost').val();
	   if(test_name=="" || test_cost=="")
	{
		$('#UPDATE_TEST_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/updateTest',  
				    data: {testId:testId,test_name:test_name,test_cost:test_cost,discount_cost:updatediscount_cost},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_TEST_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_TEST_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}
         return false;
		 
	
}
function deleteTest(testId)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Master/deleteTest', 
							data: {testId:testId},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}


/**********
Add Params For Tests 
********/

function add_params(testId)
{
	$.ajax({
		
                    type: "post",
                    url:baseUrl+'/Webservices/Master/addParams',  
				    data: {testId:testId},
                            success: function(response){
								 
								  $("#add_params").modal('show');
						          $('#_add_params_div').html(response);
								  $("#add1").hide();
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
 //today
function add_Parameter(testId){
	
	var add_parameter = $("input[id='add_parameter']") .map(function(){return $(this).val()}).get();
	var count=add_parameter.length;
	var add_value = $("input[id='add_value']") .map(function(){return $(this).val();}).get();
	var add_measurment = $("input[id='add_measurment']") .map(function(){return $(this).val();}).get();
	var add_remarks = $("input[id='add_remarks']") .map(function(){return $(this).val();}).get();
	var dataString = 'add_parameter=' + add_parameter + '&add_value=' + add_value + '&add_measurment=' + add_measurment + '&add_remarks=' + add_remarks +  '&testId=' + testId+  '&count=' + count;
	if(add_parameter=="" || add_value=="" || add_measurment=="")
	{
		$('#ADD_PARAMETERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/addParameter',
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#ADD_PARAMETERS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#ADD_PARAMETERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}
function edit_params(testId)
{

	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/editParameter',
				    data: {testId:testId},
                            success: function(response){
								 
								 $("#editparam").modal('show');
						         $('#_edit_param_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
 
function update_Parameter(testId)
{
		
	    var id = $("input[id='id']") .map(function(){return $(this).val();}).get();
		var edit_parameter = $("input[id='edit_parameter']") .map(function(){return $(this).val();}).get();
	    var edit_value = $("input[id='edit_value']") .map(function(){return $(this).val();}).get();
	    var edit_measurment = $("input[id='edit_measurment']") .map(function(){return $(this).val();}).get();
	    var edit_remarks = $("input[id='edit_remarks']") .map(function(){return $(this).val();}).get();
 $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/updateParams',  
				    data: {testId:testId,edit_parameter:edit_parameter,edit_value:edit_value,edit_measurment:edit_measurment,edit_remarks:edit_remarks,id:id},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_PARAMETERS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_PARAMETERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}


function add_users()
{
	
	var serviceID = $("#serviceId option:selected").val();
	
	if(logginAdminServiceId > 0 )
	{
	    serviceID=logginAdminServiceId;
	}
	if(LogginagentServiceId > 0 )
	{
	   serviceID=LogginagentServiceId;
	}

	var verticalsID = $("#verticals option:selected").val();
	
	if(logginAgentVericalID > 0 )
	{
	   verticalsID=logginAgentVericalID;
	}
	
	var name=$('#user_name').val();
	var emailAdress=$('#email_adress').val();
	var phoneNumber=$('#phone_number').val();
	var password=$('#password').val();
	var cpassword=$('#confirm_password').val();
	
	var dataString = 'name=' + name + '&emailAdress=' + emailAdress + '&phoneNumber=' + phoneNumber + '&password=' + password + '&cpassword=' + cpassword + '&serviceID=' + serviceID+'&verticalsID=' + verticalsID;
		
	if(name=="" || emailAdress=="" || phoneNumber=="" || password=="" || cpassword=="" || serviceID=="" || verticalsID=="")
	{
		$('#USERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Users/addUser',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#USERS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#USERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	
}

function deleteUser(userId)
{
	var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Users/deleteUser', 
							data: {userId:userId},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}
function user_inactivate(userId,id){ 
   
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Users/usersActiveNess', 
				    data: {userId:userId,id:id},
                            success: function(response){
								
						    $('#activeUser_'+userId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}

function user_active(userId,id){
         
            $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Users/usersActiveNess',  
				    data: {userId:userId,id:id},
                            success: function(response){
								
						    $('#activeUser_'+userId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}
function edit_user(userId)
{ 
//alert(userId);
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Users/editUsers',  
				    data: {userId:userId},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editusers").modal('show');
						         $('#_edit_user_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function update_user(userId)
{
	    var serviceId = $("#serviceID option:selected").val();
		if(logginAdminServiceId > 0 )
		{
			serviceId=logginAdminServiceId;
		}
		if(LogginagentServiceId > 0 )
		{
		   serviceId=LogginagentServiceId;
		}

		 var verticalsId = $("#verticalsId option:selected").val();
		
		if(logginAgentVericalID > 0 )
		{
		   verticalsId=logginAgentVericalID;
		}
		
	   var user_name=$('#updateuser_name').val();
	   var emailAddress=$('#update_emailAddress').val();
	   var phoneNumber=$('#update_phoneNumber').val();
	   
	    if(user_name==""  || emailAddress=="" || phoneNumber=="" || serviceId=="" || verticalsId=="")
		{
			$('#UPDATE_USERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
			return false;
		}else {
	          $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Users/updateUsers',  
				    data: {userId:userId,user_name:user_name,emailAddress:emailAddress,phoneNumber:phoneNumber,verticalsId:verticalsId,serviceId:serviceId},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_USERS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_USERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
		}	
         return false;
	
}

/**********
Add Hospitals
********/
function add_hospitals()
{
	var name=$('#user_name').val();
	var emailAdress=$('#email_adress').val();
	var phoneNumber=$('#phone_number').val();
	var address=$('#address').val();
	var fromTimings=$('#fromTimings').val()+ ' ' +$('#am option:selected').text();
	var toTimings=$('#toTimings').val()+ ' ' +$('#pm option:selected').text();
	var dataString = 'name=' + name + '&emailAdress=' + emailAdress + '&phoneNumber=' + phoneNumber + '&address=' + address + '&fromTimings=' + fromTimings + '&toTimings=' + toTimings ;
	if(name=="" || emailAdress=="" || phoneNumber=="" || address=="" || fromTimings=="" || toTimings=="")
	{
		$('#HOSPITALS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Hospitals/addHospital',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#HOSPITALS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#HOSPITALS_MESSAGE').html('<span class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	
}
function edit_hospital(hospitalId)
{ 

	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Hospitals/editHospitals',  
				    data: {hospitalId:hospitalId},
                            success: function(response){
								$.getScript(customvalidationJs);
								$("#editHospitals").modal('show');
						        $('#_edit_hospital_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}

function update_hospital(hospitalId)
{
	   var name=$('#updatehospital_name').val();
	   var emailAddress=$('#update_emailAddress').val();
	   var phoneNumber=$('#update_phoneNumber').val();
	   var address=$('#update_address').val();
	   var fromTimings=$('#from_timings').val()+ ' ' +$('#update_am option:selected').text();
	   var toTimings=$('#to_timings').val()+ ' ' +$('#update_pm option:selected').text();
	    if(name==""  || emailAddress=="" || phoneNumber=="" || address=="" || address=="" || fromTimings==""  || toTimings=="" )
		{
			$('#UPDATE_HOSPITALS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
			return false;
		}else {
	          $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Hospitals/updateHospitals',  
				    data: {hospitalId:hospitalId,name:name,emailAddress:emailAddress,phoneNumber:phoneNumber,address:address,fromTimings:fromTimings,toTimings:toTimings},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_HOSPITALS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_HOSPITALS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
		}	
         return false;
	
}
function deleteHospital(hospitalId)
{
	var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Hospitals/deleteHospital', 
							data: {hospitalId:hospitalId},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}
function hospital_inactivate(hospitalId,id){ 
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Hospitals/hospitalsActiveNess', 
				    data: {hospitalId:hospitalId,id:id},
                            success: function(response){
								
						    $('#activeHospital_'+hospitalId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}

function hospital_active(hospitalId,id){
         
            $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Hospitals/hospitalsActiveNess',  
				    data: {hospitalId:hospitalId,id:id},
                            success: function(response){
								
						    $('#activeHospital_'+hospitalId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}

function getUsers(Email_or_Phone)
		{ 
		
		$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Records/checkuser',  
				    data: {Email_or_Phone:Email_or_Phone},
                                success: function(response){
								
								if(response.status == '1'){
								var userId=response.result[0].userId;
								$('#name').val(response.result[0].userName);
							    $('#emailAdress').val(response.result[0].emailAddress);
								$('#phoneNumber').val(response.result[0].phoneNumber);
								$('#dob').val(response.result[0].dob);
								$('#age').val(response.result[0].age);
								$('#gender').val(response.result[0].gender);
								$('#blood_group option[value='+response.result[0].bloodGroup+']').attr('selected','selected');
								if(response.result[0].gender == 1){
								//$("#male").attr('checked', 'checked');
								$("#male").prop("checked", true);
								}else{
								$("#female").attr('checked', 'checked');
								}
							    $('#addbutton').html(' <button type="button" class="btn green" onclick="add_record('+userId+')">Add RECORD</button>' );
								}else{
								//refresh_form_fields();	
							    var userId= 0;
								$('#addbutton').html(' <button type="button" class="btn green" onclick="add_record('+userId+')">Add RECORD</button>' );
								}
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
/*function add_record(userId)
{   
    var userId = userId;
    var testId =$("select[name='test[]']").map(function() { return this.value; }).toArray();
    if(userId == 0){
	 	userId ='';
   	}
	var parameterId = [];
	var measurment = [];
    for (var i  = 0; i < testId.length; i++){
	var	j=i+1;
	parameterId[i] = $("input[id='parameterId"+j+"']").map(function(){return $(this).val();}).get();
    measurment[i] = $("input[id='measurment"+j+"']").map(function(){return $(this).val();}).get();
    }
    var parameterValues= JSON.stringify(parameterId);
    var measurmentValues= JSON.stringify(measurment);
   
    var name=$('#name').val();
	var emailAdress=$('#emailAdress').val();
	var phoneNumber=$('#phoneNumber').val();
	var gender=$('input[name="optradio"]:checked').val();
    var age=$('#age').val();
	var dob=$('#dob').val();
	var blood_group=$('#blood_group').val();
	var dataString = 'userId=' + userId + '&testId=' + testId+ '&parameterID=' + parameterValues+ '&measurment=' + measurmentValues + '&name=' + name + '&emailAdress=' + emailAdress + '&phoneNumber=' + phoneNumber+ '&gender=' + gender+ '&age=' + age+ '&dob=' + dob+ '&blood_group=' + blood_group ;
	alert(dataString);
	if(name =="" || emailAdress=="" || phoneNumber =="")
	{
		$('#AGENT_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Records/addRecord',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#AGENT_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
		                            refresh_form_fields();
									window.location.href="";
								}else{
									$('#AGENT_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
		                            return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             alert("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	
}*/

function getParameters(testID,index)
{ 
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Records/ParamersByTest',  
				    data: {testID:testID,index:index},
                            success: function(response){
								//alert(response);
								$('#parameterForm'+index).html(response);
								 $("#add_test_div").show();
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
function another_test(index)
{ 
var index =index+1;
       $.ajax({
                type: "post",
                url:baseUrl+'/Webservices/Records/AnotherTest',  
			    data: {index:index},
                        success: function(response){
							$('#form_sample_2').append(response);
							$('#button1').html('<button type="button" id="button1" class="btn green" onclick="another_test('+index+')">Another Test</i></button>' );
					    },
						error: function(xhr, statusText, err){
                           console.log("Error:" + xhr.status);  
						}
				 });
 				
         return false;
}
/***********
Add Partners
************/
function add_partner()
{
	var name=$('#name').val();
	var emailAdress=$('#emailAdress').val();
	var phoneNumber=$('#phoneNumber').val();
	var password=$('#password').val();
	var cpassword=$('#cpassword').val();
	var address=$('#address').val();
	var website=$('#website').val();
	var formData = new FormData($('#partnersForm')[0]);
	if(name=="" || emailAdress=="" || phoneNumber=="" || address=="" || website=="" ||password=="" || cpassword =="")
	{
		$('#PARTNERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/addPartner',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#PARTNERS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#PARTNERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	
}

function deletePartner(partnerId)
{
	var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Partners/deletePartner', 
							data: {partnerId:partnerId},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}

function partner_inactivate(partnerId,id){ 
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/partnersActiveNess', 
				    data: {partnerId:partnerId,id:id},
                            success: function(response){
								
						    $('#activePartner_'+partnerId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}

function partner_active(partnerId,id){
           $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/partnersActiveNess',  
				    data: {partnerId:partnerId,id:id},
                            success: function(response){
								
						    $('#activePartner_'+partnerId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}

function edit_partner(partnerId)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Partners/editPartner',  
				    data: {partnerId:partnerId},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editpartner").modal('show');
						         $('#_edit_partner_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function update_partner(partnerId)
{
	
	var name=$('#update_name').val();
	var emailAddress=$('#update_emailAddress').val();
	var phoneNumber=$('#update_phoneNumber').val();
	var address=$('#update_address').val();
	var website=$('#update_website').val();
	var formData = new FormData($('#UpdatePartner')[0]);
	
	if(name=="" || emailAddress=="" || phoneNumber=="" || address=="" || website == "" )
	{
		$('#UPDATE_AGENTS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/updatePartner',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json",  
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#UPDATE_PARTNERS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_PARTNERS_MESSAGE').html('<span class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}
function update_profile(userId)
	{
	   var name=$('#name').val();
	   var emailAdress=$('#emailAdress').val();
	  var number=$('#number').val();
	  
	   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Superadmin/updateProfile',  
				    data: {name:name,emailAdress:emailAdress,number:number,userId:userId},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#PROFILE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#PROFILE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}

function edit_password(adminID)
{
	
	$.ajax({
                    type: "post",
					 url:baseUrl+'/Webservices/Superadmin/editAdminPassword',  
                    data: {adminID:adminID},
                            success: function(response){
								 
								 $("#Update_Password").modal('show');
						         $('#_update_password_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
function Update_Superadmin_Password(adminID){
	var update_password=$('#update_password').val();
	var new_password=$('#update_npassword').val();
	var confirm_password=$('#update_cpassword').val();
	var dataString = 'update_password=' + update_password + '&new_password=' + new_password + '&confirm_password=' + confirm_password +  '&adminID=' + adminID;
	
	if(update_password=="" || new_password=="" || confirm_password=="")
	{
		$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/SuperAdmin/updateSuperAdminPassword',  					
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}


function refresh_form_fields()
{
	
	$('#country_name').val('');
	$('#country_code').val('');
	$('#state_name').val('');
	$('#state_code').val('');
	$('#city_name').val('');
	$('#city_code').val('');
	$("#countryID option:selected").val('');
	$("#stateID option:selected").val('');
	$("#cityID option:selected").val('');
	$('#city_name').val('');
	$('#city_code').val('');
	$("#stateID option:selected").val('');
	$("#countryID option:selected").val('');
	$('#kios_name').val('');
	$('#kios_location').val('');
	
	$('#name').val('');
	$('#emailAdress').val('');
	$('#phoneNumber').val('');
	$('#dob').val('');
	$('#age').val('');
	$('#blood_group').val('');
	
	$('#test_name').val('');
	$('#test_cost').val('');
    $('#add_parameter').val('');
	$('#add_value').val('');
	$('#add_measurment').val('');
	$('#add_remarks').val('');
   	$('#parameter').val('');
	$('#remarks').val('');
	$('#value').val('');
	$('#measurment').val('');
	//New adding refreshment fiedls
	$('#agent_name').val('');
	$('#agent_location').val('');
	$("#serviceID option:selected").val('');
	$('#emailAdress').val('');
	$('#phoneNumber').val('');
	$('#password').val('');
	$('#cpassword').val('');
	$("#verticals option:selected").val('');
	
	$('#update_password').val('');
	$('#update_npassword').val('');
	$('#update_cpassword').val('');
	
	$('#name').val('');
	$('#admin_location').val('');
	
	$('#test_name').val('');
	$('#test_cost').val('');
	$('#discount_cost').val('');
	
	$('#user_name').val('');
	$('#email_adress').val('');
	$('#phone_number').val('');
	$('#password').val('');
	$('#confirm_password').val('');
	
	$('#address').val('');
	$('#website').val('');
	
	$('#Name').val('');
	$('#location').val('');
	
	$('#PartnerId').val('');
	$('#name').val('');
	$('#package_amount').val('');
	$('#package_discount').val('');
	$('#productName').val('');
	$('#productType').val('');
	$('#brand').val('');
    $('#cost').val('');
	$('#number').val('');
	$('#tipName').val('');
	$('#tipDesc').val('');
	
	$('#serviceName').val('');
	$('#serviceDesc').val('');
}

function add_Verticals(serviceID)
{
	
	//var serviceID = $("#serviceID option:selected").val();
	var name=$('#Name').val();
	var emailAdress=$('#emailAdress').val();
	var phoneNumber=$('#phoneNumber').val();
	var locations=$('#location').val();
	var formData = new FormData($('#verticalsForm')[0]);
	//var dataString = 'name=' + name + '&emailAdress=' + emailAdress + '&phoneNumber=' + phoneNumber + '&serviceID=' + serviceID + '&location=' + locations ;
	if(name=="" || emailAdress=="" || phoneNumber=="" ||  locations=="" ||serviceID=="" )
	{
		$('#VERTICAL_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Verticals/addVerticals',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                    success: function(response){
								
								if(response.status == '1')
								{
									$('#VERTICAL_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#VERTICAL_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	
	
}

function deleteVerticals(Id)
{
	var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Verticals/deleteVertical', 
							data: {Id:Id},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}

function Vertical_inactivate(ID,id){ 
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Verticals/verticalActiveNess', 
				    data: {ID:ID,id:id},
                            success: function(response){
								
						    $('#activeVertical_'+ID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}

function Vertical_active(ID,id){
           $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Verticals/verticalActiveNess',  
				    data: {ID:ID,id:id},
                            success: function(response){
								
						    $('#activeVertical_'+ID).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}

function editVerticals(ID)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Verticals/editVertical',  
				    data: {ID:ID},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editVerticals").modal('show');
						         $('#_edit_Verticals_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}


function updateVertical(ID,serviceID)
{
	var formData = new FormData($('#updateVerticals')[0]);
	
	var name=$('#updateName').val();
	var emailAdress=$('#updateEmailAddress').val();
	var phoneNumber=$('#update_phoneNumber').val();
	var locations=$('#updateLocation').val();
	var dataString = 'name=' + name + '&emailAdress=' + emailAdress + '&phoneNumber=' + phoneNumber + '&serviceID=' + serviceID + '&location=' + locations + '&ID=' + ID;
	if(name=="" || emailAdress=="" || phoneNumber=="" ||  locations=="" || ID=="")
	{
		$('#UPDATE_VERTICAL_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Verticals/updateVerticals',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#UPDATE_VERTICAL_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#UPDATE_VERTICAL_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	
	
}
//27/10
function getAgentDetails(verticle){
	
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Verticals/agentDetails',  
				    data: {verticle:verticle},
                            success: function(response){
								$("#agentDetailsPopup").modal('show');
						        $('#_agent_details_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
//27/10
function getVerticals(serviceID)
{ 
 $.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Agents/VerticalsByServices',  
				    data: {serviceID:serviceID},
                            success: function(response){
							   //$("#verticals").select2("val", "");	
							   $('#verticals').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function save_package(){
	
    var PartnerId=$('#PartnerId').val();
	if(logginRoleId == partnerRoleID){
		PartnerId=logginuserId;
	}
	var package_name=$('#name').val();
	var package_amount=$('#package_amount').val();
	var package_discount=$('#package_discount').val();
    var test = [];
        $.each($("#multi-append option:selected"), function(){            
            test.push($(this).val());
        });
    var tests= test.join(", ");    
	var count= tests.split(',').length;
    var x = new Array();
    x = tests.split(",");
    s = JSON.stringify(x);
	
   var dataString = 'package_name=' + package_name + '&package_amount=' + package_amount+ '&test=' + s + '&count=' + count+ '&package_discount=' + package_discount + '&PartnerId=' + PartnerId ;
	
	if(package_name=="" || package_amount==""|| test =="" )
	{
		$('#ADD_PACKAGE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/savePackage',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									
									$('#ADD_PACKAGE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#ADD_PACKAGE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}
function deletePackage(packageId)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Partners/deletePackage', 
							data: {packageId:packageId},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}
function edit_package(packageId)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Partners/editPackage',  
				    data: {packageId:packageId},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editpackage").modal('show');
						         $('#_edit_package_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
function update_package(packageId)
{
	
	var packageName=$('#packageName').val();
	var packageAmount=$('#packageAmount').val();
    var packageDiscount = $('#packageDiscount').val();
	var uptest = [];
        $.each($("#packageTest option:selected"), function(){            
            uptest.push($(this).val());
        });
    var uptests= uptest.join(", ");
	var count= uptests.split(',').length;
	var x = new Array();
    x = uptests.split(",");
    s = JSON.stringify(x);
	
	var dataString = 'packageName=' + packageName + '&packageAmount=' + packageAmount + '&packageTest=' + s + '&packageDiscount=' + packageDiscount + '&packageId=' + packageId;
	if(packageName=="" || packageAmount=="" || packageTest=="")
	{
		$('#UPDATE_PACKAGE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/updatePackage',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#UPDATE_PACKAGE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_PACKAGE_MESSAGE').html('<span class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}
function select_test(index)
{
       $.ajax({
                type: "post",
                url:baseUrl+'/Webservices/partners/SelectTest',  
			    data: {index:index},
                        success: function(response){
							$('#addField').append(response);
							$('#button1').html('<button type="button" id="button1" class="btn green" onclick="select_test()"><i class="fa fa-plus"></i></button>' );
					    },
						error: function(xhr, statusText, err){
                       
						   console.log("Error:" + xhr.status);  
						}
				 });
 				
         return false;
}


//Mounika 

function uploadPdf()
{

var formData = new FormData($('#upload')[0]);	
$.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Records/uploadPDF',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPLOAD_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPLOAD_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function add_product()
{  
    var productName=$('#productName').val();
	var productType=$('#productType').val();
	var brand=$('#brand').val();
    var cost=$('#cost').val();
	var formData = new FormData($('#form_sample_2')[0]);
	if(productName =="" || productType=="" || brand =="" ||cost=="" )
	{
		$('#PRODUCT_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
		$.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/addProduct',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#PRODUCT_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#PRODUCT_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 			}	
         return false;
	
}
  
function edit_product(productId)
{  
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Partners/editProduct',  
				    data: {productId:productId},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editproduct").modal('show');
						         $('#_edit_product_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function update_product(productId)
{  
   
    var ProductName=$('#ProductName').val();
	var ProductType=$('#ProductType').val();
	var Brand=$('#Brand').val();
    var Cost=$('#Cost').val();
	var formData = new FormData($('#UpdateProduct')[0]);
	
	if(ProductName =="" || ProductType=="" || Brand =="" ||Cost=="" )
	{
		$('#UPDATE_PRODUCT_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
      $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/updateProduct',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_PRODUCT_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_PRODUCT_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	 
	
}
function deleteProduct(productId)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Partners/deleteProduct', 
							data: {productId:productId},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}
function update_partner_profile(userId)
	{
		
	   var name=$('#name').val();
	   var emailAdress=$('#emailAdress').val();
	  var number=$('#number').val();
	    if(name=="" || emailAdress=="" || number=="")
	  {
		 $('#PROFILE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	  }else{
	   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/updateProfile',  
				    data: {name:name,emailAdress:emailAdress,number:number,userId:userId},
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#PROFILE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#PROFILE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	  }
         return false;
	
}
function edit_partner_password(partnerId)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Partners/passwordUpdatePopup',  
				    data: {partnerId:partnerId},
                            success: function(response){
								 
								 $("#Update_Password").modal('show');
						         $('#_update_password_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function Update_partner_Password(partnerId){
	var update_password=$('#update_password').val();
	var new_password=$('#update_npassword').val();
	var confirm_password=$('#update_cpassword').val();
	var dataString = 'update_password=' + update_password + '&new_password=' + new_password + '&confirm_password=' + confirm_password +  '&partnerId=' + partnerId;
	
	if(update_password=="" || new_password=="" || confirm_password=="")
	{
		$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Partners/updatePassword',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_PASSWORD_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
}

var packagesList = new Array();
var testsList = new Array();
function getPackageAmount(packageId,packageName,packageCost,checkboxValue)
	{
      //var packageIdlist =$("input:checkbox[name='packages[]']:checked").map(function() { return this.value; }).toArray();
	  //var testIdList =$("input:checkbox[name='tests[]']:checked").map(function() { return this.value; }).toArray();
	  
	  var totalCost=$('#totalCost').val();
  	  if(checkboxValue== 1){
		  packagesList[packagesList.length] =packageId;
		 
		  var totalAmount=parseInt(totalCost)+parseInt(packageCost);
		  $('#totalCost').val(totalAmount);
		  
		  if(packagesList.length > 0){
		   $('#packagesList').show();
	       $('<tr><td><input  style="padding-left:40px;border: 0;outline: 0;background: transparent;" type="text" id="'+packageName+'_'+packageId+'"   value="'+packageName+'"/></td></tr></br>' ).appendTo("#packagesList");
		  }
     }else{
		   var index = packagesList.indexOf(packageId);
		  if (index > -1) {
			packagesList.splice(index, 1);
		  }
		  
		  $('input#'+packageName+'_'+packageId+'[type="text"]').remove();
		  if(packagesList.length == 0){
				$('#packagesList').hide();
			}
			
			var totalAmount = parseInt(totalCost) - parseInt(packageCost);
			$('#totalCost').val(totalAmount);
	  }
	  if(packagesList == 0 && testsList == 0 ){
		   $('#packages_tests_list').hide();
	  }else{
		   $('#packages_tests_list').show();
	  }
  }


function getTestsByPackageId(packageId)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Records/TestsListByPackage',  
				    data: {packageId:packageId},
                            success: function(response){
								$("#TestsByPackagePopUp").modal('show');
						        $('#_edit_TestsPackages_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}

function getTestAmount(testId,testName,testCost,checkboxValue)
	{	
	  var totalCost=$('#totalCost').val();
	  var testName=testName.replace(/\s+/g, '');
  	  if(checkboxValue== 1){
		  testsList[testsList.length] =testId;
		  var totalAmount=parseInt(totalCost)+parseInt(testCost);
		  $('#totalCost').val(totalAmount);
		  if(testsList.length > 0){
			 
		   $('#testsList').show();
	       $('<tr><td><input  style="padding-left:40px;border: 0;outline: 0;background: transparent;" type="text" id="'+testName+'_'+testId+'"   value="'+testName+'"/></td></tr></br>' ).appendTo("#testsList");
		  }
     }else{
		  var index = testsList.indexOf(testId);
		  if (index > -1) {
			testsList.splice(index, 1);
		  }
		  $('input#'+testName+'_'+testId+'[type="text"]').remove();
		  if(testsList.length == 0){
				$('#testsList').hide();
			}
			
			var totalAmount = parseInt(totalCost) - parseInt(testCost);
			$('#totalCost').val(totalAmount);
	  }
	  
	   if(packagesList.length == 0 && testsList.length == 0 ){
		   $('#packages_tests_list').hide();
	  }else{
		   $('#packages_tests_list').show();
	  }
 }


function add_record(userId)
{ 
    
	var serviceId=$('#serviceId').val();
	if(logginAdminServiceId > 0 )
	{
	    serviceId=logginAdminServiceId;
	}
	if(LogginagentServiceId > 0 )
	{
	   serviceId=LogginagentServiceId;
	}

	var verticalId=$('#verticalsId').val();
	if(logginAgentVericalID > 0 )
	{
	   verticalId=logginAgentVericalID;
	}
	
	var testId =$("input:checkbox[name='tests[]']:checked").map(function() { return this.value; }).toArray();
	var packageId =$("input:checkbox[name='packages[]']:checked").map(function() { return this.value; }).toArray();
	
	if(userId == 0){
			userId ='';
	}
	var name=$('#name').val();
	var emailAdress=$('#emailAdress').val();
	var phoneNumber=$('#phoneNumber').val();
	var age=$('#age').val();
	var dob=$('#dob').val();
	var blood_group = $("#blood_group option:selected").val();
	var dataString = 'userId=' + userId + '&testId=' + testId+ '&packageId=' + packageId+ '&name=' + name + '&emailAdress=' + emailAdress + '&phoneNumber=' + phoneNumber+  '&age=' + age+ '&dob=' + dob + '&blood_group=' + blood_group + '&verticalId=' + verticalId
	+ '&serviceId=' + serviceId;
	if(packageId =="" && testId ==""){
		$('#RECORDS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please select any package or test.</div>');
		$("html, body").animate({ scrollTop: 0 }, 200);
		return false;
	}else{
		
		
	if(name =="" || emailAdress=="" || phoneNumber ==""|| serviceId =="" ||verticalId=="")
	{
		$('#RECORDS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please fill all required fields.</div>');
		$("html, body").animate({ scrollTop: 0 }, 200);
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Records/addRecord',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#RECORDS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
		                            refresh_form_fields();
									$("html, body").animate({ scrollTop: 0 }, 200);
									window.location.href="";
								}else{
									$('#RECORDS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									$("html, body").animate({ scrollTop: 0 }, 200);
		                            return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	  }
	}	
         return false;
}


//25/10
function update_reports(testReportId)
{        

var formData = new FormData($('#form_sample_2')[0]);
 var parameters = [];
    $("input[name='parameterValue[]']").each(function() {
      var value = $(this).val();
       if (value) {
        parameters.push(value);
      }
    });
    if (parameters.length === 0) {
		$('#UPDATE_REPORT_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Provide atleast one parameter value.</div>');
			return false;

	}else{
        $.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Records/updateReportsValues',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
					        success: function(response){
								if(response.status==1)
								{
									$('#UPDATE_REPORT_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									   refresh_form_fields();
									window.location.href="";
									
								}else{
									$('#UPDATE_REPORT_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}
         return false;
}

function update_values(userId,testname,testId,reportId)
{ 
		
	  $.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Records/updateTestValues',  
				    data: {userId:userId,testId:testId,reportId:reportId,testname:testname},
                            success: function(response){
								 
								 $("#updateReport").modal('show');
						         $('#_update_report_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}

function edit_pdf(ID,sts)
{  

$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Records/updatePdf',  
				    data: {'ID':ID,'sts':sts},
                            success: function(response){
								
								 $("#uploadPDF").modal('show');
						         $('#_edit_upload_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}

/**********
Add Banners
********/
function add_banner()
{
var formData = new FormData($('#iosBanner')[0]);	
$.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Banners/addBanner',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#BANNER_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#BANNER_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function deleteBanner(image,appId)
{
 var ok = confirm("Are you sure to Delete?"); 
  if (ok) {
	  
         $.ajax({
                          type: "post",
						  url:baseUrl+'/Webservices/Banners/deleteBanner', 
                          data: {image:image,appId:appId},
                          cache: false,
                          dataType: "json", 							
						   success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
 				
         return false;
	  }
	
}

function verticalsByServices(serviceId)
{ 
   
$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Records/verticalsByServices',  
				    data: {serviceId:serviceId},
                            success: function(response){
								
							 $("#verticalsId").select2("val", "");	
							 $('#verticalsId').html(response);
							 
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function add_tip()
{  
    var tipName=$('#tipName').val();
	var tipDesc=$('#tipDesc').val();
	var formData = new FormData($('#form_sample_3')[0]);
	if(tipName =="" || tipDesc=="" )
	{
		$('#TIPS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
		$.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Healthtips/addTip',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#TIPS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#TIPS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 			}	
         return false;
	
}
  
function edit_tip(tipId)
{  
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Healthtips/editTip',  
				    data: {tipId:tipId},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#edittip").modal('show');
						         $('#_edit_tip_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}
function update_tip(tipId)
{  
   
    var Tip=$('#h_tip').val();
	var TipDesc=$('#h_tip_description').val();
	var formData = new FormData($('#UpdateTip')[0]);
	
	if(Tip =="" || TipDesc==""  )
	{
		$('#UPDATE_TIP_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
      $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Healthtips/updateTip',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status==1)
								{
									$('#UPDATE_TIP_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									window.location.href="";
								}else{
									$('#UPDATE_TIP_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	 
	
}
function deleteTip(tipId)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Healthtips/deleteTip', 
							data: {tipId:tipId},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}

function getSatesAtAjax(countryID)
{ 
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/StatesByCountry',  
				    data: {countryID:countryID},
                            success: function(response){
								$('#stateId').html(response);
								 $('#cityId').html('');
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function getCitysAtAjax(stateID)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/citiesByState',  
				    data: {stateID:stateID},
                            success: function(response){
								
							   $('#cityId').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function getVerticalsAtAjax(serviceID)
{ 
	$.ajax({
						type: "post",
						url:baseUrl+'/Webservices/Agents/VerticalsByServices',  
						data: {serviceID:serviceID},
								success: function(response){
								   //$("#verticals").select2("val", "");	
								   $('#verticalsId').html(response);
						},
							error: function(xhr, statusText, err){
								 console.log("Error:" + xhr.status);  
							}
							
					});
					
			 return false;
}

/***********
Add Services
************/
function add_Services()
{
	var serviceName=$('#serviceName').val();
	var serviceDesc=$('#serviceDesc').val();
	var formData = new FormData($('#servicesForm')[0]);
	if(serviceName=="" || serviceDesc=="")
	{
		$('#SERVICES_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/addServices',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#SERVICES_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#SERVICES_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	
	
}

function deleteService(serviceId)
{
	var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	 
			$.ajax({
							type: "post",
							url:baseUrl+'/Webservices/Master/deleteService', 
							data: {serviceId:serviceId},
                            cache: false,
                            dataType: "json", 							
							success: function(response){
										 window.location.href="";
										 
							},
								error: function(xhr, statusText, err){
									 console.log("Error:" + xhr.status);  
								}
								
						});
	  }		
         return false;
	
}

function Service_inactivate(serviceId,id){ 
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/serviceActiveNess', 
				    data: {serviceId:serviceId,id:id},
                            success: function(response){
								
						    $('#activeService_'+serviceId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
  return false;

}

function Service_active(serviceId,id){
           $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/serviceActiveNess',  
				    data: {serviceId:serviceId,id:id},
                            success: function(response){
								
						    $('#activeService_'+serviceId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}

function editServices(serviceId)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Master/editService',  
				    data: {serviceId:serviceId},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editServices").modal('show');
						         $('#_edit_Services_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}


function updateService(serviceID)
{
	var formData = new FormData($('#updateServices')[0]);
	var name=$('#updateServiceName').val();
	var description=$('#updateServiceDesc').val();
	if(name=="" || description=="")
	{
		$('#UPDATE_SERVICES_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
	    $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Master/updateService',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									$('#UPDATE_SERVICES_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('#UPDATE_SERVICES_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){ 
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}		
         return false;
	
}

function getRevenueDetails(serviceId,verticleID)
{ 
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Webservices/Verticals/revenueDetails',  
				    data: {serviceId:serviceId,verticleID:verticleID},
                            success: function(response){
								$("#revenuePopup").modal('show');
						        $('#_revenue_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
} 

function uploadUsersExcel()
{
   $('button').prop('disabled',true);
   var formData = new FormData($('#usersExcelForm')[0]);	
   $.ajax({
                    type: "POST",
                    url:baseUrl+'/Webservices/Users/uploadUsersExcel',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "json", 
                            success: function(response){
							if(response.status == '1')
								{
									$('#EXCEL_USERS_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									refresh_form_fields();
									window.location.href="";
								}else{
									$('button').prop('disabled',false);
									$('#EXCEL_USERS_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
	
}

function allButtonsDisabled()
{
	$('button').prop('disabled',true);
	
}

function allButtonsEnabled()
{
	$('button').prop('disabled',false);
}