<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
//require_once  'C:/xampp/htdocs/oes_live/admin/ExcelReader/PHPExcel/IOFactory.php';
require_once  '/var/www/html/admin/ExcelReader/PHPExcel/IOFactory.php';
class Promotions extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('common_helper');
	}
	
	function index()
	{
		$isSessionIn=$this->session->userdata('isSessionIn');
	    if(empty($isSessionIn))
	    {
	      redirect(LOGOUT_URL,'refresh');
	    }
		
		
		$messege = array('status'=>"",'message'=>"");
		if(isset($_POST['submit'])){
		    $this->load->library('form_validation');
	        $this->form_validation->set_rules('excelUsersFile','Excel file','trim');
	        if($this->form_validation->run()!=false)
	        {
				
				$result=0;
				$target_path ='./uploads/';
				$rand=rand();
				$xlsx_name=$rand.time().'.xlsx'; 
				$fileTypes = array('xlsx');
			
				if(!empty($_FILES['excelUsersFile']['name'])){ 
					
					   $target_path = $target_path . $xlsx_name;
					   $response['file_name'] = basename($_FILES['excelUsersFile']['name']);
					   $filename=basename($_FILES['excelUsersFile']['name']);
					   $file_extension=pathinfo($_FILES['excelUsersFile']['name']);
					
					if (in_array(strtolower($file_extension['extension']), $fileTypes)) {
						
						$movefile=move_uploaded_file($_FILES['excelUsersFile']['tmp_name'], $target_path);
						
						if($movefile)
						{                 
								try {
									  $objPHPExcel = PHPExcel_IOFactory::load($target_path);
								} catch(Exception $e) {
									 die('Error loading file "'.pathinfo($target_path,PATHINFO_BASENAME).'": '.$e->getMessage());
								}
								$allUsersInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
								$usersCount = count($allUsersInSheet);
								if($usersCount<=1){ 
									
									$messege =array('status'=>2,'message'=>"Please select valid excel sheet.");
								}else{
									
									for($i=2;$i<=$usersCount;$i++){
										$phone = trim($allUsersInSheet[$i]["A"]);
										$email = trim($allUsersInSheet[$i]["B"]);
										$mobilePromotionalText = trim($allUsersInSheet[$i]["C"]);
										$emailPromotionalText = trim($allUsersInSheet[$i]["D"]);
										
										
										//otp to mobile
										if($phone != "" || $phone != null){
										    $res = sendOTP($mobilePromotionalText, $phone);
										}
										//send a email
										if($email != "" || $email != null){
											//$subject = "RE: Promotional";
										    //$smtpEmailSettings = $this->config->item('smtpEmailSettings');
											//$isEmailSent = //sendSmtpEmail($smtpEmailSettings['smtp_user'],$email,$subject,$emailPromotionalText);
											//print_r($isEmailSent);die();
										}
										$mes=SUCCESS;
										$messege =array('status'=>1,'message'=>$mes);
										
									}
									
									
								}
						
						}
						else
						{
							$messege =array('status'=>2,'message'=>FILE_NOT_UPLOADED);
							
							
						}
					}else{
						 $messege =array('status'=>2,'message'=>MODULE_FILE_ALLOWED_TYPES);
						 
					}
				}
				 
			}else{
				$messege = array('status' => 2,'message' =>validation_errors(),'result' => '');
			}
		}
		
		
		$this->load->view('dashboard/includes/header');
		$this->load->view('dashboard/promotions/promotions',$messege);
		$this->load->view('dashboard/includes/footer');
		
	}
	
	function downloadSheet()
	{
		$file="/var/www/html/admin/uploads/Promotions.xlsx";
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		ob_clean();
		flush();
		readfile($file);
		exit;
	}
}