<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
//require_once  'C:/xampp/htdocs/oes_live/admin/ExcelReader/PHPExcel/IOFactory.php';
require_once  '/var/www/html/admin/ExcelReader/PHPExcel/IOFactory.php';
class Questionbook extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Questionmodel');
	}
	
	/**********************************
	This method will useful to show the que and 
	test details
	**********************************/
	/* function index()
	{
		
		try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			
			
			
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			
			}else{
				$resellerId = 0;
			}
			$where = array('isDeleted'=>0,'isActive'=>1,'resellerId'=>$resellerId);
			$tests = $this->getAllRecords(TBL_TESTS,$where,'*');
			$subjects = $this->getAllRecords(TBL_SUBJECTS,$where,'*');
			$topics = $this->getAllRecords(TBL_TOPICS,$where,'*');
			$subjectId="";
			$subjectId=$this->input->post('subject');
			$topic=$this->input->post('topic');
             if($subjectId &&  $topic){
				$where = array('isDeleted'=>0,'subject_id'=>$subjectId,'topic_id'=>$topic,'resellerId'=>$resellerId); //on 18th
			}else if($subjectId && empty($topic) ){
				$where = array('isDeleted'=>0,'subject_id'=>$subjectId,'resellerId'=>$resellerId);
			}else if($topic && empty($subjectId)){
				$where = array('isDeleted'=>0,'topic_id'=>$type,'resellerId'=>$resellerId);
			}else{
				$where = array('isDeleted'=>0,'resellerId'=>$resellerId);
			}
			$questions = $this->getAllRecordsByDesc(TBL_QUETIONS,$where,'id','*');
			$data['questions']=$questions;
			$data['tests'] = $tests; 
			$data['topics'] = $topics; 
			$data['subjects'] = $subjects;  
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/questionBook/questions',$data);
			$extraFooter = $this->load->view('dashboard/questions_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	} */
	
	function index()
	{
		
		try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			
			}else{
				$resellerId = 0;
			}
			$where = array('isDeleted'=>0,'isActive'=>1,'resellerId'=>$resellerId);
			$tests = $this->getAllRecords(TBL_TESTS,$where,'*');
			$subjects = $this->getAllRecords(TBL_SUBJECTS,$where,'*');
			$topics = $this->getAllRecords(TBL_TOPICS,$where,'*');
			$config["per_page"] =50;$page=0;$sPage=0;	
			$config["base_url"] =  QUESTION_BOOK_URL;
			$testId=$this->uri->segment(5);
			if($this->uri->segment(4) == "test"){
				$config["base_url"] =  QUESTION_BOOK_URL.'/test/'.$testId;
			 }
			  if($this->uri->segment(4)=="test"){
			     $page=($this->uri->segment(6))? ($this->uri->segment(6)*$config["per_page"])-($config["per_page"]):0;
				 $sPage=$this->uri->segment(6);
				}else{
					$page=($this->uri->segment(4))? ($this->uri->segment(4)*$config["per_page"])-($config["per_page"]):0;
					$sPage=$this->uri->segment(4);
				} 
			 if($this->input->is_ajax_request())
		     { 
		      $page=$this->input->post('page');
				 if($page){
					$page=($page*$config["per_page"])-($config["per_page"]);
				}else{
				  $page=0;	
				} 
				
		    }
			$Totalquestions = $this->getAllRecords(TBL_QUETIONS,$where,'*','id');
		   $this->load->library('pagination');
	  
		$total_row = count($Totalquestions);
		$config["total_rows"] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] =3;
        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
		
		$this->pagination->initialize($config);
		$data["questions"] = $this->Questionmodel->getQuestions($config["per_page"], $page,$where,'id');
		$str_links = $this->pagination->create_links();
        $data['links'] = explode('&nbsp;',$str_links );
			
		    $data['tests'] = $tests; 
			$data['topics'] = $topics; 
			$data['subjects'] = $subjects;  
			$data['FromSno']=1;
			$data['ToSno']=1;
			if($sPage){
			$data['FromSno']=($sPage*$config["per_page"])-$config["per_page"]+1;
			}else{
				$data['FromSno']=1;
			}
			
			$this->load->view('dashboard/includes/header');
			if($this->input->post('paginationurl')){
				 $data['testId']=$this->input->post('testId');
				 $this->load->view('dashboard/questionBook/loadMoreQuestions',$data);
			}else{
		    $this->load->view('dashboard/questionBook/questions',$data);
			}
		
			$extraFooter = $this->load->view('dashboard/questions_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	}
	
	function addQuestion()
	{ 
		$TestType = $this->uri->segment(3);
		
		try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			}else{
				$resellerId=0;
				$where = array('isActive'=>1,"isDeleted"=>0,'resellerId'=>0);
			}
			$tests = $this->getAllRecords(TBL_TESTS,$where,'*');
			$subjects = $this->getAllRecords(TBL_SUBJECTS,$where,'*');
			$topics = $this->getAllRecords(TBL_TOPICS,$where,'*');
            $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId); //on 18th
			$serialNumber = $this->getAllRecords(TBL_QUETIONS,$where,'*');
			$serialNumber = count($serialNumber);
			$data['serialNumber']=$serialNumber ;
			$where = array('isActive'=>1,'isDeleted'=>0); //on 18th
			$totalQue = $this->getAllRecords(TBL_QUETIONS,$where,'*');
			$totalQue = count($totalQue);
			$data['lastQuestion']=$totalQue;
			$data['topics'] = $topics; 
			$data['tests'] = $tests; 
			$data['subjects'] = $subjects; 
			if($this->session->userdata('resellerId')){
				$resellerId=$this->session->userdata('resellerId');
				}else{
					$resellerId=0;
				}
			if(isset($_POST['AddQuetions']))
			{ 
				$quetions = $this->input->post('quetion[]');
				$hint = $this->input->post('hint[]');
				$subject = $this->input->post('subject');
				$topic = $this->input->post('topic');
				$queType = $this->input->post('queType[]');
				
			   if($quetions)
				{
					$i=$totalQue+1;
					$j= 0;
					$qId = '';
				   $quesArray=array_keys($quetions);
					foreach($quesArray as $quetionA)
					{
						
						$queArray = array('que' => trim($quetions[$quetionA]),
						                  'subject_id' => $subject,
						                  'topic_id' => $topic,
										  'hint' => $hint[$quetionA],
										  'queType' => $queType[$quetionA],
										  'resellerId' => $resellerId,
						                 
						);
						
						$queId = $this->insertOrUpdate(TBL_QUETIONS,$where=array(),$queArray);
						if($queArray['queType'] == 2){
							$textType=1;
							$options=$this->input->post('option'.$quetionA);
							if($options){
								foreach($options as $opt){
								$optionsArray = array('option' => trim($opt),
														  'que_id' => $queId,
														  'is_answer' => 1,
														  'textType' =>$textType,
														  'isActive' =>1,
														  'isDeleted'=>0,
														 
														  
									);	
								$this->insertOrUpdate(TBL_QUE_OPTIONS,$where=array(),$optionsArray);
								}
						   }
						}elseif($queArray['queType'] == 3){
							$textType=1;
							$options=$this->input->post('option'.$quetionA);
							if($options){
								foreach($options as $opt){
								$optionsArray = array('option' => trim($opt),
														  'que_id' => $queId,
														  'is_answer' => 1,
														  'textType' =>$textType,
														  'isActive' =>1,
														  'isDeleted'=>0,
														 
														  
									);	
								$this->insertOrUpdate(TBL_QUE_OPTIONS,$where=array(),$optionsArray);
								}
						   }
							
						}else{
						$TextOptions[$quetionA] = $this->input->post('option'.$quetionA.'[]');
						$TextOptions=array_filter($TextOptions[$quetionA]);
						$textType=1;
						$options[$quetionA] =$TextOptions;
						
						if($options[$quetionA] || @$_FILES['option'.$quetionA]){  
							$a=0; $b=1;
							
								
							$optionImages[$quetionA]=$_FILES['option'.$quetionA];
							$optimageNames=$optionImages[$quetionA]['name'];
							$optionsMime=$_FILES['option'.$quetionA]['type'];
							$h=0;
						    foreach($optimageNames as $name)
							{
								$option[$a] = "";
								$que_ans = $this->input->post('que'.$quetionA.$b.'ans');
								$is_answer = 0;
								if($que_ans == "on")
								{  
									$is_answer = 1;
								} 
															if(!empty($name)){
															$mime=$optionsMime[$h];
															if(strstr($mime, "video/")){
															$filetype = "video";
															$textType=3;
															}else if(strstr($mime, "image/")){
															$filetype = "image";
															$textType=2;
															}else if(strstr($mime, "audio/")){
															$filetype = "audio";
															$textType=4;
															}else{
																$textType=1;
															}  
															$file_extension=pathinfo($name);
															$rand=rand();
											                 $picname=$rand.time().'.'.strtolower($file_extension['extension']);
											                 $icon_target_path = './uploads/options/';
											                 $icon_target_path = $icon_target_path . basename($picname);
											                 $movefile=move_uploaded_file($optionImages[$quetionA]['tmp_name'][$a], $icon_target_path);
															 $option[$a]=$picname;
															}

						
								if(!empty($options[$quetionA][$a]))
								{
									 $option[$a]=$options[$quetionA][$a];
									 $textType=1;
								}
								
								if($option[$a]){
								$optionsArray = array('option' => trim($option[$a]),
								                      'que_id' => $queId,
													  'is_answer' => $is_answer,
													  'textType' =>$textType,
													  'isActive' =>1,
													  'isDeleted'=>0,
													 
								                      
								);
								$this->insertOrUpdate(TBL_QUE_OPTIONS,$where=array(),$optionsArray);
								}
							  $a++; $b++;$h++;
							}
						}
						$i++;
			             }
						
				 }
					
			     }
				 if($queId)
			     {
				   //$data['successMessage'] = SUCCESS;
				   $this->session->set_flashdata('Error', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>New question added successfully.</strong> </div>");
				   redirect(QUESTION_BOOK_URL,'refresh');
			     }else{
				    $data['errorMessage'] = ERROR;
			      } 
			}
			
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/questionBook/addQuestion',$data);
			$extraFooter = $this->load->view('dashboard/questions_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	}
	
	
	function editQuetions()
	{ 
		try{
			$queid=$this->uri->segment(4);
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			
			}else{
				$resellerId = 0;
			}
			//for edit
			if(isset($_POST['EditQuetions']))
			{
			//neatPrintAndDie($_POST);
				$quetions = $this->input->post('quetion[]');
				$queType = $this->input->post('queType[]');//31
				$optionA = $this->input->post('optionA[]');
				$optionB = $this->input->post('optionB[]');
				$optionC = $this->input->post('optionC[]');
				$optionD = $this->input->post('optionD[]');
				$optionAns = $this->input->post('optionAns[]');
				$queIds = $this->input->post('queId[]');
				$test=$this->input->post('test');
				$hint=$this->input->post('hint');
				if($test){
					$where = array();
					$testData['que_id']=$queIds[0];
					$testData['test_id']=$test;
					
					$testId = $this->insertOrUpdate(TBL_TEST_QUESTIONS,$where,$testData); 
				}
				
				if($quetions)
				{  
					$i= 0;
					$j= 0;
					$a= $c=0; 
					$opt="";
					foreach($queIds as $ID)
					{ 
 					
						 $Exists = $this->getSingleRecord(TBL_QUETIONS,array('id'=>$ID),'*' );
						 $DD=$ID;
						 if($Exists){
							 
								$queArray = array('que' => $quetions[$DD],
								                  'hint'=>$hint,
								);
							    $where = array('id'=>$queIds[$a++]);
								$QUEID = $where['id'];
								$queId = $this->insertOrUpdate(TBL_QUETIONS,$where,$queArray);
								
								
								if($queId)
								{   
   							
							        $options[$DD] = $this->input->post('option'.$DD.'[]');
							        $queType[$DD] = $this->input->post('queType'.$DD.'[]');
									
									$textType=1;
									$optionsId[$DD] = $this->input->post('optionsId'.$DD.'[]');
									
									$options[$DD] = $this->input->post('option'.$DD.'[]');
											$k = $l=0; 
									$b=1;  
									if($options[$DD]){  
									    if(count(array_filter($options[$DD])) > 0){ 
									        foreach($options[$DD] as $option[$DD])
									        {
										
												$actualOption = $option[$DD];
											if($actualOption != ""){	
												$QUEetonID = $QUEID;
												$que_ans = $this->input->post('que'.$DD.$b.'ans');
												$is_answer = 0;
												
												if($que_ans == "on")
												{  
													$is_answer = 1;
												}
												    $optionsArray = array('option' =>$actualOption,
																	  'is_answer' => $is_answer,
																	  'textType' => $textType,
																);
													if( $queType[$DD][0] == 3){
										                   $optionsArray = array('option' =>$actualOption,
																	  'is_answer' => 1,
																	  'textType' => $textType,
																);
									                     }
                                 											 
												$where=array();
												$where['que_id'] = $QUEetonID; 
												$where['option_id'] = $optionsId[$DD][$l]; 									 
												$haveRecord = $this->getSingleRecord(TBL_QUE_OPTIONS,$where, '*');
												if($haveRecord){
													 $where = array('option_id'=>$optionsId[$DD][$k]);
													 $this->insertOrUpdate(TBL_QUE_OPTIONS,$where,$optionsArray);
												}else{
													$saveoptionsArray = array('option' =>$option[$DD],
																			  'is_answer' => $is_answer,
																			  'que_id'=>$QUEetonID,
																			  'textType' => $textType,
																			  'isActive' =>1,
																              'isDeleted'=>0,
																	  );
													$this->insertOrUpdate(TBL_QUE_OPTIONS,array(),$saveoptionsArray);
													
												}
											}
												
											  $b++;	$l++; $k++;
									        } 
									}
									$fileTypes = array('jpeg', 'png', 'jpg');
							        $videoTypes = array( "gif", "mp3", "mp4", "wma");
								    if(!empty($_FILES['option'.$DD]['name'])){
									    $Imagename=$_FILES['option'.$DD]['name'];
										$z=0; $actualOption = "";
										$optionsMime=$_FILES['option'.$DD]['type'];
							            $V=0;
										if(count(array_filter($_FILES['option'.$DD]['name']))>0){ 
											
											foreach($Imagename as $name){
													
											    if(!empty($name)){  
												    $filename=basename($name);
					                                $file_extension=pathinfo($name);
					                               
													       $mime=$optionsMime[$V];
															if(strstr($mime, "video/")){
															$filetype = "video";
															$textType=3;
															}else if(strstr($mime, "image/")){
															$filetype = "image";
															$textType=2;
															}else if(strstr($mime, "audio/")){
															$filetype = "audio";
															$textType=4;
															}else{
																$textType=1;
															} 
															$file_extension=pathinfo($name);
															
													$rand=rand();
													$picname=$rand.time().'.'.strtolower($file_extension['extension']);
													$icon_target_path = './uploads/options/';
													$icon_target_path = $icon_target_path . basename($picname);
													$movefile=move_uploaded_file($_FILES['option'.$DD]['tmp_name'][$z], $icon_target_path);
													$actualOption[$z] = $picname;
													$QUEetonID = $QUEID; $cc = $z+1;
										            $que_ans = $this->input->post('que'.$i.$cc.'ans');
										
										            $is_answer = 0;
										            if($que_ans == "on")
													{  
														$is_answer = 1;
													}
										
													$optionsArray = array('option' =>$actualOption[$z],
																		 'is_answer' => $is_answer,
																		  'textType' => $textType
													                );
										
												    $where['que_id'] = $QUEetonID; 
												    $where['option_id'] = $optionsId[$DD][$z]; 										 
                                        
													$haveRecord = $this->getSingleRecord(TBL_QUE_OPTIONS,$where, '*');
													if($haveRecord){
														 $where = array('option_id'=>$optionsId[$DD][$z]);
														 $this->insertOrUpdate(TBL_QUE_OPTIONS,$where,$optionsArray);
													}else{
														if($optionsId[$DD][$z] != ""){ 
														$saveoptionsArray = array('option' =>$actualOption[$z],
																				  'is_answer' => $is_answer,
																				  'que_id'=>$QUEetonID,
																				  'textType' => $textType,
																				  'isActive' =>1,
																                  'isDeleted'=>0,
																		  );
																  
														$this->insertOrUpdate(TBL_QUE_OPTIONS,array(),$saveoptionsArray);
														}
													}
											    }
											     $z++; $V++;
										    }
												$Imageoptions[$DD]=$actualOption;
									        $k = $l=0; 
									        $b=1;
									
							            }
						            }
					            }
						}	
						}else{ 
						       
						        $queArray = array('que' => trim($quetions[$DD]),
						                  'que_id' => $queIds[$a++],
										  'queType	' => $queType[$DD],
						                 
						        );
								
								$where=array();
								$queId = $this->insertOrUpdate(TBL_QUETIONS,$where,$queArray);
								
						     	if($queType[$DD] == 2){
                                      $textType=1;
							
							        $options[$DD] = $this->input->post('option'.$DD.'[]');
									
									if($options[$DD]){
										$optns=$options[$DD];
										foreach($optns as $opt){
										$optionsArray = array('option' => trim($opt),
																  'que_id' => $queArray['que_id'],
																  'is_answer' => 1,
																  'textType' =>$textType,
																  'isActive' =>1,
																  'isDeleted'=>0,
																 
																  
											);	
											
										$this->insertOrUpdate(TBL_QUE_OPTIONS,$where=array(),$optionsArray);
										}
									}	
								}elseif($queType[$DD] == 3){
									$textType=1;
									 $options[$DD] = $this->input->post('option'.$DD.'[]');
									if($options[$DD]){
										$optns=$options[$DD];
										foreach($optns as $opt){
										$optionsArray = array('option' => trim($opt),
														  'que_id' => $queArray['que_id'],
														  'is_answer' => 1,
														  'textType' =>$textType,
														  'isActive' =>1,
														  'isDeleted'=>0,
									);	
								$this->insertOrUpdate(TBL_QUE_OPTIONS,$where=array(),$optionsArray);
								}
						   }
							
						}else{
	                     		$textType=1;
								$options[$DD] = $this->input->post('option'.$DD.'[]');
								/***Adding images for options***/
								
								if($options[$DD] || isset($_FILES['option'.$DD])){
										$SavingOptionImages='';
										$optionImages[$DD]=$_FILES['option'.$DD];
										$optimageNames=$optionImages[$DD]['name'];
										$optionsMime=$_FILES['option'.$DD]['type'];
							            $h=0;$z=0;$d=1;$r=0;
										$opt[$z]="";
										foreach($optimageNames as $name){
										$opt[$r]="";
											if(!empty($name)){
												$mime=$optionsMime[$h];
												if(strstr($mime, "video/")){
												     $filetype = "video";
													 $textType=3;
												}else if(strstr($mime, "image/")){
													  $filetype = "image";
													  $textType=2;
												}else if(strstr($mime, "audio/")){
													  $filetype = "audio";
													  $textType=4;
												}else{
													  $textType=1;
												 } 
													$file_extension=pathinfo($name);
													$rand=rand();
											        $picname=$rand.time().'.'.strtolower($file_extension['extension']);
											        $icon_target_path = './uploads/options/';
											        $icon_target_path = $icon_target_path . basename($picname);
											        $movefile=move_uploaded_file($optionImages[$DD]['tmp_name'][$z], $icon_target_path);
													$opt[$r]=$picname;
											}
											if($options[$DD][$z])
											{
												 $opt[$r]=$options[$DD][$z];
												 $textType=1;
											}
										
											$que_ans = $this->input->post('que'.$DD.$d.'ans');
											$is_answer = 0;
											if($que_ans == "on")
											{  
												$is_answer = 1;
											}
											
											if(!empty($opt[$r])){ 
											$optionsArray = array('option' => trim($opt[$r]),
																  'que_id' => $queArray['que_id'],
																  'is_answer' => $is_answer,
																  'textType' => $textType,
																  'isActive' =>1,
																  'isDeleted'=>0,
											                );
															
											$this->insertOrUpdate(TBL_QUE_OPTIONS,array(),$optionsArray);
											}
											  $d++; $h++; @$z++;
									    }
									   
									
								}
							
						}							
									
						}
						$i++;
						$que=$ID;
						$where=array('id'=>$ID);
						$questionDetails=$this->getSingleRecord(TBL_QUETIONS,$where,"*");
						if($questionDetails){
							$where=array('que_id'=>$que,'isDeleted'=>0);
							$testsIds=$this->getAllRecords(TBL_MAIN_TEST_QUESTIONS,$where,'test_id');
							if($testsIds){
								foreach($testsIds as $tIds){
									$testArray[]=$tIds->test_id;
								} 
								$update=$this->updatequestions($que,$questionDetails,$testArray);
								//if($update){
									$testIdsstring=implode(',',$testArray);
									 $copyOptions=$this->Questionmodel->copyOptions($que); 
									$sql="DELETE FROM oes_que_options WHERE que_id=$que and test_id in ($testIdsstring)"; 
									$this->db->query($sql); 
									foreach($testArray as $tstId){
			                     if($copyOptions){
				                foreach($copyOptions as $copy){
								$copydata['option_id']=$copy->option_id;
								$copydata['option']=$copy->option;
								$copydata['textType']=$copy->textType;
								$copydata['is_answer']=$copy->is_answer;
								$copydata['que_id']=$que;
								$copydata['isActive']=1;
								$copydata['isDeleted']=0;
								$copydata['createdTime']=date('y-m-d h:i:s');
								$copydata['test_id']=$tstId; 
								$where=array();
								$ress=$this->insertOrUpdate(TBL_MAIN_TEST_OPTIONS,$where,$copydata);// neatPrintAndDie($this->db->last_query());
								}
						}
			         }
								
				//}
								
			}
	     }//end stuff
						}
					
				}
				
			  if($queId)
			  {
				   $data['successMessage'] = SUCCESS;
				   redirect(QUESTION_BOOK_URL,'refresh');
			   }else{
				   $data['errorMessage'] = ERROR;
			  }
			}
			$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$tests = $this->getAllRecords(TBL_TESTS,$where,'*');
			$data['tests']=$tests;
			$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$subjects = $this->getAllRecords(TBL_SUBJECTS,$where,'*');
			$topics = $this->getAllRecords(TBL_TOPICS,$where,'*');
			$data['subjects']=$subjects;
			$data['topics']=$topics;
			if($this->session->userdata('resellerId')){
				$resellerId=$this->session->userdata('resellerId');
				}else{
					$resellerId=0;
				}
			$Quedata = $this->getAllRecords(TBL_QUETIONS, array('id'=>$queid,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId),'*');//on 18 th
			
			//$Quedata = $this->getAllRecords(TBL_QUETIONS, array('id'=>$queid,'isActive'=>1,'isDeleted'=>0),'*');
			if($Quedata)
			{
				$i=0;
				foreach($Quedata as $Que)
				{
					$where=array('que_id'=>$Que->id,'isActive'=>1,'isDeleted'=>0);
					$options = $this->getAllRecords(TBL_QUE_OPTIONS,$where,'*');
					$Quedata[$i++]->options = $options;
				}
			}
			$data['Quedata']  = $Quedata; 
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/questionBook/edit_test',$data);
		   			$extraFooter = $this->load->view('dashboard/users_script',$data);
		    $this->load->view('dashboard/includes/footer',$extraFooter);
			
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 	
	}
	function updatequestions($queId,$questionDetails,$testArray){
		//neatPrintAndDie($questionDetails);
		$que=$questionDetails->que;
		$hint=$questionDetails->hint;
		$queType=$questionDetails->queType;
		if($testArray){ $test_id=implode(',',$testArray);
			$sq="UPDATE oes_quetions SET que='$que',hint='$hint',queType='$queType' WHERE que_id=$queId and test_id in ($test_id) ";
			$res=$this->db->query($sq);
			return $res;
		}
		
	}

	/***** 
function used to upload questions from excel sheet
 ****/	
function uploadQueFromExcel()
{    
	   $isSessionIn=$this->session->userdata('isSessionIn');
	  if(empty($isSessionIn))
	  {
	    redirect(LOGOUT_URL,'refresh');
	  }
	  $test=$this->uri->segment(4);
      $data='';   
	  $error_message='';
      $this->load->library('form_validation');
	    if(empty($_FILES['excelUsersFile']['name'])){
	        $this->form_validation->set_rules('excelUsersFile','Excel File','trim|required');
	      }
	      $this->form_validation->set_rules('excelUsersFile','Excel file','trim');
	if($this->form_validation->run()!=false)
	{
			$result=0;
			$target_path ='./uploads/';
			$rand=rand();
			$xlsx_name=$rand.time().'.xlsx'; 
			$fileTypes = array('xlsx');
			if(!empty($_FILES['excelUsersFile']['name'])){ 
				   $target_path = $target_path . $xlsx_name;
			       $response['file_name'] = basename($_FILES['excelUsersFile']['name']);
			       $filename=basename($_FILES['excelUsersFile']['name']);
			       $file_extension=pathinfo($_FILES['excelUsersFile']['name']);
				
			if (in_array(strtolower($file_extension['extension']), $fileTypes)) {
				 $movefile=move_uploaded_file($_FILES['excelUsersFile']['tmp_name'], $target_path);
			   if($movefile)
				{                 
					try {
					      $objPHPExcel = PHPExcel_IOFactory::load($target_path);
					} catch(Exception $e) {
					     die('Error loading file "'.pathinfo($target_path,PATHINFO_BASENAME).'": '.$e->getMessage());
					}
					$allUsersInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
					$usersCount = count($allUsersInSheet);
					if($usersCount<=1){ 
						 $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please select valid excel sheet.</strong> </div>");
						 redirect(ADD_ECXEL_QUESTIONS_URL);
					}
					$x=1;$y=2;
					$where=array();
					$testQuestions=$this->Questionmodel->getLastQuestionNumbers(TBL_QUETIONS,$where,'*');
					if($testQuestions){
					   $lastQuestion=$testQuestions->id;
					}else{
					   $lastQuestion=0;
					}
					$x=$lastQuestion+1;$k=0; $l=1;
					for($i=2;$i<=$usersCount;$i++){
							$subId="";$topic ="";
						$subId = trim($allUsersInSheet[$i]["A"]);
						$QuesNo =$l;
						$Quetion = trim($allUsersInSheet[$i]["C"]);
						//$QuesNo = trim($allUsersInSheet[$i]["B"]);
						$topic = trim($allUsersInSheet[$i]["D"]);
						if($subId && $topic){
						$topicExists=$this->getSingleRecord(TBL_TOPICS,$where=array('topic_id'=>$topic,'subject_id'=>$subId,'isActive'=>1,'isDeleted'=>0),"*");
						$subExists=$this->getSingleRecord(TBL_SUBJECTS,$where=array('id'=>$subId),"*"); //neatPrintAndDie( count($subExists));
						if(count($subExists)<=0 || count($topicExists)<=0){
							if(count($subExists) <=0 && count($topicExists)<=0){
								$NotUploadedReason[$k]="Que. ".$QuesNo." was not uploaded because this subject not exists.";
							}else if(count($subExists) <=0 && count($topicExists)>0 ){
								$NotUploadedReason[$k]="Que. ".$QuesNo." was not uploaded because this subject not exists.";
							}else if(count($subExists) > 0 && count($topicExists)<=0 ){
								$NotUploadedReason[$k]="Que. ".$QuesNo." was not uploaded because this  topic not exists for this subject.";
							}
							$questionNotUplOADED[]=$QuesNo; 
						}else{
						$subId = trim($allUsersInSheet[$i]["A"]);
						$QuesNo = trim($allUsersInSheet[$i]["B"]);
						$Quetion = trim($allUsersInSheet[$i]["C"]); 
						$topic = trim($allUsersInSheet[$i]["D"]);
						$Option1 = trim($allUsersInSheet[$i]["E"]);
						$Option2 = trim($allUsersInSheet[$i]["F"]);
						$Option3 = trim($allUsersInSheet[$i]["G"]);
						$Option4 = trim($allUsersInSheet[$i]["H"]);
						$Option5 = trim($allUsersInSheet[$i]["I"]);
						$Option6 = trim($allUsersInSheet[$i]["J"]);
						$Option7 = trim($allUsersInSheet[$i]["K"]);
						$Option8 = trim($allUsersInSheet[$i]["L"]);
						$Option9 = trim($allUsersInSheet[$i]["M"]);
						$Option10 = trim($allUsersInSheet[$i]["N"]);
						$Option11 = trim($allUsersInSheet[$i]["O"]);
						$Option12 = trim($allUsersInSheet[$i]["P"]);
						$Ans = trim($allUsersInSheet[$i]["Q"]);
						$hint = trim($allUsersInSheet[$i]["R"]);
						$images = trim($allUsersInSheet[$i]["S"]); 
						$imagePosition = trim($allUsersInSheet[$i]["T"]);
						$Ans = str_replace(' ', '', $Ans);	
						$QuesNo =$l;
						if($Quetion)
						{   
						 $result = $this->Questionmodel->saveQue($Quetion,$hint,$subId,$topic,$images,$imagePosition);
							/* $questionNotUplOADED[]=$QuesNo; 
							$NotUploadedReason[]=$QuesNo." was not uploaded due to empty question.";
						break; */
						}else{
							$questionNotUplOADED[]=$QuesNo; 
							$NotUploadedReason[$k]="Que. ".$QuesNo." was not uploaded due to empty question.";
						}
						
						if($result){
						
					   $queId =$result;	
					   $answers= explode("#", $Ans);	
					if($queId){                               	
					if(!empty($Option1))
						{
							
							$isOption=0;
							$number1=1;
							if(in_array($number1,$answers))
							{
							$isOption=1;
							}
							$re=$this->Questionmodel->saveQueOptions($Option1,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 1st option was not uploaded due to ".$re;
							}
						}
						
						if(!empty($Option2))
						{
							$isOption=0;
							$number2=2;
							if(in_array($number2,$answers))
							{
							$isOption=1;
							}
							$re=$this->Questionmodel->saveQueOptions($Option2,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 2nd option was not uploaded due to ".$re;
							} 
						}
						if(!empty($Option3))
						{
							
							$isOption=0;
							$number3=3;
							if(in_array($number3,$answers))
							{
							$isOption=1;
							}
							$re=$this->Questionmodel->saveQueOptions($Option3,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 3rd option was not uploaded due to ".$re;
							} 
						}
						
						//option 4
						if(!empty($Option4))
						{
							 
							$isOption=0;
							$number4=4;
							if(in_array($number4,$answers))
							{
							$isOption=1;
							}
							$re=$this->Questionmodel->saveQueOptions($Option4,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 4th option was not uploaded due to ".$re;
							} 
						}
						//option 5
						if(!empty($Option5))
						{
							$isOption=0;
							$number5=5;
							if(in_array($number5,$answers))
							{
							$isOption=1;
							}
							$re=$this->Questionmodel->saveQueOptions($Option5,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 5th option was not uploaded due to ".$re;
							}  
						}
						//option 6
						if(!empty($Option6))
						{
							
							$number6=6;
							$isOption=0;
							if(in_array($number6,$answers))
							{
							$isOption=1;
							}
							$re=$this->Questionmodel->saveQueOptions($Option6,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 6th option was not uploaded due to ".$re;
							} 
						}
						
						//option 7
						if(!empty($Option7))
						{
							$number7=7;
							$isOption=0;
							if(in_array($number7,$answers))
							{
							$isOption=1;
							}
							$re=$this->Questionmodel->saveQueOptions($Option7,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 7th option was not uploaded due to ".$re;
							} 
						}
						//option 8
						if(!empty($Option8))
						{
							$isOption=0;$number8=8;
							if(in_array($number8,$answers))
							{
							$isOption=1;
							}
							$re=$this->Questionmodel->saveQueOptions($Option8,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 8th option was not uploaded due to ".$re;
							} 
						}
						
						//option 9
						if(!empty($Option9))
						{
							$isOption=0;$number9=9;
							if(in_array($number9,$answers))
							{
							$isOption=1;
							}
							$this->Questionmodel->saveQueOptions($Option9,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 9th option was not uploaded due to ".$re;
							} 
						}
						
						//option 10
						if(!empty($Option10))
						{
							$isOption=0;$number10=10;
							if(in_array($number10,$answers))
							{
							$isOption=1;
							}
							$this->Questionmodel->saveQueOptions($Option10,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 10th option was not uploaded due to ".$re;
							} 
						}
						//option 11
						if(!empty($Option11))
						{
							$isOption=0;$number11=11;
							if(in_array($number11,$answers))
							{
							$isOption=1;
							}
							$this->Questionmodel->saveQueOptions($Option11,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 11th option was not uploaded due to ".$re;
							}  
						}
						//option 12
						if(!empty($Option12))
						{
						$isOption=0;$number12=12;
						if(in_array($number12,$answers))
						{
						$isOption=1;
						}
						$re=$this->Questionmodel->saveQueOptions($Option12,$isOption,$queId);
							if($re == "1"){
							}else{
								$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - 12th option was not uploaded due to ".$re;
							} 
						}
					}else{
						$NotUploadedReason[$k]->optionErrors[]="Que. ".$QuesNo." - options was not uploaded due to empty question id.";
					}
						
						$data['messege']="success";
						}else{
							
							$data['error_messege']="Error";
						}
						 }
						}
						else{
							$QNumber = trim($allUsersInSheet[$i]["B"]);
							if($QNumber){
							$questionNotUplOADED[]=$QuesNo;
							if(empty($subId ) && empty($topic)){
								$NotUploadedReason[$k]="Que. ".$QuesNo." was not uploaded due to empty id's for subject and topic.";
							}else if(empty($topic)){
								$NotUploadedReason[$k]="Que. ".$QuesNo." was not uploaded due to empty id's for topic.";
							}else if(empty($subId)){
								$NotUploadedReason[$k]="Que. ".$QuesNo." was not uploaded due to empty id's for subject.";
							}
							
							}
							//else{
							//	$NotUploadedReason[$k]="Que. ".$QuesNo." was not uploaded due to empty question.";
							//}
						}
						
						
						$y++;$k++;$l++;
						}
					 if($questionNotUplOADED && count($questionNotUplOADED)>0){
						   $questionNotUplOADED=array_unique($questionNotUplOADED);
						   $str=implode(',',$questionNotUplOADED);
						   $str = rtrim($str, ',');
						   $mes="question ".$str ." was not imported ";
						 if($NotUploadedReason){
						 foreach($NotUploadedReason as $nUr){ 
						 
						  $optErr="";
							  if(count(@$nUr->optionErrors)>0){
									$optErr="";
								  $optionErrors=$nUr->optionErrors;
								  for($m=0;$m<count($optionErrors);$m++){
									$optErr.="<p>".$optionErrors[$m]."</p>"; 
								  }
							  }
							 $mes.="<br><p>".$nUr."</p>"; 
							 $mes.="<p>".$optErr."</p>"; 
						 } 
						 
						 }
						  $messege =array('status'=>0,'message'=>$mes);
						 $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".$mes."</strong> </div>");
						 redirect(ADD_ECXEL_QUESTIONS_URL);
					 }else{
						 $mes=SUCCESS;
						 $messege =array('status'=>0,'message'=>$mes);
						 $this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".$mes."</strong> </div>");
					     redirect(ADD_ECXEL_QUESTIONS_URL);
					 }
			
			  }
			  else
			  {
				 $error_message =array('status'=>0,'message'=>FILE_NOT_UPLOADED);
				 $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".FILE_NOT_UPLOADED."</strong> </div>");
				 redirect(ADD_ECXEL_QUESTIONS_URL);
				
			   }
			}else{
				 $error_message =array('status'=>0,'message'=>MODULE_FILE_ALLOWED_TYPES);
				 $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Please upload .xlsx files.</strong> </div>");
				 redirect(ADD_ECXEL_QUESTIONS_URL);
			
			}
	 }
		 
	}else{
	    $message = array('status' => 0,'message' =>validation_errors(),'result' => '');
    }
	    $this->load->view('dashboard/includes/header');
	    $this->load->view('dashboard/tests/add_excel_questions',$data);
	    $extraFooter = $this->load->view('dashboard/users_script');
	    $this->load->view('dashboard/includes/footer',$extraFooter);
	
	}
	function confirm_selected_quens(){
		$test=$_GET['test'];
		$where=array('test_id'=>$test,'isActive'=>1,'isDeleted'=>0,'isApproved'=>0);
		$result=$this->getAllRecords(TBL_TEST_QUESTIONS,$where,'*');
		
		if($result){
			foreach($result as $res){
				$que_id=$res->que_id;
				$where=array('id'=>$que_id);
				$questions=$this->getSingleRecord(TBL_QUETIONS,$where,'*');
				$res->question=$questions->que;
				$res->subject=$questions->subject_id;
				$res->topic=$questions->topic_id;
				$res->queType=$questions->queType;
			}
			
		}
 
		 $data['questions']=$result;
             if($this->session->userdata('resellerId')){
			      $resellerId=$this->session->userdata('resellerId');
			  }else{
				  $resellerId=0;
			  } 
        $where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
        $subjects=$this->getAllRecords(TBL_SUBJECTS,$where,'*');
        $topics=$this->getAllRecords(TBL_TOPICS,$where,'*');
        $data['subjects']=$subjects;
        $data['topics']=$topics;//neatPrintAndDie($data);
		$this->load->view('dashboard/includes/header');
		$this->load->view('dashboard/tests/approve_questions',$data);
        $extraFooter = $this->load->view('dashboard/users_script');
	    $this->load->view('dashboard/includes/footer',$extraFooter);
		
	}
	
	 function add_test_question(){
		$test=$_GET['test'];
		$where=array('testId'=>$test);
		$testDetails=$this->getSingleRecord(TBL_TESTS,$where,'*');
		$totalQue=$testDetails->totalQuestions;
		$totalQuestions=$this->getAllRecords(TBL_MAIN_TEST_QUESTIONS,$where=array('isActive'=>1,'isDeleted'=>0,'test_id'=>$test));
		if($totalQuestions && count($totalQuestions)>0){
				$quesNo=count($totalQuestions);
			}
			else{
				$quesNo=0;
			} 
			
		
		
		
		$where=array('test_id'=>$test,'isActive'=>1,'isDeleted'=>0,'isApproved'=>0);
		$ApprovedQuestions=$this->getAllRecords(TBL_TEST_QUESTIONS,$where,'*');
		$testSelectQuestion=$quesNo+count($ApprovedQuestions);
			if($testSelectQuestion > $totalQue){ 
				$this->session->set_flashdata('Error',"<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> You have selected more questions than Total Queston(".$totalQue.") of test.</strong> </div>");
				// neatPrintAndDie(current_url()); 
				redirect(CONFIRM_TEST_QUESTIONS.'?test='.$test);
				 //echo json_encode(array('status'=>0,'message'=>"You have selected more questions then Total Queston(".$totalQue.") of test."));die(); 
			}
		if($ApprovedQuestions){ 
			foreach($ApprovedQuestions as $aquestions){
		  //newww
		  $where=array('test_id'=>$test);
		  $testQuestions=$this->Questionmodel->getLastQuestionNumbers(TBL_MAIN_TEST_QUESTIONS,$where,'*');
		 // neatPrintAndDie($testQuestions);
			if($testQuestions){
				$lastQuestion=$testQuestions->que_id;
				$lastQuestion=$lastQuestion+1;
			}else{
				$lastQuestion=1;
			}
		    $createdTime=date('y-m-d h:i:s');
			$que=$aquestions->que_id;
		    $sql="insert into oes_quetions(que,test_id,que_id,hint,queType,isActive,isDeleted,createdTime ) 
		    select que,'$test','$que',hint,queType,isActive,isDeleted,'$createdTime' from oes_quetions_total where (id= $que )";
			$res=$this->db->query($sql);
			
			 if($res){
			 $result=$this->Questionmodel->copyOptions($que);
			  //neatPrintAndDie($result);
			if($result){
				foreach($result as $res){
					$data['option_id']=$res->option_id;
					$data['option']=$res->option;
					$data['textType']=$res->textType;
					$data['is_answer']=$res->is_answer;
					$data['test_id']=$test;
					$data['que_id']=$que;
					//$data['que_id']=$lastQuestion;
					$data['isActive']=1;
					$data['isDeleted']=0;
					$data['createdTime']=date('y-m-d h:i:s');
					$where=array();
					$this->insertOrUpdate(TBL_MAIN_TEST_OPTIONS,$where,$data);
				}
			}
			}
		//end
		}
	}
	$where=array('test_id'=>$test);
		$adata['isApproved']=1;
		$result=$this->insertOrUpdate(TBL_TEST_QUESTIONS,$where,$adata);
		if($result){
		 $this->session->set_flashdata('Error',"<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> Question added successfully</strong> </div>");
		 redirect(QUETION_MANAGEMENT_URL);
		}
	}
	

}
?>