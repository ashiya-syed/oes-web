<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Libraries extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Couponmodel'); 
		$this->load->model('Commonmodel');
	}
	
	
	function index()
	{
		
		try{
			if($this->session->userdata('resellerId')){
				$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
			
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
            $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$data['packages'] = $this->getAllRecords(TBL_PACKAGES,$where,'*');//print_r($this->db->last_query());die();
		//$data['libraries'] = $this->getAllRecords(TBL_LIBRARIES,$where=array('isDeleted'=>0,'resellerId'=>$resellerId),"*");
		$data['libraries'] = $this->getAllRecordsByDesc(TBL_LIBRARIES,$where=array('isDeleted'=>0,'resellerId'=>$resellerId),'id',"*");
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/libraries/libraries',$data);
			$extraFooter = $this->load->view('dashboard/users_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	}
	function addLibrary(){
		
		$pack=$_POST['drop'];
	        if(empty($pack)) { 
			$error_message =array('status'=>0,'message'=>"please select package");
			 echo json_encode($error_message);die();
			}else{
				    $package=json_decode($pack,TRUE);
				 $package = implode(',', $package);
			   }
			   
 /* if(count($pack)<1){			   
				   $error_message =array('status'=>0,'message'=>"please select package");
				  echo json_encode($error_message);die();
			   } */
		if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
		}else{
			$resellerId=0;
		}
		 if(empty($_FILES['library_file']['name'])){
				$error_message =array('status'=>0,'message'=>"provide  library file");
				echo json_encode($error_message);die();
		}
		if(empty($_POST['library_name'])){
				$error_message =array('status'=>0,'message'=>"provide  library Name");
				echo json_encode($error_message);die();
		}
		
        $library_name=$_POST['library_name'];
		$where=array('library_name'=>$library_name,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
		$exists=$this->getAllRecords(TBL_LIBRARIES,$where,'*'); //neatPrintAndDie($this->db->last_query());
		if($exists){
				$error_message =array('status'=>0,'message'=>"Library existed with this name.Try another name.");
				echo json_encode($error_message);die();
		}
	    $target_path ='./uploads/libraries/';
	    $fileTypes = array('jpeg', 'png', 'jpg','gif','mp3','mp4','wma','doc','docx', 'txt', 'pdf');
			if(!empty($_FILES['library_file']['name'])){
			  //$target_path = $target_path . basename($_FILES['library_file']['name']);
			  $response['file_name'] = basename($_FILES['library_file']['name']);
			  $filename=basename($_FILES['library_file']['name']);
			  $optionsMime=$_FILES['library_file']['type'];// neatPrintAndDie($optionsMime);
			  

			  //if(!empty($name)){
				   $textType="";
						$mime=$optionsMime;
						if(strstr($mime, "video/")){
							$filetype = "video";
							$textType='video';
							}else if(strstr($mime, "image/")){
							$filetype = "image";
							$textType='image';
							}else if(strstr($mime, "audio/")){
							$filetype = "audio";
							$textType='audio';
							}else if(strstr($mime, "text/")){
							$filetype = "audio";
							$textType='text';
							}else if(strstr($mime, "pdf/")){
							$filetype = "pdf";
							$textType='pdf';
							}else if(strstr($mime, "application/pdf")){
							$filetype = "pdf";
							$textType='pdf';
							}else if(strstr($mime, "application/octet-stream")){
							$filetype = "doc";
							$textType='doc';
							}
			  $rand=rand();
			  $file_extension=pathinfo($_FILES['library_file']['name']); //neatPrintAndDie($file_extension);
			   $picname=$rand.time().'.'.strtolower($file_extension['extension']); 
			   $target_path = $target_path . $picname;
			    if (in_array(strtolower($file_extension['extension']), $fileTypes)) {
					$movefile=move_uploaded_file($_FILES['library_file']['tmp_name'], $target_path);
				    if($movefile){
						$message = array('status' => 1,'message' => 'success','result' => $movefile);
						$libraryData['library_name']=$library_name;
						$libraryData['library_file']=$picname;
						$libraryData['resellerId']=$resellerId;
						$libraryData['packageId']=$package;
						$libraryData['fileType']=$textType;//qb 
						//neatPrintAndDie($libraryData);
						$where=array();
						$this->insertOrUpdate(TBL_LIBRARIES,$where,$libraryData);
						//neatPrintAndDie($this->db->last_query());
						echo json_encode(array('status' => 1,'message' => ADDING_SUCCESS,'result' => $movefile));die();
			          
				     }else{
					    $error_message =array('status'=>0,'message'=>FILE_NOT_UPLOADED);
						echo json_encode($error_message);die();
						
					 }
				}else{
						$error_message =array('status'=>0,'message'=>MODULE_FILE_ALLOWED_TYPES);
						echo json_encode($error_message);die();
						
			   }
			}
    
	}
	function deleteLibrary()
	{
	
		if($this->input->is_ajax_request())
		{
			$id = $this->input->post('id');
			$where = array('id'=>$id);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_LIBRARIES,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	function activateLibrary()
	{
		if($this->input->is_ajax_request())
		{
			$libraryId = trim($this->input->post('libraryId'));
			$id     = trim($this->input->post('id'));
			$where = array('id'=>$libraryId);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_LIBRARIES,$where,$data);
			$record = $this->getSingleRecord(TBL_LIBRARIES,$where,'id,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateLibrary($libraryId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateLibrary($libraryId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	function editLibrary()
	 {
		  $libraryId= trim($this->input->post('libraryId'));
		   $where = array('id' => $libraryId);
		  $details=$this->getSingleRecord(TBL_LIBRARIES,$where);
		  $data['details']=$details;
		  if($this->session->userdata('resellerId')){
				$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
			$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$data['packages'] = $this->getAllRecords(TBL_PACKAGES,$where,'*');//print_r($this->db->last_query());die();
			
		  
		  $this->load->view('dashboard/libraries/edit_libraries_ajax',$data);
	} 
	function updateLibrary(){
		$pack=$_POST['drop'];
		if(empty($pack)) { 
			 $error_message =array('status'=>0,'message'=>"please select package");
			 echo json_encode($error_message);die();
			}else{
				    $package=json_decode($pack,TRUE);
				    $package = implode(',', $package);
			   }
		$id=$_POST['id'];
		if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
		}else{
			$resellerId=0;
		}
		
		if(empty($_POST['updatelibrary_name'])){
				$error_message =array('status'=>0,'message'=>"provide  library name");
				echo json_encode($error_message);die();
		}
		 if(empty($_FILES['updatelibrary_file']['name']) && empty($_POST['updatelibrary_name'])){
				$error_message =array('status'=>0,'message'=>"Please fill all fields.");
				echo json_encode($error_message);die();
		}
		
		
        $library_name=$_POST['updatelibrary_name'];
		$where=array('library_name'=>$library_name,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
		$exists=$this->Commonmodel->libraryExists(TBL_LIBRARIES,$where,$id);
		if($exists && count($exists)>0){
				$error_message =array('status'=>0,'message'=>"Library existed with this name.Try another name.");
				echo json_encode($error_message);die();
		}
		//if($_FILES){
	    $target_path ='./uploads/libraries/';
 	    $fileTypes = array('jpeg', 'png', 'jpg','gif','mp3','mp4','wma','doc','docx', 'txt', 'pdf');
			if(!empty($_FILES['updatelibrary_file']['name'])){
				
				$optionsMime=$_FILES['updatelibrary_file']['type'];
				//neatPrintAndDie($optionsMime);
			  //if(!empty($name)){
				  $textType="";
						$mime=$optionsMime;
						if(strstr($mime, "video/")){
							$filetype = "video";
							$textType='video';
							}else if(strstr($mime, "image/")){
							$filetype = "image";
							$textType='image';
							}else if(strstr($mime, "audio/")){
							$filetype = "audio";
							$textType='audio';
							}else if(strstr($mime, "text/")){
							$filetype = "audio";
							$textType='text';
							}else if(strstr($mime, "pdf/")){
							$filetype = "pdf";
							$textType='pdf';
							}else if(strstr($mime, "application/pdf")){
							$filetype = "pdf";
							$textType='pdf';
							}else if(strstr($mime, "application/octet-stream")){
							$filetype = "doc";
							$textType='doc';
							}
				//neatPrintAndDie($textType);
				
			  $response['file_name'] = basename($_FILES['updatelibrary_file']['name']);
			  $filename=basename($_FILES['updatelibrary_file']['name']);
			  $file_extension=pathinfo($_FILES['updatelibrary_file']['name']);
			   $rand=rand();
			  $file_extension=pathinfo($_FILES['updatelibrary_file']['name']);
			   $picname=$rand.time().'.'.strtolower($file_extension['extension']); 
			   $target_path = $target_path . $picname;
			    if (in_array(strtolower($file_extension['extension']), $fileTypes)) {
					$movefile=move_uploaded_file($_FILES['updatelibrary_file']['tmp_name'], $target_path);
				    if($movefile){
						//$message = array('status' => 1,'message' =>ADDING_SUCCESS,'result' => $movefile);
						$libraryData['library_name']=$library_name;
						$libraryData['library_file']=$picname;
						$libraryData['resellerId']=$resellerId;
						$libraryData['packageId']=$package;
						$libraryData['fileType']=$textType;

						$where=array('id'=>$id);
						$this->insertOrUpdate(TBL_LIBRARIES,$where,$libraryData);
						
						echo json_encode(array('status' => 1,'message' => 'Success','result' => $movefile));die();
			          
				     }else{
					    $error_message =array('status'=>0,'message'=>FILE_NOT_UPLOADED);
						echo json_encode($error_message);die();
						
					 }
				
				}else{
						$error_message =array('status'=>0,'message'=>MODULE_FILE_ALLOWED_TYPES);
						echo json_encode($error_message);die();
						
			   }
			 }else{
				        $libraryData['library_name']=$library_name;
						$libraryData['resellerId']=$resellerId;
						$libraryData['packageId']=$package;
						$where=array('id'=>$id);
						$this->insertOrUpdate(TBL_LIBRARIES,$where,$libraryData);
						echo json_encode(array('status' => 1,'message' => 'Success'));die();
			          
			}
	}

	
}
?>