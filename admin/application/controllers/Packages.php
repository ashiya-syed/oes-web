<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Packages extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Packagemodel');
	}
	
	
	function index()
	{
		
		try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			/*$Role_Id=$this->session->userdata('Role_Id');
			if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
			{
				redirect(ADMIN_LOGIN_URL,'refresh');
			}*/
			
			if($this->session->userdata('resellerId')){
			$where = array('isDeleted'=>0,'resellerId'=>$resellerId);	
			}else{
				$where = array('isDeleted'=>0,'resellerId'=>0);
			}
			
			$fpackages=array();
			//$packages = $this->getAllRecords(TBL_PACKAGES,$where,'*');
			$packages = $this->getAllRecordsByDesc(TBL_PACKAGES,$where,'package_id','*');
			if($this->session->userdata('isReseller') == 2 && $resellerId ){
			$packages = $this->Packagemodel->getPromoterPackages($resellerId); 
			}
			
			if($packages && count($packages)>0)
			{
				$i = 0;
				foreach($packages as $package)
				{
					$package->course="-";
					$ids= explode(',', $package->package_test);
					$packageTests = $this->Packagemodel->getalltestsByPackage($ids);
					$packages[$i++]->packageTests = $packageTests;
					if($package->courseId){
					$course=$this->getSingleRecord(TBL_COURSES,$where=array('courseId'=>$package->courseId),'courseName');
					$package->course = $course->courseName;
					}
					$fpackages[]=$package;
				}
				
			}
			   if($this->session->userdata('resellerId')){
				    $resellerId=$this->session->userdata('resellerId');
			   }else{
				     $resellerId=0;
			   }
			
			$data['packages'] = $fpackages;
			 $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$data['tests'] = $this->getAllRecords(TBL_TESTS,$where,'*');
			
			$this->load->view('dashboard/includes/header');
			if($this->session->userdata('isReseller') == 2 && $this->session->userdata('resellerId') ){
				
				$this->load->view('dashboard/packages/promoter_packages',$data);
            }else{
			    $this->load->view('dashboard/packages/packages',$data);
             }

			$extraFooter = $this->load->view('dashboard/packages/packages_script');
		    $this->load->view('dashboard/packages/footer');
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	}
	
	function addPackage()
	{
		 try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			/*$Role_Id=$this->session->userdata('Role_Id');
			if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
			{
				redirect(ADMIN_LOGIN_URL,'refresh');
			}*/
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			       $where=array('reseller_id'=>$resellerId); 
				   $resellerDetails=$this->getSingleRecord(TBL_RESELLERS,$where,'*');
				   if($resellerDetails){
					    $packages_limit=$resellerDetails->reseller_packageLimit;
					   
				   }else{
					   $packages_limit=$this->session->userdata('packages_limit');
				   }
			      // $packages_limit=$this->session->userdata('packages_limit');
				   $where=array('isDeleted'=>0,'resellerId'=>$resellerId);
				   //$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				   $packCount=$this->getAllRecords(TBL_PACKAGES,$where,'*');
				   //neatPrintAndDie($packCount);
				  
				   if($packCount && count($packCount)>= $packages_limit){
				    $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>You already exceed your Packages limit.For further information contact admin.</strong> </div>");
			       redirect($_SERVER['HTTP_REFERER']);
				   } 
			}else{
				$resellerId=0;
			}
		    $data = array();
		    if(isset($_POST['AddPackage']))
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('packageName',' Package Name','trim|required');
				$this->form_validation->set_rules('course','course','trim|required');
				$this->form_validation->set_rules('packageType','Package Type','trim|required');
				$this->form_validation->set_rules('test[]','Tests','trim|required');
				if($this->form_validation->run()!=false)
				{
					$course   = trim($this->input->post('course'));
					$packageName   = trim($this->input->post('packageName'));
					$packageType   = trim($this->input->post('packageType'));
					$packageAmount = trim($this->input->post('packageAmount'));
					if($packageType=="paid"){
						if(empty($packageAmount)){ 
							$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package Amount is required.</strong> </div>");
			            redirect(ADD_PACKAGE_URL);
						}
					}
					
					
					 $where=array('package_name'=>$packageName,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
					 $ifPackageNameExists=$this->getAllRecords(TBL_PACKAGES,$where,'*');
				     if($ifPackageNameExists && count($ifPackageNameExists)>0){
				        $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package Name Already existed.Please try with another Name.</strong> </div>");
			            redirect(current_url());
					 }
					 //new
					 $startdate = trim($this->input->post('startdate'));
					 $enddate = trim($this->input->post('enddate')); 
					  if(strtotime($enddate) <= strtotime($startdate)){
				        $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package Validity ending date must be greater than start date.</strong> </div>");
			            redirect(current_url());
					 }
					 if($this->input->post('packageAmount')){
					$packageAmount = trim($this->input->post('packageAmount'));
					 }else{$packageAmount = 0;}
					 if($packageType=="paid"){
						 $packageType=1;
					 }else{
						 $packageType=0;
					 }
					$tests = $this->input->post('test[]');
					$tests = implode(",", $tests);
					$tests = str_replace(" ","", $tests);
					$package_code=generateRandNumber(10);
					$packageData['courseId']   = $course;//29
					$packageData['resellerId']   = $resellerId;//29
					$packageData['package_name']   = $packageName;
					$packageData['package_amount'] = $packageAmount;
					$packageData['isPaidPackage'] = $packageType;
					$packageData['package_code']   = $package_code;
					$packageData['package_test']   = $tests;
					$packageData['package_valid_from']   = date("Y-m-d H:i:s", strtotime($this->input->post('startdate')) );
					$packageData['package_valid_to']   =   date("Y-m-d H:i:s", strtotime($this->input->post('enddate')) );
					//neatPrintAndDie($packageData);
					$result = $this->insertOrUpdate(TBL_PACKAGES,array(),$packageData);
					if($result)
					{
						$data['successMessage'] = SUCCESS;
						redirect(PACKAGES_MANAGEMENT_URL,'refresh');
					}else{
						$data['errorMessage'] = ERROR;
					}
				}else{
					$data['errorMessage'] = validation_errors();
				}
				
			}
			   //new
			   if($this->session->userdata('resellerId')){
				    $resellerId=$this->session->userdata('resellerId');
			   }else{
				     $resellerId=0;
			   }
			   //$where = array('isActive'=>1,'isDeleted'=>0,'testType'=>1,'resellerId'=>$resellerId);
			   $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);//qb
			   $data['tests'] = $this->getAllRecords(TBL_TESTS,$where,'*');
				$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			    $data['courses']=$this->getAllRecords(TBL_COURSES,$where);
				$this->load->view('dashboard/packages/header');
				$this->load->view('dashboard/packages/add_package',$data);
				$extraFooter = $this->load->view('dashboard/packages/packages_script');
				$this->load->view('dashboard/packages/footer',$extraFooter);
		 }catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		 } 
	}
	
	function editPackage($packageId=0)
	{
		try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			/*$Role_Id=$this->session->userdata('Role_Id');
			if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
			{
				redirect(ADMIN_LOGIN_URL,'refresh');
			}*/
			//29
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
		    $data = array();
		    if(isset($_POST['EditPackage']))
			{
				
				$this->load->library('form_validation');
				$this->form_validation->set_rules('packageName',' Package Name','trim|required');
				//$this->form_validation->set_rules('packageAmount','Package Amount','trim|required');
				$this->form_validation->set_rules('test[]','Tests','trim|required');
				$this->form_validation->set_rules('startdate','Package valid from','trim|required');
				$this->form_validation->set_rules('enddate','Package valid to','trim|required');
				$this->form_validation->set_rules('packageType','Package type','trim|required');
				$this->form_validation->set_rules('course','Course','trim|required');
				if($this->form_validation->run()!=false)
				{
					$packageName   = trim($this->input->post('packageName'));
					 $packageType = trim($this->input->post('packageType'));
					  $packageAmount = trim($this->input->post('packageAmount'));
					  
					  
					   $where=array('package_name'=>$packageName,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
					  $ifPackageNameExists=$this->Packagemodel->checkPackageNameExistsInEdit(TBL_PACKAGES,$where,$packageId);
				      if($ifPackageNameExists && count($ifPackageNameExists)>0){
				        $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package Name Already existed.Please try with another Name.</strong> </div>");
			            redirect(PACKAGES_MANAGEMENT_URL);
					 }
					  
					  
					// neatPrintAndDie($packageType);
					 if($packageType == "paid"){ 
						if(empty($packageAmount)){ 
							$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package Amount is required.</strong> </div>");
			               redirect(EDIT_PACKAGES_URL.'/'.$packageId);
						}
					}
					 if($this->input->post('packageAmount')){
					  $packageAmount = trim($this->input->post('packageAmount'));
					 
					 }else{$packageAmount = 0;}
					$tests = $this->input->post('test[]');
					$tests = implode(",", $tests);
					$tests = str_replace(" ","", $tests);
					$startdate = trim($this->input->post('startdate'));
					 $enddate = trim($this->input->post('enddate'));
					 if(strtotime($enddate) <= strtotime($startdate)){
				        $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package Validity ending date must be greater than start date.</strong> </div>");
			            redirect(EDIT_PACKAGES_URL.'/'.$packageId);
					 }
					$packageData['package_valid_from']   = date("Y-m-d H:i:s", strtotime($this->input->post('startdate')) );
					$packageData['package_valid_to']   =   date("Y-m-d H:i:s", strtotime($this->input->post('enddate')) );
					$packageData['package_name']   = $packageName;
					
					$packageData['package_test']   = $tests;
					$packageType = trim($this->input->post('packageType')); //qb
					 
					 if($packageType=="paid"){
						 $packageType=1;
						 $packageData['package_amount'] = $packageAmount;
						 $packageData['isPaidPackage'] = $packageType;
					 }else{
						 $packageType=0;
						 $packageData['package_amount'] = 0;
						 $packageData['isPaidPackage'] = $packageType;
					 }
					 if($this->input->post('course')){
					    $packageData['courseId']   = $this->input->post('course'); 
                        }
					
					$where = array('package_id'=>$packageId);
					$packageCodeExists=$this->getSingleRecord(TBL_PACKAGES,$where,'*');
				     if(empty($packageCodeExists->package_code)){
					    $packageData['package_code'] = generateRandNumber(10);
				     }
					$result = $this->insertOrUpdate(TBL_PACKAGES,$where,$packageData);
					if($result)
					{
						if($packageAmount){
						$this->updateCartAmount($packageAmount,$packageId);
						}
						$where = array('packageId'=>$packageId);
						$userpackageData['packageTests']=$tests;
					    $result = $this->insertOrUpdate(TBL_USER_PACKAGES,$where,$userpackageData);
						$data['successMessage'] = SUCCESSFULLY_UPDATED;
						redirect(PACKAGES_MANAGEMENT_URL,'refresh');
					}else{
						$data['errorMessage'] = ERROR;
					}
				}else{
					$data['errorMessage'] = validation_errors();
				}
				
			}
			   
			    $where = array('package_id'=>$packageId);
			    $data['record'] = $this->getSingleRecord(TBL_PACKAGES,$where,'*');
				
				$courseId = $data['record']->courseId;	
			    $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId,'course_id'=>$courseId);
				$data['tests'] = $this->getAllRecords(TBL_TESTS,$where,'*');
				
			    /* $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				$data['tests'] = $this->getAllRecords(TBL_TESTS,$where,'*'); */
				$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			    $data['courses']=$this->getAllRecords(TBL_COURSES,$where);
				$this->load->view('dashboard/packages/header');
				$this->load->view('dashboard/packages/edit_package',$data);
				$extraFooter = $this->load->view('dashboard/packages/packages_script');
				$this->load->view('dashboard/packages/footer',$extraFooter);
		 }catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		 } 
	}
	function updateCartAmount($amount,$id){
		//print_r($this->session->all_userdata());die();
		$userIdsList="";
		if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
        }else{
			$resellerId=0;
		}
		$userIds=$this->getAllRecords(TBL_USERS,array('resellerId'=>$resellerId,'isDeleted'=>0),'userId');
		if($userIds){
			$uIdsList="";
			foreach($userIds as $uid){
				$uIdsList.=$uid->userId.',';
			}
			$userIdsList=rtrim($uIdsList,',');
		}
		$update=$this->Packagemodel->updateCart($userIdsList,$amount,$id,'1');
	}
	function editPromoterPackage($packageId=0)
	{
		try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			/*$Role_Id=$this->session->userdata('Role_Id');
			if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
			{
				redirect(ADMIN_LOGIN_URL,'refresh');
			}*/
			//29
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
		    $data = $p=array();
		    if(isset($_POST['EditPackage']))
			{
				
				$this->load->library('form_validation');
				$this->form_validation->set_rules('packageName',' Package Name','trim|required');
				//$this->form_validation->set_rules('packageAmount','Package Amount','trim|required');
				$this->form_validation->set_rules('test[]','Tests','trim|required');
				$this->form_validation->set_rules('startdate','Package valid from','trim|required');
				$this->form_validation->set_rules('enddate','Package valid to','trim|required');
				$this->form_validation->set_rules('packageType','Package type','trim|required');
				$this->form_validation->set_rules('course','Course','trim|required');
				if($this->form_validation->run()!=false)
				{
					$packageName   = trim($this->input->post('packageName'));
					 $packageType = trim($this->input->post('packageType'));
					  $packageAmount = trim($this->input->post('packageAmount'));
					  
					  
					   $where=array('package_name'=>$packageName,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
					  $ifPackageNameExists=$this->Packagemodel->checkPackageNameExistsInEdit(TBL_PACKAGES,$where,$packageId);
				      if($ifPackageNameExists && count($ifPackageNameExists)>0){
				        $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package Name Already existed.Please try with another Name.</strong> </div>");
			            redirect(PACKAGES_MANAGEMENT_URL);
					 }
					  
					  
					// neatPrintAndDie($packageType);
					 if($packageType == "paid"){ 
						if(empty($packageAmount)){ 
							$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package Amount is required.</strong> </div>");
			               redirect(EDIT_PROMPTER_PACKAGES_URL.'/'.$packageId);
						}
					}
					 if($this->input->post('packageAmount')){
					  $packageAmount = trim($this->input->post('packageAmount'));
					 
					 }else{$packageAmount = 0;}
					$tests = $this->input->post('test[]');
					$tests = implode(",", $tests);
					$tests = str_replace(" ","", $tests);
					$startdate = trim($this->input->post('startdate'));
					 $enddate = trim($this->input->post('enddate'));
					 if(strtotime($enddate) <= strtotime($startdate)){
				        $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package Validity ending date must be greater than start date.</strong> </div>");
			            redirect(EDIT_PROMPTER_PACKAGES_URL.'/'.$packageId);
					 }
					$packageData['package_valid_from']   = date("Y-m-d H:i:s", strtotime($this->input->post('startdate')) );
					$packageData['package_valid_to']   =   date("Y-m-d H:i:s", strtotime($this->input->post('enddate')) );
					$packageData['package_name']   = $packageName;
					
					$packageData['package_test']   = $tests;
					$packageType = trim($this->input->post('packageType')); //qb
					 
					 if($packageType=="paid"){
						 $packageType=1;
						 $packageData['package_amount'] = $packageAmount;
						 $packageData['isPaidPackage'] = $packageType;
					 }else{
						 $packageType=0;
						 $packageData['package_amount'] = 0;
						 $packageData['isPaidPackage'] = $packageType;
					 }
					 if($this->input->post('course')){
					    $packageData['courseId']   = $this->input->post('course'); 
                        }
						

						if($packageAmount){
							 $amountExeedsLimit=$this->checkPakageAmountLimit($packageAmount,$packageId);
							if($amountExeedsLimit){
					   	         $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Amount must be less than ".$amountExeedsLimit['originalPackAmount']."</strong> </div>");
			                     redirect(EDIT_PROMPTER_PACKAGES_URL.'/'.$packageId);
							} 
							$p['p_sell_amount'] = $packageAmount;
						}
					
					$where = array('p_package_id'=>$packageId,'p_is_active'=>1,'p_is_deleted'=>0,'p_promoter_id'=>$resellerId);
					$result = $this->insertOrUpdate(TBL_PROMOTER_PACKAGES,$where,$p);
					if($result)
					{ if($packageAmount){
						$this->updateCartAmount($packageAmount,$packageId);
						}
						$where = array('packageId'=>$packageId);
						$userpackageData['packageTests']=$tests;
					    $result = $this->insertOrUpdate(TBL_USER_PACKAGES,$where,$userpackageData);
						$data['successMessage'] = SUCCESSFULLY_UPDATED;
						redirect(PACKAGES_MANAGEMENT_URL,'refresh');
					}else{
						$data['errorMessage'] = ERROR;
					}
				}else{
					$data['errorMessage'] = validation_errors();
				}
				
			}
			   
			    $where = array('package_id'=>$packageId);
			    $data['record'] = $this->getSingleRecord(TBL_PACKAGES,$where,'*');
				if($data['record']){
					 $amountsql="select p_sell_amount from oes_promoter_packages where p_promoter_id = ".$resellerId." and p_package_id = ".$data['record']->package_id."";
					 $amount=$this->db->query($amountsql)->row();
					 $data['record']->package_amount=$amount->p_sell_amount;
				}
			    $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
				$data['tests'] = $this->getAllRecords(TBL_TESTS,$where,'*');
				$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
			    $data['courses']=$this->getAllRecords(TBL_COURSES,$where);
				$this->load->view('dashboard/packages/header');
				$this->load->view('dashboard/packages/edit_prom_package',$data);
				$extraFooter = $this->load->view('dashboard/packages/packages_script');
				$this->load->view('dashboard/packages/footer',$extraFooter);
		 }catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		 } 
	}
	function checkPakageAmountLimit($newPackageAmount,$id){
		 $where = array('package_id'=>$id);
		 $data['record'] = $this->getSingleRecord(TBL_PACKAGES,$where,'*');
		 $originalPackAmount=$data['record']->package_amount;
		      if($newPackageAmount > $originalPackAmount ){
			      $result['originalPackAmount']=$originalPackAmount;
			      return $result;
				}else{
					return 0;
				} 
	}
	/*  function checkPakageAmountLimit($newPackageAmount,$id){
				     /* $resellerId=$this->session->userdata('resellerId');
                     $amountsql="select p_sell_amount from oes_promoter_packages where p_promoter_id = ".$resellerId." and p_package_id = ".$id."";
					 $amount=$this->db->query($amountsql)->row();
					 $data['record']->package_amount=$amount->p_sell_amount; */
			 /*   $where = array('package_id'=>$id);
			    $data['record'] = $this->getSingleRecord(TBL_PACKAGES,$where,'*');
				$originalPackAmount=$data['record']->package_amount);
				if($newPackageAmount > $originalPackAmount ){
					return 1;
				}else{
					return 0;
				}
				//$$data['record']->package_amount
		
	}  */
	function apply_package_code(){
		$resellerId=$this->session->userdata('resellerId');
		$isReseller=$this->session->userdata('isReseller');
       
		if($_GET['pCode'])
		{
		   $package_code=$_GET['pCode'];
		   $where = array('package_code'=>$package_code,'resellerId'=>0);
		   $packExists = $this->getSingleRecord(TBL_PACKAGES,$where,'*'); 
		   if($packExists){
			  $pack_id=$packExists->package_id;
			  $where = array('p_package_id'=>$pack_id,'p_is_active'=>1,'p_is_deleted'=>0,'p_promoter_id'=>$resellerId);
		      $packExistsForPromoter = $this->getSingleRecord(TBL_PROMOTER_PACKAGES,$where,'*');
			  if($packExistsForPromoter){
				$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package already existed in your account.</strong> </div>");
			    redirect(PACKAGES_MANAGEMENT_URL);
				  
			  }else{
				  $data['p_package_id']=$pack_id;
				  $data['p_promoter_id']=$resellerId;
				  $data['p_merchant_amount']=$packExists->package_amount;
				  $data['p_sell_amount']=$packExists->package_amount;
				  $where=array();
				  $res=$this->insertOrUpdate(TBL_PROMOTER_PACKAGES,$where,$data);
                  if($res){				 
				    $this->session->set_flashdata('Error', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Package added successfully.</strong> </div>");
			     }else{
					 $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Error occured.</strong> </div>");

				 }			
				 redirect(PACKAGES_MANAGEMENT_URL);
			  }
		   }else{
			   $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Enter valid code.</strong> </div>");
			redirect(PACKAGES_MANAGEMENT_URL);
		   }
		   
			
		}else{
			$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Enter valid code.</strong> </div>");
			redirect(PACKAGES_MANAGEMENT_URL);
		}
	}
	function courseTests(){
		$courseId =$this->input->post('courseId');
		$resellerId=$this->session->userdata('resellerId');
		if($resellerId){
		$resellerId=$this->session->userdata('resellerId');}
		else{$resellerId=0;}
		//print($courseId);die();
		$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId,'course_id'=>$courseId);//qb
			   $data['tests'] = $this->getAllRecords(TBL_TESTS,$where,'*');
			   $this->load->view('dashboard/packages/test_ajax',$data);
	}
	

}
?>