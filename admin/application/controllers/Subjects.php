<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Subjects extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Usermodel'); 
	}
	
	
	function index()
	{
		
		try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			if($this->session->userdata('resellerId')){
				$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
			$where = array('isDeleted'=>0,'resellerId'=>$resellerId);
			//$data['subjects'] = $this->getAllRecords(TBL_SUBJECTS,$where,'*');
			$data['subjects'] = $this->getAllRecordsByDesc(TBL_SUBJECTS,$where,'id','*');
		
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/subjects/subjects',$data);
			$extraFooter = $this->load->view('dashboard/users_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	}
	
}
?>