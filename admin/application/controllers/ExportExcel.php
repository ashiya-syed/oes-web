<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
//require_once  'C:/xampp/htdocs/ONYX-EXAMS/admin/ExcelReader/PHPExcel/IOFactory.php';
//require_once  '/var/www/html/admin/ExcelReader/PHPExcel/IOFactory.php';
class ExportExcel extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Exportexcelmodel');
		$this->load->helper('download');

	}
	
	function exportQuestions(){
	     $testId=$this->uri->segment('4');
		 $testDetails=$this->getSingleRecord(TBL_TESTS,array('testId'=>$testId));
		 $testName=$testDetails->testName;
		 $Quedata=$this->Exportexcelmodel->getQuestionsData($testId);
	     $this->load->library('excel');
         $this->excel->setActiveSheetIndex(0); 
		 if(count($Quedata)<=0){
			$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>No questions exists for this test.</strong> </div>");
			        redirect(QUETION_MANAGEMENT_URL);
		 }
                //name the worksheet
                $this->excel->getActiveSheet()->setTitle('Questions');
                //set cell A1 content with some text
                $this->excel->getActiveSheet()->setCellValue('A1', 'Questions Excel Sheet');
                $this->excel->getActiveSheet()->setCellValue('A2', 'Subject');
                $this->excel->getActiveSheet()->setCellValue('B2', 'Question No');
                $this->excel->getActiveSheet()->setCellValue('C2', 'Question');
                $this->excel->getActiveSheet()->setCellValue('D2', 'Topic id');
                $this->excel->getActiveSheet()->setCellValue('E2', 'Option1');
                $this->excel->getActiveSheet()->setCellValue('F2', 'Option2');
                $this->excel->getActiveSheet()->setCellValue('G2', 'Option3');
                $this->excel->getActiveSheet()->setCellValue('H2', 'Option4');
                $this->excel->getActiveSheet()->setCellValue('I2', 'Option5');
                $this->excel->getActiveSheet()->setCellValue('J2', 'Option6');
                $this->excel->getActiveSheet()->setCellValue('K2', 'Option7');
                $this->excel->getActiveSheet()->setCellValue('L2', 'Option8');
                $this->excel->getActiveSheet()->setCellValue('M2', 'Option7');
                $this->excel->getActiveSheet()->setCellValue('N2', 'Option9');
                $this->excel->getActiveSheet()->setCellValue('O2', 'Option10');
                $this->excel->getActiveSheet()->setCellValue('P2', 'Option11');
                $this->excel->getActiveSheet()->setCellValue('Q2', 'Option12');
                $this->excel->getActiveSheet()->setCellValue('R2', 'Answer');
                $this->excel->getActiveSheet()->setCellValue('S2', 'Hint');
                //merge cell A1 until C1
                $this->excel->getActiveSheet()->mergeCells('A1:D1');
                //set aligment to center for that merged cell (A1 to C1)
                $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                //make the font become bold
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
                $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
 			  for($col = ord('A'); $col <= ord('C'); $col++){ 
	               $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                 
                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                   }
				     $exceldata="";
					 foreach ($Quedata as $row){
						$exceldata[] = $row; 
				      } 
				
                //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');
                 
                $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('J4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('K4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('L4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('N4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('O4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('P4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('Q4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('R4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $file=$testName.' questions.xls';
                $filename=$file; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
    

	}

}
?>