<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Resellers extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Resellermodel'); 
		$this->load->model('Admin_model'); 

	}
	
	
	function index()
	{
		
		try{
			$Role_Id=$this->session->userdata('Role_Id');
			if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
			{
				redirect(LOGOUT_URL,'refresh');
			}
           
			$data['resellers'] = $this->Resellermodel->getallResellers();
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/resellers/resellers',$data);
			$extraFooter = $this->load->view('dashboard/users_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	}
	function editReseller()
	 {
		 
		    $Role_Id=$this->session->userdata('Role_Id');
			if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
			{
				redirect(LOGOUT_URL,'refresh');
			}
		 
		  $resellerId= $this->uri->segment(5);
		   
		  $where = array('reseller_id' => $resellerId);
		  $details=$this->getSingleRecord(TBL_RESELLERS,$where);
		  $data['details']=$details;
		  
		  $where = array('resellerId' => $resellerId);
		  $Resellerdetails=$this->getSingleRecord(TBL_RESELLERS_DETAILS,$where,$select="*");
		  $data['Resellerdetails']=$Resellerdetails;
		 // neatPrintAndDie($this->db->last_query());
		  
		  $where=array('isActive'=>1,'isDeleted'=>0,'testType'=>1);
		  $data['tests']=$this->getAllRecords(TBL_TESTS,$where,$select="*");
		  
		  $where=array('isActive'=>1,'isDeleted'=>0);
		  $data['packages']=$this->getAllRecords(TBL_PACKAGES,$where,$select="*");
		  $this->load->view('dashboard/packages/header');
		  $this->load->view('dashboard/resellers/edit_resellers_ajax',$data);
				//$this->load->view('dashboard/packages/edit_package',$data);
				$extraFooter = $this->load->view('dashboard/packages/packages_script');
				$this->load->view('dashboard/packages/footer',$extraFooter);
		  
	} 
	function d(){
		$isSessionIn=$this->session->userdata('isSessionIn');
		$resellerId=$this->session->userdata('resellerId');
        $isReseller=$this->session->userdata('isReseller');
			if(empty($isSessionIn) && empty($resellerId))
			{
				redirect(RESELLER_LOGOUT_URL,'refresh');
			}
			if($isReseller == 2){
			  $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId,'roleID'=>2);
              $usersCount = $this->getAllRecords(TBL_USERS,$where,'count(*) as users_count');
		      $data['usersCount'] = $usersCount[0];
		      $where = array('p_t_is_active'=>1,'p_t_is_deleted'=>0,'p_t_promoter_id'=>$resellerId);
              $testsCount = $this->getAllRecords(TBL_PROMOTER_TESTS,$where,'count(*) as tests_count');
		      $data['testsCount'] = $testsCount[0];
			  $where = array('p_is_active'=>1,'p_is_deleted'=>0,'p_promoter_id'=>$resellerId);
              $packagesCount = $this->getAllRecords(TBL_PROMOTER_PACKAGES,$where,'count(*) as packages_count');
		      $data['packagesCount'] = $packagesCount[0];
			}else{
		      $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId,'roleID'=>2);
              $usersCount = $this->getAllRecords(TBL_USERS,$where,'count(*) as users_count');
		      $data['usersCount'] = $usersCount[0];
		      $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
              $testsCount = $this->getAllRecords(TBL_TESTS,$where,'count(*) as tests_count');
		      $data['testsCount'] = $testsCount[0];
              $packagesCount = $this->getAllRecords(TBL_PACKAGES,$where,'count(*) as packages_count');
		      $data['packagesCount'] = $packagesCount[0];
			}
	
		$where = array('isActive'=>1,'isDeleted'=>0);
		//new
		$UserPurchases = $this->getAllRecords(TBL_USER_PACKAGE,$where,'*');//neatPrintAndDie($UserPurchases);
		$amount=0;
		foreach($UserPurchases as $purchase){
			$uid=$purchase->userId;
			$resellerid=$this->Admin_model->getUserInfo($uid);
			if($resellerid == $resellerId ){
				$amount+=$purchase->amount;
			}
			
		} //neatPrintAndDie($amount);
		$data['amount']=$amount;
		$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
		$coupons = $this->getAllRecords(TBL_COUPONS,$where,'count(*) as coupons');
		$data['coupons'] = $coupons[0];
		//neatPrintAndDie($data);
        $this->load->view('dashboard/includes/dashboardheader');
		$this->load->view('dashboard/dashboard',$data);
		$this->load->view('dashboard/includes/footer');
	}
	
	/*******************
	********************
	This method is for
	reseller login
	********************
	********************/
	function login()
	{
		try{
				$this->load->helper('url');
				$data=array();
				if($this->session->userdata('resellerId')){
					redirect(RESELLER_DASHBOARD_URL,'refresh'); 
				}
				$urlName=$this->uri->segment(1);
				$where=array('reseller_name'=>$urlName);
				$check=$this->getSingleRecord(TBL_RESELLERS,$where,$select="*");
				if($check && count($check)>0){
				if (!empty($_POST)) 
				{
					$this->session->unset_userdata('Role_Id');
					$this->session->unset_userdata('User_Name');
					$this->load->library('form_validation');
					$this->form_validation->set_rules('Email_OR_phone','Email or Phone Number','trim|required');
					$this->form_validation->set_rules('password','Password','trim|required');
					if($this->form_validation->run()!=false)
					{
						$remember = trim($this->input->post('remember'));
						$this->session->set_userdata('remember',$remember);
						
						$Email_OR_phone = trim($this->input->post('Email_OR_phone'));
						$password       = trim($this->input->post('password'));
						$result = $this->Resellermodel->login($Email_OR_phone, $password);
						if($result) 
						{
							$session_remember=$this->session->userdata('remember');
							if(!empty( $session_remember) && $session_remember==1){
								
							  $cookie1= array('name'   => 'reseller_Name', 'value'  => $Email_OR_phone, 'expire' => '630720000','path'=>'/'  );
							  $cookie2= array('name'   => 'reseller_password', 'value'  => $password, 'expire' => '630720000', 'path'=>'/' );
							  $cookie3= array('name'   => 'reseller_remember', 'value'  => $session_remember, 'expire' => '630720000', 'path'=>'/' );
							    $this->input->set_cookie($cookie1);
							    $this->input->set_cookie($cookie2);
							    $this->input->set_cookie($cookie3);
						     }else {   
							  $cookie1= array('name'   => 'reseller_Name', 'value'  => '','expire' => time()-3600, );
							  $cookie2= array('name'   => 'reseller_password','value'  => '','expire' => time()-3600, );
							  $cookie3= array('name'   => 'reseller_remember','value'  => '','expire' => time()-3600,);
								$this->input->set_cookie($cookie1);
								$this->input->set_cookie($cookie2);
								$this->input->set_cookie($cookie3);
						    }
							
							$this->setreseller_sessiondata($result); 
							$data['successMessage'] = LOGIN_SUCCESS;
							redirect(RESELLER_DASHBOARD_URL,'refresh'); 
						} 
						else 
						{
								$data['errorMessage'] = EMAIL_OR_NAME_AND_PASSWORD_NOT_MATCH;
						}
					}
					else {
						$data['errorMessage']=validation_errors();
					}
					
				}
				$this->load->view('login',$data);
		}else{
			$this->load->view('pageNotFound');
		}
				
				
		 }catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		 } 	
	}
	/*******************
	********************
	This method is useful to set the 
	login reseller details into session
	********************
	********************/
	function setreseller_sessiondata($result){ 
		if($result)
		   {
		        $resellerId = $result->reseller_id;
			   	$reseller_Name = $result->reseller_name;
			   	$packages_limit = $result->reseller_packageLimit;
			   	$tests_limit = $result->reseller_testLimit;
			   	$users_limit = $result->reseller_usersLimit;
			   	$isReseller = $result->isReseller;
			   
			   	$newdata = array('resellerId'  => $resellerId,
			   	                 'reseller_Name'  => $reseller_Name,
			   	                 'packages_limit'  => $packages_limit,
			   	                 'tests_limit'  => $tests_limit,
			   	                 'isReseller'  => $isReseller,
			   	                 'users_limit'  => $users_limit,
			   	                 'isSessionIn'  => 1,
								 'url'=> SITEURL.'/'.$reseller_Name,
			   	                 
			   			   );
						   
			   	$this->session->set_userdata($newdata);
			}
	}
	 function logout(){
		                     $this->session->unset_userdata('resellerId');
				             $this->session->unset_userdata('packages_limit');
				             $this->session->unset_userdata('tests_limit');
				             $this->session->unset_userdata('users_limit');
				             $this->session->unset_userdata('reseller_Name');
				            // $this->session->unset_userdata('isSessionIn');
                             $url=$this->session->userdata('url');
							 redirect($url);
	                 }
	
	/*******************
	********************
	This method is for reseller 
	profile
	********************
	********************/
	function profile(){
        $isSessionIn=$this->session->userdata('isSessionIn');
		$resellerId=$this->session->userdata('resellerId');
			if(empty($resellerId))
			{
				redirect(RESELLER_LOGOUT_URL,'refresh');
			}
		
		
		$where = array('reseller_id' => $resellerId);
		$details=$this->getSingleRecord(TBL_RESELLERS,$where);
		$data['details']=$details;
        $this->load->view('dashboard/includes/dashboardheader');
		$this->load->view('dashboard/users_script');
		$this->load->view('dashboard/resellers/profile',$data);
		$this->load->view('dashboard/includes/footer');
		
		}
		
	/*******************
	********************
	This method is for
	update profile
	********************
	********************/
		function updateProfile()
	 {
		$isSessionIn=$this->session->userdata('isSessionIn');
		$resellerId=$this->session->userdata('resellerId');
			if( empty($resellerId))
			{
				redirect(RESELLER_LOGOUT_URL,'refresh');
			}
		 $userId = trim($this->input->post('userId'));
		 $name = trim($this->input->post('name'));
		 $emailAddress = trim($this->input->post('emailAdress'));
		 $phoneNumber = trim($this->input->post('number'));
		
		 if(!empty($name))        { $data['reseller_name']= $name;                      }
		 if(!empty($emailAddress)){ $data['reseller_email']= $emailAddress;          }
		 if(!empty($phoneNumber)) { $data['reseller_mobile']= $phoneNumber;            }
		
         $data['updatedTime'] = date("Y-m-d H:i:s");
		 $where = array('reseller_id'=>$userId);
		 $result=$this->insertOrUpdate(TBL_RESELLERS,$where,$data);
		 if($result>0){
				   $message = array('status' => 1,'message' => SUCCESSFULLY_UPDATED,'result' => $result);
			    }else{
			
				    $error_message = array('status' => 0,'message' => 'Error occured','result' =>'');
			    }
				echo json_encode($message); die();
	 }
	 
	 
	 /*******************
	********************
	This method is for
	update password
	********************
	********************/
	  function editPassword()
	 {
		  $isSessionIn=$this->session->userdata('isSessionIn');
		 $resellerId=$this->session->userdata('resellerId');
			if(empty($resellerId))
			{
				redirect(RESELLER_LOGOUT_URL,'refresh');
			}
		  $userId= trim($this->input->post('userId'));
		  $where = array('reseller_id' => $userId);
		  $details=$this->getSingleRecord(TBL_RESELLERS,$where);
		  $data['details']=$details;
		  
		  $where = array('isDeleted' => 0,'isActive'=>1);
		  $this->load->view('dashboard/resellers/password_update_ajax',$data);
	}
	function updatePassword()

	{ 
		$isSessionIn=$this->session->userdata('isSessionIn');
		$resellerId=$this->session->userdata('resellerId');
			if(empty($resellerId))
			{
				redirect(RESELLER_LOGOUT_URL,'refresh');
			}
		 $this->load->library('form_validation');
		 $this->form_validation->set_rules('new_password','Password','trim|required');
		 $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[new_password]');
	     if($this->form_validation->run()!=false)
		 { 
		 $userId= trim($this->input->post('userId'));
		 $update_password=trim($this->input->post('update_password'));
		 $where = array('reseller_id' => $userId);
		 //$update_password = md5($update_password);
		 $details=$this->getSingleRecord(TBL_RESELLERS,$where);
		
			  $oldpwd=$this->decrypt($details->password);
			 if($oldpwd == $update_password)
			 {  
				 $new_password = trim($this->input->post('new_password'));
				 $confirm_password = trim($this->input->post('confirm_password'));
				if(!empty($new_password)){ $superadmin_data['password']=$this->encrypt($new_password);    }
				 $superadmin_data['updatedTime'] = date("Y-m-d H:i:s");
				 $where = array('reseller_id'=>$userId);
				 $result=$this->insertOrUpdate(TBL_RESELLERS,$where,$superadmin_data);
				
				 if($result>0){
						   $message = array('status' => 1,'message' => PASSWORD_UPDATE_SUCCESS,'result' => $result);
						}else{
					
							$error_message = array('status' => 0,'message' => 'Error occured','result' =>'');
						}
			 }
			 else
			 {
				 $message = array('status' => 0,'message' =>'Current Password is not Correct','result' => '');
			 }

				}
		
		else
		{
			$message = array('status' => 0,'message' =>validation_errors(),'result' => '');
			
		}
		echo json_encode($message); die();
		 
	}
		
	
}
?>