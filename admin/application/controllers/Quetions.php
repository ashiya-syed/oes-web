<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
//require_once  'C:/xampp/htdocs/ONYX-EXAMS/admin/ExcelReader/PHPExcel/IOFactory.php';
require_once  '/var/www/html/admin/ExcelReader/PHPExcel/IOFactory.php';
class Quetions extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Questionmodel');
	}
	
	/**********************************
	This method will useful to show the que and 
	test details
	**********************************/
	function index()
	{
		$TestType = $this->uri->segment(3);
		
		try{
			//30
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			/*$Role_Id=$this->session->userdata('Role_Id');
			if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
			{
				redirect(ADMIN_LOGIN_URL,'refresh');
			}*/
			if($TestType =="maintest")
			{
				$tType= MAIN_TEST;
			}else if($TestType =="practicaltest"){
				$tType= PRACTICAL_TEST;
			}
			$data['tType'] = $tType;
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			$where = array('isDeleted'=>0,'testType'=>$tType,'resellerId'=>$resellerId);
			}else{
				$where = array("isDeleted"=>0,'testType'=>$tType,'resellerId'=>0);
			}
			if($resellerId && $this->session->userdata('isReseller') == 2){
			  $tests = $this->Questionmodel->getPromoterTests($resellerId,$tType);
			 }else{
			  $tests = $this->getAllRecords(TBL_TESTS,$where,'*');
			} 
			if($tests && count($tests)>0)
			{    
		        $i = 0;
				foreach($tests as $test)
				{ 
				    $test->courseName='-';
				    $curseName=$this->getSingleRecord(TBL_COURSES,array('courseId'=>$test->course_id),'courseName');
					if($curseName){
						$test->courseName=$curseName->courseName;
					}
				    $where = array('test_id'=>$test->testId,'isActive'=>1,'isDeleted'=>0);
					$totalQue = $this->getAllRecords(TBL_MAIN_TEST_QUESTIONS,$where,'*');
					$totalQue = count($totalQue);
					$test->TotalQue = $totalQue;
					$i++;
				}
			}
			
			$data['tests'] = $tests; 
			$this->load->view('dashboard/includes/header');
			if($resellerId && $this->session->userdata('isReseller') == 2){
		       $this->load->view('dashboard/tests/promotion_tests',$data);
			}else{
			   $this->load->view('dashboard/tests/tests',$data);
            }
			$extraFooter = $this->load->view('dashboard/users_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	}
	
	/*******************************
	This method will useful to add the 
	tests and multiple que of the test.
	********************************/
	
	function addQuetions()
	{
		$TestType = $this->uri->segment(3);
		//neatPrintAndDie($TestType);
		try{
			//30
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			
			if($this->session->userdata('resellerId')){
			       $resellerId=$this->session->userdata('resellerId');
				   $where=array('reseller_id'=>$resellerId); 
				   $resellerDetails=$this->getSingleRecord(TBL_RESELLERS,$where,'*');
				   if($resellerDetails){
					    $tests_limit=$resellerDetails->reseller_testLimit;
					   
				   }else{
					   $tests_limit=$this->session->userdata('tests_limit');
				   }
			       
				   $where=array('isDeleted'=>0,'resellerId'=>$resellerId,'testType'=>1);
				   $testCount=$this->getAllRecords(TBL_TESTS,$where,'*');
				   if($testCount && count($testCount)>=$tests_limit && $TestType =="maintest"){
				    $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>You already exceed your tests limit.For further information contact admin.</strong> </div>");
			        redirect(QUETION_MANAGEMENT_URL);
					}
				   
			 }else{
				    $resellerId=0;
			  }
			
			if($TestType =="maintest")
			{
				$tType= MAIN_TEST;
			}else if($TestType =="practicaltest"){
				$tType= PRACTICAL_TEST;
			}
			$data = array();
			$data['type']=$tType; 
			
			
			if(isset($_POST['AddQuetions']) || isset($_POST['selectQuestion']) )
			{
				$course_id = $this->input->post('course');
				$attempts = $this->input->post('attempts');
				$testName = $this->input->post('testName');
				$testAmount = $this->input->post('testAmount');
				$testDuration = $this->input->post('testDuration');
				$testQuestions = $this->input->post('testQuestions');
				$testMarks = $this->input->post('testMarks');
				$quetions = $this->input->post('quetion[]');
				$options = $this->input->post('option[]');
				$rightMarks = $this->input->post('positiveMarks');
				$isShuffle = $this->input->post('isShuffle');
				$isFree = $this->input->post('isFree');
				$testCode=generateRandNumber(10);
				$nagativeMarks = $this->input->post('nagativeMarks');
				$where=array('testName'=>$testName,'testType'=>$tType,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				$ifTestNameExists=$this->getAllRecords(TBL_TESTS,$where,'*');
				if($ifTestNameExists && count($ifTestNameExists)>0){
				$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Test Name Already existed.Please try with another Name.</strong> </div>");
			        redirect(current_url());
					
				}
				if($testQuestions){
					$checkMarks=$rightMarks*$testQuestions;
					if($checkMarks != $testMarks){
						$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Assigned positive marks are not equal to total marks of the test.</strong> </div>");
			       redirect(current_url());
					}
				}
				
				if($isFree==0){
					if(empty($testAmount)){
						$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Test amount is required.</strong> </div>");
			             redirect(current_url());
					}
				}
				
				$testData['testCode'] = $testCode;//2018
				$testData['course_id'] = $course_id;//2018
				$testData['resellerId'] = $resellerId;//29
				$testData['attempts'] = $attempts;
				$testData['testName'] = $testName;
				$testData['testAmount'] = $testAmount;
				$testData['testTime'] = $testDuration;
				$testData['rightMarks'] = $rightMarks;
				$testData['negativeMarks'] = $nagativeMarks;
				$testData['testType'] = $tType;
				$testData['totalQuestions'] = $testQuestions;
				$testData['totalMarks'] = $testMarks;
				if($isShuffle){
				$testData['isShuffleQueOrder'] = $isShuffle;
				}else{
					$testData['isShuffleQueOrder'] = 0;
				}
				
				if(@$isFree){
				  $testData['isFree'] = $isFree;
				  $testData['testAmount'] = 0;
				} else{
				  $testData['isFree'] = 0;	
				}
				$testId = $this->insertOrUpdate(TBL_TESTS,$where=array(),$testData);
				
				
			   if($testId)
			   {
				   if(isset($_POST['selectQuestion'])){  redirect(QUESTION_BOOK_URL.'?test='.$testId,'refresh');}
				   $data['successMessage'] = SUCCESS;
				   
				   redirect(QUETION_MANAGEMENT_URL,'refresh');
			   }else{
				    $data['errorMessage'] = ERROR;
			   }
				
			}
			
			$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$data['courses']=$this->getAllRecords(TBL_COURSES,$where);
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/tests/add_test',$data);
			$extraFooter = $this->load->view('dashboard/users_script');
			$this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	}
	
	function addTestQuestions(){
	   
		try{
		
			$TestId = $this->uri->segment(4);
			$queId=0;
	        $where=array('testId'=> $TestId);
	        $testDetails=$this->getSingleRecord(TBL_TESTS,$where);
			if($testDetails){
	        $data['testName']=$testDetails->testName;
			$where=array('test_id'=> $TestId);
	        $testQuestions=$this->Questionmodel->getLastQuestionNumbers(TBL_QUETIONS,$where,'*');
			if($testQuestions){
				$lastQuestion=$testQuestions->que_id;
			}else{
				$lastQuestion=0;
			}
			$data['lastQuestion']=$lastQuestion;
			//t
			$where=array('test_id'=> $TestId,'isActive'=>1,'isDeleted'=>0);
	        $TotalActiveQuestions=$this->getAllRecords(TBL_QUETIONS,$where,'*');
			$data['QuestionNumber']=count($TotalActiveQuestions)+1;
			//end
			if(isset($_POST['AddQuetions']))
			{
				$quetions = $this->input->post('quetion[]');
				$hint = $this->input->post('hint[]');
				$queType = $this->input->post('queType[]');
			   if($quetions)
				{
					$i=$lastQuestion;
					$j= 0;
					$qId = '';
				   $quesArray=array_keys($quetions);
				
					foreach($quesArray as $quetionA)
					//foreach($quetions as $quetion)
					{
						
						$queArray = array('que' => trim($quetions[$quetionA]),
						                  'que_id' => $quetionA,
						                  'test_id' => $TestId,
										  'hint' => $hint[$quetionA],
										  'queType' => $queType[$quetionA],
						                 
						);
						
						$queId = $this->insertOrUpdate(TBL_QUETIONS,$where=array(),$queArray);
						
						if($queArray['queType'] == 2){
							$textType=1;
							$options=$this->input->post('option'.$quetionA);
							if($options){
								foreach($options as $opt){
								$optionsArray = array('option' => trim($opt),
														  'test_id' => $TestId,
														  'que_id' => $quetionA,
														  'is_answer' => 1,
														  'textType' =>$textType,
														  'isActive' =>1,
														  'isDeleted'=>0,
														 
														  
									);	
								$this->insertOrUpdate(TBL_QUE_OPTIONS,$where=array(),$optionsArray);
								}
						   }
						}elseif($queArray['queType'] == 3){
							$textType=1;
							$options=$this->input->post('option'.$quetionA);
							if($options){
								foreach($options as $opt){
								$optionsArray = array('option' => trim($opt),
														  'test_id' => $TestId,
														  'que_id' => $quetionA,
														  'is_answer' => 1,
														  'textType' =>$textType,
														  'isActive' =>1,
														  'isDeleted'=>0,
														 
														  
									);	
								$this->insertOrUpdate(TBL_QUE_OPTIONS,$where=array(),$optionsArray);
								}
						   }
							
						}else{
						$TextOptions[$quetionA] = $this->input->post('option'.$quetionA.'[]');
						$TextOptions=array_filter($TextOptions[$quetionA]);
						$textType=1;
						$options[$quetionA] =$TextOptions;
						
						if($options[$quetionA] || @$_FILES['option'.$quetionA]){  
							$a=0; $b=1;
							
								
							$optionImages[$quetionA]=$_FILES['option'.$quetionA];
							$optimageNames=$optionImages[$quetionA]['name'];
						    //foreach($options[$i] as $option[$i])
							$optionsMime=$_FILES['option'.$quetionA]['type'];
							$h=0;
						    foreach($optimageNames as $name)
							{
								$option[$a] = "";
								$que_ans = $this->input->post('que'.$quetionA.$b.'ans');
								$is_answer = 0;
								//if(@$que_ans[$i][$a] == "on")
								if($que_ans == "on")
								{  
									$is_answer = 1;
								} 
															if(!empty($name)){
															$mime=$optionsMime[$h];
															if(strstr($mime, "video/")){
															$filetype = "video";
															$textType=3;
															}else if(strstr($mime, "image/")){
															$filetype = "image";
															$textType=2;
															}else if(strstr($mime, "audio/")){
															$filetype = "audio";
															$textType=4;
															}else{
																$textType=1;
															}  
															//neatPrintAndDie($textType);
															
															$file_extension=pathinfo($name);
															$rand=rand();
											                 $picname=$rand.time().'.'.strtolower($file_extension['extension']);
											                 $icon_target_path = './uploads/options/';
											                 $icon_target_path = $icon_target_path . basename($picname);
											                 $movefile=move_uploaded_file($optionImages[$quetionA]['tmp_name'][$a], $icon_target_path);
															 $option[$a]=$picname;
															}

						
								if(!empty($options[$quetionA][$a]))
								{
									 $option[$a]=$options[$quetionA][$a];
									 $textType=1;
								}
								
								if($option[$a]){
								$optionsArray = array('option' => trim($option[$a]),
								                      'test_id' => $TestId,
								                      'que_id' => $quetionA,
													  'is_answer' => $is_answer,
													  'textType' =>$textType,
													  'isActive' =>1,
													  'isDeleted'=>0,
													 
								                      
								);
								$this->insertOrUpdate(TBL_QUE_OPTIONS,$where=array(),$optionsArray);
								}
							  $a++; $b++;$h++;
							}
						  //}
						}
					//}
						$i++;
			             }
						////end else
						
					//else{$queId=0;redirect(QUETION_MANAGEMENT_URL,'refresh');}
				 }
					
			     }
				 if($queId)
			     {
				   $data['successMessage'] = SUCCESS;
				   redirect(QUETION_MANAGEMENT_URL,'refresh');
			     }else{
				    $data['errorMessage'] = ERROR;
			      } 
			}
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/tests/add_test_questions',$data);
			$extraFooter = $this->load->view('dashboard/users_script',$data);
		    $this->load->view('dashboard/includes/footer',$extraFooter);
			}
		}
			
		catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
			
	}
	
	
	function editQuetions()
	{
		try{
			$testId=$this->uri->segment(4);
			
			if(isset($_POST['EditQuetions']))
			{
				
				$course = trim($this->input->post('course'));
				$attempts = trim($this->input->post('testAttempts'));
				$testName = trim($this->input->post('testName'));
				$testAmount = trim($this->input->post('testAmount'));
				$testDuration = trim($this->input->post('testDuration'));
				$rightMarks = trim($this->input->post('testPositive'));
				$negativeMarks = trim($this->input->post('testNegative'));
				$isShuffle = trim($this->input->post('isShuffle'));
		        $isFree = trim($this->input->post('isFreeT'));
				 $testMarks = trim($this->input->post('testMarks'));
		        $testQuestions = trim($this->input->post('testQuestions'));
				$quetions = $this->input->post('quetion[]');
				$hint = $this->input->post('hint[]');
				$queType = $this->input->post('queType[]');//31
				$optionA = $this->input->post('optionA[]');
				$optionB = $this->input->post('optionB[]');
				$optionC = $this->input->post('optionC[]');
				$optionD = $this->input->post('optionD[]');
				$optionAns = $this->input->post('optionAns[]');
				$queIds = $this->input->post('queId[]');
				$testData['testName'] = $testName;
				$testData['testAmount'] = $testAmount;
				$testData['attempts'] = $attempts;
				$testData['rightMarks'] = $rightMarks;
				$testData['negativeMarks'] = $negativeMarks;
				$testData['testTime'] = $testDuration;
				if($course){
				$testData['course_id'] = $course;
				}
				
				$testData['totalQuestions'] = $testQuestions;
				$testData['totalMarks'] = $testMarks;
				
				if($testQuestions){
					$checkMarks=$rightMarks*$testQuestions;
					if($checkMarks != $testMarks){
						$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Assigned positive marks are not equal to total marks of the test.</strong> </div>");
			            redirect(current_url());
					}
				}
				
				if($isShuffle){
					$isShuffle=1;
				}else{$isShuffle=0;}
				$testData['isShuffleQueOrder'] = $isShuffle;
				if($isFree){
					$isFree=1;
				}else{$isFree=0;}
				$testData['isFree'] = $isFree; 
				if($this->session->userdata('resellerId')){
					$resellerId=$this->session->userdata('resellerId');
				}else{
					$resellerId=0;
				}
				$testType=$this->input->post('testType'); 
				$where=array('testName'=>$testName,'testType'=>$testType,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				$ifTestNameExists=$this->Questionmodel->checkNameExistsInEdit(TBL_TESTS,$where,$testId);
				if($ifTestNameExists && count($ifTestNameExists)>0){
				$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Test Name Already existed.Please try with another Name.</strong> </div>");
			        redirect(current_url());
					
				}
				
				if($isFree == 0){
					if(empty($testAmount)){
						$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Test amount is required.</strong> </div>");
			             redirect(current_url());
					}
				}
				$where = array('testId'=>$testId);
			    $testCodeExists=$this->getSingleRecord(TBL_TESTS,$where,'*');
				if(empty($testCodeExists->testCode)){
					$testData['testCode'] = generateRandNumber(10);
				}
				$testId = $this->insertOrUpdate(TBL_TESTS,$where,$testData);
				if($testId)
			   { 
		          if($testAmount){
				   	$this->updateCartAmount($testAmount,$testId);
                  }
				   $data['successMessage'] = SUCCESS;
				   redirect(QUETION_MANAGEMENT_URL,'refresh');
			   }else{
				    $data['errorMessage'] = ERROR;
			   }
				
			}
			
			$where = array('testId'=>$testId);
			$TestData = $this->getSingleRecord(TBL_TESTS,$where,'*');
			if(empty($TestData))
			{
				redirect(ADMIN_DASHBOARD_URL,'refresh');
			}
			$Quedata = $this->getAllRecords(TBL_MAIN_TEST_QUESTIONS, array('test_id'=>$testId,'isActive'=>1,'isDeleted'=>0),'*');
			/*if($Quedata)
			{
				$i=0;
				foreach($Quedata as $Que)
				{
					$questions = $this->getSingleRecord(TBL_QUETIONS,array('id'=>$Que->que_id),'*');
					$where=array('que_id'=>$Que->que_id,'isActive'=>1,'isDeleted'=>0);
					$options = $this->getAllRecords(TBL_QUE_OPTIONS,$where,'*');
					$Quedata[$i]->options = $options;
					$Quedata[$i]->question = $questions->que;
					$Quedata[$i]->subject = $questions->subject_id;
					$Quedata[$i]->topic = $questions->topic_id;
				$i++;
				}
				
			}*/
			if($this->session->userdata('resellerId')){
			       $resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
			$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$data['subjects']=$this->getAllRecords(TBL_SUBJECTS,$where,'*');
			$data['topics']=$this->getAllRecords(TBL_TOPICS,$where,'*');

			$data['TestData'] = $TestData;
			$data['Quedata']  = $Quedata;
			$lastQuestion = 0;
			if( count($Quedata)>0){
					$lastQueNo = count($Quedata) - 1;
					$lastQuestion= $Quedata[$lastQueNo]->que_id;
			}
			
			$where=array('test_id'=> $testId,'isActive'=>1,'isDeleted'=>0,'isApproved'=>1);
	        $TotalActiveQuestions=$this->getAllRecords(TBL_TEST_QUESTIONS,$where,'*');
			$lastQuestiondata['QuestionNumber']=count($TotalActiveQuestions)+1;
			$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$data['courses']=$this->getAllRecords(TBL_COURSES,$where);
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/tests/edit_test',$data);
			$extraFooter = $this->load->view('dashboard/users_script',$lastQuestiondata);
		    $this->load->view('dashboard/includes/footer',$extraFooter);
			
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 	
	}
	
	function updateCartAmount($amount,$id){
		//print_r($this->session->all_userdata());die();
		$userIdsList="";
		if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
        }else{
			$resellerId=0;
		}
		$userIds=$this->getAllRecords(TBL_USERS,array('resellerId'=>$resellerId,'isDeleted'=>0),'userId');
		if($userIds){
			$uIdsList="";
			foreach($userIds as $uid){
				$uIdsList.=$uid->userId.',';
			}
			$userIdsList=rtrim($uIdsList,',');
		}
		$update=$this->Questionmodel->updateCart($userIdsList,$amount,$id,'2');
	}
	
	function editPromoterTest()
	{
		try{
			$testId=$this->uri->segment(4);
			
			if(isset($_POST['EditQuetions']))
			{
				
				$course = trim($this->input->post('course'));
				$attempts = trim($this->input->post('testAttempts'));
				$testName = trim($this->input->post('testName'));
				$testAmount = trim($this->input->post('testAmount'));
				$testDuration = trim($this->input->post('testDuration'));
				$rightMarks = trim($this->input->post('testPositive'));
				$negativeMarks = trim($this->input->post('testNegative'));
				$isShuffle = trim($this->input->post('isShuffle'));
		        $isFree = trim($this->input->post('isFreeT'));
				 $testMarks = trim($this->input->post('testMarks'));
		        $testQuestions = trim($this->input->post('testQuestions'));
				$quetions = $this->input->post('quetion[]');
				$hint = $this->input->post('hint[]');
				$queType = $this->input->post('queType[]');//31
				$optionA = $this->input->post('optionA[]');
				$optionB = $this->input->post('optionB[]');
				$optionC = $this->input->post('optionC[]');
				$optionD = $this->input->post('optionD[]');
				$optionAns = $this->input->post('optionAns[]');
				$queIds = $this->input->post('queId[]');
				$testData['testName'] = $testName;
				$tData['p_t_sell_amount'] = $testAmount;
				$testData['attempts'] = $attempts;
				$testData['rightMarks'] = $rightMarks;
				$testData['negativeMarks'] = $negativeMarks;
				$testData['testTime'] = $testDuration;
				if($course){
				$testData['course_id'] = $course;
				}
				
				$testData['totalQuestions'] = $testQuestions;
				$testData['totalMarks'] = $testMarks;
				
				if($testQuestions){
					$checkMarks=$rightMarks*$testQuestions;
					if($checkMarks != $testMarks){
						$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Assigned positive marks are not equal to total marks of the test.</strong> </div>");
			            redirect(current_url());
					}
				}
				
				if($isShuffle){
					$isShuffle=1;
				}else{$isShuffle=0;}
				$testData['isShuffleQueOrder'] = $isShuffle;
				if($isFree){
					$isFree=1;
				}else{$isFree=0;}
				$testData['isFree'] = $isFree; 
				if($this->session->userdata('resellerId')){
					$resellerId=$this->session->userdata('resellerId');
				}else{
					$resellerId=0;
				}
				$testType=$this->input->post('testType'); 
				$where=array('testName'=>$testName,'testType'=>$testType,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				$ifTestNameExists=$this->Questionmodel->checkNameExistsInEdit(TBL_TESTS,$where,$testId);
				if($ifTestNameExists && count($ifTestNameExists)>0){
				$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Test Name Already existed.Please try with another Name.</strong> </div>");
			        redirect(current_url());
					
				}
				
				if($isFree == 0){
					if(empty($testAmount)){
						$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Test amount is required.</strong> </div>");
			             redirect(current_url());
					}
				}
				$where = array('p_test_id'=>$testId);
				if($tData){
				   $testAmountExceedsLimit= $this->checkTestAmountLimit($testAmount,$testId);
					if($testAmountExceedsLimit){
					$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Test amount must be less than ".$testAmountExceedsLimit['originalTestAmount'].".</strong> </div>");
			        redirect(current_url());
					}
				    $testId = $this->insertOrUpdate(TBL_PROMOTER_TESTS,$where,$tData);
				}
				if($testId)
			   {
				    if($testAmount){
				     	$this->updateCartAmount($testAmount,$testId);
                    }
				   $data['successMessage'] = SUCCESS;
				   redirect(QUETION_MANAGEMENT_URL,'refresh');
			   }else{
				    $data['errorMessage'] = ERROR;
			   }
				
			}
			$resellerId=$this->session->userdata('resellerId');
			$where = array('testId'=>$testId);
			$TestData = $this->getSingleRecord(TBL_TESTS,$where,'*');
			if(empty($TestData))
			{
				redirect(ADMIN_DASHBOARD_URL,'refresh');
			}else{
				$selleAmount=$this->getSingleRecord(TBL_PROMOTER_TESTS,$where=array('p_test_id'=>$testId,'p_t_promoter_id'=>$resellerId),'*'); 
				$TestData->testAmount=$selleAmount->p_t_sell_amount;
			}
			$Quedata = $this->getAllRecords(TBL_MAIN_TEST_QUESTIONS, array('test_id'=>$testId,'isActive'=>1,'isDeleted'=>0),'*');
			/*if($Quedata)
			{
				$i=0;
				foreach($Quedata as $Que)
				{
					$questions = $this->getSingleRecord(TBL_QUETIONS,array('id'=>$Que->que_id),'*');
					$where=array('que_id'=>$Que->que_id,'isActive'=>1,'isDeleted'=>0);
					$options = $this->getAllRecords(TBL_QUE_OPTIONS,$where,'*');
					$Quedata[$i]->options = $options;
					$Quedata[$i]->question = $questions->que;
					$Quedata[$i]->subject = $questions->subject_id;
					$Quedata[$i]->topic = $questions->topic_id;
				$i++;
				}
				
			}*/
			if($this->session->userdata('resellerId')){
			       $resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
			$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
			$data['subjects']=$this->getAllRecords(TBL_SUBJECTS,$where,'*');
			$data['topics']=$this->getAllRecords(TBL_TOPICS,$where,'*');

			$data['TestData'] = $TestData;
			$data['Quedata']  = $Quedata;
			$lastQuestion = 0;
			if( count($Quedata)>0){
					$lastQueNo = count($Quedata) - 1;
					$lastQuestion= $Quedata[$lastQueNo]->que_id;
			}
			
			$where=array('test_id'=> $testId,'isActive'=>1,'isDeleted'=>0,'isApproved'=>1);
	        $TotalActiveQuestions=$this->getAllRecords(TBL_TEST_QUESTIONS,$where,'*');
			$lastQuestiondata['QuestionNumber']=count($TotalActiveQuestions)+1;
			$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
			$data['courses']=$this->getAllRecords(TBL_COURSES,$where);
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/tests/edit_promoter_test',$data);
			$extraFooter = $this->load->view('dashboard/users_script',$lastQuestiondata);
		    $this->load->view('dashboard/includes/footer',$extraFooter);
			
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 	
	}
	function checkTestAmountLimit($newTestAmount,$id){
		    $where = array('testId'=>$id);
			$TestData = $this->getSingleRecord(TBL_TESTS,$where,'*');
			$originalTestAmount=$TestData->testAmount;
		      if($newTestAmount > $originalTestAmount ){
			      $result['originalTestAmount']=$originalTestAmount;
			      return $result;
				}else{
					return 0;
				} 
	}
	
	function uploadQueFromExcel()
	{    
	     $isSessionIn=$this->session->userdata('isSessionIn');
	if(empty($isSessionIn))
	{
	redirect(LOGOUT_URL,'refresh');
	}
	     $test=$this->uri->segment(4);
    	 $data='';   
	     $error_message='';
         $this->load->library('form_validation');
	    if(empty($_FILES['excelUsersFile']['name'])){
	 $this->form_validation->set_rules('excelUsersFile','Excel File','trim|required');
	}
	
	  $this->form_validation->set_rules('excelUsersFile','Excel file','trim');
	      if($this->form_validation->run()!=false)
	  {
	    $result=0;
	$target_path ='./uploads/';
	$rand=rand();
	    $xlsx_name=$rand.time().'.xlsx'; 
	$fileTypes = array('xlsx');
	if(!empty($_FILES['excelUsersFile']['name'])){ 
	       $target_path = $target_path . $xlsx_name;
	   $response['file_name'] = basename($_FILES['excelUsersFile']['name']);
	   $filename=basename($_FILES['excelUsersFile']['name']);
	   $file_extension=pathinfo($_FILES['excelUsersFile']['name']);
	    
	   if (in_array(strtolower($file_extension['extension']), $fileTypes)) {
	     
	         $movefile=move_uploaded_file($_FILES['excelUsersFile']['tmp_name'], $target_path);
	if($movefile){                 
	    //$target_path ='./uploads/';
	                        //$target_path = $target_path . 'oes1.xlsx'; 
	            try {
	$objPHPExcel = PHPExcel_IOFactory::load($target_path);
	} catch(Exception $e) {
	die('Error loading file "'.pathinfo($target_path,PATHINFO_BASENAME).'": '.$e->getMessage());
	}
                                    $allUsersInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	$usersCount = count($allUsersInSheet);
	$x=1;$y=2;
	$testId = trim($allUsersInSheet[$y]["A"]);
	$where=array('test_id'=>$testId);
	$testQuestions=$this->Questionmodel->getLastQuestionNumbers(TBL_QUETIONS,$where,'*');
	if($testQuestions){
	$lastQuestion=$testQuestions->que_id;
	}else{
	$lastQuestion=0;
	}
	$x=$lastQuestion+1;
	    for($i=2;$i<=$usersCount;$i++){
			//neatPrintAndDie($allUsersInSheet);
	$testId = trim($allUsersInSheet[$i]["A"]);
	if($testId==$test){	
	$testId = trim($allUsersInSheet[$i]["A"]);
	$QuesNo = trim($allUsersInSheet[$i]["B"]);
	$Quetion = trim($allUsersInSheet[$i]["C"]);
	$Option1 = trim($allUsersInSheet[$i]["D"]);
	$Option2 = trim($allUsersInSheet[$i]["E"]);
	$Option3 = trim($allUsersInSheet[$i]["F"]);
	$Option4 = trim($allUsersInSheet[$i]["G"]);
	$Option5 = trim($allUsersInSheet[$i]["H"]);
	$Option6 = trim($allUsersInSheet[$i]["I"]);
	$Option7 = trim($allUsersInSheet[$i]["J"]);
	$Option8 = trim($allUsersInSheet[$i]["K"]);
	$Option9 = trim($allUsersInSheet[$i]["L"]);
	$Option10 = trim($allUsersInSheet[$i]["M"]);
	$Option11 = trim($allUsersInSheet[$i]["N"]);
	$Option12 = trim($allUsersInSheet[$i]["O"]);
	
	$Ans = trim($allUsersInSheet[$i]["P"]);
	$hint = trim($allUsersInSheet[$i]["Q"]);
	//$Options = str_replace(' ', '', $Options);	
	$Ans = str_replace(' ', '', $Ans);	
	if($Quetion == "")
	{
	break;
	}
	
	
	$result = $this->Questionmodel->saveQue($Quetion,$hint,$testId,$x++);
	if($result){
	
	$queId = $result;	
                                            //$options= explode("#", $Options);	
                                            $answers= explode("#", $Ans);	
                                     	
	    /*for($j=0;$j<count($options);$j++)
	{
	$option =$options[$j];
	$isOption=0;
	if(in_array($option,$answers))
	{
	$isOption=1;
	}
	
	$suc = $this->Questionmodel->saveQueOptions($option,$isOption,$testId,$queId);
	}*/
	
	if(!empty($Option1))
	{
	$isOption=0;
	if(in_array($Option1,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option1,$isOption,$testId,$queId);
	}
	//print_r('dsdj');die();
	
	if(!empty($Option2))
	{
	$isOption=0;
	if(in_array($Option2,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option2,$isOption,$testId,$queId);
	}
	if(!empty($Option3))
	{
	$isOption=0;
	if(in_array($Option3,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option3,$isOption,$testId,$queId);
	}
	
	//option 4
	if(!empty($Option4))
	{
	$isOption=0;
	if(in_array($Option4,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option4,$isOption,$testId,$queId);
	}
	//option 5
	if(!empty($Option5))
	{
	$isOption=0;
	if(in_array($Option5,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option5,$isOption,$testId,$queId);
	}
	//option 6
	if(!empty($Option6))
	{
	$isOption=0;
	if(in_array($Option6,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option6,$isOption,$testId,$queId);
	}
	
	//option 7
	if(!empty($Option7))
	{
	$isOption=0;
	if(in_array($Option7,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option7,$isOption,$testId,$queId);
	}
	//option 8
	if(!empty($Option8))
	{
	$isOption=0;
	if(in_array($Option8,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option8,$isOption,$testId,$queId);
	}
	
	//option 9
	if(!empty($Option9))
	{
	$isOption=0;
	if(in_array($Option9,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option9,$isOption,$testId,$queId);
	}
	
	//option 10
	if(!empty($Option10))
	{
	$isOption=0;
	if(in_array($Option10,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option10,$isOption,$testId,$queId);
	}
	//option 11
	if(!empty($Option11))
	{
	$isOption=0;
	if(in_array($Option11,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option11,$isOption,$testId,$queId);
	}
	//option 12
	if(!empty($Option12))
	{
	$isOption=0;
	if(in_array($Option12,$answers))
	{
	$isOption=1;
	}
	$this->Questionmodel->saveQueOptions($Option12,$isOption,$testId,$queId);
	}
	
	$data['messege']="success";
	}else{
	$data['error_messege']="Error";
	}
	}$y++;
	
	 }
	  $messege =array('status'=>0,'message'=>SUCCESS);
	        }else{
	 $error_message =array('status'=>0,'message'=>FILE_NOT_UPLOADED);
	
	 }
	        }else{
	     $error_message =array('status'=>0,'message'=>MODULE_FILE_ALLOWED_TYPES);
	
	}
	     }
	}else{
	   $message = array('status' => 0,'message' =>validation_errors(),'result' => '');
	
	    }
	//$data['messege']=$messege;
	    //$data['error_message']=$error_message;
	    $this->load->view('dashboard/includes/header');
	    $this->load->view('dashboard/tests/add_excel_questions',$data);
	$extraFooter = $this->load->view('dashboard/users_script');
	    $this->load->view('dashboard/includes/footer',$extraFooter);
	
	}
	function downloadSheet()
	{
		//$file="/var/www/html/admin/uploads/12413325181500287905.xlsx";
		$file="/var/www/html/admin/uploads/2537010471501760235.xlsx";
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		ob_clean();
		flush();
		readfile($file);
		exit;
	}
	function images(){
		try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
           
			$data['images'] = $this->getAllRecords(TBL_IMAGES,$where=array('isDeleted'=>0),"*");
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/tests/images',$data);
			$extraFooter = $this->load->view('dashboard/users_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	}
	function addImage(){
		if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
		}else{
			$resellerId=0;
		}
		 if(empty($_FILES['library_file']['name'])){
				$error_message =array('status'=>0,'message'=>"provide  library file");
				echo json_encode($error_message);die();
		}
		
		
	    $target_path ='./uploads/images/';
	    $fileTypes = array('jpeg', 'png', 'jpg','gif','mp3','mp4','wma');
			if(!empty($_FILES['library_file']['name'])){
			  //$target_path = $target_path . basename($_FILES['library_file']['name']);
			  $response['file_name'] = basename($_FILES['library_file']['name']);
			  $filename=basename($_FILES['library_file']['name']);
			  $rand=rand();
			  $file_extension=pathinfo($_FILES['library_file']['name']);
			   $picname=$rand.time().'.'.strtolower($file_extension['extension']); 
			   $target_path = $target_path . $picname;
			    if (in_array(strtolower($file_extension['extension']), $fileTypes)) {
					$movefile=move_uploaded_file($_FILES['library_file']['tmp_name'], $target_path);
				    if($movefile){
						$message = array('status' => 1,'message' => 'success','result' => $movefile);
						$libraryData['image']=$picname;
						$libraryData['resellerId']=$resellerId;
                         $where=array();
						$this->insertOrUpdate(TBL_IMAGES,$where,$libraryData);
						echo json_encode(array('status' => 1,'message' => ADDING_SUCCESS,'result' => $movefile));die();
			          
				     }else{
					    $error_message =array('status'=>0,'message'=>FILE_NOT_UPLOADED);
						echo json_encode($error_message);die();
						
					 }
				}else{
						$error_message =array('status'=>0,'message'=>MODULE_FILE_ALLOWED_TYPES);
						echo json_encode($error_message);die();
						
			   }
			}
    
	}
	function deleteImage()
	{
	
		if($this->input->is_ajax_request())
		{
			$id = $this->input->post('id');
			$where = array('id'=>$id);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_IMAGES,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	function apply_test_code(){
		$resellerId=$this->session->userdata('resellerId');
		$isReseller=$this->session->userdata('isReseller');
       
		if($_GET['pCode'])
		{
		   $code=$_GET['pCode'];
		   $where = array('testCode'=>$code,'resellerId'=>0);
		   $testExists = $this->getSingleRecord(TBL_TESTS,$where,'*'); 
		   if($testExists){
			  $testId=$testExists->testId;
			  $where = array('p_test_id'=>$testId,'p_t_is_active'=>1,'p_t_is_deleted'=>0,'p_t_promoter_id'=>$resellerId);
		      $testExistsForPromoter = $this->getSingleRecord(TBL_PROMOTER_TESTS,$where,'*');
			  if($testExistsForPromoter){
				$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Test already existed in your account.</strong> </div>");
			    redirect(QUETION_MANAGEMENT_URL);
				  
			  }else{
				  $data['p_test_id']=$testId;
				  $data['p_t_promoter_id']=$resellerId;
				  $data['p_t_merchant_amount']=$testExists->testAmount;
				  $data['p_t_sell_amount']=$testExists->testAmount;
				  $where=array();
				  $res=$this->insertOrUpdate(TBL_PROMOTER_TESTS,$where,$data);
                  if($res){				 
				    $this->session->set_flashdata('Error', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Test added successfully.</strong> </div>");
			     }else{
					 $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Error occured.</strong> </div>");

				 }			
				 redirect(QUETION_MANAGEMENT_URL);
			  }
		   }else{
			   $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Enter valid code.</strong> </div>");
			   redirect(QUETION_MANAGEMENT_URL);
		   }
		   
			
		}else{
			$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>Enter valid code.</strong> </div>");
				 redirect(QUETION_MANAGEMENT_URL);
		}
	}

}
?>