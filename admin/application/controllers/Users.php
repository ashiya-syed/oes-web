<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Users extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Usermodel'); 
	}
	
	
	function index()
	{
		
		try{
			//30
			$resellerId=$this->session->userdata('resellerId'); 
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			if($this->session->userdata('resellerId')){
				$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
			
			//$data['users'] = $this->Usermodel->getallUsers();
			
			$where = array('isActive'=>1,'isDeleted'=>0,'testType'=>1,'resellerId'=>$resellerId);
			$data['tests'] = $this->getAllRecords(TBL_TESTS,$where,'*');
			$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$data['packages'] = $this->getAllRecords(TBL_PACKAGES,$where,'*');
			
			$startDate=$this->input->post('startdate');
            $enddate=$this->input->post('enddate');
			$month=$this->uri->segment(4);
			$data['users'] = $this->Usermodel->getallUsers($startDate,$enddate,$month);
			//new
			$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			if($this->session->userdata('isReseller') == 2 && $resellerId ){
			 $where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
			}
			
			$data['courses']=$this->getAllRecords(TBL_COURSES,$where);
			
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/users',$data);
			$extraFooter = $this->load->view('dashboard/users_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	}
	function updateUserPassword(){
		
		$data['id']=$_GET['id']; 
		if(isset($_POST['submit'])){
			$this->load->library('form_validation');
		    $this->form_validation->set_rules('update_npassword','Password','trim|required');
		    $this->form_validation->set_rules('update_cpassword','Confirm Password','trim|required|matches[update_npassword]');
	     if($this->form_validation->run()!=false)
		 {
		 $userId= $data['id'];
		 $password=trim($this->input->post('update_npassword'));
		 $where = array('userId' => $userId);
		 $pwd=$this->encrypt($password);
			   $userData['password'] = $pwd;
			   $userData['updatedTime'] = date('y-m-d h:i:s');
		       $result=$this->insertOrUpdate(TBL_USERS,$where,$userData);
			   if($result){
			    $this->session->set_flashdata('Error', "<div class='alert alert-success alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".PASSWORD_SUCCESSFULLY_UPDATED ."</strong> </div>");
			    redirect(USER_MANAGEMENT_URL);
			   }else{
				$this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".PASSWORD_UPDATE_FAILED ."</strong> </div>");
			   }
				
		 }else{
			 $this->session->set_flashdata('Error', "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".validation_errors() ."</strong> </div>");
		 }
		}
		$this->load->view('dashboard/includes/header');
		$this->load->view('dashboard/changeUserPwd',$data);
		$extraFooter = $this->load->view('dashboard/users_script');
		$this->load->view('dashboard/includes/footer',$extraFooter);
	}
}
?>