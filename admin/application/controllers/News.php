<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class News extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Newsmodel'); 
	}
	
	
	function index()
	{
		
		try{
			//30
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			/*$Role_Id=$this->session->userdata('Role_Id');
			if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
			{
				redirect(ADMIN_LOGIN_URL,'refresh');
			}*/
			
			$data['news'] = $this->Newsmodel->getNews();
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/news/news',$data);
			$extraFooter = $this->load->view('dashboard/users_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	}
}
?>