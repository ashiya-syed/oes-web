<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Ajaxhelper extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Usermodel'); 
		$this->load->model('Resellermodel'); 
		$this->load->model('Packagemodel'); 
				$this->load->model('Questionmodel');
				$this->load->model('Commonmodel');

	}
	
	
	function deleteUser()
	{
		if($this->input->is_ajax_request())
		{
			$userID = $this->input->post('userId');
			$where = array('userId'=>$userID);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_USERS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	function activateUser()
	{
		if($this->input->is_ajax_request())
		{
			$userID = trim($this->input->post('userId'));
			$id     = trim($this->input->post('id'));
			$where = array('userId'=>$userID);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_USERS,$where,$data);
			$record = $this->getSingleRecord(TBL_USERS,$where,'userId,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateUser($userID ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateUser($userID ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}function activateQuestion()
	{
		if($this->input->is_ajax_request())
		{
			$qId = trim($this->input->post('qId'));
			$id     = trim($this->input->post('id'));
			$where = array('id'=>$qId);
			$data['isActive'] = $id;
			$this->insertOrUpdate(TBL_MAIN_TEST_QUESTIONS,$whereqw=array('que_id'=>$qId),$data); //on 2nd
			$success = $this->insertOrUpdate(TBL_QUETIONS,$where,$data);
			$record = $this->getSingleRecord(TBL_QUETIONS,$where,'id,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateQuestion($qId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateQuestion($qId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	
	function addPackage()
	{
		if($this->input->is_ajax_request())
		{
			$packageName   = trim($this->input->post('packageName'));
			$packageAmount = trim($this->input->post('packageAmount'));
			$tests = $this->input->post('test');
			$tests=json_decode( $tests , true);
			$tests = implode(",", $tests);
			$tests = str_replace(" ","", $tests);
			
			$packageData['package_name']   = $packageName;
			$packageData['package_amount'] = $packageAmount;
			$packageData['package_test']   = $tests;
			
			$result = $this->insertOrUpdate(TBL_PACKAGES,array(),$packageData);
			if($result)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}else{
				echo json_encode(array('status'=>0,'message'=>ERROR));
			}
			die();
		}
	}
	
	function editPackage()
	{
		if($this->input->is_ajax_request())
		{
			$packageId  = trim($this->input->post('packageId'));
			$where = array('package_id'=>$packageId);
			$data['record'] = $this->getSingleRecord(TBL_PACKAGES,$where,'*');
			$where = array('isActive'=>1,'isDeleted'=>0);
			$data['tests'] = $this->getAllRecords(TBL_TESTS,$where,'*');
			$this->load->view('dashboard/packages/edit_packages_ajax',$data);
		}
	}
	
	function updatePackage()
	{
		if($this->input->is_ajax_request())
		{
			$packageId     = trim($this->input->post('packageId'));
			$packageName   = trim($this->input->post('packageName'));
			$packageAmount = trim($this->input->post('packageAmount'));
			$tests = $this->input->post('test'); 
			$tests=json_decode( $tests , true);
			$tests = implode(",", $tests);
			$tests = str_replace(" ","", $tests);
			$packageData['package_name']   = $packageName;
			$packageData['package_amount'] = $packageAmount;
			$packageData['package_test']   = $tests;
			$where = array('package_id'=>$packageId);
			$result = $this->insertOrUpdate(TBL_PACKAGES,$where,$packageData);
			if($result)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}else{
				echo json_encode(array('status'=>0,'message'=>ERROR));
			}
			die();
		}
	}
	
	function deletePackage()
	{
		if($this->input->is_ajax_request())
		{
			$packageId  = trim($this->input->post('packageId'));
			$where = array('package_id'=>$packageId);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_PACKAGES,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	function activatePackage()
	{
		if($this->input->is_ajax_request())
		{
			$packageId = trim($this->input->post('packageId'));
			$id        = trim($this->input->post('id'));
			$where = array('package_id'=>$packageId);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_PACKAGES,$where,$data);
			$record = $this->getSingleRecord(TBL_PACKAGES,$where,'package_id,isActive');
			$this->insertOrUpdate(TBL_USER_PACKAGES,$where=array('packageId'=>$packageId),$data);
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activatePackage($packageId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activatePackage($packageId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	
	
	function deleteTest()
	{
		if($this->input->is_ajax_request())
		{
			$testId  = trim($this->input->post('testId'));
			$where = array('testId'=>$testId);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_TESTS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	
	
	function deleteQue()
	{
		if($this->input->is_ajax_request())
		{
			$testId  = trim($this->input->post('testId'));
			$queId  = trim($this->input->post('que'));
			$where = array('test_id'=>$testId,'que_id'=>$queId);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_MAIN_TEST_QUESTIONS,$where,$data);
			$where = array('test_id'=>$testId,'isActive'=>1,'isDeleted'=>0);
		    $tests=$this->getAllRecords(TBL_MAIN_TEST_QUESTIONS,$where,$select="*");
			if($tests){
				$count=count($tests);
			}else{$count=0;}
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS,'count'=>$count));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	//newwwwwwww
	function deleteQuetion()
	{
		if($this->input->is_ajax_request())
		{
			//$testId  = trim($this->input->post('testId'));
			$queId  = trim($this->input->post('que'));
			$where = array('id'=>$queId);
			$data['isDeleted'] = 1;
			$this->insertOrUpdate(TBL_MAIN_TEST_QUESTIONS,$where1=array('que_id'=>$queId),$data);//on 2 nd
			$success = $this->insertOrUpdate(TBL_QUETIONS,$where,$data);
			$where = array('isActive'=>1,'isDeleted'=>0);
		    $tests=$this->getAllRecords(TBL_QUETIONS,$where,$select="*");
			if($tests){
				$count=count($tests);
			}else{$count=0;}
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS,'count'=>$count));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	function deleteRefQuestion()
	{
		if($this->input->is_ajax_request())
		{
			$id  = trim($this->input->post('que')); 
			$isApproved  = trim($this->input->post('isApproved')); 
			$where = array('id'=>$id,'isApproved'=>$isApproved,'isActive'=>1);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_TEST_QUESTIONS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	//qb
	function deleteOpt()
	{
		if($this->input->is_ajax_request())
		{
			$queId  = trim($this->input->post('que'));
			$option  = trim($this->input->post('opt'));
			$where = array('que_id'=>$queId,'option_id'=>$option);
			$data['isDeleted'] = 1;
			 $this->insertOrUpdate(TBL_MAIN_TEST_OPTIONS,$where,$data); //on 2 nd
			$success = $this->insertOrUpdate(TBL_QUE_OPTIONS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	//qb 
	function selectedQuestions(){
		$res=0;
		$test=trim($_POST['testid']);
		$check=@$_POST['checBNBNk'];
		$where=array('testId'=>$test);
		$testDetails=$this->getSingleRecord(TBL_TESTS,$where,'*');
		$totalQue=$testDetails->totalQuestions;
		
		$where=array();
		$totalQuestions=$this->getAllRecords(TBL_TEST_QUESTIONS,$where=array('isActive'=>1,'isDeleted'=>0,'test_id'=>$test));
			if($totalQuestions && count($totalQuestions)>0){
				$quesNo=count($totalQuestions);
			}
			else{
				$quesNo=0;
			}
			
			
			$testSelectQuestion=$quesNo+count($check);
			/* if($testSelectQuestion > $totalQue){
				$this->session->set_flashdata('Error',"<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong> You have selected more questions then Total Queston(".$totalQue.") of test.</strong> </div>");
				 echo json_encode(array('status'=>0,'message'=>"You have selected more questions then Total Queston(".$totalQue.") of test."));die(); 
			} */
			
			$i=1;
		if($check){	
		foreach($check as $checkId ){
			$where=array();
			$testData['que_id']=$checkId;
			$testData['test_id']=$test;
			$testData['isApproved']=0;
			$testData['que_s_no']=$quesNo+$i;
			$res=$this->insertOrUpdate(TBL_TEST_QUESTIONS,$where,$testData); //neatPrintAndDie($this->db->last_query());
			$i++;
		}
		} 
		 if($res)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
			}
			die();
		
		
		
	}
	
	
	function activateTest()
	{
		if($this->input->is_ajax_request())
		{
			$testId    = trim($this->input->post('testId'));
			$id        = trim($this->input->post('id'));
			$where = array('testId'=>$testId);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_TESTS,$where,$data);
			$record = $this->getSingleRecord(TBL_TESTS,$where,'testId,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateTest($testId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateTest($testId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	
	
	function getTestsByTestNames()
	{
		
		if($this->input->is_ajax_request())
		{
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
			$testName = trim($this->input->post('testname'));
			$packageId = trim($this->input->post('packageId'));
			$tests = $this->Packagemodel->getTests($testName);
			if($tests){
				
				if($packageId >0){
					    $where = array('package_id'=>$packageId);
			            $record = $this->getSingleRecord(TBL_PACKAGES,$where,'*');
				                 $selected=array();
								 $packageTests = explode(',', $record->package_test);
								 if($packageTests && count($packageTests) > 0){ 
										foreach( $packageTests as $key=>$val ){
													$selected[$key] = $val;
								 		}
								 }
				
					 foreach($tests as $key=>$val){
						    if(in_array($val->testId ,$selected))
							{
								$select="checked";
							}else{
								$select="";
						    }
							$class = "label label-warning";
							
							echo "
							<span class='".$class."'><input type='checkbox' name='test[]' value=".$val->testId." onclick='showPackgeTestNames(\"$val->testId\",\"$val->testName\", this.checked ? 1 : 0)' $select>$val->testName
							</span>";
							echo "</br>";
					}
				}else{
					 foreach($tests as $test){
							$class = "label label-warning";
							
							echo "
							<span class='".$class."'><input type='checkbox' name='test[]' value=".$test->testId." onclick='showPackgeTestNames(\"$test->testId\",\"$test->testName\", this.checked ? 1 : 0)'>$test->testName
							</span>";
							echo "</br>";
					}
				}
			}else {
				
				echo "<span style='color:red'>No Records</span>";
			}
			die();
			
		}
	}
	function getTestsByType()
	{
		
		if($this->input->is_ajax_request())
		{
			
			if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
			$type = trim($this->input->post('type'));
			$packageId = trim($this->input->post('packageId'));
			$where=array('testType'=>$type,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
			$tests=$this->getAllRecords(TBL_TESTS,$where,'*');
			if($tests){
				
				if($packageId >0){
					    $where = array('package_id'=>$packageId);
			            $record = $this->getSingleRecord(TBL_PACKAGES,$where,'*');
				                 $selected=array();
								 $packageTests = explode(',', $record->package_test);
								 if($packageTests && count($packageTests) > 0){ 
										foreach( $packageTests as $key=>$val ){
													$selected[$key] = $val;
								 		}
								 }
				
					 foreach($tests as $key=>$val){
						    if(in_array($val->testId ,$selected))
							{
								$select="checked";
							}else{
								$select="";
						    }
							$class = "label label-warning";
							
							echo "
							<span class='".$class."'><input type='checkbox' name='test[]' value=".$val->testId." onclick='showPackgeTestNames(\"$val->testId\",\"$val->testName\", this.checked ? 1 : 0)' $select>$val->testName
							</span>";
							echo "</br>";
					}
				}else{
					 foreach($tests as $test){
							$class = "label label-warning";
							
							echo "
							<span class='".$class."'><input type='checkbox' name='test[]' value=".$test->testId." onclick='showPackgeTestNames(\"$test->testId\",\"$test->testName\", this.checked ? 1 : 0)'>$test->testName
							</span>";
							echo "</br>";
					}
				}
			}else {
				
				echo "<span style='color:red'>No tests found with your search</span>";
			}
			die();
			
		}
	}
	
	
	/************************************
	* This Method will useful to create new user
	* Method:POST
	* @param userName
	* @param phoneNumber
	* @param emailAddress
	* @param password
	* @param cPassword
    * @responseType JSON 
	****************************************/
	
   /*  function addDummyUser(){
	$this->load->library('form_validation');
        $this->form_validation->set_rules('userName','Full Name','trim|required');
		$this->form_validation->set_rules('phoneNumber','Phone Number','trim|required|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('emailAddress','Email Address','trim|required|valid_email');
		$this->form_validation->set_rules('password','Password','trim|required|min_length[6]|max_length[10]');
        $this->form_validation->set_rules('cPassword','Confirm Password','trim|required|matches[password]');	
       
	   if($this->form_validation->run()!=false)
		{
			$emailAddress = trim($this->input->post('emailAddress'));
			$phoneNumber     = trim($this->input->post('phoneNumber'));
			$details=$this->Usermodel->user_exists($phoneNumber,$emailAddress);
			
			if($details){
			    $message = array('status' => 0,'message' =>PHONE_NUMBER_OR_EMAIL_ALREADY_EXISTS,'result' => '');
			   // $this->response(array($message), 200);
			
			}else{
			   $userName = trim($this->input->post('userName'));
			   $phoneNumber = trim($this->input->post('phoneNumber')); 
			   $emailAddress = trim($this->input->post('emailAddress'));
			   $password=trim($this->input->post('password'));
			   $password=encrypt($password);
			   $expireDateFormat = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")+3, date("Y"));
               $expirationDate = date("Y-m-d H:i:s",$expireDateFormat);
			   $token=md5(uniqid());
		       $userData['userName'] = $userName;
			   $userData['phoneNumber'] = $phoneNumber;
			   $userData['emailAddress'] = $emailAddress;
			   $userData['is_user_verified'] = 0;
			   $userData['accountExpiryDate'] = $expirationDate;
			   $userData['roleID'] = 2;
			   $userData['token'] = $token;
			   $userData['password'] = md5($password);

			   $where = array(); 
			   $result = $this->insertOrUpdate(TBL_USERS,$where,$userData);
			  if($result >0){ 
			        $subject="Verify your Account";
				//	$url='http://78.47.226.131/onyx_exams/Account/verifyAccount?token='.$token;
					//$url='http://localhost/onyx_educationals/Account/verifyAccount?token='.$token;
					$message= "Hi ".$userName.',';
					$message.= "We created your account in onyx educationals.Your Details are<br>
					<table><tr>Name:".$userName."</tr><tr>Mobile:".$phoneNumber."</tr>
					<tr>Email:".$emailAddress."</tr>
					<tr>Password:".$password."</tr></table><br>";
					
					$message.="<p>Please <a type='button' href='$url'  class='btn register_btn blue_btn mt5'>Click here</a> to check your packages or test details.</p>";
					$email_id=$emailAddress;
		            $smtpEmailSettings = $this->config->item('smtpEmailSettings');
		            $isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
		            $message = array('status' => 1,'message' => USER_REGISTRATION_SUCCESS);
			   }else{
				    $message = array('status' => 0,'message' => USER_REGISTRATION_FAILED,'result' =>"");
			   }
				
		   }
		}
		else
		{
			$message = array('status' => 0,'message'=>strip_tags(validation_errors()),'reason' => 'Validation error');
			
		}
		echo json_encode($message); die();
		
	 }  */
	 function addNewUser(){ 
		$this->load->library('form_validation');
        $this->form_validation->set_rules('userName','Full Name','trim|required');
		$this->form_validation->set_rules('phoneNumber','Phone Number','trim|required|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('emailAddress','Email Address','trim|required|valid_email');
		$this->form_validation->set_rules('password','Password','trim|required|min_length[6]|max_length[10]');
        $this->form_validation->set_rules('cPassword','Confirm Password','trim|required|matches[password]');	
		$this->form_validation->set_rules('adress','Adress','trim|required');	
		$this->form_validation->set_rules('course','Course','trim|required');	

       
	   if($this->form_validation->run()!=false)
		{
			$emailAddress = trim($this->input->post('emailAddress'));
			$phoneNumber     = trim($this->input->post('phoneNumber'));
			 $password=trim($this->input->post('password'));
			$details=$this->Usermodel->user_exists($phoneNumber,$emailAddress);
			
			//29
			if($this->session->userdata('resellerId')){
			    $resellerId=$this->session->userdata('resellerId');
			    $userName = trim($this->input->post('userName'));
			    $phoneNumber = trim($this->input->post('phoneNumber')); 
			    $emailAddress = trim($this->input->post('emailAddress'));
			    $details=$this->Usermodel->checkResellerUsersExists($phoneNumber,$emailAddress,$userName);
				$resellerNameExists=$this->Usermodel->checkResellerNameExists($userName);
				if($resellerNameExists){
					$message = array('status' => 0,'message' =>"User already existed with this name.Try another name.",'result' => '');
				    echo json_encode($message); die();
				}
			       $where=array('reseller_id'=>$resellerId); 
				   $resellerDetails=$this->getSingleRecord(TBL_RESELLERS,$where,'*');
				   if($resellerDetails){
					    $users_limit=$resellerDetails->reseller_usersLimit;
					   
				   }else{
					   $users_limit=$this->session->userdata('users_limit');
				   }
			       //$users_limit=$this->session->userdata('users_limit');
				   $where=array('isDeleted'=>0,'resellerId'=>$resellerId);
				  //$where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				   $userCount=$this->getAllRecords(TBL_USERS,$where,'*');
				   if($users_limit && $userCount && count($userCount)>=$users_limit){
			        $message = array('status' => 0,'message' =>"You already exceed your users limit.For further information contact admin.",'result' => '');
					echo json_encode($message); die();
				   }
			}else{
				$resellerId=0;
				$details=$this->Usermodel->user_exists($phoneNumber,$emailAddress);
			}
			if($details){
			   if($this->session->userdata('resellerId')){
				    $message = array('status' => 0,'message' =>"Phone number ,email and username must be unique for reseller user",'result' => '');
			   
			   }else{
			    $message = array('status' => 0,'message' =>PHONE_NUMBER_OR_EMAIL_ALREADY_EXISTS,'result' => '');
			   }
			}else{
				
			   $test="";$package="";
			   $userName = trim($this->input->post('userName'));
			   $phoneNumber = trim($this->input->post('phoneNumber')); 
			   $emailAddress = trim($this->input->post('emailAddress'));
			   $password=trim($this->input->post('password'));
			   $course=trim($this->input->post('course'));
			   $address=trim($this->input->post('address'));
			   $tests = trim($this->input->post('tests'));
			   $packages = trim($this->input->post('packages'));
			   //new
			   $adress = trim($this->input->post('adress'));
			   $accountType = trim($this->input->post('acntType'));
			   if($accountType == '2'){
			   $expireDateFormat = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")+3, date("Y")); 
               $expirationDate = date("Y-m-d H:i:s",$expireDateFormat);
			   $userData['accountExpiryDate'] = $expirationDate;
			   }
			   //end new
			   $token=md5(uniqid());
		       if(!empty($tests)) {
			     $test=json_decode($tests,TRUE);
				  $test = implode(',', $test);
		       }
			    
			   if(!empty($packages)) {
			     $package=json_decode($packages,TRUE);
				 $package = implode(',', $package);
		       }
			     
			   if(!empty($course)) {
			     $course=json_decode($course,TRUE);
				  $course = implode(',', $course);
		       }
			    $userData['course'] = $course;
			   $userData['userName'] = $userName;
			   $userData['phoneNumber'] = $phoneNumber;
			   $userData['testIds'] = $test;
			   $userData['packageIds'] = $package;
			   $userData['emailAddress'] = $emailAddress;
			   $userData['is_user_verified'] = 1;
			   //$userData['is_user_verified'] = 0;
			   $userData['roleID'] = 2;
			   $userData['token'] = $token; 
			   $pwd=$this->encrypt($password);
			   $userData['password'] = $pwd;
			   $userData['address'] = $adress;
			   $userData['accountType'] = $accountType;
			   $userData['createdTime'] =date('y-m-d h:i:s');
			   $userData['resellerId'] = $resellerId;
               $where = array(); 
			   $result = $this->insertOrUpdate(TBL_USERS,$where,$userData);
			  if($result >0){
                    $savePcakOrTest=$this->savePcakOrTest($test,$package,$result);				  
			        $subject="Verify your Account";
					$url=EMAIL_VERIFY_URL.$token;
					//need change
					$url2= FRONTENDURL.$userName;
					if($this->session->userdata('resellerId')){
						$emailData['url2']=$url2;
					}
					$emailData['url']=$url;
					$emailData['name']=$userName;
					$emailData['email']=$emailAddress;
					$emailData['phoneNumber']=$phoneNumber;
					$emailData['password']=$password;
					$emailData['accountType']=$accountType;
					$message=$this->load->view('verifyEmail', $emailData,true);
					/*$message= "Hi ".$userName.',';
					$message.= "We created your account in onyx educationals.Your Details are<br>
					<table><tr>Name:".$userName."</tr><tr>Mobile:".$phoneNumber."</tr>
					<tr>Email:".$emailAddress."</tr>
					<tr>Password:".$password."</tr></table><br>";
					if($this->session->userdata('resellerId'))
						$message.= "<tr>Url:".$url2."</tr>";
					if($accountType == 2){
						$message.="<p>Please <a type='button' href='$url'  class='btn register_btn blue_btn mt5'>Click here</a> to Verify your account.</p>";
					}else{
						$message.="<p>Please <a type='button' href='$url'  class='btn register_btn blue_btn mt5'>Click here</a> to check your packages or test details.</p>";
					}*/
					$email_id=$emailAddress;
		            $smtpEmailSettings = $this->config->item('smtpEmailSettings');
		            $isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
		            $message = array('status' => 1,'message' => USER_REGISTRATION_SUCCESS);
			   }else{
				    $message = array('status' => 0,'message' => USER_REGISTRATION_FAILED,'result' =>"");
			   }
				
				  		
			 }
		}
		else
		{
			$message = array('status' => 0,'message'=>strip_tags(validation_errors()),'reason' => 'Validation error');
			
		}
		echo json_encode($message); die();
		
	 } 
	 function savePcakOrTest($test,$package,$userId){
		              if($test){   
									$where=array();
									$updata['createdTime']= date("Y-m-d H:i:s");
									$updata['userId']=$userId;
									$updata['packageId']=0;
									$updata['isActive']= 1;
									$updata['isDeleted']= 0;
									$updata['packageTests']= $test;
									$updata['couponCode']= 0;
									$updata['txnid']= 0;
									$updata['amount']= 0;
									$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
									
								}
								 if($package){
									 $packageArray=explode(',',$package);
									 foreach($packageArray as $packary){
									$pId=$packary;
									$where=array('package_id'=>$pId);
									$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
									$testId=$packageDetails->package_test;
									$where=array();
									$updata['createdTime']= date("Y-m-d H:i:s");
									$updata['userId']=$userId;
									$updata['packageId']=$pId;
									$updata['isActive']= 1;
									$updata['packageTests']=$testId;
									$updata['amount']= 0;
									$updata['couponCode']= 0;
									$updata['txnid']= 0;
									$updata['isDeleted']= 0;
									$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
									   }
									}
	 }
	 function getTotalAmount(){
		
		 $tests = trim($this->input->post('tests'));
		 $packages = trim($this->input->post('packages')); 
		 if(!empty($tests)) {
			$test=json_decode($tests,TRUE);
		  }
		 $test = implode(',', $test);
		if(!empty($packages)) {
		   $package=json_decode($packages,TRUE);
		 }
		 $package = implode(',', $package);
		
        $res=$this->Usermodel->getAmount($package,$test);
		if($res){
        $message = array('pamount' => $res['pamount'],'tamount' => $res['tamount'],'amount'=>$res['pamount']+$res['tamount']);	
		echo json_encode($message); die();
		}
	 }
	 
	  /***************************
	 ***************************
	 * This service will useful to get the user details to update.
	 * @param userId
	 * @serviceType POST
	 * @responseType HTML
	 ****************************
	 ****************************/
	 function editUser()
	 {
		  $userId= trim($this->input->post('userId'));
		  $where = array('userId' => $userId);
		  $details=$this->getSingleRecord(TBL_USERS,$where);
		  $data['details']=$details;
		  
		  $this->load->view('dashboard/edit_users_ajax',$data);
	} 
	
	function updateUser()
	{
		 $userId = trim($this->input->post('userId'));
		 $user_name = trim($this->input->post('userName'));
		 $emailAddress = trim($this->input->post('emailAddress'));
		 $phoneNumber = trim($this->input->post('phoneNumber'));
		 $acntType = trim($this->input->post('acntType'));
		 
		 $data['userName']     = $user_name;        
		 $data['emailAddress'] = $emailAddress;  
		 $data['phoneNumber']  = $phoneNumber; 
        //new		 
		 $data['accountType']  = $acntType;
		 $data['updatedTime'] = date("Y-m-d H:i:s");
		 $where = array('userId'=>$userId);
		 $result=$this->insertOrUpdate(TBL_USERS,$where,$data);
		 if($result>0){
		   $message = array('status' => 1,'message' => SUCCESSFULLY_UPDATED);
		}else{
			$message = array('status' => 0,'message' => 'Error occured','result' =>'');
		}
		echo json_encode($message); die();
	}
	
	  /***************************
	 ***************************
	 * This service will useful to add news.
	 * @param news_title
	 * @param news_description
	 * @serviceType POST
	 * @responseType JSON
	 ****************************
	 ****************************/
	function addNews(){
		$this->load->library('form_validation');
        $this->form_validation->set_rules('news_title','News Title','trim|required');
		$this->form_validation->set_rules('news_description','Description','trim|required');
		if($this->form_validation->run()!=false)
		{
			$news_title = trim($this->input->post('news_title'));
			$news_description     = trim($this->input->post('news_description'));
			
			   $Data['newsTitle'] = $news_title;
			   $Data['description'] = $news_description;
			   $Data['createdTime'] = date("Y-m-d H:i:s");
			    if($this->session->userdata('resellerId')){
			      $resellerId=$this->session->userdata('resellerId');
			    }else{
				   $resellerId=0;
			    }
				 $Data['resellerId'] = $resellerId;
			   $where = array(); 
			   $result = $this->insertOrUpdate(TBL_NEWS,$where,$Data);
			   if($result >0){ 
		            $news['title']=$news_title;
		            $news['description']=$news_description;
		            $notifyString=json_encode($news);  
		            $where=array('isActive'=>1,'isDeleted'=>0);
					$fcmTokens=$this->Usermodel->getFcmTokens();
					if($fcmTokens){
					//$notiMessage['title'] = "New News from Onyx exams";
					  $notiMessage['text'] = $notifyString;
						foreach($fcmTokens as  $fcm){ 
								$token=$fcm->fcm_token;
							if($token){
								$fields = array(
									'data' => $notiMessage,
									'to' => $token
									);	
								  $json = json_encode($fields);
								  $send = sendFCMNoti($json);
							}
			            }
			       }  
					$message = array('status' => 1,'message' => ADDING_NEWS_SUCCESS);
			   }else{
				    $message = array('status' => 0,'message' => ADDING_NEWS_FAILED,'result' =>"");
			   }
		}
		else
		{
			$message = array('status' => 0,'message'=>strip_tags(validation_errors()),'reason' => 'Validation error');
			
		}
		echo json_encode($message); die();
		
	 } 
	
	
	
	  /***************************
	 ***************************
	 * This service will useful to get the news details to update.
	 * @param id
	 * @serviceType POST
	 * @responseType HTML
	 ****************************
	 ****************************/
	
	 function editNews()
	 {
		  $Id= trim($this->input->post('newsId'));
		  $where = array('id' => $Id);
		  $details=$this->getSingleRecord(TBL_NEWS,$where);
		  $data['details']=$details;
		  $this->load->view('dashboard/news/edit_news_ajax',$data);
	} 
	
	function updateNews()
	{
		 $id = trim($this->input->post('id'));
		 $updatenews_title = trim($this->input->post('updatenews_title'));
		 $updatenews_desc = trim($this->input->post('updatenews_desc'));
		 $data['newsTitle']     = $updatenews_title;        
		 $data['description'] = $updatenews_desc;  
		 $data['updatedTime'] = date("Y-m-d H:i:s");
		 $where = array('id'=>$id);
		 $result=$this->insertOrUpdate(TBL_NEWS,$where,$data);
		 if($result>0){
		   $message = array('status' => 1,'message' => SUCCESSFULLY_UPDATED);
		}else{
			$message = array('status' => 0,'message' => 'Error occured','result' =>'');
		}
		echo json_encode($message); die();
	}
	
	  /***************************
	 ***************************
	 * This service will useful to delete news.
	 * @param id
	 * @serviceType POST
	 * @responseType json
	 ****************************
	 ****************************/
	function deleteNews()
	{
		if($this->input->is_ajax_request())
		{
			$id = $this->input->post('id');
			$where = array('id'=>$id);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_NEWS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	
	  /***************************
	 ***************************
	 * This service will useful to activate or deactivate news.
	 * @param id
	 * @serviceType POST
	 * @responseType json
	 ****************************
	 ****************************/
	
	function activateNews()
	{
		if($this->input->is_ajax_request())
		{
			$newsId = trim($this->input->post('newsId'));
			$id     = trim($this->input->post('id'));
			$where = array('id'=>$newsId);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_NEWS,$where,$data);
			$record = $this->getSingleRecord(TBL_NEWS,$where,'id,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateNews($newsId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateNews($newsId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	function deleteCoupon()
	{
		if($this->input->is_ajax_request())
		{
			$id = $this->input->post('id');
			$where = array('coupon_id'=>$id);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_COUPONS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
   
	function editCoupon()
	 {
		  $couponId= trim($this->input->post('couponId'));
		  $where = array('coupon_id' => $couponId);
		  $details=$this->getSingleRecord(TBL_COUPONS,$where);
		  $where = array('c_coupon_id' => $couponId);
		  $details1 =$this->getSingleRecord(TBL_USER_COUPON,$where);
		  $userId =$details1->c_user_id;
		  $where = array('userId' => $userId);
		  $details2 =$this->getSingleRecord(TBL_USERS,$where,'*');
		  $data['details']=$details;
		  $data['users'] =$details2;
		  $this->load->view('dashboard/coupons/edit_coupons_ajax',$data);
	}
   
	function addCoupon(){
		$this->load->library('form_validation');
        $this->form_validation->set_rules('coupon_amount','Coupon Amount','trim|required');
		$this->form_validation->set_rules('coupon_validity','Coupon Validity','trim|required');
		$this->form_validation->set_rules('couponType','Coupon Type','trim|required');

		
		if($this->form_validation->run()!=false)
		{
			$users =trim($this->input->post('users'));
			$couponType =trim($this->input->post('couponType'));//2018
			$coupon_amount = trim($this->input->post('coupon_amount'));//2018
			$coupon_quantity = trim($this->input->post('coupon_quantity'));
			//$coupon_code=$this->generateRandNumber(8);
			$coupon_validity     =  date("Y-m-d H:i:s", strtotime($this->input->post('coupon_validity')) );
			 if(strtotime($coupon_validity) <= strtotime(date("Y-m-d H:i:s"))){
				      $message = array('status' => 0,'message' => CHECK_COUPON_VALIDITY,'result' =>"");			          
					  echo json_encode($message); die();
					 }
			  if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
			  }else{
				  $resellerId=0;
			  }
			  if($couponType == 2){
				 $Data['isPromocode'] = 2;
               }
			    $Data['resellerId'] = $resellerId;
               $Data['coupon_amount'] = $coupon_amount;			   
			   $Data['coupon_validity'] = $coupon_validity ;
			   $Data['coupon_createdTime'] = date("Y-m-d H:i:s");
			   if($coupon_quantity){
				  for($i=0;$i<$coupon_quantity;$i++){
					  $coupon_code=$this->generateRandNumber(8);
					  $Data['coupon_code'] = $coupon_code;
					  $where = array(); 
			          $result = $this->insertOrUpdate(TBL_COUPONS,$where,$Data);
				  }
				  
				  
			  }else{
				   $coupon_code=$this->generateRandNumber(8);
				  $Data['coupon_code'] = $coupon_code;
			      $where = array(); 
			      $result = $this->insertOrUpdate(TBL_COUPONS,$where,$Data);
			  
			  }
			   if($result >0){
                 if($users)
				  {
						  $data['c_user_id'] = $users;
						  $data['c_coupon_id'] = $result;
						  $where =array();
						  $this->insertOrUpdate(TBL_USER_COUPON,$where,$data);
				  }				   
					$message = array('status' => 1,'message' => ADDING_COUPNS_SUCCESS);
			   }else{
				    $message = array('status' => 0,'message' => ADDING_COUPNS_FAILED,'result' =>"");
			   }
		}
		else
		{
			$message = array('status' => 0,'message'=>strip_tags(validation_errors()),'reason' => 'Validation error');
			
		}
		echo json_encode($message); die();
		
	 } 
	
	
	function activateCoupon()
	{
		if($this->input->is_ajax_request())
		{
			$couponId = trim($this->input->post('couponId'));
			$id     = trim($this->input->post('id'));
			$where = array('coupon_id'=>$couponId);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_COUPONS,$where,$data);
			$record = $this->getSingleRecord(TBL_COUPONS,$where,'coupon_id,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateCoupon($couponId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateCoupon($couponId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	function updateCoupon()
	{
		 $id = trim($this->input->post('id'));
		 $updatecoupon_code = trim($this->input->post('updatecoupon_code'));
		 $updatecoupon_amount = trim($this->input->post('updatecoupon_amount'));
		 $updatecoupon_validity = trim($this->input->post('updatecoupon_validity'));
		 if(strtotime($updatecoupon_validity) <= strtotime(date("Y-m-d H:i:s"))){
				      $message = array('status' => 0,'message' => CHECK_COUPON_VALIDITY,'result' =>"");			          
					  echo json_encode($message); die();
					 }
			
		 $data['coupon_code']     = $updatecoupon_code;        
		 $data['coupon_amount'] = $updatecoupon_amount;  
		 $data['coupon_validity'] =date("Y-m-d H:i:s", strtotime( $updatecoupon_validity) );
		 $where = array('coupon_id'=>$id);
		 $result=$this->insertOrUpdate(TBL_COUPONS,$where,$data);
		 if($result>0){
		   $message = array('status' => 1,'message' => SUCCESSFULLY_UPDATED);
		}else{
			$message = array('status' => 0,'message' => 'Error occured','result' =>'');
		}
		echo json_encode($message); die();
	}
	
	 function addReseller(){ 
		$this->load->library('form_validation');
        $this->form_validation->set_rules('reseller_name','Reseller Name','trim|required');
        $this->form_validation->set_rules('reseller_email','Reseller Email','trim|required|valid_email');
		$this->form_validation->set_rules('reseller_mobile','Reseller Phone Number','trim|required');
		$this->form_validation->set_rules('reseller_password','Reseller password','trim|required|min_length[6]|max_length[10]');
		$this->form_validation->set_rules('reseller_cpassword','Reseller confirm password','trim|required|matches[reseller_password]');
        $account_type = trim($this->input->post('account_type'));
		if($account_type == 2 ){
			$this->form_validation->set_rules('reseller_packageLimit','Reseller Package Limit','trim|required');
			$this->form_validation->set_rules('reseller_testLimit','Reseller Tests Limit','trim|required');
			$this->form_validation->set_rules('reseller_usersLimit','Reseller Users Limit','trim|required');
		}
		if($this->form_validation->run()!=false)
		{
			$account_type = trim($this->input->post('account_type'));
			$reseller_email = trim($this->input->post('reseller_email'));
			$reseller_name = trim($this->input->post('reseller_name'));
			$reseller_mobile = trim($this->input->post('reseller_mobile'));
			$reseller_password     = trim($this->input->post('reseller_password'));
			$reseller_packageLimit     = trim($this->input->post('reseller_packageLimit'));
			$reseller_testLimit     = trim($this->input->post('reseller_testLimit'));
			$reseller_usersLimit     = trim($this->input->post('reseller_usersLimit'));
			$details=$this->Resellermodel->reseller_exists($reseller_name,$reseller_email);
			if($details){
			    $message = array('status' => 0,'message' =>"Already reseller existed with this name or email.Try Another one",'result' => '');
			}else{
			
			   $Data['isReseller'] = $account_type;
			   $Data['reseller_name'] = $reseller_name;
			   $Data['reseller_email'] = $reseller_email;
			   $Data['reseller_mobile'] = $reseller_mobile;
			   $pwd=$this->encrypt($reseller_password);
               //$Data['password'] = md5($reseller_password);
               $Data['password'] = $pwd;
			   $Data['reseller_packageLimit'] = $reseller_packageLimit;
			   $Data['reseller_testLimit'] = $reseller_testLimit ;
			   $Data['reseller_usersLimit'] = $reseller_usersLimit;
			   $Data['createdTime'] = date("Y-m-d H:i:s");
			   $where = array(); 
			   $result = $this->insertOrUpdate(TBL_RESELLERS,$where,$Data);
			   $subject="Reseller Account Details";
					$emailData['name']=$reseller_name;
					$emailData['email']=$reseller_email;
					$emailData['phoneNumber']=$reseller_mobile;
					$emailData['password']=$reseller_password;
					$emailData['users']=$reseller_usersLimit;
					$emailData['packages']=$reseller_packageLimit;
					$emailData['isReseller']=1;
					$emailData['tests']=$reseller_testLimit;
					$emailData['url2']=SITEURL.'/'.$reseller_name;
					/*$message= "Hi ".$reseller_name.',';
					$message.= "We created your account in onyx educationals.Your Details are<br>
					<table><tr>Name:".$reseller_name."</tr>
					<tr>Email:".$reseller_email."</tr>
					<tr>Mobile:".$reseller_mobile."</tr>
					<tr>Password:".$reseller_password."</tr>
					<tr>Users Limit:".$reseller_usersLimit."</tr>
					<tr>Packages Limit:".$reseller_packageLimit."</tr>
					<tr>Tests Limit:".$reseller_testLimit."</tr>
					<tr>Reseller Url: " .SITEURL.'/'.$reseller_name."</tr>
					</table><br>";*/
					$message=$this->load->view('verifyEmail', $emailData,true);
					$email_id=$reseller_email;
		            $smtpEmailSettings = $this->config->item('smtpEmailSettings');
		            $isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
			   if($result >0){ 
					$message = array('status' => 1,'message' => ADDING_RESELLER_SUCCESS);
			   }else{
				    $message = array('status' => 0,'message' => ADDING_RESELLER_FAILED,'result' =>"");
			   }
			}
		}
		else
		{
			$message = array('status' => 0,'message'=>strip_tags(validation_errors()),'reason' => 'Validation error');
			
		}
		echo json_encode($message); die();
		
	 } 
	function activateReseller()
	{
		if($this->input->is_ajax_request())
		{
			$resellerId = trim($this->input->post('resellerId'));
			$id     = trim($this->input->post('id'));
			$where = array('reseller_id'=>$resellerId);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_RESELLERS,$where,$data);
			$record = $this->getSingleRecord(TBL_RESELLERS,$where,'reseller_id,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateReseller($resellerId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateReseller($resellerId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	function deleteReseller()
	{
		if($this->input->is_ajax_request())
		{
			$id = $this->input->post('id');
			$where = array('reseller_id'=>$id);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_RESELLERS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	function editReseller()
	 {
		  $resellerId= trim($this->input->post('resellerId'));
		  $where = array('reseller_id' => $resellerId);
		  $details=$this->getSingleRecord(TBL_RESELLERS,$where);
		  $data['details']=$details;
		  $this->load->view('dashboard/resellers/edit_resellers_ajax',$data);
	} 
	function updateReseller()
	{
		 $id = trim($this->input->post('id'));
		 $updatereseller_name = trim($this->input->post('updatereseller_name'));
		 $updatereseller_package = trim($this->input->post('updatereseller_package'));
		 $updatereseller_test = trim($this->input->post('updatereseller_test'));
		 $updatereseller_user = trim($this->input->post('updatereseller_user'));
		 $emailAddress = trim($this->input->post('updatereseller_email'));
		  $updatereseller_mobile = trim($this->input->post('updatereseller_mobile')); 
		 
		 $data['reseller_email']     = $emailAddress;     
         $data['reseller_mobile']     = $updatereseller_mobile;		 
		 $data['reseller_name']     = $updatereseller_name;        
		 $data['reseller_packageLimit'] = $updatereseller_package;  
		 $data['reseller_testLimit'] = $updatereseller_test;  
		 $data['reseller_usersLimit'] =$updatereseller_user;
		 $data['updatedTime'] =date('y-m-d h:i:s');
		 $where = array('reseller_id'=>$id);
		
		 $result=$this->insertOrUpdate(TBL_RESELLERS,$where,$data);
		 if($result>0){
		   $message = array('status' => 1,'message' => SUCCESSFULLY_UPDATED);
		}else{
			$message = array('status' => 0,'message' => 'Error occured','result' =>'');
		}
		echo json_encode($message); die();
	}
	
	function addQuestionTo(){
		 $testTo = trim($this->input->post('test'));
		 $testFrom = trim($this->input->post('testFrom'));
		 
		 
		 $que = trim($this->input->post('que'));
		 $where=array('test_id'=>$testTo);
		 $testQuestions=$this->Questionmodel->getLastQuestionNumbers(TBL_QUETIONS,$where,'*');
			if($testQuestions){
				$lastQuestion=$testQuestions->que_id;
				$lastQuestion=$lastQuestion+1;
			}else{
				$lastQuestion=1;
			}
			$createdTime=date('y-m-d h:i:s');
		    $sql="insert into oes_quetions(que,test_id,que_id,hint,queType,isActive,isDeleted,createdTime ) 
		    select que,'$testTo','$lastQuestion',hint,queType,isActive,isDeleted,'$createdTime' from oes_quetions where (test_id=$testFrom and que_id= $que )";
		     $res=$this->db->query($sql);
			 $result=$this->Questionmodel->copyOptions($testTo,$testFrom,$que,$lastQuestion);
			if($result){
				foreach($result as $res){
					$data['option']=$res->option;
					$data['textType']=$res->textType;
					$data['is_answer']=$res->is_answer;
					$data['test_id']=$testTo;
					$data['que_id']=$lastQuestion;
					$data['isActive']=1;
					$data['isDeleted']=0;
					$data['createdTime']=date('y-m-d h:i:s');
					$where=array();
					$this->insertOrUpdate(TBL_QUE_OPTIONS,$where,$data);
				}
			}
			 if($res){
                 $message = array('status' => 1,'message' => ADDING_SUCCESS);
		    }else{
                
			 $message = array('status' => 0,'message' => 'Error occured','result' =>'');
		    }
		   echo json_encode($message); die();
	}
	//for subjects
	 function addSubject(){
		$this->load->library('form_validation');
        $this->form_validation->set_rules('subject','Subject','trim|required');
		
		
		if($this->form_validation->run()!=false)
		{
			$subject = trim($this->input->post('subject'));
			if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
			  }else{
				  $resellerId=0;
			  }
			    $where=array('sub_name'=>$subject,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				$ifsubtNameExists=$this->getAllRecords(TBL_SUBJECTS,$where,'*');
				if($ifsubtNameExists && count($ifsubtNameExists)>0){
				$message = array('status' => 2,'message'=>'Subject already existed.Please try with another name.','reason' => 'Validation error');
                echo json_encode($message); die();
				
				//redirect(SUBJECT_MANAGEMENT_URL);
				}
			   $Data['resellerId'] = $resellerId;
               $Data['sub_name'] = $subject;			   
			   $Data['createdTime'] = date("Y-m-d H:i:s");
			   $where=array();
			    $result = $this->insertOrUpdate(TBL_SUBJECTS,$where,$Data);
				 if($result >0){ 
					$message = array('status' => 1,'message' => ADDING_SUBJECT_SUCCESS);
			   }else{
				    $message = array('status' => 0,'message' => ADDING_SUBJECT_FAILED,'result' =>"");
			   }
		}
		else
		{
			$message = array('status' => 0,'message'=>strip_tags(validation_errors()),'reason' => 'Validation error');
			
		}
		echo json_encode($message); die();
		
	 }
	
	 function editSubject()
	 {
		  $id= trim($this->input->post('id'));
		  $where = array('id' => $id);
		  $details=$this->getSingleRecord(TBL_SUBJECTS,$where);
		  $data['details']=$details;
		  $this->load->view('dashboard/subjects/edit_subject_ajax',$data);
	} 
	
	function deleteSubject()
	{
		if($this->input->is_ajax_request())
		{
			$id = $this->input->post('id');
			$where = array('id'=>$id);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_SUBJECTS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
function updateSubject()
	{
		 $id = trim($this->input->post('id'));
		 $update_subject = trim($this->input->post('update_subject'));
		 if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
		 }else{
			  $resellerId=0;
		 }
		 $data['sub_name']     = $update_subject;        
		 $data['updatedTime']     = date('y-m-d h:i:s'); 
                $where=array('sub_name'=>$update_subject,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				$ifsubtNameExists=$this->Commonmodel->checkSubjectExists(TBL_SUBJECTS,$where,$id);
				if($ifsubtNameExists && count($ifsubtNameExists)>0){
				$message = array('status' => 2,'message'=>'Subject already existed.Please try with another name.','reason' => 'Validation error');
                echo json_encode($message); die();
				}
        		 
		 $where = array('id'=>$id);
		 $result=$this->insertOrUpdate(TBL_SUBJECTS,$where,$data);
		 if($result>0){
		   $message = array('status' => 1,'message' => SUCCESSFULLY_UPDATED);
		}else{
			$message = array('status' => 0,'message' => 'Error occured','result' =>'');
		}
		echo json_encode($message); die();
	}
	
 function activateSubject()
	{
		if($this->input->is_ajax_request())
		{
			$subId = trim($this->input->post('subId'));
			$id     = trim($this->input->post('id'));
			$where = array('id'=>$subId);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_SUBJECTS,$where,$data);
			$record = $this->getSingleRecord(TBL_SUBJECTS,$where,'id,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateSubject($subId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateSubject($subId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	//topics
	 function addTopic(){
		// neatPrintAndDie($_POST);
		$this->load->library('form_validation');
        $this->form_validation->set_rules('subject','Subject','trim|required');
        $this->form_validation->set_rules('topic','topic','trim|required');
		
		
		if($this->form_validation->run()!=false)
		{
			$subject = trim($this->input->post('subject'));
			$topic = trim($this->input->post('topic'));
			if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
			  }else{
				  $resellerId=0;
			  }
			    $where=array('topic_name'=>$topic,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				$ifsubtNameExists=$this->getAllRecords(TBL_TOPICS,$where,'*');
				if($ifsubtNameExists && count($ifsubtNameExists)>0){
				$message = array('status' => 2,'message'=>'Topic already existed.Please try with another name.');
                echo json_encode($message); die();
				//redirect(SUBJECT_MANAGEMENT_URL);
				}
			   $Data['resellerId'] = $resellerId;
               $Data['subject_id'] = $subject;			   
               $Data['topic_name'] = $topic;			   
			   $Data['createdTime'] = date("Y-m-d H:i:s");
			   $where=array();
			    $result = $this->insertOrUpdate(TBL_TOPICS,$where,$Data);
				 if($result >0){ 
					$message = array('status' => 1,'message' => ADDING_TOPIC_SUCCESS);
			   }else{
				    $message = array('status' => 0,'message' => ADDING_TOPIC_FAILED,'result' =>"");
			   }
		}
		else
		{
			$message = array('status' => 0,'message'=>strip_tags(validation_errors()),'reason' => 'Validation error');
			
		}
		echo json_encode($message); die();
		
	 } 
	 function editTopic()
	 {
		  $id= trim($this->input->post('id'));
		  $where = array('topic_id' => $id);
		  $details=$this->getSingleRecord(TBL_TOPICS,$where);
		  if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
			  }else{
				  $resellerId=0;
			  }
		  $where = array('isDeleted'=>0,'resellerId'=>$resellerId,'isActive'=>1);
          $data['subjects'] = $this->getAllRecords(TBL_SUBJECTS,$where,'*'); 
		  $data['details']=$details;
		 // neatPrintAndDie($data);
		  $this->load->view('dashboard/topics/edit_topic_ajax',$data);
	} 
	function deleteTopic()
	{
		if($this->input->is_ajax_request())
		{
			$id = $this->input->post('id');
			$where = array('topic_id'=>$id);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_TOPICS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	function updateTopic()
	{
		 $id = trim($this->input->post('id'));
		 $update_subject = trim($this->input->post('update_subject'));
		 $update_topic = trim($this->input->post('update_topic'));
		 if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
		 }else{
			  $resellerId=0;
		 }
		 $data['topic_name']     = $update_topic;        
		 $data['subject_id']     = $update_subject;        
		 $data['updatedTime']     = date('y-m-d h:i:s'); 
                $where=array('topic_name'=>$update_topic,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				$ifsubtNameExists=$this->Commonmodel->checkTopicExists(TBL_TOPICS,$where,$id);
				if($ifsubtNameExists && count($ifsubtNameExists)>0){
				$message = array('status' => 2,'message'=>'Topic already existed.Please try with another name.','reason' => 'Validation error');
                echo json_encode($message); die();
				}
        		 
		 $where = array('topic_id'=>$id);
		 $result=$this->insertOrUpdate(TBL_TOPICS,$where,$data);
		 if($result>0){
		   $message = array('status' => 1,'message' => SUCCESSFULLY_UPDATED);
		}else{
			$message = array('status' => 0,'message' => 'Error occured','result' =>'');
		}
		echo json_encode($message); die();
	}
	
	function activateTopic()
	{
		if($this->input->is_ajax_request())
		{
			$topicId = trim($this->input->post('topicId'));
			$id     = trim($this->input->post('id'));
			$where = array('topic_id'=>$topicId);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_TOPICS,$where,$data); 
			$record = $this->getSingleRecord(TBL_TOPICS,$where,'topic_id,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateTopic($topicId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateTopic($topicId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	//qb
	function topicsBySubject(){
		$id=$this->input->post('id');
		if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
		 }else{
			  $resellerId=0;
		 }
		 $where = array('isActive' => 1,'isDeleted' => 0,'subject_id'=>$id,'resellerId'=>$resellerId);
		 $result=$this->getAllRecords(TBL_TOPICS,$where);
	     echo "<option value=''>Select Topics</option>";
			  if($result && count($result)>0){
				foreach($result as $city){
			            echo "<option value=$city->topic_id>$city->topic_name</option>";
	            } 
	         }
			 die();
	
	}
	
	function questionBySearch(){
		$sub= trim($this->input->post('sub'));
		$topic= trim($this->input->post('topic'));
		$type= trim($this->input->post('type'));
		$id= trim($this->input->post('id'));
		   if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
			}else{
			   $resellerId = 0;
			}
			$where = array('isDeleted'=>0,'isActive'=>1,'resellerId'=>$resellerId);
			$subjects = $this->getAllRecords(TBL_SUBJECTS,$where,'*');
			$topics = $this->getAllRecords(TBL_TOPICS,$where,'*');
			$where = array('isDeleted'=>0,'subject_id'=>$sub,'topic_id'=>$topic,'queType'=>$type);
			$questions = $this->getAllRecords(TBL_QUETIONS,$where,'*');
			$data['questions']=$questions;
		//	$data['tests'] = $tests; 
			$data['topics'] = $topics; 
			$data['subjects'] = $subjects;  
			$data['id'] = $id;  
			$this->load->view('dashboard/questionBook/questions_ajax',$data);
			
	}
	function searchSelectedQuestions(){
		
		$sub= trim($this->input->post('sub'));
		$topic= trim($this->input->post('topic'));
		$type= trim($this->input->post('type'));
		$test= trim($this->input->post('test')); 
		if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
		}else{
			$resellerId=0;
			} 
        $where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
        $subjects=$this->getAllRecords(TBL_SUBJECTS,$where,'*');
        $topics=$this->getAllRecords(TBL_TOPICS,$where,'*');
        $data['subjects']=$subjects;
        $data['topics']=$topics;
		$data['questions']=$this->Questionmodel->searchSlectedQuestions($test,$sub,$topic,$type);
		$this->load->view('dashboard/tests/search_selected_qestions',$data);
		
	}
	//on 26th 
	function deleteSelectedQuesns(){ 
		$check=@$_POST['checBNBNk'];
		$i=1;
		if($check){	
		foreach($check as $checkId ){
			$queId=$checkId;
			$where=array('id'=>$queId);
			$data['isDeleted'] = 1;
			$this->insertOrUpdate(TBL_MAIN_TEST_QUESTIONS,$where1=array('que_id'=>$queId),$data);
			$success = $this->insertOrUpdate(TBL_QUETIONS,$where,$data);
			$i++;
		}
		   if($success){
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}else{
			}
			die();
		
	}
	}
	function deleteMultiRefQuesns(){
	
	if($this->input->is_ajax_request())
	{
	  $check=@$_POST['checBNBNk'];
	  if($check){	
			foreach($check as $checkId ){
				$id=$checkId;
				$where=array('id'=>$id,'isActive'=>1);
				$data['isDeleted'] = 1;
				$success=$this->insertOrUpdate(TBL_TEST_QUESTIONS,$where,$data);
			 }
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				
			}
			die();
			 
	   }
	 }
	}
	
	/* categorys */
	
	 function addCategory(){
		if(empty($_FILES['library_file']['name'])){
				$error_message =array('status'=>0,'message'=>"Course image is required");
				echo json_encode($error_message);die();
		}
		if(empty($_POST['category'])){
				$error_message =array('status'=>0,'message'=>"Course name is required.");
				echo json_encode($error_message);die();
		}
		if(empty($_POST['description'])){
				$error_message =array('status'=>0,'message'=>"Course description is required.");
				echo json_encode($error_message);die();
		}
       // $this->form_validation->set_rules('category','Category','trim|required');
		
		
		/* if($this->form_validation->run()!=false)
		{ */
			$courseName = trim($this->input->post('category'));
			if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
			  }else{
				  $resellerId=0;
			  }
			    $where=array('courseName'=>$courseName,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
				$ifcourseNameExists=$this->getAllRecords(TBL_COURSES,$where,'*');
				if($ifcourseNameExists && count($ifcourseNameExists)>0){
				$message = array('status' => 2,'message'=>'Course already existed.Please try with another name.','reason' => 'Validation error');
                echo json_encode($message); die();
				}
				//redirect(SUBJECT_MANAGEMENT_URL);
				//}
                           $data=array();
			               $data['resellerId'] = $resellerId;
                           $data['isActive']   = 1;
                           $data['isDeleted']   =0;
                           $data['courseName'] = $courseName;			   
                           $data['courseDescription'] = trim($this->input->post('description'));
                  /* upload course image */

                $target_path ='./uploads/courses/';
	            $fileTypes = array('jpeg', 'png', 'jpg','gif');
				if(!empty($_FILES['library_file']['name'])){
					
					$rand=rand();
			        $file_extension=pathinfo($_FILES['library_file']['name']); //neatPrintAndDie($file_extension);
			        $picname=$rand.time().'.'.strtolower($file_extension['extension']); 
			        $target_path = $target_path . $picname;
			        if (in_array(strtolower($file_extension['extension']), $fileTypes)) {
					    $movefile=move_uploaded_file($_FILES['library_file']['tmp_name'], $target_path);
				    if($movefile){
						   $data['courseImage'] = $picname;	
					}
					
				  }
				}
			   /* end */
			   $data['createdTime'] = date("Y-m-d H:i:s");
			   $where=array();
			    $result = $this->insertOrUpdate(TBL_COURSES,$where,$data);
				 if($result >0){ 
					$message = array('status' => 1,'message' => ADDING_COURSE_SUCCESS);
			   }else{
				    $message = array('status' => 0,'message' => ADDING_COURSE_FAILED,'result' =>"");
			   }
		/* }
		else
		{
			$message = array('status' => 0,'message'=>strip_tags(validation_errors()),'reason' => 'Validation error');
			
		} */
		echo json_encode($message); die();
		
	 }
	
	 function editCategory()
	 {
		  $courseId= trim($this->input->post('courseId'));
          $this->session->set_userdata('courseId',$courseId);
		  $where = array('courseId' => $courseId);
		  $details=$this->getSingleRecord(TBL_COURSES,$where);
		  $data['details']=$details;
		  $this->load->view('dashboard/category/edit_category_ajax',$data);
	} 
	
	function deleteCategory()
	{
		if($this->input->is_ajax_request())
		{
			$courseId = $this->input->post('courseId');
			$where = array('courseId'=>$courseId);
			$data['isDeleted'] = 1;
			$success = $this->insertOrUpdate(TBL_COURSES,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
function updateCategory()
	{ 
		 $courseId = trim($this->input->post('courseId'));
		 $update_category = trim($this->input->post('update_category'));
		 if($this->session->userdata('resellerId')){
			   $resellerId=$this->session->userdata('resellerId');
		 }else{
			  $resellerId=0;
		 }
		 $data['courseName']     = $update_category;        
		 $data['courseName']     = $update_category;        
		 $data['updatedTime']     = date('y-m-d h:i:s'); 
		 $data['courseDescription'] = trim($this->input->post('description'));
                $where=array('courseName'=>$update_category,'isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId); 
				$ifcourseNameExists=$this->Commonmodel->checkcourseNameExists(TBL_COURSES,$where,$courseId); 
				if($ifcourseNameExists && count($ifcourseNameExists)>0){
				$message = array('status' => 2,'message'=>'Course already existed.Please try with another name.','reason' => 'Validation error');
                echo json_encode($message); die();
				}  
        		 
		 $where = array('courseId'=>$courseId);
		  /* upload course image */
            if($_FILES['library_file']['name']){
                $target_path ='./uploads/courses/';
	            $fileTypes = array('jpeg', 'png', 'jpg','gif');
				if(!empty($_FILES['library_file']['name'])){
					
					$rand=rand();
			        $file_extension=pathinfo($_FILES['library_file']['name']); 
			        $picname=$rand.time().'.'.strtolower($file_extension['extension']); 
			        $target_path = $target_path . $picname;
			        if (in_array(strtolower($file_extension['extension']), $fileTypes)) {
					    $movefile=move_uploaded_file($_FILES['library_file']['tmp_name'], $target_path);
				    if($movefile){
						   $data['courseImage'] = $picname;	
					}
					
				  }
				}
            }
			
			   /* end */
		 $result=$this->insertOrUpdate(TBL_COURSES,$where,$data);
		 if($result>0){
		   $message = array('status' => 1,'message' => SUCCESSFULLY_UPDATED);
		}else{
			$message = array('status' => 0,'message' => 'Error occured','result' =>'');
		}
		echo json_encode($message); die();
	}
	
 function activateCategory()
	{
		if($this->input->is_ajax_request())
		{
			$courseId = trim($this->input->post('courseId'));
			$id     = trim($this->input->post('id'));
			$where = array('courseId'=>$courseId);
			$data['isActive'] = $id;
			$success = $this->insertOrUpdate(TBL_COURSES,$where,$data);
			$record = $this->getSingleRecord(TBL_COURSES,$where,'courseId,isActive');
			$html = "";
			$status=$record->isActive;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activateSubject($courseId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activateSubject($courseId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	
	
	/* end category */
	
	/* tests by selected courses */
	 function getPromoterDetails($resellerId){
		 $packageIds="";
		 $sql="SELECT  GROUP_CONCAT(p_package_id SEPARATOR ', ') AS packages FROM oes_promoter_packages where p_promoter_id =".$resellerId." AND p_is_deleted=0 AND p_is_active=1";
		 $query=$this->db->query($sql);
			 if($query){
				  $packages=$query->row();
			      $packageIds=$packages->packages;
			   } 
			   $data['packageIds']=$packageIds;
		$sql1="SELECT  GROUP_CONCAT(p_test_id SEPARATOR ', ') AS tests FROM oes_promoter_tests where p_t_promoter_id =".$resellerId."  AND p_t_is_deleted=0 AND p_t_is_active=1";
	    $query=$this->db->query($sql1); 
		if($query){
			$result=$query->row();
			$testIds=$result->tests;
			 
		 }
		  $data['testIds']=$testIds;
		  return $data;
	  }
		function saveCourseInSession(){ 
		$tests=$packages=array();
		$courses=$this->input->post('courses'); 
		//$courses=7; 
		$resellerId=($this->session->userdata('resellerId'))?$this->session->userdata('resellerId'):0;
        $tsql="SELECT  * FROM oes_tests WHERE course_id IN(".$courses.") AND `isActive` = 1 AND `isDeleted` =0 AND `resellerId` =  ".$resellerId."";
		$psql="SELECT  * FROM oes_packages WHERE courseId IN(".$courses.") AND `isActive` = 1 AND `isDeleted` =0 AND `resellerId` = ".$resellerId."";
		if($resellerId && $this->session->userdata('isReseller') == 2){
			$promoterDetails=$this->getPromoterDetails($resellerId);
			$testIds=$promoterDetails['testIds'];
			$packageIds=$promoterDetails['packageIds'];
	        $tsql="SELECT  * FROM oes_tests WHERE course_id IN(".$courses.") AND `isActive` = 1 AND `isDeleted` =0 AND  testId IN (".$testIds.")";
		    $psql="SELECT  * FROM oes_packages WHERE courseId IN(".$courses.") AND `isActive` = 1 AND `isDeleted` =0 AND  package_id IN (".$packageIds.") ";
		}
		$tquery = $this->db->query($tsql);
		$pquery = $this->db->query($psql);
		if($tquery){
			$tests=$tquery->result();
		}
		if($pquery){
			$packages=$pquery->result();
		}
		@$data['tests'] = $tests;
		@$data['packages'] = $packages; //print_r($tsql);die();
	    $filter_view =$this->load->view('dashboard/test_ajax',$data,true);
		echo $filter_view;die();
	}
	
	function deletePromoterTest()
	{
		if($this->input->is_ajax_request())
		{
			$testId  = trim($this->input->post('testId'));
			$resellerId=$this->session->userdata('resellerId');
			$where = array('p_test_id'=>$testId,'p_promoter_id'=>$resellerId);
			$data['p_t_is_deleted'] = 1;
			$success = $this->insertOrUpdate(TBL_PROMOTER_TESTS,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	function activatePromoterTest()
	{
		if($this->input->is_ajax_request())
		{
			$testId    = trim($this->input->post('testId'));
			$id        = trim($this->input->post('id'));
			$resellerId=$this->session->userdata('resellerId');
            $where     = array('p_test_id'=>$testId,'p_t_promoter_id'=>$resellerId); 
			$data['p_t_is_active'] = $id;
			$success = $this->insertOrUpdate(TBL_PROMOTER_TESTS,$where,$data);
			$record = $this->getSingleRecord(TBL_PROMOTER_TESTS,$where,'p_test_id,p_t_is_active');
			$html = "";
			$status=$record->p_t_is_active;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activatePromoterTest($testId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activatePromoterTest($testId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	function deletePromoterPackage()
	{
		if($this->input->is_ajax_request())
		{
			$packageId  = trim($this->input->post('packageId'));
			$resellerId=$this->session->userdata('resellerId');
			$where = array('p_package_id'=>$packageId,'p_promoter_id'=>$resellerId);
			$data['p_is_deleted'] = 1;
			$success = $this->insertOrUpdate(TBL_PROMOTER_PACKAGES,$where,$data);
			if($success)
			{
				echo json_encode(array('status'=>1,'message'=>SUCCESS));
			}
			else
			{
				echo json_encode(array('status'=>0));
			}
			die();
		}
	}
	function activatePromoterPackage()
	{
		if($this->input->is_ajax_request())
		{
			$packageId = trim($this->input->post('packageId'));
			$id        = trim($this->input->post('id'));
			$resellerId=$this->session->userdata('resellerId');
			$where = array('p_package_id'=>$packageId,'p_promoter_id'=>$resellerId);
			$data['p_is_active'] = $id;
			$success = $this->insertOrUpdate(TBL_PROMOTER_PACKAGES,$where,$data);
			$record = $this->getSingleRecord(TBL_PROMOTER_PACKAGES,$where,'p_package_id,p_is_active');
			//$this->insertOrUpdate(TBL_USER_PACKAGES,$where=array('packageId'=>$packageId),$data);
			$html = "";
			$status=$record->p_is_active;
			if($status == 1)
			{
				$html .= "<span class='btn btn-success' style='padding:4px' onclick='activatePromoterPackage($packageId ,0)'>Active</span>";
			}else{
				$html .= "<span class='btn btn-danger' style='padding:4px' onclick='activatePromoterPackage($packageId ,1)'>InActive</span>";
			}
			echo $html;
			die();
		}
	}
	
	
}

?>