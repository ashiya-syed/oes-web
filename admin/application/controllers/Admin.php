<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Admin extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Admin_model'); 
		$this->load->model('Resellermodel'); 
	}
	
	/*************
	**************
	This method will useful to login as a super admin.
	@param : Email_OR_phone
	@param : password
	return boolen
	**************
	*************/
	function index()
	{
		try{
				$this->load->helper('url');
				$data=array();
				if($this->session->userdata('Role_Id')){
					redirect(ADMIN_DASHBOARD_URL,'refresh'); 
				}
				//if (isset($_POST['Admin_LoginBtn'])) 
				if (!empty($_POST)) 
				{
					$this->session->unset_userdata('resellerId');
					$this->load->library('form_validation');
					$this->form_validation->set_rules('Email_OR_phone','Email or Phone Number','trim|required');
					$this->form_validation->set_rules('password','Password','trim|required');
					if($this->form_validation->run()!=false)
					{
						//Remember me set in to sessions
						$remember = trim($this->input->post('remember'));
						//print_r($remember);die();
						$this->session->set_userdata('remember',$remember);
						
						$Email_OR_phone = trim($this->input->post('Email_OR_phone'));
						$password       = trim($this->input->post('password'));
						$result = $this->Admin_model->login($Email_OR_phone, $password);
						//neatPrintAndDie($this->db->last_query());
						if($result) 
						{
							$session_remember=$this->session->userdata('remember');
							if(!empty( $session_remember) && $session_remember==1){
								
							  $cookie1= array('name'   => 'Superadmin_Name', 'value'  => $Email_OR_phone, 'expire' => '630720000','path'=>'/'  );
							  $cookie2= array('name'   => 'Superadmin_password', 'value'  => $password, 'expire' => '630720000', 'path'=>'/' );
							  $cookie3= array('name'   => 'Superadmin_remember', 'value'  => $session_remember, 'expire' => '630720000', 'path'=>'/' );
							    $this->input->set_cookie($cookie1);
							    $this->input->set_cookie($cookie2);
							    $this->input->set_cookie($cookie3);
						     }else {   
							  $cookie1= array('name'   => 'Superadmin_Name', 'value'  => '','expire' => time()-3600, );
							  $cookie2= array('name'   => 'Superadmin_password','value'  => '','expire' => time()-3600, );
							  $cookie3= array('name'   => 'Superadmin_remember','value'  => '','expire' => time()-3600,);
								$this->input->set_cookie($cookie1);
								$this->input->set_cookie($cookie2);
								$this->input->set_cookie($cookie3);
						    }
							
							$this->setuser_sessiondata($result); 
							
							$trackData['userId']=$result->userId;
							$trackData['ipAddresss']=$this->input->ip_address();
							$trackData['createdTime']=date("Y-m-d H:i:s");
							$this->insertOrUpdate(TBL_USER_TRACK,$where=array(),$trackData);
							$subject="Login Details";
							$email_id=$result->emailAddress;
		                    $smtpEmailSettings = $this->config->item('smtpEmailSettings');
							$messege="<strong>Hi ,".$result->userName." <br> <strong>".$trackData['ipAddresss']."</strong><p> is your last login IP address</p>";
		                    $isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$messege);
							
							$data['successMessage'] = LOGIN_SUCCESS;
							redirect(ADMIN_DASHBOARD_URL,'refresh'); 
						} 
						else 
						{
								$data['errorMessage'] = EMAIL_OR_NAME_AND_PASSWORD_NOT_MATCH;
						}
					}
					else {
						$data['errorMessage']=validation_errors();
					}
					
				}
				
				
		 }catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		 } 	
		
		
		$this->load->view('login',$data);
	}
	
	
	
	/*******************
	********************
	This method is useful to 
	logout the user.
	********************
	********************/
	function logout(){
           // $this->session->sess_destroy();
           // $this->session->unset_userdata($newdata);
			$this->session->unset_userdata('Role_Id');
            $this->session->unset_userdata('userId');
            $this->session->unset_userdata('User_Name');
            $this->session->unset_userdata('loggedIn');
            $this->session->unset_userdata('profilePicture');
            $this->session->unset_userdata('isSessionIn');
            $this->session->unset_userdata('emailAddress');
            redirect(SITEURL);
			
        }
	
	/*******************
	********************
	This method is useful to set the login user details into session
	********************
	********************/
	function setuser_sessiondata($result){ 
		if($result)
		   {
		        $userId = $result->userId;
			   	$User_Name = $result->userName;
			   	$email_address = $result->emailAddress;
				$Role_Id=$result->roleID;
				$profilePicture=$result->profilePicture;
			   	$newdata = array('userId'  => $userId,
			   	                 'User_Name'  => $User_Name,
			   	                 'emailAddress' => $email_address,
								 'Role_Id' => $Role_Id,
			   	                 'loggedIn' => TRUE,
								 'profilePicture' => $profilePicture,
								 'isSessionIn'  => 1,
								 'isReseller'=>0
			   			   );
						   
			   	$this->session->set_userdata($newdata);
			}
	}
	
	/*******************
	********************
	This method is useful to show the admin dashboard  after login as
	a admin only.
	********************
	********************/
	function d()
	{ 
		try{
			
			$Role_Id=$this->session->userdata('Role_Id');
			if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
			{
				redirect(LOGOUT_URL,'refresh');
			}
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		}
		$resellerId=0;
		$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId,'roleID'=>2);
        $usersCount = $this->getAllRecords(TBL_USERS,$where,'count(*) as users_count');
		$data['usersCount'] = $usersCount[0];
		$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
        $testsCount = $this->getAllRecords(TBL_TESTS,$where,'count(*) as tests_count');
		$data['testsCount'] = $testsCount[0];
        $packagesCount = $this->getAllRecords(TBL_PACKAGES,$where,'count(*) as packages_count');
		$data['packagesCount'] = $packagesCount[0];
		
		$where = array('isActive'=>1,'isDeleted'=>0);
		$UserPurchases = $this->getAllRecords(TBL_USER_PACKAGE,$where,'*');
		$amount=0;
		foreach($UserPurchases as $purchase){
			$uid=$purchase->userId;
			$resellerid=$this->Admin_model->getUserInfo($uid);
			if($resellerid == 0){
				$amount+=$purchase->amount;
			}
			
		}
		$data['amount']=$amount;
		$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
		$coupons = $this->getAllRecords(TBL_COUPONS,$where,'count(*) as coupons');
		$data['coupons'] = $coupons[0];
		$where = array('isActive'=>1,'isDeleted'=>0);
		$resellers = $this->getAllRecords(TBL_RESELLERS,$where,'count(*) as resellers');
		$data['resellers'] = $resellers[0];
        $this->load->view('dashboard/includes/dashboardheader');
		$this->load->view('dashboard/dashboard',$data);
		$this->load->view('dashboard/includes/footer');
	}
	function profile(){
           $Role_Id=$this->session->userdata('Role_Id');
		   
           $userId=$this->session->userdata('userId');
		 if(empty($userId))
		{
			redirect(LOGOUT_URL,'refresh');
		}
		
		 
		 if($Role_Id != SUPERADMIN_ROLE_ID && empty($Role_Id))
		{
			redirect(LOGOUT_URL,'refresh');
		}
		
		$where = array('userId' => $userId,'roleID'=>$Role_Id);
		$details=$this->getSingleRecord(TBL_USERS,$where);
		$data['details']=$details;
        $this->load->view('dashboard/includes/dashboardheader');
		$this->load->view('dashboard/users_script');
		$this->load->view('dashboard/profile',$data);
		$this->load->view('dashboard/includes/footer');
		
		}
		
	function updateProfile()
	 {
		
		 $userId = trim($this->input->post('userId'));
		 $name = trim($this->input->post('name'));
		 $emailAddress = trim($this->input->post('emailAdress'));
		 $phoneNumber = trim($this->input->post('number'));
		
		 if(!empty($name))        { $data['userName']= $name;                      }
		 if(!empty($emailAddress)){ $data['emailAddress']= $emailAddress;          }
		 if(!empty($phoneNumber)) { $data['phoneNumber']= $phoneNumber;            }
		
         $data['updatedTime'] = date("Y-m-d H:i:s");
		 $where = array('userId'=>$userId);
		 $result=$this->insertOrUpdate(TBL_USERS,$where,$data);
		 if($result>0){
				   $message = array('status' => 1,'message' => SUCCESSFULLY_UPDATED,'result' => $result);
			    }else{
			
				    $error_message = array('status' => 0,'message' => 'Error occured','result' =>'');
			    }
				echo json_encode($message); die();
	 }
	 function editAdminPassword()
	 {
		  $userId= trim($this->input->post('userId'));
		  $where = array('userId' => $userId);
		  $details=$this->getSingleRecord(TBL_USERS,$where);
		  $data['details']=$details;
		  
		  $where = array('isDeleted' => 0,'isActive'=>1);
		  $this->load->view('dashboard/password_update_ajax',$data);
	}
	 function updateSuperAdminPassword()

	{
		
		 $this->load->library('form_validation');
		 $this->form_validation->set_rules('new_password','Password','trim|required');
		 $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[new_password]');
	     if($this->form_validation->run()!=false)
		 {
		 $userId= trim($this->input->post('userId'));
		 $update_password=trim($this->input->post('update_password'));
		 $where = array('userId' => $userId);
		 $details=$this->getSingleRecord(TBL_USERS,$where);
		
			 $oldpwd=$this->decrypt($details->password); 
			 if($oldpwd == $update_password)
			 { 
				 $new_password = trim($this->input->post('new_password'));
				 $confirm_password = trim($this->input->post('confirm_password'));
				if(!empty($new_password)){ $superadmin_data['password']=$this->encrypt($new_password);    }
				 $superadmin_data['updatedTime'] = date("Y-m-d H:i:s");
				 $where = array('userId'=>$userId);
				 $result=$this->insertOrUpdate(TBL_USERS,$where,$superadmin_data);
				
				 if($result>0){
						   $message = array('status' => 1,'message' => PASSWORD_UPDATE_SUCCESS,'result' => $result);
						}else{
					
							$error_message = array('status' => 0,'message' => 'Error occured','result' =>'');
						}
			 }
			 else
			 {
				 $message = array('status' => 0,'message' =>'Current Password is not Correct','result' => '');
			 }

				}
		
		else
		{
			$message = array('status' => 0,'message' =>validation_errors(),'result' => '');
			
		}
		echo json_encode($message); die();
		 
	}
	
	
	
	
}