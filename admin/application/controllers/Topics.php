<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Topics extends Healthcontroller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
	}
	
	
	function index()
	{
		
		try{
			$resellerId=$this->session->userdata('resellerId');
			$isSessionIn=$this->session->userdata('isSessionIn');
			if(empty($isSessionIn)  && empty($resellerId))
			{
				redirect(LOGOUT_URL,'refresh');
			}
			if($this->session->userdata('resellerId')){
				$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
			$where = array('isDeleted'=>0,'resellerId'=>$resellerId,'isActive'=>1);
			$data['subjects'] = $this->getAllRecords(TBL_SUBJECTS,$where,'*');
			$where = array('isDeleted'=>0,'resellerId'=>$resellerId);
			//$data['topics'] = $this->getAllRecords(TBL_TOPICS,$where,'*');
			$data['topics'] = $this->getAllRecordsByDesc(TBL_TOPICS,$where,'topic_id','*');
			$this->load->view('dashboard/includes/header');
		    $this->load->view('dashboard/topics/topics',$data);
			$extraFooter = $this->load->view('dashboard/users_script');
		    $this->load->view('dashboard/includes/footer',$extraFooter);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
		
	}
	
}
?>