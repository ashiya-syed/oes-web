<?php

Class Usermodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_USERS;
    protected $_order_by = 'userId desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
    
     /* function getallUsers()
      {
		 $this->db->select('userId,userName,emailAddress,phoneNumber,isActive,roleID,profilePicture,updatedTime,lastLogin'); 
		 $this->db->from(TBL_USERS);
		 $this->db->where('isDeleted',0);
		 $this->db->where('roleID',USER_ROLE_ID);
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->result():'';
    }  */
function getallUsers($startDate="",$enddate="",$month="")
      {
		  if($this->session->userdata('resellerId')){
		  $resellerId=$this->session->userdata('resellerId');
		  }else{
				$resellerId=0;
				 }
		  if($startDate){
			$sdate=date('Y-m-d', strtotime($startDate));
		    $query="select * from oes_users where isDeleted=0 AND roleID=2 and DATE(`createdTime`) = '$sdate' and resellerId =$resellerId  and isPromotionMember = 0 order by userId desc";
			
		 }else if($startDate && $enddate){
			 $edate=date('Y-m-d', strtotime($enddate));
			 $query= "select * from oes_users where isDeleted=0 AND roleID=2 and createdTime >=$sdate and createdTime<=$edate  and isPromotionMember = 0 order by userId desc";
		 }else if($month){
			  $query="select * from oes_users where isDeleted=0 AND roleID=2 and month(createdTime)=$month and resellerId =$resellerId  and isPromotionMember = 0 order by userId desc";
			 
		 }
		 else{
			 $query="SELECT * from oes_users where isDeleted=0 AND roleID=2 and resellerId =$resellerId  and isPromotionMember = 0 order by userId desc";
		 } 
			
		
      
        /*if($startDate){
			$sdate=date('Y-m-d', strtotime($startDate));
		    $query="select * from oes_users where isDeleted=0 AND roleID=2 and DATE(`createdTime`) = '$sdate' ";
		}else if($startDate && $enddate){
			 $edate=date('Y-m-d', strtotime($enddate));
			 $query= "select * from oes_users where isDeleted=0 AND roleID=2 and createdTime >=$sdate and createdTime<=$edate";
		 }else if($month){
			  $query="select * from oes_users where isDeleted=0 AND roleID=2 and month(createdTime)=$month ";
		 }
		 else{
			 $query="SELECT * from oes_users where isDeleted=0 AND roleID=2 ";
		 }*/
	 
		  //neatPrintAndDie( $query);
          $res=$this->db->query($query)->result();
          return $res;
		
    }   	
	
	public function user_exists($phoneNumber,$email_address){
   
       // $query = $this->db->query("SELECT userId,phoneNumber,emailAddress FROM oes_users WHERE (phoneNumber='".$phoneNumber."' OR emailAddress='".$email_address."') AND isDeleted=0 AND is_user_verified=1");
        $query = $this->db->query("SELECT userId,phoneNumber,emailAddress FROM oes_users WHERE (phoneNumber='".$phoneNumber."' OR emailAddress='".$email_address."') AND isDeleted=0 and resellerId=0 and isPromotionMember = 0 and roleID =2");
        //neatPrintAndDie($this->db->last_query());
		if (!$query)
		{
		     $this->throwException($query);
		}else{
			if($row = $query->row()){
             return $query->row();
			}else{
				 return FALSE;
			}
		}
        
	}
	function getAmount($pack,$tests=""){
		      $pamount =""; $tamount="";
				 if($pack){
					$res=$this->db->query("SELECT sum(package_amount) as packageAmount FROM oes_packages WHERE package_id IN ($pack)")->row();
				    $pamount=$res->packageAmount;
				 }
				 if($tests)
				 {
					$res=$this->db->query("SELECT sum(testAmount) as testAmount FROM oes_tests WHERE testId IN ($tests)")->row();
				    $tamount=$res->testAmount; 
					
				 }
			   $amount['pamount']=$pamount;
			   $amount['tamount']=$tamount;
		       return $amount;
	}
	function getIpAddress($id){
		
		 $this->db->select('ipAddresss');  //31
		 $this->db->from(TBL_USER_TRACK);
		 $this->db->where('userID',$id);
		
		 $this->db->order_by('id','DESC');
		 $this->db->LIMIT(1);
         $query = $this->db->get();
		 $RES=$query->row();
		 return $RES;
	}
	function checkResellerNameExists($userName){
		
		if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
		}else{
			$resellerId=0;
		}
		$query = $this->db->query("SELECT userId,phoneNumber,emailAddress FROM oes_users WHERE (userName='".$userName."') AND isDeleted=0 AND isActive=1 and isPromotionMember = 0 ");
		if (!$query)
		{
          $this->throwException($query);
		}else{
			$row = $query->row();
			if($row){
             return $row;
			}else{
				 return FALSE;
			}
		}
	}
	function checkResellerUsersExists($phone_number,$email_address,$userName){
		
		if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
		}else{
			$resellerId=0;
		}
		$query = $this->db->query("SELECT userId,phoneNumber,emailAddress FROM oes_users WHERE (phoneNumber='".$phone_number."' OR emailAddress='".$email_address."' OR userName='".$userName."') AND isDeleted=0 AND isActive=1 and isPromotionMember = 0 and resellerId=".$resellerId);
		if (!$query)
		{
          $this->throwException($query);
		}else{
			$row = $query->row();
			if($row){
             return $row;
			}else{
				 return FALSE;
			}
		}
	}
	function getUserPackages($id){
		 $finalres=array();
		 $this->db->select('packageId'); 
		 $this->db->from(TBL_USER_PACKAGE);
		 $this->db->where('userId',$id);
		 $this->db->where('isActive',1);
		 $this->db->where('isDeleted',0);
		 $this->db->where('packageId!=',0);
		 $this->db->order_by('id','DESC');
		$query = $this->db->get();
		 $RESULT=$query->result();
		 if($RESULT){
			 $stringArray=array();
			 foreach($RESULT as $res){
				 $stringArray[]=$res->packageId;
			 }
		
		 $string=array_unique($stringArray);
		 $string=implode(',',$string);
		 $sql="select  package_name from oes_packages where  package_id in ($string)";
		 $finalres=$this->db->query($sql)->result();
		  }
		 
		 return $finalres;
	}
	function courseNames($course){
		$sql="select * from oes_courses where courseId in (".$course.")";
		$query=$this->db->query($sql);
		if($query){
			$courses=$query->result();
			if($courses){
				$courseName="";
			foreach($courses as $course){
				$courseName.=$course->courseName.',';
			}
			$course=rtrim($courseName,',');
			return $course;
			}
		}else{
			return "-";
		}
	
	}
}








