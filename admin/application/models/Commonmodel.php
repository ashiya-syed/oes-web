<?php

Class Commonmodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_NEWS;
    protected $_order_by = 'id desc';
    protected $_timestamps = TRUE;
   
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
	function libraryExists($table,$where,$id){
		$this->db->select('*');
	    $this->db->from($table);
	    $this->db->where($where);
	    $this->db->where('id!=',$id);
	    $query = $this->db->get();
	    $result=$query->result(); 
	     return $result;
	}
    function checkTopicExists($table,$where,$id){
		$this->db->select('*');
	    $this->db->from($table);
	    $this->db->where('resellerId',$this->session->userdata('resellerId'));
	    $this->db->where($where);
	    $this->db->where('topic_id!=',$id);
	    $query = $this->db->get();
	    $result=$query->result(); 
	 return $result;
	} 
	function checkSubjectExists($table,$where,$id){
		$this->db->select('*');
	    $this->db->from($table);
	    $this->db->where($where);
	    $this->db->where('resellerId',$this->session->userdata('resellerId'));
        $this->db->where('id!=',$id);
	   $query = $this->db->get();
	   $result=$query->result(); 
	  return $result;
	}
     function checkcourseNameExists($table,$where,$id){ 
		$this->db->select('*');
	   $this->db->from($table);
	   $this->db->where($where);
	   $this->db->where('resellerId',$this->session->userdata('resellerId'));
       $this->db->where('courseId!=',$id);
	   $query = $this->db->get();
	   $result=$query->result();
	  return $result;
	}
      
	
	
}








