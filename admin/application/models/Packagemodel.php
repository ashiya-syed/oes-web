<?php

Class Packagemodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_TESTS;
    protected $_order_by = 'testId desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
    
      function getalltestsByPackage($ids)
      {
		 $this->db->select('*'); 
		 $this->db->from(TBL_TESTS);
		 $this->db->where('isDeleted',0);
		 $this->db->where_in('testId',$ids);
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->result():'';
    }   
	
	
	function getTests($txt)
	{
		//29
		if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
	 	 $this->db->select('*'); 
		 $this->db->from(TBL_TESTS);
		 $this->db->where('isDeleted',0);
		 $this->db->where('isActive',1);
		 $this->db->where('resellerId',$resellerId);
		 $this->db->like('testName',$txt);
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->result():'';
	}
	function checkPackageNameExistsInEdit($table,$where,$id){
		$this->db->select('*');
	   $this->db->from($table);
	   $this->db->where($where);
	$this->db->where('package_id!=',$id);
	$query = $this->db->get();
	$result=$query->result(); 
	 return $result;
	}
	function getPromoterPackages($id){
		    $newpacks=array();
		    $sql2="SELECT  GROUP_CONCAT(p_package_id SEPARATOR ', ') AS packages FROM oes_promoter_packages where p_promoter_id =".$id." AND p_is_deleted=0";
			$query=$this->db->query($sql2);
			if($query){
				$packages=$query->row();
			    $packageIds=$packages->packages;
			} 
			if(empty($packageIds)){
				return array();
			}
			$sql="SELECT * FROM `oes_packages` WHERE isDeleted = 0  ";
			/* if($id){
				$sql.=" AND `resellerId`= ".$id."";
			} */
			if($packageIds){
				$sql.=" AND package_id IN (".$packageIds.")";
			}
			$sql.=" order by package_id desc ";
			
			 $query=$this->db->query($sql); 
			 if($query->num_rows() >0){
				 $packages=$query->result();
				 if($packages){
					 $newpacks=$query->result();
				      foreach($newpacks as $pack){
					      $amountsql="select p_sell_amount from oes_promoter_packages where p_promoter_id = ".$id." and p_package_id = ".$pack->package_id."";
					      $amount=$this->db->query($amountsql)->row();
						  $pack->package_amount=$amount->p_sell_amount;
					} 
				 }
				 return $newpacks;
			 }else{
				 return array();
			 }
		
	}
	function updateCart($userIdsList,$amount,$id,$type){
	$sql="UPDATE `oes_cart` SET amount=".$amount." WHERE itemId=".$id." AND itemType = ".$type." AND userId IN (".$userIdsList.") AND isActive =1 AND isDeleted = 0";
   // print_r($sql);die();
	$query=$this->db->query($sql);
	if($query){
		return true;
	}else{}
	return false;
}
}








