<?php

Class Resellermodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_USERS;
    protected $_order_by = 'resller_id desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
		$this->CI->load->helper('common_helper');

    }
     
	function getallResellers()
      {
      
			 $query="SELECT * from oes_resellers where isDeleted=0 order by reseller_id desc";
		     $result=$this->db->query($query)->result();
			 if($result){
				 foreach($result as $res){
				 $id=$res->reseller_id;
				
				 $res->userCount=$this->getCount(TBL_USERS,$id);
				  $res->packageCount=$this->getCount(TBL_PACKAGES,$id);
				  $res->testsCount=$this->getCount(TBL_TESTS,$id);
				
				 }
				 }
			
			 
            return $result;
		
    }  
	function getCount($table,$id){
		 $where=array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$id);
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get(); 
		return ($query->num_rows() >0)?count($query->result()):'';
	
	}
function getResellerPackage($packages,$id){
	$query="select package_id,package_name,package_test from oes_packages where package_id in ($packages) "; 
	$result=$this->db->query($query)->result();
	if($result){
		/*foreach($result as $re){
		$packTest=$re->package_test;
	    $re->packageTests=$this->db->query("select testId,testName from oes_tests where testId in ($packTest)")->result();
		}*/
		return $result;
	}
	else{
		return array();
	}
	
}	
function getResellerUsers($userIds,$id){
	$query="select userName,userId from oes_users where userId in ($userIds) ";
	$result=$this->db->query($query)->result();
	if($result)
		return $result;
	else
		return array();
	
}
function getResellerTests($test,$id){
	$result=$this->db->query("select testId,testName from oes_tests where testId in ($test) ")->result();
	if($result)
		return $result;
	else
		return array();
	
}	
function login($Email_or_Phone,$password)
    {
      
        $this->db->select('*');
		$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$this->db->where("(reseller_email = '$Email_or_Phone' OR reseller_name = '$Email_or_Phone')");
		//$this->db->where('reseller_name',$Email_or_Phone);
		//$this->db->where('password', md5($password)); 
		$this->db->limit(1);
        $query = $this->db->get(TBL_RESELLERS);
		if (!$query)
		{
		     $this->throwException($query);
		}else{
		
			$user = $query->row();
			$pwd="";
			if($user){
			$pwd=decrypt($user->password); 
			}
			if ($user && $pwd == $password){
				return $user;
			}else{
				return false;
			}
			
	   }
    }
	function reseller_exists($name,$email_address){
   
        $query = $this->db->query("SELECT * FROM oes_resellers WHERE (reseller_name='".$name."' OR reseller_email='".$email_address."') AND isDeleted=0 ");
        //neatPrintAndDie($this->db->last_query());
		if (!$query)
		{
		     $this->throwException($query);
		}else{
			if($row = $query->row()){
             return $query->row();
			}else{
				 return FALSE;
			}
		}
        
	}
	
	
}








