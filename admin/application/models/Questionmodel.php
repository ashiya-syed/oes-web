<?php

Class Questionmodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_TESTS;
    protected $_order_by = 'testId desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
	function getQuestions($offset=0,$page=0,$where,$id=""){
		$this->db->select('*');
		$this->db->where($where);
		 if($id){
			$this->db->order_by($id,'desc');
		} 
		$this->db->limit($offset, $page);
        $query = $this->db->get(TBL_QUETIONS);
		$results = $query->result();
	    return $results;
	}
    
      function getCourseDetails($name)
      {
		$this->db->select('*');
		$this->db->from(TBL_COURSES);
		$this->db->where('isDeleted',0);
		$this->db->where('isActive',1);
		$this->db->like('course_name',$name);
		$query = $this->db->get();
		$result=$query->row();
		if($result){return $result;}else{return 0;}
		
	}  
function getLastQuestionNumbers($table,$where){
	    $this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by('que_id','DESC');
		$query = $this->db->get();
		$result=$query->row();
		if($result){return $result;}else{return 0;}
}	
function deleteTestQuestion($testId)
   {
	    $this->db->where('test_id',$testId);
   		$query=$this->db->delete(TBL_QUETIONS);
		if($query){
   		  return true;
		}
	     return false;
	   
   }function deleteTestOptions($testId)
   {
	    $this->db->where('test_id',$testId);
   		$query=$this->db->delete(TBL_QUE_OPTIONS);
		if($query){
   		  return true;
		}
	     return false;
	   
   }
   
 function saveQue($Quetion,$hint,$subject, $topic,$images,$imagePosition)
   {
	  if($this->session->userdata('resellerId')){
				$resellerId=$this->session->userdata('resellerId');
				}else{
					$resellerId=0;
				}
	   $queSql = "INSERT INTO oes_quetions_total(que,hint,subject_id,topic_id,queType,resellerId) VALUES('".$Quetion."', '".$hint."','".$subject."', '". $topic ."',1,'".$resellerId."') ";
		$result = $this->db->query( $queSql );
	   $insert_id = $this->db->insert_id();
       if( $insert_id)
	   {
		    $images=explode(',',$images);
		    if($images){ 
			   foreach($images as $image){
				   if($image){
				    $imagePosition=($imagePosition)?$imagePosition:0;
				    $imgSql = "INSERT INTO oes_questions_images(q_que_id,q_image,q_position) VALUES('".$insert_id."', '".$image."', '".$imagePosition."') ";
		            $result = $this->db->query( $imgSql ); 
				   }
			   }
		   }
		 return $insert_id;
	   }
	   return false;
   }
	
	
	/* function saveQueOptions($option,$isOption,$queId)
	{
		$optionSql = "INSERT INTO oes_que_options_total(`option`, `is_answer`, `que_id`) VALUES('".$option."', '".$isOption."', '". $queId ."') ";
		$result = $this->db->query($optionSql);
	    if( $result)
	    {
		  return $this->db->insert_id();
	    }
	    return false;
	} */
	function saveQueOptions($option,$isOption,$queId)
	{  
		if($queId){
		$optionSql = "INSERT INTO oes_que_options_total(`option`, `is_answer`, `que_id`) VALUES('".$option."', '".$isOption."', '". $queId ."') ";
		$result = $this->db->query($optionSql);
			if($result)
			{
			  return true;
			  
			}else{
				$error=$this->db->error();
				return $error['message'];
			}
		}else{
			return "empty question id";
         }
	   
    }
	function getRelatedTests($table,$where,$id){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$this->db->where('testId!=',$id);
		$query = $this->db->get();
		$result=$query->result();
		if($result){return $result;}else{return 0;}
	}
	function copyOptions($que){
		$this->db->select('*');
		$this->db->from(TBL_QUE_OPTIONS);
		//$this->db->where('test_id',$testFrom);
		$this->db->where('que_id',$que);
		$sql1="select * from `oes_que_options_total` where  que_id= $que ";
		$query = $this->db->get();
		$result=$query->result();
		if($result){return $result;}else{return 0;}
		}
		
		function getSelectedquestions($id){
	$this->db->select('*');
	$this->db->from(TBL_TEST_QUESTIONS);
	$this->db->where('test_id',$id);
	$this->db->where('isActive',1);
	$this->db->where('isDeleted',0);
	$this->db->where('isApproved',0);
	$query=$this->db->get();
	$result=$query->result(); 
	if($result){
		return count($result);
	}else{
		return 0;
	}
}
	
function searchSlectedQuestions($test,$sub,$topic,$type){
	if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId'); 
		}else{
			$resellerId=0;
			} 
	
	 //$this->db->select(TBL_TEST_QUESTIONS.'.*,'.TBL_QUETIONS.'.*');
	 $this->db->select(TBL_TEST_QUESTIONS.'.id as Sno,' .TBL_QUETIONS.'.*');
	 $this->db->from(TBL_QUETIONS);
	 $this->db->join(TBL_TEST_QUESTIONS, TBL_TEST_QUESTIONS.'.que_id='.TBL_QUETIONS.'.id ','inner');
	 $this->db->where(TBL_TEST_QUESTIONS.'.test_id=',$test);
	 $this->db->where(TBL_TEST_QUESTIONS.'.isActive',1);
	 $this->db->where(TBL_TEST_QUESTIONS.'.isDeleted',0);
	 $this->db->where(TBL_TEST_QUESTIONS.'.isApproved',0);
	 $this->db->where(TBL_QUETIONS.'.subject_id',$sub);
	 $this->db->where(TBL_QUETIONS.'.topic_id',$topic);
	 $this->db->where(TBL_QUETIONS.'.queType',$type);
	 $this->db->where(TBL_QUETIONS.'.resellerId',$resellerId);
	 $query = $this->db->get();
	 $result=$query->result(); //neatPrintAndDie($this->db->last_query());
	 return $result;
}

function checkNameExistsInEdit($table,$where,$id){
	$this->db->select('*');
	$this->db->from($table);
	$this->db->where($where);
	$this->db->where('testId!=',$id);
	$query = $this->db->get();
	$result=$query->result(); 
	 return $result;
}
/* function checkQuestionExistsInanotherTest($id){
	$this->db->select('*');
	$this->db->from(TBL_TEST_QUETIONS);
	$this->db->where('que_id',$id);
	$query = $this->db->get();
	$result=$query->result();
	if($result){
	 return $result;
	}else{
		return false;
	}
	
} */
function checkQuestionExistsInanotherTest($id){
	$this->db->select('*');
	$this->db->from(TBL_MAIN_TEST_QUESTIONS);
	$this->db->where('que_id',$id);
	$this->db->where('isActive',1);
	$this->db->where('isDeleted',0);
	$query = $this->db->get();
	$result=$query->result();
	if($result){
	 return $result;
	}else{
		return false;
	}
	
}

function getExistedquestions($id){
	$this->db->select('que_id');
	$this->db->from(TBL_MAIN_TEST_QUESTIONS);
	$this->db->where('test_id',$id);
	$this->db->where('isActive',1);
	$this->db->where('isDeleted',0);
	$query=$this->db->get();
	$result=$query->result(); 
	if($result){
		 $arry=array();
		if($result){
			foreach($result as $re){
				$arry[]=$re->que_id;
			}
		}
		return $arry;
	}else{
		return array();
	}
}
function getSelectedquestionsExists($id){
	$this->db->select('*');
	$this->db->from(TBL_TEST_QUESTIONS);
	$this->db->where('test_id',$id);
	$this->db->where('isActive',1);
	$this->db->where('isDeleted',0);
	$this->db->where('isApproved',0);
	$query=$this->db->get();
	$result=$query->result();
	if($result){
		 $arry=array();
		if($result){
			foreach($result as $re){
				$arry[]=$re->que_id;
			}
		}
		return $arry;
	}else{
		return array();
	}
}
function getCheckedquestions($table,$where){
	$this->db->select('que_id');
	$this->db->from($table);
	$this->db->where($where);
	$query=$this->db->get();
	$result=$query->result();
	if($result){   
		foreach($result as $res){
			$array[]=$res->que_id;
		}
		$array=array_unique($array);
	 return $array;
	}else{
		return false;
	}
}
function getPromoterTests($id,$tType){
	$sql1="SELECT  GROUP_CONCAT(p_test_id SEPARATOR ', ') AS tests FROM oes_promoter_tests where p_t_promoter_id =".$id."  AND p_t_is_deleted=0";
	$query=$this->db->query($sql1); 
	if($query){
		$result=$query->row();
		$testIds=$result->tests;
		if($testIds){
		
		 $sql="SELECT * FROM oes_tests WHERE `testId` IN (".$testIds.") AND isDeleted = 0  AND isActive = 1 AND testType=".$tType." ";
		 $sql.=" order by testId DESC";
		 $query=$this->db->query($sql);
		 if($query){
			 $tests=$query->result();  
		 }else{
			 $tests=array();
		 }
	    return $tests;
		}else{
			return array();
		}
		
	}else{ 
		return array();
	}
}
function updateCart($userIdsList,$amount,$id,$type){
	$sql="UPDATE `oes_cart` SET amount=".$amount." WHERE itemId=".$id." AND itemType = ".$type." AND userId IN (".$userIdsList.") AND isActive =1 AND isDeleted = 0";
   // print_r($sql);die();
	$query=$this->db->query($sql);
	if($query){
		return true;
	}else{}
	return false;
}

}








?>