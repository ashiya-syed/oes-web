<?php

Class Newsmodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_NEWS;
    protected $_order_by = 'id desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
    
      function getNews()
      {
		 $this->db->select('*'); 
		 $this->db->from(TBL_NEWS);
		 $this->db->where('isDeleted',0);
		 $this->db->order_by('id','desc');
		// $this->db->where('isActive',1);
		 if($this->session->userdata('resellerId')){
			$resellerId=$this->session->userdata('resellerId');
			}else{
				$resellerId=0;
			}
		 $this->db->where('resellerId',$resellerId);
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->result():'';
    }   
	
	
}








