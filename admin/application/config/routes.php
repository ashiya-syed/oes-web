<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/* $route['default_controller'] = 'Admin';
$route['authority/logout'] = 'admin/logout';
$route['authority/d'] = 'admin/d';
$route['authority/profile'] = 'admin/profile';
$route['authority/d/users'] = 'Users/index';
$route['authority/d/users/(:any)'] = 'Users/index';

$route['authority/d/(:any)/quetions'] = 'Quetions/index';
$route['authority/d/(:any)/addquetions'] = 'Quetions/addQuetions';
$route['authority/d/packages'] = 'Packages/index';
$route['authority/d/editquetions/(:any)'] = 'Quetions/editQuetions/$1';
$route['authority/d/addpackage'] = 'Packages/addPackage';
$route['authority/d/editpackage/(:any)'] = 'Packages/editPackage/$1';
$route['authority/d/news'] = 'News/index';
$route['authority/d/addTestQuestions/(:any)'] = 'Quetions/addTestQuestions';
$route['authority/d/uploadQueFromExcel/(:any)'] = 'Quetions/uploadQueFromExcel';
$route['authority/d/coupons'] = 'Coupons/index';
$route['(:any)'] = 'Resellers/login';
$route['authority/d/resellers'] = 'Resellers/index';
$route['authority/d/download'] = 'Quetions/downloadSheet'; 
$route['authority/d/resellers/editReseller/(:any)'] = 'Resellers/editReseller/$1'; 
$route['authority/d/libraries'] = 'Libraries/index'; */

$route['default_controller'] = 'Admin';
$route['authority/logout'] = 'admin/logout';
$route['authority/d'] = 'admin/d';
$route['authority/profile'] = 'admin/profile';
$route['authority/d/users'] = 'Users/index';
$route['authority/d/users/(:any)'] = 'Users/index';

$route['authority/d/(:any)/quetions'] = 'Quetions/index'; 
//$route['authority/d/questionbook'] = 'Questionbook/index'; 
$route['authority/d/addquetions'] = 'Questionbook/addQuestion';
$route['authority/d/packages'] = 'Packages/index';
$route['authority/d/editquen/(:any)'] = 'Questionbook/editQuetions/$1';
$route['authority/d/editquetions/(:any)'] = 'Quetions/editQuetions/$1';
$route['authority/d/addpackage'] = 'Packages/addPackage';
$route['authority/d/editpackage/(:any)'] = 'Packages/editPackage/$1';
$route['authority/d/news'] = 'News/index';
$route['authority/d/addTestQuestion'] = 'Questionbook/add_test_question';
$route['authority/d/selectedQuestions'] = 'Questionbook/selectedQuestions';
$route['authority/d/confirmQuens'] = 'Questionbook/confirm_selected_quens';
$route['authority/d/addTestQuestions/(:any)'] = 'Quetions/addTestQuestions';
//$route['authority/d/uploadQueFromExcel/(:any)'] = 'Quetions/uploadQueFromExcel';
$route['authority/d/uploadQueFromExcel'] = 'Questionbook/uploadQueFromExcel';
$route['authority/d/coupons'] = 'Coupons/index';
$route['(:any)'] = 'Resellers/login';
$route['authority/d/resellers'] = 'Resellers/index';
$route['authority/d/download'] = 'Quetions/downloadSheet'; 
$route['authority/d/resellers/editReseller/(:any)'] = 'Resellers/editReseller/$1'; 
$route['authority/d/libraries'] = 'Libraries/index';
$route['authority/d/subjects'] = 'Subjects/index';
$route['authority/d/topics'] = 'Topics/index';
$route['authority/d/(:any)/addquetions'] = 'Quetions/addQuetions';
//new
$route['authority/d/questionbook'] = 'Questionbook/index'; 
$route['authority/d/questionbook/(:any)'] = 'Questionbook/index'; 
$route['authority/d/questionbook/test/(:any)'] = 'Questionbook/index'; 
$route['authority/d/questionbook/test/(:any)/(:any)'] = 'Questionbook/index';

$route['authority/d/promotions'] = 'Promotions/index';
$route['authority/d/promotions/downloadSheet'] = 'Promotions/downloadSheet';
//new on 15th novemvember
$route['authority/d/exportQuestions/(:any)'] = 'ExportExcel/exportQuestions'; 
$route['authority/d/category'] = 'Category/index';

$route['authority/d/apply_package_code'] = 'Packages/apply_package_code';
$route['authority/d/(:any)/apply_test_code'] = 'Quetions/apply_test_code';
$route['authority/d/editPromoterPackage/(:any)'] = 'Packages/editPromoterPackage/$1';
$route['authority/d/editPromoterTest/(:any)'] = 'Quetions/editPromoterTest/$1';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
