 <div class="page-prefooter">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
                        <h2>About</h2>
                        <p> At www.bookmyparty.com.au, we are committed to protecting your privacy as an online visitor to our website. We use the information we collect about you to maximize the services that we provide to you. <a href="http://www.bookmyparty.com.au/User/about"> Readmore</a></p>
                    </div>
                  
                    <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
                        <h2>Follow Us On</h2>
                        <ul class="social-icons">
                           <!--   <li>
                                <a href="" data-original-title="rss" class="rss" target="_blank"></a>
                            </li> -->
                            <li>
                                <a href="https://www.facebook.com/bookmypartyau" data-original-title="facebook" class="facebook" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/bookmypartyau" data-original-title="twitter" target="_blank" class="twitter"></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/u/0/b/112533558122945868490/112533558122945868490/posts" data-original-title="googleplus" class="googleplus" target="_blank"></a>
                            </li>
                           <!-- <li>
                                <a href="javascript:;" data-original-title="linkedin" class="linkedin" target="_blank"></a>
                            </li>-->
                            <li>
                                <a href="https://www.youtube.com/channel/UCurScMa7m8VFmHqxt3ldicA" data-original-title="youtube" target="_blank" class="youtube"></a>
                            </li>
                            
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
                        <h2>Contacts</h2>
                        <address class="margin-bottom-40">ABN : 32972604910 
                            <br> Email:
                            <a href="mailto:raz@bookmyparty.com.au">raz@bookmyparty.com.au</a>
                        </address>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PRE-FOOTER -->
        <!-- BEGIN INNER FOOTER -->
        <div class="page-footer">
            <div class="container"> 2015 &copy;  BMP ADMIN
                <a href="http://www.kramssgroup.com/"   target="_blank">Kramss Group!</a>
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>