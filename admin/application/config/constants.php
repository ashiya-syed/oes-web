<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define("APP_FOLDER","admin");
define("FRONT_APP_FOLDER","");
define("ROOT",$_SERVER['DOCUMENT_ROOT']."/".APP_FOLDER);
define("SITEURL2","https://".$_SERVER['SERVER_NAME']);
//define("SITEURL2","https://".$_SERVER['SERVER_NAME']."/".APP_FOLDER."/index.php");
define("SITEURL","https://".$_SERVER['SERVER_NAME']."/".APP_FOLDER);
define("FRONTENDURL","http://".$_SERVER['SERVER_NAME']."/".FRONT_APP_FOLDER);


define("LOGOUT_URL",SITEURL.'/authority/logout');
define("RESELLER_LOGOUT_URL",SITEURL.'/Resellers/logout');
define("ADMIN_LOGIN_URL",SITEURL.'/authority');
define('ADMIN_DASHBOARD_URL',SITEURL.'/authority/d');    
define('USER_MANAGEMENT_URL',SITEURL.'/authority/d/users');    
define('QUETION_MANAGEMENT_URL',SITEURL.'/authority/d/maintest/quetions');    
define('PRACTICAL_QUETION_MANAGEMENT_URL',SITEURL.'/authority/d/practicaltest/quetions');    
define('PACKAGES_MANAGEMENT_URL',SITEURL.'/authority/d/packages');    
define('ADD_QUETIONS_URL',SITEURL.'/authority/d/maintest/addquetions');    
define('ADD_PRACTICAL_QUETIONS_URL',SITEURL.'/authority/d/practicaltest/addquetions');    
define('ADD_PACKAGE_URL',SITEURL.'/authority/d/addpackage');
define('EDIT_PACKAGES_URL',SITEURL.'/authority/d/editpackage');//new  
define('IMAGE_COLLECTION_URL',SITEURL.'/quetions/images'); 
  
    
define('NEWS_UPDATES_URL',SITEURL.'/authority/d/news'); 
define('UPDATE_USER_URL',SITEURL.'/users/updateUserPassword'); 
define('PROFILE_URL',SITEURL.'/authority/profile'); 
define('ADD_TESTQUESTIONS_URL',SITEURL.'/authority/d/addTestQuestions'); 
 define('ADD_ECXEL_QUESTIONS_URL',SITEURL.'/authority/d/uploadQueFromExcel'); 
  

define('ASSETS_URL', SITEURL."/assets/");
define('CSS_URL', ASSETS_URL."css/");
define('JS_URL', ASSETS_URL."js/");
define('IMG_URL', ASSETS_URL."img/");
define('FONT_URL', ASSETS_URL."font/");
define('IMAGES_PATH',ASSETS_URL."img/");
define('ICON_IMAGES_PATH',ASSETS_URL."images/");
define('UPLOADS_PATH',SITEURL."/uploads/");
define('USER_SERVICE_IMAGES_PATH',SITEURL."/uploads/servicepics/");
define('ANDROID_BANNER_IMAGES_PATH',SITEURL."/uploads/banners/android/");
define('IOS_BANNER_IMAGES_PATH',SITEURL."/uploads/banners/ios/");
define('COMMON_BANNER_IMAGES_PATH',SITEURL."/uploads/banners/common/");
define('ADMIN_ASSETS_URL',SITEURL."/assets_admin/");
define("UPLOAD",SITEURL.'/uploads/productImages');
define("TIPS_IMAGES_PATH",SITEURL.'/uploads/healthtips/');
define("UPLOADS_OPTIONS_PATH",SITEURL.'/uploads/options/');
define("COURSE_IMAGES_PATH",SITEURL.'/uploads/courses/');

define('COUPON_MANAGEMENT_URL',SITEURL.'/authority/d/coupons'); 
define('TOPIC_MANAGEMENT_URL',SITEURL.'/authority/d/topics'); //QB
define('SUBJECT_MANAGEMENT_URL',SITEURL.'/authority/d/subjects'); //QB
define('RESELLER_MANAGEMENT_URL',SITEURL.'/authority/d/resellers'); 
define('EDIT_RESELLER_URL',SITEURL.'/authority/d/resellers/editReseller'); 
define('RESELLER_DASHBOARD_URL',SITEURL.'/resellers/d'); 
define('EMAIL_VERIFY_URL',FRONTENDURL.'/account/verifyAccount?token='); //need change
define('RESELLER_PROFILE_URL',SITEURL.'/resellers/profile');
define("DOWNLOAD_EXEL",SITEURL.'/authority/d/download');
define('LIBRARY_MANAGEMENT_URL',SITEURL.'/authority/d/libraries');    
define('QUESTION_BOOK_URL',SITEURL.'/authority/d/questionbook'); //neww   
define('ADD_QUESTION',SITEURL.'/authority/d/addquetions');   //qb
define('CONFIRM_TEST_QUESTIONS',SITEURL.'/authority/d/confirmQuens');   //newwww 
define('ADD_TEST_QUESTIONS',SITEURL.'/authority/d/addTestQuestion');   //newwww 

//new for promotions
define("PROMOTIONAL_DOWNLOAD_EXEL",SITEURL.'/authority/d/promotions/downloadSheet');
define("PROMOTIONAL_URL",SITEURL.'/authority/d/promotions');
//new on 15 nov
define("EXPORT_QUESTIONS",SITEURL.'/authority/d/exportQuestions');


//tables
define('TBL_USERS','users');
define('TBL_TESTS','tests');
define('TBL_QUETIONS','oes_quetions_total');
define('TBL_QUE_OPTIONS','oes_que_options_total');
define('TBL_PACKAGES','packages');
define('TBL_NEWS','oes_news');
define('TBL_COURSES','oes_courses');
define('TBL_COUPONS','oes_coupons ');
define('TBL_RESELLERS','oes_resellers ');
define('TBL_LIBRARIES','oes_libraries ');
define('TBL_IMAGES','oes_images ');
define('TBL_USER_PACKAGE','oes_user_packages');
define('TBL_TEST_QUETIONS','oes_test_questions');
define('TBL_USER_PACKAGES','oes_user_packages');


//new
define('TBL_SUBJECTS','oes_subjects');//qb
define('TBL_TOPICS','oes_topics');//qb
define('TBL_TEST_QUESTIONS','oes_test_questions');//qb
define('TBL_MAIN_TEST_QUESTIONS','oes_quetions');//qb
define('TBL_MAIN_TEST_OPTIONS','oes_que_options');//qb
define('TBL_USER_TRACK','oes_usertrack');//19th

define('TBL_PROMOTER_PACKAGES','oes_promoter_packages');
define('TBL_PROMOTER_TESTS','oes_promoter_tests');



 

//user roles
define('USER_ROLE_ID','2');
define('SUPERADMIN_ROLE_ID','1');
//test types
define('MAIN_TEST','1');
define('PRACTICAL_TEST','2');

define("ADDING_RESELLER_SUCCESS",'Reseller added Successfully');
define("ADDING_RESELLER_FAILED",'Adding Reseller Failed');
define("CHECK_COUPON_VALIDITY",'Coupon valid date must be greater than current date');
define("ADDING_SUBJECT_SUCCESS",'Subject added successfully');//qb
define("ADDING_TOPIC_SUCCESS",'Topic added successfully');//qb
define("ADDING_COUPNS_SUCCESS",'Coupon added successfully');
define("ADDING_COUPNS_FAILED",'Adding Coupon failed');
define("ADDING_SUBJECT_FAILED",'Adding Subject failed');//qb
define("ADDING_TOPIC_FAILED",'Adding topic failed');//qb
define("ADDING_NEWS_SUCCESS",'News added Successfully');
define("ADDING_NEWS_FAILED",'Adding news Successfully');
define("USER_REGISTRATION_SUCCESS",'Registration Success');
define("USER_REGISTRATION_FAILED",'Registration failed');
define("PHONE_NUMBER_ALREADY_EXISTS","This phone Number already exists.");
define("PHONE_NUMBER_OR_EMAIL_ALREADY_EXISTS","This phone number or email already exists.");
define("RECORDS_EXISTS","Records Exists");
define("NO_RECORDS_EXISTS","No Records Exists");
define("LOGIN_SUCCESS","loggin success");
define("EMAIL_OR_PHONE_AND_PASSWORD_NOT_MATCH","Failed to authenticate Email Or Phone Number,passwords are invalid.");
define("EMAIL_OR_NAME_AND_PASSWORD_NOT_MATCH","Failed to authenticate Email Or Name,passwords are invalid.");

define("PHONE_NUMBER_OR_EMAIL_OR_NAME_ALREADY_EXISTS","The Name or phone number or email already exists.");
define("SUCCESS","Success.");
define("ERROR","Error Occured.");
define("SUCCESSFULLY_UPDATED","Successfully Updated.");
define("NOT_UPDATED","Not Updated.");
define("SUCCESSFULLY_DELETED","Successfully Deleted.");
define('USER_CREATION_FAILED','Registration failed.');
define('USER_CREATION_SUCCESS','Registration Success.');
define('FILE_NOT_UPLOADED','file not uploaded.');
define('MODULE_FILE_ALLOWED_TYPES','Please dont upload zip ,rar format files.');
define('SUCCESSFULLY_ALLOTED','Successfully Alloted.');
define('PLEASE_CHECK_YOUR_MAIL_FOR_OTP','Please Check Your Mail For OTP.');
define('INVALID_OTP','Invalid OTP.');
define('PASSWORD_SUCCESSFULLY_UPDATED','Password Successfully Updated.');
define('PASSWORD_UPDATE_FAILED','Password updating failed.');
define('PROFILE_SUCCESSFULLY_UPDATED','Profile Successfully Updated.');
define('THANKYOU_FOR_YOUR_REQUEST','Thanyou for your request,Admin will contact you soon.');
define('THANKYOU_FOR_CONTCTUS',"Thank You for contacting the fine'N'fit Team."); 
define('CURRENT_PASSWORD_NOT_MATCH','Please provide valid current password.');
define('PLEASE_TRY_DIFFRRENT_PASSWORD','Please try different password to update.');
define('UPDATE_FAILED','Update Failed.');
define('PASSWORD_UPDATE_SUCCESS','Password Successfully Updated.');
define('PLEASE_PROVIDE_VALID_ADDRESS','Please provide valid address.');

define("PHONE_NUMBER_OR_EMAIL_OR_LOCATION_ALREADY_EXISTS","The Location or phone number or email already exists.");                                                                                              
define("AREA_ALREADY_EXISTS","Area name or Area code already exists.");                
define("CITY_ALREADY_EXISTS","City name or City code already exists.");                
define("STATE_ALREADY_EXISTS","State name or State code already exists.");                
define("COUNTRY_ALREADY_EXISTS","Country name or Country code already exists.");                 
define("TEST_ALREADY_EXISTS","Test name already exists.");                 
define("HOSPITAL_ALREADY_EXISTS","Hospital already exists."); 
define("ADDING_SUCCESS","Successfully added.");
  
define('CATEGORY_MANAGEMENT_URL',SITEURL.'/authority/d/category');  	 
define("ADDING_COURSE_SUCCESS",'Course added successfully');
define("ADDING_COURSE_FAILED",'Adding Course failed');  


define("APLPLY_TEST_CODE",SITEURL.'/authority/d/apply_test_code');  
define("APLPLY_PACKAGE_CODE",SITEURL.'/authority/d/apply_package_code');  
define('EDIT_PROMPTER_PACKAGES_URL',SITEURL.'/authority/d/editPromoterPackage');//new  

define("TBL_USER_COUPON","oes_user_coupons"); 


/* End of file constants.php */
/* Location: ./application/config/constants.php */