<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Healthcontroller extends CI_Controller {

	function __construct()
    {

	
        // Call the Model constructor
        parent::__construct();
	
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('email');
	
        $this->load->library('session');
		$this->load->helper('url');
		//$this->load->helper('captcha');
    }
	
	
	/**
	 * Method to log the exception
	 *
	 */
	protected function logExceptionMessage($exception)
	{
		$errorMessage = 'Exception of type \'' . get_class($exception) . 
					'\' occurred with message: ' . $exception->getMessage() . 
					' in file ' . $exception->getFile() . 
					' at line ' . $exception->getLine();
				 
					// Add backtrace:					
					$errorMessage .= "\r\n Backtrace: \r\n";
					$errorMessage .= $exception->getTraceAsString();
					log_message('error',$errorMessage,TRUE);
	}
		
		
	function makeServiceCall($service_url, $data = array(), $method = 'POST')
	{		
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => $method,
				'content' => http_build_query($data),
			),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($service_url, false, $context);	
		//var_dump($result);die();
		$result = utf8_encode($result);
		
		$result = str_replace('ï»¿','',$result);
		$result = json_decode($result);
		return $result;
	}	
	

   function isValidSession($sessionId,$Method)
   {
	   return true;
	   
   }   
   
   //custom 
	function getAllRecords($table,$where,$select="*")
	{
		$this->db->select($select);
		$this->db->where($where);
        $query = $this->db->get($table);
		$results = $query->result();
	    return $results;
	}
	function getAllRecordsByDesc($table,$where,$id,$select="*")
	{
		$this->db->select($select);
		$this->db->where($where);
		$this->db->order_by($id,'desc');
        $query = $this->db->get($table);
		$results = $query->result();
	    return $results;
	}
	
	function insertOrUpdate($table,$where=array(),$data)
	{
		if($where){
			$this->db->where($where);
			$query=$this->db->update($table,$data);
			if (!$query)
			{    
		         return false;
				
			}else{
				return true;
			}
			
		}else{
		
			if(!empty($data))
			{
				$this->db->insert($table,$data);
				return $this->db->insert_id();
			}
		}	
			
			return false;
	}
	
	function getSingleRecord($table,$where,$select="*")
	{
		$this->db->select($select);
		$this->db->where($where);
        $query = $this->db->get($table); 
		$results = $query->row();
	    return $results;
	}
	

		function generateRandNumber($length=0){

		$characters = 
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJ
KLMNOPQRSTUVWXYZ';

		$randomString = '';

		for ($i = 0; $i < $length; $i++) {

			$randomString .= 
$characters[rand(0, strlen($characters) - 1)];

		}
       $str=preg_replace('/\s+/', '', $randomString);
		return $str;

	}

	
	function encrypt($string){
		//print_r('hi');die();
	 $key = 'password to (en/de)crypt';
    //$string = 'ashiya '; 
    
    
    $iv = mcrypt_create_iv(
        mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
        MCRYPT_DEV_URANDOM
    );
    
    $encrypted = base64_encode(
        $iv .
        mcrypt_encrypt(
            MCRYPT_RIJNDAEL_128,
            hash('sha256', $key, true),
            $string,
            MCRYPT_MODE_CBC,
            $iv
        )
    );
	return $encrypted;
    
}
function decrypt($encrypted){
	
	$key = 'password to (en/de)crypt';
	$data = base64_decode($encrypted);
    $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

    $decrypted = rtrim(
        mcrypt_decrypt(
            MCRYPT_RIJNDAEL_128,
            hash('sha256', $key, true),
            substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
            MCRYPT_MODE_CBC,
            $iv
        ),
        "\0"
    );
	 return $decrypted;
}
}

