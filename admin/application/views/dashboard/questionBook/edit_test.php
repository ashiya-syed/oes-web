    <head>
	<script src="<?php echo SITEURL ;?>/ckeditor/ckeditor.js"></script>
	<script src="<?php echo SITEURL ;?>/ckeditor/samples/js/sample.js"></script>
	<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
	</head>
	  <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   <?php echo sideMenu(); ?>
                   <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                   
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
							 <span>Edit Question</span>
                             <i class="fa fa-angle-right"></i>
                            </li> 
							
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Edit Question</span>
                                    </div>
                                 </div>
								
                                <div class="portlet-body">
                                   <!-- <div class="table-toolbar">
                                        <div class="row">
                                           
                                            
                                        </div>
                                    </div>-->
									 <?php echo $this->session->flashdata('messege'); ?>
                                    <form action="" enctype="multipart/form-data" id="form_sample_2" class="form-horizontal" method="post">
                                       <div id="PROFILE_MESSAGE"></div>
										<div class="form-body" id="replacefunn">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> Please fill all required fields </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
                                            <div class="form-group">
                                               
                                                    
                                                </label>
                                                <div class="col-md-6">
                                                 <!-- <div id="PROFILE_MESSAGE"></div>-->
                                            </div> 
											</div>
										<?php if($Quedata && count($Quedata)>0){
												?>
												<h5><strong>Questions</strong></h5>
												<?php
												   //$lastQueNo = count($Quedata) - 1;
												   //$lastQuestion= $Quedata[$lastQueNo]->que_id;
                                                   $i=1; $a=0;
												   
											       foreach($Quedata as $que){ //neatPrintAndDie($topics);
													    $queType=$que->queType; 
													    $a= $que->id;$m=$que->id;
											?>
											
											<input type="hidden" name="queId[]" value="<?php echo $que->id; ?>">
											
											
									       <div class="form-group">
										    
                                                <label class="control-label col-md-3" >Question 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
														
                                                        <textarea id="ckeditor<?php echo $m; ?>" name="quetion[<?php echo $que->id;?>]" class="form-control" rows="3" width="150" required > <?php echo $que->que; ?></textarea>
													
												 </div></div></div>
												 <div class="form-group">
										    
                                                <label class="control-label col-md-3" >Hint 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
														
                                                        <textarea class="ckeditor" name="hint" class="form-control" rows="3" width="150" required > <?php echo $que->hint; ?></textarea>
													
												 </div></div></div>
												 <!--<label class="control-label col-md-12" style="text-align: left;margin-bottom: 9px;padding-top: 12px;" >Add question to another test: 
                                                    
                                                </label>
												 <div class="dropdown">
		                                                 <select  class="form-control" id="test" name="test" onchange="addQuestionTo(this.value,<?php// echo $que->que_id; ?>,<?php //echo $TestData->testId; ?>)" >
			                                              <option value="">Select Test</option>
														  <?php if($tests){ 
															  foreach($tests as $test){ ?>
																<option value= "<?php echo $test->testId; ?>"> <?php echo $test->testName; ?> </option>
		                                                   
															 <?php }
														  } ?>
														   
			                                              </select>
		                                                  </div>-->
                                                        <div class="form-group" >
                                                <label class="control-label col-md-3" >Subject 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
		                                                 <select  class="form-control" id="Filter" disabled  onchange="addQuestionTo(this.value,<?php// echo $que->que_id; ?>,<?php //echo $TestData->testId; ?>)" >
			                                              <option value="">Select Subject</option>
														  <?php if($subjects){ 
															  foreach($subjects as $subject){ 
															  if($que->subject_id == $subject->id){
																  $class="selected";
															  }else{
																   $class="";
															  }
															  
															  ?>
																<option value= "<?php echo $subject->id; ?>" <?php echo $class;?>> <?php echo $subject->sub_name; ?> </option>
		                                                   
															 <?php }
														  } ?>
														   
			                                              </select>
		                                                  </div> </div> </div>
														   <div class="form-group" >
                                                <label class="control-label col-md-3" >Topic 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                 
		                                                 <select  class="form-control" id="Filter" disabled  onchange="addQuestionTo(this.value,<?php// echo $que->que_id; ?>,<?php //echo $TestData->testId; ?>)" >
			                                              <option value="">Select Topic</option>
														  <?php if($topics){ 
															  foreach($topics as $topic){ 
															  if($topic->topic_id == $que->topic_id){
																  $class="selected";
															  }else{
																   $class="";
															  }
															  
															  ?>
																<option value= "<?php echo $topic->topic_id; ?>" <?php echo $class;?>> <?php echo $topic->topic_name; ?> </option>
		                                                   
															 <?php }
														  } ?>
														   
			                                              </select>
		                                                  </div></div></div>
                                                 <!-- <label class="control-label col-md-12" style="text-align: left;margin-bottom: 9px;padding-top: 12px;" ><b>Question Type:</b> 
                                                  </label>
                                                    
														  </br>-->
													  <div class="form-group" class="quetionsDiv">
													   <label class="control-label col-md-3" > 
                                                    <span class="required">  </span>
                                                </label>
													   <div class="col-md-6">
													<?php $option = $que->options;  
													if($option)
													{   
												        $k=0; $j=1; $x=0;
														foreach($option as $option)
														{ 
														 $textType=$option->textType;
														 $option_id=$option->option_id;?> 
														
														 
													
													<!-- on 20 th april-->
													<?php 
														  $checked= "";
														  if($option->is_answer == 1)
														  {
															  $checked= "checked";
														  }
														?>
														<div class="que<?php echo $que->id; ?>" style="margin-bottom: 15px;">
														<div class="col-sm-5"><?php echo $j; ?>.
														
														<?php
                                                      
                                                         if($queType == 1 || $queType == 0){
														if($textType == 0 || $textType == 1 ){ ?>
														<textarea class="opt" name="option<?php echo $a; ?>[]"  id="opt<?php echo $option_id;?>"  ><?php echo $option->option; ?></textarea> </div>
														<div class="col-sm-1">or</div><input type="file" class="col-sm-5"  name="option<?php echo $a; ?>[]" >
														<input type="checkbox" name="que<?php echo $a.$j; ?>ans" <?php echo $checked; ?>>
													    <a href="javascript:void(0);" onclick="deleteOpt(<?php echo $que->id;?>,<?php echo $option_id; ?>)">
												        <img  src=<?php echo SITEURL; ?>/assets/close.jpg width="20" height="20" style="margin-top: -64px;margin-left: 618px;">
												       </a>
													  <script>
														
														var c=<?php echo  $option->option_id; ?>;
															CKEDITOR.replace( 'opt'+c,
															{   toolbar:'MA'    }
														 );
														</script>
														<?php }else if($textType == 2){?>
														
															<input type="file" name="option<?php echo $a; ?>[]" id="option<?php echo $option_id;?>" value="<?php echo $option->option; ?>" ><img src="<?php echo UPLOADS_OPTIONS_PATH.$option->option?>" width="90" height="45"></div>
														<div class="col-sm-1">or</div><input type="text" name="option<?php echo $a; ?>[]" >
														<input type="checkbox" name="que<?php echo $a.$j; ?>ans" <?php echo $checked; ?>>
														<a href="javascript:void(0);" onclick="deleteOpt(<?php echo $que->id;?>,<?php echo $option_id; ?>)">
												        <img  src=<?php echo SITEURL; ?>/assets/close.jpg width="20" height="20" style="margin-top: -64px;margin-left: 618px;">
												       </a>
														<div class="clearfix"></div>
														<?php }else if($textType == 3){?>
															<input type="file" name="option<?php echo $a; ?>[]" id="option<?php echo $option_id;?>" value="<?php echo $option->option; ?>" ><video width="120" height="90" controls> <source src="<?php echo UPLOADS_OPTIONS_PATH.$option->option;?>" ></video></div></div>
														<div class="col-sm-1">or</div><input type="text" name="option<?php echo $a; ?>[]" >
														<input type="checkbox" name="que<?php echo $a.$j; ?>ans" <?php echo $checked; ?>>
                                                         <a href="javascript:void(0);" onclick="deleteOpt(<?php echo $que->id;?>,<?php echo $option_id; ?>)">
												        <img  src=<?php echo SITEURL; ?>/assets/close.jpg width="20" height="20" style="margin-top: -64px;margin-left: 618px;">
												       </a>
														<div class="clearfix"></div>
														<?php }else{ ?>
															<input type="file" name="option<?php echo $a; ?>[]" id="option<?php echo $option_id;?>" value="<?php echo $option->option; ?>" ><audio width="120" height="90" controls> <source src="<?php echo UPLOADS_OPTIONS_PATH.$option->option;?>" ></audio></div>
														<div class="col-sm-1">or</div><input type="text" name="option<?php echo $a; ?>[]" >
														<input type="checkbox" name="que<?php echo $a.$j; ?>ans"  <?php echo $checked; ?>>	
														<a href="javascript:void(0);" onclick="deleteOpt(<?php echo $que->id;?>,<?php echo $option_id; ?>)">
												        <img  src=<?php echo SITEURL; ?>/assets/close.jpg width="20" height="20" style="margin-top: -64px;margin-left: 618px;">
												       </a>
														<div class="clearfix"></div>
														<?php }
														
														}elseif($queType == 3){ ?>
														<label>
														<input type="radio" class="form-control" name="option<?php echo $a; ?>[]" <?php if($option->option == "True") echo $checked; ?> value="True">True</div>
														</label>
														<label>
														<input type="radio" class="form-control" name="option<?php echo $a; ?>[]" <?php if($option->option == "False") echo $checked; ?>  value="False">False
												        </label>
														<input type="hidden" name="queType<?php echo $a; ?>[]" value="3">	<div class="clearfix"></div>
														
														<?php }else{ ?>
														
															<input type="text" class="form-control" style="border: 0;outline: 0;background: transparent;border-bottom: 1px solid black;width:240% "  name="option<?php echo $a; ?>[]" value="<?php echo $option->option; ?>" > </div>
														     
														<input type="hidden" name="que<?php echo $a.$j; ?>ans" <?php echo $checked; ?>>
														
														<?php } ?>
														
														
														</div>
															<div class="clearfix"></div>
														
														<input type="hidden" name="optionsId<?php echo $a; ?>[]" value="<?php echo $option->option_id ?>">
												  <?php $j++; ?>
												  
												  <?php }
													  }
													?>
											
													<div>
													  </div>
													
													<!---Add More Options --->
													
													<div id="optionsque<?php echo $que->id; ?>"></div>
													<div>
													 <?php if($que->queType == 1 || $que->queType == 0 ){?>
													   <button type="button" onclick="addNewOptionInEdit('<?php echo $que->id; ?>','<?php echo count($que->options); ?>')" class="text-center">Add Options</button>
													<?php } ?>
													
													</div>
													
													<!--end --->
													
													
												</div>
											</div>
											
										<script>
										var m=<?php echo $m ?>;
										CKEDITOR.replace('ckeditor'+m,
										{  
										filebrowserBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html',
										filebrowserImageBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html?type=Images',
										filebrowserFlashBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html?type=Flash',
										filebrowserUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
										filebrowserImageUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
										filebrowserFlashUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
										}
										);
										CKEDITOR.replace('hint'+m,
										{  
										filebrowserBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html',
										filebrowserImageBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html?type=Images',
										filebrowserFlashBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html?type=Flash',
										filebrowserUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
										filebrowserImageUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
										filebrowserFlashUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
										}
										);
																							
									   </script>
											<?php $i++; } ?>
											<div id="que">
                                            </div>
											</br>
											<div id="que"></div>
					                       <!-- <div class="text-center"><button type="button" onclick="AddChlidNewRow()">Add More Questions</button></div>
											--><input type="hidden" id="yx" value="">
											</br>
											
											<?php } ?>
											<div class="modal-footer btn-group center-block "  id="button">
											    <div class="text-center">
											      <button type="submit" class="btn green" name="EditQuetions">Update</button>
                                                </div>
										     </div>
                                      	</div>	
                                    </form>
								
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		<div class="modal fade" id="Update_Password" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_update_password_div">
			</div>
		</div>
	<script>
	initSample();
	    jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });
		jQuery(document).bind("keyup keydown", function(e){ if( e.keyCode == 44){  e.preventDefault(); } });

</script>							
											
									
										
       