<script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<script>MathJax.Hub.Queue(['Typeset',MathJax.Hub,'body']);</script>
<?php if(@$id){ $testId = $id;} else{ $testId = 0; } 
 $checkALresyInTest=$this->Questionmodel->getExistedquestions($testId);
 $checkALresyInTestBeforeApproving=$this->Questionmodel->getSelectedquestionsExists($testId);

?>
									 
                   <div class="portlet-body ">
									<table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
											 <?php if($testId){ ?>
                                               <th><input type="checkbox" ></th>
											 <?php } ?>
                                                <th>Q.No</th>
												<th>Delete </th>
                                                <th>Question</th>
                                                <th>Type</th>
												<th>Subject </th>
												<th>Topic </th>
												<!--<th> Status </th>-->
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($questions && count($questions)>0){
													$i=1;$sub='-';$type='-';$topic='-';
													 foreach($questions as $question){
														 
														 if($subjects && count($subjects) > 0){ 
														 
														  foreach( $subjects as $sxcxcub){
															  
																if($sxcxcub->id == $question->subject_id){
																 	$sub=$sxcxcub->sub_name;
																}
														  }
														 }
                                                   if($topics && count($topic) > 0){ 
														 
														  foreach( $topics as $top){
															  
																if($top->topic_id == $question->topic_id){
																 	$topic=$top->topic_name;
																}
														  }
														 }
														 
														 if($question->queType == 1)
														 {$type="Multiple Choice Questions";}
													     else if($question->queType == 2)
														 { $type="Fill in the Blanks";}
													     else if($question->queType == 3){$type="True or False";}
														 else{$type="-";}
												            $selectedQuestion=$this->Questionmodel->checkQuestionExistsInanotherTest($question->id);
															if($selectedQuestion){
															$id="exists";
														     }else{$id ="";}
																		?>
														<tr id="<?php echo $id; ?>">
														<?php if($testId){
                                                           $test=$testId;
								$where=array('test_id'=>$test,'isActive'=>1,'isDeleted'=>0,'isApproved'=>0);
								$selectedQuestions=$this->Questionmodel->getCheckedquestions(TBL_TEST_QUESTIONS,$where);

														?>
															<td><input type="checkbox" <?php if(in_array($question->id,$checkALresyInTest) || in_array($question->id,$checkALresyInTestBeforeApproving)){echo "disabled";} ?> id="selectQue[]" name="selectQue"  value="<?php echo $question->id;  ?>" >	
																	</td>
														 <?php } ?>
                                                         <td><?php echo $i;  ?></td>
														  <td><input type="checkbox"  id="deleteQue[]" name="deleteQue"  value="<?php echo $question->id;  ?>" >	</td>
														<!--<td><?php echo $question->id;  ?></td>-->
														<td><?php echo $question->que;  ?></td>
														<td><?php  echo $type;  ?></td>
														<td><?php echo $sub;  ?></td>
														<td><?php echo $topic;  ?></td>
														
																							
                                               <!-- <td>
                                                    <span id="activeUser_<?php echo $question->id; ?>">
													<?php $status=$question->isActive;
														if($status=='1'){ ?>
													   <span class="btn btn-success" style="padding:4px" onclick="activateQuestion('<?php echo $question->id; ?>','0')"> Active </span>
													 <?php   } else { ?>
													   <span class="btn btn-danger" style="padding:4px" onclick="activateQuestion('<?php echo $question->id; ?>','1')"> InActive </span>
													 <?php } ?>
													 </span>
                                                </td>-->
												<td class="text-center"> 
												<a class="btn btn-icon-only default" href="<?php  echo SITEURL.'/authority/d/editquen/'.$question->id ; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
												<button class="btn btn-icon-only red" onclick="deleteQuestion('<?php echo $question->id ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                                 </tr>
											<?php $i++; } }else{ ?>
												
												<!--<div class="alert alert-danger">No results found with your search.</div>-->
											<?php } ?>  
                                        </tbody>
                                    </table>
                                    </div>
									<?php $extraFooter = $this->load->view('dashboard/users_script');
		                                  $this->load->view('dashboard/includes/search_footer',$extraFooter); ?>
