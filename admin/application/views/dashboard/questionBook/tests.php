   <style>
#search_div{
    margin-top:-4%; /* Don't copy this */
	float:left;
}
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
</style>
<body onbeforeunload='reset_options()'>
<script>
	/*function reset_options() {
    document.getElementById('MySelect').options.length = 0;
    return true;

}*/
if (!!window.performance && window.performance.navigation.type === 2) {
            window.location.reload(); 

}


</script>
  <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   
                        <?php echo sideMenu(); ?>
                        

                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <span>Tests</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Tests</span>
                                    </div>
						
                                     <div class="col-md-6 pull-right text-right" id="new">
									
									  <!--<form class="navbar-form  pull-left" role="search" method="get" action="<?php //echo USERS_URL;?>">
										  <div class="input-group stylish-input-group" style="border: 1px solid #26a1ab;" id="search_div">
											<input type="text" class="form-control"  placeholder="Search With Health Issues" name="search" id="search" >
												<span class="input-group-addon">
												<button type="submit">
												<span class="glyphicon glyphicon-search"></span>
												</button>  
												</span>
											</div>
											
									     </form> -->  
                                        
										  <div class="btn-group">
											 <?php 
											   if($tType == 1)
											   {
												   $url = ADD_QUETIONS_URL;
											   }else if($tType == 2)
											   {
												   $url = ADD_PRACTICAL_QUETIONS_URL;
											   }
											     
											 ?>
											<a href="<?php echo $url; ?>"> <button class="btn sbold green"> Add Tests<i class="fa fa-plus"></i>
											 </button></a>
											 <form style="margin:0px;" class="navbar-form  pull-left" role="search" method="post"  action="<?php echo DOWNLOAD_EXEL?>">
											<button  class="btn sbold green" onclick="form.submit();" id="download" > Download Sample Excel<i  class="fa fa-download"></i>
											 </button>
											 </form>
										  </div> 
								   </div>
                                </div>
                                            
                                <div class="portlet-body">
                                  <?php echo $this->session->flashdata('Error'); ?>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th class="text-center">S.No</th>
                                                <th class="text-center">Test Id</th>
                                                <th class="text-center">Test Name</th>
												<th class="text-center">Test Amount</th>
												<th class="text-center">Test Duration</th>
												<th class="text-center">Total Questions</th>
												<th class="text-center">Total Attempts</th>
												<th class="text-center">Shuffling Que is On</th>
												<th class="text-center">Add questions</th>
                                                <th class="text-center"> Status </th>
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php 
												if($tests && count($tests)>0){
													 $i=1;
													 foreach($tests as $test){
														
														 if($test->isShuffleQueOrder){
															 $isShuffle="Yes";
														 }else{ $isShuffle="No";}
												?>
							                
                                            <tr class="odd gradeX" id="refresh">
                                                        <td class="text-center"><?php echo $i;  ?></td>
														<td class="text-center"><?php echo $test->testId;  ?></td>
														<td class="text-center"><?php echo $test->testName;  ?></td>
														<td class="text-center"><?php if($test->testAmount) echo $test->testAmount; else echo 0;  ?></td>
														<td class="text-center"><?php echo $test->testTime.' min';  ?></td>
														<td class="text-center"><?php echo $test->TotalQue; ?></td>
														<td class="text-center"><?php echo $test->attempts; ?></td>
														<td class="text-center"><?php echo $isShuffle; ?></td>
														 <td class="text-center"> <div class="dropdown">
		                                                 <select onchange="redirect(this.value)" class="form-control" id="MySelect" >
			                                              <option value="<?php echo current_url(); ?>">Select Type</option>
			                                              <option value= "<?php echo ADD_TESTQUESTIONS_URL.'/'.$test->testId; ?>"> Add questions </option>
			                                              <option value= "<?php echo ADD_ECXEL_QUESTIONS_URL.'/'.$test->testId;?>"> Import Questions </option>
		                                                  </select>
		                                                  </div></td>
														   <script>
														   function redirect(value){
															   alert(value);
															   window.location = value;
														   }
														   </script>
                                                          <!--<td class="text-center"> <a href="<?php echo ADD_TESTQUESTIONS_URL.'/'.$test->testId; ?>"> <button class="btn sbold green"> Add Questions<i class="fa fa-plus"></i>
											              </button></a></td>-->																					
                                                     <td class="text-center">
                                                    <span id="activetest_<?php echo $test->testId; ?>">
													<?php $status=$test->isActive;
														if($status=='1'){ ?>
													   <span class="btn btn-success" style="padding:4px" onclick="activateTest('<?php echo $test->testId?>','0')"> Active </span>
													 <?php   } else { ?>
													   <span class="btn btn-danger" style="padding:4px" onclick="activateTest('<?php echo $test->testId?>','1')"> InActive </span>
													 <?php } ?>
													 </span>
                                                </td>
												
                                                <td class="text-center"> 
												<a class="btn btn-icon-only default" href="<?php  echo SITEURL.'/authority/d/editquetions/'.$test->testId ; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
												<button class="btn btn-icon-only red" onclick="deleteTest('<?php echo $test->testId ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                                </tr>
											<?php $i++; } } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		
		<div class="modal fade" id="addusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add User</h4>
                                                </div>
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal">
                                        <div id="USERS_MESSAGE"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

											 <div class="form-group">
                                                <label class="control-label col-md-3">Service
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <select class="form-control select2me" id="serviceId" name="serviceId" onchange="getVerticals(this.value);">
                                                        <option value="">Select an option</option>
														   
                                                      
                                                    </select>
                                                </div>
                                            </div>
											 <div class="form-group">
                                                            <label class="control-label col-md-3">Verticals 
															<span class="required"> * </span></label>
                                                            <div class="col-md-6">
                                                                <select class="form-control " id="verticals">
                                                                    <option value="">Select an option</option>
														   
																</select>
                                                            </div>
                                            </div>
													
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Name" id="user_name" name="name" /> </div>
                                                </div>
                                            </div> 

											<div class="form-group">
                                                <label class="control-label col-md-3">Email Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Email" id="email_adress" name="email"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Phone Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Phone" id="phone_number" name="number"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Paswword
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Password" id="password" name="password"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Confirm Paswword
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Confirm Password" id="confirm_password" name="cpassword"/> </div>
                                                </div>
                                            </div>
											
											
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_users()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                      
		
		<div class="modal fade" id="editusers" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_edit_user_div">
			
			</div>
		</div>
			
			<script>		
	document.getElementById('dropdown').onchange = function(e) {
  if (e.target.value == 'All') {
    location.reload();
  }
};
$(document).ready(function () {
    $("select").each(function () {
        $(this).val($(this).find('option[selected]').val());
    });
})

</script>							
			

					
       