     
	 <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   <?php echo sideMenu(); ?>
                   <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                   
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
							 <span>Add Tests</span>
                             <i class="fa fa-angle-right"></i>
                            </li> 
							
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Add Tests</span>
                                    </div>
                                 </div>
								
                                <div class="portlet-body">
                                   <!-- <div class="table-toolbar">
                                        <div class="row">
                                           
                                            
                                        </div>
                                    </div>-->
                                    <form action="" id="form_sample_2" class="form-horizontal" method="post">
                                       <div id="PROFILE_MESSAGE"></div>
										<div class="form-body" id="replacefunn">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> Please fill all required fields </div>
                                           <!-- <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>-->
                                            
                                            <div class="form-group">
                                               
                                                    
                                                </label>
                                                <div class="col-md-6">
                                                 <!-- <div id="PROFILE_MESSAGE"></div>-->
                                            </div> 
											</div>
										  
											<div class="form-group">
                                                <label class="control-label col-md-3">Test Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" maxlength="30"  class="form-control" value="" placeholder="Enter Name" id="name" name="testName" required /> </div>
                                                </div>
                                            </div>
											
										
                                             <div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Test Amount
                                                    <?php if($this->uri->segment(3)== "maintest"){ ?>
														<span class="required"> * </span>
														 <?php } ?>
														</label>
														<div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');this.value = minmax(this.value, 1, 5)"  placeholder="Enter Amount"  id="testAmount" name="testAmount" <?php if($this->uri->segment(3)== "maintest") echo "required"; ?> /> </div>
                                                </div>
												</div>
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Test Duration
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');this.value = minmax(this.value, 1, 4)" placeholder="Enter Duration in minutes"  id="testDuration" name="testDuration"  required/> </div>
                                                </div>
                                            </div>
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Positive marks
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeypress="return isNumberKey(event);"  placeholder="Enter Positive mark per question"  id="positiveMarks" name="positiveMarks"  required/> </div>
                                                </div>
                                            </div>
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Negative marks
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeypress="return isNumberKey(event);"   placeholder="Enter Negative mark per question"  id="nagativeMarks" name="nagativeMarks"  required/> </div>
                                                </div>
                                            </div>
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Shuffle questions order
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                       <input type="checkbox" name="isShuffle" id="isShuffle" onclick="$(this).attr('value', this.checked ? 1 : 0)">
                                              </div>
                                            </div>
											</div>
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Attempts
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');this.value = minmax(this.value, 1, 2)" placeholder="Enter Attempts per Test"  id="attempts" name="attempts"  required/> </div>
                                                </div>
                                            </div>
											
											<!--<h5><strong>Questions</strong></h5>
									       <div class="form-group" class="quetionsDiv">
										     
                                                <label class="control-label col-md-3" >Question 1
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <textarea name="quetion[]" placeholder="Enter question." class="form-control" rows="3" width="150" required> </textarea>
													</div> 
													</br>
													
														<div class="que1">
														1. <input type="text" name="option0[]" required><input type="checkbox" name="que01ans" > &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
														2. <input type="text" name="option0[]" required><input type="checkbox" name="que02ans" >
														</div>
														</br>
														<div class="que1">
														3. <input type="text" name="option0[]" required><input type="checkbox" name="que03ans" > &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
														4. <input type="text" name="option0[]" required><input type="checkbox" name="que04ans" >
														</div>
														</br>
													
													<div id="optionsque1"></div>
													<div>
													   <!--Answer from above options: <input type="text" name="optionAns[]">--> <!--<button type="button" onclick="addNewOption(1)" class="text-center">Add Options</button>
													<!--</div>
												</div>
												<!--<div class="col-sm-3"> <a href="javascript:void(0);" onclick="deleteRow(this)"><img  src="<?php //echo SITEURL ?>/assets/close.jpg" width="20" height="20"></a></div>-->
											<!--</div>
											
											<div id="que">
                                            </div>
					                        <div class="text-center"><button type="button" onclick="AddChlidNewRow()">Add More Questions</button></div>
											</br>-->
											<div class="modal-footer btn-group center-block "  id="button">
											    <div class="text-center">
											      <button type="submit" class="btn green" name="AddQuetions">Save</button>
                                                </div>
										     </div>
                                      	</div>	
                                    </form>
								
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		
	
			<div class="modal fade" id="Update_Password" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog" style="margin-top:80px;" id="_update_password_div">
			
			  </div>
		</div>
		
										
										
       