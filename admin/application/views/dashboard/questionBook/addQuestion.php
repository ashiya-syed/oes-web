
<script src="<?php echo base_url();?>ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url();?>ckeditor/samples/js/sample.js"></script>
	 <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   <?php echo sideMenu(); ?>
                   <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                   
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
							 <span>Add Questions</span>
                             <i class="fa fa-angle-right"></i>
                            </li> 
							
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Add Questions</span>
                                    </div>
                                 </div>
								
                                <div class="portlet-body">
                                   <!-- <div class="table-toolbar">
                                        <div class="row">
                                           
                                            
                                        </div>
                                    </div>-->
                                    <form action="" enctype="multipart/form-data" id="form_sample_2" class="form-horizontal" method="post">
                                       <div id="PROFILE_MESSAGE"></div>
										<div class="form-body" id="replacefunn">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> Please fill all required fields </div>
                                            <!--<div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>-->
                                            
                                            <div class="form-group">
                                               
                                                    
                                                </label>
                                                <div class="col-md-6">
                                                 <!-- <div id="PROFILE_MESSAGE"></div>-->
                                            </div> 
											</div>
                                           <?php 
										   // print_r($serialNumber);die();
										   if($serialNumber){ 
											    $serialNumber =$serialNumber+1;
										   }else{ $serialNumber =1;}
										   
										   $QuestionNumber =$lastQuestion+1;  ?>
                                             <div class="form-group">
                                                <label class="control-label col-md-3">Select Subjects
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6 ">
                                                    <div class="input-icon right" >
                                                        <i class="fa"></i>
                                                        <select class="form-control" name="subject" id="subject" onchange="getselectedTopics(this.value);" required>
                                                         <option value="">Select Subject</option>
                                                         <?php if($subjects){
                                                               foreach($subjects as $sub){ ?>
                                                          <option value="<?php echo $sub->id;?>"><?php echo $sub->sub_name; ?></option>
                                                          <?php } } ?>
			                                             </select>
                                                     </div>
                                                 </div>
											</div> 
                                          <div class="form-group">
                                                <label class="control-label col-md-3">Select Topic
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6 ">
                                                    <div class="input-icon right" >
                                                        <i class="fa"></i>
                                                        <select class="form-control" name="topic"  id="cityID" required>
                                                         <option value="">Select Topic</option>
                                                         <?php if($topics){
                                                               foreach($topics as $topic){ ?>
                                                          <option value="<?php echo $topic->topic_id;?>"><?php echo $topic->topic_name; ?></option>
                                                          <?php } } ?>
			                                             </select>
                                                     </div>
                                                 </div>
											</div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Select Type of question
                                                    <span class="required"> * </span>
                                                </label>
											<div  class="col-sm-6">
											<div class="input-icon right" >
                                                        <i class="fa"></i>
													    <select class="form-control" onchange="selectQuestionType(<?php echo $lastQuestion+1;?>,this.value,<?php echo $QuestionNumber;?>)"  id="queType" >
			                                              <option value="">Select Type</option>
			                                              <option value="1">Multiple Choice</option>
			                                              <option value= "2">Fill in the Blanks</option>
			                                              <option value= "3">True or False </option> 
		                                                  </select>
														  </div>
														  </div>
														  </div>
				                          <h5><strong>Questions</strong></h5>
									       <div class="form-group" class="quetionsDiv" id="quetionsDiv<?php echo $lastQuestion+1;?>">
										       
                                                <label class="control-label col-md-3" >Question <?php echo $serialNumber;?>
                                                    <span class="required"> * </span>
                                                </label>
												<div class="col-md-6" id="questionType">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
														
                                                        <textarea id="ckeditor<?php echo $QuestionNumber ?>" name="quetion[<?php echo $lastQuestion+1;?>]" placeholder="Enter question." class="form-control" rows="3" width="150" required> </textarea>
													</div> 
													</br>
													 <!--new-->
													<div class="input-icon right">
													
                                                        <i class="fa"></i>
														
                                                         <?php echo 'Hint:'; ?><textarea id="hint<?php echo $QuestionNumber ?>" name="hint[<?php echo $lastQuestion+1;?>]" placeholder="Enter hint." class="form-control" rows="3" width="150" required> </textarea>
													</div> <br>
													<!--end-->
													
													<?php if($lastQuestion == 0){$ltstq=1;}else{$ltstq=$lastQuestion;}?>
														<div class="que<?php echo $lastQuestion;?> row" style="margin-bottom: 15px;">
														<div class="col-sm-5">1. <textarea name="option<?php echo $lastQuestion+1;?>[]"  id="opt1<?php echo $lastQuestion+1;?>"></textarea> </div>
														<div class="col-sm-1">or</div>
														<input type="file" class="col-sm-5" name="option<?php echo $lastQuestion+1;?>[]" >
														<input type="checkbox" class="col-sm-1" name="que<?php echo $lastQuestion+1;?>1ans" >
														<div class="clearfix"></div>
														</div>
														
														<div class="que<?php echo $lastQuestion;?> row"  style="margin-bottom: 15px;">
														<div class="col-sm-5">2. <textarea name="option<?php echo $lastQuestion+1;?>[]" id="opt2<?php echo $lastQuestion+1;?>" ></textarea></div>
														<div class="col-sm-1">or</div>
														<input type="file" class="col-sm-5" name="option<?php echo $lastQuestion+1;?>[]" >
														<input type="checkbox" class="col-sm-1" name="que<?php echo $lastQuestion+1;?>2ans" >
														<div class="clearfix"></div>
														</div>
														
														<div class="que<?php echo $lastQuestion;?> row"  style="margin-bottom: 15px;">
														<div class="col-sm-5">3. <textarea name="option<?php echo $lastQuestion+1;?>[]" id="opt3<?php echo $lastQuestion+1;?>" ></textarea></div>
														<div class="col-sm-1">or</div>
														<input type="file" class="col-sm-5" name="option<?php echo $lastQuestion+1;?>[]" >
														<input type="checkbox"  class="col-sm-1" name="que<?php echo $lastQuestion+1;?>3ans" >
														<div class="clearfix"></div>
														</div>
														<div class="que<?php echo $lastQuestion;?> row"  style="margin-bottom: 15px;">
														<div class="col-sm-5">4. <textarea name="option<?php echo $lastQuestion+1;?>[]" id="opt4<?php echo $lastQuestion+1;?>" ></textarea></div>
														<div class="col-sm-1">or</div>
														<input type="file" class="col-sm-5" name="option<?php echo $lastQuestion+1;?>[]" >
														<input type="checkbox" class="col-sm-1" name="que<?php echo $lastQuestion+1;?>4ans" >
														<div class="clearfix"></div>
														</div>
														<input type="hidden" name="queType[<?php echo $lastQuestion+1;?>]" value="1">
													<div id="optionsque<?php echo $lastQuestion+1;?>"></div>
													<!--<div  class="col-sm-6">
													    <select class="form-control" style="width: 82%; height: 34px;margin-left: 32px;" onchange="selectQuestionType(<?php echo $lastQuestion+1;?>,this.value,<?php echo $QuestionNumber;?>)"  id="queType" >
			                                              <option value="">Select Type</option>
			                                              <option value="1">Multiple Choice</option>
			                                              <option value= "2">Fill in the Blanks</option>
			                                              <option value= "3">True or False </option> 
		                                                  </select>
														  </div> -->
                                                     <span>
													  <button type="button" onclick="addNewOption(<?php echo $lastQuestion+1;?> )" class="text-center">Add Options</button>
													
													</span>
												</div>
											<div class="clearfix"></div>
											<div id="que">
                                            </div>
											 <div class="text-center">
											</div>
											</br>
										</div>	
										<div class="modal-footer btn-group center-block "  id="button">
											    <div class="text-center">
											      <button type="submit" class="btn green" name="AddQuetions">Save</button>
                                                </div>
										     </div>
                                    </form>
									<body id="main">

<script>
function getselectedTopics(id)
{ 
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Ajaxhelper/topicsBySubject',  
				    data: {id:id},
                            success: function(response){
							   $('#cityID').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
	var i=<?php echo $QuestionNumber ?>;
	CKEDITOR.replace('ckeditor'+i,
	{  
	filebrowserBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	}
	);
	CKEDITOR.replace('hint'+i,
	{  
	filebrowserBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl: '<?php echo SITEURL; ?>/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl: '<?php echo SITEURL; ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	}
	);
														
	              
</script>

</body>
									<script type="text/javascript">
									$('input').attr('autocomplete', 'off');
									</script>
									<script type="text/javascript">
									CKEDITOR.replace( 'opt1'+<?php echo $lastQuestion+1;?> ,
										{   toolbar:'MA'    }
									   );
									   CKEDITOR.replace( 'opt2'+<?php echo $lastQuestion+1;?> ,
										{   toolbar:'MA'    }
									   );
									   CKEDITOR.replace( 'opt3'+<?php echo $lastQuestion+1;?> ,
										{   toolbar:'MA'    }
									   );
									   CKEDITOR.replace( 'opt4'+<?php echo $lastQuestion+1;?> ,
										{   toolbar:'MA'    }
									   );
									
									</script>
									<head>
<script type = "text/javascript" >
 //history.pushState({ page: 1 }, "Title 1", "");
window.onhashchange = function (console.log('here');) {
//window.location = "http://gmail.com";
};
</script>
</head>