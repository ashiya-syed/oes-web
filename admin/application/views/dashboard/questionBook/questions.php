<script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
   <style>
#search_div{
    margin-top:-4%; /* Don't copy this */
	float:left;
}
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
#exists{
   background-color: #FFFF99;
}
.pac-container {
			background-color: #FFF;
			z-index: 20;
			position: fixed;
			display: inline-block;
			float: left;
		}
		.modal{
			z-index: 20;   
		}
		.modal-backdrop{
			z-index: 10;        
		}​
		ul.pagination li a
			{
			border:solid 1px;
			border-radius:3px;
			-moz-border-radius:3px;
			-webkit-border-radius:3px;
			padding:6px 9px 6px 9px;
			}
			ul.pagination li
			{
			padding-bottom:1px;
			}
			ul.pagination li a:hover,
			ul.pagination li a.current
			{
			color:#FFFFFF;
			box-shadow:0px 1px #EDEDED;
			-moz-box-shadow:0px 1px #EDEDED;
			-webkit-box-shadow:0px 1px #EDEDED;
			}
			ul.pagination
			{
			margin:4px 0;
			padding:0px;
			height:100%;
			overflow:hidden;
			font:12px 'Tahoma';
			list-style-type:none;
			}
			ul.pagination li
			{
			float:left;
			margin:0px;
			padding:0px;
			margin-left:5px;
			}
			ul.pagination li a
			{
			color:black;
			display:block;
			text-decoration:none;
			padding:7px 10px 7px 10px;
			}
			ul.pagination li a img
			{
			border:none;
			}
			ul.pagination li a
			{
			color:#0A7EC5;
			border-color:#8DC5E6;
			background:#F8FCFF;
			}
			ul.pagination li a:hover,
			ul.pagination li a.current
			{
			text-shadow:0px 1px #388DBE;
			border-color:#3390CA;
			background:#58B0E7;
			background:-moz-linear-gradient(top, #B4F6FF 1px, #63D0FE 1px, #58B0E7);
			background:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0.02, #B4F6FF), color-stop(0.02, #63D0FE), color-stop(1, #58B0E7));
			}
</style>

  <!-- BEGIN CONTAINER -->
        <div class="page-container" >
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   
                        <?php echo sideMenu(); ?>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                  <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <span>Questions</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
					<?php echo $this->session->flashdata('Error');?>
					 <div id="question_error"></div>
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Questions</span>
                                    </div>
									<?php /* if(@$_GET['test']){ 
									  $testId = $_GET['test'];
										
									 } else{
										 $testId = 0;
									 } */
									 if(@$this->uri->segment(4)=="test"){ 
									  $testId = $this->uri->segment(5);
									  } else{
										 $testId = 0;
									 }
									 $checkALresyInTest=$this->Questionmodel->getExistedquestions($testId);
									 $checkALresyInTestBeforeApproving=$this->Questionmodel->getSelectedquestionsExists($testId);
									 ?>
                               <div class="col-md-6 pull-right text-right" id="new">
								 <div class="btn-group" style="display:inline">
											 <?php if($testId){ ?>
											  <a  style="margin:5px;margin-left:-10px;margin-right: 11px;"><button onclick="selectedQuestions(<?php echo $testId ?>)" class="btn sbold green"> Assign to test<i class="fa fa-plus"></i>
											 </button></a>
											 <button  style="margin-left: 132px; " class="btn sbold green" onclick="DeleteSelectedQuestions()" id="download" > Delete Selected <i class="fa fa-trash" aria-hidden="true"></i>
											 </button>
											 <?php if($testId){
												 $this->load->model('Questionmodel');
												 $selectedQuestion=$this->Questionmodel->getSelectedquestions($testId);
												if($selectedQuestion > 0){
												?>
											    <a href="<?php echo CONFIRM_TEST_QUESTIONS ?>?test=<?php echo $testId ?>"><button style="margin:4px" class="btn sbold green"><?php echo $selectedQuestion ;?> selected Questions</button></a>
											 <?php } }?>
												 
											 <?php } else{ ?>
											 <a style="margin:5px;margin-left: -10px;margin-right: 11px;" href="<?php echo ADD_QUESTION; ?>"> <button class="btn sbold green"> New Question<i class="fa fa-plus"></i>
											 </button></a>
												 <a  href="<?php echo ADD_ECXEL_QUESTIONS_URL;?>"> <button  class="btn sbold green"> Import Questions<i class="fa fa-plus"></i>
											 </button></a> 
											<form  style="margin-left: 290px;" id="role" class="navbar-form  pull-left" role="search" method="post"  action="<?php echo DOWNLOAD_EXEL?>">
											<button  class="btn sbold green" onclick="form.submit();" id="download" > Download Sample Excel<i  class="fa fa-download"></i>
											 </button>
											 </form>
											  <button  style="margin-left: 132px; margin-top: -42px;" class="btn sbold green" onclick="DeleteSelectedQuestions()" id="download" > Delete Selected <i class="fa fa-trash" aria-hidden="true"></i>
											 </button>
											<?php  }?>
											 
										</div>
				                 </div>
			               </div>
		                </div>
                       </div>
					   <form id="search" method="post" name="search" action="<?php //echo QUESTION_BOOK_URL ?>">
						<select class="form-control input-sm input-small input-inline" placeholder="Select Subject" id="subject" name="subject" onchange="getTopics(this.value);"  class="date-control"  >
						<?php if($subjects && count($subjects) > 0){  ?>
						 <option value="">Select subject</option>
								<?php foreach( $subjects as $s){ ?>
									<option value="<?php echo $s->id ?>"> <?php echo $s->sub_name ?></option>
								<?php  }} ?>
														 
						
						</select>
						<select class="form-control input-sm input-small input-inline" placeholder="Select Topic" name="topic" id="cityID"   class="date-control"  >
						<?php if($topics && count($topics) > 0){  ?>
						 <option value="">Select Topic</option>
								<?php foreach( $topics as $t){ ?>
									<option value="<?php echo $t->topic_id ?>"> <?php echo $t->topic_name ?></option>
								<?php  }} ?>
														 
						
						</select>
						<select class="form-control input-sm input-small input-inline" placeholder="Select Subject" name="type"  id="type" class="date-control"  >
						<option value="">Select Question type</option>
						<option value="1"> Multiple choice</option>
						<option value="2"> Fill in the Blanks</option>
						<option value="3"> True or False</option>
						</select>
						<input type="button" name="search" onclick="searchResults(<?php echo $testId;?>)" value="Search">
						</form><br>
						<div class="portlet-body " id="questionss">
									<!-- <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_11">-->
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="">
                                        <thead>
                                            <tr>
											 <?php if($testId){ ?>
                                               <th><input type="checkbox" ></th>
											 <?php } ?>
                                                <th>Q.No</th>
												<th>Delete </th>
                                                <th>Question</th>
                                                <th>Type</th>
												<th>Subject </th>
												<th>Topic </th>
												
												<!--<th> Status </th>-->
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($questions && count($questions)>0){
													$i=$FromSno;$sub='-';$type='-';$topic='-';
													 foreach($questions as $question){
														 
														 if($subjects && count($subjects) > 0){ 
														 
														  foreach( $subjects as $sxcxcub){
															  
																if($sxcxcub->id == $question->subject_id){
																 	$sub=$sxcxcub->sub_name;
																}
														  }
														 }
                                                   if($topics && count($topic) > 0){ 
														 
														  foreach( $topics as $top){
															  
																if($top->topic_id == $question->topic_id){
																 	$topic=$top->topic_name;
																}
														  }
														 }
														 
														 if($question->queType == 1)
														 {$type="Multiple Choice Questions";}
													     else if($question->queType == 2)
														 { $type="Fill in the Blanks";}
													     else if($question->queType == 3){$type="True or False";}
														 else{$type="-";}
														 
								                              $selectedQuestion=$this->Questionmodel->checkQuestionExistsInanotherTest($question->id);
															if($selectedQuestion){
																$id="exists";
															}else{$id ="";}
												?>
														<tr  id="<?php echo $id; ?>" class="odd gradeX" >
														<?php if($testId){ ?>
															<td><input type="checkbox" <?php if(in_array($question->id,$checkALresyInTest) || in_array($question->id,$checkALresyInTestBeforeApproving)){echo "disabled";} ?> id="selectQue[]" name="selectQue" value="<?php echo $question->id;  ?>" >	
																	</td>
														 <?php } ?>
                                                         <td><?php echo $i;?></td>
														 <td><input type="checkbox"  id="deleteQue[]" name="deleteQue"  value="<?php echo $question->id;  ?>" >	</td>
														<!--<td><?php echo $question->id;  ?></td>-->
														<td><?php echo $question->que;  ?></td>
														<td><?php  echo $type;  ?></td>
														<td><?php echo $sub;  ?></td>
														<td><?php echo $topic;  ?></td>
														
													<td class="text-center"> 
												<a class="btn btn-icon-only default" href="<?php  echo SITEURL.'/authority/d/editquen/'.$question->id ; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
												<button class="btn btn-icon-only red" onclick="deleteQuestion('<?php echo $question->id ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                                 </tr>
											<?php $i++; } } ?> 
                                        </tbody>
                                    </table>
									<div id="pagination" style="text-align:center">
									<ul class="pagination">
									<!-- Show pagination links -->
									<?php foreach (@$links as $link) {
									echo "<li>". $link."</li>";
									 } ?>
									</div>
                                    </div>
                                </div>
                            </div>
                        </div>
  <script>
function getTopics(id)
{ 
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Ajaxhelper/topicsBySubject',  
				    data: {id:id},
                            success: function(response){
							   $('#cityID').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
  return false;
}
</script>
		
										
			

					
       