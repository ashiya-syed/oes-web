<script>
function deleteUser(userId)
{
	   var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	          $.ajax({
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/deleteUser', 
				    data: {'userId':userId},
					dataType:'json',
                    success: function(response){
						 window.location.href="";   
                    },
				    error: function(xhr, statusText, err){
                              console.log("Error:" + xhr.status);  
				    }
						
                });
	   }
 				
  return false;
}

function activateUser(userId,id)
{
	   
	          $.ajax({
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/activateUser', 
				    data: {'userId':userId,'id':id},
					success: function(response){
						 $('#activeUser_'+userId).html(response);
                    },
				    error: function(xhr, statusText, err){
                              console.log("Error:" + xhr.status);  
				    }
						
                });
	 
}

var rowCount = 1;

function AddChlidNewRow(){
	rowCount ++; 
    cnt = rowCount-1;
   var text = '<div class="form-group" class="quetionsDiv">'+
										     
                                                '<label class="control-label col-md-3" >Quetion '+rowCount+' '+
                                                    '<span class="required"> * </span>'+
                                                '</label>'+
                                                '<div class="col-md-6">'+
                                                    '<div class="input-icon right">'+
                                                        '<i class="fa"></i>'+
                                                        '<textarea name="quetion[]" class="form-control" rows="3" width="150">'+'</textarea>'+
													'</div>'+
													'</br>'+
													'<div class="que'+rowCount+'">'+
													'1. <input type="text" name="option'+cnt+'[]"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+
													'2. <input type="text" name="option'+cnt+'[]">'+
													'</div>'+
													'</br>'+
													'<div class="que'+rowCount+'">'+
													'3. <input type="text" name="option'+cnt+'[]"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+
													'4. <input type="text" name="option'+cnt+'[]">'+
													'</div>'+
													'</br>'+
													
													'<div id="optionsque'+rowCount+'"></div>'+
													
													'<div>'+
													  ' Answer from above options: <input type="text"  name="optionAns[]">'+
													  '  <button type="button" onclick="addNewOption('+rowCount+')">Add Options</button>'+
													'</div>'+
												'</div>'+
												
												'<div class="col-sm-3">'+
												'<a href="javascript:void(0);" onclick="deleteRow(this)">'+
												'<img  src='+ baseUrl+'/assets/close.jpg width="20" height="20">'+
												'</a></div>'+
												
											'</div>';
								
			$('#que').append(text);
}

function deleteRow(node)
    {    
    r=node.parentNode.parentNode;
    r.parentNode.removeChild(r);
    rowCount -=1;
   }

   // add the new options for each que.
   function addNewOption(queNumber)
   {   
	      var len = $('.que'+queNumber).length * 2; 
		  var first = len+1;
		  var second = first+1;
	       var txt = '<div class="que'+queNumber+'">'+ first +': <input type="text" name="option[]">'+                                  
		             '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+
			         second +': <input type="text" name="option[]">'+
					 '</div>'+
					 
					 '</br>';
					 $('#optionsque'+queNumber).append(txt);
   }   
   
   
   
   
function deleteQue(testId)
{ 
	  var ok = confirm("Are you sure to Delete?"); 
       if (ok) {
	        $.ajax({
                    type: "post",
                    url:baseUrl+'/Ajaxhelper/deleteQue',  
				    data: {testId:testId},
                            success: function(response){
								 window.location.href="";
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	   }
         return false;

}
function activateTest(testId,id){
	
	         $.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/activateTest',  
				    data: {testId:testId,id:id},
                            success: function(response){
						    $('#activetest_'+testId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}						

</script>