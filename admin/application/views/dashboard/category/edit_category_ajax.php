 
<div class="modal-content">
                                  
								  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Edit Course</h4>
                                                </div>
                                                <div class="modal-body"> 
										
                                    <!-- BEGIN FORM-->
                                     <form action="#" enctype="multipart/form-data" class="form-horizontal"  name="courseForm" id="EDITcourseForm" autocomplete="off">
                                       <div id="Edit_Coupon_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
											<div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">Course
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="update_category"  id="update_category"  value="<?php echo $details->courseName; ?>"  required/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Course Description
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <textarea type="text" required  value="<?php echo $details->courseDescription; ?>" class="form-control" placeholder="Enter Description" id="description" name="description"><?php echo $details->courseDescription; ?></textarea> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Image
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="file" class="form-control" <?php if(empty($details->courseImage)){ echo "required";} ?> placeholder="Enter Amount" id="library_file" name="library_file"/> </div>
                                                </div>
                                            </div>
											<input type="hidden" name="courseId" id="courseId" value="<?php echo $details->courseId; ?>">
                                           
											
										</div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" onclick="update_category('<?php echo $details->courseId; ?>')">Update</button>
                                                </div>
                                            </div>									
						
<script>
 /*    function update_category()
{ 
	var update_category   = $('#update_category').val();
	var courseId          = $('#courseId').val(); 
	
	if(update_category == ""  )
	{
		$('#Edit_Coupon_Message').html('<div class="alert alert-danger" style="text-align:center">Please fill all fields</div>');
		return false;
	}else{
		
	        $.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/updateCategory',  
				    data: {update_category:update_category,id:courseId},
					dataType: "JSON",
                    success: function(response){  
						if(response.status ==1)
						{
							$('#Edit_Coupon_Message').html('<div class="alert alert-success" style="text-align:center">'+response.message+'</div>');
							location.reload();
						}else if(response.status ==2){
							$('#Edit_Coupon_Message').html('<div class="alert alert-danger" style="text-align:center">'+response.message+ '</div>');
						
						}else{
							$('#Edit_Coupon_Message').html('<div class="alert alert-danger" style="text-align:center">'+response.message+ '</div>');
						}
						   
                    },
					error: function(xhr, statusText, err){
                         console.log("Error:" + xhr.status);  
					}
						
                });

         return false;
   }
}  
*/
 function update_category()
{ 
	 var formData = new FormData($('#EDITcourseForm')[0]);
		
	        $.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/updateCategory',  
				    data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "JSON",
                    success: function(response){  
						if(response.status ==1)
						{
							$('#Edit_Coupon_Message').html('<div class="alert alert-success" style="text-align:center">'+response.message+'</div>');
							location.reload();
						}else if(response.status ==2){
							$('#Edit_Coupon_Message').html('<div class="alert alert-danger" style="text-align:center">'+response.message+ '</div>');
						
						}else{
							$('#Edit_Coupon_Message').html('<div class="alert alert-danger" style="text-align:center">'+response.message+ '</div>');
						}
						   
                    },
					error: function(xhr, statusText, err){
                         console.log("Error:" + xhr.status);  
					}
						
                });

         return false;
   
} 
    </script>