   <style>
#search_div{
    margin-top:-4%; /* Don't copy this */
	float:left;
}
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
.pac-container {
			background-color: #FFF;
			z-index: 20;
			position: fixed;
			display: inline-block;
			float: left;
		}
		.modal{
			z-index: 20;   
		}
		.modal-backdrop{
			z-index: 10;        
		}​
</style>

  <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   
                        <?php echo sideMenu();  ?>
                      <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <span>Course</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
					 <?php echo $this->session->flashdata('Error'); ?>
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Course</span>
                                    </div>
                              
                              <div class="col-md-6 pull-right text-right" id="new">
								 <div class="btn-group">
											<button data-toggle="modal" href="#addusers" class="btn sbold green"> Add New Course<i class="fa fa-plus"></i>
											 </button>   </div> 
									 </div>
								</div>
                                <div class="portlet-body">
								 <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Course Id</th>
                                                <th>Course Name</th>
                                                <th>Course Image</th>
                                                 <th> Status </th>
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($courses&& count($courses)>0){
													$i=1;
													 foreach($courses as $course){
														 
												?>
                                            <tr class="odd gradeX">
                                                        <td><?php echo $i;  ?></td>
														<td><?php echo $course->courseId;  ?></td>
														<td><?php echo $course->courseName;  ?></td>
														<td><?php if($course->courseImage){ ?><img src="<?php echo COURSE_IMAGES_PATH.'/'.$course->courseImage;?>" height="100px" weight="100px" alt="course image"> <?php }else{ echo "-";} ?></td>
														 <td>
                                                    <span id="activeUser_<?php echo $course->courseId; ?>">
													<?php $status=$course->isActive;
														if($status=='1'){ ?>
													   <span class="btn btn-success" style="padding:4px" onclick="activateCategory('<?php echo $course->courseId?>','0')"> Active </span>
													 <?php   } else { ?>
													   <span class="btn btn-danger" style="padding:4px" onclick="activateCategory('<?php echo $course->courseId?>','1')"> InActive </span>
													 <?php } ?>
													 </span>
                                                </td>
												<td class="text-center"> 
												<button class="btn btn-icon-only default" onclick="edit_category('<?php echo $course->courseId  ?>');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
												<button class="btn btn-icon-only red" onclick="deleteCategory('<?php echo $course->courseId ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                            </tr>
											<?php $i++; } } ?> 
                                        </body>
                                    </table>
									
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		
		<div class="modal fade" id="addusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add Course</h4>
                                                </div>
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" enctype="multipart/form-data" class="form-horizontal"  name="courseForm" id="courseForm" autocomplete="off">
                                        <div id="Add_Coupons_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

											<div class="form-group">
                                                <label class="control-label col-md-3">Course
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text"  class="form-control" placeholder="Enter Course" id="category" name="category"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Course Description
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text"  class="form-control" placeholder="Enter Description" id="description" name="description"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Image
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="file" class="form-control"  placeholder="Enter Amount" id="library_file" name="library_file"/> </div>
                                                </div>
                                            </div>
											</div>
											
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_category()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
									<div class="modal fade" id="addDummyusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                              
                                                <div class="modal-body"> 
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_dummy_users()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                      
		
		<div class="modal fade" id="edituser" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_edit_user_div">
			
			</div>
		</div>
	
		<script>
                    //subjects
/* function add_category()
{ 
    var formData = new FormData($('#courseForm')[0]);	
	var category     = $('#category').val();
	if(category == "")
	{
		$('#Add_Coupons_Message').html('<div class="alert alert-danger" style="text-align:center">Please fill all fields</div>');
		return false;
	}else{
	        $.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/addCategory',  
				    data: {category:category},
					dataType: "JSON",
                    success: function(response){
						if(response.status ==1)
						{
							$('#Add_Coupons_Message').html('<div class="alert alert-success" style="text-align:center">'+response.message+'</div>');
							refresh_form_fields();
							location.reload();
						}
						else if(response.status ==2){
							$('#Add_Coupons_Message').html('<div class="alert alert-danger" style="text-align:center">'+response.message+ '</div>');
						
							}
						else{
							$('#Add_Coupons_Message').html('<div class="alert alert-danger" style="text-align:center">'+response.message+ '</div>');
						}
						   
                    },
					error: function(xhr, statusText, err){
                         console.log("Error:" + xhr.status);  
					}
						
                });
	}	
         return false;
} */
 function add_category()
{ 
    var formData = new FormData($('#courseForm')[0]);

	        $.ajax({
				  
                    type: "POST",
					url:baseUrl+'/Ajaxhelper/addCategory',
					data:formData,
					async: false,
					cache: false,
                    contentType: false,
                    processData: false,
					dataType: "JSON",
                    success: function(response){ 
						if(response.status ==1)
						{
							$('#Add_Coupons_Message').html('<div class="alert alert-success" style="text-align:center">'+response.message+'</div>');
							refresh_form_fields();
							location.reload();
						}
						else if(response.status ==2){
							$('#Add_Coupons_Message').html('<div class="alert alert-danger" style="text-align:center">'+response.message+ '</div>');
						
							}
						else{
							$('#Add_Coupons_Message').html('<div class="alert alert-danger" style="text-align:center">'+response.message+ '</div>');
						}
						   
                    },
					error: function(xhr, statusText, err){
                         console.log("Error:" + xhr.status);  
					}
						
                });
		
         return false;
} 
function edit_category(courseId)
{
	$.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/editCategory',  
				    data: {courseId:courseId},
                            success: function(response){
						    $("#edituser").modal('show');
						    $('#_edit_user_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}



function activateCategory(courseId,id)
{
	 $.ajax({
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/activateCategory', 
				    data: {'courseId':courseId,'id':id},
					success: function(response){
						
						 $('#activeUser_'+courseId).html(response);
                    },
				    error: function(xhr, statusText, err){
                              console.log("Error:" + xhr.status);  
				    }
						
                });
	 
}

function deleteCategory(courseId)
{
	   var ok = confirm("Are you sure to delete?"); 
       if (ok) {
	          $.ajax({
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/deleteCategory', 
				    data: {'courseId':courseId},
					dataType:'json',
                    success: function(response){
						location.reload();
                    },
				    error: function(xhr, statusText, err){
                              console.log("Error:" + xhr.status);  
				    }
						
                });
	   }
 				
  return false;
}
                    </script>
			

					
       