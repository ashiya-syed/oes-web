 <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
  
   <style>
#search_div{
    margin-top:-4%; /* Don't copy this */
	float:left;
}
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
.pac-container {
			background-color: #FFF;
			z-index: 20;
			position: fixed;
			display: inline-block;
			float: left;
		}
		.modal{
			z-index: 20;   
		}
		.modal-backdrop{
			z-index: 10;        
		}​
</style>

  <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   
                        <?php echo sideMenu(); $this->load->model('Usermodel'); ?>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                  <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <span>Selected Questions</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
					<?php echo $this->session->flashdata('Error');?>
					<div id="question_error"></div>
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase">Selected Questions</span>
                                    </div>
									<?php if(@$_GET['test']){ 
									  $testId = $_GET['test'];
										
									 } else{
										 $testId = 0;
									 }?>
                               <div class="col-md-6 pull-right text-right" id="new">
								<div class="btn-group" style="display:inline">
											<a style="margin:5px;margin-left: -10px;margin-right: 11px;" href="<?php echo QUESTION_BOOK_URL.'/test/'.$testId; ?>"> <button class="btn sbold green"> Back to Question Bank
											 </button></a>
											 <a style="margin-right: 187px;" href="<?php echo ADD_TEST_QUESTIONS.'?test='.$testId; ?>"> <button class="btn sbold green"> Save Question
											 </button></a>
									</div>
									<br>
									<button  class="btn sbold green" style="display:inline; margin-right: 30px; margin-top: -58px;" onclick="multieRefQuestion()" id="download" > Delete Selected <i class="fa fa-trash" aria-hidden="true"></i>
								 </button>
				                 </div>
			               </div>
		                </div>
                       </div>
                    <form id="search" method="post" name="search" action="<?php //echo QUESTION_BOOK_URL ?>">
						<select class="form-control input-sm input-small input-inline" placeholder="Select Subject" id="subject" name="subject" onchange="getTopics(this.value);"  class="date-control"  >
						<?php if($subjects && count($subjects) > 0){  ?>
						 <option value="">Select subject</option>
								<?php foreach( $subjects as $s){ ?>
									<option value="<?php echo $s->id ?>"> <?php echo $s->sub_name ?></option>
								<?php  }} ?>
														 
						
						</select>
						<select class="form-control input-sm input-small input-inline" placeholder="Select Topic" name="topic" id="cityID"   class="date-control"  >
						<?php if($topics && count($topics) > 0){  ?>
						 <option value="">Select Topic</option>
								<?php foreach( $topics as $t){ ?>
									<option value="<?php echo $t->topic_id ?>"> <?php echo $t->topic_name ?></option>
								<?php  }} ?>
														 
						
						</select>
						<select class="form-control input-sm input-small input-inline" placeholder="Select Subject" name="type"  id="type" class="date-control"  >
						<option value="">Select Question type</option>
						<option value="1"> Multiple choice</option>
						<option value="2"> Fill in the Blanks</option>
						<option value="3"> True or False</option>
						</select>
						<input type="button" name="search" onclick="searchSelectedQuestions(<?php echo $testId;?>)" value="search">
						</form><br>
						
                        <div class="portlet-body " id="searchQuestions">
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_11">
                                        <thead>
                                            <tr>
                                                <th>Question No</th>
												<th>Delete</th>
                                                <th>Question</th>
                                                <th>Subject</th>
                                                <th>Topic</th>
                                                <th>Type</th>
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($questions && count($questions)>0){
													$i=1;$sub='-';$topic='-';
													 foreach($questions as $question){
                                                        if($subjects && count($subjects) > 0){ 
														 
														  foreach( $subjects as $sxcxcub){
															  
																if($sxcxcub->id == $question->subject){
																 	$sub=$sxcxcub->sub_name;
																}
														  }
														 }
                                                   if($topics && count($topics) > 0){ 
														 
														  foreach( $topics as $top){
															  
																if($top->topic_id == $question->topic){
																 	$topic=$top->topic_name;
																}
														  }
														 }
														  if($question->queType == 1)
														 {$type="Multiple Choice Questions";}
													     else if($question->queType == 2)
														 { $type="Fill in the Blanks";}
													     else if($question->queType == 3){$type="True or False";}
														 else{$type="-";}
														
												?>
                                            <tr class="odd gradeX">
                                                        
                                                        <td><?php echo $i;  ?></td>
														<td><input type="checkbox"  id="deleteAQue[]" name="deleteAQue"  value="<?php echo $question->id;  ?>" >	</td>
														<!--<td><?php echo $question->que_id;  ?></td>-->
														<td><?php echo $question->question;  ?></td>
														<td><?php echo $sub;  ?></td>

														<td><?php echo $topic;  ?></td>
														<td><?php echo $type;  ?></td>
												<td class="text-center"> 
												<button class="btn btn-icon-only red" onclick="deleteRefQuestion('<?php echo $question->id ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                                 </tr>
											<?php $i++; } } ?> 
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
            
		
	
										
			

					
       