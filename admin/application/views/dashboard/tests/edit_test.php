    <head>
	<script src="<?php echo SITEURL ;?>/ckeditor/ckeditor.js"></script>
	<script src="<?php echo SITEURL ;?>/ckeditor/samples/js/sample.js"></script>
	<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
	</head>
	  <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   <?php echo sideMenu(); ?>
                   <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                   
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
							 <span>Edit Test</span>
                             <i class="fa fa-angle-right"></i>
                            </li> 
							
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
					 <?php echo $this->session->flashdata('Error');?>
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Edit Test</span>
                                    </div>
                                 </div>
								
                                <div class="portlet-body">
                                   <!-- <div class="table-toolbar">
                                        <div class="row">
                                           
                                            
                                        </div>
                                    </div>-->
									 <?php echo $this->session->flashdata('messege'); ?>
                                    <form action="" enctype="multipart/form-data" id="form_sample_2" class="form-horizontal" method="post">
                                       <div id="PROFILE_MESSAGE"></div>
										<div class="form-body" id="replacefunn">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> Please fill all required fields </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
                                            <div class="form-group">
                                               
                                                    
                                                </label>
                                                <div class="col-md-6">
                                                 <!-- <div id="PROFILE_MESSAGE"></div>-->
                                            </div> 
											</div>
											<input type="hidden" name="testType" value="<?php echo $TestData->testType; ?>">
											<?php if($TestData->testType == 2)  {?>
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Is Free Test
                                                   
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                       <input type="checkbox" name="isFreeT" id="isFreeT" <?php if($TestData->isFree == 1)echo 'checked';?> onclick="$(this).attr('value', this.checked ? 1 : 0);hideAmount();">
                                              </div>
                                            </div>
											</div>
											<?php } ?>
											
											 <?php if($TestData->isFree == 0) {  ?>
                                             <div class="form-group" id="amount1">
                                                <label class="control-label col-md-3" >Test Amount
												</label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="this.value = minmax(this.value, 1, 5)" placeholder="Enter Amount" name="testAmount" value="<?php echo $TestData->testAmount;?>"  /> </div>
                                                </div>
                                            </div>
											<?php } else{ ?>
											<div class="form-group" id="amount2">
                                                <label class="control-label col-md-3" >Test Amount
												<?php if($TestData->testType == 1) { ?>
													
												<?php } ?>
                                                  
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="this.value = minmax(this.value, 1, 5)" placeholder="Enter Amount" name="testAmount"  /> </div>
                                                </div>
                                            </div> 
											<?php } ?>
										<div class="form-group">
                                                <label class="control-label col-md-3">Test Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control " placeholder="Enter Name" id="name1" name="testName"  value="<?php echo $TestData->testName; ?>" required/> </div>
                                                </div>
                                            </div>
                                          <div class="form-group">
                                                <label class="control-label col-md-3">Select Course
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6 ">
                                                    <div class="input-icon right" >
                                                        <i class="fa"></i>
                                                        <select class="form-control" name="course" id="course" required>
                                                         <option value="">Select course</option>
                                                         <?php if($courses){
                                                               foreach($courses as $course){ ?>
                                                          <option value="<?php echo $course->courseId;?>" <?php if($TestData->course_id == $course->courseId){ echo "selected";}?>><?php echo $course->courseName; ?></option>
                                                          <?php } } ?>
			                                             </select>
                                                     </div>
                                                 </div>
											</div> 
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Test Duration
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="this.value = minmax(this.value, 0, 4)" placeholder="Enter Duration in minutes" name="testDuration" value="<?php echo $TestData->testTime;?>" required/> </div>
                                                </div>
                                            </div>
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Test Questions
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');this.value = minmax(this.value, 1, 4)" placeholder="Enter total questions"  id="testQuestions" name="testQuestions" value="<?php echo $TestData->totalQuestions;?>"  required/>  </div>
                                                </div>
                                            </div>
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Test Marks
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');this.value = minmax(this.value, 1, 4)" placeholder="Enter total marks"  id="testMarks" name="testMarks"    value="<?php echo $TestData->totalMarks;?>" required/> </div>
                                                </div>
                                            </div>
											
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Positive marks
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="this.value = minmax(this.value, 0, 4)" placeholder="Enter Amount" name="testPositive" value="<?php echo $TestData->rightMarks;?>" required/> </div>
                                                </div>
                                            </div>
											
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Negative marks
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="this.value = minmax(this.value, 0, 4)" placeholder="Enter Amount" name="testNegative" value="<?php echo $TestData->negativeMarks;?>" required/> </div>
                                                </div>
                                            </div>
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Shuffle questions order
                                                   
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                       <input type="checkbox" name="isShuffle" id="isShuffle" <?php if($TestData->isShuffleQueOrder == 1)echo 'checked';?> onclick="$(this).attr('value', this.checked ? 1 : 0)">
                                              </div>
                                            </div>
											</div>
											
											<div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Attempts
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="this.value = minmax(this.value, 1, 2)" placeholder="Enter Attempts per test" name="testAttempts" value="<?php echo $TestData->attempts;?>" required/> </div>
                                                </div>
                                            </div>
											<h5><strong>Test Questions:</strong></h5>
												<div class="portlet-body ">
									
                                     <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>Question No</th>
                                                <th>Question</th>
                                               <!-- <th>Subjest</th>
                                                <th>Topic</th>-->
                                                <th>Action</th>
												
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($Quedata  && count($Quedata )>0){
													$k=1;//*$sub="-";$topic="-";
													 foreach($Quedata  as $Que){
														// neatPrintAndDie($subjects);
														 /*if($subjects && count($subjects) > 0){ 
														 
														  foreach( $subjects as $sxcxcub){
															  
																if($sxcxcub->id == $Que->subject){
																 	$sub=$sxcxcub->sub_name;
																}
														  }
														 }
                                                   if($topics && count($topic) > 0){ 
														 
														  foreach( $topics as $top){
															  
																if($top->topic_id == $Que->topic){
																 	$topic=$top->topic_name;
																}
														  }
														 }*/
														
												?>
                                            <tr class="odd gradeX">
                                                        
                                                        <td><?php echo $k;  ?></td>
														<td><?php echo $Que->que;  ?></td>
														<!--<td><?php //echo $sub;  ?></td>
														<td><?php// echo $topic;  ?></td>-->
														<td><a href="javascript:void(0);" class="btn btn-icon-only red" onclick="deleteQue('<?php echo  $Que->test_id ?>','<?php echo $Que->que_id ?>')"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                                 </td>
                                                 </tr>
											<?php $k++; } } ?> 
                                        </tbody>
                                    </table>
                                    </div>
											<div class="modal-footer btn-group center-block "  id="button">
											    <div class="text-center">
											      <button type="submit" class="btn green" name="EditQuetions">Update</button>
                                                </div>
										     </div>
                                      	</div>	
                                    </form>
								
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		<div class="modal fade" id="Update_Password" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_update_password_div">
			</div>
		</div>
	<script>
	initSample();
	    jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });
		jQuery(document).bind("keyup keydown", function(e){ if( e.keyCode == 44){  e.preventDefault(); } });

</script>	
<script>
	$("#amount2").hide();
	initSample();
	    jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });
		jQuery(document).bind("keyup keydown", function(e){ if( e.keyCode == 44){  e.preventDefault(); } });

		   function hideAmount(){ 
			var isFree =$("#isFreeT").val();
			if(isFree == 1){
				$("#amount1").hide();
				$("#amount2").hide();
			}else if(isFree == 0){
				$("#amount2").show();
			}
			
		}

</script>							
											
									
										
       