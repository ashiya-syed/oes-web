<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
 <div class="portlet-body " id="searchQuestions">
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_11">
                                        <thead>
                                            <tr>
                                                <th>Question No</th>
												<th>Delete</th>
                                                <th>Question</th>
                                                <th>Subject</th>
                                                <th>Topic</th>
                                                <th>Type</th>
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($questions && count($questions)>0){
													$i=1;$sub='-';$topic='-';
													 foreach($questions as $question){
                                                        if($subjects && count($subjects) > 0){ 
														 
														  foreach( $subjects as $sxcxcub){
															  
																if($sxcxcub->id == $question->subject_id){
																 	$sub=$sxcxcub->sub_name;
																}
														  }
														 }
                                                   if($topics && count($topics) > 0){ 
														 
														  foreach( $topics as $top){
															  
																if($top->topic_id == $question->topic_id){
																 	$topic=$top->topic_name;
																}
														  }
														 }
														 if($question->queType == 1)
														 {$type="Multiple Choice Questions";}
													     else if($question->queType == 2)
														 { $type="Fill in the Blanks";}
													     else if($question->queType == 3){$type="True or False";}
														 else{$type="-";}
														
												?>
                                                <tr class="odd gradeX">
                                                        
                                                        <td><?php echo $i;  ?></td>
														<td><input type="checkbox"  id="deleteAQue[]" name="deleteAQue"  value="<?php echo $question->Sno;  ?>" >	</td>
														<!--<td><?php echo $question->que_id;  ?></td>-->
														<td><?php echo $question->que;  ?></td>
														<td><?php echo $sub;  ?></td>

														<td><?php echo $topic;  ?></td>
														<td><?php echo $type;  ?></td>
												<td class="text-center"> 
												<button class="btn btn-icon-only red" onclick="deleteRefQuestion('<?php echo $question->Sno ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                                 </tr>
											<?php $i++; } }else{ ?>
												
												<!--<div class="alert alert-danger">No results found with your search.</div>-->
											<?php } ?> 
                                        </tbody>
                                    </table>
                                    </div>
									<?php $extraFooter = $this->load->view('dashboard/users_script');
	                                $this->load->view('dashboard/includes/footer',$extraFooter); ?>
									 <script>MathJax.Hub.Queue(['Typeset',MathJax.Hub,'body']);</script>