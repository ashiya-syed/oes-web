  <?php //$this->load->view('dashboard/includes/calender');?>
<div class="modal-content">
                                  
								  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Edit Library</h4>
                                                </div>
                                                <div class="modal-body"> 
										
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="editLibrary" enctype="multipart/form-data" class="form-horizontal" method="post">
                                       <div id="Edit_Coupon_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
											<div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">Library Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="updatelibrary_name" id="updatelibrary_name"   value="<?php echo $details->library_name; ?>"  required /> </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">File
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="file" class="form-control"  name="updatelibrary_file" id="updatelibrary_file" value="<?php echo $details->library_file; ?>"  /> </div>
                                                </div>
                                            </div> 
											<div class="form-group">
											<label class="control-label col-md-3">Select packages
                                                    <span class="required"> * </span>
                                                </label>
												 <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
											<!-- <select id="packasdsdge" class="form-control select2" name="package" multiple>
											 -->
											 <select id="packasdsdge" class="form-control" name="package" >
                                                  
												<option value="">Select packages</option>
																	     
														<?php 
														$selected=array();
														
														$selectedPackages=explode(',',$details->packageId); 
														 if($selectedPackages && count($selectedPackages) > 0){ 
														 
														  foreach( $selectedPackages as $key ){
																$selected[$key] = $key;
														  }
														 }
														
														 if($packages && count($packages)>0){
															foreach($packages as $key=>$val){
																		if (in_array($val->package_id ,$selected))
																		 {
																			 $select="selected";
																		 }	else {
																			 $select="";
																		 }
														 ?>
                                                    <option value="<?php echo $val->package_id ?>" <?php echo $select; ?>><?php echo $val->package_name ?></option>
														 <?php } }  ?>
														 
														 
														 
																				 
                                               </select>
											</div>
											</div>
											</div>
											<input type="hidden" name="id" value="<?php echo $details->id; ?>">
											
                                            
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" onclick="update_library('<?php echo $details->id; ?>')">Update</button>
                                                </div>
                                            </div>									
		<script src="<?php echo SITEURL ?>/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>										
        										