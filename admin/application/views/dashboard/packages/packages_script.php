<script>
function addpackage()
{
	
	var packageName = $('#packageName').val();
	var packageAmount = $('#packageAmount').val();
	
	var test = [];
        $.each($("#TestId option:selected"), function(){            
            test.push($(this).val());
        });
    var tests= test.join(", ");    
	var count= tests.split(',').length;
    var x = new Array();
    x = tests.split(",");
    s = JSON.stringify(x);
	if(packageName=="" || packageAmount==""|| test =="" )
	{
		allButtonsEnabled();
		$('#ADD_PACKAGE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
		 var dataString = 'packageName=' + packageName + '&packageAmount=' + packageAmount+ '&test=' + s;
		
		$.ajax({
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/addPackage',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									
									$('#ADD_PACKAGE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									
									 location.reload();
								}else{
									
									$('#ADD_PACKAGE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}
	return false;
}

function edit_package(packageId)
{
	$.ajax({
                    type: "post",
                    url:baseUrl+'/Ajaxhelper/editPackage',  
				    data: {packageId:packageId},
                            success: function(response){
								 $.getScript(customvalidationJs);
								 $("#editpackage").modal('show');
						         $('#_edit_package_div').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}

function updatepackage(packageId)
{
	var packageName = $('#package_name').val();
	var packageAmount = $('#package_amount').val();
	
	var test = [];
        $.each($("#testids option:selected"), function(){            
            test.push($(this).val());
        });
    var tests= test.join(", ");    
	var count= tests.split(',').length;
    var x = new Array();
    x = tests.split(",");
    s = JSON.stringify(x);
	if(packageName=="" || packageAmount==""|| test =="" )
	{
		allButtonsEnabled();
		$('#UPDATE_PACKAGE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">Please Fill All Fields</div>');
		return false;
	}else{
		 var dataString = 'packageName=' + packageName + '&packageAmount=' + packageAmount+ '&test=' + s + '&packageId=' + packageId;
		
		$.ajax({
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/updatePackage',  
				    data: dataString,
					cache: false,
                    dataType: "json", 
                            success: function(response){
								
								if(response.status == '1')
								{
									
									$('#UPDATE_PACKAGE_MESSAGE').html('<div class="alert alert-success" style="text-align:center">' + response.message + '</div>');
									
									 location.reload();
								}else{
									
									$('#UPDATE_PACKAGE_MESSAGE').html('<div class="alert alert-danger" style="text-align:center">' + response.message + '</div>');
									return false;
								}
								
						    
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	}
	return false;
}

function deletePackage(packageId)
{
	   var ok = confirm("Are you sure to delete?"); 
       if (ok) {
	        $.ajax({
                    type: "post",
                    url:baseUrl+'/Ajaxhelper/deletePackage',  
				    data: {packageId:packageId},
                            success: function(response){
								  location.reload();
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	   }
         return false;
}


function deletePromoterPackage(packageId)
{ 
	   var ok = confirm("Are you sure to delete?"); 
       if (ok) {
	        $.ajax({
                    type: "post",
                    url:baseUrl+'/Ajaxhelper/deletePromoterPackage',  
				    data: {packageId:packageId},
                            success: function(response){ 
								  location.reload();
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
	   }
         return false;
}

var packagesList = new Array();
function showPackgeTestNames(packageId,packageName,checkboxValue)
{  
	if(checkboxValue== 1){
		  packagesList[packagesList.length] =packageId;
		  if(packagesList.length > 0){
		   $('#packagesList').show();
		   $('input#'+packageName+'_'+packageId+'[type="text"]').remove();
	       $('<tr><td><input  style="padding-left:40px;border: 0;outline: 0;background: transparent;" type="text" id="'+packageName+'_'+packageId+'"   value="'+packageName+'"/></td></tr></br>' ).appendTo("#packagesList");
		  }
     }else{
		  var index = packagesList.indexOf(packageId);
		  if (index > -1) {
			packagesList.splice(index, 1);
		  }
		  
		  $('input#'+packageName+'_'+packageId+'[type="text"]').remove();
		  if(packagesList.length == 0){
				$('#packagesList').hide();
			}
	}
}

function getTests(testname,packageId)
{
	if(packageId === undefined) {
      packageId = false;
    }
	  
	       $.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/getTestsByTestNames',  
				    data: {testname:testname,packageId:packageId},
                            success: function(response){
						    $('#packageDIV').html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;
}
jQuery('#phoneNumber').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;
    this.value = this.value.replace(/[^0-9-+]/g,'');
    this.setSelectionRange(start, end);
  });	

 jQuery('#packageName').keyup(function () { 
			 var start = this.selectionStart,
             end = this.selectionEnd;
             this.value = this.value.replace(/[^a-zA-Z0-9  \s]/g,'');
			 this.setSelectionRange(start, end);
 });
 
function minmax(value, min, max) 
{ 
	if(value.length < min || isNaN(parseInt(value)) || parseInt(value) < min) 
		return ""; 
	  else if(value.length > max) 
		return ""; 
	else return value;
}  
function activatePackage(packageId,id){
	
	         $.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/activatePackage',  
				    data: {packageId:packageId,id:id},
                            success: function(response){
						    $('#activePackage_'+packageId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}
function activatePromoterPackage(packageId,id){
	         $.ajax({
				  
                    type: "POST",
                    url:baseUrl+'/Ajaxhelper/activatePromoterPackage',  
				    data: {packageId:packageId,id:id},
                            success: function(response){ 
						    $('#activePackage_'+packageId).html(response);
                    },
						error: function(xhr, statusText, err){
                             console.log("Error:" + xhr.status);  
						}
						
                });
 				
         return false;

}
</script>