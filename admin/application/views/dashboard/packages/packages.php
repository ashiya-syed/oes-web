   <style>
#search_div{
    margin-top:-4%; /* Don't copy this */
	float:left;
}
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
</style>
  <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   
                        <?php echo sideMenu(); ?>
                        

                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <span>Packages</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Packages</span>
                                    </div>
						
                                     <div class="col-md-6 pull-right text-right" id="new">
									
									  <!--<form class="navbar-form  pull-left" role="search" method="get" action="<?php //echo USERS_URL;?>">
										  <div class="input-group stylish-input-group" style="border: 1px solid #26a1ab;" id="search_div">
											<input type="text" class="form-control"  placeholder="Search With Health Issues" name="search" id="search" >
												<span class="input-group-addon">
												<button type="submit">
												<span class="glyphicon glyphicon-search"></span>
												</button>  
												</span>
											</div>
											
									     </form> -->  
                                        
										  <div>
											 
											 <a  href="<?php echo ADD_PACKAGE_URL; ?>" class="btn sbold green"> Add Packages <i class="fa fa-plus"></i>
											 </a>
										 </div> 
								   </div>
                                </div>
                                            
                                <div class="portlet-body">
                                 <?php echo $this->session->flashdata('Error'); ?>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Package Id</th>
                                                <th class="text-center">Package Name</th>
                                                <th class="text-center">Package Code</th>
												<th class="text-center">Course</th>

												<th class="text-center">Package Amount</th>
												<th class="text-center">Package Tests</th>
												<th class="text-center"> Status </th>
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($packages && count($packages)>0){
													$j=1;
													 foreach($packages as $package){
												?>
                                            <tr class="odd gradeX">
                                                        <td class="text-center"><?php echo $j;  ?></td>
														<td class="text-center"><?php echo $package->package_name;  ?></td>
														<td class="text-center"><?php if($package->package_code) { echo $package->package_code; }else{ echo "-";} ?></td>
														<td class="text-center"><?php echo $package->course;  ?></td>
														<td class="text-center"><?php echo $package->package_amount;  ?></td>
														<td class="text-center">
														<?php 
														$packageTests = $package->packageTests;
														if($packageTests && count($packageTests)>0)
														{
															$testNames = '';
															foreach($packageTests as $packageTest)
															{
																$testNames .= $packageTest->testName.', ';
															}
															$testNames = rtrim($testNames, ", ");
													        echo $testNames;
															
														}else { echo "--";  }
														?>
														
														</td>
																							
                                                     <td class="text-center">
                                                    <span id="activePackage_<?php echo $package->package_id; ?>">
													<?php $status=$package->isActive;
														if($status=='1'){ ?>
													   <span class="btn btn-success" style="padding:4px" onclick="activatePackage('<?php echo $package->package_id; ?>','0')"> Active </span>
													 <?php   } else { ?>
													   <span class="btn btn-danger" style="padding:4px" onclick="activatePackage('<?php echo $package->package_id; ?>','1')"> InActive </span>
													 <?php } ?>
													 </span>
                                                </td>
												<td class="text-center"> 
												<!--<button class="btn btn-icon-only default" onclick="edit_package('<?php echo $package->package_id;  ?>');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>-->
												<a class="btn btn-icon-only default" href="<?php echo SITEURL.'/authority/d/editpackage/'.$package->package_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
												<button class="btn btn-icon-only red" onclick="deletePackage('<?php echo $package->package_id; ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                            </tr>
											<?php $j++; } } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		
		<div class="modal fade" id="addusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add Package</h4>
                                                </div>
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal">
                                       <div id="ADD_PACKAGE_MESSAGE"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>		
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Package Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Package Name" id="packageName" name="name" /> </div>
                                                </div>
                                            </div> 

											<div class="form-group">
                                                <label class="control-label col-md-3">Package Amount
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Package Amount" id="packageAmount" name="name"/> </div>
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                            <label class="control-label col-md-3">
															Tests<span class="required"> * </span>
															</label>
                                                            <div class="col-md-6">
                                                                <div class="input-group select2-bootstrap-append">
                                                                    <select id="TestId" class="form-control select2" name="myOption" multiple>
                                                                        <option value="">Select Tests</option>
																		<?php
																		
																		if($tests && count($tests) >0){
																		  for($i=0;$i<count($tests);$i++){
																		  	 
																		?>
																		<option value="<?php echo $tests[$i]->testId; ?>"><?php echo $tests[$i]->testName; ?> </option>
																		<?php } }  ?>
																		</select>
                                                                   
                                                                </div>
                                                            </div>
                                              </div>
											
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="AddPackage" onclick="addpackage()">Add Package</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                      
		
		<div class="modal fade" id="editpackage" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_edit_package_div">
			
			</div>
		</div>
			
										
			

					
       