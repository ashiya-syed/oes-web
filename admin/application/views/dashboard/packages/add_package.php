<link href="<?php echo SITEURL ?>/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITEURL ?>/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<?php $this->load->view('dashboard/includes/calender'); ?>
<style>
.records .col-sm-6.col-xs-12.panel.panel-default {
    /* padding: 5px 10px; */
    /* border: 1px solid #ccc; */
    /* background: #f5f5f5; */
    /* margin-bottom: 1px; */
    box-shadow: 0 0;
}

.panel-default>.panel-heading {
    color: #34495e;
    background-color: #17c4bb;
    border-color: #ddd;
}

form.form-inline.level_details .form-group {
    margin: 5px 1px;
}
form.form-inline.level_details .form-group {
    width: 16.05%;
}
.panel.panel-default span.label.label-info, .panel.panel-default span.label.label-warning {
    font-size: 17px;
    text-transform: capitalize;
    display: inline-block;
    margin: 5px;
    padding: 10px 15px;
    border-radius: 20px;
    line-height: 23px;
    color: #fff;
}
.panel-body span.label.label-warning {
    background: #17c4bb;
}
.panel-body span.label.label-info {
    background: #3289c8;
}
.col-sm-6.col-sm-offset-2.col-xs-12.no-pad {
    margin-top: 30px !important;
}

.panel-heading.text-center {
    display: flex;
}

.panel-heading.text-center .col-sm-2.pull-right {
    float: right;
}

.panel-heading.text-center span.col-sm-7.text-center {
    font-size: 22px;
    text-align: left;
}
</style>
 <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   <?php echo sideMenu(); ?>
                   <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                    <div id="ADD_PACKAGE_MESSAGE"></div>
                     <div class="row" style="margin-top:-14px">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                             <form action="" method="post">        
                                <div class="portlet-body">
								<?php echo $this->session->flashdata('Error'); ?>
                                   <?php if (isset($successMessage)) { echo '<span style="text-align:center;padding:15px 0px;color:green;">'.$successMessage. '</span>';} ?>
                                  <?php if (isset($errorMessage)) { echo "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".$errorMessage. '</strong> </div>';} ?>
                                </div>
				<div class="col-sm-12 no-pad">
                 
					                        
				<div class="clearfix"></div>	  
				<div class="col-sm-9" style="margin-top:10px">
				  <div class="panel panel-default">
					<div class="panel-heading text-center">
				  <span class="col-sm-7 text-center" style="color:#fff">Add Package</span>
					<div class="col-sm-3 pull-right">
						<div class="dropdown">
						  
						</div>
					</div>
					<div class="col-sm-3 no-pad">
					  <input type="search" class="form-control" placeholder="Search" onkeyup="getTests(this.value);">
					</div>
					</div>
				
					<div class="col-sm-6" style="margin-top:15px; padding:0px;">
							<div class="form-group" class="quetionsDiv">
								<label class="control-label col-sm-4" >Package Name:
									<span class="required"> * </span>
								</label>
									<div class="input-icon right col-sm-8">
										<i class="fa"></i>
										 <input type="text"  id="packageName1" class="form-control" placeholder="Enter package Name" name="packageName" required/>
									</div> 
							</div>
					</div>
                      
					  <div class="col-sm-6" style="margin-top:15px; padding:0px;" >
					  <div class="form-group " class="quetionsDiv">
				      <label class="control-label col-sm-4"> Package Type:  <span class="required"> * </span></label>
					  <div class="input-icon right col-sm-8"><i class="fa"></i>
					  <select id="select" class="form-control" name="packageType" onchange="showDiv()" >
                      <option value="paid">Paid </option>
					  <option value="free" >Free</option>
					  </select>
                      </div>
                      </div>
                      </div>
					  <div class="col-sm-6" style="margin-top:15px; padding:0px;" >
					  <div class="form-group " class="">
				      <label class="control-label col-sm-4"> Select course:  <span class="required"> * </span></label>
					  <div class="input-icon right col-sm-8"><i class="fa"></i>
					  <select class="form-control" name="course" id="course" onchange="getTestsByCourseId()">
                      <option value="">Select course</option>
                            <?php if($courses){
                                   foreach($courses as $course){ ?>
                             <option value="<?php echo $course->courseId;?>"><?php echo $course->courseName; ?></option>
                             <?php } } ?>
					  </select>
                      </div>
                      </div>
                      </div>					
					<div class="col-sm-6" style="margin-top:15px; padding:0px;" id="packageAmount">
							<div class="form-group" class="quetionsDiv">
								<label class="control-label col-sm-4" >Package Amount:
								<span class="required"> * </span>
								</label>
									<div class="input-icon right col-sm-8">
										<i class="fa"></i>
										 <input type="text" class="form-control"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');this.value = minmax(this.value, 1, 7)" placeholder="Enter package Amount" name="packageAmount" >
									</div> 
							</div>
					</div>
					
					<div class="col-sm-6" style="margin-top:15px; padding:0px;">
							<div class="form-group" class="quetionsDiv">
								<label class="control-label col-sm-4" >Package Valid From:
									<span class="required"> * </span>
								</label>
									<div class="input-icon right col-sm-8">
										<i class="fa"></i>
										<input type="text" class="form-control" placeholder="dd-mm-yyyy" name="startdate" required id="datepicker" class="date-control"  value="<?php if(isset($_POST['startdate'])) echo $_POST['startdate']?>"></div> 
							</div>
					</div>
					<div class="col-sm-6" style="margin-top:15px; padding:0px;">
							<div class="form-group" class="quetionsDiv">
								<label class="control-label col-sm-4" >Package Valid To:
									<span class="required"> * </span>
								</label>
									<div class="input-icon right col-sm-8">
										<i class="fa"></i>
										<input type="text" class="form-control" placeholder="dd-mm-yyyy" required name="enddate" id="datepicker1" class="date-control"  value="<?php if(isset($_POST['enddate'])) echo $_POST['enddate']?>"></div> 
							</div>
					</div>
					<div id="selectTests"></div>
					<!--div class="clearfix"></div>
					<h5 style="margin:20px 0 10px 15px;"><strong>Tests List:</strong></h5>
					<div class="col-sm-3 pull-right">
						<!--<div class="dropdown">
						  <select class="form-control login_field select-down-new border_none" id="testSelect" onchange="getTestType(this.value)">
                        <option value="1">Prac</option>
                        <option value="2">Main</option>
                    </select>
						</div>-->
					<!--/div-->
						<!--div class="clearfix"></div-->
							<!---div class="panel-body" id="packages">
							<div class="col--sm-12" id="packageDIV">
							 <?php if($tests){
									 foreach($tests as $test){
										 $class = "label label-warning";
							 ?>
							  <span class="<?php echo $class; ?>"><input type="checkbox" name="test[]" value="<?php  echo $test->testId ?>" onclick="showPackgeTestNames('<?php  echo $test->testId ?>','<?php  echo $test->testName ?>', this.checked ? 1 : 0);"> <?php  echo $test->testName; ?>
							  </span>
							  <?php echo "</br>"; ?>
									<?php    } }  ?>
								
							</div>
						</div-->
						
						
					</div>
				</div>

			  <!--div class="col-sm-3" style="margin-top: 21px;">
				 <table id="packagesList" style="display:none;">
				 <tr><th style="padding-left:30px;"><font color="#17c4bb"><u>Packages List</u></font></th></tr>
				 
				 </table>
			</div-->
           </div>
       <div class="col-sm-12 text-center"  id="addbutton">
		    <button type="submit" class="btn green" name="AddPackage" style="margin-bottom:20px">Add Package</button>
		</div>  
									
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                      
                                    </table>
									</form>
                                </div>
							</div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
		
			<div class="modal fade" id="TestsByPackagePopUp" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog"  id="_edit_TestsPackages_div" style="margin-top:80px;">
                      	</div>
            </div>						

<script src="<?php echo SITEURL ?>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo SITEURL ?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo SITEURL ?>/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
	<script>
	function showDiv(){
		var type=$("#select").val();
		if(type == "paid"){
			$("#packageAmount").show();
			
		}
		if(type == "free"){
			$("#packageAmount").hide();
		}
	}
	
	function getTestsByCourseId(){ 
	$('#selectTests').hide();
	var courseId=$('#course').val();
				   if(courseId){
					$.ajax({ 
						           url:baseUrl+'/Packages/courseTests',  
								    type: "POST",
									data: {courseId:courseId},
						           success: function(output) {
								  
								   $('#selectTests').html(output);
								    $('#selectTests').show();
						  }
				    });
				   }else{
					    //$('#NameError').html('Please select atleast one course.');
					   
				   }
}
	jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });//for cntrl+p
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });//cntrl+u

 function copyToClipboard() {
	var aux = document.createElement("input"); 
  aux.setAttribute("value", "Dont take screenshot.");
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
  document.body.innerHTML ="<br><br><br><div style='color: #FF0000; font-size: 36px;text-align: center;'>Screenshorts are restricted for this site.</div>";
 // alert("Screenshots are restricted.");
}

$(window).keyup(function(e){
  if(e.keyCode == 44){
    copyToClipboard();
  }
}); 
$(document).ready(function () {
   $("body").on("contextmenu",function(e){ 
        return false;
    });
});
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
	</script>