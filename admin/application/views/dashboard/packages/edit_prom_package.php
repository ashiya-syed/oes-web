<link href="<?php echo SITEURL ?>/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITEURL ?>/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<?php $this->load->view('dashboard/includes/calender'); ?>
<style>
.records .col-sm-6.col-xs-12.panel.panel-default {
    /* padding: 5px 10px; */
    /* border: 1px solid #ccc; */
    /* background: #f5f5f5; */
    /* margin-bottom: 1px; */
    box-shadow: 0 0;
}

.panel-default>.panel-heading {
    color: #34495e;
    background-color: #17c4bb;
    border-color: #ddd;
}

form.form-inline.level_details .form-group {
    margin: 5px 1px;
}
form.form-inline.level_details .form-group {
    width: 16.05%;
}
.panel.panel-default span.label.label-info, .panel.panel-default span.label.label-warning {
    font-size: 17px;
    text-transform: capitalize;
    display: inline-block;
    margin: 5px;
    padding: 10px 15px;
    border-radius: 20px;
    line-height: 23px;
    color: #fff;
}
.panel-body span.label.label-warning {
    background: #17c4bb;
}
.panel-body span.label.label-info {
    background: #3289c8;
}
.col-sm-6.col-sm-offset-2.col-xs-12.no-pad {
    margin-top: 30px !important;
}

.panel-heading.text-center {
    display: flex;
}

.panel-heading.text-center .col-sm-2.pull-right {
    float: right;
}

.panel-heading.text-center span.col-sm-7.text-center {
    font-size: 22px;
    text-align: left;
}
input[type="checkbox"][readonly] {
  pointer-events: none;
}
</style>
 <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <?php echo sideMenu(); ?>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                   
                     <div class="row" style="margin-top:-14px">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                             <form action="" method="post">        
                                <div class="portlet-body">
								<?php echo $this->session->flashdata('Error'); ?>
                                   <?php if (isset($successMessage)) { echo '<span style="text-align:center;padding:15px 0px;color:green;">'.$successMessage. '</span>';} ?>
                                  <?php if (isset($errorMessage)) { echo "<div class='alert alert-danger alert-dismissable'><a href='javascript:void(0)' class='close' data-dismiss='alert' aria-label='close'></a><strong>".$errorMessage. '</strong> </div>';} ?>
                                </div>
				<div class="col-sm-12 no-pad">
                 
					                        
				<div class="clearfix"></div>	  
				<div class="col-sm-9" style="margin-top:10px">
				  <div class="panel panel-default">
					<div class="panel-heading text-center">
				  <span class="col-sm-7 text-center">Edit Package</span>
					<div class="col-sm-3 pull-right">
						<div class="dropdown">
						  
						</div>
					</div>
					<div class="col-sm-3 no-pad">
					  <input type="search" class="form-control" placeholder="Search" onkeyup="getTests(this.value,'<?php echo $record->package_id; ?>');">
					</div>
					</div>
					<div class="col-sm-6" style="margin-top:15px; padding:0px;">
							<div class="form-group" class="quetionsDiv">
								<label class="control-label col-sm-4" >Package Name:
									<span class="required"> * </span>
								</label>
									<div class="input-icon right col-sm-8">
										<i class="fa"></i>
										 <input type="text" readonly class="form-control"  id="packageName1" name="packageName" value="<?php echo $record->package_name; ?>" required/>
									</div> 
							</div>
					</div>	
					<div class="col-sm-6" style="margin-top:15px; padding:0px;" >
					  <div class="form-group " class="">
				      <label class="control-label col-sm-4">  Course:  <span class="required"> * </span></label>
					  <div class="input-icon right col-sm-8"><i class="fa"></i>
					  <select id="select" class="form-control" name="course" id="course" required style="display:none">
                      <option value="">Select course</option>
                            <?php if($courses){ 
                                   foreach($courses as $course){ ?>
                             <option value="<?php echo $course->courseId;?>" <?php if($record->courseId == $course->courseId){ echo "selected";}?> ><?php echo $course->courseName; ?></option>
                             <?php } } ?>
					  </select>
					   <?php if($courses){ 
                                   foreach($courses as $course){ 
								   if($record->courseId == $course->courseId){
								   ?>
						<input type="text" class="form-control" readonly value="<?php echo $course->courseName; ?>" >

					   <?php } } } ?>
                      </div>
                      </div>
                      </div>	
					<div class="col-sm-6" style="margin-top:15px; padding:0px;" >
					  <div class="form-group ">
				      <label class="control-label col-sm-4"> Package Type:  <span class="required"> * </span></label>&nbsp;
					  <div class="input-icon right col-sm-8">
					  <!--select  readonly id="select" class="form-control" name="packageType" onchange="showDiv()" >
                      <option value="paid" <?php if($record->package_amount == 1){ echo "selected"; } ?>>Paid </option>
					  <option value="free" <?php if($record->package_amount == 0){ echo "selected"; } ?> >Free</option>
					  </select-->
					   <input type="text" readonly class="form-control"  id="packageType" name="packageType" value="<?php if($record->package_amount == 1){ echo "Paid"; } else { echo "Free"; } ?>" required/>
									
                      </div>
                      </div>
                </div>	
				<?php if($record->package_amount>0){ ?>
					<div class="col-sm-6" style="margin-top:15px; padding:0px;" id="pacAmount">
							<div class="form-group" class="quetionsDiv">
								<label class="control-label col-sm-4" >Package Amount:
									
								</label>
									<div class="input-icon right col-sm-8">
										<i class="fa"></i>
										 <input type="text" class="form-control" required onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');this.value = minmax(this.value, 1, 7)" name="packageAmount" value="<?php echo $record->package_amount; ?>" >
									</div> 
							</div>
					</div>	
				<?php } else{ ?>
				<div class="col-sm-6" style="margin-top:15px; padding:0px;" id="psdacAmount">
							<div class="form-group" class="quetionsDiv">
								<label class="control-label col-sm-4" >Package Amount:
									
								</label>
									<div class="input-icon right col-sm-8">
										<i class="fa"></i>
										 <input type="text" class="form-control" required onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');this.value = minmax(this.value, 1, 7)" name="packageAmount" value="" >
									</div> 
							</div>
					</div>	
				<?php } ?>
					<div class="col-sm-6" style="margin-top:15px; padding:0px;">
							<div class="form-group" class="quetionsDiv">
								<label class="control-label col-sm-4" >Package Valid From:
									<span class="required"> * </span>
								</label>
									<div class="input-icon right col-sm-8">
										<i class="fa"></i>
										<input type="text" class="form-control" required  readonly placeholder="dd-mm-yyyy" name="startdate" id="" class="date-control"  value="<?php if(isset($record->package_valid_from)) echo date("d-m-Y", strtotime($record->package_valid_from) ) ;?>"></div> 
							</div>
					</div>
					<div class="col-sm-6" style="margin-top:15px; padding:0px;">
							<div class="form-group" class="quetionsDiv">
								<label class="control-label col-sm-4" >Package Valid To:
									<span class="required"> * </span>
								</label>
									<div class="input-icon right col-sm-8">
										<i class="fa"></i>
										<input type="text" class="form-control" readonly required placeholder="dd-mm-yyyy" name="enddate" id="" class="date-control"  value="<?php if(isset($record->package_valid_from)) echo date("d-m-Y", strtotime($record->package_valid_to) ) ; ?>"></div> 
							</div>
					</div>
						<div class="clearfix"></div>
					<h5 style="margin:20px 0 10px 15px;"><strong>Tests List:</strong></h5>
						<div class="clearfix"></div>
						<div class="panel-body" id="packages">
							<div class="col--sm-12" id="packageDIV">
							<?php
								 $selected=array();
								 $packageTests = explode(',', $record->package_test);
								 if($packageTests && count($packageTests) > 0){ 
										foreach( $packageTests as $key=>$val ){
													$selected[$key] = $val;
										}
								}
																		
							?>
							<?php if($tests){
									 foreach($tests as $key=>$val){
										if (in_array($val->testId ,$selected))
										{
												$select="checked";
										}	else {
												$select="";
								        }
										 $class = "label label-warning";
							 ?>
							 
							 
							 <?php if($select == "checked"){ ?>
							  <span class="<?php echo $class; ?>"><input readonly   type="checkbox" name="test[]" value="<?php  echo $val->testId ?>" onclick="getPackageAmount('<?php  echo $val->testId ?>',  this.checked ? 1 : 0);" <?php echo $select ?>> <?php  echo $val->testName; ?>
							  </span>
							  <?php echo "</br>"; ?>
							<?php    } } } ?>
								
							</div>
						</div>
						
					</div>
				</div>

			  <div class="col-sm-4" id="packages_tests_list"  style="display:none;border:2px solid lightblue;width:20%;">
				 <table id="packagesList">
				 <tr><th style="padding-left:30px;"><font color="#17c4bb"><u>Packages List</u></font></th></tr>
				 </table>
				 <br>
				
				 <table id="testsList">
				 <tr><th style="padding-left:30px;"><font color="#17c4bb"><u>Tests List</u></font></th></tr>
				 </table>

			</div>
           
          </div>

         <div class="col-sm-12 text-center"  id="addbutton">
		    <button type="submit" class="btn green" name="EditPackage" style="margin-bottom:20px">Update Package</button>
		</div>  
									
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                      
                            </table>
									</form>
                                </div>
							</div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
		
									
			<div class="modal fade" id="TestsByPackagePopUp" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog"  id="_edit_TestsPackages_div" style="margin-top:80px;">
                      	</div>
            </div>						
<script src="<?php echo SITEURL ?>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo SITEURL ?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo SITEURL ?>/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
	<script>
	/* $(function(){
		var type=$("#select").val();
		if(type == "paid"){
			$("#packageAmount").show();
		}
		if(type == "free"){
			$("#packageAmount").hide();
		}
	} */
	$("#psdacAmount").hide();
	function showDiv(){ 
		 var type=$("#select").val();
		if(type == "paid"){ 
			$("#psdacAmount").show();
		}
		if(type == "free"){
			$("#psdacAmount").hide();
			$("#pacAmount").hide();
		} 
	}
	
	jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });//for cntrl+p
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });//cntrl+u

 function copyToClipboard() {
	var aux = document.createElement("input"); 
  aux.setAttribute("value", "Dont take screenshot.");
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
  document.body.innerHTML ="<br><br><br><div style='color: #FF0000; font-size: 36px;text-align: center;'>Screenshorts are restricted for this site.</div>";
 // alert("Screenshots are restricted.");
}

$(window).keyup(function(e){
  if(e.keyCode == 44){
    copyToClipboard();
  }
}); 
$(document).ready(function () {
   $("body").on("contextmenu",function(e){ 
        return false;
    });
});
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
	</script>