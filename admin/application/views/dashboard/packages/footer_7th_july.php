<!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> <?php echo date("Y"); ?> &copy; Onyx Educationals.
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
       <?php 
		$segment=$this->uri->segment(3);
		
		if($segment == "packages" ){
		?>
		 <script src="<?php echo SITEURL ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<?php } ?>
		<script src="<?php echo SITEURL ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo SITEURL ?>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo SITEURL ?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo SITEURL ?>/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        
		<script src="<?php echo SITEURL ?>/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<script src="<?php echo SITEURL ?>/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
		
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo SITEURL ?>/assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
		
		<script src="<?php echo SITEURL ?>/assets/global/scripts/customValidation.js" type="text/javascript"></script>
		<script> var customvalidationJs='<?php echo SITEURL ?>/assets/global/scripts/customValidation.js';  </script>
		<script src="<?php echo SITEURL ?>/assets/global/scripts/forms_refresh.js" type="text/javascript"></script>
		 <?php if(isset($extraFooter)){ print_r($extraFooter); } ?>
    </body>

</html>