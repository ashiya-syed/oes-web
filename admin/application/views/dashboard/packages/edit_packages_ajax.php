<div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Edit Package</h4>
                                                </div>
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal">
                                       <div id="UPDATE_PACKAGE_MESSAGE"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>		
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Package Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Package Name" id="package_name" name="name" value="<?php echo $record->package_name; ?>"/> </div>
                                                </div>
                                            </div> 

											<div class="form-group">
                                                <label class="control-label col-md-3">Package Amount
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Package Amount" id="package_amount" name="name" value="<?php echo $record->package_amount; ?>"/> </div>
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                            <label class="control-label col-md-3">
															Tests<span class="required"> * </span>
															</label>
															   <?php
																		 $selected=array();
																		 $packageTests = explode(',', $record->package_test);
																		 
																		 if($packageTests && count($packageTests) > 0){ 
																		 
																		  foreach( $packageTests as $key=>$val ){
																				$selected[$key] = $val;
																		  }
																		 }
																		
																	?>
															
                                                            <div class="col-md-6">
                                                                <div class="input-group select2-bootstrap-append">
                                                                    <select id="testids" class="form-control select2" name="myOption" multiple>
                                                                        <option value="">Select Tests</option>
																			
																		
																		<?php
																		
																		if($tests && count($tests) >0){
																		  foreach($tests as $key=>$val){
																		  	 if (in_array($val->testId ,$selected))
																			 {
																				 $select="selected";
																			 }	else {
																				 $select="";
																			 }
																		?>
																		<option value="<?php echo $val->testId; ?>" <?php echo $select; ?>><?php echo $val->testName; ?> </option>
																		<?php } }  ?>
																		</select>
                                                                   
                                                                </div>
                                                            </div>
                                              </div>
											
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="updatePackage" onclick="updatepackage('<?php echo $record->package_id; ?>')">update Package</button>
                                                </div>
                                            </div>
											
		<script src="<?php echo SITEURL ?>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>    
        <script src="<?php echo SITEURL ?>/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>