<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Onyx Educationals</title>
		<link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/logo-default.png" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo SITEURL ?>/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo SITEURL ?>/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo SITEURL ?>/assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo SITEURL ?>/assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        
		<link href="<?php echo SITEURL ?>/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyA4mW9bS8UJ8xHJAMFxSOctIt5f_Rj6jJM&sensor=false&libraries=places&language=en-AU"></script>
		
		 <script>
           var baseUrl= "<?php echo SITEURL ?>";
		   
        </script>
		<link rel="shortcut icon" href="favicon.ico" />
  
      </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
				<?php 	$Role_Id=$this->session->userdata('Role_Id'); ?>
				<?php 	
				 if($Role_Id == SUPERADMIN_ROLE_ID) {?> 
                    <a href="<?php echo ADMIN_DASHBOARD_URL ?>">
                         <img src="<?php echo SITEURL ?>/assets/logo-default.png" alt="logo" class="logo-default" style="width:50px" /> </a>
				<?php  }else{?>
				<a href="<?php echo RESELLER_DASHBOARD_URL ?>">
				<img src="<?php echo SITEURL ?>/assets/logo-default.png" alt="logo" class="logo-default" style="width:50px" /> </a>
				
				<?php }	?>
				   <div class="menu-toggler sidebar-toggler" style="margin-top:25px;">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->
                <div class="page-actions">
                    <div class="btn-group">
                       <!-- <button type="button" class="btn btn-circle btn-outline red dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-plus"></i>&nbsp;
                            <span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;
                            <i class="fa fa-angle-down"></i>
                        </button>-->
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-docs"></i> New Post </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-tag"></i> New Comment </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-share"></i> Share </a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-flag"></i> Comments
                                    <span class="badge badge-success">4</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-users"></i> Feedbacks
                                    <span class="badge badge-danger">2</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- END PAGE ACTIONS -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    
					 <div class="page-actions">
                    <div class="btn-group">
					<!--<?php 	 //if($Role_Id != PARTNER_ROLE_ID) { ?>
					 <button type="button" onclick="location.href='<?php //echo RECORDS_URL ?>';" class="btn sbold green btn-circle btn-outline ">
                      <i class="fa fa-plus"></i>&nbsp;                          
						  <span class="hidden-sm hidden-xs" >Add Health Records&nbsp;</span>&nbsp;</a>
                        </button>
					<?php //} ?> -->
                    </div>
                </div>
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte 
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> 0 </span>
                                </a>
                               
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte 
                            <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-envelope-open"></i>
                                    <span class="badge badge-default"> 0 </span>
                                </a>
                              
                            <!-- END INBOX DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte 
                            <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-calendar"></i>
                                    <span class="badge badge-default"> 0 </span>
                                </a>
                               
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								 <?php $profilePicture=$this->session->userdata('profilePicture');
								      if($profilePicture == NULL){
										   $profilePicture='icon-user-default.png';
										}
											 
									?>
                                    <img alt="" class="img-circle" src="<?php echo SITEURL ?>/assets/layouts/layout2/img/<?php echo $profilePicture; ?>" />
									<?php if($this->session->userdata('User_Name')) $User_Name= $this->session->userdata('User_Name');else $User_Name=$this->session->userdata('reseller_Name'); 
									?><span class="username username-hide-on-mobile">  <?php echo $User_Name ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
								 <?php 	if($Role_Id == SUPERADMIN_ROLE_ID) {?>
								  <li>
								
                                      <a href="<?php echo PROFILE_URL ?>">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
								<?php }else{ ?>
								 <li>
								<a href="<?php echo RESELLER_PROFILE_URL ?>">
                                 <i class="icon-user"></i> My Profile </a>
                                    </li>
								<?php } ?>
							
									 <?php if($Role_Id == SUPERADMIN_ROLE_ID) {?>
								 <li>
                                    <a href="<?php echo LOGOUT_URL ?>">
                                        <i class="icon-key"></i> Log Out </a>
                                    </li>
							      <?php   }else{ ?>
									 <li>
                                        <a href="<?php echo RESELLER_LOGOUT_URL ?>">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
									
								<?php } ?>
								
									
								</ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                           <!--- <li class="dropdown dropdown-extended quick-sidebar-toggler">
                                <span class="sr-only">Toggle Quick Sidebar</span>
                                <i class="icon-logout"></i>
                            </li>--->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
<script>
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 80){  e.preventDefault(); } });//for cntrl+p
jQuery(document).bind("keyup keydown", function(e){ if(e.ctrlKey && e.keyCode == 85){  e.preventDefault(); } });//cntrl+u

 function copyToClipboard() {
	var aux = document.createElement("input"); 
  aux.setAttribute("value", "Dont take screenshot.");
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
  document.body.innerHTML ="<br><br><br><div style='color: #FF0000; font-size: 36px;text-align: center;'>Screenshorts are restricted for this site</div>";
 // alert("Screenshots are restricted.");
}

$(window).keyup(function(e){
  if(e.keyCode == 44){
    copyToClipboard();
  }
}); 
$(document).ready(function () {
   $("body").on("contextmenu",function(e){ 
        return false;
    });
});
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>