   <style>
#search_div{
    margin-top:-4%; /* Don't copy this */
	float:left;
}
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
.pac-container {
			background-color: #FFF;
			z-index: 20;
			position: fixed;
			display: inline-block;
			float: left;
		}
		.modal{
			z-index: 20;   
		}
		.modal-backdrop{
			z-index: 10;        
		}​
</style>

  <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   
                        <?php echo sideMenu(); 
						$this->load->view('dashboard/includes/calender');
						 		$this->load->model('Usermodel'); 
 ?>
                        

                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <span>Users</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Users</span>
                                    </div>
                              
         <!--&nbsp;&nbsp;&nbsp;
      End Date&nbsp; <input type="text" placeholder="mm/dd/yyyy" name="enddate" id="datepicker" class="date-control" onchange="form.submit();" value="<?php if(isset($_POST['enddate'])) echo $_POST['enddate']?>">
-->
                                     <div class="col-md-6 pull-right text-right" id="new">
									
		
      
									  <!--<form class="navbar-form  pull-left" role="search" method="get" action="<?php //echo USERS_URL;?>">
										  <div class="input-group stylish-input-group" style="border: 1px solid #26a1ab;" id="search_div">
											<input type="text" class="form-control"  placeholder="Search With Health Issues" name="search" id="search" >
												<span class="input-group-addon">
												<button type="submit">
												<span class="glyphicon glyphicon-search"></span>
												</button>  
												</span>
											</div>
											
									     </form> -->  
                                        
										  <div class="btn-group">
											<button data-toggle="modal" href="#addusers" class="btn sbold green"> Add New User<i class="fa fa-plus"></i>
											 </button>   </div> 
											<!-- <div class="btn-group"> <button data-toggle="modal" href="#addDummyusers" class="btn sbold green"> Add Dummy User<i class="fa fa-plus"></i>
											 </button></div>--->
										
								   </div>
								    
	  
	 <!-- <div class="col-md-12 pull-left text-right" >
			 <label class="control-label col-md-3">Packages
             </label>
			<div class="col-md-6">
             <select id="package" onchange="getUsersByPackage()" class="form-control select2" name="package" multiple>
             <option value="">Select Packages</option>
			<?php 
				$selected=array();
				if($packages && count($packages) > 0){ 
				foreach( $packages as $key=>$val ){
				$selected[$key] = $val->package_id;
				 }
				 }
				if($packages && count($packages)>0){
				foreach($packages as $key=>$val){
				$select="";
				 ?>
               <option value="<?php echo $val->package_id ?>" <?php echo $select; ?>><?php echo $val->package_name?></option>
				 <?php } }  ?>
				</select>
               </div>
			   <div class="col-md-12 pull-right text-right" >
			   <label class="control-label col-md-3">Tests
                                                    <span class="required"> * </span>
                                                </label>
											    <div class="col-md-6">
                                               <select id="test" onchange="getUsersByPackage()" class="form-control select2" name="test" multiple required>
                                               <option value="">Select Test</option>
														<?php 
														$selected=array();
														 if($tests && count($tests) > 0){ 
														 
														  foreach( $tests as $key=>$val ){
																$selected[$key] = $val->testId;
														  }
														 }
														
														 if($tests && count($tests)>0){
															foreach($tests as $key=>$val){
																		/*if (in_array($val->testId ,$selected))
																		 {
																			 $select="selected";
																		 }	else {
																			 $select="";
																		 }*/
																		  $select="";
														 ?>
                                                    <option id="selectedTest" value="<?php echo $val->testId ?>" <?php echo $select; ?>><?php echo $val->testName.'  - RS '.$val->testAmount ?></option>
														 <?php } }  ?>
																		 
                                               </select>
                                            </div>
											</div>
											
	  </div>--->
											<!--package end-->
        </div>
                              
                            <div class="container-fluid">
									<div class="row">
										<div class="col-sm-3">
											<div class="header-encuesta">
												<span class="text-white"> Search user by Joining Month</span>
											</div>&nbsp;
											<div>
												 <div class="btn-group">
										<a class="form-control" data-toggle="dropdown" href="ownertransactions"> Select Month <span class="caret"></span> </a>
										  <ul class="dropdown-menu">
											<li><a href="<?php echo USER_MANAGEMENT_URL?>";>All</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/01'?>">January</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/02'?>">February</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/03'?>">March</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/04'?>">April</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/05'?>">May</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/06'?>">June</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/07'?>">July</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/08'?>">August</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/09'?>">September</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/10'?>">October</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/11'?>">November</a></li>
											<li><a href="<?php echo USER_MANAGEMENT_URL.'/12'?>">December</a></li>
											<!--               <li class="divider"></li>-->
										  </ul>
										</div>
											</div>
										</div>
										<div class="col-sm-3">
											<div >
												<span >Search user by Joining Date&nbsp;</span>
											</div>&nbsp;
											<div>
											  <form   role="search" method="post" action="<?php echo USER_MANAGEMENT_URL?>">
												<input type="text" class="form-control" placeholder="dd-mm-yyyy" name="startdate" id="datepicker3" class="date-control" onchange="form.submit();" value="<?php if(isset($_POST['startdate'])) echo $_POST['startdate']?>">
												<!--<label >Exit Date&nbsp; </label><input type="text" class="form-control" placeholder="mm/dd/yyyy" name="enddate" id="datepicker1" class="date-control" onchange="form.submit();" value="<?php if(isset($_POST['enddate'])) echo $_POST['enddate']?>">
												--></form>
											</div>
										</div>
									</div>
                              </div>

							  </div>
                                            
                                <div class="portlet-body table-responsive">
									 <?php echo $this->session->flashdata('Error');?>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_11">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
												<th>Email </th>
												<th>Mobile</th>
												<th>Address</th>
												<th>Joining Date</th>
												<th>Course</th>
												<th>Packages</th>
												<th>Account </th>
												<th>Last Login</th>
												<th>IP Used</th>
												<th>password</th>
												<th> Status </th>
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($users && count($users)>0){
													$i=1;
													 foreach($users as $user){
														$ipDetails= $this->Usermodel->getIpAddress($user->userId);
														if($ipDetails){
														$ip=$ipDetails->ipAddresss;
														}else{$ip="-";}
														
														$userPackages= $this->Usermodel->getUserPackages($user->userId);
														$courseNames= $this->Usermodel->courseNames($user->course);
														if($userPackages){
															$userPACK="";
															foreach($userPackages as $uPack){
																$userPACK=$uPack->package_name.',';
															}
															$userPACK.=rtrim($userPACK,',');
														}
														
														 if($user->accountType ==1 || $user->accountType ==0){
															 $type = "Active ";
														 }else{
															  $type = "Guest ";
														 }
														 @$pwd= decrypt($user->password);
												?>
                                            <tr class="odd gradeX">
                                                        <td><?php echo $i;  ?></td>
														<td><?php echo @$user->userName;  ?></td>
														<td><?php echo @$user->emailAddress;  ?></td>
														<td class="text-center"><?php echo !empty(@$user->phoneNumber)? @$user->phoneNumber :'-';  ?></td>
														<td class="text-center"><?php echo !empty(@$user->address)? @$user->address :'-';  ?></td>
														<td class="text-center"><?php echo !empty(@$user->createdTime)?  date("d-m-Y", strtotime($user->createdTime) )  :'-';  ?></td>
														<td class="text-center"><?php echo $courseNames; ?></td>
														<td class="text-center"><?php if(@$userPACK) {echo @$userPACK;} else{ echo "-";} ?></td>
														<td class="text-center"><?php echo $type; ?></td>
														<td class="text-center"><?php echo time_ago(@$user->lastLogin);  ?></td>
														<td class="text-center"><?php echo @$ip;  ?></td>
														<td class="text-center"><a href="<?php echo UPDATE_USER_URL.'?id='.$user->userId ?>"><?php echo @$pwd;  ?></a></td>
														   
																							
                                                <td>
                                                    <span id="activeUser_<?php echo $user->userId; ?>">
													<?php $status=$user->isActive;
														if($status=='1'){ ?>
													   <span class="btn btn-success" style="padding:4px" onclick="activateUser('<?php echo $user->userId?>','0')"> Active </span>
													 <?php   } else { ?>
													   <span class="btn btn-danger" style="padding:4px" onclick="activateUser('<?php echo $user->userId?>','1')"> InActive </span>
													 <?php } ?>
													 </span>
                                                </td>
												<td class="text-center"> 
												<button class="btn btn-icon-only default" onclick="edit_user('<?php echo $user->userId  ?>');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
												<button class="btn btn-icon-only red" onclick="deleteUser('<?php echo $user->userId ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                            </tr>
											<?php $i++; } } ?> 
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		<?php $this->load->view('dashboard/users_script'); ?>
		<div class="modal fade" id="addusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add User</h4>
                                                </div>
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal" autocomplete="off">
                                        <div id="Add_User_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

											 
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Name" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/ +?/g, '');" id="username" name="name" /> </div>
                                                </div>
                                            </div> 

											<div class="form-group">
                                                <label class="control-label col-md-3">Email Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Email" id="email_adress" name="email"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <textarea class="form-control" rows="2" cols="50" placeholder="Enter Address" id="adress" name="adress"></textarea>
                                                      </div>
                                            </div>
											</div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Phone Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control"  maxlength="10" placeholder="Enter Phone" id="phone_number" name="number"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Password" id="password" name="password"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Confirm Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Confirm Password" id="confirm_password" name="cpassword"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Account Type
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
														<select class="form-control" onchange="hide(this.value)" id="acntType" name="acntType">
                                                      <option value="1">Active</option>
													  <option  value="2">Guest</option>
                                                      <!--<option  value="3">In Active</option>-->
													  </select>
                                                         </div>
                                            </div>
											</div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Select Course
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6 ">
                                                    <div class="input-icon right" >
                                                        <i class="fa"></i>
                                                        <select class="form-control select2" name="course" onchange="saveValue(this.value)" id="course" multiple required>
                                                         <option value="">Select course</option>
                                                         <?php if($courses){
                                                               foreach($courses as $course){ ?>
                                                          <option value="<?php echo $course->courseId;?>"><?php echo $course->courseName; ?></option>
                                                          <?php } } ?>
			                                             </select>
                                                     </div>
                                                 </div>
											</div> 
											<!--div class="form-group ">
				 <!--  on 13 th--> <!--label class="control-label col-md-3">Select pack/test:  <span class="required"> * </span></label>&nbsp;
					 <div class="col-md-6"><select id="select" class="form-control " onchange="showDiv()" >
                                <option value=""> Select</option>
                                <option value="test"> Tests</option>
								<option value="pack" >Packages</option>
									
					</select>
                </div>
                </div-->
											<!--div class="form-group" id="tttt">
											 <label class="control-label col-md-3">Tests
                                                    <span class="required"> * </span>
                                                </label>
											    <div class="col-md-6">
                                               <select id="test" onchange="getTotalAmount()" class="form-control select2" name="test" multiple required>
                                               <option value="">Select Test</option>
														<?php 
														$selected=array();
														 if($tests && count($tests) > 0){ 
														 
														  foreach( $tests as $key=>$val ){
																$selected[$key] = $val->testId;
														  }
														 }
														
														 if($tests && count($tests)>0){
															foreach($tests as $key=>$val){
																		/*if (in_array($val->testId ,$selected))
																		 {
																			 $select="selected";
																		 }	else {
																			 $select="";
																		 }*/
																		  $select="";
														 ?>
                                                    <option id="selectedTest" value="<?php echo $val->testId ?>" <?php echo $select; ?>><?php echo $val->testName.'  - RS '.$val->testAmount ?></option>
														 <?php } }  ?>
																		 
                                               </select>
                                            </div>
											<!--Amount :<div id="tamount"> - </div>-->
											<!--/div-->
											<!--div class="form-group" id="pppp">
											 <label class="control-label col-md-3">Packages
                                               </label>
											    <div class="col-md-6">
                                               <select id="package" onchange="getTotalAmount()" class="form-control select2" name="package" multiple>
                                               <option value="">Select Packages</option>
														<?php 
														$selected=array();
														 if($packages && count($packages) > 0){ 
														 
														     foreach( $packages as $key=>$val ){
																$selected[$key] = $val->package_id;
														     }
														 }
														if($packages && count($packages)>0){
															foreach($packages as $key=>$val){
																		$select="";
														 ?>
                                                    <option value="<?php echo $val->package_id ?>" <?php echo $select; ?>><?php echo $val->package_name.'  - RS '.$val->package_amount ?></option>
														 <?php } }  ?>
												</select>
                                            </div>
											<!-- Amount : <div id="pamount"> - </div>-->
											<!--/div-->
											 <div id="courseajax"></div>
											<div class="form-group" id="tAmnt">
											 <label class="control-label col-md-3">Total Amount
                                                    
                                                </label>
											    <div class="col-md-6"><input type="text" class="form-control" placeholder="Total Amount" readonly value="" id="amount"></div>
										</div>
											
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_users()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
									<div class="modal fade" id="addDummyusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                               <!-- <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add Dummy User</h4>
                                                </div>-->
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal" autocomplete="off">
                                        <div id="Add_dUMMY_User_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

											 
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Name" id="dummy_user_name" name="name" /> </div>
                                                </div>
                                            </div> 

											<div class="form-group">
                                                <label class="control-label col-md-3">Email Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Email" id="dummy_email_adress" name="email"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Phone Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" maxlength="10" placeholder="Enter Phone" id="dummy_phone_number" name="number"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Password" id="dummy_password" name="password"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Confirm Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Confirm Password" id="dummy_confirm_password" name="cpassword"/> </div>
                                                </div>
                                            </div>
											
											</div>
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_dummy_users()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                      
		
		<div class="modal fade" id="edituser" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_edit_user_div">
			
			</div>
		</div>
		<script>
	  //on 13 th
	$(document).ready(function() {
		
				$('#tttt').hide();
				$('#pppp').hide();
			});
	function showDiv(){ 
				var select=$("#select").val();
				if(select == 'test'){ 
				$('#tttt').show();
				$('#pppp').hide();
				}
                if(select == 'pack'){
                  $('#pppp').show();
                  $('#tttt').hide();}
              if(select == ''){
                  $('#pppp').hide();
                  $('#tttt').hide();}
				}
				function saveValue(value){ 
					var course = [];
					$.each($("#course option:selected"), function(){            
						course.push($(this).val());
					});
                   var courses= course.join(", ");
	                $('#courseIds').val(courses);
					$.ajax({ 
						           url:baseUrl+'/Ajaxhelper/saveCourseInSession',  
								    type: "POST",
									data: {courses:courses},
						           success: function(output) { 
								   $('#courseajax').html(output);
						  }
				    });
					}
				
	</script>
	
										
			

					
       