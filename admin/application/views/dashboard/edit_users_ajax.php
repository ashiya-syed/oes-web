
													
				<div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Edit User</h4>
                                                </div>
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal">
                                       <div id="Edit_User_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
											<div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="name" id="updateuser_name"  value="<?php echo $details->userName; ?>"  required/> </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Email Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="email" id="update_emailAddress" value="<?php echo $details->emailAddress; ?>" readonly /> </div>
                                                </div>
                                            </div> 
											<div class="form-group">
                                                <label class="control-label col-md-3">Phone Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="digits" id="update_phoneNumber" value="<?php echo $details->phoneNumber; ?>"  /> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Account Type
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
													  <select class="form-control" id="acountType" name="acntType">
													  <?php $status=$details->accountType;?>
                                                      <option value="1" <?php if($status== "1")echo "selected";?>>Active</option>
													  <option  value="2" <?php if($status== "2")echo "selected";?>>Guest</option>
                                                      <!--<option  value="3" <?php if($status== "3")echo "selected";?>>In Active</option>-->
													  </select>
                                                      </div>
                                               </div>
											</div>
                                            
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" onclick="update_user('<?php echo $details->userId; ?>')">Update</button>
                                                </div>
                                            </div>									
													
			<script>
   jQuery('#updateuser_name').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;		
    this.value = this.value.replace(/[^a-zA-Z0-9  \s]/g,'');
	this.setSelectionRange(start, end); 
    });
</script>										