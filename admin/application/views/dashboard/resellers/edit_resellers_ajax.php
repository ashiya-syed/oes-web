 
<div class="modal-content">
                                  
								  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Edit Reseller/Promoter</h4>
                                                </div>
                                                <div class="modal-body"> 
										
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal">
                                       <div id="Edit_Coupon_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
											<div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3"> Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" readonly name="updatereseller_name" id="updatereseller_name"  value="<?php echo $details->reseller_name; ?>"  required/> </div>
                                                </div>
                                            </div>
											<div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3"> Email
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" readonly name="updatereseller_email" id="updatereseller_email"  value="<?php echo $details->reseller_email; ?>"  required/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Phone Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" maxlength="10" name="digits" id="updatereseller_mobile" value="<?php echo $details->reseller_mobile; ?>"  /> </div>
                                                </div>
                                            </div>
											<?php if($details->isReseller == 1){ ?>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Packages Limit
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" class="form-control" name="updatereseller_package" id="updatereseller_package" value="<?php echo $details->reseller_packageLimit; ?>"  /> </div>
                                                </div>
                                            </div> 
											<div class="form-group">
                                                <label class="control-label col-md-3"> Tests Limit
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" class="form-control" name="updatereseller_test" id="updatereseller_test" value="<?php echo $details->reseller_testLimit; ?>"  /> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3"> Users Limit
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" name="updatereseller_user" id="updatereseller_user" value="<?php echo $details->reseller_usersLimit; ?>"  /> </div>
                                                </div>
                                            </div>
											<?php } ?>
											
                                            
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" onclick="update_reseller('<?php echo $details->reseller_id; ?>')">Update</button>
                                                </div>
                                            </div>	
<script>
   jQuery('#updatereseller_name').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;		
    this.value = this.value.replace(/[^a-zA-Z0-9  \s]/g,'');
	this.setSelectionRange(start, end); 
    });
	jQuery('#updatereseller_mobile').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;
    this.value = this.value.replace(/[^0-9-+]/g,'');
    this.setSelectionRange(start, end);
  });
	function minmax(value, min, max) 
		{ 
			if(value.length < min || isNaN(parseInt(value)) || parseInt(value) < min) 
				return ""; 
			else if(value.length > max) 
				return ""; 
			else return value;
		}
</script>												
												