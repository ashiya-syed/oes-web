   <style>
#search_div{
    margin-top:-4%; /* Don't copy this */
	float:left;
}
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
.pac-container {
			background-color: #FFF;
			z-index: 20;
			position: fixed;
			display: inline-block;
			float: left;
		}
		.modal{
			z-index: 20;   
		}
		.modal-backdrop{
			z-index: 10;        
		}​
</style>

  <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   
                        <?php echo sideMenu(); 
						$this->load->view('dashboard/includes/calender');?>
                        

                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <span>Resellers/Promoters</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase">Resellers/Promoters</span>
                                    </div>
                              
                              <div class="col-md-6 pull-right text-right" id="new">
								 <div class="btn-group">
											<button data-toggle="modal" href="#addusers" class="btn sbold green"> Add New Resellers/Promoters<i class="fa fa-plus"></i>
											 </button>   </div> 
											
								   </div>
								    
                                </div>
                                            
                                <div class="portlet-body table-responsive">
                                 <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_11">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Reseller Name</th>
												 <th>Reseller Email</th>
												 <th>Reseller Mobile</th>
												 <th>Reseller/Promoter</th>
                                                 <th>Packages Limit</th>
                                                 <th>Tests Limit</th>
                                                 <th>Users Limit</th>
                                                 <th>Status </th>
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($resellers && count($resellers)>0){ $i=1;
													 foreach($resellers as $reseller){

														/* $userName=""; $PackageName="";$testName="";
														if($reseller->userDetails && count($reseller->userDetails)>0){
															$userDetails=$reseller->userDetails;
															foreach($userDetails as $user){
																$ustring=$user->userName.',';
															}
															$userName=rtrim($ustring,',');
															
														}
														if($reseller->PackageDetails && count($reseller->PackageDetails)>0){
															$PackageDetails=$reseller->PackageDetails;
															foreach($PackageDetails as $Package){
																$pstring=$Package->package_name.',';
															}
															$PackageName=rtrim($pstring,',');
															
														}
														
														if($reseller->testDetails && count($reseller->testDetails)>0){
															$testDetails=$reseller->testDetails;
															foreach($testDetails as $test){
																$test=$test->package_name.',';
															}
															$testName=rtrim($test,',');
															
														}*/
														if($reseller->isReseller ==  1){
															$type="Reseller";
														}else if($reseller->isReseller ==  2){
															$type="Promoter";
														}else{
															$type="-";
														}
														//neatPrintAndDie($reseller);
														if($reseller->packageCount)
															$ResellerPackages=$reseller->packageCount;
														else $ResellerPackages=0;
														if($reseller->testsCount)
															$ResellerTests=$reseller->testsCount;
														else $ResellerTests=0;
														if($reseller->userCount)
															$ResellerUsers=$reseller->userCount;
														else $ResellerUsers=0;
												?>
                                            <tr class="odd gradeX">
                                                        <td><?php echo $i;  ?></td>
														<td><?php echo $reseller->reseller_name;  ?></td>
														<td><?php echo $reseller->reseller_email;  ?></td>
														<td><?php echo $reseller->reseller_mobile;  ?></td>
														<td><?php echo $type;  ?></td>
														<td><?php echo $ResellerPackages.'/'.$reseller->reseller_packageLimit;  ?></td>
														<td><?php echo $ResellerTests.'/'.$reseller->reseller_testLimit;  ?></td>
														<td><?php echo $ResellerUsers.'/'.$reseller->reseller_usersLimit;  ?></td>
														<!--<td><?php if(@$PackageName) echo $PackageName;else echo '-';  ?></td>
														<td><?php if(@$testName) echo $testName;else echo '-';  ?></td>
														<td><?php if(@$userName) echo $userName;else echo '-';  ?></td>-->
														   
																							
                                                <td>
                                                    <span id="activeUser_<?php echo $reseller->reseller_id; ?>">
													<?php $status=$reseller->isActive;
														if($status=='1'){ ?>
													   <span class="btn btn-success" style="padding:4px" onclick="activateReseller('<?php echo $reseller->reseller_id?>','0')"> Active </span>
													 <?php   } else { ?>
													   <span class="btn btn-danger" style="padding:4px" onclick="activateReseller('<?php echo $reseller->reseller_id?>','1')"> InActive </span>
													 <?php } ?>
													 </span>
                                                </td>
												<td class="text-center"> 
												<button class="btn btn-icon-only default" onclick="edit_reseller('<?php echo $reseller->reseller_id  ?>');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
												<button class="btn btn-icon-only red" onclick="deleteReseller('<?php echo $reseller->reseller_id ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                            </tr>
											<?php $i++; } } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		
		<div class="modal fade" id="addusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add Reseller/Promoter</h4>
                                                </div>
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal" autocomplete="off">
                                        <div id="Add_Coupons_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

											  <div class="form-group">
                                                <label class="control-label col-md-3">Account Type
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                       <select id="account_type" class="form-control "name="account_type" onchange="showDiv()">
														<option value="1">Reseller</option>
														<option value="2" >Promoter</option>
													 </select>
                                                </div>
												</div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Name" id="reseller_name" name="reseller_name" /> </div>
                                                </div>
                                            </div> 
											<div class="form-group">
                                                <label class="control-label col-md-3">Email
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="email" class="form-control" placeholder="Enter Email" id="reseller_email" name="reseller_email" /> </div>
                                                </div>
                                            </div> 
                                           <div class="form-group">
                                                <label class="control-label col-md-3">Phone Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control"  maxlength="10" placeholder="Enter Phone" id="reseller_mobile" name="number"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password"  class="form-control" placeholder="Enter Password" id="reseller_password" name="reseller_password"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Confirm password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                       <input type="password" class="form-control" placeholder="Enter Confirm Password" name="reseller_cpassword" id="reseller_cpassword" class="date-control"  value="">
													   </div>
                                                </div>
                                            </div>
											<div class="form-group" id="packageLimit">
                                                <label class="control-label col-md-3">Packages Limit
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                       <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" placeholder="Enter Packages Limit" name="reseller_packageLimit" id="reseller_packageLimit" class="date-control"  value="">
													   </div>
                                                </div>
                                            </div>
											<div class="form-group" id="testLimit">
                                                <label class="control-label col-md-3">Tests Limit
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                       <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" placeholder="Enter Tests Limit " name="reseller_testLimit" id="reseller_testLimit" class="date-control"  value="">
													   </div>
                                                </div>
                                            </div>
											<div class="form-group" id="userLimit">
                                                <label class="control-label col-md-3">Users Limit
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                       <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" placeholder="Enter Users Limit" name="reseller_usersLimit" id="reseller_usersLimit" class="date-control"  value="">
													   </div>
                                                </div>
                                            </div>
											
											
											
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_reseller()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
									<div class="modal fade" id="addDummyusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                               <!-- <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add Dummy User</h4>
                                                </div>-->
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal" autocomplete="off">
                                        <div id="Add_dUMMY_User_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

											 
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Name" id="dummy_user_name" name="name" /> </div>
                                                </div>
                                            </div> 

											<div class="form-group">
                                                <label class="control-label col-md-3">Email Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Email" id="dummy_email_adress" name="email"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Phone Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" maxlength="10" placeholder="Enter Phone" id="dummy_phone_number" name="number"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Password" id="dummy_password" name="password"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Confirm Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Confirm Password" id="dummy_confirm_password" name="cpassword"/> </div>
                                                </div>
                                            </div>
											
											</div>
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_dummy_users()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                      
		
		<div class="modal fade" id="edituser" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_edit_user_div">
			
			</div>
		</div>
	
		<script>
		//neww
		jQuery('#reseller_name').keyup(function () {
		$('#reseller_name').val($('#reseller_name').val().replace(/ +?/g, '')); //on 17 th
        /* var start = this.selectionStart,
        end = this.selectionEnd;		
        this.value = this.value.replace(/[^a-zA-Z0-9  \s]/g,'');
	    this.setSelectionRange(start, end);  */
        });
		
		$('input').attr('autocomplete', 'off');
		
		function minmax(value, min, max) 
		{ 
			if(value.length < min || isNaN(parseInt(value)) || parseInt(value) < min) 
				return ""; 
			else if(value.length > max) 
				return ""; 
			else return value;
		}
		jQuery('#reseller_mobile').keyup(function () {
    var start = this.selectionStart,
    end = this.selectionEnd;
    this.value = this.value.replace(/[^0-9-+]/g,'');
    this.setSelectionRange(start, end);
  });
         function showDiv()
			  {
				  var account_type =$("#account_type").val(); 
				  if(account_type == '2')
				  {
					  $('#packageLimit').hide();
					  $('#testLimit').hide();
				      $('#userLimit').hide();
				  }
				  if(account_type == '1')
				  {
					  $('#packageLimit').show();
					  $('#testLimit').show();
				      $('#userLimit').show();
				  }
			  }
		</script>								
			

					
       