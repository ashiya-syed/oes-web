     <?php $result=$details; ?>
	 <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   <?php echo sideMenu(); ?>
                   <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                   
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo RESELLER_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
							 <span>Profile</span>
                             <i class="fa fa-angle-right"></i>
                            </li> 
							
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Profile</span>
                                    </div>
                                    <div class="col-md-6 pull-right text-right"   id="new">
                                                <div class="btn-group">
                                                    <button data-toggle="modal"  href="#Update_Password"  onclick="edit_reseller_password('<?php echo $result->reseller_id;  ?>');" class="btn sbold green"> Update Password
                                                       
                                                    </button>
                                                </div>
                                            </div>
                                </div>
								
                                <div class="portlet-body">
                                   <!-- <div class="table-toolbar">
                                        <div class="row">
                                           
                                            
                                        </div>
                                    </div>-->
                                    <form action="#" id="form_sample_2" class="form-horizontal">
                                       <div id="PROFILE_MESSAGE"></div>
										<div class="form-body" id="replacefunn">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
                                            <div class="form-group">
                                               
                                                    
                                                </label>
                                                <div class="col-md-6">
                                                 <!-- <div id="PROFILE_MESSAGE"></div>-->
                                            </div> 
											</div>
										
											<div class="form-group">
                                                <label class="control-label col-md-3">Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" value="<?php echo $result->reseller_name; ?>" placeholder="Enter Name" readonly id="name" name="name" /> </div>
                                                </div>
                                            </div>
                                             <div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Email Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Email" readonly  value="<?php echo $result->reseller_email; ?>" id="emailAdress" name="emailAdress"  /> </div>
                                                </div>
                                            </div>
									   <div class="form-group" id="emmailid">
                                                <label class="control-label col-md-3" >Mobile Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" maxlength="10" placeholder="Enter number"  id="number" value="<?php echo $result->reseller_mobile; ?>" name="number"  /> </div>
                                                </div>
                                            </div>
									  
								        <!---  <div class="form-group">
                                                <label class="control-label col-md-3">Image
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <img src="C:\xampp\htdocs\MizpahHealth\assets\global\img\black-baby-girl-hairstyles(1).jpg" alt="Smiley face" height="42" width="42">
														</div>
                                                </div>
                                            </div>  ------>
								            
											<div class="modal-footer btn-group center-block "  id="button">
											    
												<div class="text-center">
											 <button type="button" class="btn green" onclick="update_reseller__profile('<?php echo $result->reseller_id; ?>')">Update Profile</button>
                                                </div>
										
                                        </div>
                                      	</div>	
                                    </form>
								
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		
	
			<div class="modal fade" id="Update_Password" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_update_password_div">
			
			</div>
		</div>
										
										
       