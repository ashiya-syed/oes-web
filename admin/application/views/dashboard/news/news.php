   <style>
#search_div{
    margin-top:-4%; /* Don't copy this */
	float:left;
}
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
</style>
  <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   
                        <?php echo sideMenu(); ?>
                        

                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <span>News & Updates</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> News & Updates</span>
                                    </div>
						
                                     <div class="col-md-6 pull-right text-right" id="new">
									
									  <!--<form class="navbar-form  pull-left" role="search" method="get" action="<?php //echo USERS_URL;?>">
										  <div class="input-group stylish-input-group" style="border: 1px solid #26a1ab;" id="search_div">
											<input type="text" class="form-control"  placeholder="Search With Health Issues" name="search" id="search" >
												<span class="input-group-addon">
												<button type="submit">
												<span class="glyphicon glyphicon-search"></span>
												</button>  
												</span>
											</div>
											
									     </form> -->  
                                        
										  <div class="btn-group">
											 
											 <button data-toggle="modal" href="#addusers" class="btn sbold green"> Add News<i class="fa fa-plus"></i>
											 </button>
										  </div> 
								   </div>
                                </div>
                                            
                                <div class="portlet-body">
                                  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_11">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>News Title</th>
												<th>Description</th>
												<th>Created Time</th>
												<th> Status </th>
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($news && count($news)>0){
													$i=1;
													 foreach($news as $new){
												?>
												
                                            <tr class="odd gradeX">
                                                        <td><?php echo $i;  ?></td>
														
														<td><?php if(strlen($new->newsTitle)>20){ echo substr($new->newsTitle,0,20).'...'; }else{echo substr($new->newsTitle,0,20);} ?></td>
														<td><?php  echo substr($new->description,0,50).'...';  ?></td>
														<td class="text-center"><?php  echo date(' d F Y', strtotime($new->createdTime));?></td>
														
												 <td>
                                                    <span id="activeUser_<?php echo $new->id; ?>">
													<?php $status=$new->isActive;
														if($status=='1'){ ?>
													   <span class="btn btn-success" style="padding:4px" onclick="activateNews('<?php echo $new->id?>','0')"> Active </span>
													 <?php   } else { ?>
													   <span class="btn btn-danger" style="padding:4px" onclick="activateNews('<?php echo $new->id?>','1')"> InActive </span>
													 <?php } ?>
													 </span>
                                                </td>
												<td class="text-center"> 
												<button class="btn btn-icon-only default" onclick="edit_news('<?php echo $new->id  ?>');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
												<button class="btn btn-icon-only red" onclick="deleteNews('<?php echo $new->id ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                            </tr>
											<?php $i++; } } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
		
		<div class="modal fade" id="addusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add News</h4>
                                                </div>
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal">
                                        <div id="Add_News_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

											 
                                            <div class="form-group">
                                                <label class="control-label col-md-3">News Title
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text"  class="form-control" placeholder="Enter Title" id="news_title" name="name"/> </div>
                                                </div>
                                            </div> 

											<div class="form-group">
                                                <label class="control-label col-md-3">Description
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
														 <textarea class="form-control" rows="2" cols="50" placeholder="Enter Description" id="news_description" name="news_description"></textarea>
                                                     
														</div>
                                                </div>
                                            </div>
											
											
											
											
											
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_news()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                      
		
		<div class="modal fade" id="editnews" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_edit_news_div">
			
			</div>
		</div>
			
										
			

					
       