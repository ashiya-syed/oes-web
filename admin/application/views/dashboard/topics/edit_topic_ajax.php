 
<div class="modal-content">
                                  
								  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Edit Topic</h4>
                                                </div>
                                                <div class="modal-body"> 
										
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal">
                                       <div id="Edit_Coupon_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> Please fill all fields. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
											<div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">Topic
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="update_topic"  id="update_topic"  value="<?php echo $details->topic_name; ?>"  required/> </div>
                                                </div>
                                            </div>
											<div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">Subject
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                     <select class="form-control " placeholder="Select Subject" name="update_subject" id="update_subject"  class="date-control"  >
												<?php if($subjects && count($subjects) > 0){  ?>
												 <option value="">Select subject</option>
														<?php foreach( $subjects as $s){
															
															?>
															<option value="<?php echo $s->id ?>" <?php  if($s->id== $details->subject_id){ echo "selected";} ?>> <?php echo $s->sub_name ?></option>
														<?php  }} ?>
													</select> </div>
                                            </div>
                                           
											
										</div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" onclick="update_topic('<?php echo $details->topic_id; ?>')">Update</button>
                                                </div>
                                            </div>									
												