  <?php $this->load->view('dashboard/includes/calender'); ?>
<div class="modal-content">
                                  
								  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Edit Coupon/Promocode</h4>
                                                </div>
                                                <div class="modal-body"> 
										
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal">
                                       <div id="Edit_Coupon_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
											<div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">Coupon Code
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="updatecoupon_code" readonly id="updatecoupon_code"  value="<?php echo $details->coupon_code; ?>"  required/> </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Coupon Amount
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" name="updatecoupon_amount" id="updatecoupon_amount" value="<?php echo $details->coupon_amount; ?>"  /> </div>
                                                </div>
                                            </div> 
											<div class="form-group">
                                                <label class="control-label col-md-3">Coupon validity
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="updatecoupon_validity" id="datepicker1" value="<?php echo date("d-m-Y", strtotime($details->coupon_validity) ); ?>"  /> </div>
                                                </div>
                                            </div>
											<!-- 31-01-2018 START -->
											<?php 
											if($details->isPromoCode == 2){ ?>
											<div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">User Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="PromocodeUserName" readonly id="PromocodeUserName"  value="<?php echo $users->userName; ?>"  required/> </div>
                                                </div>
                                            </div>
											<?php }?>
											  <!-- 31-01-2018 END -->
                                            
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" onclick="update_coupon('<?php echo $details->coupon_id; ?>')">Update</button>
                                                </div>
                                            </div>									
												