   <style>
#search_div{
    margin-top:-4%; /* Don't copy this */
	float:left;
}
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
.pac-container {
			background-color: #FFF;
			z-index: 20;
			position: fixed;
			display: inline-block;
			float: left;
		}
		.modal{
			z-index: 20;   
		}
		.modal-backdrop{
			z-index: 10;        
		}​
</style>

  <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                   
                        <?php echo sideMenu(); 
						$this->load->view('dashboard/includes/calender');?>
                        

                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    
                    <!--<h3 class="page-title"> Managed Datatables
                        <small>managed datatable samples</small>
                    </h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo ADMIN_DASHBOARD_URL ?>">DashBoard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <span>Coupons/Promocodes</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Coupons</span>
                                    </div>
                              
                              <div class="col-md-6 pull-right text-right" id="new">
								 <div class="btn-group">
											<button data-toggle="modal" href="#addusers" class="btn sbold green"> Add New Coupon/Promocode<i class="fa fa-plus"></i>
											 </button>   </div> 
									</div>
								 </div>
                                <div class="portlet-body ">
                                 <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_11">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Coupon code</th>
                                                <th>Coupon Amount(Rs)</th>
                                                <th>Coupon Valid From</th>
                                                <th>Coupon Valid To</th>
                                                <th>Validity</th>
                                                <th>Is promocode</th>
												<th> Status </th>
												<th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
							                <?php 
												if($coupons && count($coupons)>0){
													$i=1;
													 foreach($coupons as $coupon){
														  $couponValidFrom = date('Y-m-d', strtotime($coupon->coupon_createdTime));
                                                          $couponValidTo = date('Y-m-d', strtotime($coupon->coupon_validity));
			                                              $todatDate=date('Y-m-d');
														  if (($todatDate >= $couponValidFrom) && ($todatDate <= $couponValidTo))
                                                           {
															   $valid="Valid";
														   }else{
															    $valid="Expired";
														   }
												?>
                                            <tr class="odd gradeX">
                                                        <td><?php echo $i;  ?></td>
														<td><?php echo $coupon->coupon_code;  ?></td>
														<td><?php echo $coupon->coupon_amount;  ?></td>
														<td><?php echo !empty($coupon->coupon_createdTime)? date('d-m-Y',strtotime($coupon->coupon_createdTime)) :'-';  ?></td>
														<td ><?php echo !empty($coupon->coupon_validity)? date('d-m-Y',strtotime($coupon->coupon_validity)) :'-';  ?></td>
														<td ><?php echo $valid;  ?></td>
														   
												<td>
													<?php $status=$coupon->isPromoCode;
														if($status=='2'){ echo "Yes"; } 
														else { echo "No"; }
														 ?>
                                                </td>											
                                                <td>
                                                    <span id="activeUser_<?php echo $coupon->coupon_id; ?>">
													<?php $status=$coupon->isActive;
														if($status=='1'){ ?>
													   <span class="btn btn-success" style="padding:4px" <?php if($coupon->isPromoCode == 1){ ?> onclick="activateCoupon('<?php echo $coupon->coupon_id?>','0')" <?php } ?>> Active </span>
													 <?php   } else { ?>
													   <span class="btn btn-danger" style="padding:4px" <?php if($coupon->isPromoCode == 1){ ?> onclick="activateCoupon('<?php echo $coupon->coupon_id?>','1')" <?php } ?>> InActive </span>
													 <?php } ?>
													 </span>
                                                </td>
												
												<td class="text-center">
                                                  <?php // if($coupon->isPromoCode == 1){ ?>												
												<button class="btn btn-icon-only default" onclick="edit_coupon('<?php echo $coupon->coupon_id  ?>');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
												  <?php // } ?>
												<button class="btn btn-icon-only red" onclick="deleteCoupon('<?php echo $coupon->coupon_id ?>')"><i class="fa fa-trash" aria-hidden="true"></button></i></td>
                                            </tr>
											<?php $i++; } } ?> 
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
		
		<div class="modal fade" id="addusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add Coupon/Promocode</h4>
                                                </div>
                                                <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal" autocomplete="off">
                                        <div id="Add_Coupons_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                                  <div class="form-group">
												 <label class="control-label col-md-3">Select Coupon Type: 
												 <span class="required"> * </span>
												 </label>
												 <div class="col-md-6">
													<select id="select" class="form-control " name="couponType" id="couponType" onchange="showDiv()" >
																<option value="1"> Coupon</option>
																<option value="2"> Promo code</option>
													</select>
												</div>
												</div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Coupon Amount
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" class="form-control" placeholder="Enter Amount" id="coupon_amount" name="coupon_amount"/> </div>
                                                </div>
                                            </div>
											<div class="form-group" id="Quan">
                                                <label class="control-label col-md-3">Coupon Quantity
                                                    
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" placeholder="Enter Quantity" id="coupon_quantity" name="coupon_quantity"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Coupon Validity
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                       <input type="text" class="form-control" placeholder="dd-mm-yyyy" name="startdate" id="datepicker" class="date-control"  value="<?php if(isset($_POST['startdate'])) echo $_POST['startdate']?>">
													   </div>
                                                </div>
                                            </div>
											<div class="form-group" id='AllUsers'>
										   <label class="control-label col-md-3">Select Users:
										    <span class="required"> * </span>
										   </label>
										   <div class="col-md-6">
											<select id="users"  class="form-control login_field select2" name="users">
												<option value="">Select Users</option>
												<?php 
													$selected=array();
													if($users && count($users) > 0){ 
													foreach( $users as $key=>$val ){
														$selected[$key] = $val->userId;
														 }
														 }
													if($users && count($users)>0){
														foreach($users as $key=>$val){
														$select="";
													 ?>
												   <option value="<?php echo $val->userId ?>" <?php echo $select; ?>><?php echo $val->userName?></option>
													<?php } }  ?>
													
											</select>
										  </div>
										  </div>
											
											
                                        </div>
                                      
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_coupon()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
									<div class="modal fade" id="addDummyusers" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog"     style="margin-top:80px;">
                                            <div class="modal-content">
                                               <div class="modal-body"> 
												
												
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="form_sample_2" class="form-horizontal" autocomplete="off">
                                        <div id="Add_dUMMY_User_Message"></div>
										<div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

											 
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Name" id="dummy_user_name" name="name" /> </div>
                                                </div>
                                            </div> 

											<div class="form-group">
                                                <label class="control-label col-md-3">Email Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" placeholder="Enter Email" id="dummy_email_adress" name="email"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Phone Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" maxlength="10" placeholder="Enter Phone" id="dummy_phone_number" name="number"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Password" id="dummy_password" name="password"/> </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">Confirm Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="password" class="form-control" placeholder="Enter Confirm Password" id="dummy_confirm_password" name="cpassword"/> </div>
                                                </div>
                                            </div>
											
											</div>
                                    </form>
                                    <!-- END FORM-->
                                
												</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green" id="addUsers" onclick="add_dummy_users()">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                      
		
		<div class="modal fade" id="edituser" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="margin-top:80px;" id="_edit_user_div">
			
			</div>
		</div>
	<script>
	$(document).ready(function() {
				$('#AllUsers').hide();
			});

function showDiv()
{
	var select = $('#select').val();
	if(select == '1')
	{
		$('#AllUsers').hide();
		$('#Quan').show();
		$('#Amt').show();
	}
	if(select == '2')
	{
		$('#AllUsers').show();
		$('#Quan').hide();
		$('#Amt').show();
	}
}


	</script>
										
			

					
       