<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
<?php @$reseller=$this->uri->segment(1); ?>
    <head>
        <meta charset="utf-8" />
        <title>Onyx Educationals</title>
		<link rel="shortcut icon" href="<?php echo SITEURL ?>/assets/logo-default.png" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo SITEURL ?>/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo SITEURL ?>/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo SITEURL ?>/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo SITEURL ?>/assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <div class="menu-toggler sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo">
       <img src="<?php echo SITEURL ?>/assets/logo-default.png" alt="logo" class="logo-default"  /> 
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
			<?php if(@$reseller){ ?>
            <form class="login-form" action="" method="post">
                <h3 class="form-title font-green">Sign In</h3>
                <?php if (isset($successMessage)) { echo '<span style="text-align:center;padding:15px 0px;color:green;">'.$successMessage. '</span>';} ?>
                <?php if (isset($errorMessage)) { echo '<div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <span style="text-align:center;padding:15px 0px;color:red;">'.$errorMessage. '</span></div>';} ?>
				<div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter your username and password. </span>
                </div>
				
				
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
					  <?php $reseller_Name = $this->input->cookie('reseller_Name'); ?>
                    <input class="form-control form-control-solid placeholder-no-fix" required type="text" autocomplete="off" placeholder="Username/Email" name="Email_OR_phone" value="<?php if(!empty($reseller_Name)){
				  echo $this->input->cookie('reseller_Name',true);
				 }
			  ?>" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
					 <?php  $reseller_password = $this->input->cookie('reseller_password'); ?>
                    <input class="form-control form-control-solid placeholder-no-fix" required type="password" autocomplete="off" placeholder="Password" name="password" value="<?php if(!empty($reseller_password)){
				  echo $this->input->cookie('reseller_password',true);
				 }
			  ?>" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase" name="Admin_LoginBtn">Login</button>
                    <label class="rememberme check">
                        <input type="checkbox" id="inlineCheckbox3"  name="remember" value="1" onclick="$(this).attr('value', this.checked ? 1 : 0)" <?php if($this->input->cookie('reseller_remember')==1) echo 'checked'; ?> value="<?php echo $this->input->cookie('reseller_remember')?>"/>Remember </label>
                  <!--  <a href="javascript:;" id="forget-password" class="forget-password" >Forgot Password?</a>--->
                </div>
               
            </form>
			<?php }else{ ?>
            <form class="login-form" action="" method="post">
                <h3 class="form-title font-green">Sign In</h3>
                <?php if (isset($successMessage)) { echo '<span style="text-align:center;padding:15px 0px;color:green;">'.$successMessage. '</span>';} ?>
                 <?php if (isset($errorMessage)) { echo '<div class="alert alert-danger alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <span style="text-align:center;padding:15px 0px;color:red;">'.$errorMessage. '</span></div>';} ?>
				<div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter your username and password. </span>
                </div>
				
				
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
					  <?php $cookie_user_name = $this->input->cookie('Superadmin_Name'); ?>
                    <input class="form-control form-control-solid placeholder-no-fix" required type="text" autocomplete="off" placeholder="Username/Email" name="Email_OR_phone" value="<?php if(!empty($cookie_user_name)){
				  echo $this->input->cookie('Superadmin_Name',true);
				 }
			  ?>" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
					 <?php  $cookie_password = $this->input->cookie('Superadmin_password'); ?>
                    <input class="form-control form-control-solid placeholder-no-fix" required type="password" autocomplete="off" placeholder="Password" name="password" value="<?php if(!empty($cookie_password)){
				  echo $this->input->cookie('Superadmin_password',true);
				 }
			  ?>" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase" name="Admin_LoginBtn">Login</button>
                    <label class="rememberme check">
                        <input type="checkbox" id="inlineCheckbox3"  name="remember" value="1" onclick="$(this).attr('value', this.checked ? 1 : 0)" <?php if($this->input->cookie('Superadmin_remember')==1) echo 'checked'; ?> value="<?php echo $this->input->cookie('Superadmin_remember')?>"/>Remember </label>
                  <!--  <a href="javascript:;" id="forget-password" class="forget-password" >Forgot Password?</a>--->
                </div>
               
            </form>
			<?php } ?>
            <!-- END LOGIN FORM -->
            
        </div>
        <div class="copyright"> <?php echo date("Y"); ?> @ Onyx Educationals. Admin Dashboard. </div>
        <!--[if lt IE 9]>
<script src="<?php echo SITEURL ?>/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo SITEURL ?>/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
		<script>
		$('input').attr('autocomplete', 'off');
		   $("input").prop('required',true);
		</script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo SITEURL ?>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?php echo SITEURL ?>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo SITEURL ?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo SITEURL ?>/assets/pages/scripts/login.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

    </body>

</html>
