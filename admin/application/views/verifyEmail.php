<div style="text-align:left;font-size:130%; font-weight:bold; padding:1%;font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">Hi, <?php echo @$name ?></div>
<p style="text-align:left;font-size:110%; font-weight:bold; margin:10px 15px;font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">We created your account in Onyx Educationals.</p>
<p style="text-align:left;font-size:110%; font-weight:bold; margin:10px 15px;font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">Your Details are</p>
<div style="width:98%; padding:1%;height:auto;font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;background:#FFF;"> 
  <div style="font-size:100%;color: #333; border:2px solid #eaeaea; padding:10px 20px;">
    	<table width="100%" border="0">
          <tr>
            <td width="80px"><img src="<?php echo SITEURL ?>/assets/logo-default.png" width="60" height="60"></td>
            <td style="font-size:180%; font-weight:bold;">ONYX Educationals</td>
          </tr>
        </table>

  </div>
  <div style="font-size:100%;color: #333; border:2px solid #eaeaea; border-top:0px;">
    	<table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr style="background-color:#f3f3f3; font-size:120%; font-weight:bold;">
            <td width="25%" align="center" style="padding:15px 20px; border-bottom:1px solid #cccccc;">Name</td>
            <td width="25%" align="center" style="padding:15px 20px; border:1px solid #cccccc; border-top:0px;">Mobile</td>
            <td width="25%" align="center" style="padding:15px 20px; border:1px solid #cccccc;border-top:0px;">Email</td>
            <td width="25%" align="center" style="padding:15px 20px; border-bottom:1px solid #cccccc;border-top:0px;">Password</td>
          </tr>
          <tr>
            <td width="25%" align="center" style="font-weight:bold; font-size:120%;padding:15px 20px;"><?php echo @$name ?></td>
            <td width="25%" align="center" style="font-weight:bold; font-size:120%; border-left:1px solid #cccccc;border-right:1px solid #cccccc;"><?php echo @$phoneNumber ?></td>
            <td width="25%" align="center" style="font-weight:bold; font-size:120%; border-left:1px solid #cccccc;border-right:1px solid #cccccc;"><?php echo @$email ?></td>
            <td width="25%" align="center" style="font-weight:bold; font-size:120%;"><?php echo @$password ?></td>
          </tr>
        </table>
  </div>  
  
  <?php if(@$isReseller == 1){ ?>
  <div style="font-size:100%;color: #333; border:2px solid #eaeaea; padding:10px 20px;">
    	<table width="100%" border="0">
          <tr>
           <td width="33.33%" align="center" style="font-weight:bold; font-size:120%;padding:15px 20px;">Users : <?php echo @$users ?></td>
            <td width="33.33%" align="center" style="font-weight:bold; font-size:120%;">Tests : <?php echo @$tests ?></td>
            <td width="33.33%" align="center" style="font-weight:bold; font-size:120%;">Packages : <?php echo @$packages ?></td>
          </tr>
		  
        </table>

  </div>
  <?php } ?>
  <div style="font-size:100%;color: #333; border:2px solid #eaeaea; padding:10px 20px;">
    	<table width="100%" border="0">
          <?php if(@$accountType){ ?>
		  <tr>
           Click here <a href="<?php echo @$url;?>">Link</a> <?php  echo "to verify your account.";?></td>
          </tr> 
		  <?php } ?>
		  <?php if(@$url2){ ?>
		  <tr>
           Login url : <?php echo @$url2;?></td>
          </tr>
		  <?php } ?>
        </table>

  </div>
  
</div>