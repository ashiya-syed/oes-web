<?php 
if (! function_exists ( 'debug' )) 
{
	function debug($variable) 
	{
		echo "<pre>";
		print_r ( $variable );
		echo "</pre>";
		die ();
	}
}
if(!function_exists('generateRandNumber')){
	
	function generateRandNumber($length=0){
		$characters = '0123456789';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return strtolower($randomString);
	}
}
function sendOTP($jsondata, $phone ="")
{
    //$api_url ='http://map-alerts.smsalerts.biz/api/v3/?method=sms.json&api_key=Ac0553dac69370e10187eab73f66201c8&json='.urlencode($jsondata);
    //$response = file_get_contents($api_url);
    //return  $response;
    if($phone){
      $str='<?xml version="1.0" encoding="UTF-8"?> 
        <xmlapi> 
           <auth> 
              <username>onyx</username> 
              <password>123456ss</password> 
           </auth> 
           <sendSMS> 
              <to>'.$phone.'</to> 
              <text>'.$jsondata.'</text> 
           </sendSMS> 
           <options> 
              <flash>Y</flash> 
              <senderid>ONYXED</senderid> 
           </options> 
        </xmlapi>'; 

		$url = "http://onlinebulksmslogin.com/spanelv2/xml/api.php?data=".urlencode($str); 

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 4); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close')); 

		$start = array_sum(explode(' ', microtime())); 
		$result = curl_exec($ch); 
		return $result;
    }
    return false;
}

if(!function_exists('sendSmtpEmail'))
{

	function sendSmtpEmail($from,$to,$subject,$messages)
	{
		try {
				
		    $CI =&get_instance();
			$CI->load->library('email', config_item('smtpEmailSettings'));
            $CI->email->set_newline("\r\n");
            $CI->email->from($from, 'OnyxEducationals');
			$list = $to;
			$CI->email->to($list);
			$CI->email->subject($subject);
			$CI->email->message($messages);
			if ($CI->email->send())
			{
				return true;
			}
			else
			{
				//print_r($CI->email->print_debugger());die();
			}
		}
		catch (Exception $e)
		{
			//print_r('Caught exception: ',  $e->getMessage(), "\n");die();
		}
			
	}
	
}
if(!function_exists('timeElapsed'))
{
	function timeElapsed($datetime,$end ,$full = false) {
		//$now = new DateTime;
		$ago = new DateTime($datetime);
		$end = new DateTime($end);		
        $diff = $end->diff($ago);
		
		$diff->w = floor($diff->d / 7); 

		$diff->d -= $diff->w * 7;

		$string = array(
				'y' => 'year',
				'm' => 'month',
				'w' => 'week',
				'd' => 'day',
				'h' => 'hour',
				'i' => 'minute',
				's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}
       if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ' : '';
	}
}
if(!function_exists('secondsToTime'))
{
	function secondsToTime($seconds) {
      $hours = floor($seconds / (60 * 60));
      // extract minutes
	  $divisor_for_minutes = $seconds % (60 * 60);
	  $minutes = floor($divisor_for_minutes / 60);
	  // extract the remaining seconds
	  $divisor_for_seconds = $divisor_for_minutes % 60;
	  $seconds = ceil($divisor_for_seconds);
	 // return the final array
	  $obj = array(
			"h" => (int) $hours,
			"m" => (int) $minutes,
			"s" => (int) $seconds,
		);
	return $obj;
	}
	function encrypt($string){
	 $key = 'password to (en/de)crypt';
     $iv = mcrypt_create_iv(
        mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
        MCRYPT_DEV_URANDOM
    );
    
    $encrypted = base64_encode(
        $iv .
        mcrypt_encrypt(
            MCRYPT_RIJNDAEL_128,
            hash('sha256', $key, true),
            $string,
            MCRYPT_MODE_CBC,
            $iv
        )
    );
	
    return $encrypted;
}
function decrypt($encrypted){
	
	$key = 'password to (en/de)crypt';
	$data = base64_decode($encrypted);
    $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

    $decrypted = rtrim(
        mcrypt_decrypt(
            MCRYPT_RIJNDAEL_128,
            hash('sha256', $key, true),
            substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
            MCRYPT_MODE_CBC,
            $iv
        ),
        "\0"
    );
	 return $decrypted;
}
}
?>