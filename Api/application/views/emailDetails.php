<div style="text-align:left;font-size:130%; font-weight:bold; padding:1%;font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">Hi,<?php if(@$userName){echo @$userName;} ?></div>
<p style="text-align:left;font-size:130%; font-weight:bold; margin:10px 15px;font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">Amount:  Rs.<?php echo @$amount;?></p>
<p style="text-align:left;font-size:110%; font-weight:bold; margin:10px 15px;font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">Tests/Packages You Selected</p>
<div style="width:98%; padding:1%;height:auto;font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;background:#FFF;"> 
  <div style="font-size:100%;color: #333; border:2px solid #eaeaea; padding:10px 20px;">
    	<table width="100%" border="0">
          <tr>
            <td width="80px"><img src="<?php echo IMAGES_URL; ?>logo.png" width="60" height="60"></td>
            <td style="font-size:180%; font-weight:bold;">ONYX Educationals</td>
          </tr>
        </table>

  </div>
  <?php if(@$testDetails && count(@$testDetails)>0){?>
  <div style="font-size:100%;color: #333; border:2px solid #eaeaea; border-top:0px;">
    	<table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr style="background-color:#f3f3f3; font-size:120%; font-weight:bold;">
            <td width="33.33%" align="center" style="padding:15px 20px; border-bottom:1px solid #cccccc;">Test ID</td>
            <td width="33.33%" align="center" style="padding:15px 20px; border:1px solid #cccccc; border-top:0px;">Test Name</td>
            <td width="33.33%" align="center" style="padding:15px 20px; border-bottom:1px solid #cccccc;">Amount(Rs)</td>
          </tr>
		  <?php foreach($testDetails as $test){ ?>
          <tr>
            <td width="33.33%" align="center" style="padding:15px 20px;"><?php echo @$test->testId?></td>
            <td width="33.33%" align="center" style="font-weight:bold; font-size:120%; border-left:1px solid #cccccc;border-right:1px solid #cccccc;"><?php echo @$test->testName?></td>
            <td width="33.33%" align="center" style="font-weight:bold; font-size:120%;"><?php echo @$test->testAmount?></td>
          </tr>
		  <?php }?>
        </table>
  </div>  
  <?php } ?>
  <?php if(@$packageDetails && count(@$packageDetails)>0){?>
  <div style="font-size:100%;color: #333; border:2px solid #eaeaea; border-top:0px;">
    	<table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr style="background-color:#f3f3f3; font-size:120%; font-weight:bold;">
            <td width="33.33%" align="center" style="padding:15px 20px; border-bottom:1px solid #cccccc;">Package ID</td>
            <td width="33.33%" align="center" style="padding:15px 20px; border:1px solid #cccccc; border-top:0px;">Package Name</td>
            <td width="33.33%" align="center" style="padding:15px 20px; border-bottom:1px solid #cccccc;">Amount(Rs)</td>
          </tr>
		   <?php foreach($packageDetails as $package){ ?>
          <tr>
            <td width="33.33%" align="center" style="padding:15px 20px;"><?php echo @$package->package_id?></td>
            <td width="33.33%" align="center" style="font-weight:bold; font-size:120%; border-left:1px solid #cccccc;border-right:1px solid #cccccc;"><?php echo @$package->package_name?></td>
            <td width="33.33%" align="center" style="font-weight:bold; font-size:120%;"><?php echo @$package->package_amount?></td>
          </tr>
		  <?php } ?> 
        </table>
  </div>
  <?php } ?>
</div>