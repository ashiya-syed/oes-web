<!DOCTYPE html>
<html lang="en">
<body>
<div id="wrapper">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<h2 align='center'class="page-heading bottom-indent" style="margin-bottom: 0px;">Refund and Cancellation Policy</h2>
<div class="row">
<div class="col-xs-12 col-sm-12">
<p>Amount once paid through the payment gateway shall not be refunded other than in the following circumstances: </p>
<ul><li>Multiple times debiting of Customer’s Card or Bank Account due to technical error OR Customer's account being debited with excess amount in a single transaction due to technical error.
 In such cases, excess amount excluding payment gateway charges would be refunded to the Customer.</li>
<li>Due to technical error, payment being charged on the Customer’s Card or Bank Account but the enrolment for the examination is unsuccessful. Customer would be provided with the enrolment by meeexam.com at no extra cost. However, if in such cases, customer wishes to seek refund of the amount, he or she would be refunded net the amount, after deduction of payment gateway charges or any other charges. </li>
</ul>
<p>The customer will have to make an application for refund along with the transaction number and original payment receipt if any generated at the time of making payments.</p>
</div>
</div>
</div>
</div>  
</div>
</body>
</html>