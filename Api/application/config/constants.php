<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define("APP_FOLDER","Api");
define("FRONT_SITE_FOLDER","");
define("ROOT",$_SERVER['DOCUMENT_ROOT']."/".FRONT_SITE_FOLDER);
define("SITEURL2","http://".$_SERVER['SERVER_NAME']."/".APP_FOLDER."/index.php");
define("SITEURL","http://".$_SERVER['SERVER_NAME']."/".APP_FOLDER);
define("FSITEURL","http://".$_SERVER['SERVER_NAME']."/".FRONT_SITE_FOLDER);



define("LOGOUT_URL",SITEURL.'account/logout');
define("DASHBOARD_URL",SITEURL.'dashboard');
define("ACCOUNT_URL",SITEURL.'account');
define("REGISTRATION_URL",SITEURL.'account/registration');
define("VERIFICATION_URL",SITEURL.'account/verifyAccount');
define("PROFILE_URL",SITEURL.'account/profile');
define("FORGOTPASSWORD",SITEURL.'account/forgotpassword');
define("RECOVERPASSWORD",SITEURL.'account/resetpassword');
define("CHANGEPASSWORD",SITEURL.'account/changepassword');
define("START_EXAM_URL",SITEURL.'dashboard/startExam');
define("EXAM_URL",SITEURL.'dashboard/questionPaper');
define("SUBMIT_EXAM_URL",SITEURL.'dashboard/submitTest');
define("SUBMIT_ON_BROWSER_CLOSE_URL",SITEURL.'dashboard/browserCloseSubmit');
define("OTP_URL",SITEURL.'dashboard/verifyOtp');
define("VIEW_SYLLABUS_URL",SITEURL.'dashboard/viewSyllabus');
define("BUY_PACKAGES_URL",SITEURL.'dashboard/buypackage');
define("SINGLE_PAGE_URL",SITEURL.'dashboard/newsDetails');
define("FAILURE_URL",SITEURL.'dashboard/failure');
define("NOT_FOUND_URL",SITEURL.'dashboard/notFound');
define("CART_URL",SITEURL.'dashboard/cart');
define("BUY_PACKAGE_OR_TEST_URL",SITEURL.'dashboard/purchasePackageOrTest');

define("REDIRECT_URL",SITEURL.'account/redirect');
//define("USER_DASHBOARD_URL",SITEURL2.'/Account/d');
define("UPLOADS_OPTIONS_PATH",SITEURL.'admin/uploads/options/');
define('QUESTION_SUMMARY_URL',SITEURL.'dashboard/summeryReport');//new
define('VIEWMORE',SITEURL.'dashboard/viewMoreRecords');//new

//on 14 the
define('TBL_TEST_QUESTIONS','oes_test_questions');//qb


define("ADMINURL","http://".$_SERVER['SERVER_NAME']."/ONYX-EXAMS/".'admin');

define("LIBRARYP_PATH",ADMINURL.'/uploads/libraries/');//on 14 th



define("SAVE_OPTIONS_URL",SITEURL.'dashboard/saveOptions');
define("IMAGE_PATH",SITEURL.'uploads/');


define('ASSETS_URL', SITEURL."assets/");
define('CSS_URL', ASSETS_URL."css/");
define('JS_URL', ASSETS_URL."js/");
define('IMAGES_URL', ASSETS_URL."images/");
define('FONT_URL', ASSETS_URL."fonts/");

//on november
define("PROMOTOR_LOGOUT_URL",SITEURL.'Promotion/logout');
define("PROMOTION_MEMBER_LOGIN_URL",SITEURL.'promotion/');
define("PROMOTOR_EXAM_URL",SITEURL.'Promotion/promotion_questionPaper');
define("PROMOTER_DASHBOARD_URL",SITEURL.'Promotion/dashborad');
define("PROMOTION_OTP_URL",SITEURL.'dashboard/promotionVerify');
/* CC AVENUE */
define("CCAVENUE_URL",SITEURL.'ccavenue/orderPaymentRequestHandler.php');
define("CCAVENUE_REQUEST_URL",SITEURL.'dashboard/ccavenueRequestHandler');
define("CCAVENUE_RESPONSE_URL",SITEURL.'dashboard/callbackThroughCCAvenue');
define("CCAVENUE_SUCCESS_URL",SITEURL.'dashboard/saveDataAfterCCavenueSuccess');
define("CCAVENUE_MERCHANTID","155713");
define("CCAVENUE_WORKING_KEY","F0E3BDEAB0823BA212521CBE8E203784");
define("CCAVENUE_ACCESS_CODE","AVLN75EL18CC38NLCC");
define("CCAVENUE_AVENUE_ERROR_OCCURED","Error occured while placing your order.");
define("INVALID_ORDER_ID","Invalid order id.");

/* END */
define('TBL_PROMOTER_PACKAGES','oes_promoter_packages');
define('TBL_PROMOTER_TESTS','oes_promoter_tests');

define('TBL_QUE_OPTIONS','que_options');
define('TBL_CITIES','oes_cities');
define('TBL_STATES','oes_states');
define('TBL_EXAM_RESULTS','oes_exam_results');
define('TBL_TIMES','oes_time');

define('TBL_COURSES','oes_courses');

define('TBL_ENROLLMENTDETAILS','oes_enrollmentDetails');
define('TBL_QUESTIONS_ORDER','oes_questions_reference');
define('TBL_USER_COUPONS_DETAILS','oes_user_coupon_details ');
define('TBL_COUPONS','oes_coupons ');
define('TBL_OPTIONS_REFERENCE','oes_options_reference');
define('BUY_PACK_PAYU',SITEURL.'dashboard/buyPackThroughPayU');
define("LIBRARIES_URL",SITEURL.'dashboard/libraries');
define("GET_LIB_DETAILS",SITEURL.'dashboard/getLibDetails');




define("EMAIL_OR_PHONE_AND_PASSWORD_NOT_MATCH","Failed to authenticate email or phone number,passwords are invalid.");
define("PHONE_NUMBER_OR_EMAIL_OR_NAME_ALREADY_EXISTS","The phone number or email already exists.");

define("SUCCESSFULLY_UPDATED","Successfully updated.");
define("NOT_UPDATED","Not Updated.");
define("SUCCESSFULLY_DELETED","Successfully Deleted.");
define('USER_CREATION_FAILED','Registration failed.');
define('USER_CREATION_SUCCESS','Registration success.');
define("UPDATE_SUCCESS","Your Profile updated successfully.");
define("NO_PACKAGES_TESTS","No examination test available in your account.");
define("NO_PRACTISE_TESTS_EXISTS","Practice tests are not available.");
define("NO_PACKAGES_AVAILABLE","Packages are not available.");
define("NO_MAIN_TESTS_EXISTS","Tests are not available.");
define("ADMIN_EXAM_URL",SITEURL.'dashboard/admin_questionPaper');
define("ADMIN_OTP_URL",SITEURL.'dashboard/adminVerify');
define('QUETION_MANAGEMENT_URL',ADMINURL.'/authority/d/maintest/quetions');    
define('ADMIN_QUESTIONPAPER_URL',SITEURL.'dashboard/admin_questionPaper');    
define('ADMIN_FIFTY_FIFTY_QUE',SITEURL.'dashboard/getadminFiftyFiftyOptions');    
define('CONTACT_US',SITEURL.'Content/contactUs');    




define('PASSWORD_UPDATE_SUCCESS','Password successfully updated.');                                                                                              
define('UPDATE_FAILED','Updating failed.');
define('UNABLE_TO_ATTEMPT_EXAM','Your unable to attempt test because already you completed this test 3 times.');
define('CART_ITEMS_NOT_FOUND','Your cart is empty.');
define('TESTS_NOT_FOUND','This package does not contain any tests.');
define('NEWS_UPDATES_NOT_FOUND','News and updates are not found.');

//newwwwww
define('MAIN_TEST','1');
define('PRACTICAL_TEST','2');
define('DEFAULT_LIMIT','10');

//user roles
define('USER_ROLE_ID','2');
define('SUPERADMIN_ROLE_ID','1');

define('TBL_PACKAGES','oes_packages');
define('TBL_TESTS','oes_tests');
define('TBL_NEWS','oes_news');
define('TBL_CART','oes_cart');
define('TBL_QUETIONS','quetions');
define('TBL_USERS','users');
define('TBL_USER_PACKAGE','oes_user_packages');
define('TBL_OTPS','oes_otp');
define('TBL_RESELLERS','oes_resellers');
define('TBL_CC_CALLBACKS','oes_ccavenue_callbacks');
define('TBL_USER_COUPONS','oes_user_coupons');
define('TBL_QUE_IMAGES','oes_questions_images');

define("PROFILE_IMAGE_PATH",SITEURL."uploads/");
define("UPLOADS_PATH",ROOT."/uploads/");
define("HOME_IMAGE_PATH",FSITEURL."assets/images/");


define('PROVIDE_VALID_PACKAGE_ID','Please provide valid package id.');                                                                                              
define('CART_ITEM_ADDING_SUCCESS','Selected item successfully added to your cart.');                                                                                              
define('CART_ITEM_ADDING_FAILED','Error occured while adding to cart.');                                                                                              
define('CART_ITEM_ALREADY_EXISTS','Already this item exists in your cart.');                                                                                              
define('CART_ITEM_DELETED_SUCCESSFULLY','Cart item deleted successfully.');                                                                                              
define("SUCCESS","Success.");
define("ERROR","Error Occured.");
define("USER_NOT_EXISTS","User not exists with this email.");
define("SENDING_MAIL_FAILED","Sending mail failed due to some technical problems.");
define("FORGOT_PWD_MAIL_SENT","Please check your register email to reset your password.");
define("VERIFY_ACCOUNT_LOGIN","Verify your account to login.");
define("CONTACT_ADMIN","Your account is deactivated.Please contact admin for deatils.");
define('TBL_LIBRARIES','oes_libraries');
define('TBL_USER_TRACK','oes_usertrack');
define('RECOVERY_EMAILS','oes_recoveryemails');
define("RECORDS_EXISTS","Records exists");
define("NO_RECORDS_EXISTS","No records exists");
define("LOGIN_SUCCESS","login success");
define("ACCOUNT_EXPIRED","Your account is expired.");
define("SELECT_ATLEAST_ONE_PACK_OR_TEST","Please select at least one package or test.");
define("INVALID_OTP","Invalid Otp.");
define("VERIFICATION_MESSAGE","Please verify your email id to login into your account.");
define("PHONE_NUMBER_OR_EMAIL_ALREADY_EXISTS","This phone number or email already exists.");
define("CHECK_YOUR_MOBILE_FOR_OTP","Check your mobile for otp.");
define("SENDING_OTP_FAILED","Sending otp failed.");
define("USER_REGISTRATION_SUCCESS",'Registration success');
define("USER_REGISTRATION_FAILED",'Registration failed');
define("PHONE_NUMBER_ALREADY_EXISTS","This phone number already exists.");
define("USER_EMAIL_EXISTS","User already exists with this email.");
define("ACCOUNT_VERIFY_SUCCESS","Thanks,Your email has been verified.Login to your account.");
define("ACCOUNT_VERIFY_FAILED","Account verification failed");
define("COUPON_AMOUNT_VALIDITY","Your Coupon amount is not equal to purchase amount.");
define("VALID_COUPON","Valid coupon.");
define("COUPON_EXPIRED","This coupon is expired.");
define("INVALID_COUPON","Please enter valid coupon.");
define("INVALID_COUPON_WITH_VALID_AMOUNT","Please enter valid coupon of valid amount.");
define("PURCHASE_DONE","Your Purchase is done successfully.Check your email for details.");
define("CART_EMPTY","Your cart is empty.");
define("TIME_UPDATED","Time updated successfully.");
define("ERROR_OCCURED","Error occured.");

define("REGISTRATION_SUCCESS","Registration success");
define("VIEW_RECORD_URL",SITEURL.'dashboard/viewRecord');
define("TEST_ATTEMPTS_COMPLETED",'Your test attempts completed.');


//test status
define('TEST_COMPLETED','3');
define('TEST_PENDING','2');
define('TEST_INCOMPLETE','1');

//items
define('PACKAGES','1');
define('TESTS','2');


/* End of file constants.php */
/* Location: ./application/config/constants.php */