<?php

Class Homemodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_USERS;
    protected $_order_by = 'userId desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
    
	 
    function getUserCount()
    {
		 $this->db->select('*'); 
		 $this->db->from(TBL_USERS);
		 $this->db->where('isDeleted',0);
		 $this->db->where('roleID',USER_ROLE_ID);
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->num_rows():0;
    }   
	
	 function getTestsCompletedCount()
    {
		 $this->db->select('*'); 
		 $this->db->from(TBL_EXAM_RESULTS);
		$this->db->where('attempts',3);
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->num_rows():0;
    }  

    function getTestsAttemptedCount()
    {
		 $this->db->select('*'); 
		 $this->db->from(TBL_EXAM_RESULTS);
		 $this->db->group_by('testid');
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->num_rows():0;
    }  	
	
	 function getQueAttemptedCount()
    {
		 $this->db->select('*'); 
		 $this->db->from(TBL_EXAM_RESULTS);
		 $this->db->group_by('testid');
		 $this->db->group_by('que_id');
		 
		 $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->num_rows():0;
    }  
	
	
}








