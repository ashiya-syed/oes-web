<?php

Class Testsmodel extends Healthmodel
{

    var $CI;

    protected $_table_name = TBL_TESTS;
    protected $_order_by = 'testId desc';
    protected $_timestamps = TRUE;
    
    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
	
	/* function getPurchasedTests($resellerId="",$course,$start=0,$offset)
	{
		
	 	 /* $this->db->select('testId,testType,testName,testAmount,attempts,course_id'); 
		 $this->db->from(TBL_TESTS);
		 $this->db->where_in('course_id',$course);
		 $this->db->where('isDeleted',0);
		 $this->db->where('isActive',1);
		 $this->db->where('isFree',0); 
		 if($resellerId){
			 $this->db->where('resellerId',$resellerId);
		 }else{
			 $this->db->where('resellerId',0);
		 }
		 $this->db->limit($offset,$start);  */
		 /* if(empty($resellerId)){ $resellerId = 0;}
			 $sql="SELECT `testId`,`testName`,`testType`,`attempts` ,`course_id`,`testAmount` FROM ".TBL_TESTS." WHERE `course_id` IN (".$course.")  AND isDeleted = 0 AND isFree = 0 AND isActive = 1 AND  `resellerId` = ".$resellerId." ";
			 if($start){
				$sql.= " LIMIT ".$start.",".$offset." ";
			 }else{
				 $sql.="LIMIT ".$offset." ";
			 }
		 $query=$this->db->query($sql);
		 return ($query->num_rows() >0)?$query->result():array();
	} */
	
	function getPurchasedTests($resellerId="",$course,$start=0,$offset){
		 $isPromoterUser=$this->promoterDetails($resellerId); 
		 if(empty($resellerId)){ $resellerId = 0;}
			 $sql="SELECT `testId`,`testName`,`testType`,`attempts` ,`course_id`,`testAmount` FROM oes_tests WHERE `course_id` IN (".$course.")  AND isDeleted = 0 AND isFree = 0 AND isActive = 1 ";
			 /* if($start){
				$sql.= ",".$start."";
			 } */
			 if($resellerId && $isPromoterUser){ //16-18
			    $tests=$isPromoterUser['tests'];
			    $sql.=" AND `testId` IN (".$tests.") ";
			 }else if(empty($isPromoterUser) && $resellerId){
				 $sql.="AND  `resellerId` = ".$resellerId."";
			 }else{
				 $sql.="AND  `resellerId` = 0";
			 } 
			 if($start){
				$sql.= " LIMIT ".$start.",".$offset." ";
			 }else{
				 $sql.=" LIMIT ".$offset." ";
			 }
			 $query=$this->db->query($sql); 
			 if($query){
				 $result=$query->result();
				 if($result){
					 foreach($result as $res){
						 if($isPromoterUser){
						  $amountsql="select p_t_sell_amount from oes_promoter_tests where p_t_promoter_id = ".$resellerId." and p_test_id = ".$res->testId."";
					      $amount=$this->db->query($amountsql)->row();
						  $res->testAmount=$amount->p_t_sell_amount;
						 }
						  $t[]=$res;
						 
					 }
					 return $t;
				 }
			 }else{
				 return array();
			 }
	}
	function getUserLibraries($table,$where,$start=0,$offset){
		$libraries=$libArray=array();
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$this->db->group_by('packageId');
		$this->db->order_by('id','DESC');
		//$this->db->limit($offset,$start); 
        $query = $this->db->get();
		$records = $query->result();
		 if($records){
			foreach($records as $rec){
				$packageid=$rec->packageId;
				$validSql="SELECT DATEDIFF(package_valid_to,package_valid_from) as diff FROM oes_packages WHERE package_id = $packageid";
				$validityCheck=$this->db->query($validSql)->row();
				if(@$validityCheck->diff){
				           $validityCheck=$validityCheck->diff;
					       $createdTime=$rec->createdTime;
						   $expiryDate = date('Y-m-d h:i:s', strtotime("+$validityCheck days", strtotime($createdTime)));
						   $A=strtotime($expiryDate);$B=strtotime($createdTime);
					if($A >= $B){ 
					 $libraries=$this->getLibraries($packageid);
					 if($libraries){
						 foreach($libraries as $lib){
							  $libArray[]=$lib;
						 }
						
					 }
					}
				}
			}
	     }  
		 return $libArray;
	 }
	 function getLibraries($id){
        /*  if($this->session->userdata('reseller_id')){
			$resellerId=$this->session->userdata('reseller_id');
		    }else{
			$resellerId=0;
		    }
		$query='SELECT * FROM `oes_libraries` WHERE 
`packageId` like "%,'.$id.',%" OR `packageId` like "'.$id.',%" OR  `packageId`  like "%,'.$id.'" OR `packageId`  like "%'.$id.'%" and isActive=1 and isDeleted=0 and resellerId='.$resellerId.'';
	 */
	 $query='SELECT * FROM `oes_libraries` WHERE 
`packageId` like "%,'.$id.',%" OR `packageId` like "'.$id.',%" OR  `packageId`  like "%,'.$id.'" OR `packageId`  like "%'.$id.'%" and isActive=1 and isDeleted=0';
	
	 $results=$this->db->query($query)->result();
	    if($results){
			return $results;
			
		}else{
			return "";
		}
	} 
	 function getTotalCartItemsCount($userId){
	             $this->db->select_sum('quantity'); 
		         $this->db->from(TBL_CART);
		         $this->db->where('isActive',1);
		         $this->db->where('userId',$userId);
				 $query = $this->db->get();
				 $result=$query->row();
				 if($result){
					return $result->quantity;
				 }else{return 0;}
     }
   function getTotalCartItemsAmount($userId){
	             $this->db->select_sum('amount'); 
		         $this->db->from(TBL_CART);
		         $this->db->where('isActive',1);
		         $this->db->where('userId',$userId);
				 $query = $this->db->get();
				 $result=$query->row();
				 if($result){
					return $result->amount;
				 }else{return 0;}
}
  function getOptedTests($testType,$userId,$start=0,$offset)
	{
			 $this->db->select('id,testId,packageId,userPackageID,testType,attempts,createdTime'); 
			 $this->db->from(TBL_TIMES);
			 $this->db->where('userId',$userId);
			 $this->db->where('isActive',1); 
			// if($start){
			   $this->db->limit($offset,$start); 
			// }
			if($testType==2){
				   $this->db->group_by('testId');
				   $this->db->group_by('userPackageID');
			 }
			  if($testType==1){
				 $this->db->group_by('testId'); 
			 }
			
			 $this->db->where('testType',$testType);
			 $this->db->where('status',TEST_COMPLETED);
			 $this->db->order_by('id','desc');
			 $query = $this->db->get();
			 $optedTests=$query->result();  
				 for($i=0;$i<count($optedTests);$i++){ 
					$id=$optedTests[$i]->testId;
					$optedTests[$i]->testName="";
					$optedTests[$i]->packageName='';
					/* $testResults=$this->db->query("select * from ".TBL_TIMES."  where status= 3 AND testId=$id and userId=$userId and testType=$testType and isActive=1 ORDER BY id DESC limit 1")->row();
					$optedTests[$i]->testResults=$testResults; print_r($testResults);die(); */
					$testDetails=$this->testNameById($id);  
					$optedTests[$i]->testName=$testDetails->testName;
					$packageId= $optedTests[$i]->packageId;
					
					$packageDetails=$this->packageDetailsById($packageId);
					if($packageDetails)
					$optedTests[$i]->packageName=$packageDetails->package_name;
					$optedTests[$i]->completedTestCount=$this->getCompletedCountByTestId($id,$userId,$testType);
					$optedTests[$i]->type=$testType;
				   
				 }
			return (count($optedTests) >0)?$optedTests:array();
			 
		
	}
	function testNameById($id){
		 $this->db->select('testId,testName'); 
		 $this->db->from(TBL_TESTS);
		 $this->db->where('testId', $id);
		 $query = $this->db->get();
		 $testsDetails=$query->row();
		 return $testsDetails; 
	}
	function packageDetailsById($id){
		 $packageDetails=array();
		 $this->db->select('package_id,package_name,package_amount,isPaidPackage'); 
		 $this->db->from(TBL_PACKAGES);
		 $this->db->where('package_id', $id);
		 $query = $this->db->get();
		 $packageDetails=$query->row();
		 return $packageDetails; 
	}
	function getCompletedCountByTestId($id,$userId,$testType){
		$statusCount=$this->db->query("select * from ".TBL_TIMES."  where status= 3 AND testId=$id and userId=$userId and testType=$testType and isActive=1")->result();
		if($statusCount){
			$count=count($statusCount);
	     }else{
			$count=0;
		}
		return $count;
	}
  function getExamResults($testId,$testType,$attempts,$packageiD,$userPackageId,$userId){
	$examResults=array();$data=array();
	$data['testName']='';
	$data['totalQue']=0;
	$data['testMarks']=0;
	$data['attemptedQuestions']=0;
	$data['leftQuestions']=0;
	$data['correctAns']=0;
	$data['incorrectAns']=0;
	$data['testTime']=0;
	$data['timeTaken']=0;
	$data['rightMarks']=0;
	$data['negativeMarks']=0;
	$data['myMarks']=0;
	$data['diff']=0;
	$data['percentile']=0;
	$data['leftQueMarks']=0;
	$data['duration']=0;
	$testDetails="";
	$testSelect="testId,testType,testName,testTime,testAmount,rightMarks,negativeMarks,attempts,isShuffleQueOrder";
	$testSql="select ".$testSelect." from ".TBL_TESTS." where testId=".$testId; 
	$testDetails=$this->db->query($testSql)->row();
	$data['testName']=$testDetails->testName;
	$data['rightMarks']=$testDetails->rightMarks;
	$data['negativeMarks']=$testDetails->negativeMarks;
	$totalQue = $this->getTotalQueByTest($testId);
	$data['totalQue']=$totalQue;
	$attemptedQuestions = $this->getAttemptedQueByTest($testId,$userId,$testType,$attempts,$packageiD,$userPackageId);
   if($attemptedQuestions){
	    $leftQuestions = $totalQue-(count($attemptedQuestions));	 
   }else{
	    $leftQuestions = $totalQue;	
   }
   $data['attemptedQuestions']=($attemptedQuestions)?count($attemptedQuestions):0;
   $data['leftQuestions']=$leftQuestions;
   $examResultSql="select que_id,option_id,queType,textOption from ".TBL_EXAM_RESULTS." where testId =$testId and userId=$userId and  attempts=$attempts and testType=$testType and packageId=$packageiD and  userPackageID=$userPackageId and isActive =1";
   $examResults=$this->db->query($examResultSql)->result();
   $count=0; 
    if($examResults){
	    foreach($examResults as $examRes){
		   $queId=$examRes->que_id; 
		   $option=$examRes->option_id;
		   $res=$this->getCorrectAnsByTest($testId,$queId); 
		   $queType=$examRes->queType;
			   if($queType == 2 || $queType == 3){
						   $textOption=$examRes->textOption;
						   $where=array('option'=>$textOption,'test_id'=>$testId,'que_id'=>$queId);
						   $r=$this->getSingleRecord(TBL_QUE_OPTIONS,$where,$select="*");
							  if($r){
							   $count=$count+1;
							  }
				 }else{				   
						   $option=explode(',',$option);
						   $res=$this->getCorrectAnsByTest($testId,$queId);
						   if( is_array($option) && is_array($res) && 
							count($option) == count($res) &&
							array_diff($option, $res) === array_diff($res, $option)){$count=$count+1;}
						   
				 }
		}
   
	
  }
   $data['correctAns']=$count;
if($attemptedQuestions){   
   $data['incorrectAns'] =  count($attemptedQuestions)-$count;	
}
   $data['timeTaken']=0;
   $data['idleTime']= 0;
   $data['diff']="";
   $where=array('testId'=>$testId,'userId'=>$userId,'attempts'=>$attempts,'testType'=>$testType,'packageId'=>$packageiD,'userPackageID'=>$userPackageId,'isActive'=>1);
   $timingRecords=$this->getTimingsByAttempt(TBL_TIMES,$where,'*');
   if($timingRecords){ 
       $duration=$testDetails->testTime;
	   $diff=$timingRecords->timeDifference;
	   $date_s=$timingRecords->startTime;
	   $date_e=$timingRecords->endTime;
	   $timedifferenceFromDb= $this->getDifference($userPackageId,$attempts,$testId);
	   $data['diff']= $diff;
	   $data['duration']= $duration;
	   $data['percentile']=round((($count/$totalQue)*100),2);
       
   }
        if($count || $data['incorrectAns']){
			$data['myMarks']=($count*($testDetails->rightMarks))-($testDetails->negativeMarks*($data['incorrectAns']));
        }   
       $data['testMarks']=$totalQue*($testDetails->rightMarks);
       $data['leftQueMarks']=$leftQuestions*($testDetails->rightMarks);
	 //echo"<pre>";print_r($data);die();
  
return $data;

  }
function getDifference($uniqueId,$attempts,$testID){
		 $sql = "SELECT TIMEDIFF(endTime,startTime) as time FROM oes_time WHERE testId=$testID  and userPackageID=$uniqueId and attempts=$attempts";
		 $query = $this->db->query($sql);
		 $result=$query->row();
		 $tt=(string)$result->time; 
		 $sql1 = "SELECT TIME_TO_SEC('$tt') as sec";  
		 $query = $this->db->query($sql1);
		 $rr=$query->row(); 
		
		 return $rr;
	}
	function getTotalQueByTest($test_id)
	{
		 $this->db->select('*'); 
		 $this->db->from(TBL_QUETIONS);
		 $this->db->where('test_id',$test_id);
		 $this->db->where('isActive',1);
		 $this->db->where('isDeleted',0);
         $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->num_rows():'';
	}
	function getTimingsByAttempt($table,$where,$select)
	{
		 $this->db->select($select); 
		 $this->db->from($table);
		 $this->db->where($where);
         $query = $this->db->get(); 
		 return ($query->num_rows() >0)?$query->row():'';
	}
	function getAttemptedQueByTest($test_id,$userId,$type,$attempts,$package,$unique)
	{
		 $this->db->select('*'); 
		 $this->db->from(TBL_EXAM_RESULTS);
		 $this->db->where('testId',$test_id);
		 $this->db->where('userId',$userId);
		 $this->db->where('testType',$type);
		 $this->db->where('attempts',$attempts);
		 $this->db->where('packageId',$package);
		  $this->db->where('userPackageID',$unique);
		 $this->db->where('isActive',1);
		 $where = '(option_id !=0 OR textOption != "")';
		 $this->db->where($where);
		 $query = $this->db->get(); 
         $result=$query->result();
		 if($result){return $result;}else{return '';}
	} 
   function getCorrectAnsByTest($test_id,$queId)
	{$i=0;
         $this->db->select('option_id,option');  
		 $this->db->from(TBL_QUE_OPTIONS);
		 $this->db->where('test_id',$test_id);
		 $this->db->where('que_id',$queId);
		 $this->db->where('is_answer',1);
		 $query = $this->db->get(); 
		 $array=array();
		 $res=$query->result();
		 if($res){
		 foreach($res as $re){
		 $array[$i]=$re->option_id;
		 $i++;}
		 }
		 return ($query->num_rows() >0)?$array:array();

	}
	function promoterDetails($resellerId){
		 $checkIsPromoter="";
		 $sql="SELECT * FROM oes_resellers WHERE reseller_id= ".$resellerId."  AND isDeleted=0 AND isActive=1 AND isReseller = 2";
		 $query=$this->db->query($sql);
		 if($query){
		 $checkIsPromoter=$query->row();
		 }
		if($checkIsPromoter){
			$data['tests']="";
			$data['packages']="";
			$sql1="SELECT  GROUP_CONCAT(p_test_id SEPARATOR ', ') AS tests FROM oes_promoter_tests where p_t_promoter_id =".$resellerId."  AND p_t_is_active =1 AND p_t_is_deleted=0";
			$query=$this->db->query($sql1);
			if($query){
				$tests=$query->row();
				$data['tests']=$tests->tests;
			}
		    $sql2="SELECT  GROUP_CONCAT(p_package_id SEPARATOR ', ') AS packages FROM oes_promoter_packages where p_promoter_id =".$resellerId." AND p_is_active =1 AND p_is_deleted=0";
			$query=$this->db->query($sql2);
			if($query){
				$packages=$query->row();
			    $data['packages']=$packages->packages;
			}
			return $data;
		}else{
			return false;
		}
		
	}
	function getFreeTests($resellerId,$userId,$corseIds){
		     $result =array();$tests=$packages="";$promoterDetails="";
			 if(empty($resellerId)){ $resellerId = 0;}else{ 
				 $promoterDetails=$this->promoterDetails($resellerId);
				 if($promoterDetails){
					 $tests=$promoterDetails['tests'];
				 }
			 }
			 $sql="SELECT `testId`,`testName`,`testType`,`isFree`,`attempts` as `testAttempts`,`course_id` FROM ".TBL_TESTS." WHERE `course_id` IN (".$corseIds.") AND `testType` = ".PRACTICAL_TEST." AND isDeleted = 0 AND isFree = 1 AND isActive = 1 ";
			 if($tests){
				$sql.=" AND testId IN (".$tests.") " ;
			 }
			 if(empty($promoterDetails)){
				 $sql.=" AND `resellerId` = ".$resellerId."";
			 }
			 $sql.=" order by testId DESC";
			 $query=$this->db->query($sql);
			 $res=array();
			 if($query){
			 $res=$query->result(); 
			 }
			 if($res){ 
			 foreach($res as $r){ 
			  // $r->main='2';
			   $r->puchasedOne=0;
			   $r->expiryDate="";
			   $r->packageId=0;
			   $r->package_name="";
			   $r->package_amount=0;
			   $r->uniqueId=0;
			   $result[]=$r;}
			 }
			 $completedresult=$result;
			// if(count($completedresult) < $offset){ 
				 $newarray=array();
				 $newResults=$this->purcahsedfreeTests($userId);
				 if($newResults){
				 $k=count($result)-1; 
				 foreach($newResults as $new){
					 if($new->isFree == 1){
						 $newarray[]=$new;
					 }
					
				 $k++;}
				 $completedresult=array_merge($result,$newarray);
				 } 
			 //}
			
			 return ($completedresult)?$completedresult:array();
	}
	function purcahsedfreeTests($userId){ 
		     $mainarray=array();
       	     $testArray=array();
			 $myPurchasedTests=array();
             $myPackageTests=array();
			 //$userId=$this->session->userdata('user_id');
			 $this->db->select('*'); 
			 $this->db->from(TBL_USER_PACKAGE);
			 $this->db->where('isDeleted',0);
			 $this->db->where('userId',$userId);
			 $this->db->where('isActive',1);
			 $this->db->order_by('id','desc');
			 $query = $this->db->get(); 
			 $results=$query->result();  
			 if($results){
                  
				  foreach($results as $result){
				          $testId=$result->packageTests;
				          $packageId=$result->packageId;
				          $uniqueId=$result->id;
						  if($result->amount == 0 && $result->txnid ==0){
							  $puchasedOne=0;
						  }else{
							  $puchasedOne=1;
						  }
						   $validSql="SELECT DATEDIFF(package_valid_to,package_valid_from) as diff FROM oes_packages WHERE package_id = $packageId";
				           $validityCheck=$this->db->query($validSql)->row();
						  
					if(@$validityCheck->diff){ 
				           $validityCheck=$validityCheck->diff;
					       $createdTime=$result->createdTime;
						   $expiryDate = date('Y-m-d h:i:s', strtotime("+$validityCheck days", strtotime($createdTime)));
						   $A=strtotime($expiryDate);$B=strtotime($createdTime);
						   if($A >= $B){  
						    $mypckTests=$this->getTestsByTestIds($testId);
						    foreach($mypckTests as $mypckTest){
							$mypckTest->main='1';
							$mypckTest->puchasedOne=$puchasedOne;
							$mypckTest->expiryDate=$expiryDate;
							$mypckTest->packageId=$packageId;
							$pckName=$this->packageDetailsById($packageId);
							if($pckName){
								if($pckName->isPaidPackage == 0){
									$mypckTest->puchasedOne=1;
								}
							$mypckTest->package_name=$pckName->package_name;
							$mypckTest->package_amount=$pckName->package_amount;
                            }else{$mypckTest->package_name="";}
							$mypckTest->uniqueId=$uniqueId;
							$myPackageTests[]=$mypckTest;
						  }
					    }
				}else{  
				          $expiryDate="";
				          $mypckTests=$this->getTestsByTestIds($testId);
						  foreach($mypckTests as $mypckTest){
							  $mypckTest->main='1';
							  $mypckTest->puchasedOne=$puchasedOne;
							$mypckTest->expiryDate=$expiryDate;
							$mypckTest->packageId=$packageId;
							$pckName=$this->packageDetailsById($packageId);
							if($pckName){
							$mypckTest->package_name=$pckName->package_name;
							$mypckTest->package_amount=$pckName->package_amount;
						    }else{$mypckTest->package_name="";}
							$mypckTest->uniqueId=$uniqueId;
							$myPackageTests[]=$mypckTest;}
					}
				}
			 }
			 
			return (count($myPackageTests) > 0)?$myPackageTests:array();
	}
	/* get tests by test ids */
	function getTestsByTestIds($testId){
		 $mypckTests=array();
		 $sql1="SELECT  testId,testName,testType,isFree,attempts as testAttempts FROM oes_tests WHERE testId IN ($testId)  and  isActive=1 and isDeleted=0";
		 $query = $this->db->query($sql1);
		 if($query){
		 $mypckTests=$query->result();
		 }
		return ($query->num_rows() >0)?$mypckTests:array();
	}
	function getPaidTestsByTestIds($testId){
		$mypckTests=array();
		 $sql1="SELECT  testId,testName,testType,isFree,attempts as testAttempts FROM oes_tests WHERE testId IN ($testId)  and  isActive=1 and isDeleted=0 and isFree =0";
		 $query = $this->db->query($sql1);
		if($query){
		 $mypckTests=$query->result();
		 }
		return ($query->num_rows() >0)?$mypckTests:array();
	}
	function getLatestStatus($id, $uId, $uniqueId)
	{
		$this->db->select('*');
		$this->db->from(TBL_TIMES);
		$this->db->where('isActive', 1);
		$this->db->where('testId', $id);
		$this->db->where('userId', $uId);
		$this->db->where('userPackageID', $uniqueId);
		$this->db->order_by('attempts', 'desc');
		$query = $this->db->get();
		$results = $query->row();
		return $results;
	}
	function getNumberOfAttempts($table,$where)
	{
		         $this->db->select('*'); 
		         $this->db->from($table);
		         $this->db->where($where);
				 $this->db->order_by('id','desc');
                 $query = $this->db->get();
				 $attempts=$query->row();
				 if($attempts){return $attempts;}else{return '';}
				 
	}
	function getPaidTests($userId){
		     $mainarray=array();
       	     $testArray=array();
			 $myPurchasedTests=array();
             $myPackageTests=array();
			 $this->db->select('*'); 
			 $this->db->from(TBL_USER_PACKAGE);
			 $this->db->where('isDeleted',0);
			 $this->db->where('userId',$userId);
			 $this->db->where('isActive',1);
			// $this->db->where('isFree',0);//on 22 nd jan 2018
			 $this->db->order_by('id','desc');
			 //$this->db->limit($offset,$start); 
			 $query = $this->db->get(); 
			 $results=$query->result();  
			 if($results){
                  
				  foreach($results as $result){
				          $testId=$result->packageTests;
				          $packageId=$result->packageId;
				          $uniqueId=$result->id;
						  if($result->amount == 0 && $result->txnid ==0){
							  $puchasedOne=0;
						  }else{
							  $puchasedOne=1;
						  }
						   $validSql="SELECT DATEDIFF(package_valid_to,package_valid_from) as diff FROM oes_packages WHERE package_id = $packageId";
				           $validityCheck=$this->db->query($validSql)->row();
						  
					if(@$validityCheck->diff){
				           $validityCheck=$validityCheck->diff;
					       $createdTime=$result->createdTime;
						   $expiryDate = date('Y-m-d h:i:s', strtotime("+$validityCheck days", strtotime($createdTime)));
						   $A=strtotime($expiryDate);$B=strtotime($createdTime);
						   if($A >= $B){  
						  $mypckTests=$this->getPaidTestsByTestIds($testId);
						  foreach($mypckTests as $mypckTest){
							$mypckTest->main='1';
							$mypckTest->puchasedOne=$puchasedOne;
							$mypckTest->expiryDate=$expiryDate;
							$mypckTest->packageId=$packageId;
							$pckName=$this->packageDetailsById($packageId);
							if($pckName){
								if($pckName->isPaidPackage == 0){
									$mypckTest->puchasedOne=1;
								}
							$mypckTest->package_name=$pckName->package_name;
							$mypckTest->package_amount=$pckName->package_amount;
                            }else{$mypckTest->package_name="";}
							$mypckTest->uniqueId=$uniqueId;
							$myPackageTests[]=$mypckTest;
						  }
					    }
				}else{  
				         $expiryDate="";
				         $mypckTests=$this->getPaidTestsByTestIds($testId);
						  foreach($mypckTests as $mypckTest){
							  $mypckTest->main='1';
							  $mypckTest->puchasedOne=$puchasedOne;
							$mypckTest->expiryDate=$expiryDate;
							$mypckTest->packageId=$packageId;
							$pckName=$this->packageDetailsById($packageId);
							if($pckName){
							$mypckTest->package_name=$pckName->package_name;
							$mypckTest->package_amount=$pckName->package_amount;
						    }else{$mypckTest->package_name="";}
							$mypckTest->uniqueId=$uniqueId;
							$myPackageTests[]=$mypckTest;}
							
				  }
				}
			 }
			return (count($myPackageTests) > 0)?$myPackageTests:array();
	}
	/* function getQuestion($examid,$que_id,$start=0,$offset)
	{ 
		 $this->db->select('que_id,que,test_id,hint,queType'); 
		 $this->db->from(TBL_QUETIONS);
		 $this->db->where('test_id',$examid);
		 $this->db->where('isActive',1);
		 $this->db->where('isDeleted',0);
		 $this->db->where_in('que_id',$que_id);
		 $this->db->limit($offset,$start); 
         $query = $this->db->get();
		 $questions=$query->result();
		 if($questions){
				 foreach($questions as $question){
					 $question->isShuffle="";
					 $que_id=$question->que_id;
					 $this->db->select('option_id,option,is_answer,textType'); 
					 $this->db->from(TBL_QUE_OPTIONS);
					 $this->db->where('test_id',$examid);
					 $this->db->where('que_id',$que_id);
					 $this->db->where('isActive',1);
					 $this->db->where('isDeleted',0);
					 $query = $this->db->get();
					 $options=$query->result();
					 $question->options=$options;//for normal options
				 }
			}
		if($questions){return $questions;}else{return '';}
	} */
	function getQuestion($userId,$examid,$package,$uniqueId,$testType,$attempts,$que_id,$start=0,$offset)
	{    //print_r($que_id);die();
		 if($que_id){
			$s="";
			$str="";
			$qs=$que_id;
			foreach($qs as $q){
				$str.=$q.',';
			}$s=rtrim($str,',');
		  } 
	    
	     $isFifityFiftyApplicable=0;
		 $sql="SELECT `que_id`, `que`, `test_id`, `hint`, `queType`
			FROM `oes_quetions`
			WHERE `test_id` = '".$examid."'
			AND `isActive` = 1
			AND `isDeleted` =0
			AND `que_id` IN(".$s.") ";
			$sql.=" order by field(que_id,".$s.")";
			if($start){
				$sql.=" LIMIT ".$start." , ".$offset;
			}else{
				$sql.=" LIMIT ".$offset;
			}
			 $questionsQuery=$this->db->query($sql);  
			 if($questionsQuery){
				$questions =$questionsQuery->result();
			 } 
			// print_r($this->db->last_query());die();
		 if($questions){
				 foreach($questions as $question){ 
					 $question->isReviewed=0;$images=array();
					 $que_id=$question->que_id;
					 $SQL="select `q_image`,`q_position` from ".TBL_QUE_IMAGES." where q_que_id = ".$que_id." and q_is_active =1 and q_is_deleted = 0";
					 $imgQ=$this->db->query($SQL);
					 if($imgQ){
						 $images=$imgQ->result();
					 }
					 $question->images=$images;
					 $this->db->select('option_id,option,is_answer,textType'); 
					 $this->db->from(TBL_QUE_OPTIONS);
					 $this->db->where('test_id',$examid);
					 $this->db->where('que_id',$que_id);
					 $this->db->where('isActive',1);
					 $this->db->where('isDeleted',0);
					 $query = $this->db->get();
					 $options=$query->result();
					
					//for normal options
					 $optedOption=$this->getOptedOption($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts); //for opted options
					 $ReferenceOptions=$this->getReferenceOptions($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts); //for 50-50 reference options
				    //for checking 50-50 applicable or not
					if($options){
						$isFifityFiftyApplicable=$this->checkFifityFiftyApplicable($options);
					}
 				    $fiftyFityChancesOfTest=$this->getfiftyFityChancesOfTest($examid,$package,$testType,$uniqueId,$userId,$attempts);
                             
			       if($ReferenceOptions){
					  $options=$ReferenceOptions;
				   }else{
					  $question->isFifityFiftyApplicable=$isFifityFiftyApplicable;   
				  }
				  if($fiftyFityChancesOfTest && count($fiftyFityChancesOfTest) >=3){ 
					   $question->isFifityFiftyApplicable=0;   
				  }
				  
				  if($ReferenceOptions){
					  $question->isFifityFiftyApplicable=0; 
				  }
				  
				   $options=$this->getAnsweredOption($options,$optedOption);
					  if($optedOption && $optedOption->is_reviewed==1){
						  $question->isReviewed=1;
					 }
					 $question->options=$options;
			  }
			}
		if($questions){return $questions;}else{return array();}
	}
	function getAnsweredOption($options,$optedOption){ 
		$selectedOptionArray=array();
		 if($optedOption && $optedOption->option_id){
			$selectedOptionArray=explode(',',$optedOption->option_id);
		}
		 
        
		foreach($options as $option){
			$option->Answer=""; $option->isAnswerSelected=0;
			if($selectedOptionArray){
				 if(in_array($option->option_id,$selectedOptionArray)){
					$option->isAnswerSelected=1;
				}else{
				  $option->isAnswerSelected=0;
				} 
			}
			if($optedOption && $optedOption->textOption) {
			 $option->isAnswerSelected=1;
			 $option->Answer=$optedOption->textOption;
		 }
		}
		return $options;
	}
	function checkFifityFiftyApplicable($options){
		//echo"<pre>";print_r($options);die();
		$count=0;
		foreach($options as $opt){
			if($opt->is_answer == 1){
				$count=$count+1;
			}
		}
		if($count == 1){
			return 1;
		}else{ return 0;}
		
		
	}
/* end */
	
   
  function getAttemptedQue($test_id, $userId, $type, $attempts, $package, $uniqueId)
	{
		 $this->db->select('*'); 
		 $this->db->from(TBL_EXAM_RESULTS);
		 $this->db->where('testId',$test_id);
		 $this->db->where('userId',$userId);
		 $this->db->where('testType',$type);
		 $this->db->where('attempts',$attempts);
		 $this->db->where('packageId',$package);
		 $this->db->where('userPackageID',$uniqueId);
		 //$this->db->where('option_id!=',0);
		 //$this->db->where('textOption!=',"");
		 $this->db->where('isActive',1);
		 $where = '(option_id !=0 OR textOption != "")';
		 $this->db->where($where);
		 
		 $query = $this->db->get(); 
		
         $result=$query->result();
		 if($result){return $result;}else{return '';}
	}
	
  
   function getQuestionNumbers($examid,$isShuffle="")
   {
         $this->db->select('que_id'); 
		 $this->db->from(TBL_QUETIONS);
		 $this->db->where('isActive',1);
		 $this->db->where('isDeleted',0);
		 $this->db->where('test_id',$examid);
		 if($isShuffle == '1'){
         $this->db->order_by('rand()');   
		 }		 
         $query = $this->db->get();
		 $result=$query->result();
         if($result){return $result;}else{return '';}
    }

function getRandomElements($table,$where="",$select="",$opt="")
   {
         $this->db->select('option_id,option,is_answer,textType'); 
		 $this->db->from($table);
		 $this->db->where($where);
		 if($opt){
			  $this->db->where('option_id!=',$opt);
		 }
         //$this->db->order_by('rand()');   
         $this->db->limit(1);   
		 $query = $this->db->get(); 
		 $result=$query->result();
         if($result){return $result;}else{return '';}
    }


	function getOptedOption($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts){
		
		         $this->db->select('option_id,queType,textOption,is_reviewed');  //31
		         $this->db->from(TBL_EXAM_RESULTS);
		         $this->db->where('testId',$examid);
		         $this->db->where('que_id',$que_id);
		         $this->db->where('packageId',$package);
		         $this->db->where('testType',$testType);
		         $this->db->where('userPackageID',$uniqueId);
		         $this->db->where('userId',$userId);
		         $this->db->where('attempts',$attempts);
				 $this->db->where('isActive',1);
				 $this->db->order_by('id','desc');
				 $this->db->limit(1);
				 $query = $this->db->get();
				 $optedOption=$query->row();
				 return $optedOption;
	}
	function getReferenceOptions($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts){
	 	         $res="";
		         $this->db->select('opt_id'); 
		         $this->db->from(TBL_OPTIONS_REFERENCE);
		         $this->db->where('test_id',$examid);
		         $this->db->where('que_id',$que_id);
		         $this->db->where('userPackage',$uniqueId);
		         $this->db->where('user_id',$userId);
		         $this->db->where('attempts',$attempts);
				 $this->db->where('isActive',1);
                 $query = $this->db->get();
				 $optedOption=$query->row();
				 @$opt_id=$optedOption->opt_id;
				 if($opt_id){
				 $sql="select `option_id`,`option`,`is_answer`,`textType` from oes_que_options where  option_id in ($opt_id) and test_id='".$examid."' and que_id ='".$que_id."' and isActive=1 and isDeleted=0";
                 $query=$this->db->query($sql);	
				 
				 if($query){
				 $res=$query->result();
				 }
				 }
				 if($res){
				  return $res;
				 }else{
					 return "";
				 }
				 
	}
	function getfiftyFityChancesOfTest($examid,$package,$testType,$uniqueId,$userId,$attempts){
		         $this->db->select('opt_id'); 
		         $this->db->from(TBL_OPTIONS_REFERENCE);
		         $this->db->where('test_id',$examid);
		       //  $this->db->where('que_id',$que_id);
		         $this->db->where('userPackage',$uniqueId);
		         $this->db->where('user_id',$userId);
		         $this->db->where('attempts',$attempts);
				 $this->db->where('isActive',1);
                 $query = $this->db->get();
				 $optedOption=$query->result();
				 if($optedOption){
					 return count($optedOption);
				 }
				 else{
					 return 0;
				 }
	}

	
	function getReferenceQuestions($test_id,$user_id,$uniqueId,$attempts)
	{            
	             $testIdsArray=$this->db->query("select que_id from oes_quetions where test_id=$test_id And isActive=1 And isDeleted=0")->result();
				 if($testIdsArray){
					 foreach($testIdsArray as $questions){$mainarray[]=$questions->que_id;}
					  	  
					  $testIdsArray = array_values($mainarray);
                      }
					
				 $queArray='';$Qarray=array();
		         $this->db->select('que_id'); 
		         $this->db->from(TBL_QUESTIONS_ORDER);
		         $this->db->where('user_id',$user_id);
		         $this->db->where('test_id',$test_id);
		         $this->db->where('userPackage',$uniqueId);
		         $this->db->where('attempts',$attempts);
		         $this->db->where('isActive',1);
				 $this->db->order_by('id','desc');
				 $this->db->limit(1);
                 $query = $this->db->get();
				 $querystring=$query->row();
				 if($querystring){$querystring=$querystring->que_id;$queArray=explode(',',$querystring);
				  if($queArray){
					   foreach($queArray as $q){
						   if(in_array($q,$testIdsArray)){
							   $Qarray[]=$q;
						   }
						   
					   }
					 }
					 if(count($Qarray) < count($testIdsArray)){
					
						 foreach($testIdsArray as $qu){
							 
							  if(!in_array($qu,$Qarray)){
								  
							     array_push($Qarray,$qu);
						       }
					     }
					 }
	}
				 if($Qarray){ return $Qarray;}else{ return array();}
				 
	}
	function DeactiveTestReports($ids,$courseID,$uiD)
      {
		 $this->db->where('testType',1);
		 $this->db->where('userId',$uiD);
		 $this->db->where('isActive',1);
		 $this->db->where_in('testId',$ids);
		 $this->db->where_in('courseId',$courseID);
		 $data['isActive']=0;
		 $query = $this->db->update(TBL_EXAM_RESULTS,$data); 
		 if($query){
			 $query1=$this->DeactiveRecords($ids,$courseID,$uiD);
			 }
		return $query;
    } 
	function DeactiveRecords($ids,$courseID,$uiD){
		 $this->db->where('testType',1);
		 $this->db->where('userId',$uiD);
		 $this->db->where('isActive',1);
		 $this->db->where_in('testId',$ids);
		 $this->db->where_in('courseId',$courseID);
		 $data['isActive']=0;
		 $query=$this->db->update(TBL_TIMES,$data);
		 return $query;
	}
	function saveRecoveryEmailData($userid,$token,$purpose)
   {
	    //we create a new date that is 3 days in the future
	    $expireDateFormat = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")+3, date("Y"));
	    $expirationDate = date("Y-m-d H:i:s",$expireDateFormat);
	    $data = array(
			'userID'=>$userid,
			'purpose'=>$purpose,
			'token'=>$token,
			'isactive'=>1,
			'expirationDate'=>$expirationDate
	     );
	    $query = $this->db->insert(TBL_OTPS,$data);
	      if (!$query)
	      {
		    $this->throwException($query);
	      }
	     else
	     { 
		    return $this->db->insert_id();
	     }
    }
	//21 march

function getPackageItens(){
                 $this->db->select('*'); 
		         $this->db->from(TBL_CART);
		         $this->db->where('isActive',1);
		         $this->db->where('userId',$userId);
		         $this->db->where('itemType',1);
				 $query = $this->db->get();
				 $result=$query->row();
				 if($result){
					return $result;
				 }else{return '';}
}

function getSelectedTestsAmount($ids)
{
            //$sql = "SELECT SUM(testAmount) as total FROM oes_tests WHERE testId IN ($ids) and testType= 1 and isActive=1 and isDeleted=0 ";
            $sql = "SELECT SUM(testAmount) as total FROM oes_tests WHERE testId IN ($ids)  and isActive=1 and isDeleted=0 ";
			$query = $this->db->query($sql);
			$testArray = $query->row();
			return $testArray;
}
function getSelectedPackagesAmount($ids)
{
            $sql = "SELECT SUM(package_amount) as total FROM oes_packages WHERE package_id IN ($ids)  and isActive=1 and isDeleted=0";
			$query = $this->db->query($sql);
			$testArray = $query->row();
			return $testArray;
}
function getSelectedPackagesDetails($ids)
{
            $sql = "SELECT op.*,pp.p_sell_amount AS package_amount FROM oes_packages op LEFT JOIN oes_promoter_packages pp ON pp.p_package_id = op.package_id  WHERE op.package_id IN ($ids) and op.isActive=1 AND op.isDeleted=0";
			$query = $this->db->query($sql);
			$testArray = $query->result();
			return $testArray;
}
function getSelectedTestDetails($ids)
{          
            $testArray=array();
            $sql = "SELECT oT.*,pT.p_t_sell_amount AS testAmount FROM oes_tests ot LEFT JOIN oes_promoter_tests pt ON pt.p_test_id = ot.testId WHERE ot.testId IN ($ids) and ot.testType= 1 and ot.isActive=1 and ot.isDeleted=0 ";
           // $sql = "SELECT * FROM oes_tests WHERE testId IN ($ids) and testType= 1 and isActive=1 and isDeleted=0 ";
			$query = $this->db->query($sql);
			if($query){
			     $testArray = $query->result();
	         }
			return $testArray;
}
function getTestsNameByPackage($packageId)
	{    
		 $this->db->select(TBL_PACKAGES.'.package_test');
         $this->db->from(TBL_PACKAGES);
         $this->db->where(TBL_PACKAGES.'.package_id',$packageId);
		 $query = $this->db->get(); 
		 $res = $query->row();
		 if($res){
			 $testId=$res->package_test;
			 $sql = "SELECT * FROM oes_tests WHERE testId IN ($testId)  and isActive=1 and isDeleted=0";
			 $query = $this->db->query($sql);
			 $testArray = $query->result();
			 return $testArray;
		}
		
		
	}
	/*function getPackgesByPackageName($txt)
	{
		$this->db->select('*');
		$this->db->from(TBL_PACKAGES);
		$this->db->like('package_name', $txt);
		$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$query = $this->db->get();
		$results = $query->result();
	     return $results;
		
	}
	function getTestsBytestName($txt)
	{
		$this->db->select('*');
		$this->db->from(TBL_TESTS);
		$this->db->like('testName', $txt);
		$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$query = $this->db->get();
		$results = $query->result();
	     return $results;
	}*/
	function getPackgesByPackageName($txt,$resellerid)
	{
		$this->db->select('*');
		$this->db->from(TBL_PACKAGES);
		if($txt){
		$this->db->like('package_name', $txt);
		}
		$this->db->where('resellerId', $resellerid);
		$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$query = $this->db->get();
		$results = $query->result();
	     return $results;
		
	}
	function getTestsBytestName($txt,$resellerid)
	{
		$this->db->select('*');
		$this->db->from(TBL_TESTS);
		if($txt){
		$this->db->like('testName', $txt);
		}
		$this->db->where('resellerId', $resellerid);
		$this->db->where('isActive', 1);
		$this->db->where('isDeleted', 0);
		$this->db->where('testType', 1);
		$query = $this->db->get();
		$results = $query->result();
	     return $results;
	}
	
	function getLastAtemptedQue($id, $uId, $uniqueId, $attempts)
	{
		$this->db->select('que_id');
		$this->db->from(TBL_EXAM_RESULTS);
		$this->db->where('isActive', 1);
		$this->db->where('testId', $id);
		$this->db->where('userId', $uId);
		$this->db->where('userPackageID', $uniqueId);
		$this->db->where('attempts', $attempts);
		$this->db->order_by('attempts', 'desc');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		$results = $query->row();
		if($results){
	     return $results->que_id;
		}else{return 0;}
	}
	
	function getUserAnswers($testId,$queId,$option_id,$queType=""){
		
		//31
		if($queType == 2 || $queType == 3){
			$sql = "SELECT `option`,`textType`,`option_id` FROM oes_que_options WHERE  isActive=1 and isDeleted=0 and que_id=$queId and test_id=$testId";
		}else{
		$sql = "SELECT `option`,`textType`,`option_id` FROM oes_que_options WHERE option_id IN ($option_id)  and isActive=1 and isDeleted=0 and que_id=$queId and test_id=$testId";
		}
		$query = $this->db->query($sql);
		
		$result=$query->result();
		return $result;
	}
	function getUserCouponsDetails($uId,$couponId){
		$sql = "SELECT * FROM `oes_user_coupon_details` WHERE userId=$uId  and couponId=$couponId order by id DESC limit 1 ";
		$query = $this->db->query($sql);
		$result=$query->row();
		return $result;
	}
	function getRecord($table,$where){ 
		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		$results = $query->row();
		
		if($results){
			return $results->textOption;
			
		}else{
			return "";
		}
	}
	function getMyAnswers($userId,$testId,$userPackageId,$type,$queId,$attempts){
		 $this->db->select('*');
	     $this->db->from(TBL_EXAM_RESULTS);
	     $this->db->where('userId',$userId);
	     $this->db->where('testId',$testId);
	     $this->db->where('userPackageID',$userPackageId);
	     $this->db->where('testType',$type);
	     $this->db->where('que_id',$queId);
	     $this->db->where('attempts',$attempts);
	     $query = $this->db->get();
		 $result=$query->result(); 
		foreach($result as $res){
			if($res->queType ==0 || $res->queType ==1){
				$res->option="";$res->textType="";
				$option_id=$res->option_id;
				$query="select `textType`,`option`,`option_id` from oes_que_options where test_id=$testId and que_id=$queId and option_id in ($option_id)";
			    $re= $this->db->query($query); 
				if($re){
				$res->normalOptions=$re->result();
				}
			   
			}else if($res->queType ==2 || $res->queType ==3){
				$option_id=$res->option_id;
				$res->textType=1;
				if(empty($option_id)){
					$res->option=$res->textOption;
				}
				
			}
		
		}
		 return $result;
	}
	
	function getUserPacks($table,$where){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$this->db->group_by('packageId');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		$results = $query->result();
		
		if($results){
			return $results;
			
		}else{
			return "";
		}
	}
/* for promotion tests */
function getPromotionTests(){
		     $resellerId=$this->session->userdata('reseller_id');
			 $result ='';
			 $this->db->select('*'); 
			 $this->db->from(TBL_TESTS);
			// $this->db->where('testType',$testType);
			 $this->db->where('isPromotionalTest',1);
             $this->db->where('isDeleted',0);
			 $this->db->where('isActive',1);
			 if($resellerId){
			 $this->db->where('resellerId',$resellerId);
		     }else{ $this->db->where('resellerId',0);}
			 $query = $this->db->get();
			  
			 $res=$query->result();
			 if($res){
			 foreach($res as $r){ 
			   $r->packageId=0;
			   $r->package_name=0;
			   $r->main='2';$r->uniqueId=0;
			   $r->expiryDate="";
			   $r->puchasedOne="";
			   $r->package_amount=0;
			   $result[]=$r;}
			 } 
			 $completedresult=$result;$newarray=array();
			 $newResults=$this->freeTests(); 
			 if($newResults){ 
			 $k=count($result)-1; 
			 foreach($newResults as $new){
				 if($new->isFree == 1){
					 $newarray[]=$new;
				 }
				
			 $k++;} 
			
			 $completedresult=array_merge($result,$newarray);
			 } 
			 return ($completedresult >0)?$completedresult:'';
	}	
	 function getSingleRecord($table,$where,$select="*")
	{
		$this->db->select($select);
		$this->db->where($where);
        $query = $this->db->get($table);
		if (!$query)
			{    
		        $results ="";
			}else{
				$results = $query->row();
	            return $results;
			}
		
	}
	function getQuestionInSummary($userId,$examid,$package,$uniqueId,$testType,$attempts,$que_id,$start=0,$offset)
	{    
	     $isFifityFiftyApplicable=0;$questions=array();
		 $this->db->select('que_id,que,test_id,hint,queType'); 
		 $this->db->from(TBL_QUETIONS);
		 $this->db->where('test_id',$examid);
		 $this->db->where('isActive',1);
		 $this->db->where('isDeleted',0);
		 $this->db->where_in('que_id',$que_id);
		 $this->db->limit($offset,$start); 
        $query = $this->db->get();
		if($query){
			$questions=$query->result();
		}
		 
		 if($questions){
				 foreach($questions as $question){
					 $que_id=$question->que_id;$images=array();
					 $SQL="select `q_image`,`q_position` from ".TBL_QUE_IMAGES." where q_que_id = ".$que_id." and q_is_active =1 and q_is_deleted = 0";
					 $imgQ=$this->db->query($SQL);
					 if($imgQ){
						 $images=$imgQ->result();
					 }
					$question->images=$images;
					 $this->db->select('option_id,option,is_answer,textType'); 
					 $this->db->from(TBL_QUE_OPTIONS);
					 $this->db->where('test_id',$examid);
					 $this->db->where('que_id',$que_id);
					 $this->db->where('isActive',1);
					 $this->db->where('isDeleted',0);
					 $query = $this->db->get();
					 $options=$query->result();
					//for normal options
					 $optedOption=$this->getOptedOption($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts); //for opted options
					 $ReferenceOptions=$this->getReferenceOptions($examid,$que_id,$package,$testType,$uniqueId,$userId,$attempts); //for 50-50 reference options
				   // $correctOptions=$this->      
			       if($ReferenceOptions){
					  $options=$ReferenceOptions;
				   }
				  
				   $options=$this->getAnsweredOption($options,$optedOption);
				   $question->options=$options;
			  }
			}
		if($questions){return $questions;}else{return array();}
	}
	function checkCouponValidity($couponCode){
		$result=array();
		$sql="SELECT * FROM `oes_coupons` where BINARY `coupon_code` = '".$couponCode."' AND isActive = 1 AND isDeleted = 0"; //print_r($sql);die();
		$query=$this->db->query($sql);
		if($query){
			$result=$query->row_array();
		}else{
			return array();
		}
		return $result;
	}
	function getCourseTestOrPackage($courseId,$resellerId){
		$result['testCount']=0;
		$result['packCount']=0;
	  $testsql="select COUNT(testId) AS tests from oes_tests where course_id = ".$courseId." and isActive=1 and isDeleted= 0 and resellerId = ".$resellerId."";
	  $testQuery=$this->db->query($testsql);
	  if($testQuery){
		  $test=$testQuery->row();
		  $result['testCount']=$test->tests;
	  }
	  $packsql="select COUNT(package_id) AS packages from oes_packages where courseId = ".$courseId." and isActive=1 and isDeleted= 0 and resellerId = ".$resellerId."";
	   $packQuery=$this->db->query($packsql);
	  if($packQuery){
		  $pack=$packQuery->row();
		  $result['packCount']=$pack->packages;
	  }
	  return $result;
	  
 	}
	function getUserPayments($userId,$offset,$start){
		$this->db->select('txnid');$transactions=$Result=array();
        $this->db->distinct();		
		$this->db->From('oes_user_packages');
		$this->db->where('isActive',1);
		$this->db->where('userId',$userId);
		$this->db->where('txnid!=',"");
		$this->db->where('isDeleted',0);
		$this->db->order_by('id','desc');
		$this->db->limit($offset,$start); 
        $query=$this->db->get();
		$transactions=$query->result();
		if($transactions){
			foreach($transactions as $transaction){
				$txnid=$transaction->txnid;
				$Result[]=$this->transactionDetails($txnid);
			}
		}
		return $Result;
	}
	function transactionDetails($txnid){
		$RESULT=array();
		$packIds=$testIds="";
		$packages=$tests=array();
		$this->db->select('*');
        $this->db->distinct();		
		$this->db->From('oes_user_packages');
		$this->db->where('isActive',1);
		$this->db->where('isDeleted',0);
		$this->db->where('txnid',$txnid);
		$query=$this->db->get();
		$transactions=$query->result();
		if($transactions){
		$i=0;
			foreach($transactions as $trans){ 
				if($trans->packageId){
					$packIds.=$trans->packageId.',';
				}
				if(empty($trans->packageId) && $trans->packageTests){
					$testIds.=$trans->packageTests.',';
				}
				$RESULT['paidAmount']=$trans->paidAmount;
				$RESULT['ccavenueOrderId']=($trans->ccavenueOrderId)?$trans->ccavenueOrderId:0;
				$RESULT['couponCode']=$trans->couponCode;
				$RESULT['couponAmount']=$trans->couponAmount;
				$RESULT['txnid']=$trans->txnid;
				$RESULT['paymentDate']=$trans->createdTime;
				if($trans->ccavenueOrderId){
					$paymentType="CCavenue";
				}else{
					$paymentType="Used Coupon";
				}
				$RESULT['paymentType']=$paymentType;
				$i++;
			}
			$packId=rtrim($packIds,',');
			$testId=rtrim($testIds,',');
			if($testId){
				$sql="SELECT  testId,testName FROM oes_tests WHERE testId IN (".$testId.")  and  isActive=1 and isDeleted=0";
				$query = $this->db->query($sql);
				 if($query){
					 $tests=$query->result();
				 }
				//$tests=$this->getTestsByTestIds($testId);
			}
			if($packId){
				$sql = "SELECT package_id,package_name from oes_packages WHERE package_id IN (".$packId.")";
			     $query = $this->db->query($sql);
				 if($query){
					 $packages=$query->result();
				 }
			 }
			 $RESULT['tests']=$tests;
			 $RESULT['packages']=$packages;
			 
		}
		return $RESULT;
		
	}

}








