<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Healthcontroller extends CI_Controller {

	function __construct()
    {

	
        // Call the Model constructor
        parent::__construct();
	
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('email');
	
        $this->load->library('session');
		$this->load->helper('url');
		//$this->load->helper('captcha');
    }
	
	
	/**
	 * Method to log the exception
	 *
	 */
	protected function logExceptionMessage($exception)
	{
		$errorMessage = 'Exception of type \'' . get_class($exception) . 
					'\' occurred with message: ' . $exception->getMessage() . 
					' in file ' . $exception->getFile() . 
					' at line ' . $exception->getLine();
				 
					// Add backtrace:					
					$errorMessage .= "\r\n Backtrace: \r\n";
					$errorMessage .= $exception->getTraceAsString();
					log_message('error',$errorMessage,TRUE);
	}
		
		
	function makeServiceCall($service_url, $data = array(), $method = 'POST')
	{		
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => $method,
				'content' => http_build_query($data),
			),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($service_url, false, $context);	
		//var_dump($result);die();
		$result = utf8_encode($result);
		
		$result = str_replace('ï»¿','',$result);
		$result = json_decode($result);
		return $result;
	}	
	

   function isValidSession($sessionId,$Method)
   {
	   return true;
	   
   }   
}

