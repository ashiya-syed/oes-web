<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
/**
 * This resource contains the services for the Admin authentication..etc., and other admin related services.
 * 
 * @category	Restful WebService
 * @controller  Admin Controller
 * @author		Ashiya syed
 */

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
class Content extends REST_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->helper('common_helper');
	}
  function termsAndConditions_get(){
    $this->load->view('termsAndConditions');
  }

  function privacyPolicy_get(){
    $this->load->view('privacy_policy');
  }
  function refundPolicy_get(){
    $this->load->view('refund_policy');
  }
  function aboutUs_get(){
    $this->load->view('aboutUs');
  }
  function registrationAndServiceDetails_get(){
    $this->load->view('registrationAndServiceDeatails');
  }
  
  
          function contactUs_post(){
	                $this->load->library('form_validation');
					$this->form_validation->set_rules('email_id','email','trim|required|valid_email');
					$this->form_validation->set_rules('message','message','trim|required');
					$this->form_validation->set_rules('mobile','Mobile','trim|required|min_length[10]|max_length[10]');
					$this->form_validation->set_rules('token','Token','trim|required');

                    if($this->form_validation->run()!=false)
						{ 
					     $token=@$this->input->post('token');
						  $wrongToken= $this->checkTokenValid($token);
						   if($wrongToken){
								$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
						   }
                         $message="";
						 $message.=$this->input->post('message');
                         if($this->input->post('mobile')){
						  $message.="<br>Contact number : ".$this->input->post('mobile');
                          }
						 $subject="Contact us";
						 $email_id=$this->input->post('email_id');
						 $smtpEmailSettings = $this->config->item('smtpEmailSettings');
						 $isEmailSent = @sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
                        if(@$isEmailSent)
                        {
                          $message = array('status' => 1,'message'=> 'Mail sent successfully');
			            }
                        else
                        {
                           //$message = array('status' => 0,'message'=> 'Mail sending failed.');
						    $message = array('status' => 1,'message'=> 'Mail sent successfully.');
			            }
						 
                        }
						else {
						$message = array('status' => 0,'message'=>strip_tags(validation_errors()),'reason' => 'Validation error');
		  	           }
				     $this->response($message, 200);
          } 

}
?>