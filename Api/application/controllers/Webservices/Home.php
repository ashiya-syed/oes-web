<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
/**
 * This resource contains the services for the Admin authentication..etc., and other admin related services.
 * 
 * @category	Restful WebService
 * @controller  Admin Controller
 * @author		Ashiya syed
 */

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php'; 

class Home extends REST_Controller {
 
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		date_default_timezone_set('Asia/Kolkata');

	}
	/*******************
	********************
	This method is useful to 
	GET PACKAGES for purchasing
	********************
	********************/
	
	function packagesForPurchase_get()
	{
		
		try{
		  $token=@$this->get('token');
		  $course=@$this->get('courseIds');
		  if(empty($course))
			{
				$this->response(array('status'=>0,'message' => 'courseIds are required.'), 200); die();
			}
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
			$data = array();
			$offset = DEFAULT_LIMIT;
			$resellerId=@$_GET['resellerId'];
			$start=@$_GET['start'];
			$promoterPacks=$this->Testsmodel->promoterDetails($resellerId);
			$packages=($promoterPacks)?$promoterPacks['packages']:"";
		    $packages= $this->Packagemodel->getPackages($resellerId,$course,$packages,$start,$offset);
			 if($packages){
				 $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $packages,'offset'=>$offset);
			   }else{
			      $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => $data);
		       }
			$this->response($message, 200);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	}
	/*******************
	********************
	This method is useful to 
	get syllabus details of packages
	********************
	********************/
	
     function viewSyllabusByPackageId_get()
     { 
	     $token=@$this->get('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
	    $packageTests=array(); $idArray=array();$data=array();
		$packageId=@$this->get(packageId);
		$start=(@$this->get('start'))?@$this->get('start'):0;
		
		$offset = DEFAULT_LIMIT;
        if( empty($packageId) )
		{
			$this->response(array('status'=>0,'message' => ' packageId is required.'), 200); die();
		}
	    $where=array('package_id'=>$packageId);
		$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*"); 
		if($packageDetails){
		$data['packageName']=$packageDetails['package_name'];
		$data['package_amount']=$packageDetails['package_amount'];
		$testId=$packageDetails['package_test'];
		$ids= explode(',', $testId); 
		if($start){$i=$start;}else{$i=0;} 
		if($ids){ 
			for($i=$start;$i<count($ids);$i++){
				if(!empty($ids[$i])){
					if(count($idArray)<10){@$idArray[]=$ids[$i];}
					
				}
			} 
			
		} 
		if($idArray){
		$packageTests= $this->Packagemodel->getalltestsByTestIds($idArray);
		}
		$data['packageTests']=$packageTests;
			if($data['packageTests']){
				$message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $data,'offset'=>$offset);
			}else{
				$message = array('status' => 0,'message' => NO_RECORDS_EXISTS,'result' => $data,'offset'=>$offset);
			 }
		 $this->response($message, 200);
		
		}else{
			$message = array('status' => 0,'message' => PROVIDE_VALID_PACKAGE_ID);
			$this->response($message, 200);
		}
}
  /*******************
	********************
	This method is useful to 
	get paid tests 
	********************
	********************/
  function getMainTestsForPurchase_get(){
	try{
		$token=@$this->get('token');
		$course=@$_GET['courseIds'];
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
			if(empty($course))
			{
				$this->response(array('status'=>0,'message' => 'courseIds are required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		 $offset = DEFAULT_LIMIT;
		 
		 $resellerId=@$_GET['resellerId'];
		 $start=@$_GET['start'];
		 $PurchaseTests= $this->Testsmodel->getPurchasedTests($resellerId,$course,$start,$offset);//print_r($this->db->last_query());die();
		  if($PurchaseTests){
			  $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $PurchaseTests,'offset'=>$offset);
		 }else{
			 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
		 }
		 $this->response($message, 200);
	}catch (Exception $exception)
	{
		$data['error']=$exception->getMessage();
		$this->logExceptionMessage($exception);					
	} 
}
/*******************
	********************
	This method is useful to 
	get news and updates
	********************
	********************/
function getNewsAndUpdates_get(){
	try{
		$token=@$this->get('token');
		 /*  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   } */
	     $resellerId=0;
	     $offset = DEFAULT_LIMIT;
		 $resellerId=@$_GET['resellerId'];
		 $start=@$_GET['start'];
	       if($resellerId){
			$resellerId=$this->session->userdata('reseller_id');
		    }else{
			$resellerId=0;
		    }
			$where=array('resellerId'=>$resellerId,'isActive'=>1,'isDeleted'=>0);
			$newsUpdates=$this->getOffsetRecords(TBL_NEWS,$where,$select="id,newsTitle,description,createdTime",$start,$offset,'id'); 
			if($newsUpdates){
			  $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $newsUpdates,'offset'=>$offset);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
			 $this->response($message, 200);
			
	}catch (Exception $exception)
	{
		$data['error']=$exception->getMessage();
		$this->logExceptionMessage($exception);					
	}
}
/*******************
	********************
	This method is useful to 
	get newsdetails by id
	********************
	********************/
function newsDetails_get(){
	$token=@$this->get('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		$data=array();
		try{
		$id=@$this->get('id');
		if( empty($id) )
		{
			$this->response(array('status'=>0,'message' => 'News id is required.'), 200); die();
		}
	    $where=array('id'=>$id);$table=TBL_NEWS;
        $result = $this->getSingleRecord($table,$where,$select="id,newsTitle,description,createdTime");
		if($result){
			  $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $result);
		}else{
			 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
		}
		 $this->response($message, 200);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
  }
	  
	/*******************
	********************
	This method is useful to 
	get libraries
	********************
	********************/
	  
	function libraries_get(){ 
	     $data=array();
	     $newLib=array();
		 $offset = DEFAULT_LIMIT;
        try{
			$token=@$this->get('token');
			$start=(@$this->get('start'))?(@$this->get('start')):0; 
		    if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
			    $offset = DEFAULT_LIMIT;
				$userId=@$this->get('userId');
				if( empty($userId) )
				{
					$this->response(array('status'=>0,'message' => 'UserId is required.'), 200); die();
				}
			$where=array('userId'=>$userId);			
		    $libraries=$this->Testsmodel->getUserLibraries(TBL_USER_PACKAGE,$where,$start,$offset);
			 if($libraries){ 
			 for($i=$start;$i<count($libraries);$i++){
				  if(!empty($libraries[$i])){
					if(count($newLib) < $offset){ @$newLib[]=$libraries[$i]; }
					 }
			    } 
			 }  $libraries=$newLib;
			if($libraries){
			  $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $libraries,'offset'=>$offset);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
			 $this->response($message, 200);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	}
	
	/*******************
	********************
	This method is useful to 
	get library details by id
	********************
	********************/
	  
	function getLibDetails_get(){
		try{
			$token=@$this->get('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
			$id=@$_GET['id'];
			if( empty($id) )
			{
			  $this->response(array('status'=>0,'message' => 'Library id is required.'), 200); die();
			}
			$where=array('id'=>$id);
			$details=$this->getSingleRecord(TBL_LIBRARIES,$where,"*");
			if($details){
			  $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $details);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
			 $this->response($message, 200);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
    }
	
	
}

?>