<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
class Payment extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		date_default_timezone_set('Asia/Kolkata');
	}
	
	/*********************************************
	 ccavenueCallBack
	*********************************************/
	function ccavenueCallBack()
	{
		 

		    if(@$_POST){
				
				$workingKey = CCAVENUE_WORKING_KEY;
				$encResponse=@$_POST["encResp"];	 
				$where=array();
				$saveEncData['request']='Android';
				$saveEncData['response']=$encResponse;
				$this->insertOrUpdate(TBL_CC_CALLBACKS,$where,$saveEncData);
				
				$rcvdString=$this->ccdecrypt($encResponse, $workingKey);
				
				$order_status="";
				$orderID=0;
				$decryptValues=explode('&', $rcvdString);
				$dataSize=sizeof($decryptValues);
				for($i = 0; $i < $dataSize; $i++) 
				{
					$information=explode('=',$decryptValues[$i]);
					if($i==3)	$order_status=$information[1];
					if($i==0)	$orderID=$information[1];
				}
				echo "<center>";
				if($order_status=="Success"){ 
					$this->saveDataAfterCCavenueSuccess($orderID);
					echo "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";
				}else if($order_status == "Aborted"){
					 //$this->saveDataAfterCCavenueSuccess($orderID);
					 echo "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
				}else if($order_status==="Failure"){
					echo "<br>Thank you for shopping with us.However,the transaction has been declined.";
				}else
				{
				  echo "<br>Security Error. Illegal access detected";
				}
				
				echo "<br><br>";

				echo "<table cellspacing=4 cellpadding=4 style='color:#fff'>";
				for($i = 0; $i < $dataSize; $i++) 
				{
					$information=explode('=',$decryptValues[$i]);
						echo '<tr><td>'.$information[0].'</td><td>'.$information[1].'</td></tr>';
				}

				echo "</table><br>";
				echo "</center>"; die();
			}else{
				
				echo "You Don't have access to this url. Unauthorized!";die();
			}
	}
	
	/* update order id status and remoove items from cart */
function saveDataAfterCCavenueSuccess($orderId){ 
	
	    $details=$this->getAllRecords(TBL_USER_PACKAGE,$where=array('ccavenueOrderId'=>$orderId,'isActive'=>0,'isDeleted'=>0));
		if($orderId && $details){ 
			$updata['createdTime']= date("Y-m-d H:i:s");
			$updata['isActive']= 1;
			$updata['isDeleted']= 0;
			$packageids="";
			$testIds="";
			$results=array();
		    foreach($details as $detail){ 
			  if($detail->packageId){ 
			    $userId=$detail->userId;
				$packageids.=$detail->packageId.',';
				$where=array('packageId'=>$detail->packageId,'ccavenueOrderId'=>$orderId);
				$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
			   }else if($detail->packageId == 0){ 
				  $userId=$detail->userId;
				  $testIds.=$detail->packageTests.',';
			      $where=array('packageTests'=>$detail->packageTests,'ccavenueOrderId'=>$orderId);
				  $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
			   }
		     }
			 if($results){
				   $where=array('userId'=>$userId);
				   $cartData['isActive']=0;
				   $cartData['isDeleted']=1;
				   $results=$this->insertOrUpdate(TBL_CART,$where,$cartData);
			}
			$totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
			$data['packageDetails']=0;$data['testDetails']=0;
			$packageids=rtrim($packageids,','); 
			$testIds=rtrim($testIds,',');
			if($packageids){
			$data['packageDetails']=$this->Testsmodel->getSelectedPackagesDetails($packageids);
			}
			if($testIds){
			$data['testDetails']=$this->Testsmodel->getSelectedTestDetails($testIds);
			}
			$where=array('userId'=>$userId);
			$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
			$data['userName']=$details['userName'];
			$data['amount']=$this->session->userdata('amount');;
			$subject="Purchase Details";
			$message=$this->load->view('emailDetails', $data,true);
			$smtpEmailSettings = $this->config->item('smtpEmailSettings');
			$email_id=$details['emailAddress'];
			//@$isEmailSent = @sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
			$result['totalAmount']=($totalAmount)?$totalAmount:0;
			$result['message']=PURCHASE_DONE;
			$result['error_message']="";
		   return $result;
		
        }else{
			$result['totalAmount']=0;
			$result['message']="";
			$result['error_message']=INVALID_ORDER_ID;
			return $result;
       
        }
    } 
	/* crypto functions */
	
	
	/* to decrypt ccavenue response */
	function ccdecrypt($encryptedText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=$this->hextobin($encryptedText);
		$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
		mcrypt_generic_deinit($openMode);
		return $decryptedText;
	
	}
	
	
	//********** Hexadecimal to Binary function for php 4.0 version ********
	
	function hextobin($hexString)
	{
		$length = strlen($hexString);
		$binString="";
		$count=0;
		while($count<$length)
		{
			$subString =substr($hexString,$count,2);
			$packedString = pack("H*",$subString);
			if ($count==0)
			{
				$binString=$packedString;
			}
			 
			else
			{
				$binString.=$packedString;
			}
			 
			$count+=2;
		}
		return $binString;
	}
	function getAllRecords($table,$where,$select="*")
	{
		$this->db->select($select);
		if($where){
		$this->db->where($where);
		}
        $query = $this->db->get($table); 
		@$results = $query->result();
	    return $results;
	}
	
	function insertOrUpdate($table,$where=array(),$data)
	{
		
		if($where){ 
			$this->db->where($where);
			$query=$this->db->update($table,$data);
			if (!$query)
			{    
		         return false;
				 //$this->throwException($query);
			}else{
				return true;
			}
			
		}else{
		
			if(!empty($data))
			{
				$this->db->insert($table,$data);
				return $this->db->insert_id();
			}
		}	
			
			return false;
	}
	function getSingleRecord($table,$where,$select="")
	{
		
		if($select){
			$this->db->select($select);
		}else{
			$this->db->select('*');
		}
		$this->db->where($where);
        $query = $this->db->get($table); 
		$results = $query->row_array();
	    return $results;
	}
}