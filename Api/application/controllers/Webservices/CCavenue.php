<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
/**
 * This resource contains the services for the Admin authentication..etc., and other admin related services.
 * 
 * @category	Restful WebService
 * @controller  Admin Controller
 * @author		Ashiya syed
 */

require APPPATH.'/libraries/REST_Controller.php';
class CCavenue extends REST_Controller {
 
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		date_default_timezone_set('Asia/Kolkata');

	}
	function callbackThroughCCAvenue_get(){
		if(@$_POST){
    $workingKey = CCAVENUE_WORKING_KEY;
    $encResponse=@$_POST["encResp"];	 
    //$encResponse="401536b41111e9b4de00e1edfde8b3574fcb393845c3ad2308b27f812fb8661d546c9b12ac1acfd83c7ce80ed022954e81e1af8f272b3af572401e2c143e7d46999c313c2ecad7f5e116321e49d9804ed76523b5b7956ed6541634658e2e2ae234b568b6f6f7e475e974cf1bff84b0af31fc0e138c1c945a30ffd2d8cec687f9acd733494b37149b73911fc3270eb9b225da0a35733dbecbe047e5bee7542110deb43b2963c47d620326f4c7806cdc6eef3575915f351bc948709bd47ccd4b7fc7c601694a59c67783e0e589208bc003a835da9518486160b98c639bb81b468b27069faf9aa430512b2ca2932e0791e86d9ba2ce97ad71e968a357c6446791114b80462fca83b4aa9ccc511398174d2670e5554f6d6736b75958016d8bc8549c4472d2b3d64d4e5caf0df63b7cc8f70fa5662a86ae935c2334a9af72dcb6b9f2e542d8c5a265f766db29659c64014fead4d3284b3dcda1a1f1ff9a2890b1170c21be91b12a8d6bac545a6b4f7e708b9930d9dd2b65e0359b66c0da65d969382b6c427657f8b9326452aae8ff76187eaf78859c9f3d5c0599212099cc6276deb02aa1c144089349f806b3ad0b05c5f594a9737ee910715634df282396e0b696ea8533059ef6a7bde14d3977a7193303da103e0930ce36731b3fa1c2439866c4c6415c38329b9d340d04179a5cae80b1c84b044da2f4d3b1b0ad99e0bfcd2871af86be3d1f735422b0bba66c6cc087c938873e0dacbfd6c51e1a97041e3a69c3556690130a468a661fec4f3602ca6cc828ad62947ba26d39f9f5449ef83eb101cfd290573c63d332492ce2b8bffff8a7c0f2e04a13064254a8f57dccdbe62cdcc1c9c6f95f78fe0a9916a8cb2070ff04a4c2216cdd7cc8a3be8489d25d3e5d4222f597fbd8e441aeb8a0db37ced02abb53651b1735c65d2ce2e055f060ebda1c40d4ff2c5a73f2d5a2cb5ca525bee9f65736c72a694051bce4aa747486bf5e7943a73ff1bc0bce2cc8d4c38be8d1bc09553ca4c738e301fb7ae5f6a226c24fa20db6134a5b31ca96dba8969edd5e46cc0559cc907310143a8d808a02214ff39149d6d0d4a8feaa2c5d030d1ecc7c5f3f5a6582e24eb29f41a444c97b9f0735931791983209fc8133eaf6a802303edf51ffe79e6e722d1c69f289d56bffecff1e019e4a6811ab8e6bf91c261b750cab1b6b";	 
	$rcvdString=$this->ccdecrypt($encResponse, $workingKey);
	$saveEncData['request']='Android';
	$saveEncData['response']=$rcvdString;
	$where=array();
	$this->insertOrUpdate(TBL_CC_CALLBACKS,$where,$saveEncData);
	$order_status="";
    $orderID=0;
	$decryptValues=explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);
	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
		if($i==3)	$order_status=$information[1];
		if($i==0)	$orderID=$information[1];
	}
	if($order_status=="Success"){ 
		$res=$this->saveDataAfterCCavenueSuccess($orderID);
		if($res){
		    $message = array('status' => 1,'message' => $res['message'],'totalCartAmount'=>$res['totalAmount']); 
	    }else{
		    $message = array('status' => 2,'message' =>$res['message'],'totalCartAmount'=>$res['totalAmount']); 
	   }
	   if($res['error_message']){
		   $message = array('status' => 0,'message' =>$res['error_message']); 
	   }
       $this->response($message, 200);
	}elseif($order_status == "Aborted"){
		 $this->saveDataAfterCCavenueSuccess($orderID);
	}else{
		$message = array('status' => 0,'message' => CCAVENUE_AVENUE_ERROR_OCCURED); 
	    $this->response($message, 200);
	}
		}else{
			$message = array('status' => 0,'message' => CCAVENUE_AVENUE_ERROR_OCCURED); 
	        $this->response($message, 200);
		}
	 
}
/* update order id status and remove items from cart */
function saveDataAfterCCavenueSuccess($orderId){ 
	  //$orderId="190021514444176";
	   $details=$this->getAllRecords(TBL_USER_PACKAGE,$where=array('ccavenueOrderId'=>$orderId,'isActive'=>0,'isDeleted'=>0));
		if($orderId && $details){ 
			    
        	    $updata['createdTime']= date("Y-m-d H:i:s");
				$updata['isActive']= 1;
				$updata['isDeleted']= 0;
				$packageids="";
				$testIds="";
				$results=array();
		    foreach($details as $detail){ 
			   //deactivate coupon code
			     $CouponCode=$detail->couponCode;
				$where=array('coupon_code'=>$CouponCode);
				$couponData['isActive']=0;
				$this->insertOrUpdate(TBL_COUPONS,$where,$couponData);          
		       //deactivate user promocode
			   
                $where=array('coupon_code'=>$CouponCode);
		        $coupondetails= $this->getSingleRecord(TBL_COUPONS,$where,'*');	
				 $updata['couponAmount']=$coupondetails['coupon_amount'];
			    $id=$coupondetails['coupon_id'];
			    $promoData['c_is_used']=1;
		        $promoData['c_is_active']=0;
		        $promoData['c_is_deleted']=1;
				$where=array('c_coupon_id'=>$id,'c_user_id'=>$detail->userId);
                $this->insertOrUpdate(TBL_USER_COUPONS,$where,$promoData);
				$totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
                $updata['paidAmount']= $totalAmount;
			   /* end */
			  if($detail->packageId){ 
			    $userId=$detail->userId;
				$packageids.=$detail->packageId.',';
				$where=array('packageId'=>$detail->packageId,'ccavenueOrderId'=>$orderId);
				$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
			   }else if($detail->packageId == 0){ 
				  $userId=$detail->userId;
				  $testIds.=$detail->packageTests.',';
			      $where=array('packageTests'=>$detail->packageTests,'ccavenueOrderId'=>$orderId);
				  $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
			   }
		     }
			 
		         if($results){
					   $where=array('userId'=>$userId);
					   $cartData['isActive']=0;
					   $cartData['isDeleted']=1;
					   $results=$this->insertOrUpdate(TBL_CART,$where,$cartData);
				}
	    //$totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
	    $data['packageDetails']=0;$data['testDetails']=0;
		$packageids=rtrim($packageids,','); 
		$testIds=rtrim($testIds,',');
		if($packageids){
		$data['packageDetails']=$this->Testsmodel->getSelectedPackagesDetails($packageids);
		}
		if($testIds){
		$data['testDetails']=$this->Testsmodel->getSelectedTestDetails($testIds);
		}
		$where=array('userId'=>$userId);
		$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
		$data['userName']=$details['userName'];
		$data['amount']=$totalAmount;
		$subject="Purchase Details";
		$message=$this->load->view('emailDetails', $data,true);
		$smtpEmailSettings = $this->config->item('smtpEmailSettings');
		$email_id=$details['emailAddress'];
		//@$isEmailSent = @sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
		$result['totalAmount']=($totalAmount)?$totalAmount:0;
        $result['message']=PURCHASE_DONE;
        $result['error_message']="";
	   return $result;
		
}else{
	    $result['totalAmount']=0;
        $result['message']="";
        $result['error_message']=INVALID_ORDER_ID;
	    return $result;
       
}
} 
	/* crypto functions */
	
	
	/* to decrypt ccavenue response */
	function ccdecrypt($encryptedText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=$this->hextobin($encryptedText);
		$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
		mcrypt_generic_deinit($openMode);
		return $decryptedText;
	
	}
	
	
	//********** Hexadecimal to Binary function for php 4.0 version ********
	
	function hextobin($hexString)
	{
		$length = strlen($hexString);
		$binString="";
		$count=0;
		while($count<$length)
		{
			$subString =substr($hexString,$count,2);
			$packedString = pack("H*",$subString);
			if ($count==0)
			{
				$binString=$packedString;
			}
			 
			else
			{
				$binString.=$packedString;
			}
			 
			$count+=2;
		}
		return $binString;
	}
	  
	
}

?>