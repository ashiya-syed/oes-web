<?php defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

/**
 * This resource contains the services for the Admin authentication..etc., and other admin related services.
 * 
 * @category	Restful WebService
 * @controller  Admin Controller
 * @author		SharthReddy
 */

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Admin extends REST_Controller
{
	public function __construct() {
		
        parent::__construct();
         $this->load->model('Admin_model', 'admin');
         //$this->load->model('Master_model', 'master');
	}
	
	/**
	 * This service will check whether the logged in admin is valid or not.
	 * 
	 * @serviceType POST
	 * @param string email_or_phone
	 * @param string password
	 * @responseType JSON
	 */
	
	function signin_post(){
	    $this->load->library('form_validation');
	    $this->form_validation->set_rules('email_or_phone','Phone Number','trim|required|min_length[1]');
		$this->form_validation->set_rules('password','Password','trim|required|min_length[1]');
		  
		if($this->form_validation->run()!=false)
		{     
			   $email_or_phone = trim($this->post('email_or_phone')); 
			   $password = trim($this->post('password'));
			   $result=$this->admin->login($email_or_phone,$password);
			   
			   if($result){
				 $message = array('status' => 1,'message' => LOGIN_SUCCESS,'result' => $result);
			     $this->response($message, 200);
			    }else{
			         $error_message = array('status' => 0, 'message' => EMAIL_OR_PHONE_AND_PASSWORD_NOT_MATCH,'result' => '');
				    $this->response($error_message, 200);
				}
		}
		else
		{
			$message = array('status' => 0,'message' =>strip_tags(validation_errors()),'result' => '');
			$this->response($message, 200);
		}
	  }
	  
}