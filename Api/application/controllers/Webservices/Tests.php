<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
/**
 * This resource contains the services for the Admin authentication..etc., and other admin related services.
 * 
 * @category	Restful WebService
 * @controller  Admin Controller
 * @author		Ashiya syed
 */

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
class Tests extends REST_Controller {
 
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		$this->load->model('Usermodel'); 
       date_default_timezone_set('Asia/Kolkata');

	}
	function getAllTestsInSignup_get(){
	    $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
		$where_in=trim($this->input->get('courseIds')); 
		$tests = $this->getAllRecordsBasedOnList(TBL_TESTS,$where,$where_in,'course_id','testId,testName,testAmount,course_id');//print_r($this->db->last_query());die();
            if(empty($where_in)){
			$message = $this->response(array('status' => 5, 'message' => "Please select course."),200);die();
		   }
		   if($tests){
			     $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $tests);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
			 $this->response($message, 200);		
	}
	function getAllCoursesInSignup_get(){
		$resellerId=(@$_GET['resellerId'])?(@$_GET['resellerId']):0;
		$resellerDetails=$this->getSingleRecord(TBL_RESELLERS,array('reseller_id'=>$resellerId),'*');
		 $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
		if($resellerDetails && $resellerDetails['isReseller'] == 2){
			$where = array('isActive'=>1,'isDeleted'=>0);
		}
	   
		$tests = $this->getAllRecords(TBL_COURSES,$where,'*');
            if($tests){
			     $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $tests);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
			 $this->response($message, 200);		
	}
	function getAllPackagesInSignUp_get(){
		$where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>0);
		$courseIds=trim($this->input->get('courseIds')); 
		if(empty($courseIds)){
			$message = $this->response(array('status' => 5, 'message' => "Please select course."),200);die();
		}
		$packages = $this->getAllRecordsBasedOnList(TBL_PACKAGES,$where,$courseIds,'courseId','package_id,package_name,package_amount,courseId');
		 if($packages){
			     $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $packages);
		 }else{
			 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
		}
	    $this->response($message, 200);	
	}       
		   
		   
		
	/*******************
	********************
	This method is useful to 
	GET PACKAGES for purchasing
	********************
	********************/
	
	function optedTests_get(){
		 $token=@$this->get('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		 $userId=@$this->get('userId');
		  if( empty($userId) )
			{
				$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();
			}
			$offset = DEFAULT_LIMIT;
		    $start=@$_GET['start'];
			$testType=@$this->get('testType');
			 if( empty($testType) )
			{
				$this->response(array('validation_error' => 'testType is required.'), 200); die();
			}
	        $optedTests= $this->Testsmodel->getOptedTests($testType,$userId,$start,$offset);
			 if($optedTests){
			     $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $optedTests,'offset'=>$offset);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
			 $this->response($message, 200);

	}
	
	function getAttemptsRecordsbyTest_get(){
		 $token=@$this->get('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		$userPackageID=0;$offset = DEFAULT_LIMIT;
		$id=@$this->get('id'); 
		$where=array('id'=>$id);
		$testAttemptDetail=$this->getSingleRecord(TBL_TIMES,$where,'*'); 
		$start=@$this->get('start'); 
		if( empty($id) ){$this->response(array('status'=>0,'message' =>'id is required.'), 200); die();}
		$testId=$testAttemptDetail['testId'];
		$userId=$testAttemptDetail['userId'];
		$userPackageID=$testAttemptDetail['userPackageID'];
		//$where=array('testId'=>$testId,'userId'=>$userId,'userPackageID'=>$userPackageID,'isActive'=>1,'status'=>3);
		$where=array('testId'=>$testId,'userId'=>$userId,'isActive'=>1,'status'=>3);
		$select="id,testId,packageId,testType,attempts,status,createdTime";
        $atempts=$this->getOffsetRecords(TBL_TIMES,$where,$select=$select,$start,$offset);
	    if($atempts){
			    $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $atempts);
		}else{
				$message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
		}
			 $this->response($message, 200);
	}
	
	function viewExamReportByAttempt_get(){ 
	 $token=@$this->get('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		$id=@$this->get('id'); 
		$userId=@$this->get('userId'); 
		if( empty($id) ){$this->response(array('status'=>0,'message' => 'id is required.'), 200); die();}
		if( empty($userId) ){$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();}
		$where=array('id'=>$id);
        $atempts=$this->getSingleRecord(TBL_TIMES,$where,$select='*');
		$testId=$atempts['testId']; 
		$testType=$atempts['testType'];
		$attempts=$atempts['attempts'];
		$packageiD=$atempts['packageId'];
		$userPackageId=$atempts['userPackageID']; //print_r($testType);die();
		$result=$this->Testsmodel->getExamResults($testId,$testType,$attempts,$packageiD,$userPackageId,$userId);
		$timeTakn=$this->secondsToTime($result['diff']*60); 
		 $result['timeTaken']= $timeTakn;
		 $result['testTime']= $result['duration'] ." min.";
		 $idleTime=abs(($result['duration']*60)-($result['diff']*60));
	     $idlTime=$this->secondsToTime($idleTime);
	     $result['idleTime']= $idlTime;
		 unset($result['diff']);
		if($result){
			$final[]=(object)array('value'=>$result['testName'],'name'=>'Test Name');
			$final[]=(object)array('value'=>$result['totalQue'],'name'=>'Total No. of Questions');
			$final[]=(object)array('value'=>$result['testMarks'],'name'=>'Total Marks for this Test');
			$final[]=(object)array('value'=>$result['rightMarks'],'name'=>'Right Marks');
			$final[]=(object)array('value'=>$result['negativeMarks'],'name'=>'Negative Marks');
			$final[]=(object)array('value'=>$result['testTime'],'name'=>'Total Time of Test');
			$final[]=(object)array('value'=>$result['attemptedQuestions'],'name'=>'My Questions in Test');
			$final[]=(object)array('value'=>$result['leftQuestions'],'name'=>'Left Questions');
			$final[]=(object)array('value'=>$result['leftQueMarks'],'name'=>'Left Question Marks');
			$final[]=(object)array('value'=>$result['correctAns'],'name'=>'Correct Answers');
			$final[]=(object)array('value'=>$result['incorrectAns'],'name'=>'Incorrect Answers');
			$final[]=(object)array('value'=>$result['myMarks'],'name'=>'My Marks');
			$final[]=(object)array('value'=>$result['percentile'],'name'=>'My Percentile');
			$final[]=(object)array('value'=>$result['timeTaken'],'name'=>'My Time');
			$final[]=(object)array('value'=>$result['idleTime'],'name'=>'Unproductive Time');
			$final[]=(object)array('value'=>$result['idleTime'],'name'=>'Idle Time');
			$message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $final);
		}else{
				$message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
		}
			 $this->response($message, 200);
	}
	function secondsToTime($seconds) {
	  $time="";
      $hours = floor($seconds / (60 * 60));
	  $divisor_for_minutes = $seconds % (60 * 60);
	  $minutes = floor($divisor_for_minutes / 60);
	  $divisor_for_seconds = $divisor_for_minutes % 60;
	  $seconds = ceil($divisor_for_seconds);
	  $obj = array(
			"h" => (int) $hours,
			"m" => (int) $minutes,
			"s" => (int) $seconds,
		); 
		if($obj){
			$h=$obj['h'];
			$m=$obj['m'];
			$s=$obj['s'];
			$time= $h.'h '. $m .'m '. $s.'s ';
		}
	return $time;
	}
	function freeTestsInHome_get(){
		 $token=@$this->get('token');
		  $newTEST=array();
		   if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
			
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		    $data = array();$resellerId=0;$data['practicalTests']=array();
			$start=(@$this->get('start'))?(@$this->get('start')):0; 
			$resellerId=@$this->get('resellerId'); 
			$userId=@$this->get('userId'); 
		    if( empty($userId) ){$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();}
			$offset = DEFAULT_LIMIT;
			$userdetails= $this->Usermodel->profile($userId);
			$data['practicalTests']= $this->Testsmodel->getFreeTests($resellerId,$userId,$userdetails->course); 
			if($data['practicalTests']){
			for($i=0;$i<count($data['practicalTests']);$i++)
			{
				$data['practicalTests'][$i]->isRedoTest=0;
				$testId=$data['practicalTests'][$i]->testId;
				$packageId=$data['practicalTests'][$i]->packageId;
				$uniqueId=$data['practicalTests'][$i]->uniqueId;
				$where=array('testId'=>$testId,'userId'=>$userId,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'status'=>TEST_COMPLETED,'isActive'=>1);
				$count=$this->getAllRecords(TBL_TIMES,$where,$select="status");//completed tests times count
				if($count){
				$data['practicalTests'][$i]->count=count($count);
				}else{$data['practicalTests'][$i]->count=0;}
				$where=array('testId'=>$testId,'userId'=>$userId,'userPackageID'=>$uniqueId,'packageId'=>$packageId,'isActive'=>1);
				$attempts=$this->Testsmodel->getNumberOfAttempts(TBL_TIMES,$where);
				if($attempts){
				$data['practicalTests'][$i]->attempted=$attempts->attempts;
				}else{$data['practicalTests'][$i]->attempted=0;}
				$presentSatus=$this->Testsmodel->getLatestStatus($testId, $userId, $uniqueId);//echo"<pre>";print_r($presentSatus);die();
				if($presentSatus){
					if($presentSatus->status == 2 || ($presentSatus->status == 0)){$data['practicalTests'][$i]->isRedoTest=1;}
				}
			}
		}
		$tests=$data['practicalTests'];
			if($tests){ 
			 for($i=$start;$i<count($tests);$i++){
				  if(!empty($tests[$i])){
					if(count($newTEST) < $offset){ @$newTEST[]=$tests[$i]; }
					 }
			    } 
			 }  
			$data['practicalTests']=$newTEST;
		if(@$data['practicalTests']){
			 $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $data,'offset'=>$offset);
		}else{
				$message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' =>$data);
		}
		 $this->response($message, 200);
	}
	function homePaidTests_get(){
		 $token=@$this->get('token');
		  $newTEST=array();
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		    $data = array();$resellerId=0;$start=0;
			$start=@$this->get('start'); 
			$userId=@$this->get('userId'); 
		    if( empty($userId) ){$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();}
			$offset = DEFAULT_LIMIT;
			$tests= $this->Testsmodel->getPaidTests($userId); 
			 if($start){$i=$start;}else{$i=0;} 
		      
		    $data['mainTests']=$tests;
			if($data['mainTests']){
			for($i=0;$i<count($data['mainTests']);$i++)
			{ 
				$data['mainTests'][$i]->isRedoTest=0;
				$testId=$data['mainTests'][$i]->testId;
				$packageId=$data['mainTests'][$i]->packageId;
				$uniqueId=$data['mainTests'][$i]->uniqueId;
				$where=array('testId'=>$testId,'userId'=>$userId,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'status'=>TEST_COMPLETED,'isActive'=>1);
				$count=$this->getAllRecords(TBL_TIMES,$where,$select="status");
				if($count){
				$data['mainTests'][$i]->count=count($count);
				}else{$data['mainTests'][$i]->count=0;}
				$where=array('testId'=>$testId,'userId'=>$userId,'userPackageID'=>$uniqueId,'packageId'=>$packageId,'isActive'=>1);
				$attempts=$this->Testsmodel->getNumberOfAttempts(TBL_TIMES,$where);
				if($attempts){
				$data['mainTests'][$i]->attempted=$attempts->attempts;
				}else{$data['mainTests'][$i]->attempted=0;}
				$presentSatus=$this->Testsmodel->getLatestStatus($testId, $userId, $uniqueId);
				/* if($presentSatus){
					if($presentSatus->status == 2){$data['mainTests'][$i]->isRedoTest=1;}
				} */
				if($presentSatus){
					if($presentSatus->status == 2 || ($presentSatus->status == 0)){$data['mainTests'][$i]->isRedoTest=1;}
				}
			}
			}
			if($data['mainTests']){ 
				$newTEST=array();
				for($i=$start;$i<count($data['mainTests']);$i++){
					if(!empty($data['mainTests'][$i])){
						if(count($newTEST)<10){@$newTEST[]=$data['mainTests'][$i];}
						
					  }
					} 
				 }$data['mainTests']=$newTEST;
			if($data['mainTests']){
				
			    $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $data);
			}else{
				$message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' =>$data);
			}
			 $this->response($message, 200);
	}
	
	
	function coursesInLandingPage_get(){
		$resellerId=(@$_GET['resellerId'])?(@$_GET['resellerId']):0;
		$resellerDetails=$this->getSingleRecord(TBL_RESELLERS,array('reseller_id'=>$resellerId),'*');
		 $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
		if($resellerDetails && $resellerDetails['isReseller'] == 2){
			$where = array('isActive'=>1,'isDeleted'=>0);
		}
	   
		$courses = $this->getAllRecords(TBL_COURSES,$where,'*');
            if($courses){
				foreach($courses as $course){
					$courseDATA=array();
					$courseId=$course->courseId;
					$coursedetails=$this->Testsmodel->getCourseTestOrPackage($courseId,$resellerId);
					if($coursedetails){
						$courseDATA[]=(object)array('value'=>$course->courseDescription,'name'=>'description');
						$courseDATA[]=(object)array('value'=>"This course contains ".$coursedetails['testCount']." tests",'name'=>'tests');
						$courseDATA[]=(object)array('value'=>"This course contains ".$coursedetails['packCount']." packages",'name'=>'packages');
						}
					 $course->details=$courseDATA;
					
				}
				$analytics=$this->getAnalytics($resellerId);
			     $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $courses,'analyticsdata'=>$analytics);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
			 $this->response($message, 200);		
	}
	function getAnalytics($resellerId){
		      $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId,'roleID'=>2);
              $usersCount = $this->getAllRecords(TBL_USERS,$where,'count(*) as users_count');
			  if($usersCount[0]){
				  $IMG=HOME_IMAGE_PATH.'users.png';
		      $data[] = (object)array('value'=>$usersCount[0]->users_count,'name'=>'usersCount','image'=>$IMG);
			  }
		      $where = array('isActive'=>1,'isDeleted'=>0,'resellerId'=>$resellerId);
              $testsCount = $this->getAllRecords(TBL_TESTS,$where,'count(*) as tests_count');
			  if($testsCount[0]){
				  $IMG=HOME_IMAGE_PATH.'tests.png';
		      $data[] = (object)array('value'=>$testsCount[0]->tests_count,'name'=>'testsCount','image'=>$IMG);
			  }
              $packagesCount = $this->getAllRecords(TBL_PACKAGES,$where,'count(*) as packagesCount');
			  if($packagesCount[0]){
				  $IMG=HOME_IMAGE_PATH.'packages.png';
		       $data[] = (object)array('value'=>$packagesCount[0]->packagesCount,'name'=>'packagesCount','image'=>$IMG);
			  }
			  $questionsCount = $this->getAllRecords('oes_quetions_total',$where,'count(*) as que_count');
			  if($questionsCount[0]){
				  $IMG=HOME_IMAGE_PATH.'questions.png';
		          $data[] = (object)array('value'=>$questionsCount[0]->que_count,'name'=>'questionsCount','image'=>$IMG);
			  }
			  return $data;
	}
	
}

?>