<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
/**
 * This resource contains the services for the Admin authentication..etc., and other admin related services.
 * 
 * @category	Restful WebService
 * @controller  Admin Controller
 * @author		Ashiya syed
 */

//This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
class Users extends REST_Controller {
 
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		//$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		$this->load->model('Usermodel'); 
        date_default_timezone_set('Asia/Kolkata');

	}
	/*******************
	********************
	This method is useful to 
      to register user	
	********************
	********************/
	function signup_post(){
		 try{
			
					$this->load->library('form_validation');
					$this->form_validation->set_rules('email','email','trim|required|valid_email');
					$this->form_validation->set_rules('name','name','trim|required|min_length[3]|max_length[15]');
					$this->form_validation->set_rules('password','Password','trim|required|min_length[6]|max_length[20]');
					$this->form_validation->set_rules('mobile','Phone Number','trim|required|min_length[10]|max_length[10]');
					$this->form_validation->set_rules('otp','Otp','trim|required');
					$this->form_validation->set_rules('courseIds','courseIds','trim|required');
					  /* verify otp is valid or not */
                      $otp = trim($this->input->post('otp'));
					  $mobile = trim($this->input->post('mobile'));
					  $where=array('token'=>$otp,'phoneNumber'=>$mobile,'isactive'=>1,'purpose'=>'registration');
					  $checkOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*"); 
					   if(empty($checkOtp)){
						  $this->response(array('status' => 0,'message' => INVALID_OTP), 200); die();
						}
						/* end */
					    if($this->form_validation->run()!=false)
						{
							$email = trim($this->input->post('email'));
							$name = trim($this->input->post('name'));
							$password = trim($this->input->post('password'));
							$cpassword = trim($this->input->post('cpassword'));
							$phoneNumber = trim($this->input->post('mobile'));
							$courseIds = $this->input->post('courseIds');
							$test = $this->input->post('test');
                            $package = $this->input->post('package');
                           if(empty($package) && empty($test)){
								
								 $message = array('status' => 0, 'message' => SELECT_ATLEAST_ONE_PACK_OR_TEST); 
							     $this->response($message, 200);
							} 
							$roleID = USER_ROLE_ID;
							$token=md5(uniqid());
							$pwd=encrypt($password);
							if($test){
							    $data = array('userName' => $name , 'emailAddress' => $email, 'phoneNumber' => $phoneNumber, 'password' =>$pwd,'roleID'=>$roleID,'token'=>$token,'testIds'=>$test,'course'=>$courseIds);
							}
							if($package){
								$data = array('userName' => $name , 'emailAddress' => $email, 'phoneNumber' => $phoneNumber, 'password' => $pwd,'roleID'=>$roleID,'token'=>$token,'packageIds'=>$package,'course'=>$courseIds);
							}
							if($test && $package){
							    $data = array('userName' => $name , 'emailAddress' => $email, 'phoneNumber' => $phoneNumber, 'password' => $pwd,'roleID'=>$roleID,'token'=>$token,'testIds'=>$test,'packageIds'=>$package,'course'=>$courseIds);
							}
							$details=$this->Usermodel->user_exists($phoneNumber,$email);
							if($details && $details->isPromotionMember == 0){
								$message = array('status' => 0, 'message' => PHONE_NUMBER_OR_EMAIL_ALREADY_EXISTS); 
							    $this->response($message, 200);
								
							}else{ 
								if($details && $details->isPromotionMember == 1){
									$where1=array('userId'=>$details->userId);
									$data['isPromotionMember']= 0;
									$data['is_user_verified']= 1; 
									$inputdata=$data;
									$result=$this->insertOrUpdate(TBL_USERS,$where1,$inputdata);
								}else{ 
									$data['is_user_verified']= 1; 
									$result=$this->Usermodel->register($data);	 
								}
							   if($result) 
								 { 
							 /* need to remove after sending mails */
								  if($test){   
									$where=array();
									$updata['createdTime']= date("Y-m-d H:i:s");
									$updata['userId']=$result;
									$updata['packageId']=0;
									$updata['isActive']= 1;
									$updata['isDeleted']= 0;
									$updata['packageTests']= $test;
									$updata['couponCode']= 0;
									$updata['txnid']= 0;
									$updata['amount']= 0;
									$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
									
								}
								 if($package){
									 $packageArray=explode(',',$package);
									 foreach($packageArray as $packary){
									$pId=$packary;
									$where=array('package_id'=>$pId);
									$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
									$testId=$packageDetails['package_test'];
									$where=array();
									$updata['createdTime']= date("Y-m-d H:i:s");
									$updata['userId']=$result;
									$updata['packageId']=$pId;
									$updata['isActive']= 1;
									$updata['packageTests']=$testId;
									$updata['amount']= 0;
									$updata['couponCode']= 0;
									$updata['txnid']= 0;
									$updata['isDeleted']= 0;
									$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
									   }
									}
					                $where=array('token'=>$otp,'phoneNumber'=>$mobile,'isactive'=>1,'purpose'=>'registration');
									$otpdata['isactive']=0;
					                $checkOtp=$this->insertOrUpdate(TBL_OTPS,$where,$otpdata);
									
									/* need to remove after sending mails */
									/* to send mail after registration */
									$details= $this->Usermodel->profile($result);
								   $subject="Verify your Account";
								  $url=FSITEURL.'Account/verifyAccount?token='.$token;
								  $emailData['url']=$url;
								  $emailData['name']=$name;
								  $emailData['email']=$email;
								  $emailData['password']=$password;
								  $emailData['phoneNumber']=$phoneNumber;
								  $message=$this->load->view('verifyEmail', $emailData,true);
								  $email_id=$email;
		                          $smtpEmailSettings = $this->config->item('smtpEmailSettings');
		                          //$isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message)
									/* end */
		                            $message = array('status' => 1,'message' => 'Please verify your email id to login into your account.','result'=>$details);
									// $message = array('status' => 1,'message' => USER_REGISTRATION_SUCCESS,'result'=>$details); 
								  
								 }else{
									  $message = array('status' => 1,'message' => USER_REGISTRATION_FAILED); 
								 } 
							}
						}
						else {
							 $message = array('status' => 0,'message' => strip_tags(validation_errors()));
					         
						}
			$this->response($message, 200);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
		
	}
	 function verifyAccount_post(){
		 
		  $token = @$_REQUEST['token'];
		  $this->session->set_userdata('verifyToken',$token);
		  $tokenDetails = $this->Usermodel->isValidToken(trim($token));
		   if(empty($tokenDetails)){
			  $message = array('status' => 0,'message' => INVALID_OTP);
			  $this->response($message, 200);
			  
		  }
		    $where=array('token'=>trim($token));
			$res=$this->getSingleRecord(TBL_USERS,$where,'userId,userName,phoneNumber,emailAddress,testIds,packageIds,is_user_verified,createdTime,accountExpiryDate,resellerId,accountType');
			$data['userdetails']=$res; 
			$accountType=$res['accountType'];
			
			if($accountType == 2){ 
				$up=$this->Usermodel->UpdateUserVerified(trim($token));
				$message = array('status' => 1,'message' => ACCOUNT_VERIFY_SUCCESS,'result'=>$res);
			    $this->response($message, 200);
				
			}else{ 
				$testIDs=$res['testIds'];
				$packageIds=$res['packageIds'];
				$testIDs=$res['testIds'];
				$packageIds=$res['packageIds'];
				if($testIDs){ 
				    $where=array();
					$updata['createdTime']= date("Y-m-d H:i:s");
					$updata['userId']=$res['userId'];
					$updata['packageId']=0;
					$updata['isActive']= 1;
					$updata['isDeleted']= 0;
					$updata['packageTests']= $testIDs;
					$updata['couponCode']= 0;
					$updata['txnid']= 0;
					$updata['amount']= 0;
                    $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
					
				}
				if($packageIds){
		             $packageArray=explode(',',$packageIds);
					 foreach($packageArray as $packary){
					$pId=$packary;
					$where=array('package_id'=>$pId);
					$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
					$testId=$packageDetails->package_test;
					$where=array();
					$updata['createdTime']= date("Y-m-d H:i:s");
					$updata['userId']=$res->userId;
					$updata['packageId']=$pId;
					$updata['isActive']= 1;
					$updata['packageTests']=$testId;
					$updata['amount']= 0;
					$updata['couponCode']= 0;
					$updata['txnid']= 0;
					$updata['isDeleted']= 0;
					$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
					   }
					}
				$up=$this->Usermodel->UpdateUserVerified(trim($token));
				if($up){
				  $message = array('status' => 1,'message' => ACCOUNT_VERIFY_SUCCESS,'result'=>$res);
				}else{
				  $message = array('status' => 1,'message' => ACCOUNT_VERIFY_SUCCESS,'result'=>$res);
				}
				 $this->response($message, 200);

            }
		}
	/*******************
	********************
	This method is useful to 
      to get profile	
	********************
	********************/
	         function sendOtpInRegistration_post(){
		          $email=$this->input->post('email');
				  $phoneNumber=$this->input->post('mobile');
				   if(empty($phoneNumber) || empty($email))
				   {
					 $this->response(array('status'=>0,'message' => 'Mobile number and email are required.'), 200); die();
				   }
                  $where=array('emailAddress'=>$email,'isActive'=>1,'isDeleted'=>0,'resellerId'=>0,'isPromotionMember'=>0);
		          $emailExists = $this->getAllRecords(TBL_USERS,$where=array('emailAddress'=>$email,'isActive'=>1,'isDeleted'=>0,'resellerId'=>0,'isPromotionMember'=>0));
 		          $MobileExists= $this->getAllRecords(TBL_USERS,$where=array('phoneNumber'=>$phoneNumber,'isActive'=>1,'isDeleted'=>0,'resellerId'=>0,'isPromotionMember'=>0));
				  if($emailExists && empty($MobileExists)){
					  $this->response(array('status' => 0,'message' => USER_EMAIL_EXISTS), 200); die();
				  }else if($emailExists && $MobileExists){
					   $this->response(array('status' => 0,'message' => PHONE_NUMBER_OR_EMAIL_ALREADY_EXISTS), 200); die();
				  }else if(empty($emailExists) && $MobileExists){
					   $this->response(array('status' => 0,'message' => PHONE_NUMBER_ALREADY_EXISTS), 200); die();
				  }
				
		        $token=generateRandNumber(6);
				$to[] = array('to'=>$phoneNumber);
				$smsText = "Please use this otp for validate your account with onyx educationals ".$token; 
                $response = sendOTP($smsText, $phoneNumber);
				$where=array('phoneNumber'=>$phoneNumber,'isActive'=>1);
				$checkUserMultipleotp=$this->getAllRecords(TBL_OTPS,$where);
				if($checkUserMultipleotp){
					$where=array('phoneNumber'=>$phoneNumber,'isactive'=>1);
					$udata['isactive']=0;
					$this->insertOrUpdate(TBL_OTPS,$where,$udata);
				}
				$purpose="registration";
				$Rdata['phoneNumber']=$phoneNumber;
				$Rdata['token']=$token;
				$Rdata['purpose']=$purpose;
				$Rdata['isactive']=1;
				$where=array();
			    $res=$this->insertOrUpdate(TBL_OTPS,$where,$Rdata);//print_r($this->db->last_query());die();
				if($res){
					 $message = array('status' => 1,'message' => CHECK_YOUR_MOBILE_FOR_OTP);
				}else{
					 $message = array('status' => 0,'message' => SENDING_OTP_FAILED);
				}
				$this->response($message, 200);
			  
	}
	function checkRegistrationOtp_post(){
		  $otp = trim($this->input->post('otp'));
		  $mobile = trim($this->input->post('mobile'));
		  $where=array('token'=>$otp,'phoneNumber'=>$mobile,'isactive'=>1,'purpose'=>'registration');
		  $checkOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*");
			if(count($checkOtp)>0){
			  $message = array('status' => 1,'message' => SUCCESS);
			}else{ 
			  $message = array('status' => 0,'message' => INVALID_OTP);
			}
		$this->response($message, 200);                 
	 }
	
	/*******************
	********************
	This method is useful to 
      to get profile	
	********************
	********************/
	function login_post(){
		
		try{
				   
					$data=array();
				    $this->load->library('form_validation');
					$this->form_validation->set_rules('email_or_phone','Email or Phone Number','trim|required');
					$this->form_validation->set_rules('password','Password','trim|required');
					$this->form_validation->set_rules('deviceDetails','Device Details','trim|required');
					$this->form_validation->set_rules('deviceId','Device Id','trim|required');
					$this->form_validation->set_rules('fcm_token','Fcm token','trim|required');
					if($this->form_validation->run()!=false)
					{
						$logoutFromAllDevice = trim($this->input->post('logoutFromAllDevice'));
						$deviceDetails = trim($this->input->post('deviceDetails'));
						$Email_OR_phone = trim($this->input->post('email_or_phone'));
						$password       = trim($this->input->post('password'));
						$resellerId       = trim($this->input->post('resellerId'))?trim($this->input->post('resellerId')):0;
						$result = $this->Usermodel->login($Email_OR_phone, $password,$resellerId);
						if($result) 
					 	{  
					      $userId=$result->userId;
					      /* check user login in other device */
						if($logoutFromAllDevice == 1){
								$trackData['userID']=$userId;
								$trackData['isActive']=0;
		                        $this->insertOrUpdate(TBL_USER_TRACK,$where=array('userID'=>$userId),$trackData);
							}
							$checkUserLogedInOtherDevice=$this->Usermodel->checkUserLogedInOtherDevice($userId);
						if($checkUserLogedInOtherDevice && $checkUserLogedInOtherDevice->isActive==1){
							$mes="You already logged in device ".$checkUserLogedInOtherDevice->browser_or_device_details.".Please logout from that device to login.";
						    $message = array('status' => 2, 'message' => $mes); 
							$this->response($message, 200);
						}
					       $exp=strtotime($result->accountExpiryDate);
				            if($result->is_user_verified == 0){
							   $message = array('status' => 0, 'message' => VERIFY_ACCOUNT_LOGIN); 
							   $this->response($message, 200);
					         }else{
								 if($result->isActive == 0){
									$message = array('status' => 0, 'message' => CONTACT_ADMIN); 
							        $this->response($message, 200);
								 }
						      if($exp){
								  
								if($result->accountExpiryDate>$result->createdTime)
								{    
							       $deviceId = trim($this->input->post('deviceId'));
							 	   $fcm_tokem = trim($this->input->post('fcm_token'));
                                    $this->saveFcmToken($userId,$deviceId,$fcm_tokem);
							         //$this->saveLoginData($userId);
							         $details= $this->Usermodel->profile($userId);
									 $saveToken=$this->saveTrackingData($userId,$deviceDetails);
                                     $message = array('status' => 1, 'message' => LOGIN_SUCCESS,'details'=>$details,'token'=>$saveToken); 
							         $this->response($message, 200);
							     }else{
									  $message = array('status' => 0, 'message' => ACCOUNT_EXPIRED); 
							          $this->response($message, 200);
								}
						    
					         }else{ 
							 	 $deviceId = trim($this->input->post('deviceId'));
							 	 $fcm_tokem = trim($this->input->post('fcm_token'));
                                 $this->saveFcmToken($userId,$deviceId,$fcm_tokem);
								 $details= $this->Usermodel->profile($userId);
								 $saveToken=$this->saveTrackingData($userId,$deviceDetails);
                                 $message = array('status' => 1, 'message' => LOGIN_SUCCESS,'details'=>$details,'token'=>$saveToken); 
							     $this->response($message, 200);
							 }
						   }
						} 
						else 
						{
							$message = array('status' => 0, 'message' => EMAIL_OR_PHONE_AND_PASSWORD_NOT_MATCH); 
							$this->response($message, 200);
						}
					}
					else {
						$message = array('status' => 0,'message' => strip_tags(validation_errors()));
					    $this->response($message, 200);
					}
					
		}catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		 } 
	}
	function saveFcmToken($userId,$deviceId,$token){
		    $tokendata['fcm_token']=$token;
			if($userId){
				$tokendata['userId']=$userId;
            }
			$tokendata['deviceId']=$deviceId;
			$inactive['isActive']=0;
			$res=$this->insertOrUpdate('oes_fcm_tokens',array('deviceId'=>$deviceId,'isActive'=>1,'isDeleted'=>0),$inactive);
			if($userId){
				$where=array('userId'=>$userId,'deviceId'=>$deviceId,'isDeleted'=>0);
			}else{
				$where=array('deviceId'=>$deviceId,'isDeleted'=>0);
			}
		$results=$this->getSingleRecord('oes_fcm_tokens',$where,'*');
		if($results){
			$where=$where;
		}else{
			$where=array();
		}
		$tokendata['isActive']=1;
		$res=$this->insertOrUpdate('oes_fcm_tokens',$where,$tokendata);
		if($res){
			return true;
		}else{
			return false;
		}
		
	}
	
	function updateFcmToken_post(){
		 $this->load->library('form_validation');
		$this->form_validation->set_rules('deviceId','Device Id','trim|required');
		$this->form_validation->set_rules('fcm_token','Fcm token','trim|required');
		//$this->form_validation->set_rules('userId','userId','trim|required');
			if($this->form_validation->run()!=false)
			{
				$deviceId = trim($this->input->post('deviceId'));
				$fcm_tokem = trim($this->input->post('fcm_token'));
				$userId = trim($this->input->post('userId'));
				$result=$this->saveFcmToken($userId,$deviceId,$fcm_tokem);
				if($result){
					$message = array('status' => 1,'message' =>SUCCESS);
				}else{
					$message = array('status' => 0,'message' => FAILED);
				}
			}else{
				$message = array('status' => 0,'message' => strip_tags(validation_errors()));
			}
			$this->response($message, 200);

			
	}
	/* function checkUserLogedInOtherDevice($userId){
		$exists=$this->Usermodel->checkUserLogedInOtherDevice($userId);
		
	} */
	function saveTrackingData($userId,$deviceDetails){
		$where=array('userId'=>$userId);
		$loginUpdate['lastLogin']=date("Y-m-d H:i:s");
		$RES=$this->insertOrUpdate(TBL_USERS,$where,$loginUpdate);
		$token=generateRandNumber(6); 
		$trackData['userId']=$userId;
		$trackData['ipAddresss']=$this->input->ip_address();
		$trackData['browser_or_device_details']=$deviceDetails;
		$trackData['createdTime']=date("Y-m-d H:i:s");
		$trackData['token']=$token;
		$res=$this->insertOrUpdate(TBL_USER_TRACK,$where=array(),$trackData);
		if($res){
			return $token;
		}else{
			return "";
		}
							 
	}
	/* function saveLoginData($userId){
		$where=array('userId'=>$userId);
		$loginUpdate['lastLogin']=date("Y-m-d H:i:s");
		$RES=$this->insertOrUpdate(TBL_USERS,$where,$loginUpdate);
		$trackData['userId']=$userId;
		$trackData['ipAddresss']=$this->input->ip_address();
		$trackData['createdTime']=date("Y-m-d H:i:s");
		$this->insertOrUpdate(TBL_USER_TRACK,$where=array(),$trackData);
	} */
	
	/*******************
	********************
	This method is useful to 
      to get profile	
	********************
	********************/
	
	function profile_get(){
		 $userId=@$this->get('userId');
		 $token=@$this->get('token');
		  if(empty((int)$userId) || empty($token))
			{
				$this->response(array('status'=>0,'message' => 'userId and token are required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		  $details= $this->Usermodel->profile($userId);
			 if($details){
			     $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $details);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
			 $this->response($message, 200);

	}
	/*******************
	********************
	This method is useful to 
      to update profile	
	********************
	********************/
	
	function updateProfile_post(){
	   $details=array();
		 try{	 
			    $this->load->library('form_validation');
				$this->form_validation->set_rules('address','Address','trim|required');
				$this->form_validation->set_rules('userId','userId','trim|required');
				$this->form_validation->set_rules('token','token','trim|required');
			if($this->form_validation->run()!=false)
			{
				$wrongToken= $this->checkTokenValid($this->input->post('token'));
				   if($wrongToken){
						$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
				   }
				if($this->input->post('name')){$name= trim($this->input->post('name'));$updateData['userName']=$name;}
				if($this->input->post('address')){$address= trim($this->input->post('address')); $updateData['address']=$address;}
				if($this->input->post('gender')){$gender= trim($this->input->post('gender'));  $updateData['gender']=$gender;}
				if($this->input->post('zipCode')){$zipCode= trim($this->input->post('zipCode'));  $updateData['zipCode']=$zipCode;}
			      $Image=$this->input->post('profilePic');
				  $userId=$this->input->post('userId');
				 $rand=rand();
				 $picname=$rand.time().'.png';
				 $Image = str_replace('\/','/',$Image);
				 if(!empty($Image)){	
							$imageData = base64_decode($Image);
							$source = imagecreatefromstring($imageData); 
							$angle = 0;
							$rotate = imagerotate($source, $angle, 0); 
							$imageName = UPLOADS_PATH.$picname; 
							$imageSave = imagejpeg($rotate,$imageName,100);
							imagedestroy($source);
							$updateData['profilePicture'] = $picname;	
				 }
				  $updateData['updatedTime'] = date("Y-m-d H:i:s");
				  $result=$this->Usermodel->updateUser($updateData,$userId);	
				  if($result){
							$details= $this->Usermodel->profile($userId);
							$message = array('status' => 1,'message' => UPDATE_SUCCESS,'result'=>$details);
						}else{
							$message = array('status' => 0,'message' => UPDATE_FAILED,'result'=>$details);
						}
				     $this->response($message, 200);	
			 }else{
					$message = array('status' => 0,'message' => strip_tags(validation_errors()));
					$this->response($message, 200);
			 }
		 }
		 catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	 
	}
	/*******************
	********************
	This method is useful to 
      to change password	
	********************
	********************/
	
	function changePassword_post(){
			try{
				 $this->load->library('form_validation');
		         $this->form_validation->set_rules('oldPassword',' Password','trim|required|min_length[6]|max_length[20]');
		         $this->form_validation->set_rules('newPassword','New Password','trim|required');
		         $this->form_validation->set_rules('userId','Confirm Password','trim|required');
				 $this->form_validation->set_rules('token','token','trim|required');

				   if($this->form_validation->run()!=false)
				   {	
                   $wrongToken= $this->checkTokenValid($this->input->post('token'));
				   if($wrongToken){
						$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
				   }
				   			   
					  $userId = trim($this->input->post('userId'));
					  $oldPassword = trim($this->input->post('oldPassword'));
					  $newPassword = trim($this->input->post('newPassword'));
					  $userDetails = $this->getSingleRecord(TBL_USERS,$where=array('userId'=>$userId));
 		              if(decrypt($userDetails['password']) != $oldPassword){  
						 $this->response(array('status' => 0,'message' => 'Current password is wrong.'),200); die();
					  }
					  $pwd=encrypt($newPassword);
					  $updateData['password']=$pwd;						
					  $updateData['updatedTime'] = date("Y-m-d H:i:s");
					  $where=array('userId'=>$userId);
					  $result=$this->insertOrUpdate(TBL_USERS,$where,$updateData);
						if($result)
						{ 
					        $message = array('status' => 1,'message' => PASSWORD_UPDATE_SUCCESS);
 					    }else{
							$message = array('status' => 0,'message' => UPDATE_FAILED);
						}
				 }else{
					$message = array('status' => 0,'message' => strip_tags(validation_errors()));
				}
				$this->response($message, 200);
		 }
		 catch (Exception $exception)
		 {
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 			   
	}
	/*******************
	********************
	This method is useful to 
      for forgot password
	********************
	********************/
	
	function forgotpassword_post(){
		try{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email_id','Email','trim|required|valid_email');	
            $this->form_validation->set_rules('token','token','trim|required');
			
			if($this->form_validation->run()!=false)
			{
				$wrongToken= $this->checkTokenValid($this->input->post('token'));
				   if($wrongToken){
						$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
				   }
				$email=$this->input->post('email_id'); 
				$uId="";
                $checkUserExists = $this->Usermodel->email_exists($email);			
				if($checkUserExists){
						$sendEmail=$this->Usermodel->recoverUserPassword($email,$uId);
						if($sendEmail){ 
							$message = array('status' => 1,'message' =>FORGOT_PWD_MAIL_SENT );
                       }else{
							$message = array('status' => 0,'message' =>SENDING_MAIL_FAILED);
						}
				 }else{ $message = array('status' => 0,'message' => USER_NOT_EXISTS);}			 
			}else {
				$message = array('status' => 0,'message' => strip_tags(validation_errors()));
			}
			$this->response($message, 200);
			
	}catch (Exception $exception)
	 {
		$data['error']=$exception->getMessage();
		$this->logExceptionMessage($exception);					
	 } 	
			
}
   function updateProfilePic_post(){
		         $Image=$this->input->post('profilePic');
				 $userId=$this->input->post('userId');
				 $token=$this->input->post('token');
				  if(empty((int)$userId))
					{
						$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();
					}
					 if(empty($token))
					{
						$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
					}
				   $wrongToken= $this->checkTokenValid($token);
				   if($wrongToken){
						$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
				   }
		         $rand=rand();
				 $picname=$rand.time().'.png';
				 $Image = str_replace('\/','/',$Image);
				 if(!empty($Image)){	
							$imageData = base64_decode($Image);
							$source = imagecreatefromstring($imageData); 
							$angle = 0;
							$rotate = imagerotate($source, $angle, 0); 
							$imageName = UPLOADS_PATH.$picname; 
							$imageSave = imagejpeg($rotate,$imageName,100);
							imagedestroy($source);
							$updateData['profilePicture'] = $picname;	
				 }
				  $updateData['updatedTime'] = date("Y-m-d H:i:s");
				  $result=$this->Usermodel->updateUser($updateData,$userId);	
				  if($result){
							$details= $this->Usermodel->profile($userId);
							$message = array('status' => 1,'message' => UPDATE_SUCCESS,'result'=>$details);
						}else{
							$message = array('status' => 0,'message' => UPDATE_FAILED,'result'=>$details);
						}
				     $this->response($message, 200);	
	}
	function updateCourse_post(){
		         $course=trim($this->input->post('courseIds'));
				 $userId=trim($this->input->post('userId'));
				 $token=trim($this->input->post('token'));
				  if(empty((int)$userId) )
					{
						$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();
					}
					if(empty((int)$course) )
					{
						$this->response(array('status'=>0,'message' => 'Please select course.'), 200); die();
					}
					 if(empty($token))
					{
						$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
					}
				   $wrongToken= $this->checkTokenValid($token);
				   if($wrongToken){
						$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
				   }
		         
				  $updateData['course'] = $course;
				  $updateData['updatedTime'] = date("Y-m-d H:i:s");
				  $result=$this->Usermodel->updateUser($updateData,$userId);	
				  if($result){
							$details= $this->Usermodel->profile($userId);
							$message = array('status' => 1,'message' => UPDATE_SUCCESS,'result'=>$details);
						}else{
							$message = array('status' => 0,'message' => UPDATE_FAILED,'result'=>$details);
						}
				     $this->response($message, 200);	
	}
	function Resellers_get(){
		 
		$where = array('isActive'=>1,'isDeleted'=>0);
		$table = TBL_RESELLERS;
		$details= $this->getAllRecords(TBL_RESELLERS,$where,$select="reseller_id,reseller_name,isReseller");
		if($details){
			$message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $details);
		}else{
		$message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
		}
		$this->response($message, 200);

}
   function logout_post(){
	        $userId=$this->input->post('userId');
	        $token=$this->input->post('token');
	        $deviceId=$this->input->post('deviceId');
			  if(empty((int)$userId) && empty($token))
					{
						$this->response(array('status'=>0,'message' => 'userId and token are required.'), 200); die();
					}
					$wrongToken= $this->checkTokenValid($token);
					   if($wrongToken){
							$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
					   }
			$trackData['isActive']=0;
			$fcmdata['isActive']=0;
		    $result=$this->insertOrUpdate(TBL_USER_TRACK,$where=array('userID'=>$userId,'token'=>$token),$trackData);
			if($deviceId){
			 $this->insertOrUpdate('oes_fcm_tokens',$where=array('userID'=>$userId,'deviceId'=>$deviceId),$fcmdata);
            }
            if($result){
			   $message = array('status' => 1,'message' => 'Logout successfully.');
		    }else{
			   $message = array('status' => 0, 'message' => 'Error occured.');
			}
			$this->response($message, 200);
	}
	
	
}

?>