<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
/**
 * This resource contains the services for the Admin authentication..etc., and other admin related services.
 * 
 * @category	Restful WebService
 * @controller  Admin Controller
 * @author		Ashiya syed
 */

require APPPATH.'/libraries/REST_Controller.php';
class Cart extends REST_Controller {
 
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		$this->load->model('Usermodel'); 

		date_default_timezone_set('Asia/Kolkata');

	}
	/*******************
	********************
	This method is useful to 
	add test/package to cart
	********************
	********************/
	
function addToCart_post()
 {
		
		   try{
			    $cartCount=$amount=0;
			    $this->load->library('form_validation');
				$this->form_validation->set_rules('userId','UserId','trim|required');
				$this->form_validation->set_rules('type','Type','trim|required');
				$this->form_validation->set_rules('id','id','trim|required');
                $this->form_validation->set_rules('token','token','trim|required');
				if($this->form_validation->run()!=false)
				{
					$wrongToken= $this->checkTokenValid($this->input->post('token'));
				   if($wrongToken){
						$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
				   }
					$quantity = 1;
					$itemType=$this->input->post('type');
					$userId=$this->input->post('userId');
					$id=$this->input->post('id');
					$cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
					$profile=$this->Usermodel->profile($userId);
					$resellerId=$profile->resellerId;
					//print_r($profile->isPromoterUser);die();
				   if($itemType=="1"){
					  
					  if($profile->isPromoterUser == 1){
						  $packageamount=$this->getSingleRecord(TBL_PROMOTER_PACKAGES,$where=array('p_package_id'=>$id,'p_promoter_id'=>$resellerId),'p_sell_amount as package_amount'); 
                       }else{ 
						  $packageamount=$this->getSingleRecord(TBL_PACKAGES,$where=array('package_id'=>$id),'package_amount'); 
                       }
					  
					   $amount=$packageamount['package_amount'];
				   }
				   if($itemType=="2"){
					    if($profile->isPromoterUser == 1){
					       $packageamount=$this->getSingleRecord(TBL_PROMOTER_TESTS,$where=array('p_test_id'=>$id,'p_t_promoter_id'=>$resellerId),'p_t_sell_amount as testAmount'); 
						}else{ 
					      $packageamount=$this->getSingleRecord(TBL_TESTS,$where=array('testId'=>$id),'testAmount'); 
                        }
					    $amount=$packageamount['testAmount'];
				   }
					$where=array('itemType'=>$itemType,'itemId'=>$id,'userId'=>$userId,'isActive'=>1,'isDeleted'=>0);
				    $checkisItemSelected=$this->getSingleRecord(TBL_CART,$where,'*');
				    $updata['itemType']=$itemType;
					$updata['userId']=$userId;
					$updata['itemId']=$id;
					if($checkisItemSelected && count($checkisItemSelected)>0){
						$message = array('status' => 0,'message' => CART_ITEM_ALREADY_EXISTS,'cartCount'=>$cartCount);
					}else{
						$where=array();
						$updata['createdTime']=date("Y-m-d H:i:s");
						$updata['quantity']=$quantity;
						$updata['amount']=$amount*($quantity);
						$results=$this->insertOrUpdate(TBL_CART,$where,$updata);
						if($results){
							$cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
							$message = array('status' => 1,'message' => CART_ITEM_ADDING_SUCCESS,'cartCount'=>$cartCount);
						}else{
							$message = array('status' => 0,'message' => CART_ITEM_ADDING_FAILED,'cartCount'=>$cartCount);
						}
					}
		}
		else
		{
			$message = array('status' => 0,'message' => strip_tags(validation_errors()));
			
		}
		$this->response($message, 200);	
	}catch (Exception $exception)
	{
		$data['error']=$exception->getMessage();
		$this->logExceptionMessage($exception);					
	} 
}
	
	function cartCount_get(){
		 try{
		  $token=@$this->get('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
			$cartCount=$amount=0;
			$userId=@$this->get('userId');
			if( empty($userId) )
			{
				$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();
			}
			$cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
			 $message = array('status' => 1,'message' => RECORDS_EXISTS,'cartCount'=>$cartCount);
			}else{
			 $message = array('status' => 0,'message' => NO_RECORDS_EXISTS,'cartCount'=>$cartCount);
			}
			$this->response($message, 200);
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 
	}
	/*******************
	********************
	This method is useful to 
	get syllabus details of packages
	********************
	********************/
	
     function cartDetails_get()
      { 
	     $token=@$this->get('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
	     $tests=array();
	     $packages=array();
		 $userId=@$this->get('userId');
		 $start=@$this->get('start');
		 $offset = DEFAULT_LIMIT;
          if( empty($userId) )
			{
				$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();
			}
		  $where=array('isActive'=>1,'userId'=>$userId);
		  $results = $this->getOffsetRecords(TBL_CART,$where,$select="Id,userId,itemId,amount,itemType",$start,$offset);
	      if($results && count($results)>0)
		  {
		       for($i=0;$i<count($results);$i++)
                   {
					   $results[$i]->isPackageOrTest=0;
					   $itemId=$results[$i]->itemId;
					   $itemType=trim($results[$i]->itemType);
					    /* if($i==5){
							if($itemType == 1){echo "here";die();}
						  // print_r($itemType);die();
					   } */
					   $results[$i]->itemName="";
					   if($itemType == 1){ 
                       $where=array('package_id'=>$itemId);   
                       $details=$this->getSingleRecord(TBL_PACKAGES,$where);
					    $results[$i]->isPackageOrTest=1;
						  if($details){
							 $results[$i]->itemName=$details['package_name'];
							}
						  $packageids[$i]=$itemId;
						 }else if($itemType == 2){
							 $results[$i]->isPackageOrTest=2;
							  $where=array('testId'=>$itemId);   
                              $details=$this->getSingleRecord(TBL_TESTS,$where);
                             if($details){
					           $results[$i]->itemName=$details['testName'];
					           }
					         $testids[$i]=$itemId;
					 }
					
			     } 
			} 
			$data['results']=$results;
		   $cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
				$data['cartCount']=$cartCount;
			}else{
				$data['cartCount']=0;
			}
         $where=array('userId'=>$userId);
         $totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
         $data['amount']=(!empty($totalAmount))?$totalAmount:0;
		    if($data['results']){
			     $message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $data);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => $data);
			 }
			 $this->response($message, 200);
      }
	  /*******************
	********************
	This method is useful to 
	remove cart item
	********************
	********************/
	  function removeCartItem_post(){
		   $token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		  $id = $this->input->post('id');
		  $userId=@$this->post('userId');
		  if( empty($id) )
			{
				$this->response(array('status'=>0,'message' => 'Item id is required.'), 200); die();
			} 
			if( empty($userId) )
			{
				$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();
			}
		    $where = array('Id'=>$id);
			$data['isDeleted'] = 1;
			$data['isActive'] = 0;
			$success = $this->insertOrUpdate(TBL_CART,$where,$data);
		    $totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
			if(empty($totalAmount)){$totalAmount=0;}
		    $cartCount=$this->Testsmodel->getTotalCartItemsCount($userId);
			if($cartCount){
				$cartCount=$cartCount;
			}else{
				$cartCount=0;
			}

			if($success){
			     $message = array('status' => 1,'message' => CART_ITEM_DELETED_SUCCESSFULLY,'cartCount'=>$cartCount,'amount'=>$totalAmount);
			 }else{
				 $message = array('status' => 0, 'message' => ERROR,'cartCount'=>cartCount,'amount'=>$totalAmount);
			 }
			 $this->response($message, 200);
	  }
	/*******************
	********************
	This method is useful to 
	check coupon valididty
	********************
	********************/
	 function checkCouponValidity($coupon,$cartAmount,$userId)
	  {   
	     $couponCode=$coupon;
		 $amount=$cartAmount;
		 $where=array('coupon_code'=>$couponCode,'isActive'=>1,'isDeleted'=>0);
		 //$result=$this->getSingleRecord(TBL_COUPONS,$where,$select="*"); 
		 $result=$this->Testsmodel->checkCouponValidity($couponCode);
		 $data['isPromocode']=0;
		 if(!$result){
			 $data['status']=0;
			 $data['message']=INVALID_COUPON;
		}else{
			/* CHECK COUPON IS PROMOTER ONE OR NOT */
			if($result['isPromoCode'] == 2){
				$promoDetails=$this->getSingleRecord(TBL_USER_COUPONS,$where=array('c_coupon_id'=>$result['coupon_id'],'c_user_id'=>$userId,'c_is_used'=>0),'*');
				if(empty($promoDetails)){
					$data['status']=0;
				    $data['message']="Invalid promo code.";
					return $data;
				}else{
					$data['isPromocode']=1;
				}
			}
			
			/* END */
			 $couponValidFrom = date('Y-m-d', strtotime($result['coupon_createdTime']));
             $couponValidTo = date('Y-m-d', strtotime($result['coupon_validity']));
			 $todatDate=date('Y-m-d');
			if (($todatDate >= $couponValidFrom) && ($todatDate <= $couponValidTo))
             { 
               if($amount >= $result['coupon_amount']){
				   $data['status']= 1;
				   $data['pendingAmount']= abs($amount-$result['coupon_amount']);
				   $data['message']= COUPON_AMOUNT_VALIDITY; 
				 }else if($amount < $result['coupon_amount']){
					//$data['pendingAmount']= 0;
				    $data['status']= 0;
				    $data['message']= "Coupon amount is greater than your cart amount.";
				}else{
					$data['pendingAmount']= 0;
				    $data['status']= 1;
				    $data['message']= VALID_COUPON;
				}
             }
			else
            {   
		        $data['status']=0;
				$data['message']= COUPON_EXPIRED;
           }
	    }
		
	  return $data;
	}
	 function cartTestsOrPacks($uid="")
     { 
		 $userId= $uid;
		 $data['packageids']="";
		 $data['testIds']="";
		 $userId= $uid;
		 if( empty($userId) )
			{
				$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();
			}
		  $where=array('isActive'=>1,'userId'=>$userId,'itemType'=>1);
		  $packages = $this->getAllRecords(TBL_CART,$where,$select="*");
	      if($packages && count($packages)>0)
		  {
			  $packstring="";
		      for($i=0;$i<count($packages);$i++)
                   {
					   $itemId=$packages[$i]->itemId;
                       $where=array('package_id'=>$itemId);   
                       $details=$this->getSingleRecord(TBL_PACKAGES,$where);
                      if($details){
					     $packages[$i]->packageName=$details['package_name'];
					   }
					  $packageids[$i]=$itemId;
					   $packstring.=$itemId.',';
					 } 
					$data['packageids']=rtrim($packstring,',');
				}
		   $where=array('isActive'=>1,'userId'=>$userId,'itemType'=>2);
		   $tests = $this->getAllRecords(TBL_CART,$where,$select="*");
		   if($tests && count($tests)>0)
		    {  $testString="";
		      for($i=0;$i<count($tests);$i++)
                   {
					  $testitemId=$tests[$i]->itemId;
					  $where=array('testId'=>$testitemId);   
                      $details=$this->getSingleRecord(TBL_TESTS,$where);
                       if($details){
					      $tests[$i]->testName=$details['testName'];
					   }
					    $testids[$i]=$testitemId;
						$testString.=$testitemId.',';
					 }
					 $data['testIds']=rtrim($testString,',');
				 }
		   $where=array('userId'=>$userId);
          $totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
          $data['amount']=(!empty($totalAmount))?$totalAmount:0;
		    return $data;
      }
	/*******************
	********************
	This method is useful to 
	buy pack/test by coupon 
	********************
	********************/
	function applyCoupon_post()
	{ 
	      $token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
	  $userId=$this->input->post('userId');
	  $totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
	  $testOrPackDetails=$this->cartTestsOrPacks($userId);
	  $packageids=$testOrPackDetails['packageids'];
	  $testIds=$testOrPackDetails['testIds'];
	  $amount=$testOrPackDetails['amount'];
	  $where=array('isActive'=>1,'userId'=>$userId);
	  $cartDetails = $this->getAllRecords(TBL_CART,$where,$select="*");
	  if(empty($cartDetails)){
		 $message = $this->response(array('status' => 0,'message' => CART_EMPTY),200);die();
	  } 
	  $CouponCode=$this->input->post('CouponCode');
	  $checkCouponValidity=$this->checkCouponValidity($CouponCode,$amount,$userId);
	  if($checkCouponValidity['status'] == 0){
		 $this->response(array('status' => 0,'message' => $checkCouponValidity['message']),200);
	    die(); }

	       $where=array('coupon_code'=>$CouponCode);
		   $couponData['isActive']=0;
		   $this->insertOrUpdate(TBL_COUPONS,$where,$couponData);
	    if($packageids){
		  $this->savePackagesInCart($packageids,$userId,$CouponCode);
	    }
	    if($testIds){
		   $this->saveTestsInCart($testIds,$userId,$CouponCode);
		 }
	    $data['packageDetails']=0;$data['testDetails']=0;
		 if($packageids){
		$data['packageDetails']=$this->Testsmodel->getSelectedPackagesDetails($packageids);
		}
		if($testIds){
		$data['testDetails']=$this->Testsmodel->getSelectedTestDetails($testIds);
		} 
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$data['txnid']=$txnid;
		$where=array('userId'=>$userId);
		$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
		$data['userName']=$details['userName'];
		$data['amount']=$this->input->post('amount');
		$subject="Purchase Details";
		$message=$this->load->view('emailDetails', $data,true); 
		$smtpEmailSettings = $this->config->item('smtpEmailSettings');
		$email_id=$details['emailAddress'];
		@$isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
       
	   if($totalAmount){
		    $message = array('status' => 1,'message' => PURCHASE_DONE,'totalCartAmount'=>$totalAmount); 
	   }else{
		     $message = array('status' => 2,'message' => PURCHASE_DONE,'totalCartAmount'=>$totalAmount); 
	   }
      
		$this->response($message, 200);
}
   /*******************
	********************
	This method is useful to 
	save packages in cart
	********************
	********************/
   function savePackagesInCart($packageids,$userId,$txnid,$CouponCode){
	      $packageArray=explode(',',$packageids);$results=array();
		  $totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);

		  foreach($packageArray as $packary){
			$pId=$packary;
			$where=array('package_id'=>$pId);
			$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
			$testId=$packageDetails['package_test'];
			$where=array();
			$updata['txnid']= $txnid;
			$updata['createdTime']= date("Y-m-d H:i:s");
            $updata['userId']=$userId;
			$updata['packageId']=$pId;
			$updata['isActive']= 1;
			$updata['packageTests']=$testId;
			$updata['couponCode']= $CouponCode;
			$where=array('coupon_code'=>$CouponCode);
		    $coupondetails= $this->getSingleRecord(TBL_COUPONS,$where,'*');	
			$updata['couponAmount']=$coupondetails['coupon_amount'];
            $updata['paidAmount']= $totalAmount;
			$updata['amount']= $packageDetails['package_amount'];
			$updata['isDeleted']= 0; 
			//on 17 th
			$where=array('packageId'=>$pId,'amount'=>0,'txnid'=>0,'userId'=>$userId);
			$check=$this->getSingleRecord(TBL_USER_PACKAGE,$where,$select="*"); 
			if($check){ 
				$where=array('id'=>$check['id']);
			}else{
				$where=array();
			}
			$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
				if($results){
				   $where=array('userId'=>$userId,'itemId'=>$pId,'itemType'=>1);
				   $cartData['isActive']=0;
				   $cartData['isDeleted']=1;
				   $results=$this->insertOrUpdate(TBL_CART,$where,$cartData);
				}
        }
		return $results;
}
/*******************
	********************
	This method is useful to 
	save tests in cart
	********************
	********************/
function saveTestsInCart($testIds,$userId,$txnid,$CouponCode){
	     $where=array();$results=array();
		 $totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
		$updata['createdTime']= date("Y-m-d H:i:s");
		$updata['txnid']= $txnid;
        $updata['userId']=$userId;
        $updata['packageId']=0;
        $updata['isActive']= 1;
        $updata['isDeleted']= 0;
        $updata['packageTests']= $testIds;
		$updata['couponCode']= $CouponCode;
		$where=array('coupon_code'=>$CouponCode);
		    $coupondetails= $this->getSingleRecord(TBL_COUPONS,$where,'*');	
			$updata['couponAmount']=$coupondetails['coupon_amount'];
            $updata['paidAmount']= $totalAmount;
		$testAmount=$this->Testsmodel->getSelectedTestsAmount($testIds);
		$testAmount=$testAmount->total;
		$updata['amount']= $testAmount;
			$where=array('packageTests'=>$testIds,'amount'=>0,'txnid'=>0,'userId'=>$userId);
			$check=$this->getSingleRecord(TBL_USER_PACKAGE,$where,$select="*"); 
			if($check){
				$where=array('id'=>$check['id']);
			}else{
				$where=array();
			}
           $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
			if($results){
				$tests=explode(',',$testIds);
				foreach($tests as $test){
				   $where=array('userId'=>$userId,'itemId'=>$test,'itemType'=>2);
				   $cartData['isActive']=0;
				   $cartData['isDeleted']=1;
				   $results=$this->insertOrUpdate(TBL_CART,$where,$cartData);
		}
	  }
	  return $results;
}

function rsa_post(){
	try{
		$token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token); 
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
	  $orderId =$this->input->post('order_id');
	   if (empty($orderId)) {
            $message = $this->response(array('status' => 0,'message' => "An order_id is required."),200);die();
        }
       $url = "https://secure.ccavenue.com/transaction/getRSAKey";
        $fields = array(
           'access_code'=>"AVLN75EL18CC38NLCC",
           'order_id'=>$orderId,
         );
		
       $postvars='';
       $sep='';
	  
		foreach($fields as $key=>$value)
		{
				$postvars.= $sep.urlencode($key).'='.urlencode($value);
				$sep='&';
		}

        $ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch, CURLOPT_CAINFO, '../ccavenue/Rsa/cacert.pem');
		curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch); 
        echo $result;
		die(); 	
       }catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 		
}

/* this method used to get orderId and amount */
function getOrderId_post(){
	$amount=0;
	
	      $token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		 $userId =$this->input->post('userId');
	    if (empty($userId)) {
            $message = $this->response(array('status' => 0,'message' => "An userId is required."),200);die();
        }
		$txnid = 'A'.substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
        /* new */
		$where=array('isActive'=>1,'userId'=>$userId);
	      $cartDetails = $this->getAllRecords(TBL_CART,$where,$select="*");
	     if(empty($cartDetails)){
		 $message = $this->response(array('status' => 0,'message' => CART_EMPTY),200);die();
	    } 
		$CouponCode=$this->input->post('CouponCode');
		if($CouponCode){
			$where=array('coupon_code'=>$CouponCode,'isActive'=>1,'isDeleted'=>0);
		    $c=$this->getSingleRecord(TBL_COUPONS,$where,$select="*");
			// withcoupon code 
			$checkCouponValidity=$this->checkCouponValidity($CouponCode,$totalAmount,$userId);
			if($checkCouponValidity['status'] == 0){
			    $this->response(array('status' => 0,'message' => $checkCouponValidity['message']),200);
			die(); }
			//coupoun valid and pending amount ! =0 
			if($checkCouponValidity['status'] == 1 && $checkCouponValidity['pendingAmount']){
				 $testOrPackDetails=$this->cartTestsOrPacks($userId);
				 $tests=$testOrPackDetails['testIds'];
				 $packages=$testOrPackDetails['packageids'];
				 $orderId=rand().time(); 
				  if($tests){ 
					$result=$this->saveOrderIdWitests($userId,$tests,$txnid,$orderId,$CouponCode);
				} 
				if(@$packages){
					$result=$this->saveOrderId($userId,$packages,$txnid,$orderId,$CouponCode);
				}
				 if($result){ 
					  $couponAmount=$c['coupon_amount'];
					  $msg="Your Coupon ".$CouponCode." worth Rs ".$couponAmount ." applied successfully.Please pay Rs.".$checkCouponValidity['pendingAmount']." by online payment.";
					  $message = array('status' => 1,'message' => $msg,'orderId'=>$orderId,'amount'=>$checkCouponValidity['pendingAmount']);
				 }else{
					 $message = array('status' => 0, 'message' => ERROR,'orderId'=>$orderId,'amount'=>$totalAmount);
				 }
				 $this->response($message, 200);
				
			}else{ 
				/*  coupoun valid and pending amount == 0  */
				
				 $testOrPackDetails=$this->cartTestsOrPacks($userId);
				 $packageids=$testOrPackDetails['packageids'];
				 $testIds=$testOrPackDetails['testIds'];
				 $amount=$testOrPackDetails['amount'];
				 if($packageids){
				   $result=$this->savePackagesInCart($packageids,$userId,$txnid,$CouponCode);
				 }
				if($testIds){
				  $result=$this->saveTestsInCart($testIds,$userId,$txnid,$CouponCode);
				 }
			 $data['packageDetails']=0;$data['testDetails']=0;
			 if($packageids){
			  $data['packageDetails']=$this->Testsmodel->getSelectedPackagesDetails($packageids);
			}
			if($testIds){
			$data['testDetails']=$this->Testsmodel->getSelectedTestDetails($testIds);
			} 
			
			$data['txnid']=$txnid;
			$where=array('userId'=>$userId);
			$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
			$data['userName']=$details['userName'];
			$data['amount']=$amount;
			$subject="Purchase Details";
			$message=$this->load->view('emailDetails', $data,true); 
			$smtpEmailSettings = $this->config->item('smtpEmailSettings');
			$email_id=$details['emailAddress'];
			//@$isEmailSent = sendSmtpEmail($smtpEmailSettings['smtp_user'],$email_id,$subject,$message);
           //deactivate coupon code
		    $where=array('coupon_code'=>$CouponCode);
		    $couponData['isActive']=0;
		    $this->insertOrUpdate(TBL_COUPONS,$where,$couponData);          
		  //deactivate user promocode
			if($checkCouponValidity['isPromocode'] == 1){
			$where=array('c_user_id'=>$userId,'c_coupon_id'=>$c['coupon_id']);
		    $promoData['c_is_used']=1;
		    $promoData['c_is_active']=0;
		    $promoData['c_is_deleted']=1;
		    $promosave=$this->insertOrUpdate(TBL_USER_COUPONS,$where,$promoData);
			}
			if($result){
				$message = array('status' => 1,'message' => PURCHASE_DONE,'totalCartAmount'=>$amount); 
			 }else{
			   $message = array('status' => 0,'message' => ERROR,'totalCartAmount'=>$amount); 
			 }
			  $this->response($message, 200);
			}
	}else{
		/* without coupon code */
		 $testOrPackDetails=$this->cartTestsOrPacks($userId);
		 $tests=$testOrPackDetails['testIds'];
		 $packages=$testOrPackDetails['packageids'];
		 $orderId=rand().time(); 
		   
		  if($tests){ 
			$result=$this->saveOrderIdWitests($userId,$tests,$txnid,$orderId);
		  } 
		  if(@$packages){
			$result=$this->saveOrderId($userId,$packages,$txnid,$orderId);
		  }
		  $totalAmount=$this->Testsmodel->getTotalCartItemsAmount($userId);
          if($result){
			  $message = array('status' => 1,'message' => 'Order id saved successfully','orderId'=>$orderId,'amount'=>$totalAmount);
		 }else{
			$message = array('status' => 0, 'message' => ERROR,'orderId'=>$orderId,'amount'=>$totalAmount);
		 }
		$this->response($message, 200);	
	}
		
	}
	function saveOrderId($userId,$packageids,$txnid,$orderId,$CouponCode=""){ 
	
	     $results="";
		 $profile=$this->Usermodel->profile($userId);
		  if($packageids){ 	
          $packageArray=explode(',',$packageids);
		  foreach($packageArray as $packary){
			$pId=$packary;
			$where=array('package_id'=>$pId);
			$packageDetails=$this->getSingleRecord(TBL_PACKAGES,$where,$select="*");
			$testId=$packageDetails['package_test'];
			$where=array();
			$updata['txnid']=$txnid;
            $updata['createdTime']= date("Y-m-d H:i:s");
            $updata['userId']=$userId;
			$updata['packageId']=$pId;
			$updata['isActive']= 0;
			$updata['packageTests']=$testId;
			$updata['ccavenueOrderId']= $orderId;
			$resellerId=$profile->resellerId;
			if($CouponCode){
				// $where=array('coupon_code'=>$CouponCode);
		         $coupondetails= $this->getSingleRecord(TBL_COUPONS,array('coupon_code'=>$CouponCode),'*');
			     $updata['couponAmount']=$coupondetails['coupon_amount'];
			     $updata['CouponCode']= $CouponCode;
		    }
			 if($profile->isPromoterUser == 1){ 
				$packageamount=$this->getSingleRecord(TBL_PROMOTER_PACKAGES,array('p_package_id'=>$pId,'p_promoter_id'=>$resellerId),'p_sell_amount as package_amount'); 
                $updata['amount']= $packageamount['package_amount'];
			 }else{
				$updata['amount']= $packageDetails['package_amount'];
             }
			$updata['isDeleted']= 0;
			$results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
		}
		return $results;
	  }
	
	  
	}
	function saveOrderIdWitests($userId,$testIds,$txnid,$orderId,$CouponCode=""){
		$results="";
		$profile=$this->Usermodel->profile($userId);
		 if($testIds){
		$where=array();
		$updata['createdTime']= date("Y-m-d H:i:s");
        $updata['txnid']=$txnid;
        $updata['userId']=$userId;
        $updata['packageId']=0;
        $updata['isActive']= 0;
        $updata['isDeleted']= 0;
		if($CouponCode){
			//$where=array('coupon_code'=>$CouponCode);
		    $coupondetails= $this->getSingleRecord(TBL_COUPONS,array('coupon_code'=>$CouponCode),'*');	
			$updata['couponAmount']=$coupondetails['coupon_amount'];
			$updata['CouponCode']= $CouponCode;
		}
		$updata['ccavenueOrderId']= $orderId;
        $updata['packageTests']= $testIds;
		$testAmount=$this->Testsmodel->getSelectedTestsAmount($testIds);
		$testAmount=$testAmount->total;
		$resellerId=$profile->resellerId;
		 if($profile->isPromoterUser == 1){
			 $testss=explode(',',$testIds);
			  foreach($testss as $id){
			    $packageamount=$this->getSingleRecord(TBL_PROMOTER_TESTS,$where=array('p_test_id'=>$id,'p_t_promoter_id'=>$resellerId),'p_t_sell_amount as testAmount'); 
			    $amnt=$amnt+$packageamount['testAmount'];	
			 }
			 $updata['amount']= $amnt;
		     /* $packageamount=$this->getSingleRecord(TBL_PROMOTER_TESTS,$where=array('p_test_id'=>$id),'p_t_sell_amount as testAmount'); 
			 $updata['amount']= $packageamount['testAmount']; */
		}else{ 
	       $updata['amount']= $testAmount;
		}
         $results=$this->insertOrUpdate(TBL_USER_PACKAGE,$where,$updata);
		 } 
		 return $results;
	}
	
	
	 function paymentHistory_get(){
		  $token=@$this->get('token');
		    if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }  
	     $userId=@$this->get('userId');
		 $start=(@$this->get('start'))?@$this->get('start'):0;
		 $offset = DEFAULT_LIMIT;
           if(empty($userId))
			{
				$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();
			}
			$paymentDetails=$this->Testsmodel->getUserPayments($userId,$offset,$start);
			 if($paymentDetails){
			  $message = array('status' => 1,'message' => RECORDS_EXISTS,'paymentDetails'=>$paymentDetails,'offset'=>$offset);
		 }else{
			$message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'paymentDetails'=>$paymentDetails,'offset'=>$offset);
		 }
		  $this->response($message, 200);	
	} 
	   
	
}

?>