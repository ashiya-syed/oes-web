<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("access-control-allow-origin: *");
/**
 * This resource contains the services for the Admin authentication..etc., and other admin related services.
 * 
 * @category	Restful WebService
 * @controller  Admin Controller
 * @author		Ashiya syed
 */

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
class Exams extends REST_Controller {
 
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('Testsmodel');
		$this->load->model('Packagemodel');
		date_default_timezone_set('Asia/Kolkata');

	}
	/*******************
	********************
	This method is useful to 
	start exam and to send otp
	********************
	********************/
	function startExam_post()
	{
		try{
		  $token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		   
			$data = array();
            $testId=@$this->input->post('testId'); 
			$userId=@$this->input->post('userId'); 
			$testType=@$this->input->post('testType'); 
			$packageId=(@$this->input->post('packageId'))?@$this->input->post('packageId'):0; 
			$uniqueId=(@$this->input->post('uniqueId'))?@$this->input->post('uniqueId'):0; 
			$data['package_id'] =0;
			$data['packageName'] ="";
		    if( empty($userId) ){$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();}
		    if( empty($testId) ){$this->response(array('status'=>0,'message' => 'testId is required.'), 200); die();}
		    if( empty($testType) ){$this->response(array('status'=>0,'message' => 'testType is required.'), 200); die();}
			if($testType=="2"){ $ty=2; }else{ $ty=1; }
			 if($packageId){
				$packageDetails=$this->Testsmodel->packageDetailsById($packageId);
                $data['packageName']=@$packageDetails->package_name;
                $data['package_id'] = @$packageDetails->package_id;
            } 
			$where=array('testId'=>$testId,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'userId'=>$userId,'status'=>TEST_COMPLETED,'isActive'=>1);
			$CompletedTestTimes=$this->getAllRecords(TBL_TIMES,$where); 
			$testDetails=$this->getSingleRecord(TBL_TESTS,$where=array('testId'=>$testId),'*');
			$testAttempts=$testDetails['attempts']; 
			//if($this->session->userdata('isPromotionMember')!=1){
			if(($testAttempts)&& (count($CompletedTestTimes)>=$testAttempts)) {
				$this->response(array('status'=>0,'message' =>TEST_ATTEMPTS_COMPLETED), 200); die();
			}
			$where=array('testId'=>$testId);
            $data['totalQue'] = $this->Testsmodel->getTotalQueByTest($testId);
			if($data['totalQue'] == 0){ 
				$this->response(array('status'=>0,'message' =>"No Questions for this test.So you are unable to attempt this test."), 200); die();
			}
            $data['testId']=$testDetails['testId'];
            $data['testname']=$testDetails['testName'];
            $data['testTime']=$testDetails['testTime'];
            $data['rightMark']=$testDetails['rightMarks'];
            $data['negativeMark']=$testDetails['negativeMarks'];
            $data['totalMarks']=$testDetails['totalMarks'];
			$data['testType'] = $ty;
			$data['uniqueId'] = $uniqueId;
			if($CompletedTestTimes){
				$noOfatempts=count($CompletedTestTimes);
			}else{$noOfatempts=0;}
			$data['attemptingFor'] =$noOfatempts+1 ; 
         if($data){
			     $RESULT=$this->sentOtp($userId); 
			     $saveExamDetails=$this->saveExamDetails($userId,$testId, $testType, $packageId, $uniqueId);
			     $message = array('status' => 1,'message' => CHECK_YOUR_MOBILE_FOR_OTP,'result' => $data,'otp'=>$RESULT,'recordId'=>$saveExamDetails);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
			 $this->response($message, 200);		
		}catch (Exception $exception)
		{
			$data['error']=$exception->getMessage();
			$this->logExceptionMessage($exception);					
		} 

	}
	 /*******************
	********************
	This method is useful to 
	send otp in start exam
	********************
	********************/
	  function sentOtp($userId){
				$where=array('userId'=>$userId);
				$details=$this->getSingleRecord(TBL_USERS,$where,$select="*");
				$phoneNumber=$details['phoneNumber'];
			    $token=generateRandNumber(6);
				$msgText = "Please use this otp to start your exam ".$token;
               // $response = sendOTP($msgText, $phoneNumber);
				$userId=$details['userId'];
				$where=array('userID'=>$userId,'isactive'=>1);
				$checkUserMultipleotp=$this->getAllRecords(TBL_OTPS,$where);
				if($checkUserMultipleotp){
					$where=array('userID'=>$userId,'isactive'=>1);
					$udata['isactive']=0;
					$this->insertOrUpdate(TBL_OTPS,$where,$udata);
				}
				$purpose="startExam";
			    $this->Testsmodel->saveRecoveryEmailData($userId,$token,$purpose);
				return $token;
	 }
	 function reSendOtp_get(){
		 $userId=(int)@$this->get('userId');
		 if( empty($userId) ){$this->response(array('status'=>0,'message' => 'userId is required.'), 200); die();}
		 $result=$this->sentOtp($userId);
		  if($result){
			     $message = array('status' => 1,'message' => CHECK_YOUR_MOBILE_FOR_OTP,'otp' => $result);
			 }else{
				 $message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
			 }
		$this->response($message, 200);	
	 }
   /*******************
	********************
	This method is useful to 
	saveexam data in start exam
	********************
	********************/
	function saveExamDetails($userId,$examId, $type, $packageId, $uniqueId)
	{     
            $where=array('testId'=>$examId);
            $data['totalQue'] = $this->Testsmodel->getTotalQueByTest($examId);
            $data['testdetails']=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks");
                $startedExamId=$examId; 
				$updata['testId']=$startedExamId;
				$updata['testTime']=$data['testdetails']['testTime'];
				$updata['startTime']=date("Y-m-d H:i:s");
				$updata['userId']=$userId;
				$updata['packageId']=$packageId;
				$updata['userPackageID']=$uniqueId;
				$updata['status']=4;
				$updata['testType']=$type;
				$updata['ipAddresss']=$this->input->ip_address(); 
				$updata['createdTime']=date("Y-m-d H:i:s");
				$where=array('testId'=>$startedExamId,'userId'=>$userId,'testType'=>$type,'packageId'=>$packageId,'userPackageID'=>$uniqueId,'isActive'=>1);
				$isDoneBefore=$this->Testsmodel->getNumberOfAttempts(TBL_TIMES,$where);
				$where=array();
				if($isDoneBefore){ //print_r($isDoneBefore);die();
					$attempts=$isDoneBefore->attempts;
					$updata['attempts']=$attempts+1;
					if($isDoneBefore->status==2){ 
						$updata['attempts']=$attempts;
						$updata['timeDifference']=$isDoneBefore->timeDifference;				
						$updata['status']=$isDoneBefore->status;
                        $where=array('id'=>$isDoneBefore->id);
						$results=$this->insertOrUpdate(TBL_TIMES,$where,$updata); 
				        return $isDoneBefore->id; 
					}else if($isDoneBefore->status==4 || $isDoneBefore->status==0 || ($isDoneBefore->timeDifference && $isDoneBefore->status!=3)){ 
						$updata['attempts']=$attempts;
						$updata['status']=4;
                        $where=array('id'=>$isDoneBefore->id);
						$results=$this->insertOrUpdate(TBL_TIMES,$where,$updata); 
				        return $isDoneBefore->id; 
					}else{ 
					    $results=$this->insertOrUpdate(TBL_TIMES,$where,$updata);
				        return $results;
					}
					
				}else{
					 $updata['attempts']=1;
					 $results=$this->insertOrUpdate(TBL_TIMES,$where,$updata);
				     return $results; 
				}		   
			   
			
	}	

/*******************
	********************
	This method is useful to 
	update exam data after otp success
	********************
	********************/
	function updateExamDetails($recordId){
		$result['isRedoTest']=0; 
		$where=array('id'=>$recordId); 
	    $result=$this->getSingleRecord(TBL_TIMES,$where,$select="*");
		$returnData=$result;
		$returnData['isRedoTest']=0;
		if($result && $result['status']== 4 ){ 
	    $updata['status']=0;
		$updata['createdTime']=date("Y-m-d H:i:s");
		$updata['startTime']=date("Y-m-d H:i:s");
		$insert=$this->insertOrUpdate(TBL_TIMES,$where,$updata);
		}		
		if($result['status']==2 || $result['timeDifference']){
			$returnData['isRedoTest']=1;
			}
		if($result){ 
			return $returnData;
		}else{ 
		    return array();
		}
      }
	  /*******************
	********************
	This method is useful to 
	get test details
	********************
	********************/
	  function testDetailsById($id){ 
		  $where=array('testId'=>$id);
          $testDetails=$this->getSingleRecord(TBL_TESTS,$where,$select="testName,testId,testTime,negativeMarks,rightMarks,isShuffleQueOrder");
		  if($testDetails){
			  return $testDetails;
		  }else{
			  return array();
		  }
	  }
	   /*******************
	********************
	This method is useful to 
	get reference questions
	********************
	********************/
	  function saveReferenceQuestions($questionNumbers,$examid,$userId,$uniqueId,$attempts){
		     $mainarray=array();
			if($questionNumbers){
				foreach($questionNumbers as $questions){$mainarray[]=$questions->que_id;}
			 }		  
			$array = array_values($mainarray);
			$where=array();$table=TBL_QUESTIONS_ORDER;
			$referenceData['que_id']=implode(',',$array);
			$referenceData['test_id']=$examid;
			$referenceData['isActive']=1;
			$referenceData['user_id']=$userId;
			$referenceData['userPackage']=$uniqueId;
			$referenceData['attempts']=$attempts; 
			$data['queString']=implode(',',$array);
			$data['array']=$array;
		    $res=$this->insertOrUpdate($table,$where,$referenceData);
			return $data;
		}
	/*******************
	********************
	This method is useful to 
	verify otp and save que order
	and get starting  que
	********************
	********************/
	
	function questionPaper_post()
	{ 
	    $token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
	    $data=array();
	    $questionString="";
		$userId=@$this->input->post('userId');
	    $otp=@$this->input->post('otp');
	    $recordId=@$this->input->post('recordId'); 
		$start=@$this->input->post('start');
		$offset=100;
	    if(empty($userId) && empty($otp) && empty($recordId)){
			$message = array('status' => 0,'message' => 'Please provide userId,otp and recordId.');
		}
	    $where=array('userID'=>$userId,'isactive'=>1,'token'=>$otp,'purpose'=>'startExam');
		$checkvalidOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*");
		//if($otp == '123456'){
      	if(count($checkvalidOtp)>0){
		   $savedResults=$this->updateExamDetails($recordId); 
		  if(empty($savedResults)){
			  $this->response(array('status' => 0,'message' => 'Error occured while saving test data.'),200);die();
		  } 
		  $testTime=$savedResults['testTime'];
		  $testId=$savedResults['testId'];
		  $packageId=$savedResults['packageId'];
		  $uniqueId=$savedResults['userPackageID'];
		  $testType=$savedResults['testType'];
		  $attempts=$savedResults['attempts'];
		  $isRedoTest=$savedResults['isRedoTest'];
		  $testDetails=$this->testDetailsById($testId); 
		  $isShuffle=$testDetails['isShuffleQueOrder'];
		  $questionNumbers=$this->Testsmodel->getQuestionNumbers($testId,$isShuffle);
          if($isRedoTest == 1){
              $testTime=$savedResults['testTime']-$savedResults['timeDifference'];
              $data['previousTime']=$savedResults['timeDifference'];			  
			  $ReferenceTestId=$this->Testsmodel->getReferenceQuestions($testId,$userId,$uniqueId,$attempts);
			  $QUEarray=$ReferenceTestId;
		  }else{ 
		    $data['previousTime']=0;
			$ReferenceTestId=$this->Testsmodel->getReferenceQuestions($testId,$userId,$uniqueId,$attempts);
			if($ReferenceTestId){
				$QUEarray=$ReferenceTestId;
            }else{
				$ReferenceTestId=$this->saveReferenceQuestions($questionNumbers,$testId,$userId,$uniqueId,$attempts); 
			    $questionString=$ReferenceTestId['queString'];
			    $QUEarray=$ReferenceTestId['array']; 
			}
			
			$QUE=@$QUEarray['0'];
		  }
		 $data['testTime']=$testTime;
		 $data['question']= $this->Testsmodel->getQuestion($userId,$testId,$packageId,$uniqueId,$testType,$attempts,$QUEarray,$start,$offset); 
		 $data['question']=$this->removeTagsFromQuens($data['question']);
		if($data['question']){
			 $this->response(array('status' => 1,'message' => RECORDS_EXISTS,'result'=>$data),200);die();
		}else{
			 $this->response(array('status' =>2,'message' => NO_RECORDS_EXISTS,'result'=>$data),200);die();
		}
		 
		}else{
		  $this->response(array('status' => 0,'message' => INVALID_OTP),200);die();
		}
	}
		function removeTagsFromQuens($questions){
		if($questions){
			foreach($questions as $quesn){
				$quesn->que=strip_tags($quesn->que);
				//$quesn->que=$quesn->que;
				$quesn->que = preg_replace('~[\r\n]+~', '', $quesn->que);
                $quesn->hint=strip_tags($quesn->hint);
				$quesn->hint = preg_replace('~[\r\n]+~', '', $quesn->hint);
				$options=$quesn->options;
				if($options){ $s=array();
					 if($quesn->queType == 3){ 
					
							foreach($options as $opt){$i=0;$j=1; 
							 if($opt->option == "True"){ 
									$opt->option_id = "True";
									$opt->option = "True";
									if($opt->Answer == "True"){ $opt->isAnswerSelected=1;  }else{ $opt->isAnswerSelected=0;}
									if($opt->Answer == "False"){ $isAnswerSelected =1;}else{$isAnswerSelected =0;}
									$new = (object)array('option_id'=>"False",'option'=>'False','is_answer'=>0,'textType'=>1,'Answer'=>$opt->Answer,'isAnswerSelected'=>$isAnswerSelected);
								}else{
									$opt->option_id = "False";
									$opt->option = "False";
									if($opt->Answer == "False"){$opt->isAnswerSelected=1;  }else{ $opt->isAnswerSelected=0;}
                                    if($opt->Answer == "True"){ $isAnswerSelected =1;}else{$isAnswerSelected =0; }
									$new = (object)array('option_id'=>"True",'option'=>'True','is_answer'=>0,'textType'=>1,'Answer'=>$opt->Answer,'isAnswerSelected'=>$isAnswerSelected);
								}
								$s[$i]=$new;
                                $s[$j]=$opt;
							}
						   
					    }else{
							foreach($options as $opt){
						      $opt->option=strip_tags($opt->option);
						      $s[]=$opt;
					        }
						}
						$quesn->options=$s;
				}
				
			}
		}
		return $questions;
	}
	/*******************
	********************
	This method is useful to 
	get que bye que id and test id
	********************
	********************/
	function getAnotherQuestion_get(){
		$queId=@$this->get('queId');
		$testId=@$this->get('testId');
		$recordId=@$this->get('recordId');
		$this->getExamRecordDetails($recordId);
		$data['question']= $this->Testsmodel->getQuestion($testId,$queId);
		$this->response(array('status' => 1,'message' => RECORDS_EXISTS,'result'=>$data),200);die();
	}
	function getExamRecordDetails($recordId){
		$where=array('id'=>$recordId); 
	    $result=$this->getSingleRecord(TBL_TIMES,$where,$select="*");
		return $result;
		
	}
	/*******************
	********************
	This method is useful to 
	get timer 
	********************
	********************/
	function updateTimeDifference($recordId,$timedifference){ 
		    $updateData['timeDifference']=($timedifference/(60*1000));
		    $updateData['updatedTime']=date("Y-m-d H:i:s");
		    $where=array('id'=>$recordId);
		    $res=$this->insertOrUpdate(TBL_TIMES,$where,$updateData);
			return $res;
		}
	function saveTime_post(){
		$token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		$recordId=@$this->input->post('recordId');
		$userId=@$this->input->post('userId');
		$timedifference=@$this->input->post('time');
		if(empty($recordId) || empty($userId) || empty($timedifference)){		

			$this->response(array('status' => 0,'message' => "Provide timedifference, userId and record id"),200);die();
		}	

		$res=$this->updateTimeDifference($recordId,$timedifference);
		if($res){
			 $this->response(array('status' => 1,'message' => TIME_UPDATED),200);die();
		}else{
			$this->response(array('status' => 0,'message' => ERROR_OCCURED),200);die();
		}
  	}
	/* to save options */
	function saveOption_post(){ 
	$token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		$recordId=@$this->input->post('recordId');
		$userId=@$this->input->post('userId');
		$timedifference=@$this->input->post('time');
		if(empty($userId) ||  empty($recordId) || empty($timedifference)){
			$this->response(array('status' => 0,'message' => "Provide userId,recordId and time."),200);die();
		}
		$saveTime=$this->updateTimeDifference($recordId,$timedifference);//to save time

		$ansJson=$_POST['answers'];
		$answersArray=json_decode($ansJson, true);
		$answers=$answersArray['answers'];
		if($ansJson && empty($answersArray)){
			$this->response(array('status' => 0,'message' => "please provide valid json formate."),200);die();
		}
		if($ansJson && $answersArray){
		   foreach($answers as $ans){
			   if($ans['queType'] == 0 || $ans['queType'] == 1 ){  
				   @$results=$this->saveMultipleChoiceOptions($ans['option_id'],$ans['que_id'],$recordId,$ans['isReviewed']);
			   }else if($ans['option_id'] == "False" || $ans['option_id'] == "True"){ 
				   @$results=$this->saveBlankOptions($ans['option_id'],$ans['que_id'],$recordId,$ans['isReviewed']);
			   }else{
				   @$results=$this->saveBlankOptions($ans['blankAns'],$ans['que_id'],$recordId,$ans['isReviewed']);
			   }
			   $queArray[]=$ans['que_id'];
			}	
		}//print_r($this->db->last_query());die();
		if($results){
	        $this->response(array('status' => 1,'message' => SUCCESS,'questions'=>$queArray),200);die();
		}else{
			 $this->response(array('status' => 0,'message' => ERROR_OCCURED),200);die();
		}
	}
	function saveMultipleChoiceOptions($selectedOpt,$que,$recordId,$isReviewed){ 
		$details=$this->getExamRecordDetails($recordId);
		$examid=$details['testId'];
		$package=$details['packageId'];
		$uniqueId=$details['userPackageID'];
		$testType=$details['testType'];
		$userId=$details['userId'];
		$attempts=$details['attempts'];
		if($selectedOpt){
				$option = str_replace(" ","", $selectedOpt);
			}else{
				 $option =0;
			}
	    $where=array('que_id'=>$que,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1);
		$check=$this->getSingleRecord(TBL_EXAM_RESULTS,$where,$select="*"); 
		if($check){
			$where=array('que_id'=>$que,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1); $udata['updatedTime']=date("Y-m-d H:i:s");
		}else{
			$where=array();
			$udata['createdTime']=date("Y-m-d H:i:s");
		}
			if($isReviewed){
				$udata['is_reviewed']=$isReviewed;
             }
			$udata['option_id']=$option;
			$udata['que_id']=$que;
			$udata['testId']=$examid;
			$udata['packageId']=$package;
			$udata['userId']=$userId;
			$udata['userPackageID']=$uniqueId;
			$udata['attempts']=$attempts;
			$udata['testType']=$testType;
			$udata['queType']=1;
			$results=$this->insertOrUpdate(TBL_EXAM_RESULTS,$where,$udata);
			return $results;
	}
	
	function saveBlankOptions($val,$que,$recordId,$isReviewed){  
		$details=$this->getExamRecordDetails($recordId);
		$examid=$details['testId'];
		$package=$details['packageId'];
		$uniqueId=$details['userPackageID'];
		$testType=$details['testType'];
		$attempts=$details['attempts'];
		$userId=$details['userId'];
		$queType=2;
	    $where=array('que_id'=>$que,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1,'queType'=>2);
		$check=$this->getSingleRecord(TBL_EXAM_RESULTS,$where,$select="*"); //print_r($check);die();
		if($check){
			$where=array('que_id'=>$que,'userId'=>$userId,'testId'=>$examid,'testType'=>$testType,'attempts'=>$attempts,'packageId'=>$package,'userPackageID'=>$uniqueId,'isActive'=>1,'queType'=>2);
			$udata['updatedTime']=date("Y-m-d H:i:s");
		}else{
			$where=array();
			$udata['createdTime']=date("Y-m-d H:i:s");
		}
		    if($isReviewed){
				$udata['is_reviewed']=$isReviewed;
             }
			$udata['textOption']=$val;
			$udata['que_id']=$que;
			$udata['testId']=$examid;
			$udata['packageId']=$package;
			$udata['userId']=$userId;
			$udata['userPackageID']=$uniqueId;
			$udata['attempts']=$attempts;
			$udata['testType']=$testType;
			$udata['queType']=$queType; 
			/* if($val){ */
			$results=$this->insertOrUpdate(TBL_EXAM_RESULTS,$where,$udata);  
			/* } */
			return $results;
           		
		}
	function secondsToTime($seconds) {
	  $time="";
      $hours = floor($seconds / (60 * 60));
	  $divisor_for_minutes = $seconds % (60 * 60);
	  $minutes = floor($divisor_for_minutes / 60);
	  $divisor_for_seconds = $divisor_for_minutes % 60;
	  $seconds = ceil($divisor_for_seconds);
	  $obj = array(
			"h" => (int) $hours,
			"m" => (int) $minutes,
			"s" => (int) $seconds,
		); 
		if($obj){
			$h=$obj['h'];
			$m=$obj['m'];
			$s=$obj['s'];
			$time= $h.'h '. $m .'m '. $s.'s ';
		}
	return $time;
	}
		function submitTest_post()
	    { 
		  $token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
    	  $recordId=@$this->input->post('recordId');
    	  $userId=@$this->input->post('userId');
		  $timedifference=@$this->input->post('time');
		  $otp=@$this->input->post('otp'); 
			if(empty($recordId) || empty($userId) || empty($otp)){		
			   $this->response(array('status' => 0,'message' => "Provide userId,otp and record id"),200);die();
			}	
			if($timedifference){
              $res=$this->updateTimeDifference($recordId,$timedifference);
			}
		 //deactivate otp
			   if($otp){
					 
					  $where=array('userID'=>$userId,'isactive'=>1,'token'=>$otp,'purpose'=>'startExam');
					  $checkvalidOtp=$this->getSingleRecord(TBL_OTPS,$where,$select="*"); 
					//if($otp == '123456'){
					 if(count($checkvalidOtp)>0){
							$where=array('userID'=>$userId,'isactive'=>1,'token'=>$otp,'purpose'=>'startExam');
							$udata['isactive']=0;
							$this->insertOrUpdate(TBL_OTPS,$where,$udata);
							
							$updata['status']=TEST_COMPLETED;
							$updata['endTime']= date("Y-m-d H:i:s");
							$updata['updatedTime']= date("Y-m-d H:i:s");
							$where=array('id'=>$recordId,'userId'=>$userId,'isActive'=>1);
							$date_e=date("Y-m-d H:i:s");
							$details=$this->getSingleRecord(TBL_TIMES,$where);
							if($details){
							$date_s=$details['startTime'];
							$to_time = strtotime($date_s);
	                        $from_time = strtotime($date_e);
	                        $diff= round(abs($to_time - $from_time) / 60,2);
							   //$updata['timeDifference']=$diff;
							if($details['timeDifference']){
								//$updata['timeDifference']=$diff+$details->timeDifference;
								$where=array('id'=>$details['id']);
							}
                            $results=$this->insertOrUpdate(TBL_TIMES,$where,$updata);
							}
						}else{
							$this->response(array('status' => 0,'message' => INVALID_OTP),200);die();
						}
			   }
	    $where=array('id'=>$recordId);
        $atempts=$this->getSingleRecord(TBL_TIMES,$where,$select='*');
		$testId=$atempts['testId'];
		$testType=$atempts['testType'];
		$attempts=$atempts['attempts'];
		$packageiD=$atempts['packageId'];
		$userPackageId=$atempts['userPackageID'];
		$result=$this->Testsmodel->getExamResults($testId,$testType,$attempts,$packageiD,$userPackageId,$userId);
		if($result){
			    $timeTakn=$this->secondsToTime($result['diff']*60); 
		        $result['timeTaken']= $timeTakn;
		        $result['testTime']= $result['duration'] ." minutes";
		        $idleTime=abs(($result['duration']*60)-($result['diff']*60));
	             $idlTime=$this->secondsToTime($idleTime);
	             $result['idleTime']= $idlTime;
		         unset($result['diff']);
			$final[]=(object)array('value'=>$result['testName'],'name'=>'TEST NAME');
			$final[]=(object)array('value'=>$result['totalQue'],'name'=>'TOTAL QUESTIONS');
			$final[]=(object)array('value'=>$result['testMarks'],'name'=>'MAXIMUM MARKS');
			$final[]=(object)array('value'=>$result['attemptedQuestions'],'name'=>'TOTAL ATTEMPTED');
			$final[]=(object)array('value'=>$result['leftQuestions'],'name'=>'LEFT QUESTIONS');
			$final[]=(object)array('value'=>$result['correctAns'],'name'=>'CORRECT ANS');
			$final[]=(object)array('value'=>$result['incorrectAns'],'name'=>'INCORRECT ANS');
			$final[]=(object)array('value'=>$result['testTime'],'name'=>'TOTAL TIME(IN MIN.)');
			$final[]=(object)array('value'=>$result['timeTaken'],'name'=>'TIME TAKEN');
			$final[]=(object)array('value'=>$result['rightMarks'],'name'=>'RIGHT MARKS');
			$final[]=(object)array('value'=>$result['negativeMarks'],'name'=>'NEGATIVE MARKS');
            $final[]=(object)array('value'=>$result['myMarks'],'name'=>'TOTAL MARKS');
			$message = array('status' => 1,'message' => RECORDS_EXISTS,'result' => $final);
		}else{
				$message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'result' => array());
		}
			 $this->response($message, 200);
	}
	function getFiftyFiftyOptions_post()
	 {
		 $token=@$this->input->post('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		$que=@$this->input->post('queId');
		$recordId=@$this->input->post('recordId');
		$userId=@$this->input->post('userId');
		$timedifference=@$this->input->post('time');
		if(empty($userId) ||  empty($recordId) || empty($timedifference) || empty($que)){
			$this->response(array('status' => 0,'message' => "Provide userId,recordId,queId and time."),200);die();
		}
		 $saveTime=$this->updateTimeDifference($recordId,$timedifference);//to save time
		 $details=$this->getExamRecordDetails($recordId);
		
		$test=$details['testId'];
		$package=$details['packageId'];
		$unique=$details['userPackageID'];
		$attempts=$details['attempts'];
		/* to check number of 50-50 attempts */
		$chancesCount=$this->getAllRecords(TBL_OPTIONS_REFERENCE,$where=array('user_id'=>$userId,'test_id'=>$test,'userPackage'=>$unique,'attempts'=>$attempts,'isActive'=>1),'*');
		 if(count($chancesCount) >=3){
			$this->response(array('status'=>0,'message' => 'You are unable to take 50-50 option.You have used your three 50-50 chances.'), 200); die();
		} 
		/* end */
		 $IncorrectAnswers=array();
		
		 $where=array('que_id'=>$que,'is_answer'=>1,'test_id'=>$test,'isActive'=>1,'isDeleted'=>0);
		 $correctAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*");
		 $correctAnswers=($correctAnswers)?$correctAnswers:array();
		
		 $where=array('que_id'=>$que,'is_answer'=>0,'test_id'=>$test,'isActive'=>1,'isDeleted'=>0);
		 $IncorrectAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*");
		 $IncorrectAnswers=($IncorrectAnswers)?$IncorrectAnswers:array();
		 //new
		 if(empty($correctAnswers) && $IncorrectAnswers){
		  $incorctOption=$IncorrectAnswers[0]->option_id;
		  if(count($correctAnswers)<=0){
			  $correctAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*",$incorctOption);
		  }
		 }
		 if(empty($IncorrectAnswers) && $correctAnswers){
			 $where=array('que_id'=>$que,'is_answer'=>1,'test_id'=>$test,'isActive'=>1,'isDeleted'=>0);
		  $corctOption=$correctAnswers[0]->option_id;
		  if(count($IncorrectAnswers)<=0){
			  $IncorrectAnswers=$this->Testsmodel->getRandomElements(TBL_QUE_OPTIONS,$where,$select="*",$corctOption);
		  }
		 }
		  $details=array_merge($correctAnswers,$IncorrectAnswers);
		
		 foreach($details as $detail){ 
			 $optArr[]=$detail->option_id;
			 $detail->option=strip_tags($detail->option);
		 }
		 $opt_id=implode(',',$optArr);
		 $options=$details;
		 $insertData['user_id']=$userId;
		 $insertData['test_id']=$test;
		 $insertData['que_id']=$que;
		 $insertData['opt_id']=$opt_id;
		 $insertData['userPackage']=$unique;
		 $insertData['attempts']=$attempts;
		 $where=array();
		 $result=$this->insertOrUpdate(TBL_OPTIONS_REFERENCE,$where,$insertData);
		
		if($result && $options){
		      $message = array('status' => 1,'message' => RECORDS_EXISTS,'options' => $options);
		}else{
				$message = array('status' => 0, 'message' => NO_RECORDS_EXISTS,'options' => array());
		}
		 $this->response($message, 200);
	}
	/* This method is used to get summary report */
	
	function summaryReport_get(){
		 $token=@$this->get('token');
		  if(empty($token))
			{
				$this->response(array('status'=>0,'message' => 'token is required.'), 200); die();
			}
		   $wrongToken= $this->checkTokenValid($token);
		   if($wrongToken){
				$message = $this->response(array('status' => 5, 'message' => "Wrong Token"),200);die();
		   }
		$data=array();
	    $questionString="";
		$userId=@$this->input->get('userId');
	    $recordId=@$this->input->get('recordId'); 
		$start=@$this->input->get('start');
		$offset=100;
	    if(empty($userId) || empty($recordId)){
			$this->response(array('status' => 0,'message' => "Please provide userId and recordId."),200);die();
		}
	    $savedResults=$this->getExamRecordDetails($recordId);
		 if(empty($savedResults)){
			  $this->response(array('status' => 0,'message' => 'Please provide valid record id.'),200);die();
		  } 
		  $testTime=$savedResults['testTime'];
		  $testId=$savedResults['testId'];
		  $packageId=$savedResults['packageId'];
		  $uniqueId=$savedResults['userPackageID'];
		  $testType=$savedResults['testType'];
		  $attempts=$savedResults['attempts'];
		  
		  $testDetails=$this->testDetailsById($testId); 
		  $isShuffle=$testDetails['isShuffleQueOrder']; 
		  $ReferenceTestId=$this->Testsmodel->getReferenceQuestions($testId,$userId,$uniqueId,$attempts);
		  $QUEarray=$ReferenceTestId;
		 $data['testTime']=$testTime;
		 $data['question']= $this->Testsmodel->getQuestionInSummary($userId,$testId,$packageId,$uniqueId,$testType,$attempts,$QUEarray,$start,$offset); 
		 $questions=$this->removeTagsFromQuens($data['question']);
		 /* start */
		 $userOtpArry=array();
		 $crctOptionArray=array();
		 if($questions){
			 foreach($questions as $qn){
				 if($qn->queType == 2 || $qn->queType == 3){
				    $where=array('que_id'=>$qn->que_id,'test_id'=>$testId,'isActive'=>1,'isDeleted'=>0);
			      }else{
				     $where=array('que_id'=>$qn->que_id,'is_answer'=>1,'test_id'=>$testId,'isActive'=>1,'isDeleted'=>0);
			      }
			   $questionAnswers=$this->getAllRecords(TBL_QUE_OPTIONS,$where,$select="option_id,option,textType"); 
			   $userAnswers=$this->Testsmodel->getMyAnswers($userId,$testId,$uniqueId,$testType,$qn->que_id,$attempts);
			    if($qn->queType == 0 || $qn->queType == 1){
			  if($questionAnswers){
					foreach($questionAnswers as $qa){
						$crctOptionArray[]=$qa->option_id;
					}
				}
				if($userAnswers){

					foreach($userAnswers as $ua){
					    $notps=$ua->normalOptions;
						for($j=0;$j<count($notps);$j++){
							$userOtpArry[$j]=$notps[$j]->option_id;
							
						}
						$userOtpArry=array_filter($userOtpArry);
					}
				}
				if(is_array($crctOptionArray) && is_array($userOtpArry) && count($crctOptionArray)>0 && 
					   count($userOtpArry) == count($crctOptionArray) &&
					   array_diff($userOtpArry, $crctOptionArray) === array_diff($crctOptionArray, $userOtpArry))
					   {$isCorrect='yes';}else{$isCorrect='No';}
					  
	      }else{
			  
				  @$crctOpt=$questionAnswers[0]->option;
				  @$myopt=$userAnswers[0]->textOption;
				  
				   if($crctOpt && $crctOpt==$myopt)
				   {$isCorrect='yes';}
				   else{$isCorrect='No';}
			}
			$qn->isCorrect=$isCorrect;
			
			}
		 }
		 $d['testName']=$testDetails['testName'];
		 $d['questions']=$questions;
		 /* end */
		if($questions){
			 $this->response(array('status' => 1,'message' => RECORDS_EXISTS,'result'=>$d),200);die();
		}else{
			 $this->response(array('status' =>2,'message' => NO_RECORDS_EXISTS,'result'=>$d),200);die();
		}
		 
		
	}
	
}

?>